# PMBV2-Development
Website PMB Development UIN Sunan Gunung Djati Bandung V.2.0

###############################
Configuration PMBV2-Development
###############################

- Codeigniter 3.1.8
- PHP 5.6
- Mysql 5.7
- Nginx 1.14.0

************
Autoload.php
************

- $autoload['libraries'] = array('database', 'email', 'session', 'form_validation', 'upload');
- $autoload['drivers'] = array('cache');
- $autoload['helper'] = array('url', 'file', 'form', 'tgl_indonesia');

**********
Config.php
**********

- $config['base_url'] = 'http://localhost/PMBV2-Development/';
- $config['index_page'] = '';
- $config['log_threshold'] = 4;
- $config['cache_query_string'] = TRUE;

**********
Routes.php
**********
- $route['default_controller'] = 'dashboard';
- $route['404_override'] = '';
- $route['translate_uri_dashes'] = FALSE;

*********
.Htaccess
*********

- RewriteEngine On
- RewriteCond %{REQUEST_FILENAME} !-f
- RewriteCond %{REQUEST_FILENAME} !-d
- RewriteRule ^(.*)$ index.php/$1 [L]
