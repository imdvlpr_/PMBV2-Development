<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class View_pradaftar_ujian extends CI_Model {

	function read(){
		return $this->db->get('view_pradaftar_ujian');
	}

	function whereAnd($data){
		$this->db->where($data);
		return $this->db->get('view_pradaftar_ujian');
	}

	function whereOr($data){
		$this->db->or_where($data);
		return $this->db->get('view_pradaftar_ujian');
	}

	function likeAnd($data){
		$this->db->like($data);
		return $this->db->get('view_pradaftar_ujian');
	}

	function likeOr($data){
		$this->db->or_like($data);
		return $this->db->get('view_pradaftar_ujian');
	}

}
