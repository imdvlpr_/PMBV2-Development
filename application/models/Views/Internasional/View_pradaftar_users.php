<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class View_internasional_users extends CI_Model {

	function read(){
		return $this->db->get('view_internasional_users');
	}

	function whereAnd($data){
		$this->db->where($data);
		return $this->db->get('view_internasional_users');
	}

	function whereOr($data){
		$this->db->or_where($data);
		return $this->db->get('view_internasional_users');
	}

	function likeAnd($data){
		$this->db->like($data);
		return $this->db->get('view_internasional_users');
	}

	function likeOr($data){
		$this->db->or_like($data);
		return $this->db->get('view_internasional_users');
	}

}
