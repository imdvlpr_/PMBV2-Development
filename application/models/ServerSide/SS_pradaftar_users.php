<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class SS_pradaftar_users extends CI_Model {
    var $table = "view_pradaftar_users";  //table yang ingin di tampilkan
    var $select_column = array(
        "id_pradaftar_users",
        "kd_pembayaran",
        "nomor_peserta",
        "nik_passport",
        "nama",
        "kategori",
        "tipe_ujian",
        "pembayaran",
        "biodata",
    );  //sesuaikan dengan nama field table
	var $order_column = array(
        null,
	    "kd_pembayaran",
        "nomor_peserta",
        "nik_passport",
        "nama",
        "kategori",
        "tipe_ujian",
        "pembayaran",
        "biodata",
    );

	function make_query(){
		$this->db->select($this->select_column);
		$this->db->from($this->table);
		if(isset($_POST["search"]["value"])) {
			$this->db->like("kd_pembayaran", $_POST["search"]["value"]);
            $this->db->or_like("nomor_peserta", $_POST["search"]["value"]);
            $this->db->or_like("nik_passport", $_POST["search"]["value"]);
			$this->db->or_like("nama", $_POST["search"]["value"]);
			$this->db->or_like("kategori", $_POST["search"]["value"]);
			$this->db->or_like("tipe_ujian", $_POST["search"]["value"]);
			$this->db->or_like("pembayaran", $_POST["search"]["value"]);
			$this->db->or_like("biodata", $_POST["search"]["value"]);
		}
		if(isset($_POST["order"])) {
			$this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} else {
			$this->db->order_by('kd_pembayaran', 'DESC');
		}
	}

	function make_datatables(){
		$this->make_query();
		if($_POST["length"] != -1) {
			$this->db->limit($_POST['length'], $_POST['start']);
		}
		$query = $this->db->get();
		return $query->result();
	}

	function get_filtered_data(){
		$this->make_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	function get_all_data(){
		$this->db->select("*");
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}
}
