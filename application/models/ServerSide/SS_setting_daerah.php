<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class SS_setting_daerah extends CI_Model {
	var $table = "view_setting_daerah";  //table yang ingin di tampilkan
	var $select_column = array(
		"id_provinsi",
		"provinsi",
		"id_kabupaten",
		"kabupaten",
		"id_kecamatan",
		"kecamatan",
        "id_kelurahan",
		"kelurahan",
		"id_jenis_daerah",
		"jenis_daerah",
		"date_created",
		"date_updated"
    );  //sesuaikan dengan nama field table
	var $order_column = array(
        null,
	    "id_provinsi",
		"provinsi",
		"id_kabupaten",
		"kabupaten",
		"id_kecamatan",
		"kecamatan",
        "id_kelurahan",
		"kelurahan",
		"id_jenis_daerah",
		"jenis_daerah",
		"date_created",
		"date_updated"
    );

	function make_query(){
		$this->db->select($this->select_column);
		$this->db->from($this->table);
		if(isset($_POST["search"]["value"])) {
			$this->db->like("id_provinsi", $_POST["search"]["value"]);
			$this->db->or_like("provinsi", $_POST["search"]["value"]);
			$this->db->or_like("id_kabupaten", $_POST["search"]["value"]);
			$this->db->or_like("kabupaten", $_POST["search"]["value"]);
			$this->db->or_like("id_kecamatan", $_POST["search"]["value"]);
			$this->db->or_like("kecamatan", $_POST["search"]["value"]);
			$this->db->or_like("id_kelurahan", $_POST["search"]["value"]);
			$this->db->or_like("kelurahan", $_POST["search"]["value"]);
			$this->db->or_like("id_jenis_daerah", $_POST["search"]["value"]);
			$this->db->or_like("jenis_daerah", $_POST["search"]["value"]);
			$this->db->or_like("date_created", $_POST["search"]["value"]);
			$this->db->or_like("date_updated", $_POST["search"]["value"]);
		}
		if(isset($_POST["order"])) {
			$this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} else {
			$this->db->order_by('id_kecamatan', 'DESC');
		}
	}

	function make_datatables(){
		$this->make_query();
		if($_POST["length"] != -1) {
			$this->db->limit($_POST['length'], $_POST['start']);
		}
		$query = $this->db->get();
		return $query->result();
	}

	function get_filtered_data(){
		$this->make_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	function get_all_data(){
		$this->db->select("*");
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}
}
