<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class SS_penyesuaian_ukt extends CI_Model {

	var $table = "view_penyesuaian_ukt";  //table yang ingin di tampilkan

	var $select_column = array("nik_passport", "nama", "score", "kategori", "jumlah", "status", "rekomendasi");  //sesuaikan dengan nama field table

	var $order_column = array("nik_passport", "nama", "score", "kategori", "jumlah", "status", "rekomendasi",null);



	function make_query(){

		$this->db->select($this->select_column);

		$this->db->from($this->table);

		if(isset($_POST["search"]["value"])) {

			$this->db->like("nik_passport", $_POST["search"]["value"]);

			$this->db->or_like("nama", $_POST["search"]["value"]);

			$this->db->or_like("score", $_POST["search"]["value"]);

			$this->db->or_like("kategori", $_POST["search"]["value"]);

			$this->db->or_like("jumlah", $_POST["search"]["value"]);

			$this->db->or_like("status", $_POST["search"]["value"]);

			$this->db->or_like("rekomendasi", $_POST["search"]["value"]);

		}

		if(isset($_POST["order"])) {

			$this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);

		} else {

			$this->db->order_by('nik_passport', 'DESC');

		}

	}



	function make_datatables(){

		$this->make_query();

		if($_POST["length"] != -1) {

			$this->db->limit($_POST['length'], $_POST['start']);

		}

		$query = $this->db->get();

		return $query->result();

	}



	function get_filtered_data(){

		$this->make_query();

		$query = $this->db->get();

		return $query->num_rows();

	}



	function get_all_data(){

		$this->db->select("*");

		$this->db->from($this->table);

		return $this->db->count_all_results();

	}

}

