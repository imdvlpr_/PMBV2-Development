<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class SS_setting_jadwal extends CI_Model {
	var $table = "view_setting_jadwal";  //table yang ingin di tampilkan
	var $select_column = array(
		'id_jadwal',
		'tipe_ujian',
		'tanggal',
		'kampus',
		'gedung',
		'ruangan',
		'sesi',
		'jam_awal',
		'jam_akhir',
		'quota',
		'status',
    );  //sesuaikan dengan nama field table
	var $order_column = array(
        null,
        'tipe_ujian',
        'tanggal',
        'kampus',
        'gedung',
        'ruangan',
        'sesi',
        'jam_awal',
        'jam_akhir',
        'quota',
        'status',
    );

	function make_query(){
		$this->db->select($this->select_column);
		$this->db->from($this->table);
		if(isset($_POST["search"]["value"])) {
			$this->db->or_like("tipe_ujian", $_POST["search"]["value"]);
			$this->db->or_like("tanggal", $_POST["search"]["value"]);
			$this->db->or_like("kampus", $_POST["search"]["value"]);
			$this->db->or_like("gedung", $_POST["search"]["value"]);
			$this->db->or_like("ruangan", $_POST["search"]["value"]);
			$this->db->or_like("sesi", $_POST["search"]["value"]);
			$this->db->or_like("jam_awal", $_POST["search"]["value"]);
			$this->db->or_like("jam_akhir", $_POST["search"]["value"]);
			$this->db->or_like("status", $_POST["search"]["value"]);
		}
		if(isset($_POST["order"])) {
			$this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} else {
			$this->db->order_by('tanggal', 'DESC');
		}
	}

	function make_datatables(){
		$this->make_query();
		if($_POST["length"] != -1) {
			$this->db->limit($_POST['length'], $_POST['start']);
		}
		$query = $this->db->get();
		return $query->result();
	}

	function get_filtered_data(){
		$this->make_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	function get_all_data(){
		$this->db->select("*");
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}
}
