<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class SS_daftar_kelulusan extends CI_Model {
	var $table = "view_daftar_kelulusan";  //table yang ingin di tampilkan
	var $select_column = array("nomor_peserta", "nama", "jurusan", "fakultas", "jalur_masuk", "status_pendaftaran", "tahun_pendaftaran");  //sesuaikan dengan nama field table
	var $order_column = array("nomor_peserta", "nama", "jurusan", "fakultas", "jalur_masuk", "status_pendaftaran", "tahun_pendaftaran",null);

	function make_query(){
		$this->db->select($this->select_column);
		$this->db->from($this->table);
		if(isset($_POST["search"]["value"])) {
			$this->db->like("nomor_peserta", $_POST["search"]["value"]);
			$this->db->or_like("nama", $_POST["search"]["value"]);
			$this->db->or_like("jurusan", $_POST["search"]["value"]);
			$this->db->or_like("fakultas", $_POST["search"]["value"]);
			$this->db->or_like("jalur_masuk", $_POST["search"]["value"]);
			$this->db->or_like("status_pendaftaran", $_POST["search"]["value"]);
			$this->db->or_like("tahun_pendaftaran", $_POST["search"]["value"]);
		}
		if(isset($_POST["order"])) {
			$this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} else {
			$this->db->order_by('nomor_peserta', 'DESC');
		}
	}

	function make_datatables(){
		$this->make_query();
		if($_POST["length"] != -1) {
			$this->db->limit($_POST['length'], $_POST['start']);
		}
		$query = $this->db->get();
		return $query->result();
	}

	function get_filtered_data(){
		$this->make_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	function get_all_data(){
		$this->db->select("*");
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}
}
