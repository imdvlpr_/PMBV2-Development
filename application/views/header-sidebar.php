<aside class="main-sidebar">
    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?=base_url('assets/img/avatar.png');?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p><?=$this->session->userdata('nama');?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header text-center">Development</li>
            <li><a href="<?=base_url('Dashboard');?>"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>

            <!-- Daftar -->
            <li class="treeview">
                <a href="#"><i class="fa fa-folder">&nbsp;</i>
					<span>Daftar</span>
                    <span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
					<!-- Export -->
					<li class="treeview">
						<a href="#"><i class="fa fa-download">&nbsp;</i>
							<span>Export</span>
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<li><a href="<?=base_url('Daftar/Export/PDDIKTI/');?>"><i class="fa fa-circle-o"></i>PDDIKTI</a></li>
							<li><a href="<?=base_url('Daftar/Export/Akademik/');?>"><i class="fa fa-circle-o"></i>Akademik</a></li>
							<li><a href="<?=base_url('Daftar/Export/Biodata/');?>"><i class="fa fa-circle-o"></i>Biodata</a></li>
							<li><a href="<?=base_url('Daftar/Export/Users/');?>"><i class="fa fa-circle-o"></i>Users</a></li>
							<li><a href="<?=base_url('Daftar/Export/Kelulusan/');?>"><i class="fa fa-circle-o"></i>Kelulusan</a></li>
						</ul>
					</li>
					<!-- Import -->
					<li class="treeview">
						<a href="#">
							<i class="fa fa-upload">&nbsp;</i>
							<span>Import</span>
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<li><a href="<?=base_url('Daftar/Import/Kelulusan/');?>"><i class="fa fa-circle-o"></i>Kelulusan</a></li>
							<li><a href="<?=base_url('Daftar/Import/NIM/');?>"><i class="fa fa-circle-o"></i>NIM</a></li>
							<li><a href="<?=base_url('Daftar/Import/StatusBayar/');?>"><i class="fa fa-circle-o"></i>Status Bayar</a></li>
							<!-- Import -->
							<li class="treeview">
								<a href="#">
									<i class="fa fa-circle-o"></i>
									<span>Penyesuaian</span>
									<span class="pull-right-container">
										<i class="fa fa-angle-left pull-right"></i>
									</span>
								</a>
								<ul class="treeview-menu">
									<li><a href="<?=base_url('Daftar/Import/Penyesuaian/PenyesuaianUsers');?>"><i class="fa fa-circle-o"></i>Users</a></li>
									<li><a href="<?=base_url('Daftar/Import/Penyesuaian/PenyesuaianUKT');?>"><i class="fa fa-circle-o"></i>UKT</a></li>
									<li><a href="<?=base_url('Daftar/Import/Penyesuaian/PenyesuaianPddikti');?>"><i class="fa fa-circle-o"></i>PDDIKTI</a></li>
									<li><a href="<?=base_url('Daftar/Import/Penyesuaian/PenyesuaianExtra');?>"><i class="fa fa-circle-o"></i>Extra</a></li>
									<li><a href="<?=base_url('Daftar/Import/Penyesuaian/PenyesuaianGambar');?>"><i class="fa fa-circle-o"></i>Gambar</a></li>
								</ul>
							</li>
						</ul>
					</li>
					<!-- Settings -->
					<li class="treeview">
						<a href="#"><i class="fa fa-gears">&nbsp;</i>
							<span>Setting</span>
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<li><a href="<?=base_url('Daftar/Settings/AsalSekolah/');?>"><i class="fa fa-gear">&nbsp;</i>Asal Sekolah</a></li>
							<li><a href="<?=base_url('Daftar/Settings/AlatTransportasi/');?>"><i class="fa fa-gear">&nbsp;</i>Alat Transportasi</a></li>
							<li><a href="<?=base_url('Daftar/Settings/JalurMasuk/');?>"><i class="fa fa-gear">&nbsp;</i>Jalur Masuk</a></li>
							<li><a href="<?=base_url('Daftar/Settings/JenisPendaftaran/');?>"><i class="fa fa-gear">&nbsp;</i>Jenis Pendaftaran</a></li>
							<li><a href="<?=base_url('Daftar/Settings/JenisTinggal/');?>"><i class="fa fa-gear">&nbsp;</i>Jenis Tinggal</a></li>
							<li><a href="<?=base_url('Daftar/Settings/PembayaranListrik/');?>"><i class="fa fa-gear">&nbsp;</i>Pembayaran Listrik</a></li>
							<li><a href="<?=base_url('Daftar/Settings/PembayaranPbb/');?>"><i class="fa fa-gear">&nbsp;</i>Pembayaran PBB</a></li>
							<li><a href="<?=base_url('Daftar/Settings/RekeningListrik/');?>"><i class="fa fa-gear">&nbsp;</i>Rekening Listrik</a></li>
							<li><a href="<?=base_url('Daftar/Settings/RekeningPbb/');?>"><i class="fa fa-gear">&nbsp;</i>Rekening PBB</a></li>
							<li><a href="<?=base_url('Daftar/Settings/Tanggungan/');?>"><i class="fa fa-gear">&nbsp;</i>Tanggungan</a></li>
							<li><a href="<?=base_url('Daftar/Settings/TipeGambar/');?>"><i class="fa fa-gear"> </i>Tipe Gambar</a></li>
						</ul>
					</li>
					<!-- Statistik -->
					<li class="treeview">
						<a href="#"><i class="fa fa-line-chart">&nbsp;</i>
							<span>Statistik</span>
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<li><a href="<?=base_url('Daftar/Statistik/Users/');?>"><i class="fa fa-circle-o"></i>Users</a></li>
							<li><a href="<?=base_url('Daftar/Statistik/Kelulusan/');?>"><i class="fa fa-circle-o"></i>Kelulusan</a></li>
							<li><a href="<?=base_url('Daftar/Statistik/JurusanFakultas/');?>"><i class="fa fa-circle-o"></i>Jurusan & Fakultas</a></li>
						</ul>
					</li>
					<li><a href="<?=base_url('Daftar/Kelulusan/');?>"><i class="fa fa-circle-o"></i>Kelulusan</a></li>
					<li><a href="<?=base_url('Daftar/Users/');?>"><i class="fa fa-circle-o"></i>Users</a></li>
                </ul>
            </li>

			<!-- Internasional -->
			<li class="treeview">
				<a href="#">
					<i class="fa fa-folder">&nbsp;</i>
					<span>Internasional</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<!-- Export -->
					<li class="treeview">
						<a href="#"><i class="fa fa-download">&nbsp;</i>
							<span>Export</span>
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<li class="treeview">
		                        <a href="#">
		                            <i class="fa fa-circle-o">&nbsp;</i>
		                            <span>Users</span>
		                            <span class="pull-right-container">
										<i class="fa fa-angle-left pull-right"></i>
									</span>
		                        </a>
		                        <ul class="treeview-menu">
		                        	<li><a href="<?=base_url('Internasional/ExportExcel/');?>"><i class="fa fa-circle-o"></i>Semua</a></li>
		                            <li><a href="<?=base_url('Internasional/ExportExcel/SudahBayar/');?>"><i class="fa fa-circle-o"></i>Sudah Bayar</a></li>
		                            <li><a href="<?=base_url('Internasional/ExportExcel/BelumBayar/');?>"><i class="fa fa-circle-o"></i>Belum Bayar</a></li>
		                        </ul>
		                    </li>
							<li><a href="<?=base_url('Internasional/Export/Kelulusan/');?>"><i class="fa fa-circle-o"></i>Kelulusan</a></li>
						</ul>
					</li>
					<!-- Import -->
					<li class="treeview">
						<a href="#">
							<i class="fa fa-upload">&nbsp;</i>
							<span>Import</span>
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<li><a href="<?=base_url('Internasional/Import/Kelulusan/');?>"><i class="fa fa-circle-o"></i>Kelulusan</a></li>
							<li><a href="<?=base_url('Internasional/Import/NIM/');?>"><i class="fa fa-circle-o"></i>NIM</a></li>
						</ul>
					</li>
					<!-- Statistik -->
					<li class="treeview">
						<a href="#"><i class="fa fa-line-chart">&nbsp;</i>
							<span>Statistik</span>
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<li><a href="<?=base_url('Internasional/Statistik/Users/');?>"><i class="fa fa-circle-o"></i>Users</a></li>
							<li><a href="<?=base_url('Internasional/Statistik/Kelulusan/');?>"><i class="fa fa-circle-o"></i>Kelulusan</a></li>
							<li><a href="<?=base_url('Internasional/Statistik/PemilihanJurusan/');?>"><i class="fa fa-circle-o"></i>Pemilihan Jurusan</a></li>
							<li><a href="<?=base_url('Internasional/Statistik/PerNegara/');?>"><i class="fa fa-circle-o"></i>Per Negara</a></li>
						</ul>
					</li>
					<li class="treeview">
						<a href="#">
							<i class="fa fa-circle-o">&nbsp;</i>
							<span>Users</span>
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<li><a href="<?=base_url('Internasional/Users/SudahBayar/');?>"><i class="fa fa-circle-o"></i>Sudah Bayar</a></li>
							<li><a href="<?=base_url('Internasional/Users/BelumBayar/');?>"><i class="fa fa-circle-o"></i>Belum Bayar</a></li>
						</ul>
					</li>
				</ul>
			</li>

			<!-- Pradaftar -->
			<li class="treeview">
				<a href="#"><i class="fa fa-folder">&nbsp;</i>
					<span>Pradaftar</span>
					<span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
				</a>
				<ul class="treeview-menu">
					<!-- Export -->
					<li class="treeview">
						<a href="#"><i class="fa fa-download">&nbsp;</i>
							<span>Export</span>
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<li><a href="<?=base_url('Pradaftar/Export/ABHP/');?>"><i class="fa fa-circle-o"></i>ABHP dan Berita Acara</a></li>
							<li><a href="<?=base_url('Pradaftar/Export/Akademik/');?>"><i class="fa fa-circle-o"></i>Akademik</a></li>
							<li><a href="<?=base_url('Pradaftar/Export/Ikopin/');?>"><i class="fa fa-circle-o"></i>Ikopin</a></li>
							<li><a href="<?=base_url('Pradaftar/Export/Jadwal/');?>"><i class="fa fa-circle-o"></i>Jadwal</a></li>
						</ul>
					</li>
					<!-- Import -->
					<li class="treeview">
						<a href="#">
							<i class="fa fa-upload">&nbsp;</i>
							<span>Import</span>
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<li><a href="<?=base_url('Pradaftar/Import/Pembayaran/');?>"><i class="fa fa-circle-o"></i>Pembayaran</a></li>
						</ul>
					</li>
					<li><a href="<?=base_url('Pradaftar/Statistik/');?>"><i class="fa fa-line-chart"></i>Statistik</a></li>
					<li><a href="<?=base_url('Pradaftar/Pembayaran/');?>"><i class="fa fa-money"></i>Pembayaran</a></li>
					<li><a href="<?=base_url('Pradaftar/TmpUsers/');?>"><i class="fa fa-users"></i>Temporary Users</a></li>
					<li><a href="<?=base_url('Pradaftar/Users/');?>"><i class="fa fa-users"></i>Users</a></li>
				</ul>
			</li>

			<!-- Penyesuaian -->
            <li class="treeview">
                <a href="#"><i class="fa fa-folder">&nbsp;</i>
					<span>Penyesuaian</span>
                    <span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                	<!-- Import -->
					<li class="treeview">
						<a href="#">
							<i class="fa fa-upload">&nbsp;</i>
							<span>Import</span>
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<li><a href="<?=base_url('Penyesuaian/Import/Kuota/');?>"><i class="fa fa-circle-o"></i>Kuota</a></li>
							<li><a href="<?=base_url('Penyesuaian/Import/RekomendasiUKT/');?>"><i class="fa fa-circle-o"></i>Rekomendasi UKT</a></li>
						</ul>
					</li>
					<li><a href="<?=base_url('Penyesuaian/Users/');?>"><i class="fa fa-circle-o"></i>PDDIKTI</a></li>
					<li><a href="<?=base_url('Penyesuaian/UKT/');?>"><i class="fa fa-circle-o"></i>UKT</a></li>
                </ul>
            </li>

			<!-- Kelulusan -->
			<li class="treeview">
				<a href="#"><i class="fa fa-folder">&nbsp;</i>
					<span>Kelulusan</span>
					<span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
				</a>
				<ul class="treeview-menu">
					<!-- Export -->
					<li class="treeview">
						<a href="#"><i class="fa fa-download">&nbsp;</i>
							<span>Export</span>
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<li><a href="<?=base_url('Kelulusan/Export/Jurusan/');?>"><i class="fa fa-circle-o"></i>Jurusan</a></li>
							<li><a href="<?=base_url('Kelulusan/Export/Kelulusan/');?>"><i class="fa fa-circle-o"></i>Kelulusan</a></li>
							<li><a href="<?=base_url('Kelulusan/Export/Ujian/');?>"><i class="fa fa-circle-o"></i>Ujian</a></li>
						</ul>
					</li>
					<!-- Import -->
					<li class="treeview">
						<a href="#">
							<i class="fa fa-upload">&nbsp;</i>
							<span>Import</span>
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<li><a href="<?=base_url('Kelulusan/Import/Afirmasi/');?>"><i class="fa fa-circle-o"></i>Afirmasi</a></li>
							<li><a href="<?=base_url('Kelulusan/Import/CekKelulusan/');?>"><i class="fa fa-circle-o"></i>Cek Kelulusan</a></li>
							<li><a href="<?=base_url('Kelulusan/Import/Kelulusan/');?>"><i class="fa fa-circle-o"></i>Kelulusan</a></li>
							<li><a href="<?=base_url('Kelulusan/Import/Nilai/');?>"><i class="fa fa-circle-o"></i>Nilai</a></li>
						</ul>
					</li>
					<li><a href="<?=base_url('Kelulusan/GenerateGrade/');?>"><i class="fa fa-circle-o"></i>Generate Grade Jurusan</a></li>
					<li><a href="<?=base_url('Kelulusan/GenerateKelulusan/');?>"><i class="fa fa-circle-o"></i>Generate Kelulusan</a></li>
					<li><a href="<?=base_url('Kelulusan/GenerateNilai/');?>"><i class="fa fa-circle-o"></i>Generate Nilai</a></li>
				</ul>
			</li>

            <!-- Uang Kuliah Tunggal -->
			<li class="treeview">
                <a href="#"><i class="fa fa-folder"></i> <span>Uang Kuliah Tunggal</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
					<!-- Export -->
					<li class="treeview">
						<a href="#"><i class="fa fa-download">&nbsp;</i>
							<span>Export</span>
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<!--<li><a href="<?/*=base_url('Export/Pradaftar/ABHP/');*/?>"><i class="fa fa-circle-o"></i>ABHP</a></li>-->
						</ul>
					</li>
					<!-- Histori -->
					<li class="treeview">
						<a href="#"><i class="fa fa-table"></i> <span>Histori</span>
							<span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
						</a>
						<ul class="treeview-menu">
							<li><a href="<?=base_url('UKT/Histori/BobotNilaiUkt/');?>"><i class="fa fa-circle-o"></i>Bobot Nilai UKT</a></li>
							<li><a href="<?=base_url('UKT/Histori/BobotRangeUkt/');?>"><i class="fa fa-circle-o"></i>Bobot Range UKT</a></li>
						</ul>
					</li>
					<!-- Import -->
					<li class="treeview">
						<a href="#">
							<i class="fa fa-upload">&nbsp;</i>
							<span>Import</span>
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<li><a href="<?=base_url('UKT/Import/PenetapanUKT/');?>"><i class="fa fa-circle-o"></i>Penetapan UKT</a></li>
						</ul>
					</li>
					<li><a href="<?=base_url('UKT/Generate/');?>"><i class="fa fa-circle-o"></i>Generate</a></li>
					<li><a href="<?=base_url('UKT/ResetGenerate/');?>"><i class="fa fa-circle-o"></i>Reset Generate</a></li>
					<li><a href="<?=base_url('UKT/UKTJurusan/');?>"><i class="fa fa-circle-o"></i>Statistik Jurusan</a></li>
					<li><a href="<?=base_url('UKT/UKTRekapNilai/');?>"><i class="fa fa-circle-o"></i>Rekap Nilai</a></li>
                </ul>
            </li>

            <!-- Settings -->
            <li class="treeview">
                <a href="#"><i class="fa fa-gears"></i> <span>Settings</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?=base_url('Pradaftar/Setting');?>"><i class="fa fa-gear">&nbsp;</i>Pradaftar</a></li>
                </ul>
            </li>

            <!-- Advanced Settings -->
            <li class="treeview">
                <a href="#"><i class="fa fa-gears"></i> <span>Advanced Settings</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <!-- Admin -->
                    <li class="treeview">
                        <a href="#"><i class="fa fa-folder-o"></i> <span>Admin</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?=base_url('Settings/Bank/');?>"><i class="fa fa-gear">&nbsp;</i>Bank</a></li>
                            <li><a href="<?=base_url('Settings/Fakultas/');?>"><i class="fa fa-gear">&nbsp;</i>Fakultas</a></li>
                            <li><a href="<?=base_url('Settings/Jurusan/');?>"><i class="fa fa-gear">&nbsp;</i>Jurusan</a></li>
                            <li><a href="<?=base_url('Settings/SMTP/');?>"><i class="fa fa-gear">&nbsp;</i>SMTP</a></li>
                            <li><a href="<?=base_url('Settings/Users/');?>"><i class="fa fa-gear">&nbsp;</i>Users</a></li>
                        </ul>
                    </li>
                    <!-- Biodata -->
                    <li class="treeview">
                        <a href="#"><i class="fa fa-folder-o"></i> <span>Biodata</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?=base_url('Settings/Agama/');?>"><i class="fa fa-gear">&nbsp;</i>Agama</a></li>
                            <li><a href="<?=base_url('Settings/Pekerjaan/');?>"><i class="fa fa-gear">&nbsp;</i>Pekerjaan</a></li>
                            <li><a href="<?=base_url('Settings/Pendidikan/');?>"><i class="fa fa-gear">&nbsp;</i>Pendidikan</a></li>
                            <li><a href="<?=base_url('Settings/Penghasilan/');?>"><i class="fa fa-gear">&nbsp;</i>Penghasilan</a></li>
                            <li><a href="<?=base_url('Settings/Rumpun/');?>"><i class="fa fa-gear">&nbsp;</i>Rumpun</a></li>
                        </ul>
                    </li>
                    <!-- Daerah -->
                    <li class="treeview">
                        <a href="#"><i class="fa fa-folder-o"></i> <span>Daerah</span>
                            <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?=base_url('Settings/JenisDaerah/');?>"><i class="fa fa-gear">&nbsp;</i>Jenis Daerah</a></li>
                            <li><a href="<?=base_url('Settings/Provinsi/');?>"><i class="fa fa-gear">&nbsp;</i>Provinsi</a></li>
                            <li><a href="<?=base_url('Settings/Kabupaten/');?>"><i class="fa fa-gear">&nbsp;</i>Kabupaten</a></li>
                            <li><a href="<?=base_url('Settings/Kecamatan/');?>"><i class="fa fa-gear">&nbsp;</i>Kecamatan</a></li>
                            <li><a href="<?=base_url('Settings/Kelurahan/');?>"><i class="fa fa-gear">&nbsp;</i>Kelurahan</a></li>
                        </ul>
                    </li>
                    <!-- Pradaftar -->
                    <li class="treeview">
                        <a href="#"><i class="fa fa-folder-o">&nbsp;</i> <span>Pradaftar</span>
                            <span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?=base_url('Settings/Kampus/');?>"><i class="fa fa-gear">&nbsp;</i>Kampus</a></li>
                            <li><a href="<?=base_url('Settings/Gedung/');?>"><i class="fa fa-gear">&nbsp;</i>Gedung</a></li>
                            <li><a href="<?=base_url('Settings/Ruangan/');?>"><i class="fa fa-gear">&nbsp;</i>Ruangan</a></li>
                            <li><a href="<?=base_url('Settings/TipeUjian/');?>"><i class="fa fa-gear">&nbsp;</i>Tipe Ujian</a></li>
                            <li><a href="<?=base_url('Settings/Jadwal/');?>"><i class="fa fa-gear">&nbsp;</i>Jadwal</a></li>
                        </ul>
                    </li>
                    <!-- UKT -->
                    <li class="treeview">
                        <a href="#"><i class="fa fa-folder-o"></i> <span>UKT</span>
                            <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?=base_url('Settings/BobotUkt/');?>"><i class="fa fa-gear">&nbsp;</i>Bobot UKT</a></li>
                            <li><a href="<?=base_url('Settings/BobotRange/');?>"><i class="fa fa-gear">&nbsp;</i>Rentang UKT</a></li>
                        </ul>
                    </li>
 				</ul>
            </li>

            <li><a href="<?=base_url('Auth/Logout');?>"><i class="fa fa-sign-out"></i> <span>Sign Out</span></a></li>
        </ul>
    </section>
</aside>
