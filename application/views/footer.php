<footer class="main-footer">
	<!-- To the right -->
	<div class="pull-right hidden-xs">
        Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?>
    </div>
	<!-- Default to the left -->
	<strong>Copyright &copy; 2017 <a target="_blank" href="https://www.uinsgd.ac.id">UIN SGD Bandung</a> .</strong> All rights reserved. Support by <a class="link-color" target="_blank" href="https://ptipd.uinsgd.ac.id/">PTIPD.</a>
</footer>
