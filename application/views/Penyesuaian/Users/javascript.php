<!-- DataTables -->
<script src="<?=base_url('assets/js/jquery.dataTables.min.js')?>"></script>
<script src="<?=base_url('assets/js/dataTables.bootstrap.min.js')?>"></script>
<!-- page script -->
<script type="text/javascript" language="javascript">
    $(document).ready(function(){
        var dataTable = $('#users').DataTable({
            "processing":true,
            "serverSide":true,
            "order":[],
            "ajax":{
                url:"<?=base_url('Penyesuaian/Users/Json')?>",
                type:"POST"
            },
            "columnDefs":[
                {
                    "targets":[0, 6, 6],  // sesuaikan order table dengan jumlah column
                    "orderable":false,
                },
            ],
        });
    });
</script>