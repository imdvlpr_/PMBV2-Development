<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Users
            <small><i>Detail</i></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-gears"></i> Users</a></li>
            <li><a href="#"></i> Detail</a></li>
            <li class="active"><i>Edit</i></li>
        </ol>
    </section>
    <section class="content">
        <?php if (!empty($this->session->flashdata('type_message'))) { ?>
            <div class="alert alert-<?=$this->session->flashdata('type_message')?> alert-dismissible text-center" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?=$this->session->flashdata('message');?>
            </div>
        <?php } ?>
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Tabel Users Detail</h3>
                <div class="pull-right">
                    <a href="<?=base_url('index.php/Penyesuaian/Users/Edit_langkah1_mahasiswa/'.$viewDaftarPddikti->nik_passport)?>" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i></a>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-6">
                        <h3>Users</h3>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <tr>
                                    <th>NIK / Passport</th>
                                    <td><?=$viewDaftarUsers->nik_passport?></td>
                                </tr>
                                <tr>
                                    <th>Nomor Peserta</th>
                                    <td><?=$viewDaftarUsers->nomor_peserta?></td>
                                </tr>
                                <tr>
                                    <th>Jurusan</th>
                                    <td><?=$viewDaftarUsers->jurusan?></td>
                                </tr>
                                <tr>
                                    <th>Fakultas</th>
                                    <td><?=$viewDaftarUsers->fakultas?></td>
                                </tr>
                                <tr>
                                    <th>Jalur Masuk</th>
                                    <td><?=$viewDaftarPddikti->jalur_masuk?></td>
                                </tr>
                                <tr>
                                    <th>Username</th>
                                    <td><?=$viewDaftarUsers->username?></td>
                                </tr>
                                <tr>
                                    <th>Password</th>
                                    <td><?=$viewDaftarUsers->password?></td>
                                </tr>
                                <tr>
                                    <th>Tanggal Lahir</th>                                       
                                    <td><?=$viewDaftarUsers->tgl_lhr;?></td>
                                </tr>
                                <tr>
                                    <th>Tanggal Login</th>
                                    <td><?=$viewDaftarUsers->date_login?></td>
                                </tr>
                                <tr>
                                    <th>Tanggal Logout</th>
                                    <td><?=$viewDaftarUsers->date_logout?></td>
                                </tr>
                                <tr>
                                    <th>IP Login</th>
                                    <td><?=$viewDaftarUsers->ip_login?></td>
                                </tr>
                                <tr>
                                    <th>IP Logout</th>
                                    <td><?=$viewDaftarUsers->ip_logout?></td>
                                </tr>
                                <tr>
                                    <th>Verifikasi</th>
                                    <td><?=$viewDaftarUsers->status_verifikasi?></td>
                                </tr>
                                <tr>
                                    <th>Foto</th>                                       
                                    <td><a href="#viewImage" data-toggle="modal" data-target="#viewImage" class="btn btn-primary">Lihat Foto</a></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h3>KELULUSAN</h3>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <tr>
                                    <th>Nomor Peserta</th>
                                    <td><?=$viewDaftarKelulusan->nomor_peserta?></td>
                                </tr>
                                <tr>
                                    <th>Nama</th>
                                    <td><?=$viewDaftarKelulusan->nama?></td>
                                </tr>
                                <tr>
                                    <th>Jurusan</th>
                                    <td><?=$viewDaftarKelulusan->jurusan?></td>
                                </tr>
                                <tr>
                                    <th>Jalur Masuk</th>
                                    <td><?=$viewDaftarKelulusan->jalur_masuk?></td>
                                </tr>
                                <tr>
                                    <th>Status</th>
                                    <td><?=$viewDaftarKelulusan->status_pendaftaran?></td>
                                </tr>
                                <tr>
                                    <th>Tahun Daftar</th>
                                    <td><?=$viewDaftarKelulusan->tahun_pendaftaran?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <h3>PDDIKTI</h3>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <tr>
                                    <th>Jenis Kelamin</th>                                       
                                    <td><?=$viewDaftarPddikti->jenis_kelamin;?></td>
                                </tr>
                                <tr>
                                    <th>Agama</th>                                       
                                    <td><?=$viewDaftarPddikti->agama;?></td>
                                </tr>
                                <tr>
                                    <th>Tempat Lahir</th>                                       
                                    <td><?=$viewDaftarPddikti->tmp_lhr;?></td>
                                </tr>
                                <tr>
                                    <th>Tanggal Lahir</th>                                       
                                    <td><?=$viewDaftarPddikti->tgl_lhr;?></td>
                                </tr>
                                <tr>
                                    <th>NISN</th>                                       
                                    <td><?=$viewDaftarPddikti->nisn;?></td>
                                </tr>
                                <tr>
                                    <th>Jalur Masuk</th>                                       
                                    <td><?=$viewDaftarPddikti->jalur_masuk;?></td>
                                </tr>
                                <tr>
                                    <th>NPWP</th>                                       
                                    <td><?=$viewDaftarPddikti->npwp;?></td>
                                </tr>
                                <tr>
                                    <th>Warga Negara</th>                                       
                                    <td><?=$viewDaftarPddikti->warga_negara;?></td>
                                </tr>
                                <tr>
                                    <th>Jenis Pendaftaran</th>                                       
                                    <td><?=$viewDaftarPddikti->jenis_pendaftaran;?></td>
                                </tr>
                                <tr>
                                    <th>Tanggal Masuk Kuliah</th>                                       
                                    <td><?=$viewDaftarPddikti->tgl_msk_kuliah;?></td>
                                </tr>
                                <tr>
                                    <th>Mulai Semester</th>                                       
                                    <td><?=$viewDaftarPddikti->mulai_semester;?></td>
                                </tr>
                                <tr>
                                    <th>Jalan</th>                                       
                                    <td><?=$viewDaftarPddikti->jalan;?></td>
                                </tr>
                                <tr>
                                    <th>RT</th>                                       
                                    <td><?=$viewDaftarPddikti->rt;?></td>
                                </tr>
                                <tr>
                                    <th>RW</th>                                       
                                    <td><?=$viewDaftarPddikti->rw;?></td>
                                </tr>
                                <tr>
                                    <th>Nama Dusun</th>                                       
                                    <td><?=$viewDaftarPddikti->nama_dusun;?></td>
                                </tr>
                                <tr>
                                    <th>Kelurahan</th>                                       
                                    <td><?=$viewDaftarPddikti->kelurahan;?></td>
                                </tr>
                                <tr>
                                    <th>Kecamatan</th>                                       
                                    <td><?=$viewDaftarPddikti->kecamatan;?></td>
                                </tr>
                                <tr>
                                    <th>Kode POS</th>                                       
                                    <td><?=$viewDaftarPddikti->kode_pos;?></td>
                                </tr>
                                <tr>
                                    <th>Jenis Tinggal</th>                                       
                                    <td><?=$viewDaftarPddikti->jenis_tinggal;?></td>
                                </tr>
                                <tr>
                                    <th>Alat Transportasi</th>                                       
                                    <td><?=$viewDaftarPddikti->alat_transportasi;?></td>
                                </tr>
                                <tr>
                                    <th>Telepon Rumah</th>                                       
                                    <td><?=$viewDaftarPddikti->tlp_rmh;?></td>
                                </tr>
                                <tr>
                                    <th>Nomor HP</th>                                       
                                    <td><?=$viewDaftarUsers->nmr_hp;?></td>
                                </tr>
                                <tr>
                                    <th>Email</th>                                       
                                    <td><?=$viewDaftarUsers->username;?></td>
                                </tr>
                                <tr>
                                    <th>Terima KPS</th>                                       
                                    <td><?=$viewDaftarPddikti->terima_kps;?></td>
                                </tr>
                                <tr>
                                    <th>Nomor KPS</th>                                       
                                    <td><?=$viewDaftarPddikti->no_kps;?></td>
                                </tr>
                                <tr>
                                    <th>NIK Ayah</th>                                       
                                    <td><?=$viewDaftarAyah->nik_ayah;?></td>
                                </tr>
                                <tr>
                                    <th>Nama Ayah</th>                                       
                                    <td><?=$viewDaftarAyah->nama_ayah;?></td>
                                </tr>
                                <tr>
                                    <th>Tanggal Lahir Ayah</th>                                       
                                    <td><?=$viewDaftarAyah->tgl_lhr_ayah;?></td>
                                </tr>
                                <tr>
                                    <th>Pendidikan Ayah</th>                                       
                                    <td><?=$viewDaftarAyah->pendidikan_ayah;?></td>
                                </tr>
                                <tr>
                                    <th>Pekerjaan Ayah</th>                                       
                                    <td><?=$viewDaftarAyah->pekerjaan_ayah;?></td>
                                </tr>
                                <tr>
                                    <th>Penghasilan Ayah</th>                                       
                                    <td><?=$viewDaftarAyah->penghasilan_ayah;?></td>
                                </tr>
                                <tr>
                                    <th>NIK Ibu</th>                                       
                                    <td><?=$viewDaftarIbu->nik_ibu;?></td>
                                </tr>
                                <tr>
                                    <th>Nama Ibu</th>                                       
                                    <td><?=$viewDaftarIbu->nama_ibu;?></td>
                                </tr>
                                <tr>
                                    <th>Tanggal Lahir Ibu</th>                                       
                                    <td><?=$viewDaftarIbu->tgl_lhr_ibu;?></td>
                                </tr>
                                <tr>
                                    <th>Pendidikan Ibu</th>                                       
                                    <td><?=$viewDaftarIbu->pendidikan_ibu;?></td>
                                </tr>
                                <tr>
                                    <th>Pekerjaan Ibu</th>                                       
                                    <td><?=$viewDaftarIbu->pekerjaan_ibu;?></td>
                                </tr>
                                <tr>
                                    <th>Penghasilan Ibu</th>                                       
                                    <td><?=$viewDaftarIbu->penghasilan_ibu;?></td>
                                </tr>
                                <tr>
                                    <th>NIK Wali</th>                                       
                                    <td><?=$viewDaftarWali->nik_wali;?></td>
                                </tr>
                                <tr>
                                    <th>Nama Wali</th>                                       
                                    <td><?=$viewDaftarWali->nama_wali;?></td>
                                </tr>
                                <tr>
                                    <th>Tanggal Lahir Wali</th>                                       
                                    <td><?=$viewDaftarWali->tgl_lhr_wali;?></td>
                                </tr>
                                <tr>
                                    <th>Pendidikan Wali</th>                                       
                                    <td><?=$viewDaftarWali->pendidikan_wali;?></td>
                                </tr>
                                <tr>
                                    <th>Pekerjaan Wali</th>                                       
                                    <td><?=$viewDaftarWali->pekerjaan_wali;?></td>
                                </tr>
                                <tr>
                                    <th>Penghasilan Wali</th>                                       
                                    <td><?=$viewDaftarWali->penghasilan_wali;?></td>
                                </tr>
                                <tr>
                                    <th>Rekening Listrik</th>                                       
                                    <td><?=$viewDaftarExtra->rekening_listrik;?></td>
                                </tr>
                                <tr>
                                    <th>Rekening PBB</th>                                       
                                    <td><?=$viewDaftarExtra->rekening_pbb;?></td>
                                </tr>
                                <tr>
                                    <th>Tanggungan</th>                                       
                                    <td><?=$viewDaftarExtra->tanggungan;?></td>
                                </tr>
                                <tr>
                                    <th>Pembayaran Listrik</th>                                       
                                    <td><?=$viewDaftarExtra->pembayaran_listrik;?></td>
                                </tr>
                                <tr>
                                    <th>Pembayaran PBB</th>                                       
                                    <td><?=$viewDaftarExtra->pembayaran_pbb;?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h3>UKT</h3>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <tr>
                                    <th>NIK / Passport</th>
                                    <td><?=$viewDaftarUkt->nik_passport?></td>
                                </tr>
                                <tr>
                                    <th>Score</th>
                                    <td><?=$viewDaftarUkt->score?></td>
                                </tr>
                                <tr>
                                    <th>Kategori</th>
                                    <td><?=$viewDaftarUkt->kategori?></td>
                                </tr>
                                <tr>
                                    <th>Jumlah</th>
                                    <td><?=$viewDaftarUkt->jumlah?></td>
                                </tr>
                                <tr>
                                    <th>Status</th>
                                    <td><?=$viewDaftarUkt->status?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h3>Gambar-Gambar</h3>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <?php foreach($tbFileUpload as $value) : ?>
                                <tr>
                                <td><?php echo $value->tipe_gambar?></td>
                                <td>:</td>
                                <td><a href="#" data-target="#v<?php echo $value->id_tipe_gambar?>" data-toggle="modal" data-placement="top" id="rumah"  title="Klik Untuk Memperbesar" class="btn btn-primary">Lihat Foto</a></td>
                                </tr>

                                <!-- Modal View Image -->
                                <div class="modal fade" id="v<?php echo $value->id_tipe_gambar?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel"><?php echo $value->tipe_gambar?></h4>
                                    </div>
                                    <div class="modal-body">
                                    
                                        <?php if (!empty($value->gambar)) { ?>
                                            <img src="<?php echo 'https://pmb-daftar.uinsgd.ac.id/upload/foto/'.$value->id_tipe_gambar.'/'.$value->gambar?>" class="img-responsive center-block">
                                        <?php }else{?>
                                        <img src="<?=base_url('design-backend/images/no_image.gif');?>" class="img-responsive center-block">
                                        <?php } ?>
                                        <span class="label label-danger">Jika ingin mengubah silahkan lakukan upload ulang.</span>
                                    </div>
                                    <?php if ($this->session->userdata('verifikasi')=='BELUM VERIFIKASI') { ?>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                    </div>
                                    <?php } ?>
                                    </div>
                                </div>
                                </div>
                            <?php endforeach; ?>
                            </table>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>