<div class="content-wrapper">
                <section class="content-header">
                    <h1>
                        Penyesuaian
                        <small><i>Detail</i></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-gears"></i> Penyesuaian</a></li>
                        <li><a href="#"></i> Detail</a></li>
                        <li class="active"><i>Edit</i></li>
                    </ol>
                </section>
                <section class="content">
                    <?php if (!empty($this->session->flashdata('type_message'))) { ?>
                        <div class="alert alert-<?=$this->session->flashdata('type_message')?> alert-dismissible text-center" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <?=$this->session->flashdata('message');?>
                        </div>
                    <?php } ?>
                    <div class="box">
                      <div class="box-header with-border">
                            <h3 class="box-title">Langkah-langkah</h3>
                      </div>
                      <center>
                        <div class="box-body">
                          <div class="list-group">
                            <a href="<?=base_url('Penyesuaian/Users/Edit_langkah1_mahasiswa/'.$tbBM->nik_passport)?>" class="list-group-item" style="float: left">Mahasiswa</a>
                            <a href="<?=base_url('Penyesuaian/Users/Edit_langkah1_alamat/'.$tbBM->nik_passport)?>" class="list-group-item" style="float: left">Alamat Mahasiswa</a>
                            <a href="<?=base_url('Penyesuaian/Users/Edit_langkah1_orangtua/'.$tbBM->nik_passport)?>" class="list-group-item" style="float: left">Orangtua</a>
                            <a href="<?=base_url('Penyesuaian/Users/Edit_langkah1_foto/'.$tbBM->nik_passport)?>" class="list-group-item" style="float: left">Foto Mahasiswa</a>
                            <a href="#" class="list-group-item active" style="float: left">Tambahan</a>
                          </div>
                        </div>
                      </center>
                    </div>
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Tabel Penyesuaian Detail</h3>
                        </div>
                        <div class="box-body">
                            <?php $attributes = array('class' => 'form-horizontal', 'role' => 'form'); ?>
                            <?=form_open_multipart('Penyesuaian/Users/Simpan_tahap5/'.$tbBM->nik_passport,$attributes)?>
                              <div class="form-group">
                                <label class="col-sm-3 control-label">Tanggungan</label>
                                <div class="col-sm-9">
                                  <select class="form-control" name="id_tanggungan" required="">
                                  <?php foreach ($tbTanggungan as $key => $value): ?>
                                    <?php if ($value->id_tanggungan == $tbBM->id_tanggungan) { ?>
                                      <option value="<?=$value->id_tanggungan;?>" selected=""><?=$value->tanggungan;?></option>
                                    <?php }else{ ?>
                                      <option value="<?=$value->id_tanggungan;?>"><?=$value->tanggungan;?></option>
                                    <?php } ?>
                                  <?php endforeach ?>
                                </select>
                                </div>
                                <div class="col-sm-offset-3 col-sm-9">
                                  <span class="label label-danger">Wajib diisi</span>
                                </div>
                                <div class="col-sm-offset-3 col-sm-9">
                                  <span class="label label-warning">Tanggungan merupakan jumlah istri dan anak yang masih dibiayai/belum bekerja.</span>
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="col-sm-3 control-label">Kepemilikan Listrik</label>
                                <div class="col-sm-9">
                                  <select class="form-control" name="id_rekening_listrik" required="">
                                  <?php foreach ($tbRekeningListrik as $key => $value): ?>
                                    <?php if ($value->id_rekening_listrik == $tbBM->id_rekening_listrik) { ?>
                                      <option value="<?=$value->id_rekening_listrik;?>" selected=""><?=$value->rekening_listrik;?></option>
                                    <?php }else{ ?>
                                      <option value="<?=$value->id_rekening_listrik;?>"><?=$value->rekening_listrik;?></option>
                                    <?php } ?>
                                  <?php endforeach ?>
                                </select>
                                </div>
                                <div class="col-sm-offset-3 col-sm-9">
                                  <span class="label label-danger">Wajib diisi</span>
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="col-sm-3 control-label">Kepemilikan Rumah (PBB)</label>
                                <div class="col-sm-9">
                                  <select class="form-control" name="id_rekening_pbb" required="">
                                  <?php foreach ($tbRekeningPBB as $key => $value): ?>
                                    <?php if ($value->id_rekening_pbb == $tbBM->id_rekening_pbb) { ?>
                                      <option value="<?=$value->id_rekening_pbb;?>" selected=""><?=$value->rekening_pbb;?></option>
                                    <?php }else{ ?>
                                      <option value="<?=$value->id_rekening_pbb;?>"><?=$value->rekening_pbb;?></option>
                                    <?php } ?>
                                  <?php endforeach ?>
                                </select>
                                </div>
                                <div class="col-sm-offset-3 col-sm-9">
                                  <span class="label label-danger">Wajib diisi</span>
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="col-sm-3 control-label">Pembayaran Listrik</label>
                                <div class="col-sm-9">
                                  <select class="form-control" name="id_pembayaran_listrik" required="">
                                  <?php foreach ($tbPembayaranListrik as $key => $value): ?>
                                    <?php if ($value->id_pembayaran_litrik == $tbBM->id_pembayaran_litrik) { ?>
                                      <option value="<?=$value->id_pembayaran_listrik;?>" selected=""><?=$value->pembayaran_listrik;?></option>
                                    <?php }else{ ?>
                                      <option value="<?=$value->id_pembayaran_listrik;?>"><?=$value->pembayaran_listrik;?></option>
                                    <?php } ?>
                                  <?php endforeach ?>
                                </select>
                                </div>
                                <div class="col-sm-offset-3 col-sm-9">
                                  <span class="label label-danger">Wajib diisi</span>
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="col-sm-3 control-label">Pembayaran PBB</label>
                                <div class="col-sm-9">
                                  <select class="form-control" name="id_pembayaran_pbb" required="">
                                  <?php foreach ($tbPembayaranPBB as $key => $value): ?>
                                    <?php if ($value->id_pembayaran_pbb == $tbBM->id_pembayaran_pbb) { ?>
                                      <option value="<?=$value->id_pembayaran_pbb;?>" selected=""><?=$value->pembayaran_pbb;?></option>
                                    <?php }else{ ?>
                                      <option value="<?=$value->id_pembayaran_pbb;?>"><?=$value->pembayaran_pbb;?></option>
                                    <?php } ?>
                                  <?php endforeach ?>
                                </select>
                                </div>
                                <div class="col-sm-offset-3 col-sm-9">
                                  <span class="label label-danger">Wajib diisi</span>
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="col-sm-3 control-label">Alasan</label>
                                <div class="col-sm-9">
                                  <textarea class="form-control" name="alasan"><?=$tbExtra->alasan; ?></textarea>
                                </div>
                                <div class="col-sm-offset-3 col-sm-9">
                                  <span class="label label-danger">Wajib diisi</span>
                                </div>
                              </div>
                              <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-9">
                                  <button type="submit" class="btn btn-primary">SIMPAN</button>
                                </div>
                              </div>
                            <?=form_close();?>
                        </div>
                    </div>
                </section>
            </div>