<!-- View Image -->
<div class="modal fade" id="viewImage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">View Image</h4>
            </div>
            <div class="modal-body">
                <?php if (!empty($viewDaftarUsers->foto)) { ?>
                    <img class="img-responsive center-block" src="<?php echo 'https://pmb-daftar.uinsgd.ac.id/upload/foto/'.$viewDaftarUsers->foto;?>" alt="foto profile">
                <?php }else{?>
                    <img class="img-responsive center-block" src="<?=base_url('design-backend/dist/img/avatar.png');?>" alt="foto profile">
                <?php } ?>
            </div>
        </div>
    </div>
</div>