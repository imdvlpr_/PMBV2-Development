<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Penyesuaian
            <small><i>Detail</i></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-gears"></i> Penyesuaian</a></li>
            <li><a href="#"></i> Detail</a></li>
            <li class="active"><i>Edit</i></li>
        </ol>
    </section>
    <section class="content">
        <?php if (!empty($this->session->flashdata('type_message'))) { ?>
            <div class="alert alert-<?=$this->session->flashdata('type_message')?> alert-dismissible text-center" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?=$this->session->flashdata('message');?>
            </div>
        <?php } ?>
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Langkah-langkah</h3>
            </div>
            <center>
            <div class="box-body">
                <div class="list-group">
                <a href="#" class="list-group-item active" style="float: left">Mahasiswa</a>
                <a href="<?=base_url('Penyesuaian/Users/Edit_langkah1_alamat/'.$tbBM->nik_passport)?>" class="list-group-item" style="float: left">Alamat Mahasiswa</a>
                <a href="<?=base_url('Penyesuaian/Users/Edit_langkah1_orangtua/'.$tbBM->nik_passport)?>" class="list-group-item" style="float: left">Orangtua</a>
                <a href="<?=base_url('Penyesuaian/Users/Edit_langkah1_foto/'.$tbBM->nik_passport)?>" class="list-group-item" style="float: left">Foto Mahasiswa</a>
                <a href="<?=base_url('Penyesuaian/Users/Edit_langkah2_tambahan/'.$tbBM->nik_passport)?>" class="list-group-item" style="float: left">Tambahan</a>
                </div>
            </div>
            </center>
        </div>
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Tabel Penyesuaian Detail</h3>
            </div>
            <div class="box-body">
                <?php $attributes = array('class' => 'form-horizontal', 'role' => 'form'); ?>
                <?=form_open_multipart('Penyesuaian/Users/Simpan_tahap1/'.$tbBM->nik_passport,$attributes)?>
                    
                    <div class="form-group">
                    <label class="col-sm-3 control-label">Nomor Induk Kependudukan</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" readonly="" value="<?=$viewBM->nik_passport;?>">
                    </div>
                    <div class="col-sm-offset-3 col-sm-9">
                        <span class="label label-warning">Data tidak dapat diubah</span>
                    </div>
                    </div>
                    <div class="form-group">
                    <label class="col-sm-3 control-label">Nama</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" readonly="" value="<?=$viewBM->nama;?>">
                    </div>
                    <div class="col-sm-offset-3 col-sm-9">
                        <span class="label label-warning">Data tidak dapat diubah</span>
                    </div>
                    </div>
                    <div class="form-group">
                    <label class="col-sm-3 control-label">Tempat Lahir</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="tmp_lhr" value="<?=$tbBM->tmp_lhr;?>">
                    </div>
                    <div class="col-sm-offset-3 col-sm-9">
                        <span class="label label-danger">Wajib diisi</span>
                    </div>
                    </div>
                    <div class="form-group">
                    <label class="col-sm-3 control-label">Tanggal Lahir</label>
                    <div class="col-sm-9">
                        <input type="text" name="tgl_lhr" class="form-control" readonly="" value="<?=$viewBM->tgl_lhr;?>">
                    </div>
                    <div class="col-sm-offset-3 col-sm-9">
                        <span class="label label-warning">Data tidak dapat diubah</span>
                    </div>
                    </div>
                    <div class="form-group">
                    <label class="col-sm-3 control-label">Jenis Kelamin</label>
                    <div class="col-sm-9">
                        <?php if ($tbBM->jenis_kelamin == "LAKI-LAKI") { ?>
                        <label class="radio-inline">
                            <input type="radio" name="jk" value="LAKI-LAKI" checked=""> Laki - Laki
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="jk" value="PEREMPUAN"> Perempuan
                        </label>
                        <?php }else{ ?>
                        <label class="radio-inline">
                            <input type="radio" name="jk" value="LAKI-LAKI"> Laki - Laki
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="jk" value="PEREMPUAN" checked=""> Perempuan
                        </label>
                        <?php } ?>
                    </div>
                    </div>
                    <div class="form-group">
                    <label class="col-sm-3 control-label">Agama</label>
                    <div class="col-sm-9">
                        <select class="form-control" name="id_agama" required="">
                        <?php foreach ($tbSAG as $key => $value): ?>
                        <?php if ($value->id_agama == $tbBM->id_agama) { ?>
                            <option value="<?=$value->id_agama;?>" selected=""><?=$value->agama;?></option>
                        <?php }else{ ?>
                            <option value="<?=$value->id_agama;?>"><?=$value->agama;?></option>
                        <?php } ?>
                        <?php endforeach ?>
                        </select>
                    </div>
                    </div>
                    <div class="form-group">
                    <label class="col-sm-3 control-label">NISN</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="nisn" value="<?=$tbBM->nisn;?>">
                    </div>
                    <div class="col-sm-offset-3 col-sm-9">
                        <span class="label label-danger">Wajib diisi</span>
                    </div>
                    </div>
                    <div class="form-group">
                    <label class="col-sm-3 control-label">Rumpun Asal Sekolah</label>
                    <div class="col-sm-9">
                        <select class="form-control" name="id_rumpun" required="">
                        <?php foreach ($tbRumpun as $value): ?>
                        <?php if ($value->id_rumpun == $tbBM->id_rumpun) { ?>
                            <option value="<?=$value->id_rumpun;?>" selected=""><?=$value->rumpun;?></option>
                        <?php }else{ ?>
                            <option value="<?=$value->id_rumpun;?>"><?=$value->rumpun;?></option>
                        <?php } ?>
                        <?php endforeach ?>
                        </select>
                    </div>
                    </div>
                    <div class="form-group">
                    <label class="col-sm-3 control-label">Jalur Masuk</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" readonly="" value="<?=$viewBM->jalur_masuk;?>">
                    </div>
                    <div class="col-sm-offset-3 col-sm-9">
                        <span class="label label-warning">Data tidak dapat diubah</span>
                    </div>
                    </div>
                    <div class="form-group">
                    <label class="col-sm-3 control-label">NPWP</label>
                    <div class="col-sm-9">
                        <input type="number" class="form-control" name="npwp" value="<?=$tbBM->npwp;?>">
                    </div>
                    <div class="col-sm-offset-3 col-sm-9">
                        <span class="label label-warning">Optional</span>
                    </div>
                    <div class="col-sm-offset-3 col-sm-9">
                        <span class="label label-warning">Isi dengan 0 jika tidak memiliki.</span>
                    </div>
                    </div>
                    <div class="form-group">
                    <label class="col-sm-3 control-label">Kewarganegaraan</label>
                    <div class="col-sm-9">
                        <select class="form-control" name="warga_negara" required="">
                        <?php if ($tbBM->warga_negara == "WNI") { ?>
                            <option value="WNI" selected>WNI</option>
                            <option value="WNI">WNA</option>
                        <?php }else{ ?>
                            <option value="WNI">WNI</option>
                            <option value="WNI" selected>WNA</option>
                        <?php } ?>
                        </select>
                    </div>
                    <div class="col-sm-offset-3 col-sm-9">
                        <span class="label label-danger">Wajib diisi</span>
                    </div>
                    </div>
                    <div class="form-group">
                    <label class="col-sm-3 control-label">Tempat Tinggal</label>
                    <div class="col-sm-9">
                        <select class="form-control" name="id_jns_tinggal" required="">
                        <?php foreach ($tbJenisTinggal as $key => $value): ?>
                        <?php if ($value->id_jns_tinggal == $tbBM->id_jns_tinggal) { ?>
                            <option value="<?=$value->id_jns_tinggal;?>" selected=""><?=$value->jenis_tinggal;?></option>
                        <?php }else{ ?>
                            <option value="<?=$value->id_jns_tinggal;?>"><?=$value->jenis_tinggal;?></option>
                        <?php } ?>
                        <?php endforeach ?>
                    </select>
                    </div>
                    <div class="col-sm-offset-3 col-sm-9">
                        <span class="label label-danger">Wajib diisi</span>
                    </div>
                    </div>
                    <div class="form-group">
                    <label class="col-sm-3 control-label">Kepemilikan Kendaraan</label>
                    <div class="col-sm-9">
                        <select class="form-control" name="id_alat_transportasi" required="">
                        <?php foreach ($tbAlatTransportasi as $key => $value): ?>
                        <?php if ($value->id_alat_transportasi == $tbBM->id_alat_transportasi) { ?>
                            <option value="<?=$value->id_alat_transportasi;?>" selected=""><?=$value->alat_transportasi;?></option>
                        <?php }else{ ?>
                            <option value="<?=$value->id_alat_transportasi;?>"><?=$value->alat_transportasi;?></option>
                        <?php } ?>
                        <?php endforeach ?>
                    </select>
                    </div>
                    <div class="col-sm-offset-3 col-sm-9">
                        <span class="label label-danger">Wajib diisi</span>
                    </div>
                    </div>
                    <div class="form-group">
                    <label class="col-sm-3 control-label">Telepon Rumah</label>
                    <div class="col-sm-9">
                        <input type="number" class="form-control" name="tlp_rmh" value="<?=$tbBM->tlp_rmh;?>">
                    </div>
                    <div class="col-sm-offset-3 col-sm-9">
                        <span class="label label-warning">Optional</span>
                    </div>
                    </div>
                    <div class="form-group">
                    <label class="col-sm-3 control-label">Nomor HP</label>
                    <div class="col-sm-9">
                        <input type="number" class="form-control" name="no_hp" value="<?=$viewBM->nmr_hp;?>">
                    </div>
                    <div class="col-sm-offset-3 col-sm-9">
                        <span class="label label-danger">Wajib diisi</span>
                    </div>
                    </div>
                    <div class="form-group">
                    <label class="col-sm-3 control-label">Email</label>
                    <div class="col-sm-9">
                        <input readonly="" type="email" class="form-control" name="email" value="<?=$viewBM->username;?>">
                    </div>
                    <div class="col-sm-offset-3 col-sm-9">
                        <span class="label label-danger">Wajib diisi</span>
                    </div>
                    </div>
                    <div class="form-group">
                    <label class="col-sm-3 control-label">Terima KPS</label>
                    <div class="col-sm-2">
                        <select class="form-control" name="terima_kps" required="">
                        <?php if ($tbBM->terima_kps == "IYA") { ?>
                            <option value="IYA" selected="">IYA</option>
                            <option value="TIDAK">TIDAK</option>
                        <?php }else{ ?>
                            <option value="IYA">IYA</option>
                            <option value="TIDAK" selected="">TIDAK</option>
                        <?php } ?>
                    </select>
                    </div>
                    <label class="col-sm-3 control-label">Nomor KPS</label>
                    <div class="col-sm-4">
                        <input type="number" class="form-control" name="no_kps" value="<?=$tbBM->no_kps;?>" disabled required>
                    </div>
                    <div class="col-sm-offset-3 col-sm-9">
                        <span class="label label-danger">Wajib diisi</span>
                    </div>
                    <div class="col-sm-offset-3 col-sm-9">
                        <span class="label label-warning">*Keterangan : KPS = Kartu Perlindungan Sosial</span>
                    </div>
                    </div>
                    <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-9">
                        <button type="submit" class="btn btn-primary">SIMPAN</button>
                    </div>
                    </div>
                <?=form_close();?>
            </div>
        </div>
    </section>
</div>