<div class="content-wrapper">
                <section class="content-header">
                    <h1>
                        Penyesuaian
                        <small><i>Detail</i></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-gears"></i> Penyesuaian</a></li>
                        <li><a href="#"></i> Detail</a></li>
                        <li class="active"><i>Edit</i></li>
                    </ol>
                </section>
                <section class="content">
                    <?php if (!empty($this->session->flashdata('type_message'))) { ?>
                        <div class="alert alert-<?=$this->session->flashdata('type_message')?> alert-dismissible text-center" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <?=$this->session->flashdata('message');?>
                        </div>
                    <?php } ?>
                    <div class="box">
                      <div class="box-header with-border">
                            <h3 class="box-title">Langkah-langkah</h3>
                      </div>
                      <center>
                        <div class="box-body">
                          <div class="list-group">
                            <a href="<?=base_url('Penyesuaian/Users/Edit_langkah1_mahasiswa/'.$tbBM->nik_passport)?>" class="list-group-item" style="float: left">Mahasiswa</a>
                            <a href="<?=base_url('Penyesuaian/Users/Edit_langkah1_alamat/'.$tbBM->nik_passport)?>" class="list-group-item" style="float: left">Alamat Mahasiswa</a>
                            <a href="<?=base_url('Penyesuaian/Users/Edit_langkah1_orangtua/'.$tbBM->nik_passport)?>" class="list-group-item" style="float: left">Orangtua</a>
                            <a href="#" class="list-group-item active" style="float: left">Foto Mahasiswa</a>
                            <a href="<?=base_url('Penyesuaian/Users/Edit_langkah2_tambahan/'.$tbBM->nik_passport)?>" class="list-group-item" style="float: left">Tambahan</a>
                          </div>
                        </div>
                      </center>
                    </div>
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Tabel Penyesuaian Detail</h3>
                        </div>
                        <div class="box-body">
                            <?php $attributes = array('class' => 'form-horizontal', 'role' => 'form'); ?>
                            <?=form_open_multipart('Penyesuaian/Users/Simpan_tahap6/'.$tbBM->nik_passport,$attributes)?>
                              <div class="form-group">
                                <label class="col-sm-3 control-label">Foto</label>
                                <div class="col-sm-9">
                                  <input type="file" name="userfile" id="inputFile" class="form-control">
                                </div>
                                <div class="col-sm-offset-3 col-sm-9">
                                  <?php if (!empty($tbBM->foto)) { ?>
                                    <span class="label label-warning">Kosongkan , apabila tidak ingin mengubah</span><br />
                                    <span class="label label-danger">Maksimal Ukuran 2MB (jpg|png|gif)</span>  
                                  <?php }else{ ?>
                                    <span class="label label-danger">Wajib diisi</span><br />
                                    <span class="label label-danger">Maksimal Ukuran 2MB (jpg|png|gif)</span> 
                                  <?php } ?>
                                </div>
                              </div>
                              <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-9">
                                  <button type="submit" class="btn btn-primary">SIMPAN</button>
                                </div>
                              </div>
                            <?=form_close();?>
                        </div>
                    </div>
                </section>
            </div>