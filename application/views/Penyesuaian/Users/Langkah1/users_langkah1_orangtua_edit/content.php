<div class="content-wrapper">
                <section class="content-header">
                    <h1>
                        Penyesuaian
                        <small><i>Detail</i></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-gears"></i> Penyesuaian</a></li>
                        <li><a href="#"></i> Detail</a></li>
                        <li class="active"><i>Edit</i></li>
                    </ol>
                </section>
                <section class="content">
                    <?php if (!empty($this->session->flashdata('type_message'))) { ?>
                        <div class="alert alert-<?=$this->session->flashdata('type_message')?> alert-dismissible text-center" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <?=$this->session->flashdata('message');?>
                        </div>
                    <?php } ?>
                    <div class="box">
                      <div class="box-header with-border">
                            <h3 class="box-title">Langkah-langkah</h3>
                      </div>
                      <center>
                        <div class="box-body">
                          <div class="list-group">
                            <a href="<?=base_url('Penyesuaian/Users/Edit_langkah1_mahasiswa/'.$tbBM->nik_passport)?>" class="list-group-item" style="float: left">Mahasiswa</a>
                            <a href="<?=base_url('Penyesuaian/Users/Edit_langkah1_alamat/'.$tbBM->nik_passport)?>" class="list-group-item" style="float: left">Alamat Mahasiswa</a>
                            <a href="#" class="list-group-item active" style="float: left">Orangtua</a>
                            <a href="<?=base_url('Penyesuaian/Users/Edit_langkah1_foto/'.$tbBM->nik_passport)?>" class="list-group-item" style="float: left">Foto Mahasiswa</a>
                            <a href="<?=base_url('Penyesuaian/Users/Edit_langkah2_tambahan/'.$tbBM->nik_passport)?>" class="list-group-item" style="float: left">Tambahan</a>
                          </div>
                        </div>
                      </center>
                    </div>
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Tabel Penyesuaian Detail</h3>
                        </div>
                        <div class="box-body">
                            <?php $attributes = array('class' => 'form-horizontal', 'role' => 'form'); ?>
                            <?=form_open_multipart('Penyesuaian/Users/Simpan_tahap4/'.$tbBM->nik_passport,$attributes)?>
                              <div id="ayah" class="display:block">
                              <div class="form-group">
                                <label class="col-sm-3 text-right"><h3><strong>Ayah</strong></h3></label>
                                <div class="col-sm-9">&nbsp;</div>
                              </div>
                              <div class="form-group">
                                <label class="col-sm-3 control-label">NIK Ayah</label>
                                <div class="col-sm-9">
                                  <input type="text" class="form-control" name="nik_ayah" value="<?=$tbBM->nik_ayah;?>">
                                </div>
                                <div class="col-sm-offset-3 col-sm-9">
                                  <span class="label label-danger">Wajib diisi</span>
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="col-sm-3 control-label">Nama Ayah</label>
                                <div class="col-sm-9">
                                  <input type="text" class="form-control" name="nama_ayah" value="<?=$tbBM->nama_ayah;?>">
                                </div>
                                <div class="col-sm-offset-3 col-sm-9">
                                  <span class="label label-danger">Wajib diisi</span>
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="col-sm-3 control-label">Tanggal Lahir Ayah</label>
                                <div class="col-sm-9">
                                  <input type="text" class="form-control datepicker" name="tgl_lhr_ayah" value="<?=$tbBM->tgl_lhr_ayah;?>">
                                </div>
                                <div class="col-sm-offset-3 col-sm-9">
                                  <span class="label label-danger">Wajib diisi</span>
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="col-sm-3 control-label">Pendidikan Ayah</label>
                                <div class="col-sm-9">
                                  <select class="form-control" name="id_pendidikan_ayah">
                                  <?php foreach ($tbPendidikan as $key => $value): ?>
                                    <?php if ($value->id_pendidikan == $tbBM->id_pendidikan_ayah) { ?>
                                      <option value="<?=$value->id_pendidikan;?>" selected=""><?=$value->pendidikan;?></option>
                                    <?php }else{ ?>
                                      <option value="<?=$value->id_pendidikan;?>"><?=$value->pendidikan;?></option>
                                    <?php } ?>
                                  <?php endforeach ?>
                                  </select>
                                </div>
                                <div class="col-sm-offset-3 col-sm-9">
                                  <span class="label label-danger">Wajib diisi</span>
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="col-sm-3 control-label">Pekerjaan Ayah</label>
                                <div class="col-sm-9">
                                  <select class="form-control" name="id_pekerjaan_ayah">
                                  <?php foreach ($tbPekerjaan as $key => $value): ?>
                                    <?php if ($value->id_pekerjaan == $tbBM->id_pekerjaan_ayah) { ?>
                                      <option value="<?=$value->id_pekerjaan;?>" selected=""><?=$value->pekerjaan;?></option>
                                    <?php }else{ ?>
                                      <option value="<?=$value->id_pekerjaan;?>"><?=$value->pekerjaan;?></option>
                                    <?php } ?>
                                  <?php endforeach ?>
                                  </select>
                                </div>
                                <div class="col-sm-offset-3 col-sm-9">
                                  <span class="label label-danger">Wajib diisi</span>
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="col-sm-3 control-label">Penghasilan Ayah</label>
                                <div class="col-sm-9">
                                  <select class="form-control" name="id_penghasilan_ayah">
                                  <?php foreach ($tbPenghasilan as $key => $value): ?>
                                    <?php if ($value->id_penghasilan == $tbBM->id_penghasilan_ayah) { ?>
                                      <option value="<?=$value->id_penghasilan;?>" selected=""><?=$value->penghasilan;?></option>
                                    <?php }else{ ?>
                                      <option value="<?=$value->id_penghasilan;?>"><?=$value->penghasilan;?></option>
                                    <?php } ?>
                                  <?php endforeach ?>
                                  </select>
                                </div>
                                <div class="col-sm-offset-3 col-sm-9">
                                  <span class="label label-danger">Wajib diisi</span>
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="col-sm-3 control-label">Nominal Penghasilan Ayah</label>
                                <div class="col-sm-9">
                                  <input type="number" class="form-control" name="nominal_penghasilan_ayah" value="<?=$tbExtra->nominal_penghasilan_ayah; ?>" onkeypress="return hanyaAngka(event)" min=0>
                                </div>
                                <div class="col-sm-offset-3 col-sm-9">
                                  <span class="label label-danger">Wajib diisi</span>
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="col-sm-3 control-label">Terbilang Penghasilan Ayah</label>
                                <div class="col-sm-9">
                                  <input type="text" class="form-control" name="terbilang_penghasilan_ayah" value="<?=$tbExtra->terbilang_penghasilan_ayah; ?>">
                                </div>
                                <div class="col-sm-offset-3 col-sm-9">
                                  <span class="label label-danger">Wajib diisi</span>
                                </div>
                              </div>
                              </div>
                              <div id="ibu" class="display:block">
                              <div class="form-group">
                                <label class="col-sm-3 text-right"><h3><strong>Ibu</strong></h3></label>
                                <div class="col-sm-9">&nbsp;</div>
                              </div>
                              <div class="form-group">
                                <label class="col-sm-3 control-label">NIK Ibu</label>
                                <div class="col-sm-9">
                                  <input type="text" class="form-control" name="nik_ibu" value="<?=$tbBM->nik_ibu;?>">
                                </div>
                                <div class="col-sm-offset-3 col-sm-9">
                                  <span class="label label-danger">Wajib diisi</span>
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="col-sm-3 control-label">Nama Ibu</label>
                                <div class="col-sm-9">
                                  <input type="text" class="form-control" name="nama_ibu" value="<?=$tbBM->nama_ibu;?>">
                                </div>
                                <div class="col-sm-offset-3 col-sm-9">
                                  <span class="label label-danger">Wajib diisi</span>
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="col-sm-3 control-label">Tanggal Lahir Ibu</label>
                                <div class="col-sm-9">
                                  <input type="text" class="form-control datepicker" name="tgl_lhr_ibu" value="<?=$tbBM->tgl_lhr_ibu;?>">
                                </div>
                                <div class="col-sm-offset-3 col-sm-9">
                                  <span class="label label-danger">Wajib diisi</span>
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="col-sm-3 control-label">Pendidikan Ibu</label>
                                <div class="col-sm-9">
                                  <select class="form-control" name="id_pendidikan_ibu">
                                  <?php foreach ($tbPendidikan as $key => $value): ?>
                                    <?php if ($value->id_pendidikan == $tbBM->id_pendidikan_ibu) { ?>
                                      <option value="<?=$value->id_pendidikan;?>" selected=""><?=$value->pendidikan;?></option>
                                    <?php }else{ ?>
                                      <option value="<?=$value->id_pendidikan;?>"><?=$value->pendidikan;?></option>
                                    <?php } ?>
                                  <?php endforeach ?>
                                  </select>
                                </div>
                                <div class="col-sm-offset-3 col-sm-9">
                                  <span class="label label-danger">Wajib diisi</span>
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="col-sm-3 control-label">Pekerjaan Ibu</label>
                                <div class="col-sm-9">
                                  <select class="form-control" name="id_pekerjaan_ibu">
                                  <?php foreach ($tbPekerjaan as $key => $value): ?>
                                    <?php if ($value->id_pekerjaan == $tbBM->id_pekerjaan_ibu) { ?>
                                      <option value="<?=$value->id_pekerjaan;?>" selected=""><?=$value->pekerjaan;?></option>
                                    <?php }else{ ?>
                                      <option value="<?=$value->id_pekerjaan;?>"><?=$value->pekerjaan;?></option>
                                    <?php } ?>
                                  <?php endforeach ?>
                                  </select>
                                </div>
                                <div class="col-sm-offset-3 col-sm-9">
                                  <span class="label label-danger">Wajib diisi</span>
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="col-sm-3 control-label">Penghasilan Ibu</label>
                                <div class="col-sm-9">
                                  <select class="form-control" name="id_penghasilan_ibu">
                                  <?php foreach ($tbPenghasilan as $key => $value): ?>
                                    <?php if ($value->id_penghasilan == $tbBM->id_penghasilan_ibu) { ?>
                                      <option value="<?=$value->id_penghasilan;?>" selected=""><?=$value->penghasilan;?></option>
                                    <?php }else{ ?>
                                      <option value="<?=$value->id_penghasilan;?>"><?=$value->penghasilan;?></option>
                                    <?php } ?>
                                  <?php endforeach ?>
                                  </select>
                                </div>
                                <div class="col-sm-offset-3 col-sm-9">
                                  <span class="label label-danger">Wajib diisi</span>
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="col-sm-3 control-label">Nominal Penghasilan Ibu</label>
                                <div class="col-sm-9">
                                  <input type="number" class="form-control" name="nominal_penghasilan_ibu" value="<?=$tbExtra->nominal_penghasilan_ibu; ?>" onkeypress="return hanyaAngka(event)" min=0>
                                </div>
                                <div class="col-sm-offset-3 col-sm-9">
                                  <span class="label label-danger">Wajib diisi</span>
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="col-sm-3 control-label">Terbilang Penghasilan Ibu</label>
                                <div class="col-sm-9">
                                  <input type="text" class="form-control" name="terbilang_penghasilan_ibu" value="<?=$tbExtra->terbilang_penghasilan_ibu; ?>">
                                </div>
                                <div class="col-sm-offset-3 col-sm-9">
                                  <span class="label label-danger">Wajib diisi</span>
                                </div>
                              </div>
                              </div>
                              <div id="wali" class="display:block">
                              <div class="form-group">
                                <label class="col-sm-3 text-right"><h3><strong>Wali</strong></h3></label>
                                <div class="col-sm-9">&nbsp;</div>
                                <div class="col-sm-offset-3 col-sm-9">
                                  <span class="label label-warning">Wali diisi jika ada yang membiayai selain orang tua. Jika tidak ada dapat diabaikan.</span>
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="col-sm-3 control-label">NIK Wali</label>
                                <div class="col-sm-9">
                                  <input type="text" class="form-control" name="nik_wali" value="<?=$tbBM->nik_wali;?>">
                                </div>
                                <div class="col-sm-offset-3 col-sm-9">
                                  <span class="label label-warning">Optional</span>
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="col-sm-3 control-label">Nama Wali</label>
                                <div class="col-sm-9">
                                  <input type="text" class="form-control" name="nama_wali" value="<?=$tbBM->nama_wali;?>">
                                </div>
                                <div class="col-sm-offset-3 col-sm-9">
                                  <span class="label label-warning">Optional</span>
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="col-sm-3 control-label">Tanggal Lahir Wali</label>
                                <div class="col-sm-9">
                                  <input type="text" class="form-control datepicker" name="tgl_lhr_wali" value="<?=$tbBM->tgl_lhr_wali;?>">
                                </div>
                                <div class="col-sm-offset-3 col-sm-9">
                                  <span class="label label-warning">Optional</span>
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="col-sm-3 control-label">Pendidikan Wali</label>
                                <div class="col-sm-9">
                                  <select class="form-control" name="id_pendidikan_wali">
                                  <?php foreach ($tbPendidikan as $key => $value): ?>
                                    <?php if ($value->id_pendidikan == $tbBM->id_pendidikan_wali) { ?>
                                      <option value="<?=$value->id_pendidikan;?>" selected=""><?=$value->pendidikan;?></option>
                                    <?php }else{ ?>
                                      <option value="<?=$value->id_pendidikan;?>"><?=$value->pendidikan;?></option>
                                    <?php } ?>
                                  <?php endforeach ?>
                                  </select>
                                </div>
                                <div class="col-sm-offset-3 col-sm-9">
                                  <span class="label label-warning">Optional</span>
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="col-sm-3 control-label">Pekerjaan Wali</label>
                                <div class="col-sm-9">
                                  <select class="form-control" name="id_pekerjaan_wali">
                                  <?php foreach ($tbPekerjaan as $key => $value): ?>
                                    <?php if ($value->id_pekerjaan == $tbBM->id_pekerjaan_wali) { ?>
                                      <option value="<?=$value->id_pekerjaan;?>" selected=""><?=$value->pekerjaan;?></option>
                                    <?php }else{ ?>
                                      <option value="<?=$value->id_pekerjaan;?>"><?=$value->pekerjaan;?></option>
                                    <?php } ?>
                                  <?php endforeach ?>
                                  </select>
                                </div>
                                <div class="col-sm-offset-3 col-sm-9">
                                  <span class="label label-warning">Optional</span>
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="col-sm-3 control-label">Penghasilan Wali</label>
                                <div class="col-sm-9">
                                  <select class="form-control" name="id_penghasilan_wali">
                                  <?php foreach ($tbPenghasilan as $key => $value): ?>
                                    <?php if ($value->id_penghasilan == $tbBM->id_penghasilan_wali) { ?>
                                      <option value="<?=$value->id_penghasilan;?>" selected=""><?=$value->penghasilan;?></option>
                                    <?php }else{ ?>
                                      <option value="<?=$value->id_penghasilan;?>"><?=$value->penghasilan;?></option>
                                    <?php } ?>
                                  <?php endforeach ?>
                                  </select>
                                </div>
                                <div class="col-sm-offset-3 col-sm-9">
                                  <span class="label label-warning">Optional</span>
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="col-sm-3 control-label">Nominal Penghasilan Wali</label>
                                <div class="col-sm-9">
                                  <input type="number" class="form-control" name="nominal_penghasilan_wali" value="<?=$tbExtra->nominal_penghasilan_wali; ?>" onkeypress="return hanyaAngka(event)" min=0>
                                </div>
                                <div class="col-sm-offset-3 col-sm-9">
                                  <span class="label label-warning">Optional</span>
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="col-sm-3 control-label">Terbilang Penghasilan Wali</label>
                                <div class="col-sm-9">
                                  <input type="text" class="form-control" name="terbilang_penghasilan_wali" value="<?=$tbExtra->terbilang_penghasilan_wali; ?>">
                                </div>
                                <div class="col-sm-offset-3 col-sm-9">
                                  <span class="label label-warning">Optional</span>
                                </div>
                              </div>
                              </div>
                              <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-9">
                                  <button type="submit" class="btn btn-primary">SIMPAN</button>
                                </div>
                              </div>
                            <?=form_close();?>
                        </div>
                    </div>
                </section>
            </div>