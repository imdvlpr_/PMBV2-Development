<div class="content-wrapper">
                <section class="content-header">
                    <h1>
                        Penyesuaian
                        <small><i>Detail</i></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-gears"></i> Penyesuaian</a></li>
                        <li><a href="#"></i> Detail</a></li>
                        <li class="active"><i>Edit</i></li>
                    </ol>
                </section>
                <section class="content">
                    <?php if (!empty($this->session->flashdata('type_message'))) { ?>
                        <div class="alert alert-<?=$this->session->flashdata('type_message')?> alert-dismissible text-center" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <?=$this->session->flashdata('message');?>
                        </div>
                    <?php } ?>
                    <div class="box">
                      <div class="box-header with-border">
                            <h3 class="box-title">Langkah-langkah</h3>
                      </div>
                      <center>
                        <div class="box-body">
                          <div class="list-group">
                            <a href="<?=base_url('Penyesuaian/Users/Edit_langkah1_mahasiswa/'.$tbBM->nik_passport)?>" class="list-group-item" style="float: left">Mahasiswa</a>
                            <a href="#" class="list-group-item active" style="float: left">Alamat Mahasiswa</a>
                            <a href="<?=base_url('Penyesuaian/Users/Edit_langkah1_orangtua/'.$tbBM->nik_passport)?>" class="list-group-item" style="float: left">Orangtua</a>
                            <a href="<?=base_url('Penyesuaian/Users/Edit_langkah1_foto/'.$tbBM->nik_passport)?>" class="list-group-item" style="float: left">Foto Mahasiswa</a>
                            <a href="<?=base_url('Penyesuaian/Users/Edit_langkah2_tambahan/'.$tbBM->nik_passport)?>" class="list-group-item" style="float: left">Tambahan</a>
                          </div>
                        </div>
                      </center>
                    </div>
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Tabel Penyesuaian Detail</h3>
                        </div>
                        <div class="box-body">
                            <?php $attributes = array('class' => 'form-horizontal', 'role' => 'form'); ?>
                            <?=form_open_multipart('Penyesuaian/Users/Simpan_tahap2/'.$tbBM->nik_passport,$attributes)?>
                              <div class="form-group">
                                <label class="col-sm-3 control-label">Alamat</label>
                                <div class="col-sm-9">
                                  <textarea class="form-control" style="resize: none" id="inputAlamat" name="alamat" required=""><?=$tbBM->jalan;?></textarea>
                                </div>
                                <div class="col-sm-offset-3 col-sm-9">
                                  <span class="label label-danger">Wajib diisi</span>
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="col-sm-3 control-label">RT</label>
                                <div class="col-sm-3">
                                  <input class="form-control" name="rt" value="<?=$tbBM->rt?>" required="">
                                </div>
                                <label class="col-sm-3 control-label">RW</label>
                                <div class="col-sm-3">
                                  <input class="form-control" name="rw" value="<?=$tbBM->rt?>" required="">
                                </div>
                                <div class="col-sm-offset-3 col-sm-9">
                                  <span class="label label-danger">Wajib diisi</span>
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="col-sm-3 control-label">Nama Dusun</label>
                                <div class="col-sm-9">
                                  <input type="text" class="form-control" name="nama_dusun" value="<?=$viewBM->nama_dusun;?>">
                                </div>
                                <div class="col-sm-offset-3 col-sm-9">
                                  <span class="label label-warning">Optional</span>
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="col-sm-3 control-label">Provinsi</label>
                                <div class="col-sm-9">
                                  <select id="prov" name="id_provinsi" class="form-control" style="width:100%">
                                    <option value="">Pilih</option>
                                    <?php foreach ($tbProv as $value): ?>
                                      <?php if (strtoupper($value->id_provinsi) == $tbBM->id_provinsi) { ?>
                                        <option value="<?php echo strtoupper($value->id_provinsi); ?>" selected=""><?php echo strtoupper($value->provinsi); ?></option>
                                      <?php }else{ ?>
                                        <option value="<?php echo strtoupper($value->id_provinsi); ?>"><?php echo strtoupper($value->provinsi); ?></option>
                                      <?php } ?>
                                    <?php endforeach ?>
                                  </select>
                                </div>
                                <div class="col-sm-offset-3 col-sm-9">
                                  <span class="label label-danger">Wajib diisi</span>
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="col-sm-3 control-label">Kabupaten</label>
                                <div class="col-sm-9">
                                  <select id="kab" name="id_kabupaten" class="form-control" style="width:100%">
                                    <option value="">Pilih</option>
                                  </select>
                                  <div id="loading1" style="margin-top: 15px;">
                                    <img src="<?php echo base_url()?>/design-backend/images/loading.gif" width="18"> <small>Loading...</small>
                                  </div>
                                </div>
                                <div class="col-sm-offset-3 col-sm-9">
                                  <span class="label label-danger">Wajib diisi</span>
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="col-sm-3 control-label">Kecamatan</label>
                                <div class="col-sm-9">
                                  <select id="kec" name="id_kecamatan" class="form-control" style="width:100%" required="">
                                    <option value="">Pilih</option>
                                  </select>
                                  <div id="loading2" style="margin-top: 15px;">
                                    <img src="<?php echo base_url()?>/design-backend/images/loading.gif" width="18"> <small>Loading...</small>
                                  </div>
                                </div>
                                <div class="col-sm-offset-3 col-sm-9">
                                  <span class="label label-danger">Wajib diisi</span>
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="col-sm-3 control-label">Kelurahan</label>
                                <div class="col-sm-9">
                                  <select id="kel" name="id_kelurahan" class="form-control" style="width:100%" required="">
                                    <option value="">Pilih</option>
                                  </select>
                                  <div id="loading3" style="margin-top: 15px;">
                                    <img src="<?php echo base_url()?>/design-backend/images/loading.gif" width="18"> <small>Loading...</small>
                                  </div>
                                </div>
                                <div class="col-sm-offset-3 col-sm-9">
                                  <span class="label label-danger">Wajib diisi</span>
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="col-sm-3 control-label">Kode POS</label>
                                <div class="col-sm-9">
                                  <input type="text" class="form-control" name="kode_pos" value="<?=$tbBM->kode_pos;?>">
                                </div>
                                <div class="col-sm-offset-3 col-sm-9">
                                  <span class="label label-danger">Wajib diisi</span>
                                </div>
                              </div>
                              <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-9">
                                  <button type="submit" class="btn btn-primary">SIMPAN</button>
                                </div>
                                  <div class="col-sm-offset-3 col-sm-9">
                                      <span class="label label-danger">Isi kembali keseluruhan data jika ingin mengubah salah satu data.</span>
                                  </div>
                              </div>
                            <?=form_close();?>
                        </div>
                    </div>
                </section>
            </div>