<script>
          $(document).ready(function(){
            $("#loading1").hide();
            $("#loading2").hide();
            $("#loading3").hide();
            
            $("#prov").change(function(){
              $("#kab").hide();
              $("#loading1").show();
            
              $.ajax({
                type: "POST",
                url: "<?php echo base_url("index.php/Penyesuaian/Users/listKabupaten"); ?>",
                data: {id_provinsi : $("#prov").val()},
                dataType: "json",
                beforeSend: function(e) {
                  if(e && e.overrideMimeType) {
                    e.overrideMimeType("application/json;charset=UTF-8");
                  }
                },
                success: function(response){
                  $("#loading1").hide();
                  $("#kab").html(response.list_kabupaten).show();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                  alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError);
                }
              });
            });

            $("#kab").change(function(){
              $("#kec").hide();
              $("#loading2").show();
            
              $.ajax({
                type: "POST",
                url: "<?php echo base_url("index.php/Penyesuaian/Users/listKecamatan"); ?>",
                data: {id_kabupaten : $("#kab").val()},
                dataType: "json",
                beforeSend: function(e) {
                  if(e && e.overrideMimeType) {
                    e.overrideMimeType("application/json;charset=UTF-8");
                  }
                },
                success: function(response){
                  $("#loading2").hide();
                  $("#kec").html(response.list_kecamatan).show();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                  alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError);
                }
              });
            });

            $("#kec").change(function(){
              $("#kel").hide();
              $("#loading3").show();
            
              $.ajax({
                type: "POST",
                url: "<?php echo base_url("index.php/Penyesuaian/Users/listKelurahan"); ?>",
                data: {id_kecamatan : $("#kec").val()},
                dataType: "json",
                beforeSend: function(e) {
                  if(e && e.overrideMimeType) {
                    e.overrideMimeType("application/json;charset=UTF-8");
                  }
                },
                success: function(response){
                  $("#loading3").hide();
                  $("#kel").html(response.list_kelurahan).show();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                  alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError);
                }
              });
            });

          });
        </script>

        <script type="text/javascript">
          $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            orientation: "bottom auto",
            autoclose: true
          });
        </script>