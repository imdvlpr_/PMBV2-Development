<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Penyesuaian Mahasiswa
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-folder-open"></i> Penyesuaian</a></li>
            <li>Mahasiswa</li>
        </ol>
    </section>

    <section class="content">
        <?php if (!empty($this->session->flashdata('type_message'))) { ?>
            <div class="alert alert-<?=$this->session->flashdata('type_message')?> alert-dismissible text-center" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?=$this->session->flashdata('message');?>
            </div>
        <?php } ?>
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Tabel Mahasiswa</h3>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table id="users" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Action</th>
                                <th>NIK / Passport</th>
                                <th>Nomor Peserta</th>
                                <th>Nama</th>
                                <th>Jalur Masuk</th>
                                <th>Username</th>
                                <th>Verifikasi</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
</div>