<!-- DataTables -->

<script src="<?=base_url('assets/js/jquery.dataTables.min.js')?>"></script>

<script src="<?=base_url('assets/js/dataTables.bootstrap.min.js')?>"></script>
<script type="text/javascript" language="javascript">

			$(document).ready(function(){

				var dataTable = $('#ukt').DataTable({

					"processing":true,

					"serverSide":true,

					"order":[],

					"ajax":{

						url:"<?=base_url('Penyesuaian/UKT/Json')?>",

						type:"POST"

					},

					"columnDefs":[

						{

							"targets":[0, 6, 7],  // sesuaikan order table dengan jumlah column

							"orderable":false,

						},

					],

				});

			});

		</script>