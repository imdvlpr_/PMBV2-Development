<div class="content-wrapper">

				<section class="content-header">

					<h1>

						Penyesuaian UKT

					</h1>

					<ol class="breadcrumb">

						<li><a href="#"><i class="fa fa-folder-open"></i> Penyesuaian</a></li>

						<li>UKT</li>

					</ol>

				</section>



				<section class="content">

					<?php if (!empty($this->session->flashdata('type_message'))) { ?>

						<div class="alert alert-<?=$this->session->flashdata('type_message')?> alert-dismissible text-center" role="alert">

							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

							<?=$this->session->flashdata('message');?>

						</div>

					<?php } ?>

					<div class="box">

						<div class="box-header with-border">

							<h3 class="box-title">Data UKT</h3>

						</div>

						<div class="box-body">

							<div class="table-responsive">

								<table id="ukt" class="table table-bordered table-striped">

									<thead>

										<tr>

											<th>Action</th>

											<th>NIK</th>

											<th>Nama</th>

											<th>Score</th>

											<th>Kategori</th>

											<th>Jumlah</th>

											<th>Status</th>

											<th>Rekomendasi</th>

										</tr>

									</thead>

									<tbody>

									</tbody>

								</table>

							</div>

						</div>

					</div>

				</section>

			</div>