<div class="content-wrapper">
                <section class="content-header">
                    <h1>
                        Penyesuaian
                        <small><i>UKT</i></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-gears"></i> Penyesuaian</a></li>
                        <li><a href="#"></i> UKT</a></li>
                        <li class="active"><i>Edit</i></li>
                    </ol>
                </section>
                <section class="content">
                    <?php if (!empty($this->session->flashdata('type_message'))) { ?>
                        <div class="alert alert-<?=$this->session->flashdata('type_message')?> alert-dismissible text-center" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <?=$this->session->flashdata('message');?>
                        </div>
                    <?php } ?>
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit UKT</h3>
                        </div>
                        <?=form_open('Penyesuaian/UKT/Update/'.$tbUkt->nik_passport)?>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">NIK :</label>
                                <input type="text" name="nik" class="form-control" placeholder="NIK" required="" value="<?=$tbUkt->nik_passport;?>" readonly="">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Nama :</label>
                                <input type="text" name="nama" class="form-control" placeholder="Nama" required="" value="<?=$tbUkt->nama;?>" readonly="">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Kategori Sebelumnya :</label>
                                <input type="text" name="kategori_sebelumnya" class="form-control" placeholder="Kategori Sebelumnya" required="" value="<?=$tbUkt->kategori;?>" readonly="">
                            </div>
                            <div class="form-group">
                                <input type="hidden" name="kode_jurusan" value="<?=$tbUkt->kode_jurusan;?>">
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="recipient-name" class="control-label">Kategori :</label>
                                    <select name="kategori" class="form-control">
                                        <option value="K1">K1</option>
                                        <option value="K2">K2</option>
                                        <option value="K3">K3</option>
                                        <option value="K4">K4</option>
                                        <option value="K5">K5</option>
                                        <option value="K6">K6</option>
                                        <option value="K7">K7</option>
                                        <option value="K8">K8</option>
                                        <option value="K9">K9</option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label for="recipient-name" class="control-label">Rekomendasi :</label>
                                    <select name="rekomendasi" class="form-control">
                                        <option value="1">IYA</option>
                                        <option value="0">TIDAK</option>
                                    </select>
                                </div>
                            </div>
                            <br>
                            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                        <?=form_close()?>
                    </div>
                </section>
            </div>