<script src="<?=base_url('assets/js/jquery.dataTables.min.js');?>"></script>
<script src="<?=base_url('assets/js//dataTables.bootstrap.min.js');?>"></script>
<script>
    $(document).ready(function(){
        $('#tambah_tooltip').tooltip();
        var dataTable = $('#dataTable').DataTable({
            "processing":true,
            "serverSide":true,
            "order":[],
            "ajax":{
                url:"<?=base_url('Pradaftar/TmpUsers/Json')?>",
                type:"POST"
            },
            "columnDefs":[
                {
                    "targets":[0, 7, 7],  // sesuaikan order table dengan jumlah column
                    "orderable":true,
                },
            ],
        });
    });
</script>