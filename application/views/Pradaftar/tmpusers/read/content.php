<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Temporary Pradaftar Users
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-folder-open"></i> Temporary Pradaftar</a></li>
            <li>Users</li>
        </ol>
    </section>

    <section class="content">

        <!-- notification -->
        <?php $this->load->view('notification') ?>

        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Tabel Temporary Pradaftar Users</h3>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table id="dataTable" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Action</th>
                            <th>Kode Pembayaran</th>
                            <th>NIK</th>
                            <th>Nama</th>
                            <th>Kategori</th>
                            <th>Tanggal Daftar</th>
                            <th>Date Created</th>
                            <th>Date Updated</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>Action</th>
                            <th>Kode Pembayaran</th>
                            <th>NIK</th>
                            <th>Nama</th>
                            <th>Kategori</th>
                            <th>Tanggal Daftar</th>
                            <th>Date Created</th>
                            <th>Date Updated</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>

    </section>
</div>