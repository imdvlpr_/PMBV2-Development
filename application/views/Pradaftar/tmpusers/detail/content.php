<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Temporary Users
            <small><i>Detail</i></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-folder-open"></i> Temporary Users</a></li>
            <li><a href="#"></i> Detail</a></li>
        </ol>
    </section>
    <section class="content">

        <!-- notification -->
        <?php $this->load->view('notification') ?>

        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Tabel Temporary Users Detail</h3>
                <div class="pull-right">
                    <div class="btn-group">
                        <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-gears">&nbsp;</i> <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li><a href="<?=base_url('Pradaftar/TmpUsers/Rollback/'.$tmpPUsers->id_tmp_pradaftar_users)?>">Gunakan Kembali</a></li>
                            <li role="separator" class="divider"></li>
                        </ul>
                    </div>
                </div>
                <div class="pull-right">

                </div>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <tr>
                            <th>Kode Pembayaran</th>
                            <td><?=$tmpPUsers->kd_pembayaran?></td>
                        </tr>
                        <tr>
                            <th>NIK</th>
                            <td><?=$tmpPUsers->nik_passport?></td>
                        </tr>
                        <tr>
                            <th>Nomor Peserta</th>
                            <td><?=$tmpPUsers->nomor_peserta?></td>
                        </tr>
                        <tr>
                            <th>Nama</th>
                            <td><?=$tmpPUsers->nama?></td>
                        </tr>
                        <tr>
                            <th>Tanggal Lahir</th>
                            <td><?=$tmpPUsers->tgl_lhr?></td>
                        </tr>
                        <tr>
                            <th>Kategori</th>
                            <td><?=($tmpPUsers->kategori == 1) ? 'IPA / SAINS' : 'IPS / SOSIAL DAN HUMANIORA'?></td>
                        </tr>
                        <tr>
                            <th>Tipe Ujian</th>
                            <td><?=$tmpPUsers->id_tipe_ujian?></td>
                        </tr>
                        <tr>
                            <th>Nomor Telepon</th>
                            <td><?=$tmpPUsers->nmr_tlp?></td>
                        </tr>
                        <tr>
                            <th>Email</th>
                            <td><?=$tmpPUsers->email?></td>
                        </tr>
                        <tr>
                            <th>IP Login</th>
                            <td><?=$tmpPUsers->ip_login?></td>
                        </tr>
                        <tr>
                            <th>Login</th>
                            <td><?=$tmpPUsers->login?></td>
                        </tr>
                        <tr>
                            <th>IP Logout</th>
                            <td><?=$tmpPUsers->ip_logout?></td>
                        </tr>
                        <tr>
                            <th>Logout</th>
                            <td><?=$tmpPUsers->logout?></td>
                        </tr>
                        <tr>
                            <th>Pembayaran</th>
                            <td><?=($tmpPUsers->pembayaran == 1) ? "<span class=\"label label-success\">Sudah Bayar</span>" : "<span class=\"label label-danger\">Belum Bayar</span>"?></td>
                        </tr>
                        <tr>
                            <th>Biodata</th>
                            <td><?=($tmpPUsers->biodata == 1) ? "<span class=\"label label-success\">Sudah Verifikasi</span>" : "<span class=\"label label-danger\">Belum Verifikasi</span>"?></td>
                        </tr>
                        <tr>
                            <th>Tanggal Daftar</th>
                            <td><?=$tmpPUsers->tgl_daftar?></td>
                        </tr>
                        <tr>
                            <th>Date Create</th>
                            <td><?=$tmpPUsers->date_created?></td>
                        </tr>
                        <tr>
                            <th>Date Update</th>
                            <td><?=$tmpPUsers->date_updated?></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </section>
</div>