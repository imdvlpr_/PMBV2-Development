<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Import
            <small>Pembayaran</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-upload"></i> Import</a></li>
            <li class="active">Pembayaran</li>
        </ol>
    </section>
    <section class="content">

        <!-- notification -->
        <?php $this->load->view('notification') ?>

        <div class="row">
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Import Pembayaran</h3>
                    </div>
                    <?=form_open_multipart('Pradaftar/Import/Pembayaran/Import')?>
                    <div class="box-body">
                        <div class="form-group">
                            <label class="control-label">Upload file :</label>
                            <input type="file" name="userfile" required>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary btn-flat pull-right"><i class="fa fa-upload">&nbsp;</i>Import</button>
                    </div>
                    <?=form_close()?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Tatacara Import</h3>
                    </div>
                    <div class="box-body">
                        <p>Tatacara import data pembayaran : (Only Windows)</p>
                        <ol>
                            <li>Download template jadwal yang sudah disediakan.</li>
                            <li>Isi kolom pada tabel sesuai nama kolomnya.</li>
                            <li>Bank : BTN, BRISyariah dll</li>
                            <li>Pilih file yang ingin di upload.</li>
                            <li>Klik import.</li>
                        </ol>
                    </div>
                    <div class="box-footer">
                        <a href="<?=base_url('file/template_pembayaran_pradaftar.xlsx')?>" class="btn btn-default btn-flat" target="_blank">Download Template</a>
                    </div>
                </div>
            </div>
        </div>

    </section>
</div>