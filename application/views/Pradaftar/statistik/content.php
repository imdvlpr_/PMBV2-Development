<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Statistik
            <small><i>General</i></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-bar-chart"></i> Statistik</a></li>
            <li>General</li>
        </ol>
    </section>
    <section class="content">

        <!-- notification -->
        <?php $this->load->view('notification') ?>

        <!-- Tahun -->
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Generate Statistik</h3>
                </div>
                <div class="box-body">
                    <?=form_open('Pradaftar/Statistik/')?>
                    <div class="form-group">
                        <label class="control-label">Tahun :</label>
                        <select name="tahun" class="form-control" required="">
                            <?php foreach ($tahun as $value): ?>
                                <option value="<?=$value->tahun?>"><?=$value->tahun?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-spinner">&nbsp;</i>Generate</button>
                </div>
                <?=form_close()?>
            </div>
            </div>
            <div class="col-md-3"></div>
        </div>

        <?php if ($kosong == FALSE) : ?>
            <!-- Statistik Users -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Statistik Users</h3>
                </div>
                <div class="box-body bg-gray-light">
                    <div class="row">
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="info-box">
                                <span class="info-box-icon bg-blue-active"><i class="fa fa-users"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text">Users</span>
                                    <span class="info-box-number"><?=$num_users?></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                        <!-- /.col -->

                        <!-- fix for small devices only -->
                        <div class="clearfix visible-sm-block"></div>

                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="info-box">
                                <span class="info-box-icon bg-green-active"><i class="fa fa-check"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Users Sudah Verifikasi</span>
                                    <span class="info-box-number"><?=$num_users_sv?></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                        <!-- /.col -->

                        <!-- fix for small devices only -->
                        <div class="clearfix visible-sm-block"></div>

                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="info-box">
                                <span class="info-box-icon bg-red-active"><i class="fa fa-close"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Users Belum Verifikasi</span>
                                    <span class="info-box-number"><?=$num_users_bv?></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                        <!-- /.col -->

                        <!-- fix for small devices only -->
                        <div class="clearfix visible-sm-block"></div>

                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="info-box">
                                <span class="info-box-icon bg-blue-gradient"><i class="fa fa-flask"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Kategori IPA</span>
                                    <span class="info-box-number"><?=$num_kategori_ipa?></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                        <!-- /.col -->

                        <!-- fix for small devices only -->
                        <div class="clearfix visible-sm-block"></div>

                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="info-box">
                                <span class="info-box-icon bg-yellow-gradient"><i class="fa fa-globe"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Kategori IPS</span>
                                    <span class="info-box-number"><?=$num_kategori_ips?></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                        <!-- /.col -->

                        <!-- fix for small devices only -->
                        <div class="clearfix visible-sm-block"></div>

                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="info-box">
                                <span class="info-box-icon bg-aqua-gradient"><i class="fa fa-mars"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Laki-laki</span>
                                    <span class="info-box-number"><?=$num_man?></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                        <!-- /.col -->

                        <!-- fix for small devices only -->
                        <div class="clearfix visible-sm-block"></div>

                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="info-box">
                                <span class="info-box-icon bg-maroon-gradient"><i class="fa fa-venus"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Perempuan</span>
                                    <span class="info-box-number"><?=$num_women?></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                        <!-- /.col -->

                        <!-- fix for small devices only -->
                        <div class="clearfix visible-sm-block"></div>

                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="info-box">
                                <span class="info-box-icon bg-green-gradient"><i class="fa fa-university"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Lulus</span>
                                    <span class="info-box-number"><?=$num_lulus?></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                        <!-- /.col -->

                        <!-- fix for small devices only -->
                        <div class="clearfix visible-sm-block"></div>

                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="info-box">
                                <span class="info-box-icon bg-red-gradient"><i class="fa fa-university"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Tidak Lulus</span>
                                    <span class="info-box-number"><?=$num_tidak_lulus?></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                        <!-- /.col -->

                        <!-- fix for small devices only -->
                        <div class="clearfix visible-sm-block"></div>


                    </div>
                </div>
            </div>

            <!-- Statistik Pembayaran -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Statistik Pembayaran</h3>
                </div>
                <div class="box-body bg-gray-light">
                    <div class="row">
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="info-box">
                                <span class="info-box-icon bg-yellow-active"><i class="fa fa-money"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text">Pembayaran</span>
                                    <span class="info-box-number">
                                    <?=$num_pembayaran?><br />
                                    <?='Rp '.number_format($num_pembayaran_ua,2)?>
                                </span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                        <!-- /.col -->

                        <!-- fix for small devices only -->
                        <div class="clearfix visible-sm-block"></div>

                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="info-box">
                                <span class="info-box-icon bg-green-active"><i class="fa fa-check"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Pembayaran Sudah Verifikasi</span>
                                    <span class="info-box-number"><?=$num_pembayaran_sv?></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                        <!-- /.col -->

                        <!-- fix for small devices only -->
                        <div class="clearfix visible-sm-block"></div>

                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="info-box">
                                <span class="info-box-icon bg-red-active"><i class="fa fa-close"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Pembayaran Belum Verifikasi</span>
                                    <span class="info-box-number"><?=$num_pembayaran_bv?></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                        <!-- /.col -->

                        <!-- fix for small devices only -->
                        <div class="clearfix visible-sm-block"></div>
                    </div>
                </div>
            </div>

            <!-- Statistik Temporary Users-->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Statistik Temporary Users</h3>
                </div>
                <div class="box-body bg-gray-light">
                    <div class="row">
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="info-box">
                                <span class="info-box-icon bg-purple-active"><i class="fa fa-users"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text">Temporary Users</span>
                                    <span class="info-box-number">
                                    <?=$num_tmp_users?><br />
                                </span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                        <!-- /.col -->

                        <!-- fix for small devices only -->
                        <div class="clearfix visible-sm-block"></div>

                    </div>
                </div>
            </div>

            <!-- Statistik Pemilihan Jurusan-->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Statistik Pemilihan Jurusan</h3>
                </div>
                <div class="box-body">
                    <div class="table-responsive">
                        <table id="dataTable1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode Jurusan</th>
                                <th>Jurusan</th>
                                <th>Fakultas</th>
                                <th>Pilihan 1</th>
                                <th>Pilihan 2</th>
                                <th>Pilihan 3</th>
                                <th>Jumlah</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $no = 1; foreach ($viewSJurusan as $value) : ?>
                                <tr>
                                    <td><?=$no++?></td>
                                    <td><?=$value->kode_jurusan?></td>
                                    <td><?=$value->jurusan?></td>
                                    <td><?=$value->fakultas?></td>
                                    <td><?=$pilihan1[$value->kode_jurusan]?></td>
                                    <td><?=$pilihan2[$value->kode_jurusan]?></td>
                                    <td><?=$pilihan3[$value->kode_jurusan]?></td>
                                    <td><?=$jumlah[$value->kode_jurusan]?></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>No</th>
                                <th>Kode Jurusan</th>
                                <th>Jurusan</th>
                                <th>Fakultas</th>
                                <th>Pilihan 1</th>
                                <th>Pilihan 2</th>
                                <th>Pilihan 3</th>
                                <th>Jumlah</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>

            <div class="row">
                <!-- Statistik Kelulusan Jurusan-->
                <div class="col-md-7">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Statistik Kelulusan Jurusan</h3>
                        </div>
                        <div class="box-body">
                            <div class="table-responsive">
                                <table id="dataTable2" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Kode Jurusan</th>
                                        <th>Jurusan</th>
                                        <th>Fakultas</th>
                                        <th>Jumlah</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $no = 1; foreach ($viewSJurusan as $value) : ?>
                                        <tr>
                                            <td><?=$no++?></td>
                                            <td><?=$value->kode_jurusan?></td>
                                            <td><?=$value->jurusan?></td>
                                            <td><?=$value->fakultas?></td>
                                            <td><?=$kelulusan[$value->kode_jurusan]?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>No</th>
                                        <th>Kode Jurusan</th>
                                        <th>Jurusan</th>
                                        <th>Fakultas</th>
                                        <th>Jumlah</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Statistik Provinsi -->
                <div class="col-md-5">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Statistik Provinsi</h3>
                        </div>
                        <div class="box-body">
                            <div class="table-responsive">
                                <table id="dataTable3" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Provinsi</th>
                                        <th>Jumlah</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $no = 1; foreach ($tblSProvinsi as $value) : ?>
                                        <tr>
                                            <td><?=$no++?></td>
                                            <td><?=$value->provinsi?></td>
                                            <td><?=$provinsi[$value->id_provinsi]?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>No</th>
                                        <th>Provinsi</th>
                                        <th>Jumlah</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Statistik Tipe Ujian-->
                <div class="col-md-6">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Statistik Tipe Ujian</h3>
                        </div>
                        <div class="box-body">
                            <div class="table-responsive">
                                <table id="dataTable4" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Tipe Ujian</th>
                                        <th>Jumlah</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $no = 1; foreach ($tblSTipeUjian as $value) : ?>
                                        <tr>
                                            <td><?=$no++?></td>
                                            <td><?=$value->tipe_ujian?></td>
                                            <td><?=$tipe_ujian[$value->id_tipe_ujian]?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>No</th>
                                        <th>Tipe Ujian</th>
                                        <th>Jumlah</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Statistik Rumpun-->
                <div class="col-md-6">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Statistik Rumpun</h3>
                        </div>
                        <div class="box-body">
                            <div class="table-responsive">
                                <table id="dataTable5" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Rumpun</th>
                                        <th>Jumlah</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $no = 1; foreach ($tblSRumpun as $value) : ?>
                                        <tr>
                                            <td><?=$no++?></td>
                                            <td><?=$value->rumpun?></td>
                                            <td><?=$rumpun[$value->id_rumpun]?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>No</th>
                                        <th>Rumpun</th>
                                        <th>Jumlah</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Statistik Asal Sekolah-->
                <div class="col-md-6">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Statistik Asal Sekolah</h3>
                        </div>
                        <div class="box-body">
                            <div class="table-responsive">
                                <table id="dataTable5" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Asal Sekolah</th>
                                        <th>Jumlah</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $no = 1; foreach ($tblSAsalSekolah as $value) : ?>
                                        <tr>
                                            <td><?=$no++?></td>
                                            <td><?=$value->asal_sekolah?></td>
                                            <td><?=$asal_sekolah[$value->id_asal_sekolah]?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>No</th>
                                        <th>Asal Sekolah</th>
                                        <th>Jumlah</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </section>
</div>