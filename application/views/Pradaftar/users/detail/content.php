<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Users
            <small><i>Detail</i></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-folder-open"></i> Users</a></li>
            <li class="active"><i>Detail</i></li>
        </ol>
    </section>
    <section class="content">

        <!-- notification -->
        <?php $this->load->view('notification') ?>

        <div class="row">
            <div class="col-md-6">
                <!-- Tabel Users -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Tabel Users</h3>
                        <div class="pull-right">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fa fa-gears">&nbsp;</i> <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href="#" data-toggle="modal" data-target="#editUsers">Edit</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="<?=base_url('Pradaftar/Users/Delete/'.$tblPUsers->id_pradaftar_users);?>" onclick="return confirm('Apakah Anda Yakin ?')">Delete</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tr>
                                    <th>Kode Pembayaran</th>
                                    <td><?=$tblPUsers->kd_pembayaran?></td>
                                </tr>
                                <tr>
                                    <th>Nomor Peserta</th>
                                    <td><?=$tblPUsers->nomor_peserta?></td>
                                </tr>
                                <tr>
                                    <th>NIK</th>
                                    <td><?=$tblPUsers->nik_passport?></td>
                                </tr>
                                <tr>
                                    <th>Nama</th>
                                    <td><?=$tblPUsers->nama?></td>
                                </tr>
                                <tr>
                                    <th>Tanggal Lahir</th>
                                    <td><?=$tblPUsers->tgl_lhr?></td>
                                </tr>
                                <tr>
                                    <th>Kategori</th>
                                    <td><?=($tblPUsers->kategori == 1) ? 'IPA / SAINS' : 'IPS / SOSIAL DAN HUMANIORA'?></td>
                                </tr>
                                <tr>
                                    <th>Tipe Ujian</th>
                                    <td><?=$tblPUsers->tipe_ujian?></td>
                                </tr>
                                <tr>
                                    <th>Nomor Telepon</th>
                                    <td><?=$tblPUsers->nmr_tlp?></td>
                                </tr>
                                <tr>
                                    <th>Email</th>
                                    <td><?=$tblPUsers->email?></td>
                                </tr>
                                <tr>
                                    <th>IP Login</th>
                                    <td><?=$tblPUsers->ip_login?></td>
                                </tr>
                                <tr>
                                    <th>Login</th>
                                    <td><?=$tblPUsers->login?></td>
                                </tr>
                                <tr>
                                    <th>IP Logout</th>
                                    <td><?=$tblPUsers->ip_logout?></td>
                                </tr>
                                <tr>
                                    <th>Logout</th>
                                    <td><?=$tblPUsers->logout?></td>
                                </tr>
                                <tr>
                                    <th>Pembayaran</th>
                                    <td><?=($tblPUsers->pembayaran == 1) ? "<span class=\"label label-success\">Sudah Bayar</span>" : "<span class=\"label label-danger\">Belum Bayar</span>"?></td>
                                </tr>
                                <tr>
                                    <th>Biodata</th>
                                    <td><?=($tblPUsers->biodata == 1) ? "<span class=\"label label-success\">Sudah Verifikasi</span>" : "<span class=\"label label-danger\">Belum Verifikasi</span>"?></td>
                                </tr>
                                <tr>
                                    <th>Date Create</th>
                                    <td><?=$tblPUsers->date_created?></td>
                                </tr>
                                <tr>
                                    <th>Date Update</th>
                                    <td><?=$tblPUsers->date_updated?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <?php if ($tblPUsers->pembayaran == "1") : ?>
                    <?php if ($tblPPembayaran->status == "1") : ?>
                        <!-- Tabel Biodata-->
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">Tabel Biodata</h3>
                            </div>
                            <div class="box-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <tr>
                                            <th>Nama</th>
                                            <td><?=$tblPBiodata->nama?></td>
                                        </tr>
                                        <tr>
                                            <th>Tempat Lahir</th>
                                            <td><?=$tblPBiodata->tempat?></td>
                                        </tr>
                                        <tr>
                                            <th>Tanggal Lahir</th>
                                            <td><?=$tblPUsers->tgl_lhr?></td>
                                        </tr>
                                        <tr>
                                            <th>Jenis Kelamin</th>
                                            <td><?=($tblPBiodata->jenis_kelamin == 'L') ? 'Laki-laki' : 'Perempuan'?></td>
                                        </tr>
                                        <tr>
                                            <th>Warga Negara</th>
                                            <td><?=$tblPBiodata->warga_negara?></td>
                                        </tr>
                                        <tr>
                                            <th>Agama</th>
                                            <td><?=$tblPBiodata->agama?></td>
                                        </tr>
                                        <tr>
                                            <th>Provinsi</th>
                                            <td><?=$tblPBiodata->provinsi?></td>
                                        </tr>
                                        <tr>
                                            <th>Kota/Kabupaten</th>
                                            <td><?=$tblPBiodata->kabupaten?></td>
                                        </tr>
                                        <tr>
                                            <th>Kecamatan</th>
                                            <td><?=$tblPBiodata->kecamatan?></td>
                                        </tr>
                                        <tr>
                                            <th>Kelurahan</th>
                                            <td><?=$tblPBiodata->kelurahan?></td>
                                        </tr>
                                        <tr>
                                            <th>Kode Pos</th>
                                            <td><?=$tblPBiodata->kodepos?></td>
                                        </tr>
                                        <tr>
                                            <th>Alamat</th>
                                            <td><?=$tblPBiodata->alamat?></td>
                                        </tr>
                                        <tr>
                                            <th>Jenis Asal Sekolah</th>
                                            <td><?=$tblPBiodata->jenis_asal_sekolah?></td>
                                        </tr>
                                        <tr>
                                            <th>Asal Sekolah</th>
                                            <td><?=$tblPBiodata->asal_sekolah?></td>
                                        </tr>
                                        <tr>
                                            <th>Rumpun</th>
                                            <td><?=$tblPBiodata->rumpun?></td>
                                        </tr>
                                        <tr>
                                            <th>Foto</th>
                                            <td><a href="#" data-toggle="modal" data-target="#viewFoto" class="btn btn-sm btn-success">Foto</a></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <!-- Tabel Orang Tua-->
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">Tabel Orang Tua</h3>
                            </div>
                            <div class="box-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <?php foreach ($tblPOrangTua as $a) : ?>
                                            <tr>
                                                <th class="text-center" colspan="2"><?=$a->orangtua?></th>
                                            </tr>
                                            <tr>
                                                <th>NIK</th>
                                                <td><?=$a->nik_orangtua?></td>
                                            </tr>
                                            <tr>
                                                <th>Nama</th>
                                                <td><?=$a->nama_orangtua?></td>
                                            </tr>
                                            <tr>
                                                <th>Tanggal Lahir</th>
                                                <td><?=$a->tgl_lhr_orangtua?></td>
                                            </tr>
                                            <tr>
                                                <th>Pendidikan</th>
                                                <td><?=$a->pendidikan?></td>
                                            </tr>
                                            <tr>
                                                <th>Pekerjaan</th>
                                                <td><?=$a->pekerjaan?></td>
                                            </tr>
                                            <tr>
                                                <th>Penghasilan</th>
                                                <td><?=$a->penghasilan?></td>
                                            </tr>
                                            <tr>
                                                <th>Date Created</th>
                                                <td><?=$a->date_created?></td>
                                            </tr>
                                            <tr>
                                                <th>Date Updated</th>
                                                <td><?=$a->date_updated?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </table>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endif; ?>
            </div>

            <div class="col-md-6">
                <?php if ($tblPUsers->pembayaran == "1") : ?>
                    <!-- Tabel Pembayaran -->
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Tabel Pembayaran</h3>
                        </div>
                        <div class="box-body">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <tr>
                                        <th>Uang</th>
                                        <td>Rp <?=number_format($tblPPembayaran->uang,2);?></td>
                                    </tr>
                                    <tr>
                                        <th>Bank</th>
                                        <td><?=$tblPPembayaran->bank;?></td>
                                    </tr>
                                    <tr>
                                        <th>Status</th>
                                        <td><?=($tblPPembayaran->status == 1) ? "<span class=\"label label-success\">Sudah Verifikasi</span>" : "<span class=\"label label-danger\">Belum Verifikasi</span>"?></td>
                                    </tr>
                                    <tr>
                                        <th>Date Created</th>
                                        <td><?=$tblPPembayaran->date_created;?></td>
                                    </tr>
                                    <tr>
                                        <th>Date Updated</th>
                                        <td><?=$tblPPembayaran->date_updated;?></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>

                <?php if ($tblPUsers->pembayaran == "1") : ?>
                    <?php if ($tblPPembayaran->status == "1") : ?>
                        <!-- Tabel Pilihan-->
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">Tabel Pilihan</h3>
                            </div>
                            <div class="box-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <?php foreach ($tblPPilihan as $a) : ?>
                                            <tr>
                                                <th colspan="2" class="text-center">Pilihan <?=$a->pilihan?></th>
                                            </tr>
                                            <tr>
                                                <th>Jurusan</th>
                                                <td>
                                                    <?=$a->kode_jurusan.' - '.$a->jurusan?>&nbsp;
                                                    <?php
                                                    if ($a->akreditasi == 1)
                                                        echo '(A)';
                                                    elseif ($a->akreditasi == 2)
                                                        echo '(B)';
                                                    elseif ($a->akreditasi == 3)
                                                        echo '(C)';
                                                    else
                                                        echo '(-)';
                                                    ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Fakultas</th>
                                                <td><?=$a->fakultas?></td>
                                            </tr>
                                            <tr>
                                                <th>Date Created</th>
                                                <td><?=$a->date_created?></td>
                                            </tr>
                                            <tr>
                                                <th>Date Updated</th>
                                                <td><?=$a->date_updated?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <!-- Tabel Jadwal -->
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">Tabel Jadwal</h3>
                            </div>
                            <div class="box-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <tr>
                                            <th>Tanggal</th>
                                            <td><?=$tblPJadwal->tanggal?></td>
                                        </tr>
                                        <tr>
                                            <th>Tempat</th>
                                            <td><?=$tblPJadwal->kampus.' - '.$tblPJadwal->gedung.' - '.$tblPJadwal->ruangan?></td>
                                        </tr>
                                        <tr>
                                            <th>Sesi</th>
                                            <td><?=$tblPJadwal->sesi?></td>
                                        </tr>
                                        <tr>
                                            <th>Waktu</th>
                                            <td><?=$tblPJadwal->jam_awal.' - '.$tblPJadwal->jam_akhir?></td>
                                        </tr>
                                        <tr>
                                            <th>Date Created</th>
                                            <td><?=$tblPJadwal->date_created?></td>
                                        </tr>
                                        <tr>
                                            <th>Date Updated</th>
                                            <td><?=$tblPJadwal->date_updated?></td>
                                        </tr>
                                        <tr>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <!-- Tabel Kelulusan -->
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">Tabel Kelulusan</h3>
                            </div>
                            <div class="box-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <tr>
                                            <th>Jurusan</th>
                                            <td>
                                                <?=$tblPKelulusan->kode_jurusan.' - '.$tblPKelulusan->jurusan?>&nbsp;
                                                <?php
                                                if ($tblPKelulusan->akreditasi == 1)
                                                    echo '(A)';
                                                elseif ($tblPKelulusan->akreditasi == 2)
                                                    echo '(B)';
                                                elseif ($tblPKelulusan->akreditasi == 3)
                                                    echo '(C)';
                                                else
                                                    echo '(-)';
                                                ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Fakultas</th>
                                            <td><?=$tblPKelulusan->fakultas?></td>
                                        </tr>
                                        <tr>
                                            <th>Nilai</th>
                                            <td><?=$tblPKelulusan->nilai?></td>
                                        </tr>
                                        <tr>
                                            <th>Bobot</th>
                                            <td><?=$tblPKelulusan->bobot?></td>
                                        </tr>
                                        <tr>
                                            <th>Status Kelulusan</th>
                                            <td><?=($tblPKelulusan->status == 1) ? "<span class=\"label label-success\">Lulus</span>" : "<span class=\"label label-danger\">Tidak Lulus</span>"?></td>
                                        </tr>
                                        <?php $id = $this->session->userdata('id_users'); if ($id == 1): ?>
                                        <tr>
                                            <th>Keterangan</th>
                                            <td><?=$tblPKelulusan->keterangan?></td>
                                        </tr>
                                        <?php endif; ?>
                                        <tr>
                                            <th>Date Created</th>
                                            <td><?=$tblPKelulusan->date_created?></td>
                                        </tr>
                                        <tr>
                                            <th>Date Updated</th>
                                            <td><?=$tblPKelulusan->date_updated?></td>
                                        </tr>
                                        <tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
        </div>
    </section>
</div>