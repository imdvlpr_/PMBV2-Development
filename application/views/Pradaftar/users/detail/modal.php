<!-- View Foto -->
<div class="modal fade" id="viewFoto">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Foto</h4>
            </div>
            <div class="modal-body">
                <img class="img-responsive center-block" src="https://pmb-pradaftar.uinsgd.ac.id/upload/foto/<?=$tblPBiodata->foto?>" alt="foto profile">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- Users Edit -->
<div class="modal fade" id="editUsers">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Edit Users</h4>
            </div>
            <?=form_open('Pradaftar/Users/Update/'.$tblPUsers->id_pradaftar_users)?>
            <div class="modal-body">
                <div class="form-group has-feedback">
                    <label class="control-label">NIK (No.KTP) :</label>
                    <input type="text" minlength="16" class="form-control" placeholder="3204053007990003" name="nik_passport" required="" value="<?=$tblPUsers->nik_passport?>">
                </div>
                <div class="form-group has-feedback">
                    <label class="control-label">Nama :</label>
                    <input type="text" class="form-control" placeholder="Nama" name="nama" required="" value="<?=$tblPUsers->nama?>">
                </div>
                <div class="form-group has-feedback">
                    <label class="control-label">Tanggal Lahir :</label>
                    <input type="date" class="form-control" placeholder="yyyy-mm-dd" name="tgl_lhr" required="" value="<?=$tblPUsers->tgl_lhr?>">
                </div>
                <div class="form-group has-feedback">
                    <label class="control-label">Kategori :</label>
                    <select class="form-control" required="" name="kategori">
                        <option value="1" <?=($tblPUsers->kategori == 1) ? "selected=''" : null;?>>IPA / SAINS</option>
                        <option value="2" <?=($tblPUsers->kategori == 2) ? "selected=''" : null;?>>IPS / SOSIAL DAN HUMANIORA</option>
                    </select>
                </div>
                <div class="form-group has-feedback">
                    <label class="control-label">Nomor Telepon :</label>
                    <input type="text" minlength="12" class="form-control" placeholder="081234567890" name="nmr_tlp" required="" value="<?=$tblPUsers->nmr_tlp;?>">
                </div>
                <div class="form-group has-feedback">
                    <label class="control-label">Email :</label>
                    <input type="email" class="form-control" placeholder="Email" name="email" required="" value="<?=$tblPUsers->email;?>">
                </div>
                <div class="form-group has-feedback">
                    <label class="control-label">Password :</label>
                    <input type="password" minlength="6" maxlength="16" class="form-control" placeholder="Password" name="password">
                    <span class="label label-warning">Kosongkan apabila tidak ingin mengubah password.</span>
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Verifikasi Biodata :</label>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <input type="radio" name="biodata" class="forn-control" value="1" <?php if ($tblPUsers->biodata == "1") echo "checked" ?>>
                                </span>
                                <input type="text" class="form-control" aria-label="SUDAH VERIFIKASI" value="SUDAH VERIFIKASI" readonly>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <input type="radio" name="biodata" class="forn-control" value="0" <?php if ($tblPUsers->biodata == "0") echo "checked" ?>>
                                </span>
                                <input type="text" class="form-control" aria-label="BELUM VERIFIKASI" value="BELUM VERIFIKASI" readonly>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
            <?=form_close()?>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->