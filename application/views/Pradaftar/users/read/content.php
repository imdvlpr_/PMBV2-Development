<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Pradaftar Users
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-folder-open"></i> Pradaftar</a></li>
            <li>Users</li>
        </ol>
    </section>

    <section class="content">

        <!-- notification -->
        <?php $this->load->view('notification') ?>

        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Tabel Pradaftar Users</h3>
                <div class="pull-right">
                    <div class="btn-group">
                        <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-gears">&nbsp;</i> <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li><a href="<?=base_url('Cronejob/CJ1/');?>" target="_blank">Normalisasi Users</a></li>
                            <li role="separator" class="divider"></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table id="dataTable" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Action</th>
                            <th>Kode Pembayaran</th>
                            <th>Nomor Peserta</th>
                            <th>NIK</th>
                            <th>Nama</th>
                            <th>Kategori</th>
                            <th>Tipe Ujian</th>
                            <th>Pembayaran</th>
                            <th>Biodata</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>Action</th>
                            <th>Kode Pembayaran</th>
                            <th>Nomor Peserta</th>
                            <th>NIK</th>
                            <th>Nama</th>
                            <th>Kategori</th>
                            <th>Tipe Ujian</th>
                            <th>Pembayaran</th>
                            <th>Biodata</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>

    </section>
</div>