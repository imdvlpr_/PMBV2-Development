<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Pradaftar
            <small><i>Pembayaran</i></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-folder-open"></i> Pradaftar</a></li>
            <li>Pembayaran</li>
        </ol>
    </section>

    <section class="content">

        <!-- notification -->
        <?php $this->load->view('notification') ?>

        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Tabel Pradaftar Pembayaran</h3>
                <div class="pull-right">
                    <div class="btn-group">
                        <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-gears">&nbsp;</i> <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li><a href="#" data-toggle="modal" data-target="#tambah">Tambah</a></li>
                            <li><a href="<?=base_url('Pradaftar/Import/Pembayaran/');?>">Import Pembayaran</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="<?=base_url('Cronejob/CJ4/');?>" target="_blank">Verifikasi Pembayaran</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table id="dataTable" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Action</th>
                            <th>Kode Pembayaran</th>
                            <th>Nama</th>
                            <th>Uang</th>
                            <th>Bank</th>
                            <th>Status</th>
                            <th>Date Created</th>
                            <th>Date Updated</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>Action</th>
                            <th>Kode Pembayaran</th>
                            <th>Nama</th>
                            <th>Uang</th>
                            <th>Bank</th>
                            <th>Status</th>
                            <th>Date Created</th>
                            <th>Date Updated</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </section>
</div>