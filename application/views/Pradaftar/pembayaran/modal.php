<!-- Tambah -->
<div class="modal fade" id="tambah">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Tambah Pembayaran</h4>
            </div>
            <?=form_open('Pradaftar/Pembayaran/Create/')?>
            <div class="modal-body">
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Kode Pembayaran :</label>
                    <input type="text" name="kd_pembayaran" class="form-control" placeholder="" required="">
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Bank :</label>
                    <select class="form-control" name="id_bank" required="">
                        <?php foreach ($tblSBank as $a) : ?>
                            <option value="<?=$a->id_bank?>"><?=$a->bank?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
            <?=form_close()?>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->