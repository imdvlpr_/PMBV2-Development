
<!-- Edit -->
<?php $no=1; foreach ($tblPSetting as $value): ?>
<div class="modal fade" id="edit-<?=$value->id_pradaftar_setting?>">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Edit Setting <?=$value->nama_setting?></h4>
            </div>
            <?=form_open('Pradaftar/Setting/Update/'.$value->id_pradaftar_setting)?>
            <div class="modal-body">
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Setting : </label>
                    <input type="text" name="setting" class="form-control" placeholder="Setting" required="" value="<?=$value->setting;?>">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Update</button>
            </div>
            <?=form_close()?>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<?php endforeach; ?>
<!-- /.modal -->