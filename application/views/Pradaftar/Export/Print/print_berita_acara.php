<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Berita Acara</title>
		<style type="text/css">
			.center{
				width: 80%;
				margin: 0 auto;
				text-align: center;			
			}
			.center2{
				width: 90%;
				margin: 0 auto;
				text-align: center;			
			}
			.container {margin-right: auto; margin-left: auto; padding-left: 15px; padding-right: 15px;}
			.row {margin-left: -5px; margin-right: -15px;}
			.col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-10, .col-md-11, .col-md-12 {float: left;}
			.col-md-12 {width: 100%;}
			.col-md-11 {width: 91.66666667%;}
			.col-md-10 {width: 83.33333333%;}
			.col-md-9 {width: 75%;}
			.col-md-8 {width: 66.66666667%;}
			.col-md-7 {width: 58.33333333%;}
			.col-md-6 {width: 50%;}
			.col-md-5 {width: 41.66666667%;}
			.col-md-4 {width: 33.33333333%;}
			.col-md-3 {width: 25%;}
			.col-md-offset-7 {margin-left: 58.33333333%;}
			.col-md-2 {width: 16.66666667%;}
			.col-md-1 {width: 8.33333333%;}
			.text-center {text-align: center;}
			.thumbnail {display: block;padding: 4px;margin-bottom: 20px;line-height: 1.42857143;background-color: #ffffff;border: 1px solid #dddddd;border-radius: 4px;-webkit-transition: border 0.2s ease-in-out;-o-transition: border 0.2s ease-in-out;transition: border 0.2s ease-in-out;}
			table{border-spacing:0;border-collapse:collapse}td,th{padding:5}
			.table{border-collapse:collapse!important}.table td,.table th{background-color:#fff!important}.table-bordered td,.table-bordered th{border:1px solid black!important;}
		</style>
	</head>
	<body>
		<div class="row">
			<div class="col-md-2">
				<img src="<?='./assets/img/logo-sementara.png';?>" style="width: 100px;height: 120px;">
			</div>
			<div class="col-md-9">
				<p class="text-center">
					<span style="font-size: 14px; font-weight: bold;">
						BERITA ACARA PELAKSANAAN UJIAN<br/>
						KEMENTERIAN AGAMA REPUBLIK INDONESIA <br/>
						UNIVERSITAS ISLAM NEGERI SUNAN GUNUNG DJATI BANDUNG TAHUN 2019 / 2020
					</span><br>
					<span style="font-size: 10px">
						Jalan A.H. Nasution No. 105 Cibiru Bandung 40614<br/>
						Telepon (022) 7800525 Fak (022) 7803936<br/>
						Website: pmb.uinsgd.ac.id e-mail: contact.pmb@uinsgd.ac.id
					</span>
				</p>
			</div>
		</div>
		<hr>
		<div class="center2">
			<div class="row">
				<div class="col-md-6">
					<table class="table">
						<tr>
							<td>Hari</td>
							<td> : </td>
							<td><?=nama_hari($tanggal)?></td>
						</tr>
						<tr>
							<td>Tanggal</td>
							<td> : </td>
							<td><?=tgl_indo($tanggal);?></td>
						</tr>
						<tr>
							<td>Tipe Ujian</td>
							<td> : </td>
							<td><?=$tipe_ujian?></td>
						</tr>
					</table>
				</div>
				<div class="col-md-6">
					<table class="table">
						<tr>
							<td>Ruangan</td>
							<td> : </td>
							<td><?=$ruangan?></td>
						</tr>
						<tr>
							<td>Sesi</td>
							<td> : </td>
							<td><?=$sesi?></td>
						</tr>
						<tr>
							<td>Gedung</td>
							<td> : </td>
							<td><?=$gedung?></td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<br>
		<div class="center2">
			<h3><b>PELAKSANAAN UJIAN</b></h3><br>
			<table>
				<tr>
					<td>Ujian dimulai pukul</td>
					<td> : </td>
					<td><?=date('H:i', strtotime($jadwal->jam_awal))?> sd. <?=date('H:i', strtotime($jadwal->jam_akhir))?> WIB</td>
				</tr>
				<tr>
					<td>Jumlah peserta yang hadir</td>
					<td> : </td>
					<td>...................................... Orang</td>
				</tr>
				<tr>
					<td>Tidak hadir</td>
					<td> : </td>
					<td>...................................... Orang</td>
				</tr>
			</table><br/><br/>
			<h3><b>NOMOR PESERTA YANG TIDAK HADIR</b></h3>
			<div class="row text-center">
				<div class="col-md-3">No. Peserta</div>
				<div class="col-md-3">No. Peserta</div>
				<div class="col-md-3">No. Peserta</div>
				<div class="col-md-3">No. Peserta</div>
			</div>
			<br />
			<div class="row text-center">
				<div class="col-md-3">..............................</div>
				<div class="col-md-3">..............................</div>
				<div class="col-md-3">..............................</div>
				<div class="col-md-3">..............................</div>
			</div>
			<br />
			<div class="row text-center">
				<div class="col-md-3">..............................</div>
				<div class="col-md-3">..............................</div>
				<div class="col-md-3">..............................</div>
				<div class="col-md-3">..............................</div>
			</div>
			<br />
			<div class="row text-center">
				<div class="col-md-3">..............................</div>
				<div class="col-md-3">..............................</div>
				<div class="col-md-3">..............................</div>
				<div class="col-md-3">..............................</div>
			</div>
			<br />
			<div class="row text-center">
				<div class="col-md-3">..............................</div>
				<div class="col-md-3">..............................</div>
				<div class="col-md-3">..............................</div>
				<div class="col-md-3">..............................</div>
			</div>


			<h3><b>HAL-HAL YANG PERLU DICATAT</b></h3></br>
			..................................................................................................................................................................................................................................................................................................
			<br><br><br>
			Pengawas Ujian 1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nama ....................................&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tanda Tangan ..............................<br><br>
			Pengawas Ujian 2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nama ....................................&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tanda Tangan ..............................<br>
		</div>
	</body>
</html>
