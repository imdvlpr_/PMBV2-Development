<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Export
            <small><i>Jadwal</i></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-download"></i> Export</a></li>
            <li>Jadwal</li>
        </ol>
    </section>
    <section class="content">

        <!-- notification -->
        <?php $this->load->view('notification') ?>

        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Export Jadwal</h3>
                    </div>
                    <div class="box-body">
                        <?=form_open_multipart('Pradaftar/Export/Jadwal/Export/')?>
                        <div class="form-group">
                            <label class="control-label">Tanggal :</label>
                            <select name="tanggal" class="form-control select2" required="">
                                <?php foreach ($tanggal as $value): ?>
                                    <option value="<?=$value->tanggal?>"><?=$value->tanggal?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary btn-flat pull-right"><i class="fa fa-download">&nbsp;</i>Export</button>
                        <?=form_close()?>
                    </div>
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>
    </section>
</div>