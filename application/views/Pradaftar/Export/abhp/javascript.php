<script>
    $(document).ready(function(){
        $("#loading").hide();
        $("#tahun").change(function(){
            $("#tipe").hide();
            $("#tanggal").hide();
            $("#kampus").hide();
            $("#gedung").hide();
            $("#ruangan").hide();
            $("#sesi").hide();
            $("#loading").show();
            $.ajax({
                type: "POST",
                url: "<?=base_url("Pradaftar/Export/ABHP/listTipeUjian");?>",
                data: {tahun : $("#tahun").val()},
                dataType: "json",
                beforeSend: function(e) {
                    if(e && e.overrideMimeType) {
                        e.overrideMimeType("application/json;charset=UTF-8");
                    }
                },
                success: function(response){
                    $("#loading").hide();
                    $("#tipe").html(response.list_tipe_ujian).show();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError);
                }
            });
        });
        $("#tipe").change(function(){
            $("#tanggal").hide();
            $("#kampus").hide();
            $("#gedung").hide();
            $("#ruangan").hide();
            $("#sesi").hide();
            $("#loading").show();
            $.ajax({
                type: "POST",
                url: "<?php echo base_url("index.php/Pradaftar/Export/ABHP/listTanggal"); ?>",
                data: {tahun : $("#tahun").val(), tipe : $("#tipe").val()},
                dataType: "json",
                beforeSend: function(e) {
                    if(e && e.overrideMimeType) {
                        e.overrideMimeType("application/json;charset=UTF-8");
                    }
                },
                success: function(response){
                    $("#loading").hide();
                    $("#tanggal").html(response.list_tanggal).show();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError);
                }
            });
        });
        $("#tanggal").change(function(){
            $("#kampus").hide();
            $("#sesi").hide();
            $("#gedung").hide();
            $("#ruangan").hide();
            $("#loading").show();
            $.ajax({
                type: "POST",
                url: "<?php echo base_url("index.php/Pradaftar/Export/ABHP/listTempat"); ?>",
                data: {tahun : $("#tahun").val(), tanggal : $("#tanggal").val(), tipe : $("#tipe").val()},
                dataType: "json",
                beforeSend: function(e) {
                    if(e && e.overrideMimeType) {
                        e.overrideMimeType("application/json;charset=UTF-8");
                    }
                },
                success: function(response){
                    $("#loading").hide();
                    $("#kampus").html(response.list_tempat).show();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError);
                }
            });
        });
        $("#kampus").change(function(){
            $("#sesi").hide();
            $("#gedung").hide();
            $("#ruangan").hide();
            $("#loading").show();
            $.ajax({
                type: "POST",
                url: "<?php echo base_url("index.php/Pradaftar/Export/ABHP/listGedung"); ?>",
                data: {tahun : $("#tahun").val(), tanggal : $("#tanggal").val(), tipe : $("#tipe").val(), kampus : $("#kampus").val()},
                dataType: "json",
                beforeSend: function(e) {
                    if(e && e.overrideMimeType) {
                        e.overrideMimeType("application/json;charset=UTF-8");
                    }
                },
                success: function(response){
                    $("#loading").hide();
                    $("#gedung").html(response.list_gedung).show();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError);
                }
            });
        });
        $("#gedung").change(function(){
            $("#sesi").hide();
            $("#ruangan").hide();
            $("#loading").show();
            $.ajax({
                type: "POST",
                url: "<?php echo base_url("index.php/Pradaftar/Export/ABHP/listRuangan"); ?>",
                data: {tahun : $("#tahun").val(), tanggal : $("#tanggal").val(), tipe : $("#tipe").val(), kampus : $("#kampus").val(), gedung : $("#gedung").val()},
                dataType: "json",
                beforeSend: function(e) {
                    if(e && e.overrideMimeType) {
                        e.overrideMimeType("application/json;charset=UTF-8");
                    }
                },
                success: function(response){
                    $("#loading").hide();
                    $("#ruangan").html(response.list_ruangan).show();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError);
                }
            });
        });
        $("#ruangan").change(function(){
            $("#sesi").hide();
            $("#loading").show();
            $.ajax({
                type: "POST",
                url: "<?php echo base_url("index.php/Pradaftar/Export/ABHP/listSesi"); ?>",
                data: {tahun : $("#tahun").val(), tanggal : $("#tanggal").val(), tipe : $("#tipe").val(), kampus : $("#kampus").val(), gedung : $("#gedung").val(), ruangan : $("#ruangan").val()},
                dataType: "json",
                beforeSend: function(e) {
                    if(e && e.overrideMimeType) {
                        e.overrideMimeType("application/json;charset=UTF-8");
                    }
                },
                success: function(response){
                    $("#loading").hide();
                    $("#sesi").html(response.list_sesi).show();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError);
                }
            });
        });
    });
</script>