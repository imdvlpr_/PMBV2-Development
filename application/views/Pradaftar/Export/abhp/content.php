<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Pradaftar Export ABHP dan Berita Acara
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-folder-open"></i> Pradaftar</a></li>
            <li>Export</li>
            <li>ABHP dan Berita Acara</li>
        </ol>
    </section>
    <section class="content">

        <!-- notification -->
        <?php $this->load->view('notification') ?>

        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Export Pradaftar ABHP dan Berita Acara</h3>
            </div>
            <div class="box-body">
                <?=form_open('Pradaftar/Export/ABHP/Cetak')?>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Berkas :</label>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <input type="radio" name="berkas" class="forn-control" value="1" checked>
                                </span>
                                <input type="text" class="form-control" aria-label="ABHP" value="ABHP" readonly>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <input type="radio" name="berkas" class="forn-control" value="2">
                                </span>
                                <input type="text" class="form-control" aria-label="Berita Acara" value="Berita Acara" readonly>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Tahun :</label>
                    <select name="tahun" id="tahun" class="form-control" required="">
                        <option value="">- Pilih -</option>
                        <?php foreach($tahun as $value) {?>
                            <option value="<?=$value->tahun?>"><?=$value->tahun?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Tipe Ujian :</label>
                    <select name="tipe" id="tipe" class="form-control" required="">
                        <option value="">- Pilih -</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Tanggal :</label>
                    <select name="tanggal" id="tanggal" class="form-control" required="">
                        <option value="">- Pilih -</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Kampus :</label>
                    <select name="kampus" id="kampus" class="form-control" required="">
                        <option value="">- Pilih -</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Gedung :</label>
                    <select name="gedung" id="gedung" class="form-control" required="">
                        <option value="">- Pilih -</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Ruangan :</label>
                    <select name="ruangan" id="ruangan" class="form-control" required="">
                        <option value="">- Pilih -</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Sesi :</label>
                    <select name="sesi" id="sesi" class="form-control" required="">
                        <option value="">- Pilih -</option>
                    </select>
                </div>
                <input type="submit" class="btn btn-primary" value="Cetak">
                <?=form_close()?>
            </div>
        </div>
    </section>
</div>