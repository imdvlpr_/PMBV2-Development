<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Statistik UKT
            <small>Jurusan</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-database"></i> Statistik UKT</a></li>
            <li class="active">Jurusan</li>
        </ol>
    </section>
    <section class="content">

        <!-- notification -->
        <?php $this->load->view('notification') ?>

        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Statistik UKT Jurusan</h3>
                    </div>
                    <?=form_open('UKT/UKTJurusan/')?>
                    <div class="box-body">
                        <div class="form-group">
                            <label class="control-label">Jalur Masuk :</label>
                            <select name="jalur_masuk" class="form-control" required="">
                                <?php foreach ($tblSJalurMasuk as $value): ?>
                                    <option value="<?=$value->jalur_masuk?>"><?=$value->jalur_masuk?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Tahun :</label>
                            <select name="tahun" class="form-control" required="">
                                <?php foreach ($tahun as $value): ?>
                                    <option value="<?=$value->tahun?>"><?=$value->tahun?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary btn-flat pull-right"><i class="fa fa-eye">&nbsp;</i>Show</button>
                    </div>
                    <?=form_close()?>
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>


        <?php if ($kosong == FALSE) : ?>
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Tabel Statistik UKT Jurusan (<?=$jalur_masuk.'/'.$old_tahun?>)</h3>
                </div>
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-responsive">
                            <thead>
                            <tr>
                                <th class="text-center">Jurusan</th>
                                <th class="text-center">K1</th>
                                <th class="text-center">K2</th>
                                <th class="text-center">K3</th>
                                <th class="text-center">K4</th>
                                <th class="text-center">K5</th>
                                <th class="text-center">K6</th>
                                <th class="text-center">K7</th>
                                <th class="text-center">K8</th>
                                <th class="text-center">K9</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php for ($i=0; $i <= 8; $i++) {$jumlah[$i] = 0;} ?>
                            <?php foreach ($tbSJurusan as $a): ?>
                                <?php if ($a->kode_jurusan != 999): ?>
                                    <tr>
                                        <td><?=$a->jurusan;?></td>
                                        <?php for ($i=0; $i <= 8; $i++) : ?>
                                            <td class="text-center"><?=$count[$a->kode_jurusan][$i]?></td>
                                            <?php $jumlah[$i] += $count[$a->kode_jurusan][$i]; ?>
                                        <?php endfor; ?>
                                    </tr>
                                <?php endif ?>
                            <?php endforeach ?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th class="text-center">Jurusan</th>
                                <th class="text-center">K1</th>
                                <th class="text-center">K2</th>
                                <th class="text-center">K3</th>
                                <th class="text-center">K4</th>
                                <th class="text-center">K5</th>
                                <th class="text-center">K6</th>
                                <th class="text-center">K7</th>
                                <th class="text-center">K8</th>
                                <th class="text-center">K9</th>
                            </tr>
                            <tr>
                                <th>Jumlah</th>
                                <?php $total = 0; for ($i = 0; $i < 9 ; $i++) : $k = 'K'.($i+1); ?>
                                    <?php if ($jumlah[$i] > 0): ?>
                                        <th class="text-center">
                                            <a href="<?=base_url('UKT/UKTJurusan/Search/'.$old_tahun.'/'.$jalur_masuk.'/'.$k)?>" target="_blank">
                                                <?=$jumlah[$i];?>
                                            </a>
                                        </th>
                                    <?php else: ?>
                                        <th class="text-center"><?=$jumlah[$i];?></th>
                                    <?php endif; ?>

                                    <?php $total += $jumlah[$i]; ?>
                                <?php endfor; ?>
                            </tr>
                            <tr>
                                <th>Total</th>
                                <th colspan="5" class="text-right"><?=$total?></th>
                            </tr>
                            <tr>
                                <th>Persentase</th>
                                <?php $totalpersentase = 0; for ($i=0; $i <= 8 ; $i++) { ?>
                                    <th class="text-center"><?php if ($jumlah[$i] != 0){echo round((($jumlah[$i]/$total)*100),2);}else{echo 0;};?> %</th>
                                    <?php if ($jumlah[$i] != 0){ $totalpersentase += (($jumlah[$i]/$total)*100);}else{$totalpersentase = 0;} } ?>
                            </tr>
                            <tr>
                                <th>Total Persentase</th>
                                <th colspan="5" class="text-right"><?=$totalpersentase?>%</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </section>
</div>