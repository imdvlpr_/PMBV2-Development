<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Rekap Nilai
            <small>UKT</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-database"></i> Rekap Nilai</a></li>
            <li class="active">UKT</li>
        </ol>
    </section>
    <section class="content">

        <!-- notification -->
        <?php $this->load->view('notification') ?>

        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Rekap Nilai UKT</h3>
                    </div>
                    <?=form_open('UKT/UKTRekapNilai/')?>
                    <div class="box-body">
                        <div class="form-group">
                            <label class="control-label">Jalur Masuk :</label>
                            <select name="jalur_masuk" class="form-control" required="">
                                <?php foreach ($tblSJalurMasuk as $value): ?>
                                    <option value="<?=$value->jalur_masuk?>"><?=$value->jalur_masuk?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Tahun :</label>
                            <select name="tahun" class="form-control" required="">
                                <?php foreach ($tahun as $value): ?>
                                    <option value="<?=$value->tahun?>"><?=$value->tahun?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Status :</label>
                            <select name="status" class="form-control" required="">
                                <option value="SUDAH VERIFIKASI">SUDAH VERIFIKASI</option>
                                <option value="BELUM VERIFIKASI">BELUM VERIFIKASI</option>
                            </select>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary btn-flat pull-right"><i class="fa fa-eye">&nbsp;</i>Show</button>
                    </div>
                    <?=form_close()?>
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>

        <?php if ($kosong == FALSE) : ?>
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Tabel Rekap Nilai UKT Jurusan (<?=$jalur_masuk.'/'.$old_tahun.'/'.$status?>)</h3>
                </div>
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th class="text-center">NO</th>
                                <th class="text-center">NOMOR PESERTA</th>
                                <th class="text-center">NAMA</th>
                                <th class="text-center">JURUSAN</th>
                                <th class="text-center">SKOR</th>
                                <th class="text-center">KATEGORI</th>
                                <th class="text-center">TARIF</th>
                                <th class="text-center">PEKERJAAN AYAH</th>
                                <th class="text-center">PENGHASILAN AYAH</th>
                                <th class="text-center">PEKERJAAN IBU</th>
                                <th class="text-center">PENGHASILAN IBU</th>
                                <th class="text-center">PEKERJAAN WALI</th>
                                <th class="text-center">PENGHASILAN WALI</th>
                                <th class="text-center">REKENING LISTRIK</th>
                                <th class="text-center">REKENING PBB</th>
                                <th class="text-center">TANGGUNGAN</th>
                                <th class="text-center">PEMBAYARAN LISTRIK</th>
                                <th class="text-center">PEMBAYARAN PBB</th>
                                <th class="text-center">ALAT TRANSPORTASI</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $no = 1; $total = 0; foreach ($viewDaftarUKT as $a): ?>
                                <tr>
                                    <td><?=$no?></td>
                                    <td><?=$a['nomor_peserta']?></td>
                                    <td><?=$a['nama']?></td>
                                    <td><?=$a['jurusan']?></td>
                                    <td><?=$a['score']?></td>
                                    <td><?=$a['kategori']?></td>
                                    <td><?=number_format($a['jumlah'],2)?></td>
                                    <td><?=$a['pekerjaan_ayah']?></td>
                                    <td><?=$a['penghasilan_ayah']?></td>
                                    <td><?=$a['pekerjaan_ibu']?></td>
                                    <td><?=$a['penghasilan_ibu']?></td>
                                    <td><?=$a['pekerjaan_wali']?></td>
                                    <td><?=$a['penghasilan_wali']?></td>
                                    <td><?=$a['rekening_listrik']?></td>
                                    <td><?=$a['rekening_pbb']?></td>
                                    <td><?=$a['tanggungan']?></td>
                                    <td><?=$a['pembayaran_listrik']?></td>
                                    <td><?=$a['pembayaran_pbb']?></td>
                                    <td><?=$a['alat_transportasi']?></td>
                                </tr>
                                <?php $no++; $total += $a['jumlah'];  endforeach ?>
                            </tbody>
                            <tfoot>
                            <th class="text-center" colspan="6">TOTAL</th>
                            <td colspan="12">Rp <?=number_format($total,2)?></td>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </section>
</div>