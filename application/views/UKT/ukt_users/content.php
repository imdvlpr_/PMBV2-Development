<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Daftar Users UKT
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-folder-open"></i> Daftar</a></li>
            <li>Users UKT</li>
        </ol>
    </section>

    <section class="content">

        <!-- notification -->
        <?php $this->load->view('notification') ?>

        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Tabel Users UKT(<?=$tahun.'/'.$jalur.'/'.$kategori?>)</h3>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table id="dataTable" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Action</th>
                            <th>Nomor Peserta</th>
                            <th>Nama</th>
                            <th>Jurusan</th>
                            <th>Skore</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($viewDaftarUKT as $value) : ?>
                            <tr>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-gear"></i> <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a href="<?=base_url('Daftar/Users/Detail/'.$value->id_daftar_users.'/'.$value->nomor_peserta)?>" target='_blank'>Detail</a></li>
                                        </ul>
                                    </div>
                                </td>
                                <td><?=$value->nomor_peserta?></td>
                                <td><?=$value->nama?></td>
                                <td><?=$value->jurusan?></td>
                                <td><?=$value->score?></td>
                                <td><?=$value->status?></td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>Action</th>
                            <th>Nomor Peserta</th>
                            <th>Nama</th>
                            <th>Jurusan</th>
                            <th>Skore</th>
                            <th>Status</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </section>
</div>