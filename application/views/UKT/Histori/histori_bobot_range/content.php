<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Histori
            <small><i>Bobot Nilai UKT</i></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-table"></i> Histori</a></li>
            <li class="active"><i>Bobot Nilai UKT</i></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">

        <!-- notification -->
        <?php $this->load->view('notification') ?>

        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Tabel Histori Bobot Range UKT</h3>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table id="dataTable" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Kategori</th>
                            <th>Nilai Minimal</th>
                            <th>Nilai Maksimal</th>
                            <th>Jalur</th>
                            <th>Tahun</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $no=1; foreach ($tblHBobotRangeUkt as $value): ?>
                            <tr>
                                <td><?=$no++?></td>
                                <td><?=$value->kategori?></td>
                                <td><?=$value->nilai_min?></td>
                                <td><?=$value->nilai_max?></td>
                                <td><?=$value->jalur?></td>
                                <td><?=$value->tahun?></td>
                            </tr>
                        <?php endforeach ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Kategori</th>
                            <th>Nilai Minimal</th>
                            <th>Nilai Maksimal</th>
                            <th>Jalur</th>
                            <th>Tahun</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>

    </section>
    <!-- /.content -->
</div>