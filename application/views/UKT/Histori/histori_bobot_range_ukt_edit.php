<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>PMB - UIN Sunan Gunung Djati Bandung</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Icon -->
        <link href="<?=base_url('design-backend/images/logo.png'); ?>" rel="icon" type="image/x-icon" />
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="<?=base_url('design-backend/bootstrap/css/bootstrap.min.css');?>">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?=base_url('design-backend/dist/css/AdminLTE.min.css');?>">
        <link rel="stylesheet" href="<?=base_url('design-backend/dist/css/skins/skin-red-light.min.css');?>">
    </head>

    <body class="hold-transition skin-red-light sidebar-mini">
        <div class="wrapper">
            <?php $this->load->view('header-navbar');?>
            <?php $this->load->view('header-sidebar');?>
            <div class="content-wrapper">
                <section class="content-header">
                    <h1>
                        Histori
                        <small><i>Bobot Range UKT</i></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-table"></i> Histori</a></li>
                        <li><a href="#"></i> Bobot Range UKT</a></li>
                        <li class="active"><i>Edit</i></li>
                    </ol>
                </section>
                <section class="content">
                    <?php if (!empty($this->session->flashdata('type_message'))) { ?>
                        <div class="alert alert-<?=$this->session->flashdata('type_message')?> alert-dismissible text-center" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <?=$this->session->flashdata('message');?>
                        </div>
                    <?php } ?>
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit Tabel Log Bobot Range UKT</h3>
                        </div>
                        <?=form_open('UKT/Histori/BobotRangeUkt/Update/'.$tbHBobotRangeUkt->id_histori_bobot_range_ukt)?>
                        <div class="box-body">
							<div class="form-group">
								<label for="recipient-name" class="control-label">Kategori :</label>
								<select class="form-control" name="kategori" required>
									<option value="K1" <?php if ($tbHBobotRangeUkt->kategori == "K1") echo "selected" ?>>K1</option>
									<option value="K2" <?php if ($tbHBobotRangeUkt->kategori == "K2") echo "selected" ?>>K2</option>
									<option value="K3" <?php if ($tbHBobotRangeUkt->kategori == "K3") echo "selected" ?>>K3</option>
									<option value="K4" <?php if ($tbHBobotRangeUkt->kategori == "K4") echo "selected" ?>>K4</option>
									<option value="K5" <?php if ($tbHBobotRangeUkt->kategori == "K5") echo "selected" ?>>K5</option>
									<option value="K6" <?php if ($tbHBobotRangeUkt->kategori == "K6") echo "selected" ?>>K6</option>
									<option value="K7" <?php if ($tbHBobotRangeUkt->kategori == "K7") echo "selected" ?>>K7</option>
								</select>
							</div>
							<div class="form-group">
								<label for="recipient-name" class="control-label">Nilai Minimal :</label>
								<input type="text" name="nilai_min" class="form-control" placeholder="Nilai Minimal" value="<?=$tbHBobotRangeUkt->nilai_min?>" required="">
							</div>
							<div class="form-group">
								<label for="recipient-name" class="control-label">Nilai Maksimal :</label>
								<input type="text" name="nilai_max" class="form-control" placeholder="Nilai Maksimal" value="<?=$tbHBobotRangeUkt->nilai_max?>" required="">
							</div>
							<div class="form-group">
								<label for="recipient-name" class="control-label">Jalur :</label>
								<select class="form-control" name="jalur" required>
									<?php foreach ($tbSJalurMasuk as $value) : ?>
										<?php if ($tbHBobotRangeUkt->jalur == $value->jalur_masuk) : ?>
											<option value="<?=$value->jalur_masuk?>" selected><?=$value->jalur_masuk?></option>
										<?php else: ?>
											<option value="<?=$value->jalur_masuk?>"><?=$value->jalur_masuk?></option>
										<?php endif; ?>
									<?php endforeach; ?>
								</select>
							</div>
							<div class="form-group">
								<label for="recipient-name" class="control-label">Tahun :</label>
								<input type="number" min="1900" max="3000" name="tahun" class="form-control" placeholder="Tahun" value="<?=$tbHBobotRangeUkt->tahun?>" required="">
							</div>
                        </div>
                        <div class="box-footer">
                            <a href="<?=base_url('UKT/Histori/BobotRangeUkt/');?>" class="btn btn-default">Kembali</a>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                        <?=form_close()?>
                    </div>
                </section>
            </div>
            <?php $this->load->view('footer'); ?>
        </div>

        <!-- REQUIRED JS SCRIPTS -->
        <!-- jQuery 2.2.3 -->
        <script src="<?=base_url('design-backend/plugins/jQuery/jquery-2.2.3.min.js');?>"></script>
        <!-- Bootstrap 3.3.6 -->
        <script src="<?=base_url('design-backend/bootstrap/js/bootstrap.min.js');?>"></script>
        <!-- AdminLTE App -->
        <script src="<?=base_url('design-backend/dist/js/app.min.js');?>"></script>
    </body>
</html>
