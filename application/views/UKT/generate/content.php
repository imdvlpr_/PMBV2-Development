<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Generate
            <small>UKT</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-database"></i> Generate</a></li>
            <li class="active">UKT</li>
        </ol>
    </section>
    <section class="content">

        <!-- notification -->
        <?php $this->load->view('notification') ?>

        <div class="row">
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Generate UKT</h3>
                    </div>
                    <?=form_open('UKT/Generate/SWA')?>
                    <div class="box-body">
                        <div class="form-group">
                            <label class="control-label">Jalur Masuk :</label>
                            <select name="jalur_masuk" class="form-control" required="">
                                <?php foreach ($tblSJalurMasuk as $value): ?>
                                    <option value="<?=$value->jalur_masuk?>"><?=$value->jalur_masuk?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Tahun :</label>
                            <select name="tahun" class="form-control" required="">
                                <?php foreach ($tahun as $value): ?>
                                    <option value="<?=$value->tahun?>"><?=$value->tahun?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary btn-flat pull-right"><i class="fa fa-spinner">&nbsp;</i>Generate</button>
                    </div>
                    <?=form_close()?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Generate UKT (Personal)</h3>
                    </div>
                    <?=form_open('UKT/Generate/SWAPersonal')?>
                    <div class="box-body">
                        <div class="form-group">
                            <label class="control-label">Nomor Peserta :</label>
                            <input type="text" name="nomor_peserta" class="form-control" placeholder="Nomor Peserta" required="" >
                        </div>
                        <div class="form-group">
                            <label class="control-label">Jalur Masuk :</label>
                            <select name="jalur_masuk" class="form-control" required="">
                                <?php foreach ($tblSJalurMasuk as $value): ?>
                                    <option value="<?=$value->jalur_masuk?>"><?=$value->jalur_masuk?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Tahun :</label>
                            <select name="tahun" class="form-control" required="">
                                <?php foreach ($tahun as $value): ?>
                                    <option value="<?=$value->tahun?>"><?=$value->tahun?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary btn-flat pull-right"><i class="fa fa-spinner">&nbsp;</i>Generate</button>
                    </div>
                    <?=form_close()?>
                </div>
            </div>
        </div>

    </section>
</div>