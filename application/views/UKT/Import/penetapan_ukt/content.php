<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Import
            <small>Penetapan UKT</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-upload"></i> Import</a></li>
            <li class="active">Penetapan UKT</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">

        <!-- notification -->
        <?php $this->load->view('notification') ?>

        <div class="row">
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Import Penetapan UKT</h3>
                    </div>
                    <?=form_open_multipart('UKT/Import/PenetapanUKT/Import')?>
                    <div class="box-body">
                        <div class="form-group">
                            <label class="control-label">Upload file :</label>
                            <input type="file" name="userfile" required>
                        </div>

                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary btn-flat pull-right"><i class="fa fa-upload">&nbsp;</i>Import</button>
                    </div>
                    <?=form_close()?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Tatacara Import Penetapan UKT</h3>
                    </div>
                    <div class="box-body">
                        <p>Tatacara import data : (Only Windows)</p>
                        <ol>
                            <li>Download template yang sudah disediakan.</li>
                            <li>Isi kolom pada tabel sesuai nama kolomnya.</li>
                            <li>Pilih file yang ingin di upload.</li>
                            <li>klik import.</li>
                        </ol>
                    </div>
                    <div class="box-footer">
                        <a href="<?=base_url('file/template_penetapan_ukt.xlsx')?>" class="btn btn-default btn-flat" target="_blank">Download Template</a>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <!-- /.content -->
</div>