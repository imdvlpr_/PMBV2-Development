<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Dashboard</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">

        <!-- notification -->
        <?php $this->load->view('notification'); ?>

        <!-- Statistik Pradaftar -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Pradaftar <?=date('Y')?></h3>
            </div>
            <div class="box-body bg-gray">
                <!-- Users -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Statistik Users</h3>
                    </div>
                    <div class="box-body bg-gray-light">
                        <div class="row">
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-blue-active"><i class="fa fa-users"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text">Users</span>
                                        <span class="info-box-number" id="num_users"></span>
                                        <span class="info-box-text" id="p1">
                                            <i class="fa fa-spinner fa-pulse fa-lg" ></i>
                                        </span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            <!-- /.col -->

                            <!-- fix for small devices only -->
                            <div class="clearfix visible-sm-block"></div>

                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-green-active"><i class="fa fa-check"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text">Users Sudah Verifikasi</span>
                                        <span class="info-box-number" id="num_users_sv"></span>
                                        <span class="info-box-text" id="p2">
                                            <i class="fa fa-spinner fa-pulse fa-lg" ></i>
                                        </span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            <!-- /.col -->

                            <!-- fix for small devices only -->
                            <div class="clearfix visible-sm-block"></div>

                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-red-active"><i class="fa fa-close"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text">Users Belum Verifikasi</span>
                                        <span class="info-box-number" id="num_users_bv"></span>
                                        <span class="info-box-text" id="p3">
                                            <i class="fa fa-spinner fa-pulse fa-lg" ></i>
                                        </span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            <!-- /.col -->

                            <!-- fix for small devices only -->
                            <div class="clearfix visible-sm-block"></div>

                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-blue-gradient"><i class="fa fa-flask"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text">Kategori IPA</span>
                                        <span class="info-box-number" id="num_kategori_ipa"></span>
                                        <span class="info-box-text" id="p4">
                                            <i class="fa fa-spinner fa-pulse fa-lg" ></i>
                                        </span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            <!-- /.col -->

                            <!-- fix for small devices only -->
                            <div class="clearfix visible-sm-block"></div>

                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-yellow-gradient"><i class="fa fa-globe"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text">Kategori IPS</span>
                                        <span class="info-box-number" id="num_kategori_ips"></span>
                                        <span class="info-box-text" id="p5">
                                            <i class="fa fa-spinner fa-pulse fa-lg" ></i>
                                        </span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            <!-- /.col -->

                            <!-- fix for small devices only -->
                            <div class="clearfix visible-sm-block"></div>

                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-aqua-gradient"><i class="fa fa-mars"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text">Laki-laki</span>
                                        <span class="info-box-number" id="num_man"></span>
                                        <span class="info-box-text" id="p6">
                                            <i class="fa fa-spinner fa-pulse fa-lg" ></i>
                                        </span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            <!-- /.col -->

                            <!-- fix for small devices only -->
                            <div class="clearfix visible-sm-block"></div>

                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-maroon-gradient"><i class="fa fa-venus"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text">Perempuan</span>
                                        <span class="info-box-number" id="num_women"></span>
                                        <span class="info-box-text" id="p7">
                                            <i class="fa fa-spinner fa-pulse fa-lg" ></i>
                                        </span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            <!-- /.col -->

                            <!-- fix for small devices only -->
                            <div class="clearfix visible-sm-block"></div>

                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-green-gradient"><i class="fa fa-university"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text">Lulus</span>
                                        <span class="info-box-number" id="num_lulus"></span>
                                        <span class="info-box-text" id="p8">
                                            <i class="fa fa-spinner fa-pulse fa-lg" ></i>
                                        </span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            <!-- /.col -->

                            <!-- fix for small devices only -->
                            <div class="clearfix visible-sm-block"></div>

                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-red-gradient"><i class="fa fa-university"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text">Tidak Lulus</span>
                                        <span class="info-box-number" id="num_tidak_lulus"></span>
                                        <span class="info-box-text" id="p9">
                                            <i class="fa fa-spinner fa-pulse fa-lg" ></i>
                                        </span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            <!-- /.col -->

                            <!-- fix for small devices only -->
                            <div class="clearfix visible-sm-block"></div>

                        </div>
                    </div>
                </div>

                <!-- Pembayaran -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Statistik Pembayaran</h3>
                    </div>
                    <div class="box-body bg-gray-light">
                        <div class="row">
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-yellow-active"><i class="fa fa-money"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text">Pembayaran</span>
                                        <span class="info-box-number" id="num_pembayaran"></span>
                                        <span class="info-box-text" id="p10">
                                            <i class="fa fa-spinner fa-pulse fa-lg" ></i>
                                        </span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            <!-- /.col -->

                            <!-- fix for small devices only -->
                            <div class="clearfix visible-sm-block"></div>

                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-green-active"><i class="fa fa-check"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">Pembayaran Sudah Verifikasi</span>
                                        <span class="info-box-number" id="num_pembayaran_sv"></span>
                                        <span class="info-box-text" id="p11">
                                            <i class="fa fa-spinner fa-pulse fa-lg" ></i>
                                        </span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            <!-- /.col -->

                            <!-- fix for small devices only -->
                            <div class="clearfix visible-sm-block"></div>

                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-red-active"><i class="fa fa-close"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text">Pembayaran Belum Verifikasi</span>
                                        <span class="info-box-number" id="num_pembayaran_bv"></span>
                                        <span class="info-box-text" id="p12">
                                            <i class="fa fa-spinner fa-pulse fa-lg" ></i>
                                        </span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            <!-- /.col -->

                            <!-- fix for small devices only -->
                            <div class="clearfix visible-sm-block"></div>

                        </div>
                    </div>
                </div>

                <!-- Temporary Users -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Statistik Temporary Users</h3>
                    </div>
                    <div class="box-body bg-gray-light">
                        <div class="row">
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-purple-active"><i class="fa fa-users"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text">Temporary Users</span>
                                        <span class="info-box-number" id="num_tmp_users"></span>
                                        <span class="info-box-text" id="p13">
                                            <i class="fa fa-spinner fa-pulse fa-lg" ></i>
                                        </span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            <!-- /.col -->

                            <!-- fix for small devices only -->
                            <div class="clearfix visible-sm-block"></div>

                        </div>
                    </div>
                </div>

            </div>
        </div>

    </section>
    <!-- /.content -->
</div>