<!-- Tambah -->
<div class="modal fade" id="tambah">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Tambah Gedung</h4>
            </div>
            <?=form_open('Settings/Gedung/Create/')?>
            <div class="modal-body">
                <div class="form-group">
                    <label class="control-label">Gedung :</label>
                    <input type="text" name="gedung" class="form-control" placeholder="Gedung" required="">
                </div>
                <div class="form-group">
                    <label class="control-label">Kampus :</label>
                    <select name="id_kampus" class="form-control" required="">
                        <?php foreach ($tblSKampus as $value): ?>
                            <option value="<?=$value->id_kampus?>"><?=$value->kampus?></option>
                        <?php endforeach ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Status :</label>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <input type="radio" name="status" class="forn-control" value="0" checked>
                                </span>
                                <input type="text" class="form-control" aria-label="False" value="False" readonly>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <input type="radio" name="status" class="forn-control" value="1">
                                </span>
                                <input type="text" class="form-control" aria-label="True" value="True" readonly>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
            <?=form_close()?>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<?php foreach ($viewSGedung as $value): ?>
    <!-- Edit -->
    <div class="modal fade" id="edit-<?=$value->id_gedung?>">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Edit Gedung</h4>
                </div>
                <?=form_open('Settings/Gedung/Update/'.$value->id_gedung)?>
                <div class="box-body">
                    <div class="form-group">
                        <label class="control-label">Gedung :</label>
                        <input type="text" name="gedung" class="form-control" placeholder="Gedung" required="" value="<?=$value->gedung?>">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Kampus :</label>
                        <select name="id_kampus" class="form-control" required="">
                            <?php foreach ($tblSKampus as $row): ?>
                                <?php if ($value->id_kampus == $row->id_kampus) : ?>
                                    <option value="<?=$row->id_kampus?>" selected=""><?=$row->kampus?></option>
                                <?php else: ?>
                                    <option value="<?=$row->id_kampus?>"><?=$row->kampus?></option>
                                <?php endif; ?>
                            <?php endforeach ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Status :</label>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="input-group">
                                <span class="input-group-addon">
                                    <input type="radio" name="status" class="forn-control" value="0" <?=($value->status_gedung == 0)? 'checked' : ''?>>
                                </span>
                                    <input type="text" class="form-control" aria-label="False" value="False" readonly>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group">
                                <span class="input-group-addon">
                                    <input type="radio" name="status" class="forn-control" value="1" <?=($value->status_gedung == 1)? 'checked' : ''?>>
                                </span>
                                    <input type="text" class="form-control" aria-label="True" value="True" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
                <?=form_close()?>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <!-- Detail -->
    <div class="modal fade" id="detail-<?=$value->id_gedung?>">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Detail Gedung</h4>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <tr>
                                <th>Kampus</th>
                                <td><?=$value->kampus?></td>
                            </tr>
                            <tr>
                                <th>Status Kampus</th>
                                <td>
                                    <?php if($value->status_kampus == 0) : ?>
                                        <span class="label label-danger">False</span>
                                    <?php else: ?>
                                        <span class="label label-success">True</span>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <th>Kode Gedung</th>
                                <td><?=$value->id_gedung?></td>
                            </tr>
                            <tr>
                                <th>Gedung</th>
                                <td><?=$value->gedung?></td>
                            </tr>
                            <tr>
                                <th>Status Gedung</th>
                                <td>
                                    <?php if($value->status_gedung == 0) : ?>
                                        <span class="label label-danger">False</span>
                                    <?php else: ?>
                                        <span class="label label-success">True</span>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <th>Date Created</th>
                                <td><?=$value->date_created?></td>
                            </tr>
                            <tr>
                                <th>Date Updated</th>
                                <td><?=$value->date_updated?></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
<?php endforeach; ?>
