<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Settings
            <small><i>Bobot Range</i></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-gears"></i> Settings</a></li>
            <li class="active"><i>Bobot Range</i></li>
        </ol>
    </section>
    <section class="content">

        <!-- notification -->
        <?php $this->load->view('notification') ?>

        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Tabel Setting Bobot Range</h3>
                <div class="pull-right">
                <div class="btn-group">
                    <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-gears">&nbsp;</i> <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="#" data-toggle="modal" data-target="#tambah">Tambah</a></li>
                        <li><a href="#" data-toggle="modal" data-target="#generate">Generate Range</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="<?=base_url('Settings/BobotRange/Simpan/')?>" onclick="return confirm('Apakah Anda Yakin ?')">Simpan Setting</a></li>
                    </ul>
                </div>
                </div>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table id="dataTable" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Action</th>
                            <th>Kategori</th>
                            <th>Rentang</th>
                            <th>Jalur</th>
                            <th>Tahun</th>
                            <th>Date Created</th>
                            <th>Date Updated</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $no=1; foreach ($tblSBobotRange as $value): ?>
                            <tr>
                                <td><?=$no++?></td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-gear">&nbsp;</i> <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#" data-toggle="modal" data-target="#edit-<?=$value->id_bobot_range?>">Edit</a></li>
                                            <li role="separator" class="divider"></li>
                                            <li><a href="<?=base_url('Settings/BobotRange/Delete/'.$value->id_bobot_range)?>" onclick="return confirm('Apakah Anda Yakin ?')">Delete</i></a></li>
                                        </ul>
                                    </div>
                                </td>
                                <td><?=$value->kategori?></td>
                                <td class="text-center"><?=$value->nilai_min.' - '.$value->nilai_max?></td>
                                <td><?=$value->jalur?></td>
                                <td><?=$value->tahun?></td>
                                <td><?=$value->date_created?></td>
                                <td><?=$value->date_updated?></td>
                            </tr>
                        <?php endforeach ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Action</th>
                            <th>Kategori</th>
                            <th>Rentang</th>
                            <th>Jalur</th>
                            <th>Tahun</th>
                            <th>Date Created</th>
                            <th>Date Updated</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </section>
</div>