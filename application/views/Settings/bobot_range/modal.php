<!-- Tambah -->
<div class="modal fade" id="tambah">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Tambah Range UKT</h4>
            </div>
            <?=form_open('Settings/BobotRange/Create/')?>
            <div class="modal-body">
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Kategori :</label>
                    <select class="form-control" name="kategori">
                        <?php for ($i = 1; $i <= 7; $i++): $k = 'K'.$i; ?>
                            <option value="<?=$k?>"><?=$k?></option>
                        <?php endfor; ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Nilai Minimum :</label>
                    <input type="text" name="nilai_min" class="form-control" placeholder="Nilai Minimum" required="">
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Nilai Maksimal :</label>
                    <input type="text" name="nilai_max" class="form-control" placeholder="Nilai Maksimal" required="">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
            <?=form_close()?>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- Ganerate -->
<div class="modal fade" id="generate">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Generate Range UKT</h4>
            </div>
            <?=form_open('Settings/BobotRange/Generate/')?>
            <div class="modal-body">
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Jalur :</label>
                    <select name="jalur" class="form-control">
                        <?php foreach ($tblSJalurMasuk as $value): ?>
                            <option value="<?=$value->jalur_masuk?>"><?=$value->jalur_masuk?></option>
                        <?php endforeach ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Tahun :</label>
                    <select name="tahun" class="form-control">
                        <?php foreach ($tahun as $value): ?>
                            <option value="<?=$value->tahun?>"><?=$value->tahun?></option>
                        <?php endforeach ?>
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Generate</button>
            </div>
            <?=form_close()?>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- Edit -->
<?php foreach ($tblSBobotRange as $value): ?>
<div class="modal fade" id="edit-<?=$value->id_bobot_range?>">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Edit Setting Range UKT</h4>
            </div>
            <?=form_open('Settings/BobotRange/Update/'.$value->id_bobot_range)?>
            <div class="box-body">
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Kategori :</label>
                    <select class="form-control" name="kategori">
                        <?php for ($i = 1; $i <= 7; $i++): $k = 'K'.$i; ?>
                            <?php if ($k == $value->kategori) : ?>
                                <option value="<?=$k?>" selected=""><?=$k?></option>
                            <?php else: ?>
                                <option value="<?=$k?>"><?=$k?></option>
                            <?php endif; ?>
                        <?php endfor; ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Nilai Minimum :</label>
                    <input type="text" name="nilai_min" class="form-control" placeholder="Nilai Minimum" value="<?=$value->nilai_min?>" required="">
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Nilai Maksimal :</label>
                    <input type="text"  name="nilai_max" class="form-control" placeholder="Nilai Maksimal" value="<?=$value->nilai_max?>" required="">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Update</button>
            </div>
            <?=form_close()?>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<?php endforeach; ?>
<!-- /.modal -->