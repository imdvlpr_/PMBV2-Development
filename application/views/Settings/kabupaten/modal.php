<!-- Tambah -->
<div class="modal fade" id="tambah">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Tambah Kabupaten / Kota</h4>
            </div>
            <?=form_open('Settings/Kabupaten/Create/')?>
            <div class="modal-body">
                <div class="form-group">
                    <label class="control-label">Provinsi :</label>
                    <select name="id_provinsi" class="form-control" required="">
                        <?php foreach ($tblSProvinsi as $value): ?>
                            <option value="<?=$value->id_provinsi?>"><?='('.$value->id_provinsi.') '.$value->provinsi?></option>
                        <?php endforeach ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">ID Kabupaten / Kota :</label>
                    <input type="text" name="id_kabupaten" class="form-control" placeholder="ID Kabupaten" required="">
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Kabupaten / Kota:</label>
                    <input type="text" name="kabupaten" class="form-control" placeholder="Kabupaten" required="">
                </div>
                <div class="form-group">
                    <label class="control-label">Jenis Daerah :</label>
                    <select name="id_jenis_daerah" class="form-control" required="">
                        <?php foreach ($tblSJDaerah as $value): ?>
                            <option value="<?=$value->id_jenis_daerah?>"><?=$value->jenis_daerah?></option>
                        <?php endforeach ?>
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
            <?=form_close()?>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- Edit -->
<?php $no=1; foreach ($viewSKabupaten as $value): ?>
<div class="modal fade" id="edit-<?=$value->id_kabupaten?>">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Edit Kabupaten</h4>
            </div>
            <?=form_open('Settings/Kabupaten/Update/'.$value->id_kabupaten)?>
            <div class="modal-body">
                <div class="form-group">
                    <label class="control-label">Provinsi :</label>
                    <select name="id_provinsi" class="form-control" required="">
                        <?php foreach ($tblSProvinsi as $a): ?>
                            <?php if ($a->id_provinsi == $value->id_provinsi): ?>
                                <option value="<?=$a->id_provinsi?>" selected=""><?='('.$a->id_provinsi.') '.$a->provinsi?></option>
                            <?php else: ?>
                                <option value="<?=$a->id_provinsi?>"><?='('.$a->id_provinsi.') '.$a->provinsi?></option>
                            <?php endif; ?>
                        <?php endforeach ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">ID Kabupaten / Kota :</label>
                    <input type="text" name="id_kabupaten" class="form-control" placeholder="ID Kabupaten" value="<?=$value->id_kabupaten?>" required="">
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Kabupaten / Kota:</label>
                    <input type="text" name="kabupaten" class="form-control" placeholder="Kabupaten" value="<?=$value->kabupaten?>" required="">
                </div>
                <div class="form-group">
                    <label class="control-label">Jenis Daerah :</label>
                    <select name="id_jenis_daerah" class="form-control" required="">
                        <?php foreach ($tblSJDaerah as $a): ?>
                            <?php if ($a->id_jenis_daerah == $value->id_jenis_daerah): ?>
                                <option value="<?=$a->id_jenis_daerah?>" selected=""><?=$a->jenis_daerah?></option>
                            <?php else: ?>
                                <option value="<?=$a->id_jenis_daerah?>"><?=$a->jenis_daerah?></option>
                            <?php endif; ?>
                        <?php endforeach ?>
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Update</button>
            </div>
            <?=form_close()?>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<?php endforeach; ?>
<!-- /.modal -->