<!-- Tambah -->
<div class="modal fade" id="tambah">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Tambah Fakultas</h4>
            </div>
            <?=form_open('Settings/Fakultas/Create/')?>
            <div class="modal-body">
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Fakultas :</label>
                    <input type="text" name="fakultas" class="form-control" placeholder="Fakultas" required="">
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Potongan :</label>
                    <input type="number" name="potongan" min="0" max="100" class="form-control" placeholder="Target" required="">
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Status :</label>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <input type="radio" name="status" class="forn-control" value="0" checked>
                                </span>
                                <input type="text" class="form-control" aria-label="False" value="False" readonly>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <input type="radio" name="status" class="forn-control" value="1">
                                </span>
                                <input type="text" class="form-control" aria-label="True" value="True" readonly>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
            <?=form_close()?>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- Generate -->
<div class="modal fade" id="generate">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Generate Pendapatan & Target Fakultas</h4>
            </div>
            <?=form_open('Settings/Fakultas/Generate/')?>
            <div class="modal-body">
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Tahun :</label>
                    <select class="form-control" name="tahun">
                        <?php foreach ($tahun as $a) : ?>
                            <option value="<?=$a->tahun?>"><?=$a->tahun?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Generate</button>
            </div>
            <?=form_close()?>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


<?php foreach ($tblSFakultas as $value): ?>
    <!-- Edit -->
    <div class="modal fade" id="edit-<?=$value->id_fakultas?>">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Edit Fakultas</h4>
                </div>
                <?=form_open('Settings/Fakultas/Update/'.$value->id_fakultas)?>
                <div class="box-body">
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Fakultas :</label>
                        <input type="text" name="fakultas" class="form-control" placeholder="Fakultas" required="" value="<?=$value->fakultas;?>">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Potongan :</label>
                        <input type="number" name="potongan" min="0" max="100" class="form-control" placeholder="Target" value="<?=($value->potongan*100);?>" required="">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Status :</label>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <input type="radio" name="status" class="forn-control" value="0" <?php if ($value->status == 0){echo 'checked';}?>>
                                    </span>
                                    <input type="text" class="form-control" aria-label="False" value="False" readonly>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <input type="radio" name="status" class="forn-control" value="1" <?php if ($value->status == 1){echo 'checked';}?>>
                                    </span>
                                    <input type="text" class="form-control" aria-label="True" value="True" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
                <?=form_close()?>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <!-- Detail -->
    <div class="modal fade" id="detail-<?=$value->id_fakultas?>">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Detail Fakultas</h4>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <tr>
                                <th>Fakultas</th>
                                <td><?=$value->fakultas?></td>
                            </tr>
                            <tr>
                                <th>Pendapatan</th>
                                <td>Rp <?=number_format($value->pendapatan,2)?></td>
                            </tr>
                            <tr>
                                <th>Potongan</th>
                                <td><?=$value->potongan*100?> %</td>
                            </tr>
                            <tr>
                                <th>Target</th>
                                <td>Rp <?=number_format($value->target,2)?></td>
                            </tr>
                            <tr>
                                <th>Tahun</th>
                                <td><?=$value->tahun?></td>
                            </tr>
                            <tr>
                                <th>Status</th>
                                <td>
                                    <?php if($value->status == 0) : ?>
                                        <span class="label label-danger">False</span>
                                    <?php else: ?>
                                        <span class="label label-success">True</span>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <th>Date Created</th>
                                <td><?=$value->date_created?></td>
                            </tr>
                            <tr>
                                <th>Date Updated</th>
                                <td><?=$value->date_updated?></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
<?php endforeach; ?>