<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Settings
            <small><i>Fakultas</i></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-gears"></i> Settings</a></li>
            <li class="active"><i>Fakultas</i></li>
        </ol>
    </section>
    <section class="content">

        <!-- notification -->
        <?php $this->load->view('notification') ?>

        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Tabel Setting Fakultas</h3>
                <div class="pull-right">
                    <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#generate" title="Generate">
                        Generate
                    </button>
                    <button type="button" class="btn btn-primary btn-sm" id="tambah_tooltip" data-toggle="modal" data-target="#tambah" title="Tambah">
                        <i class="fa fa-plus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table id="dataTable" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Action</th>
                        <th>Fakultas</th>
                        <th>Status</th>
                        <th>Date Created</th>
                        <th>Date Updated</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $no=1; foreach ($tblSFakultas as $value): ?>
                        <tr>
                            <td><?=$no++?></td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-gear"></i> <span class="caret"></span>
                                        </button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" data-toggle="modal" data-target="#detail-<?=$value->id_fakultas?>">Detail</a></li>
                                        <li><a href="#" data-toggle="modal" data-target="#edit-<?=$value->id_fakultas?>">Edit</a></li>
                                        <li role="separator" class="divider"></li>
                                        <li><a href="<?=base_url('Settings/Fakultas/Delete/'.$value->id_fakultas)?>" onclick="return confirm('Apakah Anda Yakin ?')">Delete</i></a></li>
                                    </ul>
                                </div>
                            </td>
                            <td><?=$value->fakultas?></td>
                            <td class="text-center">
                                <?php if($value->status == 0) : ?>
                                    <span class="label label-danger">False</span>
                                <?php else: ?>
                                    <span class="label label-success">True</span>
                                <?php endif; ?>
                            </td>
                            <td><?=$value->date_created?></td>
                            <td><?=$value->date_updated?></td>
                        </tr>
                    <?php endforeach ?>
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>No</th>
                        <th>Action</th>
                        <th>Fakultas</th>
                        <th>Status</th>
                        <th>Date Created</th>
                        <th>Date Updated</th>
                    </tr>
                    </tfoot>
                </table>
                </div>
            </div>
        </div>
    </section>
</div>