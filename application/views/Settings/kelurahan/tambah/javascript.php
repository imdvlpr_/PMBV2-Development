<script type="text/javascript">
    $(document).ready(function(){
        $("#loading1").hide();

        $("#prov").change(function(){
            $("#kab").hide();
            $("#loading1").show();

            $.ajax({
                type: "POST",
                url: "<?=base_url("Settings/Kecamatan/listKabupaten/"); ?>",
                data: {id_provinsi : $("#prov").val()},
                dataType: "json",
                beforeSend: function(e) {
                    if(e && e.overrideMimeType) {
                        e.overrideMimeType("application/json;charset=UTF-8");
                    }
                },
                success: function(response){
                    $("#loading1").hide();
                    $("#kab").html(response.list_kabupaten).show();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError);
                }
            });
        });

    });
</script>