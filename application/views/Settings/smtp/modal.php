<!-- Tambah -->
<div class="modal fade" id="tambah">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Tambah SMTP</h4>
            </div>
            <?=form_open('Settings/SMTP/Create/')?>
            <div class="modal-body">
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Email :</label>
                    <input type="email" name="username" class="form-control" placeholder="Email" required="">
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Password :</label>
                    <input type="text" name="password" class="form-control" placeholder="Password" required="">
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Email Quota :</label>
                    <input type="number" min="0" name="quota" class="form-control" placeholder="Email Quota" required="">
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Status :</label>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <input type="radio" name="status" class="forn-control" value="0" checked>
                                </span>
                                <input type="text" class="form-control" aria-label="False" value="False" readonly>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <input type="radio" name="status" class="forn-control" value="1">
                                </span>
                                <input type="text" class="form-control" aria-label="True" value="True" readonly>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
            <?=form_close()?>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- Edit -->
<?php $no=1; foreach ($tblSSmtp as $value): ?>
<div class="modal fade" id="edit-<?=$value->id_smtp?>">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Edit SMTP</h4>
            </div>
            <?=form_open('Settings/SMTP/Update/'.$value->id_smtp)?>
            <div class="box-body">
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Username :</label>
                    <input type="email" name="username" class="form-control" placeholder="Username" required="" value="<?=$value->username;?>">
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Password :</label>
                    <input type="text" name="password" class="form-control" placeholder="Password" required="" value="<?=$value->password;?>">
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Email Quota :</label>
                    <input type="number" min="0" name="quota" class="form-control" placeholder="Email Quota" required="" value="<?=$value->quota;?>">
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Status :</label>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <input type="radio" name="status" class="forn-control" value="0" <?php if ($value->status == 0){echo 'checked';}?>>
                                </span>
                                <input type="text" class="form-control" aria-label="False" value="False" readonly>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <input type="radio" name="status" class="forn-control" value="1" <?php if ($value->status == 1){echo 'checked';}?>>
                                </span>
                                <input type="text" class="form-control" aria-label="True" value="True" readonly>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Update</button>
            </div>
            <?=form_close()?>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<?php endforeach; ?>
<!-- /.modal -->