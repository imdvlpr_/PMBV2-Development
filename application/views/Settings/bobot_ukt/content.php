<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Settings
            <small><i>Bobot UKT</i></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-gears"></i> Settings</a></li>
            <li class="active"><i>Bobot UKT</i></li>
        </ol>
    </section>
    <section class="content">

        <!-- notification -->
        <?php $this->load->view('notification') ?>

        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Tabel Setting Bobot UKT</h3>
                <div class="pull-right">
                    <div class="btn-group">
                        <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-gears">&nbsp;</i> <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li><a href="#" data-toggle="modal" data-target="#tambah">Tambah</a></li>
                            <li><a href="#" data-toggle="modal" data-target="#generate">Generate Nilai</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="<?=base_url('Settings/BobotUkt/Simpan/')?>" onclick="return confirm('Apakah Anda Yakin ?')">Simpan Setting</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table id="dataTable" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Action</th>
                            <th>Nama Field</th>
                            <th>Alias</th>
                            <th>Bobot</th>
                            <th>Nilai Maksimal</th>
                            <th>Jalur</th>
                            <th>Tahun</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $no=1; $jumlah = 0; foreach ($tblSBobotUkt as $value): $jumlah += $value->bobot;?>
                            <tr>
                                <td><?=$no++?></td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-gear">&nbsp;</i> <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#" data-toggle="modal" data-target="#edit-<?=$value->id_bobot_ukt?>">Edit</a></li>
                                            <li role="separator" class="divider"></li>
                                            <li><a href="<?=base_url('Settings/BobotUkt/Delete/'.$value->id_bobot_ukt)?>" onclick="return confirm('Apakah Anda Yakin ?')">Delete</i></a></li>
                                        </ul>
                                    </div>
                                </td>
                                <td><?=$value->nama_field?></td>
                                <td><?=$value->alias?></td>
                                <td><?=$value->bobot?></td>
                                <td><?=$value->nilai_max?></td>
                                <td><?=$value->jalur?></td>
                                <td><?=$value->tahun?></td>
                            </tr>
                        <?php endforeach ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Action</th>
                            <th>Nama Field</th>
                            <th>Alias</th>
                            <th>Bobot</th>
                            <th>Nilai Maksimal</th>
                            <th>Jalur</th>
                            <th>Tahun</th>
                        </tr>
                        </tfoot>
                    </table>
                    <p>Jumlah Bobot : <?=$jumlah?></p>
                </div>
            </div>
        </div>
    </section>
</div>