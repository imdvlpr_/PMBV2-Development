<!-- Tambah -->
<div class="modal fade" id="tambah">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Tambah Bobot UKT</h4>
            </div>
            <?=form_open('Settings/BobotUkt/Create/')?>
            <div class="modal-body">
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Nama Field :</label>
                    <input type="text" name="nama_field" class="form-control" placeholder="Nama Field" required="">
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Alias :</label>
                    <input type="text" name="alias" class="form-control" placeholder="Alias" required="">
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Bobot :</label>
                    <input type="text" name="bobot" class="form-control" placeholder="Bobot" required="">
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Nilai Maksimum :</label>
                    <input type="number" name="nilai_max" min="0" class="form-control" placeholder="Nilai Maksimum" required="">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
            <?=form_close()?>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- Ganerate -->
<div class="modal fade" id="generate">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Generate Nilai Maksimal</h4>
            </div>
            <?=form_open('Settings/BobotUkt/Generate/')?>
            <div class="modal-body">
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Jalur :</label>
                    <select name="jalur" class="form-control">
                        <?php foreach ($tblSJalurMasuk as $value): ?>
                            <option value="<?=$value->jalur_masuk?>"><?=$value->jalur_masuk?></option>
                        <?php endforeach ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Tahun :</label>
                    <select name="tahun" class="form-control">
                        <?php foreach ($tahun as $value): ?>
                            <option value="<?=$value->tahun?>"><?=$value->tahun?></option>
                        <?php endforeach ?>
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Generate</button>
            </div>
            <?=form_close()?>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- Edit -->
<?php foreach ($tblSBobotUkt as $value): ?>
<div class="modal fade" id="edit-<?=$value->id_bobot_ukt?>">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Edit Setting Bobot UKT</h4>
            </div>
            <?=form_open('Settings/BobotUkt/Update/'.$value->id_bobot_ukt)?>
            <div class="box-body">
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Nama Field :</label>
                    <input type="text" name="nama_field" class="form-control" placeholder="Nama Field" required="" value="<?=$value->nama_field?>">
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Alias :</label>
                    <input type="text" name="alias" class="form-control" placeholder="Alias" required="" value="<?=$value->alias?>">
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Bobot :</label>
                    <input type="text" name="bobot" class="form-control" placeholder="Bobot" required="" value="<?=$value->bobot?>">
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Nilai Maksimum :</label>
                    <input type="number" name="nilai_max" min="0" class="form-control" placeholder="Nilai Maksimum" required="" value="<?=$value->nilai_max?>">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Update</button>
            </div>
            <?=form_close()?>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<?php endforeach; ?>
<!-- /.modal -->