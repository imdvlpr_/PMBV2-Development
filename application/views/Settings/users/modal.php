<!-- Tambah -->
<div class="modal fade" id="tambah">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Tambah Users</h4>
            </div>
            <?=form_open_multipart('Settings/Users/Create/')?>
            <div class="modal-body">
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Username :</label>
                    <input type="text" name="username" class="form-control" placeholder="Username" required="">
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Password :</label>
                    <input type="password" name="password" class="form-control" placeholder="Password" required="">
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Nama :</label>
                    <input type="text" name="nama" class="form-control" placeholder="Nama" required="">
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Level :</label>
                    <select name="level" id="level" class="form-control">
                        <option value="AKADEMIK">AKADEMIK</option>
                        <option value="DEVELOPMENT">DEVELOPMENT</option>
                        <option value="EKSEKUTIF">EKSEKUTIF</option>
                        <option value="KEUANGAN">KEUANGAN</option>
                        <option value="KEMAHASISWAAN">KEMAHASISWAAN</option>
                        <option value="FAKULTAS">FAKULTAS</option>
                    </select>
                </div>
                <div class="form-group" id="fakultas">
                    <label for="recipient-name" class="control-label">Fakultas :</label>
                    <select name="id_fakultas" class="form-control">
                        <?php foreach ($tblSFakultas as $value):
                            if ($value->id_fakultas != 99) : ?>
                                <option value="<?=$value->id_fakultas?>"><?=$value->fakultas?></option>
                            <?php endif; endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
            <?=form_close()?>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<?php foreach ($tblSUsers as $value): ?>
    <!-- Detail -->
    <div class="modal fade" id="detail-<?=$value->id_users?>">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Detail Users</h4>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <tr>
                                <th>Username</th>
                                <td><?=$value->username?></td>
                            </tr>
                            <tr>
                                <th>Nama</th>
                                <td><?=$value->nama?></td>
                            </tr>
                            <tr>
                                <th>Level</th>
                                <td><?=$value->level?></td>
                            </tr>
                            <tr>
                                <th>Keterangan</th>
                                <td><?=$value->keterangan?></td>
                            </tr>
                            <tr>
                                <th>Login</th>
                                <td><?=$value->login?></td>
                            </tr>
                            <tr>
                                <th>Logout</th>
                                <td><?=$value->logout?></td>
                            </tr>
                            <tr>
                                <th>IP Login</th>
                                <td><?=$value->ip_login?></td>
                            </tr>
                            <tr>
                                <th>IP Logout</th>
                                <td><?=$value->ip_logout?></td>
                            </tr>
                            <tr>
                                <th>Date Created</th>
                                <td><?=$value->date_created?></td>
                            </tr>
                            <tr>
                                <th>Date Updated</th>
                                <td><?=$value->date_updated?></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <!-- Edit -->
    <div class="modal fade" id="edit-<?=$value->id_users?>">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Edit Users</h4>
                </div>
                <?=form_open('Settings/Users/Update/'.$value->id_users)?>
                <div class="box-body">
                    <?php $data = explode('@', $value->username); ?>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Username :</label>
                        <input type="text" name="username" class="form-control" placeholder="Username" required="" value="<?=$data[0]?>">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Password :</label>
                        <input type="password" name="password" class="form-control" placeholder="Password">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Nama :</label>
                        <input type="text" name="nama" class="form-control" placeholder="Nama" required="" value="<?=$value->nama?>">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Level :</label>
                        <select name="level" id="level" class="form-control">
                            <option value="AKADEMIK" <?php if($value->level=="AKADEMIK"){ echo 'selected'; } ?>>AKADEMIK</option>
                            <option value="DEVELOPMENT" <?php if($value->level=="DEVELOPMENT"){ echo 'selected'; } ?>>DEVELOPMENT</option>
                            <option value="EKSEKUTIF" <?php if($value->level=="EKSEKUTIF"){ echo 'selected'; } ?>>EKSEKUTIF</option>
                            <option value="KEUANGAN" <?php if($value->level=="KEUANGAN"){ echo 'selected'; } ?>>KEUANGAN</option>
                            <option value="KEMAHASISWAAN" <?php if($value->level=="KEMAHASISWAAN"){ echo 'selected'; } ?>>KEMAHASISWAAN</option>
                            <option value="FAKULTAS" <?php if($value->level=="FAKULTAS"){ echo 'selected'; } ?>>FAKULTAS</option>
                        </select>
                    </div>
                    <div class="form-group" id="fakultas">
                        <label for="recipient-name" class="control-label">Fakultas :</label>
                        <select name="id_fakultas" class="form-control">
                            <?php $ket = json_decode($value->keterangan); foreach ($tblSFakultas as $value):
                                if ($value->id_fakultas != 99) :
                                    if ($value->id_fakultas == $ket->id_fakultas) : ?>
                                        <option value="<?=$value->id_fakultas?>" selected><?=$value->fakultas?></option>
                                    <?php else: ?>
                                        <option value="<?=$value->id_fakultas?>"><?=$value->fakultas?></option>
                                    <?php endif; endif; endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
                <?=form_close()?>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
<?php endforeach; ?>
