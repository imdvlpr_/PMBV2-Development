<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>PMB - UIN Sunan Gunung Djati Bandung</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Icon -->
        <link href="<?=base_url('design-backend/images/logo.png'); ?>" rel="icon" type="image/x-icon" />
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="<?=base_url('design-backend/bootstrap/css/bootstrap.min.css');?>">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?=base_url('design-backend/dist/css/AdminLTE.min.css');?>">
        <link rel="stylesheet" href="<?=base_url('design-backend/dist/css/skins/skin-red-light.min.css');?>">
    </head>

    <body class="hold-transition skin-red-light sidebar-mini">
        <div class="wrapper">
            <?php $this->load->view('header-navbar');?>
            <?php $this->load->view('header-sidebar');?>
            <div class="content-wrapper">
                <section class="content-header">
                    <h1>
                        Settings
                        <small><i>Jurusan</i></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-gears"></i> Settings</a></li>
                        <li class="active"><i>Jurusan</i></li>
                    </ol>
                </section>
                <section class="content">
                    <?php if (!empty($this->session->flashdata('type_message'))) { ?>
                        <div class="alert alert-<?=$this->session->flashdata('type_message')?> alert-dismissible text-center" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <?=$this->session->flashdata('message');?>
                        </div>
                    <?php } ?>
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit Tabel Setting Jurusan</h3>
                        </div>
                        <?php if ($tbSJurusan->kode_jurusan != 999) { ?>
                            <?=form_open('Settings/Jurusan/Update/'.$tbSJurusan->kode_jurusan)?>
                            <div class="box-body">
                                <div class="form-group">
                                    <label class="control-label">Kode Jurusan :</label>
                                    <input type="number" min="0" name="kode_jurusan" class="form-control" placeholder="Kode Jurusan" required="" value="<?=$tbSJurusan->kode_jurusan?>">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Jurusan :</label>
                                    <input type="text" name="jurusan" class="form-control" placeholder="Jurusan" required="" value="<?=$tbSJurusan->jurusan?>">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Kategori :</label>
                                    <div class="form-group">
                                        <?php if ($tbSJurusan->kategori == 1) { ?>
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="kategori" value="1" checked> IPA / SAINS
                                                </label>
                                            </div>
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="kategori" value="2"> IPS / SOSIAL DAN HUMANIORA
                                                </label>
                                            </div>
                                        <?php }else{ ?>
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="kategori" value="1"> IPA / SAINS
                                                </label>
                                            </div>
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="kategori" value="2" checked> IPS / SOSIAL DAN HUMANIORA
                                                </label>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Grade :</label>
                                    <input type="number" min="0" name="grade" class="form-control" placeholder="grade" required="" value="<?=$tbSJurusan->grade?>">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Fakultas :</label>
                                    <select name="id_fakultas" class="form-control" required="">
                                        <?php foreach ($tbSFakultas as $row): ?>
                                            <?php if ($row->id_fakultas != 999) { ?>
                                                <?php if ($tbSJurusan->id_fakultas == $row->id_fakultas) { ?>
                                                    <option value="<?=$row->id_fakultas?>" selected=""><?=$row->fakultas?></option>
                                                <?php }else{ ?>
                                                    <option value="<?=$row->id_fakultas?>"><?=$row->fakultas?></option>
                                                <?php } ?>
                                            <?php } ?>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">K1 :</label>
                                    <input type="text" name="k1" value="<?=$tbSJurusan->k1?>" class="form-control" placeholder="K1" required="">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">K2 :</label>
                                    <input type="text" name="k2" value="<?=$tbSJurusan->k2?>" class="form-control" placeholder="K2" required="">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">K3 :</label>
                                    <input type="text" name="k3" value="<?=$tbSJurusan->k3?>" class="form-control" placeholder="K3" required="">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">K4 :</label>
                                    <input type="text" name="k4" value="<?=$tbSJurusan->k4?>" class="form-control" placeholder="K4" required="">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">K5 :</label>
                                    <input type="text" name="k5" value="<?=$tbSJurusan->k5?>" class="form-control" placeholder="K5" required="">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">K6 :</label>
                                    <input type="text" name="k6" value="<?=$tbSJurusan->k6?>" class="form-control" placeholder="K6" required="">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">K7 :</label>
                                    <input type="text" name="k7" value="<?=$tbSJurusan->k7?>" class="form-control" placeholder="K7" required="">
                                </div>
                            </div>
                            <div class="box-footer">
                                <a href="<?=base_url('Settings/Jurusan');?>" class="btn btn-default">Kembali</a>
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                            <?=form_close()?>
                        <?php } ?>
                    </div>
                </section>
            </div>
            <?php $this->load->view('footer'); ?>
        </div>

        <!-- REQUIRED JS SCRIPTS -->
        <!-- jQuery 2.2.3 -->
        <script src="<?=base_url('design-backend/plugins/jQuery/jquery-2.2.3.min.js');?>"></script>
        <!-- Bootstrap 3.3.6 -->
        <script src="<?=base_url('design-backend/bootstrap/js/bootstrap.min.js');?>"></script>
        <!-- SlimScroll -->
        <script src="<?=base_url('design-backend/plugins/slimScroll/jquery.slimscroll.min.js')?>"></script>
        <!-- FastClick -->
        <script src="<?=base_url('design-backend/plugins/fastclick/fastclick.js')?>"></script>
        <!-- AdminLTE App -->
        <script src="<?=base_url('design-backend/dist/js/app.min.js');?>"></script>
    </body>
</html>
