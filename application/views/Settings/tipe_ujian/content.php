<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Settings
            <small><i>Tipe Ujian</i></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-gears"></i> Settings</a></li>
            <li class="active"><i>Tipe Ujian</i></li>
        </ol>
    </section>
    <section class="content">

        <!-- notification -->
        <?php $this->load->view('notification') ?>

        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Tabel Setting Tipe Ujian</h3>
                <div class="pull-right">
                    <button type="button" class="btn btn-primary" id="tambah_tooltip" data-toggle="modal" data-target="#tambah" title="Tambah">
                        <i class="fa fa-plus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table id="dataTable" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Action</th>
                            <th>ID Tipe Ujian</th>
                            <th>Tipe Ujian</th>
                            <th>Kode</th>
                            <th>Quota</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $no=1; foreach ($tblSTipeUjian as $value): ?>
                            <tr>
                                <td><?=$no?></td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-gear"></i> <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#" data-toggle="modal" data-target="#edit-<?=$value->id_tipe_ujian?>">Edit</a></li>
                                            <li><a href="<?=base_url('Settings/TipeUjian/Delete/'.$value->id_tipe_ujian)?>" onclick="return confirm('Apakah Anda Yakin ?')">Hapus</a></li>
                                        </ul>
                                    </div>
                                </td>
                                <td><?=$value->id_tipe_ujian?></td>
                                <td><?=$value->tipe_ujian?></td>
                                <td><?=$value->kode?></td>
                                <td><?=$value->quota?></td>
                                <td class="text-center">
                                    <?php if($value->status == 0) : ?>
                                        <span class="label label-danger">False</span>
                                    <?php else: ?>
                                        <span class="label label-success">True</span>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <?php $no++; endforeach ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Action</th>
                            <th>ID Tipe Ujian</th>
                            <th>Tipe Ujian</th>
                            <th>Kode</th>
                            <th>Quota</th>
                            <th>Status</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </section>
</div>