<!-- Tambah -->
<div class="modal fade" id="tambah">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Tambah Ruangan</h4>
            </div>
            <?=form_open_multipart('Settings/Ruangan/Create/')?>
            <div class="modal-body">
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Kampus : </label>
                    <select id="id_kampus" name="id_kampus" required="" class="form-control">
                        <option value="">Pilih</option>
                        <?php foreach ($tblSKampus as $value): ?>
                            <option value="<?=$value->id_kampus;?>"><?=$value->kampus;?></option>
                        <?php endforeach ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Gedung : </label>
                    <select id="id_gedung" name="id_gedung" required="" class="form-control">
                        <option value="">Pilih</option>
                    </select>
                    <div id="loading1"><small>Loading...</small></div>
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Ruangan :</label>
                    <input type="text" name="ruangan" class="form-control" placeholder="Ruangan" required="">
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Status :</label>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <input type="radio" name="status" class="forn-control" value="0" checked>
                                </span>
                                <input type="text" class="form-control" aria-label="False" value="False" readonly>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <input type="radio" name="status" class="forn-control" value="1">
                                </span>
                                <input type="text" class="form-control" aria-label="True" value="True" readonly>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
            <?=form_close()?>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->