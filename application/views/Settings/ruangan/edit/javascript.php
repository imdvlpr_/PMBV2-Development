<script type="text/javascript">
    $(function () {
        $("#loading1").hide()

        $("#id_kampus").change(function(){
            $("#id_gedung").hide()
            $("#loading1").show()

            $.ajax({
                type: "POST",
                url: "<?=base_url("Settings/Ruangan/listGedung/"); ?>",
                data: {id_kampus : $("#id_kampus").val()},
                dataType: "json",
                beforeSend: function(e) {
                    if(e && e.overrideMimeType) {
                        e.overrideMimeType("application/json;charset=UTF-8")
                    }
                },
                success: function(response){
                    $("#loading1").hide()
                    $("#id_gedung").html(response.list_gedung).show()
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError);
                }
            })
        })
    })
</script>