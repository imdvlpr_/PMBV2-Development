<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Edit
            <small><i>Settings Ruangan</i></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-gears"></i> Settings</a></li>
            <li><i>Ruangan</i></li>
            <li class="active"><i>Edit</i></li>
        </ol>
    </section>
    <section class="content">

        <!-- notification -->
        <?php $this->load->view('notification') ?>

        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Setting Ruangan</h3>
                    </div>
                    <?=form_open('Settings/Ruangan/Update/'.$viewSRuangan->id_ruangan)?>
                    <div class="box-body">
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Kampus : <?=$viewSRuangan->kampus?></label>
                            <select id="id_kampus" name="id_kampus" class="form-control">
                                <option value="">Pilih</option>
                                <?php foreach ($tblSKampus as $value): ?>
                                    <option value="<?=$value->id_kampus;?>"><?=$value->kampus;?></option>
                                <?php endforeach ?>
                            </select>
                            <span class="label label-warning">Pilih apabila ingin mengubah data sebelumnya.</span>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Gedung : <?=$viewSRuangan->gedung?></label>
                            <input type="hidden" name="old_gedung" value="<?=$viewSRuangan->id_gedung?>">
                            <select id="id_gedung" name="id_gedung" class="form-control">
                                <option value="">Pilih</option>
                            </select>
                            <div id="loading1"><small>Loading...</small></div>
                            <span class="label label-warning">Pilih apabila ingin mengubah data sebelumnya.</span>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Ruangan :</label>
                            <input type="text" name="ruangan" class="form-control" placeholder="Ruangan" required="" value="<?=$viewSRuangan->ruangan?>">
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Status :</label>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group">
                                <span class="input-group-addon">
                                    <input type="radio" name="status" class="forn-control" value="0" <?=($viewSRuangan->status_ruangan == 0)? 'checked' : ''?>>
                                </span>
                                        <input type="text" class="form-control" aria-label="False" value="False" readonly>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                <span class="input-group-addon">
                                    <input type="radio" name="status" class="forn-control" value="1" <?=($viewSRuangan->status_ruangan == 1)? 'checked' : ''?>>
                                </span>
                                        <input type="text" class="form-control" aria-label="True" value="True" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <a href="<?=base_url('Settings/Ruangan/')?>" class="btn btn-default">Back</a>
                        <button type="submit" class="btn btn-primary pull-right">Update</button>
                    </div>
                    <?=form_close()?>
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>

    </section>
</div>