<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>PMB - UIN Sunan Gunung Djati Bandung</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Icon -->
        <link href="<?=base_url('design-backend/images/logo.png'); ?>" rel="icon" type="image/x-icon" />
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="<?=base_url('design-backend/bootstrap/css/bootstrap.min.css');?>">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- DataTables -->
        <link rel="stylesheet" href="<?=base_url('design-backend/plugins/datatables/dataTables.bootstrap.css')?>">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?=base_url('design-backend/dist/css/AdminLTE.min.css');?>">
        <link rel="stylesheet" href="<?=base_url('design-backend/dist/css/skins/skin-red-light.min.css');?>">
    </head>

    <body class="hold-transition skin-red-light sidebar-mini">
        <div class="wrapper">
            <?php $this->load->view('header-navbar');?>
            <?php $this->load->view('header-sidebar');?>
            <div class="content-wrapper">
                <section class="content-header">
                    <h1>
                        Settings
                        <small><i>Kabupaten / Kota</i></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-gears"></i> Settings</a></li>
                        <li class="active"><i>Kabupaten / Kota</i></li>
                    </ol>
                </section>
                <section class="content">
                    <?php if (!empty($this->session->flashdata('type_message'))) { ?>
                        <div class="alert alert-<?=$this->session->flashdata('type_message')?> alert-dismissible text-center" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <?=$this->session->flashdata('message');?>
                        </div>
                    <?php } ?>
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Tabel Setting Kabupaten / Kota</h3>
                            <div class="pull-right">
                                <button type="button" class="btn btn-primary" id="tambah_tooltip" data-toggle="modal" data-target="#tambah" title="Tambah">
                                <i class="fa fa-plus"></i>
                            </button>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="table-responsive">
                                <table id="kabupaten" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Provinsi</th>
                                            <th>Kabupaten / Kota</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no=1; foreach ($viewProvinsiKabupaten as $value): ?>
                                        <tr>
                                            <td><?=$no?></td>
                                            <td><?=$value->provinsi?></td>
                                            <td><?=$value->kabupaten?></td>
                                            <td class="text-center">
                                                <div class="btn-group" role="group" aria-label="Action">
                                                    <a href="<?=base_url('Settings/Kabupaten/Edit/'.$value->id_kab)?>" class="btn btn-warning"><i class="fa fa-edit"></i></a>
                                                    <a href="<?=base_url('Settings/Kabupaten/Delete/'.$value->id_kab)?>" onclick="return confirm('Apakah Anda Yakin ?')" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php $no++; endforeach ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <?php $this->load->view('footer'); ?>
        </div>

        <!-- Tambah -->
        <div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Tambah Kabupaten / Kota</h4>
                    </div>
                    <?=form_open('Settings/Kabupaten/Tambah/')?>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Provinsi :</label>
                            <select name="id_prov" class="form-control" required="">
                                <?php foreach ($tbSProvinsi as $value) { ?>
                                    <option value="<?=$value->id_prov?>"><?=$value->nama?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Kabupaten / Kota :</label>
                            <input type="text" name="nama" class="form-control" placeholder="Kabupaten / Kota" required="">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                    <?=form_close()?>
                </div>
            </div>
        </div>

        <!-- REQUIRED JS SCRIPTS -->
        <!-- jQuery 2.2.3 -->
        <script src="<?=base_url('design-backend/plugins/jQuery/jquery-2.2.3.min.js');?>"></script>
        <!-- Bootstrap 3.3.6 -->
        <script src="<?=base_url('design-backend/bootstrap/js/bootstrap.min.js');?>"></script>
        <!-- DataTables -->
        <script src="<?=base_url('design-backend/plugins/datatables/jquery.dataTables.min.js')?>"></script>
        <script src="<?=base_url('design-backend/plugins/datatables/dataTables.bootstrap.min.js')?>"></script>
        <!-- SlimScroll -->
        <script src="<?=base_url('design-backend/plugins/slimScroll/jquery.slimscroll.min.js')?>"></script>
        <!-- FastClick -->
        <script src="<?=base_url('design-backend/plugins/fastclick/fastclick.js')?>"></script>
        <!-- AdminLTE App -->
        <script src="<?=base_url('design-backend/dist/js/app.min.js');?>"></script>
        <!-- page script -->
        <script>
            $(function () {
                $('#kabupaten').DataTable({
                    "paging": true,
                    "lengthChange": true,
                    "searching": true,
                    "ordering": true,
                    "info": true,
                    "autoWidth": true
                });
            });
        </script>
        <script type="text/javascript">
            $('#tambah_tooltip').tooltip();
        </script>
    </body>
</html>
