<!-- Tambah -->
<div class="modal fade" id="tambah">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Tambah Provinsi</h4>
            </div>
            <?=form_open('Settings/Provinsi/Create/')?>
            <div class="modal-body">
                <div class="form-group">
                    <label for="recipient-name" class="control-label">ID Provinsi :</label>
                    <input type="text" name="id_provinsi" class="form-control" placeholder="ID Provinsi" required="">
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Provinsi :</label>
                    <input type="text" name="provinsi" class="form-control" placeholder="Provinsi" required="">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
            <?=form_close()?>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- Edit -->
<?php $no=1; foreach ($viewSProvinsi as $value): ?>
<div class="modal fade" id="edit-<?=$value->id_provinsi?>">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Edit Provinsi</h4>
            </div>
            <?=form_open('Settings/Provinsi/Update/'.$value->id_provinsi)?>
            <div class="modal-body">
                <div class="form-group">
                    <label for="recipient-name" class="control-label">ID Provinsi :</label>
                    <input type="text" name="id_provinsi" class="form-control" placeholder="ID Provinsi" value="<?=$value->id_provinsi?>" required="">
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Provinsi :</label>
                    <input type="text" name="provinsi" class="form-control" placeholder="Provinsi" value="<?=$value->   provinsi?>" required="">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Update</button>
            </div>
            <?=form_close()?>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<?php endforeach; ?>
<!-- /.modal -->