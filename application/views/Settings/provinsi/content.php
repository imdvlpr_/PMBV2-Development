<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Settings
            <small><i>Provinsi</i></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-gears"></i> Settings</a></li>
            <li class="active"><i>Provinsi</i></li>
        </ol>
    </section>
    <section class="content">

        <!-- notification -->
        <?php $this->load->view('notification') ?>

        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Tabel Setting Provinsi</h3>
                <div class="pull-right">
                    <button type="button" class="btn btn-sm btn-primary" id="tambah_tooltip" data-toggle="modal" data-target="#tambah" title="Tambah">
                        <i class="fa fa-plus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table id="dataTable" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Action</th>
                            <th>ID Provinsi</th>
                            <th>Provinsi</th>
                            <th>Jenis Daerah</th>
                            <th>Date Created</th>
                            <th>Date Updated</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $no=1; foreach ($viewSProvinsi as $value): ?>
                            <tr>
                                <td><?=$no++?></td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-gear"></i> <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#" data-toggle="modal" data-target="#edit-<?=$value->id_provinsi?>">Edit</a></li>
                                            <li role="separator" class="divider"></li>
                                            <li><a href="<?=base_url('Settings/Provinsi/Delete/'.$value->id_provinsi)?>" onclick="return confirm('Apakah Anda Yakin ?')">Hapus</a></li>
                                        </ul>
                                    </div>
                                </td>
                                <td><?=$value->id_provinsi?></td>
                                <td><?=$value->provinsi?></td>
                                <td><?=$value->jenis_daerah?></td>
                                <td><?=$value->date_created?></td>
                                <td><?=$value->date_updated?></td>
                            </tr>
                            <?php endforeach ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Action</th>
                            <th>ID Provinsi</th>
                            <th>Provinsi</th>
                            <th>Jenis Daerah</th>
                            <th>Date Created</th>
                            <th>Date Updated</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </section>
</div>