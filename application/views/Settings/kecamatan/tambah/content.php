<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Tambah
            <small><i>Settings Kecamatan</i></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-gears"></i> Settings</a></li>
            <li><i>Kecamatan</i></li>
            <li class="active"><i>Tambah</i></li>
        </ol>
    </section>
    <section class="content">

        <!-- notification -->
        <?php $this->load->view('notification') ?>

        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Tambah Setting Kecamatan</h3>
                    </div>
                    <?=form_open('Settings/Kecamatan/Create/')?>
                    <div class="box-body">
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Provinsi : </label>
                            <select id="prov" name="id_provinsi" required="" class="form-control">
                                <option value="">Pilih</option>
                                <?php foreach ($tblSProvinsi as $value): ?>
                                    <option value="<?=$value->id_provinsi;?>"><?=$value->provinsi;?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Kota / Kabupaten : </label>
                            <select id="kab" name="id_kabupaten" required="" class="form-control">
                                <option value="">Pilih</option>
                            </select>
                            <div id="loading1"><small>Loading...</small></div>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Kecamatan :</label>
                            <input type="text" name="kecamatan" class="form-control" placeholder="Kecamatan" required="">
                        </div>
                    </div>
                    <div class="box-footer">
                        <a href="<?=base_url('Settings/Kecamatan/')?>" class="btn btn-default">Back</a>
                        <button type="submit" class="btn btn-primary pull-right">Save</button>
                    </div>
                    <?=form_close()?>
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>

    </section>
</div>