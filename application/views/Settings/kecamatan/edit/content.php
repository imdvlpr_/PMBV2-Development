<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Edit
            <small><i>Settings Kecamatan</i></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-gears"></i> Settings</a></li>
            <li><i>Kecamatan</i></li>
            <li class="active"><i>Edit</i></li>
        </ol>
    </section>
    <section class="content">

        <!-- notification -->
        <?php $this->load->view('notification') ?>

        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Setting Kecamatan</h3>
                    </div>
                    <?=form_open('Settings/Kecamatan/Update/'.$viewSKecamatan->id_kecamatan)?>
                    <div class="box-body">
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Provinsi : <?=$viewSKecamatan->provinsi?></label>
                            <select id="prov" name="id_provinsi" class="form-control">
                                <option value="">Pilih</option>
                                <?php foreach ($tblSProvinsi as $value): ?>
                                    <option value="<?=$value->id_provinsi;?>"><?=$value->provinsi;?></option>
                                <?php endforeach ?>
                            </select>
                            <span class="label label-warning">Pilih apabila ingin mengubah data sebelumnya.</span>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Kota / Kabupaten : <?=$viewSKecamatan->kabupaten?></label>
                            <input type="hidden" name="old_kabupaten" value="<?=$viewSKecamatan->id_kabupaten?>">
                            <select id="kab" name="id_kabupaten" class="form-control">
                                <option value="">Pilih</option>
                            </select>
                            <div id="loading1"><small>Loading...</small></div>
                            <span class="label label-warning">Pilih apabila ingin mengubah data sebelumnya.</span>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">ID Kecamatan :</label>
                            <input type="text" name="id_kecamatan" class="form-control" placeholder="ID Kecamatan" value="<?=$viewSKecamatan->id_kecamatan?>" required="">
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Kecamatan :</label>
                            <input type="text" name="kecamatan" class="form-control" placeholder="Kecamatan" value="<?=$viewSKecamatan->kecamatan?>" required="">
                        </div>
                    </div>
                    <div class="box-footer">
                        <a href="<?=base_url('Settings/Kecamatan/')?>" class="btn btn-default">Back</a>
                        <button type="submit" class="btn btn-primary pull-right">Save</button>
                    </div>
                    <?=form_close()?>
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>

    </section>
</div>