<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Settings
            <small><i>Kecamatan</i></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-gears"></i> Settings</a></li>
            <li class="active"><i>Kecamatan</i></li>
        </ol>
    </section>

    <section class="content">

        <!-- notification -->
        <?php $this->load->view('notification') ?>

        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Tabel Settings Kecamatan</h3>
                <div class="pull-right">
                    <a href="<?=base_url('Settings/Kecamatan/Tambah')?>" class="btn btn-sm btn-primary">Tambah</a>
                </div>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table id="dataTable" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Action</th>
                            <th>Provinsi</th>
                            <th>ID Kabupaten</th>
                            <th>Kabupaten</th>
                            <th>ID Kecamatan</th>
                            <th>Kecamatan</th>
                            <th>Jenis Daerah</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Action</th>
                            <th>Provinsi</th>
                            <th>ID Kabupaten</th>
                            <th>Kabupaten</th>
                            <th>ID Kecamatan</th>
                            <th>Kecamatan</th>
                            <th>Jenis Daerah</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </section>
</div>