<!-- Tambah -->
<div class="modal fade" id="tambah">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Tambah Penghasilan</h4>
            </div>
            <?=form_open_multipart('Settings/Penghasilan/Create/')?>
            <div class="modal-body">
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Penghasilan :</label>
                    <input type="text" name="penghasilan" class="form-control" placeholder="Penghasilan" required="">
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Nilai :</label>
                    <input type="number" min="0" name="nilai" class="form-control" placeholder="Nilai" required="">
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Status :</label>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <input type="radio" name="status" class="forn-control" value="0" checked>
                                </span>
                                <input type="text" class="form-control" aria-label="False" value="False" readonly>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <input type="radio" name="status" class="forn-control" value="1">
                                </span>
                                <input type="text" class="form-control" aria-label="True" value="True" readonly>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
            <?=form_close()?>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<?php foreach ($tblSPenghasilan as $value): ?>
    <!-- Edit -->
    <div class="modal fade" id="edit-<?=$value->id_penghasilan?>">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Edit Penghasilan</h4>
                </div>
                <?=form_open('Settings/Penghasilan/Update/'.$value->id_penghasilan)?>
                <div class="box-body">
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Penghasilan :</label>
                        <input type="text" name="penghasilan" class="form-control" placeholder="Penghasilan" required="" value="<?=$value->penghasilan;?>">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Nilai :</label>
                        <input type="number" min="0" name="nilai" class="form-control" placeholder="Nilai" required="" value="<?=$value->nilai;?>">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Status :</label>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="input-group">
                                <span class="input-group-addon">
                                    <input type="radio" name="status" class="forn-control" value="0" <?=($value->status == 0)? 'checked' : ''?>>
                                </span>
                                    <input type="text" class="form-control" aria-label="False" value="False" readonly>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group">
                                <span class="input-group-addon">
                                    <input type="radio" name="status" class="forn-control" value="1" <?=($value->status == 1)? 'checked' : ''?>>
                                </span>
                                    <input type="text" class="form-control" aria-label="True" value="True" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
                <?=form_close()?>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
<?php endforeach; ?>
