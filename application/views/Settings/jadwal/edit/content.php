<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Settings
            <small><i>Jadwal</i></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-folder-open"></i> Settings</a></li>
            <li><a href="#"></i> Jadwal</a></li>
            <li class="active"><i>Edit</i></li>
        </ol>
    </section>
    <section class="content">

        <!-- notification -->
        <?php $this->load->view('notification') ?>

        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Jadwal</h3>
                    </div>
                    <?=form_open('Settings/Jadwal/Update/'.$viewSJadwal->id_jadwal)?>
                    <div class="box-body">
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Tipe Ujian : <?=$viewSJadwal->tipe_ujian?></label>
                            <select name="id_tipe_ujian" required="" class="form-control">
                                <?php foreach ($tblSTipeUjian as $value): ?>
                                    <?php if ($value->id_tipe_ujian == $viewSJadwal->id_tipe_ujian): ?>
                                        <option value="<?=$value->id_tipe_ujian;?>" selected><?=$value->tipe_ujian;?></option>
                                    <?php else: ?>
                                        <option value="<?=$value->id_tipe_ujian;?>"><?=$value->tipe_ujian;?></option>
                                    <?php endif; ?>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Kampus : <?=$viewSJadwal->kampus?></label>
                            <select id="id_kampus" name="id_kampus" class="form-control">
                                <option value="">Pilih</option>
                                <?php foreach ($tblSKampus as $value): ?>
                                    <option value="<?=$value->id_kampus;?>"><?=$value->kampus;?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Gedung : <?=$viewSJadwal->gedung?></label>
                            <select id="id_gedung" name="id_gedung" class="form-control">
                                <option value="">Pilih</option>
                            </select>
                            <div id="loading1"><small>Loading...</small></div>
                            <span class="label label-info">Pilih kampus untuk menampilkan data.</span>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Ruangan : <?=$viewSJadwal->ruangan?></label>
                            <input type="hidden" name="old_ruangan" value="<?=$viewSJadwal->id_ruangan?>">
                            <select id="id_ruangan" name="id_ruangan" class="form-control">
                                <option value="">Pilih</option>
                            </select>
                            <div id="loading2"><small>Loading...</small></div>
                            <span class="label label-info">Pilih gedung untuk menampilkan data.</span><br />
                            <span class="label label-warning">Pilih apabila ingin mengubah data sebelumnya.</span>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Tanggal :</label>
                            <input type="date" name="tanggal" class="form-control" placeholder="Tanggal" required="" value="<?=$viewSJadwal->tanggal?>">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Session :</label>
                            <select name="sesi" class="form-control" required="">
                                <option value="SESSION 1" <?=($viewSJadwal->sesi == 'SESSION 1')? 'selected' : ''?>>SESSION 1</option>
                                <option value="SESSION 2" <?=($viewSJadwal->sesi == 'SESSION 2')? 'selected' : ''?>>SESSION 2</option>
                                <option value="SESSION 3" <?=($viewSJadwal->sesi == 'SESSION 3')? 'selected' : ''?>>SESSION 3</option>
                                <option value="SESSION 4" <?=($viewSJadwal->sesi == 'SESSION 4')? 'selected' : ''?>>SESSION 4</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Jam Awal :</label>
                            <input type="time" name="jam_awal" class="form-control" placeholder="Jam Awal" value="<?=$viewSJadwal->jam_awal?>" required="">
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Jam Akhir :</label>
                            <input type="time" name="jam_akhir" class="form-control" placeholder="Jam Akhir" value="<?=$viewSJadwal->jam_akhir?>" required="">
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Quota :</label>
                            <input type="number" name="quota" min="0" class="form-control" placeholder="Quota" value="<?=$viewSJadwal->quota?>" required="">
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Status :</label>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group">
                                <span class="input-group-addon">
                                    <input type="radio" name="status" class="forn-control" value="0" <?=($viewSJadwal->status == 0)? 'checked' : ''?>>
                                </span>
                                        <input type="text" class="form-control" aria-label="False" value="False" readonly>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                <span class="input-group-addon">
                                    <input type="radio" name="status" class="forn-control" value="1" <?=($viewSJadwal->status == 1)? 'checked' : ''?>>
                                </span>
                                        <input type="text" class="form-control" aria-label="True" value="True" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <a href="<?=base_url('Settings/Jadwal/')?>" class="btn btn-default">Back</a>
                        <button type="submit" class="btn btn-primary pull-right">Update</button>
                    </div>
                    <?=form_close()?>
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>
    </section>
</div>