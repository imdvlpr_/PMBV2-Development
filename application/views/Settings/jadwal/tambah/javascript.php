<script type="text/javascript">
    $(function () {
        $("#loading1").hide()
        $("#loading2").hide()
        $("#loading3").hide()

        $("#id_kampus").change(function(){
            $("#id_gedung").hide()
            $("#loading2").show()

            $.ajax({
                type: "POST",
                url: "<?=base_url("Settings/Jadwal/listGedung/"); ?>",
                data: {id_kampus : $("#id_kampus").val()},
                dataType: "json",
                beforeSend: function(e) {
                    if(e && e.overrideMimeType) {
                        e.overrideMimeType("application/json;charset=UTF-8")
                    }
                },
                success: function(response){
                    $("#loading2").hide()
                    $("#id_gedung").html(response.list_gedung).show()
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError);
                }
            })
        })

        $("#id_gedung").change(function(){
            $("#id_ruangan").hide()
            $("#loading3").show()

            $.ajax({
                type: "POST",
                url: "<?=base_url("Settings/Jadwal/listRuangan/"); ?>",
                data: {id_gedung : $("#id_gedung").val()},
                dataType: "json",
                beforeSend: function(e) {
                    if(e && e.overrideMimeType) {
                        e.overrideMimeType("application/json;charset=UTF-8")
                    }
                },
                success: function(response){
                    $("#loading3").hide()
                    $("#id_ruangan").html(response.list_ruangan).show()
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError);
                }
            })
        })
    })
</script>