<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Settings
            <small><i>Jadwal</i></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-folder-open"></i> Settings</a></li>
            <li><a href="#"></i> Jadwal</a></li>
            <li class="active"><i>Tambah</i></li>
        </ol>
    </section>
    <section class="content">

        <!-- notification -->
        <?php $this->load->view('notification') ?>

        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Tambah Jadwal</h3>
                    </div>
                    <?=form_open('Settings/Jadwal/Create/')?>
                    <div class="box-body">
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Tipe Ujian : </label>
                            <select name="id_tipe_ujian" required="" class="form-control">
                                <?php foreach ($tblSTipeUjian as $value): ?>
                                    <option value="<?=$value->id_tipe_ujian;?>"><?=$value->tipe_ujian;?></option>
                                <?php endforeach ?>
                            </select>
                            <span class="label label-warning">Pastikan tipe ujian dalam kondisi <i>TRUE</i></span>
                            <div id="loading1"><small>Loading...</small></div>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Kampus : </label>
                            <select id="id_kampus" name="id_kampus" required="" class="form-control">
                                <option value="">Pilih</option>
                                <?php foreach ($tblSKampus as $value): ?>
                                    <option value="<?=$value->id_kampus;?>"><?=$value->kampus;?></option>
                                <?php endforeach ?>
                            </select>
                            <span class="label label-warning">Pastikan kampus dalam kondisi <i>TRUE</i></span>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Gedung : </label>
                            <select id="id_gedung" name="id_gedung" required="" class="form-control">
                                <option value="">Pilih</option>
                            </select>
                            <span class="label label-warning">Pastikan gedung dalam kondisi <i>TRUE</i></span>
                            <div id="loading2"><small>Loading...</small></div>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Ruangan : </label>
                            <select id="id_ruangan" name="id_ruangan" required="" class="form-control">
                                <option value="">Pilih</option>
                            </select>
                            <span class="label label-warning">Pastikan ruangan dalam kondisi <i>TRUE</i></span>
                            <div id="loading3"><small>Loading...</small></div>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Tanggal :</label>
                            <input type="date" name="tanggal" class="form-control" placeholder="Tanggal" required="">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Session :</label>
                            <select name="sesi" class="form-control" required="">
                                <option value="SESSION 1">SESSION 1</option>
                                <option value="SESSION 2">SESSION 2</option>
                                <option value="SESSION 3">SESSION 3</option>
                                <option value="SESSION 4">SESSION 4</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Jam Awal :</label>
                            <input type="time" name="jam_awal" class="form-control" placeholder="Jam Awal" required="">
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Jam Akhir :</label>
                            <input type="time" name="jam_akhir" class="form-control" placeholder="Jam Akhir" required="">
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Quota :</label>
                            <input type="number" name="quota" min="0" class="form-control" placeholder="Quota" required="">
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Status :</label>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group">
                                <span class="input-group-addon">
                                    <input type="radio" name="status" class="forn-control" value="0" checked>
                                </span>
                                        <input type="text" class="form-control" aria-label="False" value="False" readonly>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                <span class="input-group-addon">
                                    <input type="radio" name="status" class="forn-control" value="1">
                                </span>
                                        <input type="text" class="form-control" aria-label="True" value="True" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <a href="<?=base_url('Settings/Jadwal/')?>" class="btn btn-default">Back</a>
                        <button type="submit" class="btn btn-primary pull-right">Save</button>
                    </div>
                    <?=form_close()?>
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>
    </section>
</div>