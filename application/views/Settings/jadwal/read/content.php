<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Settings
            <small><i>Jadwal</i></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-gears"></i> Settings</a></li>
            <li class="active"><i>Jadwal</i></li>
        </ol>
    </section>
    <section class="content">

        <!-- notification -->
        <?php $this->load->view('notification') ?>

        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Tabel Setting Jadwal</h3>
                <div class="pull-right">
                    <div class="btn-group">
                        <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-gears">&nbsp;</i> <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li><a href="<?=base_url('Settings/Jadwal/Tambah/')?>">Tambah</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="<?=base_url('Cronejob/CJ3/');?>" target="_blank">Update Tipe Ujian</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table id="dataTable" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Action</th>
                            <th>Tipe Ujian</th>
                            <th>Tanggal</th>
                            <th>Kampus</th>
                            <th>Gedung</th>
                            <th>Ruangan</th>
                            <th>Sesi</th>
                            <th>Jam Awal</th>
                            <th>Jam Akhir</th>
                            <th>Quota</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>Action</th>
                            <th>Tipe Ujian</th>
                            <th>Tanggal</th>
                            <th>Kampus</th>
                            <th>Gedung</th>
                            <th>Ruangan</th>
                            <th>Sesi</th>
                            <th>Jam Awal</th>
                            <th>Jam Akhir</th>
                            <th>Quota</th>
                            <th>Status</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </section>
</div>