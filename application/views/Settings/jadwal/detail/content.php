<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Settings
            <small><i>Jadwal</i></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-gears"></i> Settings</a></li>
            <li><i>Jadwal</i></li>
            <li class="active"><i>Detail</i></li>
        </ol>
    </section>
    <section class="content">

        <!-- notification -->
        <?php $this->load->view('notification') ?>

        <!-- Jadwal -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Detail Jadwal</h3>
                <?php /*
                <div class="pull-right">
                    <div class="btn-group">
                        <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-gears">&nbsp;</i> <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li><a href="<?=base_url('Settings/Jadwal/Tambah/')?>">Tambah</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="<?=base_url('Cronejob/CJ3/');?>" target="_blank">Update Tipe Ujian</a></li>
                        </ul>
                    </div>
                </div>
                */ ?>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <tbody>
                                <tr>
                                    <th>Tipe Ujian</th>
                                    <td><?=$viewSJadwal->tipe_ujian?></td>
                                </tr>
                                <tr>
                                    <th>Tanggal</th>
                                    <td><?=$viewSJadwal->tanggal?></td>
                                </tr>
                                <tr>
                                    <th>Kampus</th>
                                    <td><?=$viewSJadwal->kampus?></td>
                                </tr>
                                <tr>
                                    <th>Gedung</th>
                                    <td><?=$viewSJadwal->gedung?></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <tbody>
                                <tr>
                                    <th>Ruangan</th>
                                    <td><?=$viewSJadwal->ruangan?></td>
                                </tr>
                                <tr>
                                    <th>Sesi</th>
                                    <td><?=$viewSJadwal->sesi?></td>
                                </tr>
                                <tr>
                                    <th>Jam Awal</th>
                                    <td><?=$viewSJadwal->jam_awal?></td>
                                </tr>
                                <tr>
                                    <th>Jam Akhir</th>
                                    <td><?=$viewSJadwal->jam_akhir?></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Users -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Users</h3>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table id="dataTable" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Foto</th>
                            <th>Nomor Peserta</th>
                            <th>Nama</th>
                            <th>Kategori</th>
                            <th>Jurusan</th>
                            <th>Nomor Telepon</th>
                            <th>Biodata</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php for($i = 0; $i < count($data); $i++): ?>
                            <tr>
                                <td><img class="img-responsive center-block" style="width: 200px" src="https://pmb-pradaftar.uinsgd.ac.id/upload/foto/<?=$data[$i]['biodata']->foto?>" alt="foto profile"></td>
                                <td><?=$data[$i]['users']->nomor_peserta?></td>
                                <td><?=$data[$i]['users']->nama?></td>
                                <td><?=($data[$i]['users']->kategori == 1) ? 'IPA / SAINS' : 'IPS / SOSIAL DAN HUMANIORA'?></td>
                                <td>
                                    <ol>
                                    <?php foreach ($data[$i]['pilihan'] as $a): ?>
                                        <li><?='Pilihan ke-'.$a->pilihan.' : ('.$a->kode_jurusan.') '.$a->jurusan.' - '.$a->fakultas?></li>
                                    <?php endforeach; ?>
                                    </ol>
                                </td>
                                <td><?=$data[$i]['users']->nmr_tlp?></td>
                                <td><?=($data[$i]['users']->biodata == 1) ? "<span class=\"label label-success\">Sudah Verifikasi</span>" : "<span class=\"label label-danger\">Belum Verifikasi</span>"?></td>
                            </tr>
                        <?php endfor; ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>Foto</th>
                            <th>Nomor Peserta</th>
                            <th>Nama</th>
                            <th>Kategori</th>
                            <th>Jurusan</th>
                            <th>Nomor Telepon</th>
                            <th>Biodata</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </section>
</div>