<!-- Tambah -->
<div class="modal fade" id="tambah">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Tambah Rumpun</h4>
            </div>
            <?=form_open('Settings/Rumpun/Create/')?>
            <div class="modal-body">
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Rumpun :</label>
                    <input type="text" name="rumpun" class="form-control" placeholder="Rumpun" required="">
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Status :</label>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <input type="radio" name="status" class="forn-control" value="0" checked>
                                </span>
                                <input type="text" class="form-control" aria-label="False" value="False" readonly>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <input type="radio" name="status" class="forn-control" value="1">
                                </span>
                                <input type="text" class="form-control" aria-label="True" value="True" readonly>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
            <?=form_close()?>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- Edit -->
<?php $no=1; foreach ($tblSRumpun as $value): ?>
<div class="modal fade" id="edit-<?=$value->id_rumpun?>">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Edit Rumpun</h4>
            </div>
            <?=form_open('Settings/Rumpun/Update/'.$value->id_rumpun)?>
            <div class="modal-body">
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Rumpun :</label>
                    <input type="text" name="rumpun" class="form-control" placeholder="Rumpun" required="" value="<?=$value->rumpun;?>">
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Status :</label>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <input type="radio" name="status" class="forn-control" value="0" <?php if ($value->status == 0){echo 'checked';}?>>
                                </span>
                                <input type="text" class="form-control" aria-label="False" value="False" readonly>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <input type="radio" name="status" class="forn-control" value="1" <?php if ($value->status == 1){echo 'checked';}?>>
                                </span>
                                <input type="text" class="form-control" aria-label="True" value="True" readonly>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Update</button>
            </div>
            <?=form_close()?>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<?php endforeach; ?>
<!-- /.modal -->