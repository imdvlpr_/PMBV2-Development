<!-- Tambah -->
<div class="modal fade" id="tambah">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Tambah Kampus</h4>
            </div>
            <?=form_open('Settings/Kampus/Create/')?>
            <div class="modal-body">
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Kampus :</label>
                    <input type="text" name="kampus" class="form-control" placeholder="Kampus" required="">
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Status :</label>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <input type="radio" name="status" class="forn-control" value="0" checked>
                                </span>
                                <input type="text" class="form-control" aria-label="False" value="False" readonly>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <input type="radio" name="status" class="forn-control" value="1">
                                </span>
                                <input type="text" class="form-control" aria-label="True" value="True" readonly>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
            <?=form_close()?>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- Edit -->
<?php $no=1; foreach ($tblSKampus as $value): ?>
<div class="modal fade" id="edit-<?=$value->id_kampus?>">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Edit Kampus</h4>
            </div>
            <?=form_open('Settings/Kampus/Update/'.$value->id_kampus)?>
            <div class="modal-body">
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Kampus :</label>
                    <input type="text" name="kampus" class="form-control" placeholder="Kampus" required="" value="<?=$value->kampus;?>">
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Status :</label>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <input type="radio" name="status" class="forn-control" value="0" <?php if ($value->status == 0){echo 'checked';}?>>
                                </span>
                                <input type="text" class="form-control" aria-label="False" value="False" readonly>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <input type="radio" name="status" class="forn-control" value="1" <?php if ($value->status == 1){echo 'checked';}?>>
                                </span>
                                <input type="text" class="form-control" aria-label="True" value="True" readonly>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Update</button>
            </div>
            <?=form_close()?>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<?php endforeach; ?>
<!-- /.modal -->