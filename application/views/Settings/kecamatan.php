<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>PMB - UIN Sunan Gunung Djati Bandung</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Icon -->
  <link href="<?=base_url('design-backend/images/logo.png'); ?>" rel="icon" type="image/x-icon" />
   <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?=base_url('design-backend/bootstrap/css/bootstrap.min.css');?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?=base_url('design-backend/plugins/datatables/dataTables.bootstrap.css')?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=base_url('design-backend/dist/css/AdminLTE.min.css');?>">
  <link rel="stylesheet" href="<?=base_url('design-backend/dist/css/skins/skin-red-light.min.css');?>">
</head>

<body class="hold-transition skin-red-light sidebar-mini">
<div class="wrapper">

  <?php $this->load->view('header-navbar');?>
  <?php $this->load->view('header-sidebar');?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Settings
        <small><i>Kecamatan</i></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-gears"></i> Settings</a></li>
        <li class="active"><i>Kecamatan</i></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Alert -->
      <?php if (!empty($this->session->flashdata('type_message'))) { ?>
        <div class="alert alert-<?=$this->session->flashdata('type_message')?> alert-dismissible text-center" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <?=$this->session->flashdata('message');?>
        </div>
      <?php } ?>

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Tabel Setting Kecamatan</h3>
          <div class="pull-right">
            <button type="button" class="btn btn-primary" id="tambah_tooltip" data-toggle="modal" data-target="#tambah" title="Tambah">
              <i class="fa fa-plus"></i>
            </button>
          </div>
        </div>
        <div class="box-body">
          <div class="table-responsive">  
            <table id="kecamatan" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Kabupaten</th>
                  <th>Nama</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody> 
                <?php $no=1; foreach ($tbSKEC as $value): ?>  
                <tr>
                  <td><?=$no?></td>
                  <td><?=$value->id_kab?></td>
                  <td><?=$value->nama?></td>
                  <td class="text-center">
                    <div class="btn-group" role="group" aria-label="Action">
                      <button type="button" class="btn btn-warning" id="edit_tooltip<?=$value->id_kec?>" data-toggle="modal" data-target="#edit<?=$value->id_kec?>" data-placement="top" title="Edit"><i class="fa fa-edit"></i></button>
                      <a href="<?=base_url('Settings/Kecamatan/Delete/'.$value->id_kec)?>" onclick="return confirm('Apakah Anda Yakin ?')" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash"></i></a>
                    </div>
                  </td>
                </tr>
                <?php $no++; endforeach ?>
              </tbody>
            </table>
          </div>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->

      <!-- Your Page Content Here -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      Version 1.0
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2017 <a target="_blank" href="https://www.uinsgd.ac.id">UIN SGD Bandung</a> .</strong> All rights reserved. Support by <a class="link-color" target="_blank" href="https://ptipd.uinsgd.ac.id/">PTIPD.</a>
  </footer>
  
</div>
<!-- ./wrapper -->

<!-- Tambah -->
<div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Tambah Kecamatan</h4>
      </div>
      <?=form_open_multipart('Settings/Kecamatan/Tambah/')?>
      <div class="modal-body">
        <div class="form-group">
          <label for="recipient-name" class="control-label">Kabupaten :</label>
          <select name="id_kab" class="form-control" required="">
            <?php foreach ($tbSKAB as $value2) { ?>
              <option value="<?=$value2->id_kab?>"><?=$value2->nama?></option>
            <?php } ?>
          </select>
        </div>
        <div class="form-group">
          <label for="recipient-name" class="control-label">Kecamatan :</label>
          <input type="text" name="nama" class="form-control" placeholder="Nama" required="">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
      <?=form_close()?>
    </div>
  </div>
</div>

<!-- Edit -->
<?php foreach ($tbSKEC as $value) { ?>
<div class="modal fade" id="edit<?=$value->id_kec?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Kecamatan</h4>
      </div>
      <?=form_open_multipart('Settings/Kabupaten/Update/'.$value->id_kab)?>
      <div class="modal-body">
        <div class="form-group">
          <label for="recipient-name" class="control-label">Provinsi :</label>
          <select name="id_kab" class="form-control" required="">
            <?php foreach ($tbSKAB as $value2) { ?>
              <option value="<?=$value2->id_kab?>" <?php if($value2->id_kab==$value->id_kab){ echo "selected"; }?>><?=$value2->nama?></option>
            <?php } ?>
          </select>
        </div>
        <div class="form-group">
          <label for="recipient-name" class="control-label">Nama :</label>
          <input type="text" name="nama" class="form-control" placeholder="Nama" required="" value="<?=$value->nama;?>">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
      <?=form_close()?>
    </div>
  </div>
</div>
<?php } ?>


<!-- REQUIRED JS SCRIPTS -->
<!-- jQuery 2.2.3 -->
<script src="<?=base_url('design-backend/plugins/jQuery/jquery-2.2.3.min.js');?>"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?=base_url('design-backend/bootstrap/js/bootstrap.min.js');?>"></script>
<!-- DataTables -->
<script src="<?=base_url('design-backend/plugins/datatables/jquery.dataTables.min.js')?>"></script>
<script src="<?=base_url('design-backend/plugins/datatables/dataTables.bootstrap.min.js')?>"></script>
<!-- SlimScroll -->
<script src="<?=base_url('design-backend/plugins/slimScroll/jquery.slimscroll.min.js')?>"></script>
<!-- FastClick -->
<script src="<?=base_url('design-backend/plugins/fastclick/fastclick.js')?>"></script>
<!-- AdminLTE App -->
<script src="<?=base_url('design-backend/dist/js/app.min.js');?>"></script>
<!-- page script -->
<script>
  $(function () {
    $('#kecamatan').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true
    });
  });
</script>
<script type="text/javascript">
  $('#tambah_tooltip').tooltip();
</script>
<?php foreach ($tbSKEC as $value): ?>
  <script type="text/javascript">
    $("#edit_tooltip<?=$value->id_kec?>").tooltip();
  </script>
<?php endforeach ?>
</body>
</html>