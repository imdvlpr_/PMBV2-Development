<!-- Tambah -->
<div class="modal fade" id="tambah">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Tambah Jurusan</h4>
            </div>
            <?=form_open_multipart('Settings/Jurusan/Create/')?>
            <div class="modal-body">
                <div class="form-group">
                    <label class="control-label">Kode Jurusan :</label>
                    <input type="number" min="0" name="kode_jurusan" class="form-control" placeholder="Kode Jurusan" required="">
                </div>
                <div class="form-group">
                    <label class="control-label">Jurusan :</label>
                    <input type="text" name="jurusan" class="form-control" placeholder="Jurusan" required="">
                </div>
                <div class="form-group">
                    <label class="control-label">Akreditasi :</label>
                    <select name="akreditasi" class="form-control" required="">
                        <option value="1">A</option>
                        <option value="2">B</option>
                        <option value="3">C</option>
                        <option value="4">-</option>
                    </select>
                </div>
                <div class="form-group">
                    <label class="control-label">Kategori :</label>
                    <select name="kategori" class="form-control" required="">
                        <option value="1">IPA / SAINS</option>
                        <option value="2">IPS / SOSIAL DAN HUMANIORA</option>
                    </select>
                </div>
                <div class="form-group">
                    <label class="control-label">Grade IPA :</label>
                    <input type="number" min="0" name="grade_ipa" class="form-control" placeholder="Grade IPA" required="">
                </div>
                <div class="form-group">
                    <label class="control-label">Grade IPS :</label>
                    <input type="number" min="0" name="grade_ips" class="form-control" placeholder="Grade IPS" required="">
                </div>
                <div class="form-group">
                    <label class="control-label">Quota :</label>
                    <input type="number" min="0" name="quota" class="form-control" placeholder="quota" required="">
                </div>
                <div class="form-group">
                    <label class="control-label">Quota K1 :</label>
                    <input type="number" min="0" name="quota_k1" class="form-control" placeholder="quota k1" required="">
                </div>
                <div class="form-group">
                    <label class="control-label">Fakultas :</label>
                    <select name="id_fakultas" class="form-control" required="">
                        <?php foreach ($tblSFakultas as $value): ?>
                            <option value="<?=$value->id_fakultas?>"><?=$value->fakultas?></option>
                        <?php endforeach ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Status :</label>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <input type="radio" name="status" class="forn-control" value="0" checked>
                                </span>
                                <input type="text" class="form-control" aria-label="False" value="False" readonly>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <input type="radio" name="status" class="forn-control" value="1">
                                </span>
                                <input type="text" class="form-control" aria-label="True" value="True" readonly>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
            <?=form_close()?>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<?php foreach ($viewFakultasJurusan as $value): ?>
    <!-- Edit -->
    <div class="modal fade" id="edit-<?=$value->kode_jurusan?>">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Edit Jurusan</h4>
                </div>
                <?=form_open('Settings/Jurusan/Update/'.$value->kode_jurusan)?>
                <div class="box-body">
                    <div class="form-group">
                        <label class="control-label">Kode Jurusan :</label>
                        <input type="number" min="0" name="kode_jurusan" class="form-control" placeholder="Kode Jurusan" required="" value="<?=$value->kode_jurusan?>">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Jurusan :</label>
                        <input type="text" name="jurusan" class="form-control" placeholder="Jurusan" required="" value="<?=$value->jurusan?>">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Akreditasi :</label>
                        <select name="akreditasi" class="form-control" required="">
                            <option value="1" <?=($value->akreditasi == 1)? 'selected' : ''?>>A</option>
                            <option value="2" <?=($value->akreditasi == 2)? 'selected' : ''?>>B</option>
                            <option value="3" <?=($value->akreditasi == 3)? 'selected' : ''?>>C</option>
                            <option value="4" <?=($value->akreditasi == 4)? 'selected' : ''?>>D</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Kategori :</label>
                        <select name="kategori" class="form-control" required="">
                            <option value="1" <?=($value->kategori == 1)? 'selected' : ''?>>IPA / SAINS)</option>
                            <option value="2" <?=($value->kategori == 2)? 'selected' : ''?>>IPS / SOSIAL DAN HUMANIORA</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Grade IPA :</label>
                        <input type="number" min="0" name="grade_ipa" class="form-control" placeholder="Grade IPA" required="" value="<?=$value->grade_ipa?>">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Grade IPS :</label>
                        <input type="number" min="0" name="grade_ips" class="form-control" placeholder="Grade IPS" required="" value="<?=$value->grade_ips?>">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Quota :</label>
                        <input type="number" min="0" name="quota" class="form-control" placeholder="quota" required="" value="<?=$value->quota?>">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Quota K1 :</label>
                        <input type="number" min="0" name="quota_k1" class="form-control" placeholder="quota k1" required="" value="<?=$value->quota_k1?>">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Fakultas :</label>
                        <select name="id_fakultas" class="form-control" required="">
                            <?php foreach ($tblSFakultas as $row): ?>
                                <?php if ($value->id_fakultas == $row->id_fakultas) : ?>
                                    <option value="<?=$row->id_fakultas?>" selected=""><?=$row->fakultas?></option>
                                <?php else: ?>
                                    <option value="<?=$row->id_fakultas?>"><?=$row->fakultas?></option>
                                <?php endif; ?>
                            <?php endforeach ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Status :</label>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="input-group">
                                <span class="input-group-addon">
                                    <input type="radio" name="status" class="forn-control" value="0" <?=($value->status_jurusan == 0)? 'checked' : ''?>>
                                </span>
                                    <input type="text" class="form-control" aria-label="False" value="False" readonly>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group">
                                <span class="input-group-addon">
                                    <input type="radio" name="status" class="forn-control" value="1" <?=($value->status_jurusan == 1)? 'checked' : ''?>>
                                </span>
                                    <input type="text" class="form-control" aria-label="True" value="True" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
                <?=form_close()?>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <!-- Detail -->
    <div class="modal fade" id="detail-<?=$value->kode_jurusan?>">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Detail Jurusan</h4>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <tr>
                                <th>Fakultas</th>
                                <td><?=$value->fakultas?></td>
                            </tr>
                            <tr>
                                <th>Status Fakultas</th>
                                <td>
                                    <?php if($value->status_fakultas == 0) : ?>
                                        <span class="label label-danger">False</span>
                                    <?php else: ?>
                                        <span class="label label-success">True</span>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <th>Kode Jurusan</th>
                                <td><?=$value->kode_jurusan?></td>
                            </tr>
                            <tr>
                                <th>Jurusan</th>
                                <td><?=$value->jurusan?></td>
                            </tr>
                            <tr>
                                <th>Akreditasi</th>
                                <td>
                                    <?php
                                    if ($value->akreditasi == 1)
                                        echo 'A';
                                    elseif ($value->akreditasi == 2)
                                        echo 'B';
                                    elseif ($value->akreditasi == 3)
                                        echo 'C';
                                    else
                                        echo 'D';
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <th>Kategori</th>
                                <td>
                                    <?php
                                    if ($value->kategori== 1)
                                        echo 'IPA';
                                    else
                                        echo 'IPA & IPS';
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <th>Grade IPA</th>
                                <td><?=$value->grade_ipa?></td>
                            </tr>
                            <tr>
                                <th>Grade IPS</th>
                                <td><?=$value->grade_ips?></td>
                            </tr>
                            <tr>
                                <th>Quota</th>
                                <td><?=$value->quota?></td>
                            </tr>
                            <tr>
                                <th>Quota K1</th>
                                <td><?=$value->quota_k1?></td>
                            </tr>
                            <tr>
                                <th>Status Jurusan</th>
                                <td>
                                    <?php if($value->status_jurusan == 0) : ?>
                                        <span class="label label-danger">False</span>
                                    <?php else: ?>
                                        <span class="label label-success">True</span>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <th>Date Created</th>
                                <td><?=$value->date_created?></td>
                            </tr>
                            <tr>
                                <th>Date Updated</th>
                                <td><?=$value->date_updated?></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <!-- UKT -->
    <div class="modal fade" id="ukt-<?=$value->kode_jurusan?>">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">UKT Jurusan</h4>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <tr>
                                <th>Kategori</th>
                                <th>Nominal</th>
                                <th>Status</th>
                            </tr>
                            <?php for ($i = 0; $i < 9; $i++): ?>
                            <tr>
                                <td><?=$UKT[$value->kode_jurusan][0][$i]?></td>
                                <td>Rp <?=number_format($UKT[$value->kode_jurusan][1][$i], 2)?></td>
                                <td class="text-center">
                                    <?php if($UKT[$value->kode_jurusan][2][$i] == 0) : ?>
                                        <span class="label label-danger">False</span>
                                    <?php else: ?>
                                        <span class="label label-success">True</span>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <?php endfor; ?>
                            <tr>
                                <th>Kategori</th>
                                <th>Nominal</th>
                                <th>Status</th>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="modal-footer"></div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
<?php endforeach; ?>
