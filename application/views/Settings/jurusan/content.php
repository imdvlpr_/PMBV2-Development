<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Settings
            <small><i>Jurusan</i></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-gears"></i> Settings</a></li>
            <li class="active"><i>Jurusan</i></li>
        </ol>
    </section>
    <section class="content">

        <!-- notification -->
        <?php $this->load->view('notification') ?>

        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Tabel Setting Jurusan</h3>
                <div class="pull-right">
                    <div class="btn-group">
                        <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-gears">&nbsp;</i> <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li><a href="#" data-toggle="modal" data-target="#tambah">Tambah</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="<?=base_url('Import/Settings/KuotaJurusan/');?>" target="_blank">Upload Kuota Jurusan</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table id="dataTable" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Action</th>
                            <th>Kode Jurusan</th>
                            <th>Jurusan (Akreditasi)</th>
                            <th>Fakultas</th>
                            <th>Katergori</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $no=1; foreach ($viewFakultasJurusan as $value): ?>
                            <tr>
                                <td><?=$no++?></td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-gear"></i> <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#" data-toggle="modal" data-target="#detail-<?=$value->kode_jurusan?>">Detail</a></li>
                                            <li><a href="#" data-toggle="modal" data-target="#ukt-<?=$value->kode_jurusan?>">UKT</a></li>
                                            <li><a href="#" data-toggle="modal" data-target="#edit-<?=$value->kode_jurusan?>">Edit</a></li>
                                            <li role="separator" class="divider"></li>
                                            <li><a href="<?=base_url('Settings/Jurusan/Delete/'.$value->kode_jurusan)?>" onclick="return confirm('Apakah Anda Yakin ?')">Delete</i></a></li>
                                        </ul>
                                    </div>
                                </td>
                                <td><?=$value->kode_jurusan?></td>
                                <td>
                                    <?=$value->jurusan?>
                                    <?php
                                        if ($value->akreditasi == 1)
                                            echo '(A)';
                                        elseif ($value->akreditasi == 2)
                                            echo '(B)';
                                        elseif ($value->akreditasi == 3)
                                            echo '(C)';
                                        else
                                            echo '(-)';
                                    ?>
                                </td>
                                <td><?=$value->fakultas?></td>
                                <td>
                                    <?php
                                    if ($value->kategori== 1)
                                        echo 'IPA';
                                    else
                                        echo 'IPA & IPS';
                                    ?>
                                </td>
                                <td class="text-center">
                                    <?php if($value->status_jurusan == 0) : ?>
                                        <span class="label label-danger">False</span>
                                    <?php else: ?>
                                        <span class="label label-success">True</span>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Action</th>
                            <th>Kode Jurusan</th>
                            <th>Jurusan (Akreditasi)</th>
                            <th>Fakultas</th>
                            <th>Katergori</th>
                            <th>Status</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </section>
</div>