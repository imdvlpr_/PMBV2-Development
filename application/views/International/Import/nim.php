<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>PMB - UIN Sunan Gunung Djati Bandung</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Icon -->
	<link href="<?=base_url('design-backend/images/logo.png'); ?>" rel="icon" type="image/x-icon" />
	<!-- Bootstrap 3.3.6 -->
	<link rel="stylesheet" href="<?=base_url('design-backend/bootstrap/css/bootstrap.min.css');?>">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="<?=base_url('design-backend/dist/css/AdminLTE.min.css');?>">
	<link rel="stylesheet" href="<?=base_url('design-backend/dist/css/skins/skin-red-light.min.css');?>">
</head>

<body class="hold-transition skin-red-light sidebar-mini">
<div class="wrapper">
	<?php $this->load->view('header-navbar');?>
	<?php $this->load->view('header-sidebar');?>
	<div class="content-wrapper">
		<section class="content-header">
			<h1>
				Import
				<small>NIM</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-upload"></i> Import</a></li>
				<li class="active">NIM</li>
			</ol>
		</section>
		<section class="content">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Import Data NIM Internasional</h3>
				</div>
				<div class="box-body">
					<?php if (!empty($this->session->flashdata('type_message'))) { ?>
						<div class="alert alert-<?=$this->session->flashdata('type_message')?> alert-dismissible text-center" role="alert">
							<?=$this->session->flashdata('message');?>
						</div>
					<?php } ?>
					<div class="row">
						<div class="col-md-6">
							<?=form_open_multipart('Internasional/Import/NIM/Import')?>
							<div class="form-group">
								<label class="control-label">Upload file :</label>
								<input type="file" name="userfile" required>
							</div>
							<button type="submit" onclick="return confirm('Apakah data sudah sesuai template ?')" class="btn btn-primary btn-flat pull-right"><i class="fa fa-upload">&nbsp;</i>Import</button>
							<?=form_close()?>
						</div>
						<div class="col-md-6">
							<p>Tatacara import data NIM : (Only Windows)</p>
							<ol>
								<li>Download template NIM yang sudah disediakan.</li>
								<li>Isi kolom pada tabel sesuai nama kolomnya.</li>
								<li>Pilih file yang ingin di upload.</li>
								<li>klik import.</li>
							</ol>
							<a href="<?=base_url('file/template_nim.xlsx')?>" class="btn btn-default btn-flat" target="_blank">Download Template</a>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
	<?php $this->load->view('footer'); ?>
</div>
<!-- jQuery 2.2.3 -->
<script src="<?=base_url('design-backend/plugins/jQuery/jquery-2.2.3.min.js');?>"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?=base_url('design-backend/bootstrap/js/bootstrap.min.js');?>"></script>
<!-- AdminLTE App -->
<script src="<?=base_url('design-backend/dist/js/app.min.js');?>"></script>
</body>
</html>
