<?php
header("Content-Type:application/vnd.ms-excel");
header('Content-Disposition:attachment; filename="internasional_semua_user.xls"');
?>
<table id="users" class="table table-bordered table-striped">
<thead>
	<tr>
		<th>Kode Pembayaran</th>
		<th>NIK</th>
		<th>Nomor Peserta</th>
		<th>Nama</th>
		<th>Nomor HP</th>
		<th>Jurusan 1</th>
		<th>Jurusan 2</th>
		<th>Jurusan 3</th>
		<th>Status Bayar</th>
	</tr>
</thead>
<tbody>
	<?php foreach($tbIUsers as $value): ?>
		<tr>
			<td><?=$value->kd_pembayaran?></td>
			<td><?=$value->nik_passport?></td>
			<td><?=$value->nomor_peserta?></td>
			<td><?=$value->nama?></td>
			<td><?=$value->noHp?></td>
			<td><?=$value->jurusan1?></td>
			<td><?=$value->jurusan2?></td>
			<td><?=$value->jurusan3?></td>
			<td><?=$value->status_bayar?></td>
		</tr>
	<?php endforeach ?>
</tbody>
</table>