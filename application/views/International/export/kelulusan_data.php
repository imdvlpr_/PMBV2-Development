<?php
header("Content-Type:application/vnd.ms-excel");
header('Content-Disposition:attachment; filename="internasional_kelulusan_2018.xls"');
?>
<table id="users" class="table table-bordered table-striped">
<thead>
	<tr>
		<th>Nomor</th>
		<th>Nomor Peserta</th>
		<th>Nama</th>
		<th>Status Kelulusan</th>
		<th>Kode Jurusan</th>
		<th>Jurusan</th>
		<th>Fakultas</th>
		<th>Keterangan</th>
	</tr>
</thead>
<tbody>
	<?php foreach($viewKelulusan as $value): ?>
		<tr>
			<td><?=$value->no?></td>
			<td><?=$value->nomor_peserta?></td>
			<td><?=$value->nama?></td>
			<td><?=$value->status_kelulusan?></td>
			<td><?=$value->kode_jurusan_kelulusan?></td>
			<td><?=$value->jurusan_kelulusan?></td>
			<td><?=$value->fakultas_kelulusan?></td>
			<td><?=$value->keterangan?></td>
		</tr>
	<?php endforeach ?>
</tbody>
</table>