<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Pradaftar Users
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-folder-open"></i> Pradaftar</a></li>
            <li>Users</li>
        </ol>
    </section>

    <section class="content">
        <?php if (!empty($this->session->flashdata('type_message'))) { ?>
            <div class="alert alert-<?=$this->session->flashdata('type_message')?> alert-dismissible text-center" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?=$this->session->flashdata('message');?>
            </div>
        <?php } ?>
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Tabel Pradaftar Users</h3>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table id="users" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Action</th>
                                <th>Kode Pembayaran</th>
                                <th>NIK</th>
                                <th>Nomor Peserta</th>
                                <th>Nama</th>
                                <th>Status Bayar</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($tbIUsers as $value): ?>
                                <tr>
                                    <td>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Action <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li><a href="<?=base_url('Internasional/Users/Detail/'.$value->nik_passport)?>">Detail</a></li>
                                                <li><a href="<?=base_url('Internasional/Users/'.$action.'/'.$value->nik_passport)?>">Ubah Status Bayar</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                    <td><?=$value->kd_pembayaran?></td>
                                    <td><?=$value->nik_passport?></td>
                                    <td><?=$value->nomor_peserta?></td>
                                    <td><?=$value->nama?></td>
                                    <td>
                                        <?=$value->status_bayar?>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
</div>