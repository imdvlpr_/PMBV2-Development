<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Users
            <small><i>Detail</i></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-folder-open"></i> Users</a></li>
            <li><a href="#"></i> Detail</a></li>
            <li class="active"><i>Edit</i></li>
        </ol>
    </section>
    <section class="content">
        <?php if (!empty($this->session->flashdata('type_message'))) { ?>
            <div class="alert alert-<?=$this->session->flashdata('type_message')?> alert-dismissible text-center" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?=$this->session->flashdata('message');?>
            </div>
        <?php } ?>
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Tabel Users</h3>
                        <div class="pull-right">
                    <a href="<?=base_url('index.php/Internasional/Users/Edit/'.$tbIUsers->nik_passport)?>" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i></a>
                </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <tr>
                                            <th>Kode Pembayaran</th>
                                            <td><?=$tbIUsers->kd_pembayaran?></td>
                                        </tr>
                                        <tr>
                                            <th>NIK</th>
                                            <td><?=$tbIUsers->nik_passport?></td>
                                        </tr>
                                        <tr>
                                            <th>Nomor Peserta</th>
                                            <td><?=$tbIUsers->nomor_peserta?></td>
                                        </tr>
                                        <tr>
                                            <th>Nama</th>
                                            <td><?=$tbIUsers->nama?></td>
                                        </tr>
                                        <tr>
                                            <th>Tempat/Tanggal Lahir</th>
                                            <td><?=$tbIUsers->tempat?>/<?=$tbIUsers->tgl_lhr?></td>
                                        </tr>
                                        <tr>
                                            <th>Alamat</th>
                                            <td><?=$tbIUsers->alamat?></td>
                                        </tr>
                                        <tr>
                                            <th>Jenis Kelamin</th>
                                            <td><?=$tbIUsers->jenis_kelamin?></td>
                                        </tr>
                                        <tr>
                                            <th>Email</th>
                                            <td><?=$tbIUsers->username?></td>
                                        </tr>
                                        <tr>
                                            <th>No HP</th>
                                            <td><?=$tbIUsers->noHp?></td>
                                        </tr>
                                        <tr>
                                            <th>Kebangsaan</th>
                                            <td><?=$tbIUsers->kebangsaan?></td>
                                        </tr>
                                        <tr>
                                            <th>Asal Sekolah</th>
                                            <td><?=$tbIUsers->asal_sekolah?></td>
                                        </tr>
                                        <tr>
                                            <th>Rekomendasi File</th>
                                            <td><?=$tbIUsers->rekomendasi_file?></td>
                                        </tr>
                                        <tr>
                                            <th>Login</th>
                                            <td><?=$tbIUsers->login?></td>
                                        </tr>
                                        <tr>
                                            <th>Logout</th>
                                            <td><?=$tbIUsers->logout?></td>
                                        </tr>
                                        <tr>
                                            <th>IP Login</th>
                                            <td><?=$tbIUsers->ip_login?></td>
                                        </tr>
                                        <tr>
                                            <th>IP Logout</th>
                                            <td><?=$tbIUsers->ip_logout?></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <tr>
                                            <th>Pilihan 1</th>
                                            <td><?=$tbIPilihan->jurusan1?></td>
                                        </tr>
                                        <tr>
                                            <th>Pilihan 2</th>
                                            <td><?=$tbIPilihan->jurusan2?></td>
                                        </tr>
                                        <tr>
                                            <th>Pilihan 3</th>
                                            <td><?=$tbIPilihan->jurusan3?></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <div class="row">
            <div class="col-md-6"></div>
            <div class="col-md-6"></div>
        </div>
        <div class="row">
            <div class="col-md-6"></div>
            <div class="col-md-6"></div>
        </div>
    </section>
</div>