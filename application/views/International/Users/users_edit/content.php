<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Users
            <small><i>Detail</i></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-gears"></i> Users</a></li>
            <li><a href="#"></i> Detail</a></li>
            <li class="active"><i>Edit</i></li>
        </ol>
    </section>
    <section class="content">
        <?php if (!empty($this->session->flashdata('type_message'))) { ?>
            <div class="alert alert-<?=$this->session->flashdata('type_message')?> alert-dismissible text-center" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?=$this->session->flashdata('message');?>
            </div>
        <?php } ?>
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Tabel Users Detail</h3>
            </div>
            <div class="box-body">
                <?php $attributes = array('class' => 'form-horizontal', 'role' => 'form'); ?>
                <?=form_open_multipart('Internasional/Users/Simpan',$attributes)?>
                    <input type="hidden" class="form-control" name="nik_passport" value="<?=$tbIUsers->nik_passport;?>">
                    <div class="form-group">
                    <label class="col-md-3 control-label" for="noPassport">Passport Number</label>  
                    <div class="col-md-9">
                    <input id="noPassport" name="noPassport" type="text" minlength="8" maxlength="9" value="<?=$tbIUsers->nik_passport?>" placeholder="passport number" class="form-control input-md" required="" readonly="">
                        
                    </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                    <label class="col-md-3 control-label" for="nama">Name</label>  
                    <div class="col-md-9">
                    <input id="nama" name="nama" value="<?=$tbIUsers->nama?>" type="text" placeholder="Name" class="form-control input-md" required="">
                        
                    </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                    <label class="col-md-3 control-label" for="tempat">Place/Date of Birth</label>  
                    <div class="col-md-9">
                        <div class="row">
                        <div class="col-md-3">
                            <input id="tempat" name="tempat" value="<?=$tbIUsers->tempat?>" type="text" placeholder="Place" class="form-control input-md" required="">
                        </div>
                        <div class="col-md-8">
                            <input id="tglLahir" name="tglLahir" value="<?=$tbIUsers->tgl_lhr?>" type="date" placeholder="" class="form-control input-md" required="">
                        </div>
                        </div>
                    </div>
                    </div>

                    <!-- Select Basic -->
                    <div class="form-group">
                    <label class="col-md-3 control-label" for="jenisKelamin">Gender</label>
                    <div class="col-md-9">
                        <select id="jenisKelamin" name="jenisKelamin" class="form-control" required="">
                        <option>--Choose--</option>
                        <option value="L">Male</option>
                        <option value="P">Female</option>
                        </select>
                    </div>
                    </div>

                    <!-- Textarea -->
                    <div class="form-group">
                    <label class="col-md-3 control-label" for="alamat">Address</label>
                    <div class="col-md-9">                     
                        <textarea class="form-control" id="alamat" name="alamat" required=""><?=$tbIUsers->alamat?>
                        </textarea>
                    </div>
                    </div>

                    <!-- Select Basic -->
                    <div class="form-group">
                    <label class="col-md-3 control-label" for="kebangsaan">Nationality</label>
                    <div class="col-md-9">
                        <select id="kebangsaan" name="kebangsaan" class="form-control" required="">
                        <option>--Choose--</option>
                        <?php foreach($negara as $value): ?>
                            <option value="<?=$value->country_name?>" <?php if(strtoupper($tbIUsers->kebangsaan)==strtoupper($value->country_name)){ echo 'selected'; } ?>><?=$value->country_name?></option>
                        <?php endforeach; ?>
                        </select>
                    </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                    <label class="col-md-3 control-label" for="asalSekolah">Which school are you from?
        </label>  
                    <div class="col-md-9">
                    <input id="asalSekolah" value="<?=$tbIUsers->asal_sekolah?>" name="asalSekolah" type="text" placeholder="" class="form-control input-md" required="">
                        
                    </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                    <label class="col-md-3 control-label" for="email">Email</label>  
                    <div class="col-md-9">
                    <input id="email" name="email" type="email" value="<?=$tbIUsers->username?>" placeholder="Email" class="form-control input-md" required="">
                        
                    </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                    <label class="col-md-3 control-label" for="phone">Phone Number</label>  
                    <div class="col-md-9">
                    <input id="phone" name="phone" type="text" value="<?=$tbIUsers->noHp?>" placeholder="Phone Number" class="form-control input-md" required="">
                        
                    </div>
                    </div>

                    <!-- Select Basic -->
                    <div class="form-group">
                    <label class="col-md-3 control-label" for="jurusan1">First Majors Selection</label>
                    <div class="col-md-9">
                        <select id="jurusan1" name="jurusan1" class="form-control" required="">
                        <option>--Choose--</option>
                        <?php foreach($jurusan as $value): ?>
                            <option value="<?=$value->kode_jurusan?>" <?php if($tbIPilihan->jurusan_1==$value->kode_jurusan){ echo 'selected'; } ?>><?=$value->jurusan?></option>
                        <?php endforeach; ?>
                        </select>
                    </div>
                    </div>

                    <!-- Select Basic -->
                    <div class="form-group">
                    <label class="col-md-3 control-label" for="jurusan2">Second Majors Selection</label>
                    <div class="col-md-9">
                        <select id="jurusan2" name="jurusan2" class="form-control" required="">
                        <option>--Choose--</option>
                        <?php foreach($jurusan as $value): ?>
                            <option value="<?=$value->kode_jurusan?>" <?php if($tbIPilihan->jurusan_2==$value->kode_jurusan){ echo 'selected'; } ?>><?=$value->jurusan?></option>
                        <?php endforeach; ?>
                        </select>
                    </div>
                    </div>

                    <!-- Select Basic -->
                    <div class="form-group">
                    <label class="col-md-3 control-label" for="jurusan3">Third Majors Selection</label>
                    <div class="col-md-9">
                        <select id="jurusan3" name="jurusan3" class="form-control" required="">
                        <option>--Choose--</option>
                        <?php foreach($jurusan as $value): ?>
                            <option value="<?=$value->kode_jurusan?>" <?php if($tbIPilihan->jurusan_3==$value->kode_jurusan){ echo 'selected'; } ?>><?=$value->jurusan?></option>
                        <?php endforeach; ?>
                        </select>
                    </div>
                    </div>

                    <!-- Button -->
                    <div class="form-group">
                    <label class="col-md-3 control-label" for="simpan"></label>
                    <div class="col-md-9">
                        <button id="simpan" name="simpan" class="btn btn-primary">Submit</button>
                    </div>
                    </div>
                <?=form_close();?>
            </div>
        </div>
    </section>
</div>