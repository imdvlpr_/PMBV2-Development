<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>PMB - UIN Sunan Gunung Djati Bandung</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Icon -->
        <link href="<?=base_url('design-backend/images/logo.png'); ?>" rel="icon" type="image/x-icon" />
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="<?=base_url('design-backend/bootstrap/css/bootstrap.min.css');?>">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?=base_url('design-backend/dist/css/AdminLTE.min.css');?>">
        <link rel="stylesheet" href="<?=base_url('design-backend/dist/css/skins/skin-red-light.min.css');?>">
    </head>
    <body class="hold-transition skin-red-light sidebar-mini">
        <div class="wrapper">
            <?php $this->load->view('header-navbar');?>
            <?php $this->load->view('header-sidebar');?>
            <div class="content-wrapper">
                <section class="content-header">
                    <h1>
						Statistik
						<small><i>Kelulusan Jurusan</i></small>
					</h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-table"></i> Statistik</a></li>
						<li>Kelulusan Jurusan</li>
                    </ol>
                </section>
                <section class="content">
                    <?php if (!empty($this->session->flashdata('type_message'))) { ?>
                        <div class="alert alert-<?=$this->session->flashdata('type_message')?> alert-dismissible text-center" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <?=$this->session->flashdata('message');?>
                        </div>
                    <?php } ?>
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Statistik Kelulusan Jurusan</h3>
                        </div>
                        <div class="box-body">
							<div class="table-responsive">
								<table class="table table-bordered">
									<tr>
										<th>No</th>
										<th>Kode Jurusan</th>
										<th>Jurusan</th>
										<th>Fakultas</th>
										<th>Jumlah</th>
									</tr>
									<?php $no = 1; foreach ($viewSJurusan as $value) : ?>
									<tr>
										<td><?=$no?></td>
										<td><?=$value->kode_jurusan?></td>
										<td><?=$value->jurusan?></td>
										<td><?=$value->fakultas?></td>
										<td><?=$kelulusan[$value->kode_jurusan]?></td>
									</tr>
									<?php $no++; endforeach; ?>
								</table>
							</div>
                        </div>
                    </div>
                </section>
            </div>
            <?php $this->load->view('footer'); ?>
        </div>

        <!-- REQUIRED JS SCRIPTS -->
        <!-- jQuery 2.2.3 -->
        <script src="<?=base_url('design-backend/plugins/jQuery/jquery-2.2.3.min.js');?>"></script>
        <!-- Bootstrap 3.3.6 -->
        <script src="<?=base_url('design-backend/bootstrap/js/bootstrap.min.js');?>"></script>
        <!-- AdminLTE App -->
        <script src="<?=base_url('design-backend/dist/js/app.min.js');?>"></script>

    </body>
</html>
