<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Statistik
            <small><i>Pemilihan Jurusan</i></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-table"></i> Statistik</a></li>
            <li>Pemilihan Jurusan</li>
        </ol>
    </section>
    <section class="content">
        <?php if (!empty($this->session->flashdata('type_message'))) { ?>
            <div class="alert alert-<?=$this->session->flashdata('type_message')?> alert-dismissible text-center" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?=$this->session->flashdata('message');?>
            </div>
        <?php } ?>
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Statistik Pemilihan Jurusan</h3>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <tr>
                            <th>No</th>
                            <th>Kode Jurusan</th>
                            <th>Jurusan</th>
                            <th>Fakultas</th>
                            <th>Pilihan 1</th>
                            <th>Pilihan 2</th>
                            <th>Pilihan 3</th>
                            <th>Jumlah</th>
                        </tr>
                        <?php $no = 1; foreach ($viewSJurusan as $value) : ?>
                        <tr>
                            <td><?=$no?></td>
                            <td><?=$value->kode_jurusan?></td>
                            <td><?=$value->jurusan?></td>
                            <td><?=$value->fakultas?></td>
                            <td><?=$pilihan1[$value->kode_jurusan]?></td>
                            <td><?=$pilihan2[$value->kode_jurusan]?></td>
                            <td><?=$pilihan3[$value->kode_jurusan]?></td>
                            <td><?=$jumlah[$value->kode_jurusan]?></td>
                        </tr>
                        <?php $no++; endforeach; ?>
                    </table>
                </div>
            </div>
        </div>
    </section>
</div>