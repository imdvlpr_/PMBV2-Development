<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Statistik
            <small><i>Users</i></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-bar-chart"></i> Statistik</a></li>
            <li>Users</li>
        </ol>
    </section>
    <section class="content">
        <?php if (!empty($this->session->flashdata('type_message'))) { ?>
            <div class="alert alert-<?=$this->session->flashdata('type_message')?> alert-dismissible text-center" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?=$this->session->flashdata('message');?>
            </div>
        <?php } ?>

        <!-- Statistik Users -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Statistik Users</h3>
            </div>
            <div class="box-body bg-gray-light">
                <div class="row">
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-blue-active"><i class="fa fa-users"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Users</span>
                                <span class="info-box-number"><?=$num_users?></span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-green-active"><i class="fa fa-check"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Users Sudah Bayar</span>
                                <span class="info-box-number"><?=$num_users_sv?></span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->

                    <!-- fix for small devices only -->
                    <div class="clearfix visible-sm-block"></div>

                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-red-active"><i class="fa fa-close"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Users Belum Bayar</span>
                                <span class="info-box-number"><?=$num_users_bv?></span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->
                </div>
            </div>
        </div>

    </section>
</div>