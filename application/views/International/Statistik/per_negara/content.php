<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Statistik
            <small><i>Per Negara</i></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-table"></i> Statistik</a></li>
            <li>Per Negara</li>
        </ol>
    </section>
    <section class="content">
        <?php if (!empty($this->session->flashdata('type_message'))) { ?>
            <div class="alert alert-<?=$this->session->flashdata('type_message')?> alert-dismissible text-center" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?=$this->session->flashdata('message');?>
            </div>
        <?php } ?>
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Statistik Per Negara</h3>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <tr>
                            <th>No</th>
                            <th>Kode Negara</th>
                            <th>Negara</th>
                            <th>Jumlah</th>
                        </tr>
                        <?php $no = 1; foreach ($viewSNegara as $value) : ?>
                        <?php
                            if($jumlah[$value->id_negara]==0){
                                continue;
                            }
                        ?>
                        <tr>
                            <td><?=$no?></td>
                            <td><?=$value->country_code?></td>
                            <td><?=$value->country_name?></td>
                            <td><?=$jumlah[$value->id_negara]?></td>
                        </tr>
                        <?php $no++; endforeach; ?>
                    </table>
                </div>
            </div>
        </div>
    </section>
</div>