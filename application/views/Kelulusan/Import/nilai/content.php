<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Import
            <small><i>Nilai Kelulusan</i></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-upload"></i> Kelulusan</a></li>
            <li>Import</li>
            <li class="active">Nilai</li>
        </ol>
    </section>
    <section class="content">

        <!-- Notification -->
        <?php $this->load->view('notification') ?>

        <div class="row">
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Import Nilai Kelulusan</h3>
                    </div>
                    <div class="box-body">
                        <?=form_open_multipart('Kelulusan/Import/Nilai/Import')?>
                        <div class="form-group">
                            <label class="control-label">Upload file :</label>
                            <input type="file" name="userfile" required>
                        </div>
                        <button type="submit" class="btn btn-primary btn-flat pull-right"><i class="fa fa-upload">&nbsp;</i>Import</button>
                        <?=form_close()?>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Tatacara import</h3>
                    </div>
                    <div class="box-body">
                        <p>Tatacara import : (Only Windows)</p>
                        <ol>
                            <li>Download template yang sudah disediakan.</li>
                            <li>Isi kolom pada tabel sesuai nama kolomnya.</li>
                            <li>Pilih file yang ingin di upload.</li>
                            <li>Klik import.</li>
                        </ol>
                        <a href="<?=base_url('file/template_nilai_ujian_pradaftar.xlsx')?>" class="btn btn-default btn-flat" target="_blank">Download Template</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>