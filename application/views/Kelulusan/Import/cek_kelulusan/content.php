<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Import
            <small>Cek Kelulusan</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-upload"></i> Import</a></li>
            <li class="active">Cek Kelulusan</li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Import Data Cek Kelulusan</h3>
            </div>
            <div class="box-body">
                <?php if (!empty($this->session->flashdata('type_message'))) { ?>
                    <div class="alert alert-<?=$this->session->flashdata('type_message')?> alert-dismissible text-center" role="alert">
                        <?=$this->session->flashdata('message');?>
                    </div>
                <?php } ?>
                <div class="row">
                    <div class="col-md-6">
                        <?=form_open_multipart('Kelulusan/Import/CekKelulusan/Import')?>
                        <div class="form-group">
                            <label class="control-label">Upload file :</label>
                            <input type="file" name="userfile" required>
                        </div>
                        <button type="submit" class="btn btn-primary btn-flat pull-right"><i class="fa fa-upload">&nbsp;</i>Import</button>
                        <?=form_close()?>
                    </div>
                    <div class="col-md-6">
                        <p>Tatacara import data jadwal pradaftar : (Only Windows)</p>
                        <ol>
                            <li>Download template jadwal yang sudah disediakan.</li>
                            <li>Isi kolom pada tabel sesuai nama kolomnya.</li>
                            <li>Pilih file yang ingin di upload.</li>
                            <li>Klik import.</li>
                        </ol>
                        <a href="<?=base_url('file/template_kelulusan_pradaftar.xlsx')?>" class="btn btn-default btn-flat" target="_blank">Download Template</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>