<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Import
            <small>Kelulusan Afirmasi</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-upload"></i> Import</a></li>
            <li class="active">Kelulusan Afirmasi</li>
        </ol>
    </section>
    <section class="content">

        <!-- Notification -->
        <?php $this->load->view('notification') ?>

        <div class="row">
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Import Kelulusan Afirmasi</h3>
                    </div>
                    <div class="box-body">
                        <?=form_open_multipart('Kelulusan/Import/Afirmasi/Import')?>
                        <div class="form-group">
                            <label class="control-label">Upload file :</label>
                            <input type="file" name="userfile" required>
                        </div>
                        <button type="submit" class="btn btn-primary btn-flat pull-right"><i class="fa fa-upload">&nbsp;</i>Import</button>
                        <?=form_close()?>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Tatacara Import</h3>
                    </div>
                    <div class="box-body">
                        <p>Tatacara import data : (Only Windows)</p>
                        <ol>
                            <li>Download template yang sudah disediakan.</li>
                            <li>Isi kolom pada tabel sesuai nama kolomnya.</li>
                            <li>Pilih file yang ingin di upload.</li>
                            <li>Klik import.</li>
                        </ol>
                        <a href="<?=base_url('file/template_kelulusan_pradaftar.xlsx')?>" class="btn btn-default btn-flat" target="_blank">Download Template</a>
                    </div>
                </div>
            </div>
        </div>

        <?php $error = $this->session->flashdata('error'); if ($error > 0) : ?>
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Error</h3>
                </div>
                <div class="box-body">
                    <?=$this->session->flashdata('errorHtml')?>
                </div>
            </div>
        <?php endif; ?>

    </section>
</div>