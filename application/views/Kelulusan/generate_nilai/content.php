<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Generate
            <small><i>Nilai</i></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-database"></i> Generate</a></li>
            <li>Nilai</li>
        </ol>
    </section>
    <section class="content">

        <!-- Notification -->
        <?php $this->load->view('notification') ?>

        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Generate Nilai</h3>
                    </div>
                    <div class="box-body">
                        <?php if ($status): ?>
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <h4><i class="icon fa fa-check"></i> Server Computer Based Test</h4>
                                Connect to server Computer Based Test.
                            </div>
                            <div class="text-center">
                                <a href="<?=base_url('Kelulusan/GenerateNilai/Generate/')?>" class="btn btn-primary">Generate Nilai</a>
                            </div>
                        <?php else: ?>
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <h4><i class="icon fa fa-ban"></i> Server Computer Based Test</h4>
                                Could not connect to server Computer Based Test.
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>
    </section>
</div>