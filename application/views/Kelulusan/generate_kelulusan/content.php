<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Generate
            <small><i>Kelulusan</i></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-database"></i> Generate</a></li>
            <li>Kelulusan</li>
        </ol>
    </section>
    <section class="content">

        <!-- Notification -->
        <?php $this->load->view('notification') ?>

        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Generate Kelulusan</h3>
                    </div>
                    <div class="box-body">
                        <?=form_open_multipart('Kelulusan/GenerateKelulusan/Generate/')?>
                        <div class="form-group">
                            <label class="control-label">Pilihan :</label>
                            <select name="pilihan" class="form-control" required="">
                                <option value="1">Pilihan 1</option>
                                <option value="2">Pilihan 2</option>
                                <option value="3">Pilihan 3</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Tahun :</label>
                            <select name="tahun" class="form-control" required="">
                                <?php foreach ($tahun as $value): ?>
                                    <option value="<?=$value->tahun?>"><?=$value->tahun?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary btn-flat pull-right"><i class="fa fa-spinner">&nbsp;</i>Generate</button>
                        <?=form_close()?>
                    </div>
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>
    </section>
</div>