<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Export
            <small><i>Ujian</i></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-download"></i> Export</a></li>
            <li>Ujian</li>
        </ol>
    </section>
    <section class="content">
        <?php if (!empty($this->session->flashdata('type_message'))) { ?>
            <div class="alert alert-<?=$this->session->flashdata('type_message')?> alert-dismissible text-center" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?=$this->session->flashdata('message');?>
            </div>
        <?php } ?>
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Export Ujian</h3>
                    </div>
                    <div class="box-body">
                        <?=form_open_multipart('Kelulusan/Export/Ujian/actionExport/')?>
                        <div class="form-group">
                            <label class="control-label">Tahun :</label>
                            <select name="tipe_ujian" class="form-control" required="">
                                <option value="All">Semua</option>
                                <?php foreach ($tbSTipeUjian as $value): ?>
                                    <option value="<?=$value->tipe_ujian?>"><?=$value->tipe_ujian?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Tahun :</label>
                            <select name="tahun" class="form-control" required="">
                                <?php foreach ($tahun as $value): ?>
                                    <option value="<?=$value->tahun?>"><?=$value->tahun?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary btn-flat pull-right"><i class="fa fa-download">&nbsp;</i>Export</button>
                        <?=form_close()?>
                    </div>
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>
    </section>
</div>