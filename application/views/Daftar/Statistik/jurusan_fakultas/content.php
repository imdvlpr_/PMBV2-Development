<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Statistik
            <small><i>Jurusan & Fakultas (Sudah Verifikasi)</i></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-table"></i> Statistik</a></li>
            <li>Jurusan & Fakultas</li>
        </ol>
    </section>
    <section class="content">
        <?php $this->load->view('notification') ?>
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Statistik Jurusan Fakultas</h3>
                    </div>
                    <div class="box-body">
                        <?=form_open_multipart('Daftar/Statistik/JurusanFakultas/')?>
                        <div class="form-group">
                            <label class="control-label">Tahun :</label>
                            <select name="tahun" class="form-control" required="">
                                <?php foreach ($tahun as $value): ?>
                                    <option value="<?=$value->tahun?>"><?=$value->tahun?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary btn-flat pull-right"><i class="fa fa-download">&nbsp;</i>Tampilkan</button>
                        <?=form_close()?>
                    </div>
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>
        <?php if ($kosong == FALSE) : ?>
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Statistik Jurusan & Fakultas (Sudah Verifikasi) <?=$tahun_pilih?></h3>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <tr>
                            <th>No</th>
                            <th>Kode Jurusan</th>
                            <th>Jurusan</th>
                            <th>Fakultas</th>
                            <th>SNMPTN</th>
                            <th>SPAN-PTKIN</th>
                            <th>SBMPTN</th>
                            <th>UM-PTKIN</th>
                            <th>Mandiri</th>
                            <th>Jumlah</th>
                        </tr>
                        <?php $no = 1; foreach ($viewSJurusan as $value) : ?>
                        <tr>
                            <td><?=$no?></td>
                            <td><?=$value->kode_jurusan?></td>
                            <td><?=$value->jurusan?></td>
                            <td><?=$value->fakultas?></td>
                            <td><?=$snmptn[$value->kode_jurusan]?></td>
                            <td><?=$span[$value->kode_jurusan]?></td>
                            <td><?=$sbmptn[$value->kode_jurusan]?></td>
                            <td><?=$umptkin[$value->kode_jurusan]?></td>
                            <td><?=$mandiri[$value->kode_jurusan]?></td>
                            <td><?=$jumlah[$value->kode_jurusan]?></td>
                        </tr>
                        <?php $no++; endforeach; ?>
                    </table>
                </div>
            </div>
        </div>
        <?php endif; ?>
    </section>
</div>