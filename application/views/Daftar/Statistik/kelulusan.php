<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>PMB - UIN Sunan Gunung Djati Bandung</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Icon -->
        <link href="<?=base_url('design-backend/images/logo.png'); ?>" rel="icon" type="image/x-icon" />
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="<?=base_url('design-backend/bootstrap/css/bootstrap.min.css');?>">
		<!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?=base_url('design-backend/dist/css/AdminLTE.min.css');?>">
        <link rel="stylesheet" href="<?=base_url('design-backend/dist/css/skins/skin-red-light.min.css');?>">
    </head>
    <body class="hold-transition skin-red-light sidebar-mini">
        <div class="wrapper">
            <?php $this->load->view('header-navbar');?>
            <?php $this->load->view('header-sidebar');?>
            <div class="content-wrapper">
                <section class="content-header">
                    <h1>
						Statistik
						<small><i>Kelulusan</i></small>
					</h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-bar-chart"></i> Statistik</a></li>
						<li>Kelulusan</li>
                    </ol>
                </section>
                <section class="content">
                    <?php if (!empty($this->session->flashdata('type_message'))) { ?>
                        <div class="alert alert-<?=$this->session->flashdata('type_message')?> alert-dismissible text-center" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <?=$this->session->flashdata('message');?>
                        </div>
                    <?php } ?>

					<!-- Statistik Kelulusan -->
					<div class="box">
						<div class="box-header with-border">
							<h3 class="box-title">Statistik Kelulusan (Semua Jalur)</h3>
						</div>
						<div class="box-body bg-gray-light">
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="info-box">
										<span class="info-box-icon bg-blue-active"><i class="fa fa-users"></i></span>
										<div class="info-box-content">
											<span class="info-box-text">Kelulusan</span>
											<span class="info-box-number"><?=$num_kelulusan?></span>
										</div>
										<!-- /.info-box-content -->
									</div>
									<!-- /.info-box -->
								</div>
								<!-- /.col -->
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="info-box">
										<span class="info-box-icon bg-green-active"><i class="fa fa-check"></i></span>

										<div class="info-box-content">
											<span class="info-box-text">Kelulusan Sudah Daftar</span>
											<span class="info-box-number"><?=$num_kelulusan_sv?></span>
										</div>
										<!-- /.info-box-content -->
									</div>
									<!-- /.info-box -->
								</div>
								<!-- /.col -->

								<!-- fix for small devices only -->
								<div class="clearfix visible-sm-block"></div>

								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="info-box">
										<span class="info-box-icon bg-red-active"><i class="fa fa-close"></i></span>

										<div class="info-box-content">
											<span class="info-box-text">Kelulusan Belum Daftar</span>
											<span class="info-box-number"><?=$num_kelulusan_bv?></span>
										</div>
										<!-- /.info-box-content -->
									</div>
									<!-- /.info-box -->
								</div>
								<!-- /.col -->
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-4">
							<!-- Statistik Kelulusan SNMPTN-->
							<div class="box">
								<div class="box-header with-border">
									<h3 class="box-title">Statistik Kelulusan SNMPTN</h3>
								</div>
								<div class="box-body bg-gray-light">
									<div class="row">
										<div class="col-md-12 col-sm-6 col-xs-12">
											<div class="info-box">
												<span class="info-box-icon bg-blue-active"><i class="fa fa-users"></i></span>
												<div class="info-box-content">
													<span class="info-box-text">Kelulusan</span>
													<span class="info-box-number"><?=$num_kelulusan_snmptn?></span>
												</div>
												<!-- /.info-box-content -->
											</div>
											<!-- /.info-box -->
										</div>
										<!-- /.col -->
									</div>
									<div class="row">
										<div class="col-md-12 col-sm-6 col-xs-12">
											<div class="info-box">
												<span class="info-box-icon bg-green-active"><i class="fa fa-check"></i></span>

												<div class="info-box-content">
													<span class="info-box-text">Kelulusan Sudah Daftar</span>
													<span class="info-box-number"><?=$num_kelulusan_sd_snmptn?></span>
												</div>
												<!-- /.info-box-content -->
											</div>
											<!-- /.info-box -->
										</div>
										<!-- /.col -->
									</div>
										<!-- fix for small devices only -->
										<div class="clearfix visible-sm-block"></div>
									<div class="row">
										<div class="col-md-12 col-sm-6 col-xs-12">
											<div class="info-box">
												<span class="info-box-icon bg-red-active"><i class="fa fa-close"></i></span>

												<div class="info-box-content">
													<span class="info-box-text">Kelulusan Belum Daftar</span>
													<span class="info-box-number"><?=$num_kelulusan_bd_snmptn?></span>
												</div>
												<!-- /.info-box-content -->
											</div>
											<!-- /.info-box -->
										</div>
										<!-- /.col -->
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<!-- Statistik Kelulusan SPAN-->
							<div class="box">
								<div class="box-header with-border">
									<h3 class="box-title">Statistik Kelulusan SPAN-PTKIN</h3>
								</div>
								<div class="box-body bg-gray-light">
									<div class="row">
										<div class="col-md-12 col-sm-6 col-xs-12">
											<div class="info-box">
												<span class="info-box-icon bg-blue-active"><i class="fa fa-users"></i></span>
												<div class="info-box-content">
													<span class="info-box-text">Kelulusan</span>
													<span class="info-box-number"><?=$num_kelulusan_span?></span>
												</div>
												<!-- /.info-box-content -->
											</div>
											<!-- /.info-box -->
										</div>
										<!-- /.col -->
									</div>
									<div class="row">
										<div class="col-md-12 col-sm-6 col-xs-12">
											<div class="info-box">
												<span class="info-box-icon bg-green-active"><i class="fa fa-check"></i></span>

												<div class="info-box-content">
													<span class="info-box-text">Kelulusan Sudah Daftar</span>
													<span class="info-box-number"><?=$num_kelulusan_sd_span?></span>
												</div>
												<!-- /.info-box-content -->
											</div>
											<!-- /.info-box -->
										</div>
										<!-- /.col -->
									</div>
										<!-- fix for small devices only -->
										<div class="clearfix visible-sm-block"></div>
									<div class="row">
										<div class="col-md-12 col-sm-6 col-xs-12">
											<div class="info-box">
												<span class="info-box-icon bg-red-active"><i class="fa fa-close"></i></span>

												<div class="info-box-content">
													<span class="info-box-text">Kelulusan Belum Daftar</span>
													<span class="info-box-number"><?=$num_kelulusan_bd_span?></span>
												</div>
												<!-- /.info-box-content -->
											</div>
											<!-- /.info-box -->
										</div>
										<!-- /.col -->
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<!-- Statistik Kelulusan SBMPTN-->
							<div class="box">
								<div class="box-header with-border">
									<h3 class="box-title">Statistik Kelulusan SBMPTN</h3>
								</div>
								<div class="box-body bg-gray-light">
									<div class="row">
										<div class="col-md-12 col-sm-6 col-xs-12">
											<div class="info-box">
												<span class="info-box-icon bg-blue-active"><i class="fa fa-users"></i></span>
												<div class="info-box-content">
													<span class="info-box-text">Kelulusan</span>
													<span class="info-box-number"><?=$num_kelulusan_sbmptn?></span>
												</div>
												<!-- /.info-box-content -->
											</div>
											<!-- /.info-box -->
										</div>
										<!-- /.col -->
									</div>
									<div class="row">
										<div class="col-md-12 col-sm-6 col-xs-12">
											<div class="info-box">
												<span class="info-box-icon bg-green-active"><i class="fa fa-check"></i></span>

												<div class="info-box-content">
													<span class="info-box-text">Kelulusan Sudah Daftar</span>
													<span class="info-box-number"><?=$num_kelulusan_sd_sbmptn?></span>
												</div>
												<!-- /.info-box-content -->
											</div>
											<!-- /.info-box -->
										</div>
										<!-- /.col -->
									</div>
										<!-- fix for small devices only -->
										<div class="clearfix visible-sm-block"></div>
									<div class="row">
										<div class="col-md-12 col-sm-6 col-xs-12">
											<div class="info-box">
												<span class="info-box-icon bg-red-active"><i class="fa fa-close"></i></span>

												<div class="info-box-content">
													<span class="info-box-text">Kelulusan Belum Daftar</span>
													<span class="info-box-number"><?=$num_kelulusan_bd_sbmptn?></span>
												</div>
												<!-- /.info-box-content -->
											</div>
											<!-- /.info-box -->
										</div>
										<!-- /.col -->
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-6">
							<!-- Statistik Kelulusan UMPTKIN-->
							<div class="box">
								<div class="box-header with-border">
									<h3 class="box-title">Statistik Kelulusan UM-PTKIN</h3>
								</div>
								<div class="box-body bg-gray-light">
									<div class="row">
										<div class="col-md-12 col-sm-6 col-xs-12">
											<div class="info-box">
												<span class="info-box-icon bg-blue-active"><i class="fa fa-users"></i></span>
												<div class="info-box-content">
													<span class="info-box-text">Kelulusan</span>
													<span class="info-box-number"><?=$num_kelulusan_umptkin?></span>
												</div>
												<!-- /.info-box-content -->
											</div>
											<!-- /.info-box -->
										</div>
										<!-- /.col -->
									</div>
									<div class="row">
										<div class="col-md-12 col-sm-6 col-xs-12">
											<div class="info-box">
												<span class="info-box-icon bg-green-active"><i class="fa fa-check"></i></span>

												<div class="info-box-content">
													<span class="info-box-text">Kelulusan Sudah Daftar</span>
													<span class="info-box-number"><?=$num_kelulusan_sd_umptkin?></span>
												</div>
												<!-- /.info-box-content -->
											</div>
											<!-- /.info-box -->
										</div>
										<!-- /.col -->
									</div>
										<!-- fix for small devices only -->
										<div class="clearfix visible-sm-block"></div>
									<div class="row">
										<div class="col-md-12 col-sm-6 col-xs-12">
											<div class="info-box">
												<span class="info-box-icon bg-red-active"><i class="fa fa-close"></i></span>

												<div class="info-box-content">
													<span class="info-box-text">Kelulusan Belum Daftar</span>
													<span class="info-box-number"><?=$num_kelulusan_bd_umptkin?></span>
												</div>
												<!-- /.info-box-content -->
											</div>
											<!-- /.info-box -->
										</div>
										<!-- /.col -->
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<!-- Statistik Kelulusan Mandiri-->
							<div class="box">
								<div class="box-header with-border">
									<h3 class="box-title">Statistik Kelulusan Mandiri</h3>
								</div>
								<div class="box-body bg-gray-light">
									<div class="row">
										<div class="col-md-12 col-sm-6 col-xs-12">
											<div class="info-box">
												<span class="info-box-icon bg-blue-active"><i class="fa fa-users"></i></span>
												<div class="info-box-content">
													<span class="info-box-text">Kelulusan</span>
													<span class="info-box-number"><?=$num_kelulusan_mandiri?></span>
												</div>
												<!-- /.info-box-content -->
											</div>
											<!-- /.info-box -->
										</div>
										<!-- /.col -->
									</div>
									<div class="row">
										<div class="col-md-12 col-sm-6 col-xs-12">
											<div class="info-box">
												<span class="info-box-icon bg-green-active"><i class="fa fa-check"></i></span>

												<div class="info-box-content">
													<span class="info-box-text">Kelulusan Sudah Daftar</span>
													<span class="info-box-number"><?=$num_kelulusan_sd_mandiri?></span>
												</div>
												<!-- /.info-box-content -->
											</div>
											<!-- /.info-box -->
										</div>
										<!-- /.col -->
									</div>
										<!-- fix for small devices only -->
										<div class="clearfix visible-sm-block"></div>
									<div class="row">
										<div class="col-md-12 col-sm-6 col-xs-12">
											<div class="info-box">
												<span class="info-box-icon bg-red-active"><i class="fa fa-close"></i></span>

												<div class="info-box-content">
													<span class="info-box-text">Kelulusan Belum Daftar</span>
													<span class="info-box-number"><?=$num_kelulusan_bd_mandiri?></span>
												</div>
												<!-- /.info-box-content -->
											</div>
											<!-- /.info-box -->
										</div>
										<!-- /.col -->
									</div>
								</div>
							</div>
						</div>
					</div>

                </section>
            </div>
            <?php $this->load->view('footer'); ?>
        </div>

        <!-- REQUIRED JS SCRIPTS -->
        <!-- jQuery 2.2.3 -->
        <script src="<?=base_url('design-backend/plugins/jQuery/jquery-2.2.3.min.js');?>"></script>
        <!-- Bootstrap 3.3.6 -->
        <script src="<?=base_url('design-backend/bootstrap/js/bootstrap.min.js');?>"></script>
        <!-- AdminLTE App -->
        <script src="<?=base_url('design-backend/dist/js/app.min.js');?>"></script>

    </body>
</html>
