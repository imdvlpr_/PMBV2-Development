<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Statistik
            <small><i>Kelulusan</i></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-bar-chart"></i> Statistik</a></li>
            <li>Kelulusan</li>
        </ol>
    </section>
    <section class="content">
        <?php $this->load->view('notification') ?>

        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Statistik Kelulusan</h3>
                    </div>
                    <div class="box-body">
                        <?=form_open_multipart('Daftar/Statistik/Kelulusan/')?>
                        <div class="form-group">
                            <label class="control-label">Tahun :</label>
                            <select name="tahun" class="form-control" required="">
                                <?php foreach ($tahun as $value): ?>
                                    <option value="<?=$value->tahun?>"><?=$value->tahun?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary btn-flat pull-right"><i class="fa fa-download">&nbsp;</i>Tampilkan</button>
                        <?=form_close()?>
                    </div>
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>

        <!-- Statistik Kelulusan -->
        <?php if ($kosong == FALSE) : ?>
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Statistik Kelulusan (Semua Jalur) <?=$tahun_pilih?></h3>
            </div>
            <div class="box-body bg-gray-light">
                <div class="row">
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-blue-active"><i class="fa fa-users"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Kelulusan</span>
                                <span class="info-box-number"><?=$num_kelulusan?></span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-green-active"><i class="fa fa-check"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Kelulusan Sudah Daftar</span>
                                <span class="info-box-number"><?=$num_kelulusan_sd?></span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->

                    <!-- fix for small devices only -->
                    <div class="clearfix visible-sm-block"></div>

                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-red-active"><i class="fa fa-close"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Kelulusan Belum Daftar</span>
                                <span class="info-box-number"><?=$num_kelulusan_bd?></span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <!-- Statistik Kelulusan SNMP <?=$tahun_pilih?>TN-->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Statistik Kelulusan SNMPTN <?=$tahun_pilih?></h3>
                    </div>
                    <div class="box-body bg-gray-light">
                        <div class="row">
                            <div class="col-md-12 col-sm-6 col-xs-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-blue-active"><i class="fa fa-users"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text">Kelulusan</span>
                                        <span class="info-box-number"><?=$num_kelulusan_snmptn?></span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-sm-6 col-xs-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-green-active"><i class="fa fa-check"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">Kelulusan Sudah Daftar</span>
                                        <span class="info-box-number"><?=$num_kelulusan_sd_snmptn?></span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            <!-- /.col -->
                        </div>
                            <!-- fix for small devices only -->
                            <div class="clearfix visible-sm-block"></div>
                        <div class="row">
                            <div class="col-md-12 col-sm-6 col-xs-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-red-active"><i class="fa fa-close"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">Kelulusan Belum Daftar</span>
                                        <span class="info-box-number"><?=$num_kelulusan_bd_snmptn?></span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            <!-- /.col -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <!-- Statistik Kelulusan SP <?=$tahun_pilih?>AN-->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Statistik Kelulusan SPAN-PTKIN <?=$tahun_pilih?></h3>
                    </div>
                    <div class="box-body bg-gray-light">
                        <div class="row">
                            <div class="col-md-12 col-sm-6 col-xs-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-blue-active"><i class="fa fa-users"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text">Kelulusan</span>
                                        <span class="info-box-number"><?=$num_kelulusan_span?></span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-sm-6 col-xs-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-green-active"><i class="fa fa-check"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">Kelulusan Sudah Daftar</span>
                                        <span class="info-box-number"><?=$num_kelulusan_sd_span?></span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            <!-- /.col -->
                        </div>
                            <!-- fix for small devices only -->
                            <div class="clearfix visible-sm-block"></div>
                        <div class="row">
                            <div class="col-md-12 col-sm-6 col-xs-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-red-active"><i class="fa fa-close"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">Kelulusan Belum Daftar</span>
                                        <span class="info-box-number"><?=$num_kelulusan_bd_span?></span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            <!-- /.col -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <!-- Statistik Kelulusan SBMP <?=$tahun_pilih?>TN-->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Statistik Kelulusan SBMPTN <?=$tahun_pilih?></h3>
                    </div>
                    <div class="box-body bg-gray-light">
                        <div class="row">
                            <div class="col-md-12 col-sm-6 col-xs-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-blue-active"><i class="fa fa-users"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text">Kelulusan</span>
                                        <span class="info-box-number"><?=$num_kelulusan_sbmptn?></span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-sm-6 col-xs-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-green-active"><i class="fa fa-check"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">Kelulusan Sudah Daftar</span>
                                        <span class="info-box-number"><?=$num_kelulusan_sd_sbmptn?></span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            <!-- /.col -->
                        </div>
                            <!-- fix for small devices only -->
                            <div class="clearfix visible-sm-block"></div>
                        <div class="row">
                            <div class="col-md-12 col-sm-6 col-xs-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-red-active"><i class="fa fa-close"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">Kelulusan Belum Daftar</span>
                                        <span class="info-box-number"><?=$num_kelulusan_bd_sbmptn?></span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            <!-- /.col -->
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <!-- Statistik Kelulusan UMPTK <?=$tahun_pilih?>IN-->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Statistik Kelulusan UM-PTKIN <?=$tahun_pilih?></h3>
                    </div>
                    <div class="box-body bg-gray-light">
                        <div class="row">
                            <div class="col-md-12 col-sm-6 col-xs-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-blue-active"><i class="fa fa-users"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text">Kelulusan</span>
                                        <span class="info-box-number"><?=$num_kelulusan_umptkin?></span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-sm-6 col-xs-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-green-active"><i class="fa fa-check"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">Kelulusan Sudah Daftar</span>
                                        <span class="info-box-number"><?=$num_kelulusan_sd_umptkin?></span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            <!-- /.col -->
                        </div>
                            <!-- fix for small devices only -->
                            <div class="clearfix visible-sm-block"></div>
                        <div class="row">
                            <div class="col-md-12 col-sm-6 col-xs-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-red-active"><i class="fa fa-close"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">Kelulusan Belum Daftar</span>
                                        <span class="info-box-number"><?=$num_kelulusan_bd_umptkin?></span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            <!-- /.col -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <!-- Statistik Kelulusan Mandi <?=$tahun_pilih?>ri-->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Statistik Kelulusan Mandiri <?=$tahun_pilih?></h3>
                    </div>
                    <div class="box-body bg-gray-light">
                        <div class="row">
                            <div class="col-md-12 col-sm-6 col-xs-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-blue-active"><i class="fa fa-users"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text">Kelulusan</span>
                                        <span class="info-box-number"><?=$num_kelulusan_mandiri?></span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-sm-6 col-xs-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-green-active"><i class="fa fa-check"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">Kelulusan Sudah Daftar</span>
                                        <span class="info-box-number"><?=$num_kelulusan_sd_mandiri?></span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            <!-- /.col -->
                        </div>
                            <!-- fix for small devices only -->
                            <div class="clearfix visible-sm-block"></div>
                        <div class="row">
                            <div class="col-md-12 col-sm-6 col-xs-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-red-active"><i class="fa fa-close"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">Kelulusan Belum Daftar</span>
                                        <span class="info-box-number"><?=$num_kelulusan_bd_mandiri?></span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            <!-- /.col -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php endif; ?>
    </section>
</div>