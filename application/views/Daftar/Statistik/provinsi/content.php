<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Statistik
            <small><i>Provinsi</i></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-table"></i> Statistik</a></li>
            <li>Provinsi</li>
        </ol>
    </section>
    <section class="content">
        <?php $this->load->view('notification') ?>
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Statistik Provinsi</h3>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <tr>
                            <th>No</th>
                            <th>Provinsi</th>
                            <th>Jumlah</th>
                        </tr>
                        <?php $no = 1; foreach ($tbSProvinsi as $value) : ?>
                        <tr>
                            <td><?=$no?></td>
                            <td><?=$value->provinsi?></td>
                            <td><?=$provinsi[$value->id_provinsi]?></td>
                        </tr>
                        <?php $no++; endforeach; ?>
                    </table>
                </div>
            </div>
        </div>
    </section>
</div>