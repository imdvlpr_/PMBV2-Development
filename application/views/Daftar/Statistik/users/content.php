<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Statistik
            <small><i>Users</i></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-download"></i> Statistik</a></li>
            <li>Users</li>
        </ol>
    </section>
    <section class="content">
        <?php if (!empty($this->session->flashdata('type_message'))) { ?>
            <div class="alert alert-<?=$this->session->flashdata('type_message')?> alert-dismissible text-center" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?=$this->session->flashdata('message');?>
            </div>
        <?php } ?>
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Statistik Users</h3>
                    </div>
                    <div class="box-body">
                        <?=form_open_multipart('Daftar/Statistik/Users/')?>
                        <div class="form-group">
                            <label class="control-label">Tahun :</label>
                            <select name="tahun" class="form-control" required="">
                                <?php foreach ($tahun as $value): ?>
                                    <option value="<?=$value->tahun?>"><?=$value->tahun?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary btn-flat pull-right"><i class="fa fa-download">&nbsp;</i>Tampilkan</button>
                        <?=form_close()?>
                    </div>
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>
        <?php if ($kosong == FALSE) : ?>
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Statistik Users (Semua Jalur) <?=$tahun_pilih?></h3>
            </div>
            <div class="box-body bg-gray-light">
                <div class="row">
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-blue-active"><i class="fa fa-users"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Users</span>
                                <span class="info-box-number"><?=$num_users?></span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-green-active"><i class="fa fa-check"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Users Sudah Verifikasi</span>
                                <span class="info-box-number"><?=$num_users_sv?></span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->

                    <!-- fix for small devices only -->
                    <div class="clearfix visible-sm-block"></div>

                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-red-active"><i class="fa fa-close"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Users Belum Verifikasi</span>
                                <span class="info-box-number"><?=$num_users_bv?></span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <!-- Statistik Users SNMPTN-->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Statistik Users SNMPTN <?=$tahun_pilih?></h3>
                    </div>
                    <div class="box-body bg-gray-light">
                        <div class="row">
                            <div class="col-md-12 col-sm-6 col-xs-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-blue-active"><i class="fa fa-users"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text">Users</span>
                                        <span class="info-box-number"><?=$num_users_snmptn?></span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-sm-6 col-xs-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-green-active"><i class="fa fa-check"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">Users Sudah Verifikasi</span>
                                        <span class="info-box-number"><?=$num_users_sv_snmptn?></span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            <!-- /.col -->
                        </div>
                            <!-- fix for small devices only -->
                            <div class="clearfix visible-sm-block"></div>
                        <div class="row">
                            <div class="col-md-12 col-sm-6 col-xs-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-red-active"><i class="fa fa-close"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">Users Belum Verifikasi</span>
                                        <span class="info-box-number"><?=$num_users_bv_snmptn?></span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            <!-- /.col -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <!-- Statistik Users SPAN-->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Statistik Users SPAN-PTKIN <?=$tahun_pilih?></h3>
                    </div>
                    <div class="box-body bg-gray-light">
                        <div class="row">
                            <div class="col-md-12 col-sm-6 col-xs-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-blue-active"><i class="fa fa-users"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text">Users</span>
                                        <span class="info-box-number"><?=$num_users_span?></span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-sm-6 col-xs-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-green-active"><i class="fa fa-check"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">Users Sudah Verifikasi</span>
                                        <span class="info-box-number"><?=$num_users_sv_span?></span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            <!-- /.col -->
                        </div>
                            <!-- fix for small devices only -->
                            <div class="clearfix visible-sm-block"></div>
                        <div class="row">
                            <div class="col-md-12 col-sm-6 col-xs-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-red-active"><i class="fa fa-close"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">Users Belum Verifikasi</span>
                                        <span class="info-box-number"><?=$num_users_bv_span?></span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            <!-- /.col -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <!-- Statistik Users SBMPTN-->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Statistik Users SBMPTN <?=$tahun_pilih?></h3>
                    </div>
                    <div class="box-body bg-gray-light">
                        <div class="row">
                            <div class="col-md-12 col-sm-6 col-xs-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-blue-active"><i class="fa fa-users"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text">Users</span>
                                        <span class="info-box-number"><?=$num_users_sbmptn?></span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-sm-6 col-xs-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-green-active"><i class="fa fa-check"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">Users Sudah Verifikasi</span>
                                        <span class="info-box-number"><?=$num_users_sv_sbmptn?></span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            <!-- /.col -->
                        </div>
                            <!-- fix for small devices only -->
                            <div class="clearfix visible-sm-block"></div>
                        <div class="row">
                            <div class="col-md-12 col-sm-6 col-xs-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-red-active"><i class="fa fa-close"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">Users Belum Verifikasi</span>
                                        <span class="info-box-number"><?=$num_users_bv_sbmptn?></span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            <!-- /.col -->
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <!-- Statistik Users UMPTKIN-->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Statistik Users UM-PTKIN <?=$tahun_pilih?></h3>
                    </div>
                    <div class="box-body bg-gray-light">
                        <div class="row">
                            <div class="col-md-12 col-sm-6 col-xs-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-blue-active"><i class="fa fa-users"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text">Users</span>
                                        <span class="info-box-number"><?=$num_users_umptkin?></span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-sm-6 col-xs-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-green-active"><i class="fa fa-check"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">Users Sudah Verifikasi</span>
                                        <span class="info-box-number"><?=$num_users_sv_umptkin?></span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            <!-- /.col -->
                        </div>
                            <!-- fix for small devices only -->
                            <div class="clearfix visible-sm-block"></div>
                        <div class="row">
                            <div class="col-md-12 col-sm-6 col-xs-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-red-active"><i class="fa fa-close"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">Users Belum Verifikasi</span>
                                        <span class="info-box-number"><?=$num_users_bv_umptkin?></span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            <!-- /.col -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <!-- Statistik Users Mandiri-->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Statistik Users Mandiri <?=$tahun_pilih?></h3>
                    </div>
                    <div class="box-body bg-gray-light">
                        <div class="row">
                            <div class="col-md-12 col-sm-6 col-xs-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-blue-active"><i class="fa fa-users"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text">Users</span>
                                        <span class="info-box-number"><?=$num_users_mandiri?></span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-sm-6 col-xs-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-green-active"><i class="fa fa-check"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">Users Sudah Verifikasi</span>
                                        <span class="info-box-number"><?=$num_users_sv_mandiri?></span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            <!-- /.col -->
                        </div>
                            <!-- fix for small devices only -->
                            <div class="clearfix visible-sm-block"></div>
                        <div class="row">
                            <div class="col-md-12 col-sm-6 col-xs-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-red-active"><i class="fa fa-close"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">Users Belum Verifikasi</span>
                                        <span class="info-box-number"><?=$num_users_bv_mandiri?></span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            <!-- /.col -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php endif; ?>
    </section>
</div>