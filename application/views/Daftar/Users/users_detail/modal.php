<!-- View Image -->
<div class="modal fade" id="viewImage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">View Image</h4>
            </div>
            <div class="modal-body">
                <?php if (!empty($viewDaftarUsers->foto)) { ?>
                    <img class="img-responsive center-block" src="<?php echo 'https://pmb-daftar.uinsgd.ac.id/upload/'.date('Y').'/foto/'.$viewDaftarUsers->foto;?>" alt="foto profile">
                <?php }else{?>
                    <img class="img-responsive center-block" src="<?=base_url('design-backend/dist/img/avatar.png');?>" alt="foto profile">
                <?php } ?>
            </div>
        </div>
    </div>
</div>

<!-- View Image -->
<div class="modal fade" id="EditUsers">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit Data Users</h4>
            </div>
            <?=form_open('Daftar/Users/Update/'.$viewDaftarUsers->id_daftar_users.'/'.$viewDaftarUsers->nomor_peserta)?>
            <div class="modal-body">
                <div class="form-group has-feedback">
                    <label for="recipient-name" class="control-label">Nomor Induk Kependudukan :</label>
                    <input type="number" min="1000000000000000" max="9999999999999999" value="<?=$viewDaftarUsers->nik_passport?>" class="form-control" placeholder="Nomor Induk Kependudukan" name="nik" required="">
                </div>
                <div class="form-group has-feedback">
                    <label for="recipient-name" class="control-label">Tanggal Lahir :</label>
                    <input type="text" readonly="" class="form-control datepicker" value="<?=$viewDaftarUsers->tgl_lhr?>" placeholder="yyyy-mm-dd" name="tgl" required="">
                </div>
                <div class="form-group has-feedback">
                    <label for="recipient-name" class="control-label">Nomor Kontak :</label>
                    <input type="text" minlength="10" value="<?=$viewDaftarUsers->nmr_hp?>" class="form-control" placeholder="Nomor Kontak" name="kontak" required="">
                </div>
                <div class="form-group has-feedback">
                    <label for="recipient-name" class="control-label">Email :</label>
                    <input type="email" class="form-control" placeholder="Email" name="email" required="">
                    <span class="label label-danger">Email Aktif</span>
                </div>
                <div class="form-group has-feedback">
                    <label for="recipient-name" class="control-label">Password :</label>
                    <input type="password" class="form-control" placeholder="Password" name="password">
                    <span class="label label-warning">Isi jika ingin dirubah</span>
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Status Verifikasi :</label>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <input type="radio" name="verifikasi" class="forn-control" value="SUDAH VERIFIKASI" <?php if($viewDaftarUsers->verifikasi=="SUDAH VERIFIKASI"){ echo 'checked';} ?>>
                                </span>
                                <input type="text" class="form-control" aria-label="Aktif" value="SUDAH VERIFIKASI" readonly>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <input type="radio" name="verifikasi" class="forn-control" value="BELUM VERIFIKASI" <?php if($viewDaftarUsers->verifikasi=="BELUM VERIFIKASI"){ echo 'checked';} ?>>
                                </span>
                                <input type="text" class="form-control" aria-label="Aktif" value="BELUM VERIFIKASI" readonly>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
            <?=form_close();?>
        </div>
    </div>
</div>