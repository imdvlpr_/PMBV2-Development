<!-- Datepicker Bootstrap -->
<script src="<?=base_url('assets/js/bootstrap-datepicker.min.js');?>"></script>
<script type="text/javascript">
    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd',
        orientation: "bottom auto",
        autoclose: true
    });
</script>