<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Users
            <small><i>Detail</i></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-gears"></i> Users</a></li>
            <li><a href="#"></i> Detail</a></li>
            <li class="active"><i>Edit</i></li>
        </ol>
    </section>
    <section class="content">
        <?php if (!empty($this->session->flashdata('type_message'))) { ?>
            <div class="alert alert-<?=$this->session->flashdata('type_message')?> alert-dismissible text-center" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?=$this->session->flashdata('message');?>
            </div>
        <?php } ?>
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Langkah-langkah</h3>
            </div>
            <center>
            <div class="box-body">
                <div class="list-group">
                <a href="<?=base_url('Daftar/Users/Edit_langkah1/'.$viewDaftarUsers->id_daftar_users)?>" class="list-group-item" style="float: left">Langkah 1</a>
                <a href="<?=base_url('Daftar/Users/Edit_langkah2/'.$viewDaftarUsers->id_daftar_users)?>" class="list-group-item" style="float: left">Langkah 2</a>
                <a href="<?=base_url('Daftar/Users/Edit_langkah3/'.$viewDaftarUsers->id_daftar_users)?>" class="list-group-item" style="float: left">Langkah 3</a>
                <a href="#" class="list-group-item active" style="float: left">Langkah 4</a>
                <a href="<?=base_url('Daftar/Users/Edit_langkah5/'.$viewDaftarUsers->id_daftar_users)?>" class="list-group-item" style="float: left">Langkah 5</a>
                <a href="<?=base_url('Daftar/Users/Edit_langkah6/'.$viewDaftarUsers->id_daftar_users)?>" class="list-group-item" style="float: left">Langkah 6</a>
                </div>
            </div>
            </center>
        </div>
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Tabel Users Detail</h3>
                <div class="pull-right">
                    <button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
                </div>
            </div>
            <div class="box-body">
            <div class="row">
                  <div class="col-md-3">
                    <br>
                    <ul class="nav flex-column" id="myTab" role="tablist" aria-orientation="vertical">
                      <li class="nav-item">
                        <a class="btn btn-primary" id="ayah-tab" data-toggle="pill" href="#ayah" role="tab" aria-controls="ayah" aria-selected="true">Ayah</a>
                        <br>
                      </li>
                      <li class="nav-item">
                        <a class="btn btn-primary" id="ibu-tab" data-toggle="pill" href="#ibu" role="tab" aria-controls="ibu" aria-selected="false">Ibu</a>
                        <br>
                      </li>
                      <li class="nav-item">
                        <a class="btn btn-primary" id="wali-tab" data-toggle="pill" href="#wali" role="tab" aria-controls="wali" aria-selected="false">Wali</a>
                        <br>
                      </li>
                    </ul>
                  </div>
                  <div class="col-md-9">
                    <div class="tab-content" id="myTabContent">
                      <div class="tab-pane fade" id="ayah" role="tabpanel" aria-labelledby="ayah-tab">
                      <?php $attributes = array('class' => 'form-horizontal', 'role' => 'form'); ?>
                      <?=form_open_multipart('Daftar/Users/Simpan_tahap4',$attributes)?>
                      <input type="hidden" name="id_daftar_users" value="<?php if(!empty($viewDOrangtuaAyah->id_daftar_users)){ echo $viewDOrangtuaAyah->id_daftar_users;};?>">
                        <div id="ayah" class="display:block">
                        <div class="form-group">
                            <input type="hidden" class="form-control" name="orangtua" value="Ayah">
                        </div>
                        <div class="form-group">
                          <label class="col-sm-3 control-label">NIK Ayah</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="nik_orangtua" value="<?php if(!empty($viewDOrangtuaAyah->nik_orangtua)){ echo $viewDOrangtuaAyah->nik_orangtua;};?>">
                          </div>
                          <div class="col-sm-offset-3 col-sm-9">
                            <span class="label label-danger">Wajib diisi</span>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-3 control-label">Nama Ayah</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="nama_orangtua" value="<?php if(!empty($viewDOrangtuaAyah->nama_orangtua)){ echo $viewDOrangtuaAyah->nama_orangtua;};?>">
                          </div>
                          <div class="col-sm-offset-3 col-sm-9">
                            <span class="label label-danger">Wajib diisi</span>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-3 control-label">Tanggal Lahir Ayah</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control datepicker" name="tgl_lhr_orangtua" value="<?php if(!empty($viewDOrangtuaAyah->tgl_lhr_orangtua)){ echo $viewDOrangtuaAyah->tgl_lhr_orangtua;};?>">
                          </div>
                          <div class="col-sm-offset-3 col-sm-9">
                            <span class="label label-danger">Wajib diisi</span>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-3 control-label">Pendidikan Ayah</label>
                          <div class="col-sm-9">
                            <select class="form-control" name="id_pendidikan">
                            <?php foreach ($tbPendidikan as $key => $value): ?>
                              <?php if ($value->id_pendidikan == $viewDOrangtuaAyah->id_pendidikan) { ?>
                                <option value="<?=$value->id_pendidikan;?>" selected=""><?=$value->pendidikan;?></option>
                              <?php }else{ ?>
                                <option value="<?=$value->id_pendidikan;?>"><?=$value->pendidikan;?></option>
                              <?php } ?>
                            <?php endforeach ?>
                            </select>
                          </div>
                          <div class="col-sm-offset-3 col-sm-9">
                            <span class="label label-danger">Wajib diisi</span>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-3 control-label">Pekerjaan Ayah</label>
                          <div class="col-sm-9">
                            <select class="form-control" name="id_pekerjaan">
                            <?php foreach ($tbPekerjaan as $key => $value): ?>
                              <?php if ($value->id_pekerjaan == $viewDOrangtuaAyah->id_pekerjaan) { ?>
                                <option value="<?=$value->id_pekerjaan;?>" selected=""><?=$value->pekerjaan;?></option>
                              <?php }else{ ?>
                                <option value="<?=$value->id_pekerjaan;?>"><?=$value->pekerjaan;?></option>
                              <?php } ?>
                            <?php endforeach ?>
                            </select>
                          </div>
                          <div class="col-sm-offset-3 col-sm-9">
                            <span class="label label-danger">Wajib diisi</span>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-3 control-label">Penghasilan Ayah</label>
                          <div class="col-sm-9">
                            <select class="form-control" name="id_penghasilan">
                            <?php foreach ($tbPenghasilan as $key => $value): ?>
                              <?php if ($value->id_penghasilan == $viewDOrangtuaAyah->id_penghasilan) { ?>
                                <option value="<?=$value->id_penghasilan;?>" selected=""><?=$value->penghasilan;?></option>
                              <?php }else{ ?>
                                <option value="<?=$value->id_penghasilan;?>"><?=$value->penghasilan;?></option>
                              <?php } ?>
                            <?php endforeach ?>
                            </select>
                          </div>
                          <div class="col-sm-offset-3 col-sm-9">
                            <span class="label label-danger">Wajib diisi</span>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-3 control-label">Nominal Penghasilan Ayah</label>
                          <div class="col-sm-9">
                            <input type="number" class="form-control" name="nominal_penghasilan" value="<?php if(!empty($viewDOrangtuaAyah->nominal_penghasilan)){ echo $viewDOrangtuaAyah->nominal_penghasilan;};?>" onkeypress="return hanyaAngka(event)" min=0>
                          </div>
                          <div class="col-sm-offset-3 col-sm-9">
                            <span class="label label-danger">Wajib diisi</span>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-3 control-label">Terbilang Penghasilan Ayah</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="terbilang_penghasilan" value="<?php if(!empty($viewDOrangtuaAyah->terbilang_penghasilan)){ echo $viewDOrangtuaAyah->terbilang_penghasilan;};?>">
                          </div>
                          <div class="col-sm-offset-3 col-sm-9">
                            <span class="label label-danger">Wajib diisi</span>
                          </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                          <div class="col-sm-offset-3 col-sm-9">
                            <button type="submit" class="btn btn-primary">SIMPAN</button>
                          </div>
                        </div>
                        <?=form_close(); ?>
                      </div>
                      <div class="tab-pane fade" id="ibu" role="tabpanel" aria-labelledby="ibu-tab">
                      <?php $attributes = array('class' => 'form-horizontal', 'role' => 'form'); ?>
                      <?=form_open_multipart('Daftar/Users/Simpan_tahap4',$attributes)?>
                      <input type="hidden" name="id_daftar_users" value="<?php if(!empty($viewDOrangtuaIbu->id_daftar_users)){ echo $viewDOrangtuaIbu->id_daftar_users;};?>">
                        <div id="ibu" class="display:block">
                        <div class="form-group">
                            <input type="hidden" class="form-control" name="orangtua" value="Ibu">
                        </div>
                        <div class="form-group">
                          <label class="col-sm-3 control-label">NIK Ibu</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="nik_orangtua" value="<?php if(!empty($viewDOrangtuaIbu->nik_orangtua)){ echo $viewDOrangtuaIbu->nik_orangtua;};?>">
                          </div>
                          <div class="col-sm-offset-3 col-sm-9">
                            <span class="label label-danger">Wajib diisi</span>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-3 control-label">Nama Ibu</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="nama_orangtua" value="<?php if(!empty($viewDOrangtuaIbu->nama_orangtua)){ echo $viewDOrangtuaIbu->nama_orangtua;};?>">
                          </div>
                          <div class="col-sm-offset-3 col-sm-9">
                            <span class="label label-danger">Wajib diisi</span>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-3 control-label">Tanggal Lahir Ibu</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control datepicker" name="tgl_lhr_orangtua" value="<?php if(!empty($viewDOrangtuaIbu->tgl_lhr_orangtua)){ echo $viewDOrangtuaIbu->tgl_lhr_orangtua;};?>">
                          </div>
                          <div class="col-sm-offset-3 col-sm-9">
                            <span class="label label-danger">Wajib diisi</span>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-3 control-label">Pendidikan Ibu</label>
                          <div class="col-sm-9">
                            <select class="form-control" name="id_pendidikan">
                            <?php foreach ($tbPendidikan as $key => $value): ?>
                              <?php if ($value->id_pendidikan == $viewDOrangtuaIbu->id_pendidikan) { ?>
                                <option value="<?=$value->id_pendidikan;?>" selected=""><?=$value->pendidikan;?></option>
                              <?php }else{ ?>
                                <option value="<?=$value->id_pendidikan;?>"><?=$value->pendidikan;?></option>
                              <?php } ?>
                            <?php endforeach ?>
                            </select>
                          </div>
                          <div class="col-sm-offset-3 col-sm-9">
                            <span class="label label-danger">Wajib diisi</span>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-3 control-label">Pekerjaan Ibu</label>
                          <div class="col-sm-9">
                            <select class="form-control" name="id_pekerjaan">
                            <?php foreach ($tbPekerjaan as $key => $value): ?>
                              <?php if ($value->id_pekerjaan == $tbBM->id_pekerjaan) { ?>
                                <option value="<?=$value->id_pekerjaan;?>" selected=""><?=$value->pekerjaan;?></option>
                              <?php }else{ ?>
                                <option value="<?=$value->id_pekerjaan;?>"><?=$value->pekerjaan;?></option>
                              <?php } ?>
                            <?php endforeach ?>
                            </select>
                          </div>
                          <div class="col-sm-offset-3 col-sm-9">
                            <span class="label label-danger">Wajib diisi</span>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-3 control-label">Penghasilan Ibu</label>
                          <div class="col-sm-9">
                            <select class="form-control" name="id_penghasilan">
                            <?php foreach ($tbPenghasilan as $key => $value): ?>
                              <?php if ($value->id_penghasilan == $viewDOrangtuaIbu->id_penghasilan) { ?>
                                <option value="<?=$value->id_penghasilan;?>" selected=""><?=$value->penghasilan;?></option>
                              <?php }else{ ?>
                                <option value="<?=$value->id_penghasilan;?>"><?=$value->penghasilan;?></option>
                              <?php } ?>
                            <?php endforeach ?>
                            </select>
                          </div>
                          <div class="col-sm-offset-3 col-sm-9">
                            <span class="label label-danger">Wajib diisi</span>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-3 control-label">Nominal Penghasilan Ibu</label>
                          <div class="col-sm-9">
                            <input type="number" class="form-control" name="nominal_penghasilan" value="<?php if(!empty($viewDOrangtuaIbu->nominal_penghasilan)){ echo $viewDOrangtuaIbu->nominal_penghasilan;};?>" onkeypress="return hanyaAngka(event)" min=0>
                          </div>
                          <div class="col-sm-offset-3 col-sm-9">
                            <span class="label label-danger">Wajib diisi</span>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-3 control-label">Terbilang Penghasilan Ibu</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="terbilang_penghasilan" value="<?php if(!empty($viewDOrangtuaIbu->terbilang_penghasilan)){ echo $viewDOrangtuaIbu->terbilang_penghasilan;};?>">
                          </div>
                          <div class="col-sm-offset-3 col-sm-9">
                            <span class="label label-danger">Wajib diisi</span>
                          </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                          <div class="col-sm-offset-3 col-sm-9">
                            <button type="submit" class="btn btn-primary">SIMPAN</button>
                          </div>
                        </div>
                      <?=form_close();?>
                      </div>
                      <div class="tab-pane fade" id="wali" role="tabpanel" aria-labelledby="wali-tab">
                      <?php $attributes = array('class' => 'form-horizontal', 'role' => 'form'); ?>
                      <?=form_open_multipart('Daftar/Users/Simpan_tahap4',$attributes)?>
                        <div id="wali" class="display:block">
                        <input type="hidden" name="id_daftar_users" value="<?php if(!empty($viewDOrangtuaWali->id_daftar_users)){ echo $viewDOrangtuaWali->id_daftar_users;};?>">
                        <div class="form-group">
                            <input type="hidden" class="form-control" name="orangtua" value="Wali">
                        </div>
                        <div class="form-group">
                          <div class="col-sm-offset-3 col-sm-9">
                            <span class="label label-warning">Wali diisi jika ada yang membiayai selain orang tua. Jika tidak ada dapat diabaikan.</span>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-3 control-label">NIK Wali</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="nik_orangtua" value="<?php if(!empty($viewDOrangtuaWali->nik_orangtua)){ echo $viewDOrangtuaWali->nik_orangtua;};?>">
                          </div>
                          <div class="col-sm-offset-3 col-sm-9">
                            <span class="label label-warning">Optional</span>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-3 control-label">Nama Wali</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="nama_orangtua" value="<?php if(!empty($viewDOrangtuaWali->nama_orangtua)){ echo $viewDOrangtuaWali->nama_orangtua;};?>">
                          </div>
                          <div class="col-sm-offset-3 col-sm-9">
                            <span class="label label-warning">Optional</span>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-3 control-label">Tanggal Lahir Wali</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control datepicker" name="tgl_lhr_orangtua" value="<?php if(!empty($viewDOrangtuaWali->tgl_lhr_orangtua)){ echo $viewDOrangtuaWali->tgl_lhr_orangtua;};?>">
                          </div>
                          <div class="col-sm-offset-3 col-sm-9">
                            <span class="label label-warning">Optional</span>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-3 control-label">Pendidikan Wali</label>
                          <div class="col-sm-9">
                            <select class="form-control" name="id_pendidikan">
                            <?php foreach ($tbPendidikan as $key => $value): ?>
                              <?php if ($value->id_pendidikan == $viewDOrangtuaWali->id_pendidikan) { ?>
                                <option value="<?=$value->id_pendidikan;?>" selected=""><?=$value->pendidikan;?></option>
                              <?php }else{ ?>
                                <option value="<?=$value->id_pendidikan;?>"><?=$value->pendidikan;?></option>
                              <?php } ?>
                            <?php endforeach ?>
                            </select>
                          </div>
                          <div class="col-sm-offset-3 col-sm-9">
                            <span class="label label-warning">Optional</span>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-3 control-label">Pekerjaan Wali</label>
                          <div class="col-sm-9">
                            <select class="form-control" name="id_pekerjaan">
                            <?php foreach ($tbPekerjaan as $key => $value): ?>
                              <?php if ($value->id_pekerjaan == $viewDOrangtuaWali->id_pekerjaan) { ?>
                                <option value="<?=$value->id_pekerjaan;?>" selected=""><?=$value->pekerjaan;?></option>
                              <?php }else{ ?>
                                <option value="<?=$value->id_pekerjaan;?>"><?=$value->pekerjaan;?></option>
                              <?php } ?>
                            <?php endforeach ?>
                            </select>
                          </div>
                          <div class="col-sm-offset-3 col-sm-9">
                            <span class="label label-warning">Optional</span>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-3 control-label">Penghasilan Wali</label>
                          <div class="col-sm-9">
                            <select class="form-control" name="id_penghasilan">
                            <?php foreach ($tbPenghasilan as $key => $value): ?>
                              <?php if ($value->id_penghasilan == $viewDOrangtuaWali->id_penghasilan) { ?>
                                <option value="<?=$value->id_penghasilan;?>" selected=""><?=$value->penghasilan;?></option>
                              <?php }else{ ?>
                                <option value="<?=$value->id_penghasilan;?>"><?=$value->penghasilan;?></option>
                              <?php } ?>
                            <?php endforeach ?>
                            </select>
                          </div>
                          <div class="col-sm-offset-3 col-sm-9">
                            <span class="label label-warning">Optional</span>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-3 control-label">Nominal Penghasilan Wali</label>
                          <div class="col-sm-9">
                            <input type="number" class="form-control" name="nominal_penghasilan" value="<?php if(!empty($viewDOrangtuaWali->nominal_penghasilan)){ echo $viewDOrangtuaWali->nominal_penghasilan;};?>" onkeypress="return hanyaAngka(event)" min=0>
                          </div>
                          <div class="col-sm-offset-3 col-sm-9">
                            <span class="label label-warning">Optional</span>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-3 control-label">Terbilang Penghasilan Wali</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="terbilang_penghasilan" value="<?php if(!empty($viewDOrangtuaWali->terbilang_penghasilan)){ echo $viewDOrangtuaWali->terbilang_penghasilan;};?>">
                          </div>
                          <div class="col-sm-offset-3 col-sm-9">
                            <span class="label label-warning">Optional</span>
                          </div>
                        </div>
                        </div>
                        <div class="form-group">
                          <div class="col-sm-offset-3 col-sm-9">
                            <button type="submit" class="btn btn-primary">SIMPAN</button>
                          </div>
                        </div>
                      <?=form_close();?>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </section>
</div>