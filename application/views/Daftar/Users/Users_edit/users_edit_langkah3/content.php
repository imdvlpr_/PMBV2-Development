<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Users
            <small><i>Detail</i></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-gears"></i> Users</a></li>
            <li><a href="#"></i> Detail</a></li>
            <li class="active"><i>Edit</i></li>
        </ol>
    </section>
    <section class="content">
        <?php if (!empty($this->session->flashdata('type_message'))) { ?>
            <div class="alert alert-<?=$this->session->flashdata('type_message')?> alert-dismissible text-center" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?=$this->session->flashdata('message');?>
            </div>
        <?php } ?>
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Langkah-langkah</h3>
            </div>
            <center>
            <div class="box-body">
                <div class="list-group">
                <a href="<?=base_url('Daftar/Users/Edit_langkah1/'.$viewDaftarUsers->id_daftar_users)?>" class="list-group-item" style="float: left">Langkah 1</a>
                <a href="<?=base_url('Daftar/Users/Edit_langkah2/'.$viewDaftarUsers->id_daftar_users)?>" class="list-group-item" style="float: left">Langkah 2</a>
                <a href="#" class="list-group-item active" style="float: left">Langkah 3</a>
                <a href="<?=base_url('Daftar/Users/Edit_langkah4/'.$viewDaftarUsers->id_daftar_users)?>" class="list-group-item" style="float: left">Langkah 4</a>
                <a href="<?=base_url('Daftar/Users/Edit_langkah5/'.$viewDaftarUsers->id_daftar_users)?>" class="list-group-item" style="float: left">Langkah 5</a>
                <a href="<?=base_url('Daftar/Users/Edit_langkah6/'.$viewDaftarUsers->id_daftar_users)?>" class="list-group-item" style="float: left">Langkah 6</a>
                </div>
            </div>
            </center>
        </div>
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Tabel Users Detail</h3>
                <div class="pull-right">
                    <button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
                </div>
            </div>
            <div class="box-body">
                <?php $attributes = array('class' => 'form-horizontal', 'role' => 'form'); ?>
                <?=form_open_multipart('Daftar/Users/Simpan_tahap3',$attributes)?>
                <input type="hidden" class="form-control" name="id_daftar_users" value="<?=$viewDaftarUsers->id_daftar_users;?>">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Tempat Tinggal</label>
                    <div class="col-sm-9">
                    <select class="form-control" name="id_jns_tinggal" required="">
                    <?php foreach ($tbJenisTinggal as $key => $value): ?>
                        <?php if ($value->id_jns_tinggal == $tbBM->id_jns_tinggal) { ?>
                        <option value="<?=$value->id_jns_tinggal;?>" selected=""><?=$value->jenis_tinggal;?></option>
                        <?php }else{ ?>
                        <option value="<?=$value->id_jns_tinggal;?>"><?=$value->jenis_tinggal;?></option>
                        <?php } ?>
                    <?php endforeach ?>
                    </select>
                    </div>
                    <div class="col-sm-offset-3 col-sm-9">
                    <span class="label label-danger">Wajib diisi</span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Kepemilikan Kendaraan</label>
                    <div class="col-sm-9">
                    <select class="form-control" name="id_alat_transportasi" required="">
                    <?php foreach ($tbAlatTransportasi as $key => $value): ?>
                        <?php if ($value->id_alat_transportasi == $tbBM->id_alat_transportasi) { ?>
                        <option value="<?=$value->id_alat_transportasi;?>" selected=""><?=$value->alat_transportasi;?></option>
                        <?php }else{ ?>
                        <option value="<?=$value->id_alat_transportasi;?>"><?=$value->alat_transportasi;?></option>
                        <?php } ?>
                    <?php endforeach ?>
                    </select>
                    </div>
                    <div class="col-sm-offset-3 col-sm-9">
                    <span class="label label-danger">Wajib diisi</span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Telepon Rumah</label>
                    <div class="col-sm-9">
                    <input type="number" class="form-control" name="tlp_rmh" value="<?=$tbBM->tlp_rmh;?>">
                    </div>
                    <div class="col-sm-offset-3 col-sm-9">
                    <span class="label label-warning">Optional</span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Nomor HP</label>
                    <div class="col-sm-9">
                    <input type="number" class="form-control" name="nmr_hp" value="<?=$viewDaftarUsers->nmr_hp;?>">
                    </div>
                    <div class="col-sm-offset-3 col-sm-9">
                    <span class="label label-danger">Wajib diisi</span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Email</label>
                    <div class="col-sm-9">
                    <input type="email" class="form-control" name="email" value="<?=$viewDaftarUsers->username;?>">
                    </div>
                    <div class="col-sm-offset-3 col-sm-9">
                    <span class="label label-danger">Wajib diisi</span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Terima KPS</label>
                    <div class="col-sm-2">
                    <select class="form-control" name="terima_kps" required="">
                        <?php if ($tbBM->terima_kps == "IYA") { ?>
                        <option value="IYA" selected="">IYA</option>
                        <option value="TIDAK">TIDAK</option>
                        <?php }else{ ?>
                        <option value="IYA">IYA</option>
                        <option value="TIDAK" selected="">TIDAK</option>
                        <?php } ?>
                    </select>
                    </div>
                    <label class="col-sm-3 control-label">Nomor KPS</label>
                    <div class="col-sm-4">
                    <input type="number" class="form-control" name="no_kps" value="<?=$tbBM->no_kps;?>" disabled required>
                    </div>
                    <div class="col-sm-offset-3 col-sm-9">
                    <span class="label label-danger">Wajib diisi</span>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-9">
                    <button type="submit" class="btn btn-primary">SIMPAN</button>
                    </div>
                </div>
                <div class="col-sm-offset-3 col-sm-9">
                    <span class="label label-warning">*Keterangan : KPS = Kartu Perlindungan Sosial</span>
                </div>
                <?=form_close();?>
            </div>
        </div>
    </section>
</div>