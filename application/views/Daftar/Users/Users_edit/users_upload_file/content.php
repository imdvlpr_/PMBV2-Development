<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Users
            <small><i>Detail</i></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-gears"></i> Users</a></li>
            <li><a href="#"></i> Detail</a></li>
            <li class="active"><i>Edit</i></li>
        </ol>
    </section>
    <section class="content">
        <?php if (!empty($this->session->flashdata('type_message'))) { ?>
            <div class="alert alert-<?=$this->session->flashdata('type_message')?> alert-dismissible text-center" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?=$this->session->flashdata('message');?>
            </div>
        <?php } ?>
        <div class="row">
        <div class="col-md-12">
            <div class="box">
            <!-- Default box -->
            <div class="box-header with-border">
                <h3 class="box-title">Upload</h3>
            </div>
            <div class="box-body">
            <?php $attributes = array('class' => 'form-horizontal', 'role' => 'form'); ?>
            <?=form_open_multipart('Daftar/UploadFile/Simpan',$attributes)?>
            <input type="hidden" name="nik" value="<?=$tbUsers->nik_passport?>">
                <div class="form-group">
                <label class="col-md-4 control-label" for="tipe_gambar">Tipe Gambar</label>
                <div class="col-md-4">
                    <select id="tipe_gambar" name="tipe_gambar" class="form-control">
                    <?php foreach ($tbTipeGambar as $value) { ?>
                        <option value="<?=$value->id_tipe_gambar;?>"><?=$value->tipe_gambar;?></option>
                    <?php } ?>
                    </select>
                </div>
                </div>

                <!-- File Button --> 
                <div class="form-group">
                <label class="col-md-4 control-label" for="userfile">Gambar</label>
                <div class="col-md-4">
                    <input id="userfile" name="userfile" class="input-file" type="file">
                </div>
                </div>

                <!-- Button -->
                <div class="form-group">
                <label class="col-md-4 control-label" for="button"></label>
                <div class="col-md-4">
                    <button id="button" name="button" class="btn btn-primary">UPLOAD</button>
                </div>
                </div>
                
                <?=form_close(); ?>
                <div class="alert alert-warning" role="alert">Bagi yang tidak memiliki PBB/Mengontrak/Menumpang, dapat menggunakan surat keterangan yang dikeluarkan dari kantor kepala desa/kelurahan setempat.</div>
                <div class="alert alert-warning" role="alert">Surat keterangan penghasilan orang tua dapat berupa :<br>- Legger Gaji/Rincian Gaji(Bagi instansi pemerintahan)<br>- Keterangan penghasilan dari perusahaan(Swasta/BUMN)<br>- Keterangan Dari desa/kelurahan(Wiraswasta/Tani/Buruh)</div>
            </div>
            <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        </div>

        <div class="row">
        <section class="content-header">
            <h3>
            Uploaded
            </h3>
        </section>
        <?php foreach($tbFileUpload as $value) : ?>
            <div class="col-md-4">
            <div class="box">
                <!-- Default box -->
                <div class="box-header with-border">
                <h3 class="box-title"><?php echo $value->tipe_gambar?></span></h3>
                </div>
                <div class="box-body">
                <center><a href="#" data-target="#v<?php echo $value->id_tipe_gambar?>" data-toggle="modal" data-placement="top" id="rumah"  title="Klik Untuk Memperbesar" class="btn btn-primary">Lihat Foto</a></center>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
            </div>

            <!-- Modal View Image -->
            <div class="modal fade" id="v<?php echo $value->id_tipe_gambar?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><?php echo $value->tipe_gambar?></h4>
                </div>
                <div class="modal-body">
                    <?php if (!empty($value->gambar)) { ?>
                    <img src="<?php echo 'https://pmb-snmptn.uinsgd.ac.id/upload/foto/'.$value->id_tipe_gambar.'/'.$value->gambar?>" class="img-responsive center-block">
                    <?php }else{?>
                    <img src="<?=base_url('design-backend/images/no_image.gif');?>" class="img-responsive center-block">
                    <?php } ?>
                    <span class="label label-danger">Jika ingin mengubah silahkan lakukan upload ulang.</span>
                </div>
                <?php if ($this->session->userdata('verifikasi')=='BELUM VERIFIKASI') { ?>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                </div>
                <?php } ?>
                </div>
            </div>
            </div>
        <?php endforeach; ?>
        <div class="col-md-4"></div>
        </div>
    </section>
</div>