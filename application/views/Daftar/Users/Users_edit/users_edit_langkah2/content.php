<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Users
            <small><i>Detail</i></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-gears"></i> Users</a></li>
            <li><a href="#"></i> Detail</a></li>
            <li class="active"><i>Edit</i></li>
        </ol>
    </section>
    <section class="content">
        <?php if (!empty($this->session->flashdata('type_message'))) { ?>
            <div class="alert alert-<?=$this->session->flashdata('type_message')?> alert-dismissible text-center" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?=$this->session->flashdata('message');?>
            </div>
        <?php } ?>
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Langkah-langkah</h3>
            </div>
            <center>
            <div class="box-body">
                <div class="list-group">
                <a href="<?=base_url('Daftar/Users/Edit_langkah1/'.$viewDaftarUsers->id_daftar_users)?>" class="list-group-item" style="float: left">Langkah 1</a>
                <a href="#" class="list-group-item active" style="float: left">Langkah 2</a>
                <a href="<?=base_url('Daftar/Users/Edit_langkah3/'.$viewDaftarUsers->id_daftar_users)?>" class="list-group-item" style="float: left">Langkah 3</a>
                <a href="<?=base_url('Daftar/Users/Edit_langkah4/'.$viewDaftarUsers->id_daftar_users)?>" class="list-group-item" style="float: left">Langkah 4</a>
                <a href="<?=base_url('Daftar/Users/Edit_langkah5/'.$viewDaftarUsers->id_daftar_users)?>" class="list-group-item" style="float: left">Langkah 5</a>
                <a href="<?=base_url('Daftar/Users/Edit_langkah6/'.$viewDaftarUsers->id_daftar_users)?>" class="list-group-item" style="float: left">Langkah 6</a>
                </div>
            </div>
            </center>
        </div>
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Tabel Users Detail</h3>
                <div class="pull-right">
                    <button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
                </div>
            </div>
            <div class="box-body">
                <?php $attributes = array('class' => 'form-horizontal', 'role' => 'form'); ?>
                <?=form_open_multipart('Daftar/Users/Simpan_tahap2',$attributes)?>
                    <input type="hidden" class="form-control" name="id_daftar_users" value="<?=$viewDaftarUsers->id_daftar_users;?>">
                    <div class="form-group">
                    <label class="col-sm-3 control-label">Jenis Pendaftaran</label>
                    <div class="col-sm-9">
                        <select class="form-control" name="id_jns_pndftrn" required="">
                        <?php foreach ($tbJenisPendaftaran as $key => $value): ?>
                        <?php if ($value->id_jns_pndftrn == $tbBM->id_jns_pndftrn) { ?>
                            <option value="<?=$value->id_jns_pndftrn;?>" selected=""><?=$value->jenis_pendaftaran;?></option>
                        <?php }else{ ?>
                            <option value="<?=$value->id_jns_pndftrn;?>"><?=$value->jenis_pendaftaran;?></option>
                        <?php } ?>
                        <?php endforeach ?>
                    </select>
                    </div>
                    <div class="col-sm-offset-3 col-sm-9">
                        <span class="label label-danger">Wajib diisi</span>
                    </div>
                    </div>
                    <div class="form-group">
                    <label class="col-sm-3 control-label">Jenis Pembiayaan</label>
                    <div class="col-sm-9">
                        <select class="form-control" name="id_jenis_pembiayaan" required="">
                        <?php foreach ($tbJenisPembiayaan as $key => $value): ?>
                        <?php if ($value->id_jenis_pembiayaan == $tbBM->id_jenis_pembiayaan) { ?>
                            <option value="<?=$value->id_jenis_pembiayaan;?>" selected=""><?=$value->jenis_pembiayaan;?></option>
                        <?php }else{ ?>
                            <option value="<?=$value->id_jenis_pembiayaan;?>"><?=$value->jenis_pembiayaan;?></option>
                        <?php } ?>
                        <?php endforeach ?>
                    </select>
                    </div>
                    <div class="col-sm-offset-3 col-sm-9">
                        <span class="label label-danger">Wajib diisi</span>
                    </div>
                    </div>
                    <!--<div class="form-group">
                    <label class="col-sm-3 control-label">Tanggal Masuk Kuliah</label>
                    <div class="col-sm-9">
                        <input type="hidden" class="form-control datepicker" name="tgl_msk_kuliah" value="<?=$tbBM->tgl_msk_kuliah;?>">
                    </div>
                    <div class="col-sm-offset-3 col-sm-9">
                        <span class="label label-danger">Wajib diisi</span>
                    </div>
                    </div>-->
                    <div class="form-group">
                    <label class="col-sm-3 control-label">Mulai Semester</label>
                    <div class="col-sm-9">
                        <select class="form-control" name="mulai_semester" required="">
                        <option value="SEMESTER 1" <?php if($tbBM->mulai_semester == "SEMESTER 1"){ echo 'selected'; }?>>SEMESTER 1</option>
                        <option value="SEMESTER 2" <?php if($tbBM->mulai_semester == "SEMESTER 2"){ echo 'selected'; }?>>SEMESTER 2</option>
                        <option value="SEMESTER 3" <?php if($tbBM->mulai_semester == "SEMESTER 3"){ echo 'selected'; }?>>SEMESTER 3</option>
                        <option value="SEMESTER 4" <?php if($tbBM->mulai_semester == "SEMESTER 4"){ echo 'selected'; }?>>SEMESTER 4</option>
                        <option value="SEMESTER 5" <?php if($tbBM->mulai_semester == "SEMESTER 5"){ echo 'selected'; }?>>SEMESTER 5</option>
                        <option value="SEMESTER 6" <?php if($tbBM->mulai_semester == "SEMESTER 6"){ echo 'selected'; }?>>SEMESTER 6</option>
                        <option value="SEMESTER 7" <?php if($tbBM->mulai_semester == "SEMESTER 7"){ echo 'selected'; }?>>SEMESTER 7</option>
                        <option value="SEMESTER 8" <?php if($tbBM->mulai_semester == "SEMESTER 8"){ echo 'selected'; }?>>SEMESTER 8</option>
                        <option value="SEMESTER 9" <?php if($tbBM->mulai_semester == "SEMESTER 9"){ echo 'selected'; }?>>SEMESTER 9</option>
                        <option value="SEMESTER 10" <?php if($tbBM->mulai_semester == "SEMESTER 10"){ echo 'selected'; }?>>SEMESTER 10</option>
                        <option value="SEMESTER 11" <?php if($tbBM->mulai_semester == "SEMESTER 11"){ echo 'selected'; }?>>SEMESTER 11</option>
                        <option value="SEMESTER 12" <?php if($tbBM->mulai_semester == "SEMESTER 12"){ echo 'selected'; }?>>SEMESTER 12</option>
                        </select>
                    </div>
                    <div class="col-sm-offset-3 col-sm-9">
                        <span class="label label-danger">Wajib diisi</span>
                    </div>
                    </div>
                    <div class="form-group">
                    <label class="col-sm-3 control-label">Alamat</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" style="resize: none" id="inputAlamat" name="alamat" required=""><?=$tbBM->jalan;?></textarea>
                    </div>
                    <div class="col-sm-offset-3 col-sm-9">
                        <span class="label label-danger">Wajib diisi</span>
                    </div>
                    </div>
                    <div class="form-group">
                    <label class="col-sm-3 control-label">RT</label>
                    <div class="col-sm-3">
                        <input class="form-control" name="rt" value="<?=$tbBM->rt?>" required="">
                    </div>
                    <label class="col-sm-3 control-label">RW</label>
                    <div class="col-sm-3">
                        <input class="form-control" name="rw" value="<?=$tbBM->rt?>" required="">
                    </div>
                    <div class="col-sm-offset-3 col-sm-9">
                        <span class="label label-danger">Wajib diisi</span>
                    </div>
                    </div>
                    <div class="form-group">
                    <label class="col-sm-3 control-label">Nama Dusun</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="nama_dusun" value="<?=$tbBM->nama_dusun;?>">
                    </div>
                    <div class="col-sm-offset-3 col-sm-9">
                        <span class="label label-warning">Optional</span>
                    </div>
                    </div>
                    <div class="form-group">
                    <label class="col-sm-3 control-label">Provinsi</label>
                    <div class="col-sm-9">
                        <select id="prov" name="id_provinsi" class="form-control" style="width:100%">
                        <option value="">Pilih</option>
                        <?php foreach ($tbProv as $value): ?>
                            <?php if (strtoupper($value->id_provinsi) == $tbBM->id_provinsi) { ?>
                            <option value="<?php echo strtoupper($value->id_provinsi); ?>" selected=""><?php echo strtoupper($value->provinsi); ?></option>
                            <?php }else{ ?>
                            <option value="<?php echo strtoupper($value->id_provinsi); ?>"><?php echo strtoupper($value->provinsi); ?></option>
                            <?php } ?>
                        <?php endforeach ?>
                        </select>
                    </div>
                    <div class="col-sm-offset-3 col-sm-9">
                        <span class="label label-danger">Wajib diisi</span>
                    </div>
                    </div>
                    <div class="form-group">
                    <label class="col-sm-3 control-label">Kabupaten</label>
                    <div class="col-sm-9">
                        <select id="kab" name="id_kabupaten" class="form-control" style="width:100%">
                        <option value="">Pilih</option>
                        </select>
                        <div id="loading1" style="margin-top: 15px;">
                        <img src="<?php echo base_url()?>/design-backend/images/loading.gif" width="18"> <small>Loading...</small>
                        </div>
                    </div>
                    <div class="col-sm-offset-3 col-sm-9">
                        <span class="label label-danger">Wajib diisi</span>
                    </div>
                    </div>
                    <div class="form-group">
                    <label class="col-sm-3 control-label">Kecamatan</label>
                    <div class="col-sm-9">
                        <select id="kec" name="id_kecamatan" class="form-control" style="width:100%">
                        <option value="">Pilih</option>
                        </select>
                        <div id="loading2" style="margin-top: 15px;">
                        <img src="<?php echo base_url()?>/design-backend/images/loading.gif" width="18"> <small>Loading...</small>
                        </div>
                    </div>
                    <div class="col-sm-offset-3 col-sm-9">
                        <span class="label label-danger">Wajib diisi</span>
                    </div>
                    </div>
                    <div class="form-group">
                    <label class="col-sm-3 control-label">Kelurahan</label>
                    <div class="col-sm-9">
                        <input type="hidden" name="old_id_kelurahan" value="<?=$tbBM->id_kelurahan?>">
                        <select id="kel" name="id_kelurahan" class="form-control" style="width:100%">
                        <option value="">Pilih</option>
                        </select>
                        <div id="loading3" style="margin-top: 15px;">
                        <img src="<?php echo base_url()?>/design-backend/images/loading.gif" width="18"> <small>Loading...</small>
                        </div>
                    </div>
                    <div class="col-sm-offset-3 col-sm-9">
                        <span class="label label-danger">Wajib diisi</span>
                    </div>
                    </div>
                    <div class="form-group">
                    <label class="col-sm-3 control-label">Kode POS</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="kode_pos" value="<?=$tbBM->kode_pos;?>">
                    </div>
                    <div class="col-sm-offset-3 col-sm-9">
                        <span class="label label-danger">Wajib diisi</span>
                    </div>
                    </div>
                    <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-9">
                        <button type="submit" class="btn btn-primary">SIMPAN</button>
                    </div>
                        <div class="col-sm-offset-3 col-sm-9">
                            <span class="label label-danger">Isi kembali keseluruhan data jika ingin mengubah salah satu data.</span>
                        </div>
                    </div>
                <?=form_close();?>
            </div>
        </div>
    </section>
</div>