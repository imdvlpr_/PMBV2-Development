<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Users
            <small><i>Detail</i></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-gears"></i> Users</a></li>
            <li><a href="#"></i> Detail</a></li>
            <li class="active"><i>Edit</i></li>
        </ol>
    </section>
    <section class="content">
        <?php if (!empty($this->session->flashdata('type_message'))) { ?>
            <div class="alert alert-<?=$this->session->flashdata('type_message')?> alert-dismissible text-center" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?=$this->session->flashdata('message');?>
            </div>
        <?php } ?>
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Langkah-langkah</h3>
            </div>
            <center>
            <div class="box-body">
                <div class="list-group">
                <a href="<?=base_url('Daftar/Users/Edit_langkah1/'.$viewDaftarUsers->id_daftar_users)?>" class="list-group-item" style="float: left">Langkah 1</a>
                <a href="<?=base_url('Daftar/Users/Edit_langkah2/'.$viewDaftarUsers->id_daftar_users)?>" class="list-group-item" style="float: left">Langkah 2</a>
                <a href="<?=base_url('Daftar/Users/Edit_langkah3/'.$viewDaftarUsers->id_daftar_users)?>" class="list-group-item" style="float: left">Langkah 3</a>
                <a href="<?=base_url('Daftar/Users/Edit_langkah4/'.$viewDaftarUsers->id_daftar_users)?>" class="list-group-item" style="float: left">Langkah 4</a>
                <a href="#" class="list-group-item active" style="float: left">Langkah 5</a>
                <a href="<?=base_url('Daftar/Users/Edit_langkah6/'.$viewDaftarUsers->id_daftar_users)?>" class="list-group-item" style="float: left">Langkah 6</a>
                </div>
            </div>
            </center>
        </div>
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Tabel Users Detail</h3>
                <div class="pull-right">
                    <button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
                </div>
            </div>
            <div class="box-body">
                <?php $attributes = array('class' => 'form-horizontal', 'role' => 'form'); ?>
                <?=form_open_multipart('Daftar/Users/Simpan_tahap5',$attributes)?>
                <input type="hidden" class="form-control" name="id_daftar_users" value="<?=$viewDaftarUsers->id_daftar_users;?>">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Asal Sekolah</label>
                    <div class="col-sm-9">
                    <select class="form-control" name="id_asal_sekolah" required="">
                    <?php foreach ($tbAsalSekolah as $key => $value): ?>
                        <?php if ($value->id_asal_sekolah == $tbExtra->id_asal_sekolah) { ?>
                        <option value="<?=$value->id_asal_sekolah;?>" selected=""><?=$value->asal_sekolah;?></option>
                        <?php }else{ ?>
                        <option value="<?=$value->id_asal_sekolah;?>"><?=$value->asal_sekolah;?></option>
                        <?php } ?>
                    <?php endforeach ?>
                    </select>
                    </div>
                    <div class="col-sm-offset-3 col-sm-9">
                    <span class="label label-danger">Wajib diisi</span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Tanggungan</label>
                    <div class="col-sm-9">
                    <select class="form-control" name="id_tanggungan" required="">
                    <?php foreach ($tbTanggungan as $key => $value): ?>
                        <?php if ($value->id_tanggungan == $tbExtra->id_tanggungan) { ?>
                        <option value="<?=$value->id_tanggungan;?>" selected=""><?=$value->tanggungan;?></option>
                        <?php }else{ ?>
                        <option value="<?=$value->id_tanggungan;?>"><?=$value->tanggungan;?></option>
                        <?php } ?>
                    <?php endforeach ?>
                    </select>
                    </div>
                    <div class="col-sm-offset-3 col-sm-9">
                    <span class="label label-danger">Wajib diisi</span>
                    </div>
                    <div class="col-sm-offset-3 col-sm-9">
                    <span class="label label-warning">Tanggungan merupakan jumlah istri dan anak yang masih dibiayai/belum bekerja.</span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Kepemilikan Listrik</label>
                    <div class="col-sm-9">
                    <select class="form-control" name="id_rekening_listrik" required="">
                    <?php foreach ($tbRekeningListrik as $key => $value): ?>
                        <?php if ($value->id_rekening_listrik == $tbExtra->id_rekening_listrik) { ?>
                        <option value="<?=$value->id_rekening_listrik;?>" selected=""><?=$value->rekening_listrik;?></option>
                        <?php }else{ ?>
                        <option value="<?=$value->id_rekening_listrik;?>"><?=$value->rekening_listrik;?></option>
                        <?php } ?>
                    <?php endforeach ?>
                    </select>
                    </div>
                    <div class="col-sm-offset-3 col-sm-9">
                    <span class="label label-danger">Wajib diisi</span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Kepemilikan Rumah (PBB)</label>
                    <div class="col-sm-9">
                    <select class="form-control" name="id_rekening_pbb" required="">
                    <?php foreach ($tbRekeningPBB as $key => $value): ?>
                        <?php if ($value->id_rekening_pbb == $tbExtra->id_rekening_pbb) { ?>
                        <option value="<?=$value->id_rekening_pbb;?>" selected=""><?=$value->rekening_pbb;?></option>
                        <?php }else{ ?>
                        <option value="<?=$value->id_rekening_pbb;?>"><?=$value->rekening_pbb;?></option>
                        <?php } ?>
                    <?php endforeach ?>
                    </select>
                    </div>
                    <div class="col-sm-offset-3 col-sm-9">
                    <span class="label label-danger">Wajib diisi</span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Pembayaran Listrik</label>
                    <div class="col-sm-9">
                    <select class="form-control" name="id_pembayaran_listrik" required="">
                    <?php foreach ($tbPembayaranListrik as $key => $value): ?>
                        <?php if ($value->id_pembayaran_litrik == $tbExtra->id_pembayaran_litrik) { ?>
                        <option value="<?=$value->id_pembayaran_listrik;?>" selected=""><?=$value->pembayaran_listrik;?></option>
                        <?php }else{ ?>
                        <option value="<?=$value->id_pembayaran_listrik;?>"><?=$value->pembayaran_listrik;?></option>
                        <?php } ?>
                    <?php endforeach ?>
                    </select>
                    </div>
                    <div class="col-sm-offset-3 col-sm-9">
                    <span class="label label-danger">Wajib diisi</span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Pembayaran PBB</label>
                    <div class="col-sm-9">
                    <select class="form-control" name="id_pembayaran_pbb" required="">
                    <?php foreach ($tbPembayaranPBB as $key => $value): ?>
                        <?php if ($value->id_pembayaran_pbb == $tbExtra->id_pembayaran_pbb) { ?>
                        <option value="<?=$value->id_pembayaran_pbb;?>" selected=""><?=$value->pembayaran_pbb;?></option>
                        <?php }else{ ?>
                        <option value="<?=$value->id_pembayaran_pbb;?>"><?=$value->pembayaran_pbb;?></option>
                        <?php } ?>
                    <?php endforeach ?>
                    </select>
                    </div>
                    <div class="col-sm-offset-3 col-sm-9">
                    <span class="label label-danger">Wajib diisi</span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Ukuran Kaos</label>
                    <div class="col-sm-9">
                    <select name="ukuran_baju" class="form-control">
                        <option value="S" <?php if($tbExtra->ukuran_baju=='S'){ echo 'selected'; } ?>>S</option>
                        <option value="M" <?php if($tbExtra->ukuran_baju=='M'){ echo 'selected'; } ?>>M</option>
                        <option value="L" <?php if($tbExtra->ukuran_baju=='L'){ echo 'selected'; } ?>>L</option>
                        <option value="XL" <?php if($tbExtra->ukuran_baju=='XL'){ echo 'selected'; } ?>>XL</option>
                        <option value="XXL" <?php if($tbExtra->ukuran_baju=='XXL'){ echo 'selected'; } ?>>XXL</option>
                        <option value="XXXL" <?php if($tbExtra->ukuran_baju=='XXXL'){ echo 'selected'; } ?>>XXXL</option>
                    </select>
                    </div>
                    <div class="col-sm-offset-3 col-sm-9">
                    <span class="label label-danger">Wajib diisi</span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Ukuran Jas Almamater</label>
                    <div class="col-sm-9">
                    <select name="ukuran_jas" class="form-control">
                        <option value="S" <?php if($tbExtra->ukuran_jas=='S'){ echo 'selected'; } ?>>S</option>
                        <option value="M" <?php if($tbExtra->ukuran_jas=='M'){ echo 'selected'; } ?>>M</option>
                        <option value="L" <?php if($tbExtra->ukuran_jas=='L'){ echo 'selected'; } ?>>L</option>
                        <option value="XL" <?php if($tbExtra->ukuran_jas=='XL'){ echo 'selected'; } ?>>XL</option>
                        <option value="XXL" <?php if($tbExtra->ukuran_jas=='XXL'){ echo 'selected'; } ?>>XXL</option>
                        <option value="XXXL" <?php if($tbExtra->ukuran_jas=='XXXL'){ echo 'selected'; } ?>>XXXL</option>
                    </select>
                    </div>
                    <div class="col-sm-offset-3 col-sm-9">
                    <span class="label label-danger">Wajib diisi</span>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-9">
                    <button type="submit" class="btn btn-primary">SIMPAN</button>
                    </div>
                </div>
                <?=form_close();?>
            </div>
        </div>
    </section>
</div>