<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Users
            <small><i>Detail</i></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-gears"></i> Users</a></li>
            <li><a href="#"></i> Detail</a></li>
            <li class="active"><i>Edit</i></li>
        </ol>
    </section>
    <section class="content">
        <?php if (!empty($this->session->flashdata('type_message'))) { ?>
            <div class="alert alert-<?=$this->session->flashdata('type_message')?> alert-dismissible text-center" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?=$this->session->flashdata('message');?>
            </div>
        <?php } ?>
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Langkah-langkah</h3>
            </div>
            <center>
            <div class="box-body">
                <div class="list-group">
                <a href="<?=base_url('Daftar/Users/Edit_langkah1/'.$viewDaftarUsers->id_daftar_users)?>" class="list-group-item" style="float: left">Langkah 1</a>
                <a href="<?=base_url('Daftar/Users/Edit_langkah2/'.$viewDaftarUsers->id_daftar_users)?>" class="list-group-item" style="float: left">Langkah 2</a>
                <a href="<?=base_url('Daftar/Users/Edit_langkah3/'.$viewDaftarUsers->id_daftar_users)?>" class="list-group-item" style="float: left">Langkah 3</a>
                <a href="<?=base_url('Daftar/Users/Edit_langkah4/'.$viewDaftarUsers->id_daftar_users)?>" class="list-group-item" style="float: left">Langkah 4</a>
                <a href="<?=base_url('Daftar/Users/Edit_langkah5/'.$viewDaftarUsers->id_daftar_users)?>" class="list-group-item" style="float: left">Langkah 5</a>
                <a href="#" class="list-group-item active" style="float: left">Langkah 6</a>
                </div>
            </div>
            </center>
        </div>
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Tabel Users Detail</h3>
                <div class="pull-right">
                    <button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
                </div>
            </div>
            <div class="box-body">
                <?php $attributes = array('class' => 'form-horizontal', 'role' => 'form'); ?>
                <?=form_open_multipart('Daftar/Users/Simpan_tahap6',$attributes)?>
                <input type="hidden" class="form-control" name="id_daftar_users" value="<?=$viewDaftarUsers->id_daftar_users;?>">
                    <div class="form-group">
                    <label class="col-sm-3 control-label">Foto</label>
                    <div class="col-sm-9">
                        <input type="file" name="userfile" id="inputFile" class="form-control">
                    </div>
                    <div class="col-sm-offset-3 col-sm-9">
                        <?php if (!empty($viewDaftarUsers->foto)) { ?>
                        <span class="label label-warning">Kosongkan , apabila tidak ingin mengubah</span><br />
                        <span class="label label-danger">Maksimal Ukuran 2MB (jpg|png|gif)</span>  
                        <?php }else{ ?>
                        <span class="label label-danger">Wajib diisi</span><br />
                        <span class="label label-danger">Maksimal Ukuran 2MB (jpg|png|gif)</span> 
                        <?php } ?>
                    </div>
                    </div>
                    <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-9">
                        <button type="submit" class="btn btn-primary">SIMPAN</button>
                    </div>
                    </div>
                <?=form_close();?>
            </div>
        </div>
    </section>
</div>