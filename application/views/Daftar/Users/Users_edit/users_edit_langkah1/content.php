<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Users
            <small><i>Detail</i></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-gears"></i> Users</a></li>
            <li><a href="#"></i> Detail</a></li>
            <li class="active"><i>Edit</i></li>
        </ol>
    </section>
    <section class="content">
        <?php if (!empty($this->session->flashdata('type_message'))) { ?>
            <div class="alert alert-<?=$this->session->flashdata('type_message')?> alert-dismissible text-center" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?=$this->session->flashdata('message');?>
            </div>
        <?php } ?>
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Langkah-langkah</h3>
            </div>
            <center>
            <div class="box-body">
                <div class="list-group">
                <a href="#" class="list-group-item active" style="float: left">Langkah 1</a>
                <a href="<?=base_url('Daftar/Users/Edit_langkah2/'.$viewDaftarUsers->id_daftar_users)?>" class="list-group-item" style="float: left">Langkah 2</a>
                <a href="<?=base_url('Daftar/Users/Edit_langkah3/'.$viewDaftarUsers->id_daftar_users)?>" class="list-group-item" style="float: left">Langkah 3</a>
                <a href="<?=base_url('Daftar/Users/Edit_langkah4/'.$viewDaftarUsers->id_daftar_users)?>" class="list-group-item" style="float: left">Langkah 4</a>
                <a href="<?=base_url('Daftar/Users/Edit_langkah5/'.$viewDaftarUsers->id_daftar_users)?>" class="list-group-item" style="float: left">Langkah 5</a>
                <a href="<?=base_url('Daftar/Users/Edit_langkah6/'.$viewDaftarUsers->id_daftar_users)?>" class="list-group-item" style="float: left">Langkah 6</a>
                </div>
            </div>
            </center>
        </div>
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Tabel Users Detail</h3>
            </div>
            <div class="box-body">
                <?php $attributes = array('class' => 'form-horizontal', 'role' => 'form'); ?>
                <?=form_open_multipart('Daftar/Users/Simpan_tahap1',$attributes)?>
                    <input type="hidden" class="form-control" name="id_daftar_users" value="<?=$viewDaftarUsers->id_daftar_users;?>">
                    <div class="form-group">
                    <label class="col-sm-3 control-label">Nomor Induk Kependudukan</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="nik_passport" value="<?=$viewDaftarUsers->nik_passport;?>">
                    </div>
                    <div class="col-sm-offset-3 col-sm-9">
                        <span class="label label-danger">Wajib Diisi</span>
                    </div>
                    </div>
                    <div class="form-group">
                    <label class="col-sm-3 control-label">Nama</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="nama" value="<?=$viewDaftarUsers->nama;?>">
                    </div>
                    <div class="col-sm-offset-3 col-sm-9">
                        <span class="label label-danger">Wajib diisi</span>
                    </div>
                    </div>
                    <div class="form-group">
                    <label class="col-sm-3 control-label">Tempat Lahir</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="tmp_lhr" value="<?=$tbBM->tmp_lhr;?>">
                    </div>
                    <div class="col-sm-offset-3 col-sm-9">
                        <span class="label label-danger">Wajib diisi</span>
                    </div>
                    </div>
                    <div class="form-group">
                    <label class="col-sm-3 control-label">Tanggal Lahir</label>
                    <div class="col-sm-9">
                        <input type="text" name="tgl_lhr" class="form-control datepicker" value="<?=$viewDaftarUsers->tgl_lhr;?>">
                    </div>
                    <div class="col-sm-offset-3 col-sm-9">
                        <span class="label label-danger">Wajib diisi</span>
                    </div>
                    </div>
                    <div class="form-group">
                    <label class="col-sm-3 control-label">Jenis Kelamin</label>
                    <div class="col-sm-9">
                        <?php if ($tbBM->jenis_kelamin == "LAKI-LAKI") { ?>
                        <label class="radio-inline">
                            <input type="radio" name="jk" value="LAKI-LAKI" checked=""> Laki - Laki
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="jk" value="PEREMPUAN"> Perempuan
                        </label>
                        <?php }else{ ?>
                        <label class="radio-inline">
                            <input type="radio" name="jk" value="LAKI-LAKI"> Laki - Laki
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="jk" value="PEREMPUAN" checked=""> Perempuan
                        </label>
                        <?php } ?>
                    </div>
                    </div>
                    <div class="form-group">
                    <label class="col-sm-3 control-label">Agama</label>
                    <div class="col-sm-9">
                        <select class="form-control" name="id_agama" required="">
                        <?php foreach ($tbSAG as $key => $value): ?>
                        <?php if ($value->id_agama == $tbBM->id_agama) { ?>
                            <option value="<?=$value->id_agama;?>" selected=""><?=$value->agama;?></option>
                        <?php }else{ ?>
                            <option value="<?=$value->id_agama;?>"><?=$value->agama;?></option>
                        <?php } ?>
                        <?php endforeach ?>
                        </select>
                    </div>
                    </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">NISN</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="nisn" value="<?=$tbBM->nisn;?>">
                    </div>
                    <div class="col-sm-offset-3 col-sm-9">
                        <span class="label label-danger">Wajib diisi</span>
                    </div>
                    </div>
                    <div class="form-group">
                    <label class="col-sm-3 control-label">Jalur Masuk</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" readonly="" value="<?=$viewDaftarUsers->jalur_masuk;?>">
                    </div>
                    <div class="col-sm-offset-3 col-sm-9">
                        <span class="label label-warning">Data tidak dapat diubah</span>
                    </div>
                    </div>
                    <div class="form-group">
                    <label class="col-sm-3 control-label">Rumpun Asal Sekolah</label>
                    <div class="col-sm-9">
                      <select class="form-control" name="id_rumpun" required="">
                      <?php foreach ($tbRumpun as $value): ?>
                        <?php if ($value->id_rumpun == $tbBM->id_rumpun) { ?>
                          <option value="<?=$value->id_rumpun;?>" selected=""><?=$value->rumpun;?></option>
                        <?php }else{ ?>
                          <option value="<?=$value->id_rumpun;?>"><?=$value->rumpun;?></option>
                        <?php } ?>
                      <?php endforeach ?>
                      </select>
                    </div>
                  </div>
                    <div class="form-group">
                    <label class="col-sm-3 control-label">NPWP</label>
                    <div class="col-sm-9">
                        <input type="number" class="form-control" name="npwp" value="<?=$tbBM->npwp;?>">
                    </div>
                    <div class="col-sm-offset-3 col-sm-9">
                        <span class="label label-warning">Optional</span>
                    </div>
                    </div>
                    <div class="form-group">
                    <label class="col-sm-3 control-label">Kewarganegaraan</label>
                    <div class="col-sm-9">
                        <select class="form-control" name="warga_negara" required="">
                        <?php if ($tbBM->warga_negara == "WNI") { ?>
                            <option value="WNI" selected>WNI</option>
                            <option value="WNI">WNA</option>
                        <?php }else{ ?>
                            <option value="WNI">WNI</option>
                            <option value="WNI" selected>WNA</option>
                        <?php } ?>
                        </select>
                    </div>
                    <div class="col-sm-offset-3 col-sm-9">
                        <span class="label label-danger">Wajib diisi</span>
                    </div>
                    </div>
                    <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-9">
                        <button type="submit" class="btn btn-primary">SIMPAN</button>
                    </div>
                    </div>
                <?=form_close();?>
            </div>
        </div>
    </section>
</div>