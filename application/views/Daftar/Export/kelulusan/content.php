<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Export
            <small><i>Kelulusan</i></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-download"></i> Export</a></li>
            <li>Kelulusan</li>
        </ol>
    </section>
    <section class="content">
        <?php $this->load->view('notification'); ?>
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Export Kelulusan</h3>
                    </div>
                    <div class="box-body">
                        <?=form_open_multipart('Daftar/Export/Kelulusan/actionExport/')?>
                        <div class="form-group">
                            <label class="control-label">Jalur Masuk :</label>
                            <select name="jalur_masuk" class="form-control" required="">
                                <?php foreach ($jalur_masuk as $value): ?>
                                    <option value="<?=$value->id_jlr_msk?>"><?=$value->jalur_masuk?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Status Pendaftaran :</label>
                            <select name="status_pendaftaran" class="form-control" required="">
                                <option value="SUDAH DAFTAR">SUDAH DAFTAR</option>
                                <option value="BELUM DAFTAR">BELUM DAFTAR</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Tanggal :</label>
                            <select name="tahun" class="form-control" required="">
                                <?php foreach ($tahun as $value): ?>
                                    <option value="<?=$value->tahun?>"><?=$value->tahun?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary btn-flat pull-right"><i class="fa fa-download">&nbsp;</i>Export</button>
                        <?=form_close()?>
                    </div>
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>
    </section>
</div>