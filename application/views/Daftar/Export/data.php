<?php
header("Content-Type:application/vnd.ms-excel");
header('Content-Disposition:attachment; filename="'.$jalur_masuk.'_belum_verifikasi.xls"');
?>
<table id="users" border="1" class="table table-bordered table-striped">
<thead>
	<tr>
		<th>Nomor Peserta</th>
		<th>Nama</th>
		<th>Fakultas</th>
		<th>Jurusan</th>
		<th>Nomor HP</th>
		<th>Status</th>
	</tr>
</thead>
<tbody>
	<?php foreach($viewDaftar as $value): ?>
		<tr>
			<td><?=$value->nomor_peserta?></td>
			<td><?=$value->nama?></td>
			<td><?=$value->fakultas?></td>
			<td><?=$value->jurusan?></td>
			<td><?=$value->nmr_hp?></td>
			<td><?=$value->status_verifikasi?></td>
		</tr>
	<?php endforeach ?>
</tbody>
</table>