<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Export
            <small><i>Biodata</i></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-download"></i> Export</a></li>
            <li>Biodata</li>
        </ol>
    </section>
    <section class="content">
        <?php $this->load->view('notification'); ?>
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Export Biodata</h3>
                    </div>
                    <div class="box-body">
                        <?=form_open_multipart('Daftar/Export/Biodata/actionExport/')?>
                        <div class="form-group">
                            <label class="control-label">Tanggal :</label>
                            <select name="tahun" class="form-control" required="">
                                <?php foreach ($tahun as $value): ?>
                                    <option value="<?=$value->tahun?>"><?=$value->tahun?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary btn-flat pull-right"><i class="fa fa-download">&nbsp;</i>Export</button>
                        <?=form_close()?>
                    </div>
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>
    </section>
</div>