<div class="content-wrapper">
                <section class="content-header">
                    <h1>
                        Settings
                        <small><i>Jalur Masuk</i></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-gears"></i> Settings</a></li>
                        <li><a href="#"></i> Jalur Masuk</a></li>
                        <li class="active"><i>Edit</i></li>
                    </ol>
                </section>
                <section class="content">
                    <?php if (!empty($this->session->flashdata('type_message'))) { ?>
                        <div class="alert alert-<?=$this->session->flashdata('type_message')?> alert-dismissible text-center" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <?=$this->session->flashdata('message');?>
                        </div>
                    <?php } ?>
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit Tabel Setting Jalur Masuk</h3>
                        </div>
                        <?=form_open('Daftar/Settings/JalurMasuk/Update/'.$tbSJalurMasuk->id_jlr_msk)?>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Jalur Masuk :</label>
                                <input type="text" name="jalur" class="form-control" placeholder="Jalur Masuk" required="" value="<?=$tbSJalurMasuk->jalur_masuk?>">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Awal Pendaftaran :</label>
                                <div class="row">
                                    <?php $awal = explode(' ', $tbSJalurMasuk->awal); ?>
                                    <div class="col-md-6"><input type="date" name="awal-date" class="form-control" required="" value="<?=$awal[0]?>"></div>
                                    <div class="col-md-6"><input type="time" name="awal-time" class="form-control" required="" value="<?=$awal[1]?>"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Akhir Pendaftaran :</label>
                                <div class="row">
                                    <?php $akhir = explode(' ', $tbSJalurMasuk->akhir); ?>
                                    <div class="col-md-6"><input type="date" name="akhir-date" class="form-control" required="" value="<?=$akhir[0]?>"></div>
                                    <div class="col-md-6"><input type="time" name="akhir-time" class="form-control" required="" value="<?=$akhir[1]?>"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Status Login :</label>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="input-group">
                                        <span class="input-group-addon">
                                            <input type="radio" name="status" class="forn-control" value="AKTIF" <?php if ($tbSJalurMasuk->status == "AKTIF") echo "checked" ?>>
                                        </span>
                                            <input type="text" class="form-control" aria-label="Aktif" value="Aktif" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="input-group">
                                        <span class="input-group-addon">
                                            <input type="radio" name="status" class="forn-control" value="TIDAK AKTIF" <?php if ($tbSJalurMasuk->status == "TIDAK AKTIF") echo "checked" ?>>
                                        </span>
                                            <input type="text" class="form-control" aria-label="Aktif" value="Tidak Aktif" readonly>
                                        </div>
                                    </div>
									<div class="col-md-4">
										<div class="input-group">
                                        <span class="input-group-addon">
                                            <input type="radio" name="status" class="forn-control" value="MAINTENANCE" <?php if ($tbSJalurMasuk->status == "MAINTENANCE") echo "checked" ?>>
                                        </span>
											<input type="text" class="form-control" aria-label="Aktif" value="Maintenance" readonly>
										</div>
									</div>
                                </div>
                            </div>
							<div class="form-group">
								<label for="recipient-name" class="control-label">Status UKT :</label>
								<div class="row">
									<div class="col-md-6">
										<div class="input-group">
                                        <span class="input-group-addon">
                                            <input type="radio" name="status_ukt" class="forn-control" value="AKTIF" <?php if ($tbSJalurMasuk->status_ukt == "AKTIF") echo "checked" ?>>
                                        </span>
											<input type="text" class="form-control" aria-label="Aktif" value="Aktif" readonly>
										</div>
									</div>
									<div class="col-md-6">
										<div class="input-group">
                                        <span class="input-group-addon">
                                            <input type="radio" name="status_ukt" class="forn-control" value="TIDAK AKTIF" <?php if ($tbSJalurMasuk->status_ukt == "TIDAK AKTIF") echo "checked" ?>>
                                        </span>
											<input type="text" class="form-control" aria-label="Aktif" value="Tidak Aktif" readonly>
										</div>
									</div>
								</div>
							</div>
                        </div>
                        <div class="box-footer">
                            <a href="<?=base_url('Daftar/Settings/JalurMasuk/');?>" class="btn btn-default">Kembali</a>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                        <?=form_close()?>
                    </div>
                </section>
            </div>