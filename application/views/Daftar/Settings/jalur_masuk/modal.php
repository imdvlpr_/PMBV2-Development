<!-- Tambah -->
<div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Tambah Jalur Masuk</h4>
                    </div>
                    <?=form_open('Daftar/Settings/JalurMasuk/Tambah/')?>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Jalur Masuk :</label>
                            <input type="text" name="jalur" class="form-control" placeholder="Jalur Masuk" required="">
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Awal Pendaftaran :</label>
                            <div class="row">
                                <div class="col-md-6"><input type="date" name="awal-date" class="form-control" required=""></div>
                                <div class="col-md-6"><input type="time" name="awal-time" class="form-control" required=""></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Akhir Pendaftaran :</label>
                            <div class="row">
                                <div class="col-md-6"><input type="date" name="akhir-date" class="form-control" required=""></div>
                                <div class="col-md-6"><input type="time" name="akhir-time" class="form-control" required=""></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Status Login :</label>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <input type="radio" name="status" class="forn-control" value="AKTIF">
                                        </span>
                                        <input type="text" class="form-control" aria-label="Aktif" value="Aktif" readonly>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <input type="radio" name="status" class="forn-control" value="TIDAK AKTIF">
                                        </span>
                                        <input type="text" class="form-control" aria-label="Aktif" value="Tidak Aktif" readonly>
                                    </div>
                                </div>
								<div class="col-md-4">
									<div class="input-group">
                                        <span class="input-group-addon">
                                            <input type="radio" name="status" class="forn-control" value="MAINTENANCE">
                                        </span>
										<input type="text" class="form-control" aria-label="Aktif" value="Maintenance" readonly>
									</div>
								</div>
                            </div>
                        </div>
						<div class="form-group">
							<label for="recipient-name" class="control-label">Status UKT :</label>
							<div class="row">
								<div class="col-md-6">
									<div class="input-group">
                                        <span class="input-group-addon">
                                            <input type="radio" name="status_ukt" class="forn-control" value="AKTIF">
                                        </span>
										<input type="text" class="form-control" aria-label="Aktif" value="Aktif" readonly>
									</div>
								</div>
								<div class="col-md-6">
									<div class="input-group">
                                        <span class="input-group-addon">
                                            <input type="radio" name="status_ukt" class="forn-control" value="TIDAK AKTIF">
                                        </span>
										<input type="text" class="form-control" aria-label="Aktif" value="Tidak Aktif" readonly>
									</div>
								</div>
							</div>
						</div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                    <?=form_close()?>
                </div>
            </div>
        </div>