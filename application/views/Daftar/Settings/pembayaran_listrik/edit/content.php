<div class="content-wrapper">
                <section class="content-header">
                    <h1>
                        Settings
                        <small><i>Pembayaran Listrik</i></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-gears"></i> Settings</a></li>
                        <li class="active"><i>Pembayaran Listrik</i></li>
                    </ol>
                </section>
                <section class="content">
                    <?php if (!empty($this->session->flashdata('type_message'))) { ?>
                        <div class="alert alert-<?=$this->session->flashdata('type_message')?> alert-dismissible text-center" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <?=$this->session->flashdata('message');?>
                        </div>
                    <?php } ?>
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit Data Setting Pembayaran Listrik</h3>
                        </div>
                        <?=form_open('Daftar/Settings/PembayaranListrik/Update/'.$tbSPembayaranListrik->id_pembayaran_listrik)?>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Pembayaran Listrik :</label>
                                <input type="text" name="pembayaran_listrik" class="form-control" placeholder="Pembayaran Listrik" required="" value="<?=$tbSPembayaranListrik->pembayaran_listrik;?>">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Nilai :</label>
                                <input type="number" min="0" name="nilai" class="form-control" placeholder="Nilai" required="" value="<?=$tbSPembayaranListrik->nilai;?>">
                            </div>
                        </div>
                        <div class="box-footer">
                            <a href="<?=base_url('Daftar/Settings/PembayaranListrik/');?>" class="btn btn-default">Kembali</a>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                        <?=form_close()?>
                    </div>
                </section>
            </div>