<div class="content-wrapper">
                <section class="content-header">
                    <h1>
                        Settings
                        <small><i>Rekening Listrik</i></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-gears"></i> Settings</a></li>
                        <li class="active"><i>Rekening Listrik</i></li>
                    </ol>
                </section>
                <section class="content">
                    <?php if (!empty($this->session->flashdata('type_message'))) { ?>
                        <div class="alert alert-<?=$this->session->flashdata('type_message')?> alert-dismissible text-center" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <?=$this->session->flashdata('message');?>
                        </div>
                    <?php } ?>
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Tabel Setting Rekening Listrik</h3>
                            <div class="pull-right">
                                <button type="button" class="btn btn-primary" id="tambah_tooltip" data-toggle="modal" data-target="#tambah" title="Tambah">
                                    <i class="fa fa-plus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="table-responsive">
                                <table id="dataTable" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Action</th>
                                            <th>Rekening Listrik</th>
                                            <th>Nilai</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no=1; foreach ($tbSRekeningListrik as $value): ?>
                                        <tr>
                                            <td><?=$no?></td>
                                            <td>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        Action <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="<?=base_url('Daftar/Settings/RekeningListrik/Edit/'.$value->id_rekening_listrik)?>"><i class="fa fa-edit"></i> Edit</a></li>
                                                        <li><a href="<?=base_url('Daftar/Settings/RekeningListrik/Delete/'.$value->id_rekening_listrik)?>" onclick="return confirm('Apakah Anda Yakin ?')"><i class="fa fa-trash"></i> Hapus</a></li>
                                                    </ul>
                                                </div>
                                            </td>
                                            <td><?=$value->rekening_listrik?></td>
                                            <td><?=$value->nilai?></td>
                                        </tr>
                                        <?php $no++; endforeach ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>No</th>
                                            <th>Action</th>
                                            <th>Rekening Listrik</th>
                                            <th>Nilai</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </section>
            </div>