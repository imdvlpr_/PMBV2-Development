<!-- Tambah -->
<div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Tambah Tanggungan</h4>
                    </div>
                    <?=form_open_multipart('Daftar/Settings/Tanggungan/Tambah/')?>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Tanggungan :</label>
                            <input type="text" name="tanggungan" class="form-control" placeholder="Tanggungan" required="">
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Nilai :</label>
                            <input type="number" min="0" name="nilai" class="form-control" placeholder="Nilai" required="">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                    <?=form_close()?>
                </div>
            </div>
        </div>