<div class="content-wrapper">
                <section class="content-header">
                    <h1>
                        Settings
                        <small><i>Jenis Pendaftaran</i></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-gears"></i> Settings</a></li>
                        <li class="active"><i>Jenis Pendaftaran</i></li>
                    </ol>
                </section>
                <section class="content">
                    <?php if (!empty($this->session->flashdata('type_message'))) { ?>
                        <div class="alert alert-<?=$this->session->flashdata('type_message')?> alert-dismissible text-center" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <?=$this->session->flashdata('message');?>
                        </div>
                    <?php } ?>
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit Setting Jenis Pendaftaran</h3>
                        </div>
                        <div class="box-body">
                            <?=form_open('Daftar/Settings/JenisPendaftaran/Update/'.$tbSJenisPendaftaran->id_jns_pndftrn)?>
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Jenis Pendaftaran :</label>
                                <input type="text" name="jenis_pendaftaran" class="form-control" placeholder="Jenis Pendaftaran" required="" value="<?=$tbSJenisPendaftaran->jenis_pendaftaran;?>">
                            </div>
                        </div>
                        <div class="box-footer">
                            <a href="<?=base_url('Daftar/Settings/JenisPendaftaran');?>" class="btn btn-default">Kembali</a>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                        <?=form_close()?>
                    </div>
                </section>
            </div>