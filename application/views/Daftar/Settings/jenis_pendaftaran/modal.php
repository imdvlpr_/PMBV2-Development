<!-- Tambah -->
<div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Tambah Jenis Pendaftaran</h4>
                    </div>
                    <?=form_open_multipart('Daftar/Settings/JenisPendaftaran/Tambah/')?>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Jenis Pendaftaran :</label>
                            <input type="text" name="jenis_pendaftaran" class="form-control" placeholder="Jenis Pendaftaran" required="">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                    <?=form_close()?>
                </div>
            </div>
        </div>