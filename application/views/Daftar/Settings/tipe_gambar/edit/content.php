<div class="content-wrapper">
                <section class="content-header">
                    <h1>
                        Settings
                        <small><i>Tipe Gambar</i></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-gears"></i> Settings</a></li>
                        <li class="active"><i>Tipe Gambar</i></li>
                    </ol>
                </section>

                <section class="content">
                    <?php if (!empty($this->session->flashdata('type_message'))) { ?>
                        <div class="alert alert-<?=$this->session->flashdata('type_message')?> alert-dismissible text-center" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <?=$this->session->flashdata('message');?>
                        </div>
                    <?php } ?>
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit Tabel Setting Tipe Gambar</h3>
                        </div>
                        <?=form_open('Daftar/Settings/TipeGambar/Update/'.$tbSTipeGambar->id_tipe_gambar)?>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Tipe Gambar :</label>
                                <input type="text" name="tipe_gambar" class="form-control" placeholder="Tipe Gambar" required="" value="<?=$tbSTipeGambar->tipe_gambar;?>">
                            </div>
                        </div>
                        <div class="box-footer">
                            <a href="<?=base_url('Daftar/Settings/TipeGambar');?>" class="btn btn-default">Kembali</a>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                        <?=form_close()?>
                    </div>
                </section>
            </div>