<div class="content-wrapper">
                <section class="content-header">
                    <h1>
                        Settings
                        <small><i>Rekening PBB</i></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-gears"></i> Settings</a></li>
                        <li class="active"><i>Rekening PBB</i></li>
                    </ol>
                </section>
                <section class="content">
                    <?php if (!empty($this->session->flashdata('type_message'))) { ?>
                        <div class="alert alert-<?=$this->session->flashdata('type_message')?> alert-dismissible text-center" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <?=$this->session->flashdata('message');?>
                        </div>
                    <?php } ?>
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit Data Setting Rekening PBB</h3>
                        </div>
                        <?=form_open('Daftar/Settings/RekeningPbb/Update/'.$tbSRekeningPBB->id_rekening_pbb)?>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Rekening PBB :</label>
                                <input type="text" name="rekening_pbb" class="form-control" placeholder="Rekening PBB" required="" value="<?=$tbSRekeningPBB->rekening_pbb;?>">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Nilai :</label>
                                <input type="number" min="0" name="nilai" class="form-control" placeholder="Nilai" required="" value="<?=$tbSRekeningPBB->nilai;?>">
                            </div>
                        </div>
                        <div class="box-footer">
                            <a href="<?=base_url('Daftar/Settings/RekeningPbb/');?>" class="btn btn-default">Kembali</a>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                        <?=form_close()?>
                    </div>
                </section>
            </div>