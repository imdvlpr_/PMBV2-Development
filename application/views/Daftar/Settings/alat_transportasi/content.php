<div class="content-wrapper">
                <section class="content-header">
                    <h1>
                        Settings
                        <small><i>Alat Transportasi</i></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-gears"></i> Settings</a></li>
                        <li class="active"><i>Alat Transportasi</i></li>
                    </ol>
                </section>
                <section class="content">
                    <?php $this->load->view('notification') ?>
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Tabel Setting Alat Transportasi</h3>
                            <div class="pull-right">
                                <button type="button" class="btn btn-sm btn-primary" id="tambah_tooltip" data-toggle="modal" data-target="#tambah" title="Tambah">
                                    <i class="fa fa-plus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="table-responsive">
                                <table id="agama" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Action</th>
                                            <th>Alat Transportasi</th>
                                            <th>Nilai</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no=1; foreach ($tbSAlatTransportasi as $value): ?>
                                        <tr>
                                            <td><?=$no?></td>
                                            <td>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        Action <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="<?=base_url('Daftar/Settings/AlatTransportasi/Edit/'.$value->id_alat_transportasi)?>"><i class="fa fa-edit"></i></a></li>
                                                        <li><a href="<?=base_url('Daftar/Settings/AlatTransportasi/Delete/'.$value->id_alat_transportasi)?>" onclick="return confirm('Apakah Anda Yakin ?')"><i class="fa fa-trash"></i></a></li>
                                                    </ul>
                                                </div>
                                            </td>
                                            <td><?=$value->alat_transportasi?></td>
                                            <td><?=$value->nilai?></td>
                                        </tr>
                                        <?php $no++; endforeach ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>No</th>
                                            <th>Action</th>
                                            <th>Alat Transportasi</th>
                                            <th>Nilai</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </section>
            </div>