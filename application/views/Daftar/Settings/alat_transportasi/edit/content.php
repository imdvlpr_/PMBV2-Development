<div class="content-wrapper">
                <section class="content-header">
                    <h1>
                        Settings
                        <small><i>Alat Transportasi</i></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-gears"></i> Settings</a></li>
                        <li><a href="#"></i> Alat Transportasi</a></li>
                        <li class="active"><i>Edit</i></li>
                    </ol>
                </section>
                <section class="content">
                <?php $this->load->view('notification') ?>
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit Tabel Setting Alat Transportasi</h3>
                        </div>
                        <?=form_open('Daftar/Settings/AlatTransportasi/Update/'.$tbSAlatTransportasi->id_alat_transportasi)?>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Alat Transportasi :</label>
                                <input type="text" name="alat_transportasi" class="form-control" placeholder="Alat Transportasi" required="" value="<?=$tbSAlatTransportasi->alat_transportasi;?>">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Nilai :</label>
                                <input type="text" name="nilai" class="form-control" placeholder="Nilai" required="" value="<?=$tbSAlatTransportasi->nilai;?>">
                            </div>
                        </div>
                        <div class="box-footer">
                            <a href="<?=base_url('Daftar/Settings/AlatTransportasi/');?>" class="btn btn-default">Kembali</a>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                        <?=form_close()?>
                    </div>
                </section>
            </div>