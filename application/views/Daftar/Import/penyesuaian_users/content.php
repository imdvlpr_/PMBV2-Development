<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Import
            <small><i>Users</i></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-download"></i> Import</a></li>
            <li>Users</li>
        </ol>
    </section>
    <section class="content">
    <?php $this->load->view('notification') ?>
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Import Ke Penyesuaian Users</h3>
                    </div>
                    <div class="box-body">
                        <?=form_open_multipart('Daftar/Import/Penyesuaian/Import')?>
                        <div class="form-group">
                            <label class="control-label">Tahun :</label>
                            <select name="tahun" class="form-control" required="">
                                    <option value="2018">2018</option>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary btn-flat pull-right"><i class="fa fa-download">&nbsp;</i>Import</button>
                        <?=form_close()?>
                    </div>
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>
    </section>
</div>