<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Import
            <small>Kelulusan</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-upload"></i> Import</a></li>
            <li class="active">Kelulusan</li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Import Data Kelulusan</h3>
            </div>
            <div class="box-body">
                <?php $this->load->view('notification') ?>
                <div class="row">
                    <div class="col-md-6">
                        <?=form_open_multipart('Daftar/Import/Kelulusan/Import')?>
                        <div class="form-group">
                            <label class="control-label">Jalur Masuk :</label>
                            <select name="id_jlr_msk" class="form-control" required="">
                                <?php foreach ($tbSJalurMasuk as $value): ?>
                                    <option value="<?=$value->id_jlr_msk?>"><?=$value->jalur_masuk?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Upload file :</label>
                            <input type="file" name="userfile" required>
                        </div>
                        <button type="submit" onclick="return confirm('Apakah jalur masuk sudah sesuai ?')" class="btn btn-primary btn-flat pull-right"><i class="fa fa-upload">&nbsp;</i>Import</button>
                        <?=form_close()?>
                    </div>
                    <div class="col-md-6">
                        <p>Tatacara import data kelulusan : (Only Windows)</p>
                        <ol>
                            <li>Download template kelulusan yang sudah disediakan.</li>
                            <li>Isi kolom pada tabel sesuai nama kolomnya.</li>
                            <li>Pilih jalur masuk dan pilih file yang ingin di upload.</li>
                            <li>klik import.</li>
                        </ol>
                        <a href="<?=base_url('file/template_kelulusan.xlsx')?>" class="btn btn-default btn-flat" target="_blank">Download Template</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>