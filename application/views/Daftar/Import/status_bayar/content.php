<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Import
            <small>Status Bayar (UKT)</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-upload"></i> Import</a></li>
            <li class="active">Status Bayar (UKT)</li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Import Data Status Bayar (UKT)</h3>
            </div>
            <div class="box-body">
            <?php $this->load->view('notification') ?>
                <div class="row">
                    <div class="col-md-6">
                        <?=form_open_multipart('Daftar/Import/StatusBayar/Import')?>
                        <div class="form-group">
                            <label class="control-label">Upload file :</label>
                            <input type="file" name="userfile" required>
                        </div>
                        <button type="submit" onclick="return confirm('Apakah data sudah sesuai template ?')" class="btn btn-primary btn-flat pull-right"><i class="fa fa-upload">&nbsp;</i>Import</button>
                        <?=form_close()?>
                    </div>
                    <div class="col-md-6">
                        <p>Tatacara import data Status Bayar : (Only Windows)</p>
                        <ol>
                            <li>Download template Status Bayar yang sudah disediakan.</li>
                            <li>Isi kolom pada tabel sesuai nama kolomnya.</li>
                            <li>Pilih file yang ingin di upload.</li>
                            <li>klik import.</li>
                        </ol>
                        <a href="<?=base_url('file/template_status_bayar.xlsx')?>" class="btn btn-default btn-flat" target="_blank">Download Template</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>