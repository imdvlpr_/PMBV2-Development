<div class="content-wrapper">
				<section class="content-header">
					<h1>
						Daftar Kelulusan
					</h1>
					<ol class="breadcrumb">
						<li><a href="#"><i class="fa fa-folder-open"></i> Daftar</a></li>
						<li>Kelulusan</li>
					</ol>
				</section>

				<section class="content">
                <?php $this->load->view('notification') ?>
					<div class="box">
						<div class="box-header with-border">
							<h3 class="box-title">Tabel Kelulusan</h3>
						</div>
						<div class="box-body">
							<div class="table-responsive">
								<table id="kelulusan" class="table table-bordered table-striped">
									<thead>
										<tr>
											<th>Action</th>
											<th>Nomor Peserta</th>
											<th>Nama</th>
											<th>Jurusan</th>
											<th>Fakultas</th>
											<th>Jalur Masuk</th>
											<th>Status</th>
											<th>Tahun</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</section>
			</div>