<div class="content-wrapper">
                <section class="content-header">
                    <h1>
                        Daftar
                        <small><i>Kelulusan</i></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-gears"></i> Daftar</a></li>
                        <li><a href="#"></i> Kelulusan</a></li>
                        <li class="active"><i>Edit</i></li>
                    </ol>
                </section>
                <section class="content">
                    <?php $this->load->view('notification') ?>
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit Tabel Setting Kelulusan</h3>
                        </div>
                        <?=form_open('Daftar/Kelulusan/Update/'.$tbDKelulusan->nomor_peserta)?>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Nama :</label>
                                <input type="text" name="nama" class="form-control" placeholder="Nama" required="" value="<?=$tbDKelulusan->nama;?>">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Jurusan :</label>
                                <select name="jurusan" class="form-control select2">
                                    <?php foreach ($tbSJurusan as $value): ?>
                                        <?php if($tbDKelulusan->kode_jurusan == $value->kode_jurusan){ ?>
                                            <option value="<?=$value->kode_jurusan?>" selected><?=$value->jurusan?></option>
                                        <?php }else{ ?>
                                            <option value="<?=$value->kode_jurusan?>"><?=$value->jurusan?></option>
                                        <?php } ?>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Jalur Masuk :</label>
                                <select name="jalur" class="form-control">
                                    <?php foreach ($tbSJalurMasuk as $value): ?>
                                        <?php if($tbDKelulusan->id_jlr_msk == $value->id_jlr_msk){ ?>
                                            <option value="<?=$value->id_jlr_msk?>" selected><?=$value->jalur_masuk?></option>
                                        <?php }else{ ?>
                                            <option value="<?=$value->id_jlr_msk?>"><?=$value->jalur_masuk?></option>
                                        <?php } ?>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Status :</label>
                                <br />
                                <?php if($tbDKelulusan->status == "BELUM DAFTAR"){ ?>
                                    <input type="radio" name="status" value="BELUM DAFTAR" checked> Belum Daftar
                                    <br />
                                    <input type="radio" name="status" value="SUDAH DAFTAR"> Sudah Daftar
                                <?php }else{ ?>
                                    <input type="radio" name="status" value="BELUM DAFTAR"> Belum Daftar
                                    <br />
                                    <input type="radio" name="status" value="SUDAH DAFTAR" checked> Sudah Daftar
                                <?php } ?>
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Tahun :</label>
                                <?php $min = date('Y') - 10; ?>
                                <input type="number" name="tahun" min="<?=$min?>" max="<?=date('Y')?>" class="form-control" placeholder="Tahun" required="" value="<?=$tbDKelulusan->tahun;?>">
                            </div>
                        </div>
                        <div class="box-footer">
                            <a href="<?=base_url('Daftar/Kelulusan');?>" class="btn btn-default">Kembali</a>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                        <?=form_close()?>
                    </div>
                </section>
            </div>