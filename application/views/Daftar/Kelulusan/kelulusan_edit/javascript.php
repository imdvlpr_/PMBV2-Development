<!-- DataTables -->
<script src="<?=base_url('design-backend/plugins/datatables/jquery.dataTables.min.js')?>"></script>
<script src="<?=base_url('design-backend/plugins/datatables/dataTables.bootstrap.min.js')?>"></script>
<script type="text/javascript" language="javascript">
    $(document).ready(function(){
        var dataTable = $('#users').DataTable({
            "processing":true,
            "serverSide":true,
            "order":[],
            "ajax":{
                url:"<?=base_url('Daftar/Users/Json')?>",
                type:"POST"
            },
            "columnDefs":[
                {
                    "targets":[0, 5, 6],  // sesuaikan order table dengan jumlah column
                    "orderable":false,
                },
            ],
        });
    });
</script>