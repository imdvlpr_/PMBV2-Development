<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Dashboard extends CI_Controller {
	
    function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != 'DEVELOPMENT') {
            $this->session->set_flashdata('message','Hak akses ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }

        $this->load->model('Pradaftar/Tbl_pradaftar_biodata');
        $this->load->model('Pradaftar/Tbl_pradaftar_kelulusan');
        $this->load->model('Pradaftar/Tbl_pradaftar_pembayaran');
        $this->load->model('Pradaftar/Tbl_pradaftar_pilihan');
        $this->load->model('Pradaftar/Tbl_pradaftar_users');
        $this->load->model('Tmp/Tmp_pradaftar_users');

    }
	
    function index(){
        $data = array(
            'content'       => 'dashboard/content',
            'css'           => 'dashboard/css',
            'javascript'    => 'dashboard/javascript',
            'modal'         => 'dashboard/modal',
        );
    	$this->load->view('index', $data);
    }

    /* Pradafta */
    function P1(){ //num_users
        $rules = array(
            'select'    => null,
            'where'     => array('YEAR(date_created)' => date('Y')),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $data = array(
            'num_users' => $this->Tbl_pradaftar_users->where($rules)->num_rows(),
        );
        echo json_encode($data);
    }

    function P2(){ //num_users_sv
        $rules = array(
            'select'    => null,
            'where'     => array(
                'biodata' => '1',
                'pembayaran' => '1',
                'YEAR(date_created)' => date('Y')
            ),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $data = array(
            'num_users_sv' => $this->Tbl_pradaftar_users->where($rules)->num_rows(),
        );
        echo json_encode($data);
    }

    function P3(){ //num_users_bv
        $rules = array(
            'select'    => null,
            'where'     => array(
                'biodata' => '0',
                'pembayaran' => '1',
                'YEAR(date_created)' => date('Y')
            ),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $data = array(
            'num_users_bv' => $this->Tbl_pradaftar_users->where($rules)->num_rows(),
        );
        echo json_encode($data);
    }

    function P4(){
        $rules = array(
            'select'    => null,
            'where'     => array(
                'kategori' => 1,
                'YEAR(date_created)' => date('Y')
            ),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $data = array(
            'num_kategori_ipa' => $this->Tbl_pradaftar_users->where($rules)->num_rows()
        );
        echo json_encode($data);
    }

    function P5(){
        $rules = array(
            'select'    => null,
            'where'     => array(
                'kategori' => 2,
                'YEAR(date_created)' => date('Y')
            ),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $data = array(
            'num_kategori_ips' => $this->Tbl_pradaftar_users->where($rules)->num_rows()
        );
        echo json_encode($data);
    }

    function P6(){
        $rules = array(
            'select'    => null,
            'where'     => array(
                'jenis_kelamin' => 'L',
                'YEAR(date_created)' => date('Y')
            ),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $data = array(
            'num_man' => $this->Tbl_pradaftar_biodata->where($rules)->num_rows()
        );
        echo json_encode($data);
    }

    function P7(){
        $rules = array(
            'select'    => null,
            'where'     => array(
                'jenis_kelamin' => 'P',
                'YEAR(date_created)' => date('Y')
            ),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $data = array(
            'num_women' => $this->Tbl_pradaftar_biodata->where($rules)->num_rows()
        );
        echo json_encode($data);
    }

    function P8(){
        $rules = array(
            'select'    => null,
            'where'     => array(
                'status' => '1',
                'YEAR(date_created)' => date('Y')
            ),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $data = array(
            'num_lulus' => $this->Tbl_pradaftar_kelulusan->where($rules)->num_rows()
        );
        echo json_encode($data);
    }

    function P9(){
        $rules = array(
            'select'    => null,
            'where'     => array(
                'status' => '0',
                'YEAR(date_created)' => date('Y')
            ),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $data = array(
            'num_tidak_lulus' => $this->Tbl_pradaftar_kelulusan->where($rules)->num_rows()
        );
        echo json_encode($data);
    }

    function P10(){
        $rules = array(
            'select'    => null,
            'where'     => array('YEAR(date_created)' => date('Y')),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $total = 0;
        $tblPPembayaran = $this->Tbl_pradaftar_pembayaran->where($rules);
        foreach ($tblPPembayaran->result() as $value){
            $total += $value->uang;
        }
        $data = array(
            'num_pembayaran' => $tblPPembayaran->num_rows(),
            'total' => 'Rp '.number_format($total,2),
        );
        echo json_encode($data);
    }

    function P11(){
        $rules = array(
            'select'    => null,
            'where'     => array(
                'status' => '1',
                'YEAR(date_created)' => date('Y')
            ),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $data = array(
            'num_pembayaran_sv' => $this->Tbl_pradaftar_pembayaran->where($rules)->num_rows()
        );
        echo json_encode($data);
    }

    function P12(){
        $rules = array(
            'select'    => null,
            'where'     => array(
                'status' => '0',
                'YEAR(date_created)' => date('Y')
            ),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $data = array(
            'num_pembayaran_bv' => $this->Tbl_pradaftar_pembayaran->where($rules)->num_rows()
        );
        echo json_encode($data);
    }

    function P13(){
        $rules = array(
            'select'    => null,
            'where'     => array('YEAR(date_created)' => date('Y')),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $data = array(
            'num_tmp_users' => $this->Tmp_pradaftar_users->where($rules)->num_rows(),
        );
        echo json_encode($data);
    }

}
