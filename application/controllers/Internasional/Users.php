<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Users extends CI_Controller {

	function __construct(){
		parent::__construct();
		if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
		if ($this->session->userdata('level') != 'DEVELOPMENT') {
			$this->session->set_flashdata('message','Hak akses ditolak.');
			$this->session->set_flashdata('type_message','danger');
			redirect('Auth');
		}
		$this->load->model('Views/Internasional/View_internasional_users');
		$this->load->model('Views/Internasional/View_internasional_pilihan');
		$this->load->model('Internasional/Tbl_internasional_users');
		$this->load->model('Internasional/Tbl_internasional_biodata');
		$this->load->model('Internasional/Tbl_internasional_pilihan');
		$this->load->model('Settings/Tbl_setting_jurusan');
		$this->load->model('Settings/Tbl_setting_negara');
	}

	function index(){
		$rules = array(
            'select'    => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
		$tbIUsers = $this->View_internasional_users->read($rules)->result();
		$data = array(
			'content'       => 'International/Users/users/content',
            'css'           => 'International/Users/users/css',
            'javascript'    => 'International/Users/users/javascript',
            'modal'         => 'International/Users/users/modal',
			'tbIUsers'		=> $tbIUsers,
		);
		$this->load->view('index', $data);
	}

	function SudahBayar(){
		$rules = array(
            'select'    => null,
            'where'     => array(
				'status_bayar' => 'SUDAH BAYAR',
			),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
		);
		$tbIUsers = $this->View_internasional_users->where($rules)->result();
		$data = array(
			'content'       => 'International/Users/users/content',
            'css'           => 'International/Users/users/css',
            'javascript'    => 'International/Users/users/javascript',
            'modal'         => 'International/Users/users/modal',
			'action'		=> 'UbahStatusBelumBayar',
			'tbIUsers'		=> $tbIUsers,
		);
		$this->load->view('index', $data);
	}

	function BelumBayar(){
		$rules = array(
            'select'    => null,
            'where'     => array(
				'status_bayar' => 'BELUM BAYAR',
			),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
		);
		$tbIUsers = $this->View_internasional_users->where($rules)->result();
		$data = array(
			'content'       => 'International/Users/users/content',
            'css'           => 'International/Users/users/css',
            'javascript'    => 'International/Users/users/javascript',
            'modal'         => 'International/Users/users/modal',
			'action'		=> 'UbahStatusSudahBayar',
			'tbIUsers'		=> $tbIUsers,
		);
		$this->load->view('index', $data);
	}

	function UbahStatusSudahBayar($nik_passport){
		$rules = array(
			'where' => array(
				'nik_passport' => $nik_passport,
			),
			'data'  => array(
				'status_bayar' => 'SUDAH BAYAR',
			),
		);

		if($this->Tbl_internasional_users->update($rules)){
			$this->session->set_flashdata('message','Proses Berhasil');
			$this->session->set_flashdata('type_message','success');
			redirect('Internasional/Users/SudahBayar');
		}else{
			$this->session->set_flashdata('message','Proses Gagal');
			$this->session->set_flashdata('type_message','danger');
			redirect('Internasional/Users/BelumBayar');
		}
	}

	function UbahStatusBelumBayar($nik_passport){
		$rules = array(
			'where' => array(
				'nik_passport' => $nik_passport,
			),
			'data'  => array(
				'status_bayar' => 'BELUM BAYAR',
			),
		);

		if($this->Tbl_internasional_users->update($rules)){
			$this->session->set_flashdata('message','Proses Berhasil');
			$this->session->set_flashdata('type_message','success');
			redirect('Internasional/Users/BelumBayar');
		}else{
			$this->session->set_flashdata('message','Proses Gagal');
			$this->session->set_flashdata('type_message','danger');
			redirect('Internasional/Users/SudahBayar');
		}
	}

	function Detail($nik_passport){
		$rules = array(
            'select'    => null,
            'where'     => array(
				'nik_passport'	=> $nik_passport,
			),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
		);
		$tbIUsers = $this->View_internasional_users->where($rules)->row();
		$tbIPilihan = $this->View_internasional_pilihan->where($rules)->row();
		$data = array(
			'content'       => 'International/Users/users_detail/content',
            'css'           => 'International/Users/users_detail/css',
            'javascript'    => 'International/Users/users_detail/javascript',
            'modal'         => 'International/Users/users_detail/modal',
			'tbIUsers'		=> $tbIUsers,
			'tbIPilihan'		=> $tbIPilihan,
		);
		$this->load->view('index',$data);
	}

	function edit($nik){
		$rules = array(
            'select'    => null,
            'where'     => array(
				'nik_passport'	=> $nik,
			),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
		);
		$tbIUsers = $this->View_internasional_users->where($rules)->row();
		$tbIPilihan = $this->View_internasional_pilihan->where($rules)->row();

		$rules2 = array(
            'select'    => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
		$data = array(
			'content'       => 'International/Users/users_edit/content',
            'css'           => 'International/Users/users_edit/css',
            'javascript'    => 'International/Users/users_edit/javascript',
            'modal'         => 'International/Users/users_edit/modal',
			'tbIUsers'		=> $tbIUsers,
			'tbIPilihan'		=> $tbIPilihan,
			'jurusan' 	=> $this->Tbl_setting_jurusan->read($rules2)->result(),
			'negara' 	=> $this->Tbl_setting_negara->read($rules2)->result(),
		);
		$this->load->view('index',$data);
	}

	function Simpan(){
		$rules = array(
			'where' => array(
				'nik_passport' => $this->input->post('nik_passport'),
			),
			'data'  => array(
				'nama'			=> strtoupper($this->input->post('nama')),
				'tempat'		=> strtoupper($this->input->post('tempat')),
				'tgl_lhr'		=> $this->input->post('tglLahir'),
				'jenis_kelamin'	=> $this->input->post('jenisKelamin'),
				'alamat'		=> strtoupper($this->input->post('alamat')),
				'warga_negara'	=> "WNA",
				'kebangsaan'	=> strtoupper($this->input->post('kebangsaan')),
				'asal_sekolah'	=> strtoupper($this->input->post('asalSekolah')),
				'noHp'			=> $this->input->post('phone'),
			),
		);
		$rules2 = array(
			'where' => array(
				'nik_passport' => $this->input->post('nik_passport'),
			),
			'data'  => array(
				'jurusan_1'		=> $this->input->post('jurusan1'),
				'jurusan_2'		=> $this->input->post('jurusan2'),
				'jurusan_3'		=> $this->input->post('jurusan3'),
			),
		);

		if($this->Tbl_internasional_biodata->update($rules)){
			if($this->Tbl_internasional_pilihan->update($rules2)){
				$this->session->set_flashdata('message','Proses Berhasil');
				$this->session->set_flashdata('type_message','success');
				redirect('Internasional/Users/Detail/'.$this->input->post('nik_passport'));
			}else{
				$this->session->set_flashdata('message','Proses Gagal 1');
				$this->session->set_flashdata('type_message','danger');
				redirect('Internasional/Users/Detail'.$this->input->post('nik_passport'));
			}
		}else{
			$this->session->set_flashdata('message','Proses Gagal 2');
			$this->session->set_flashdata('type_message','danger');
			redirect('Internasional/Users/Detail'.$this->input->post('nik_passport'));
		}
	}

}
