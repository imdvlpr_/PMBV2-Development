<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class ExportExcel extends CI_Controller {

	function __construct(){
		parent::__construct();
		if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
		if ($this->session->userdata('level') != 'DEVELOPMENT') {
			$this->session->set_flashdata('message','Hak akses ditolak.');
			$this->session->set_flashdata('type_message','danger');
			redirect('Auth');
		}
		$this->load->model('Views/Internasional/View_internasional_users');
		$this->load->model('Internasional/Tbl_internasional_users');
	}

	function index(){
		$tbIUsers = $this->Tbl_internasional_users->joinJurusanX()->result();
		$data = array(
			'tbIUsers'		=> $tbIUsers,
		);
		$this->load->view('International/export/semua', $data);
	}

	function SudahBayar(){
		$search = array(
			'status_bayar' => 'SUDAH BAYAR',
		);
		$tbIUsers = $this->Tbl_internasional_users->joinJurusan($search)->result();
		$data = array(
			'action'		=> 'UbahStatusBelumBayar',
			'tbIUsers'		=> $tbIUsers,
		);
		$this->load->view('International/export/sudah_bayar', $data);
	}

	function BelumBayar(){
		$search = array(
			'status_bayar' => 'BELUM BAYAR',
		);
		$tbIUsers = $this->Tbl_internasional_users->joinJurusan($search)->result();
		$data = array(
			'action'		=> 'UbahStatusSudahBayar',
			'tbIUsers'		=> $tbIUsers,
		);
		$this->load->view('International/export/belum_bayar', $data);
	}

	function UbahStatusSudahBayar($nik_passport){
		$data = array(
			'status_bayar' => 'SUDAH BAYAR',
		);

		if($this->Tbl_internasional_users->update($nik_passport, $data)){
			$this->session->set_flashdata('message','Proses Berhasil');
			$this->session->set_flashdata('type_message','success');
			redirect('Internasional/Users/SudahBayar');
		}else{
			$this->session->set_flashdata('message','Proses Gagal');
			$this->session->set_flashdata('type_message','danger');
			redirect('Internasional/Users/BelumBayar');
		}
	}

	function UbahStatusBelumBayar($nik_passport){
		$data = array(
			'status_bayar' => 'BELUM BAYAR',
		);

		if($this->Tbl_internasional_users->update($nik_passport, $data)){
			$this->session->set_flashdata('message','Proses Berhasil');
			$this->session->set_flashdata('type_message','success');
			redirect('Internasional/Users/BelumBayar');
		}else{
			$this->session->set_flashdata('message','Proses Gagal');
			$this->session->set_flashdata('type_message','danger');
			redirect('Internasional/Users/SudahBayar');
		}
	}

	function Detail($nik_passport){
		$searchNIK = array(
			'nik_passport'	=> $nik_passport,
		);
		$tbIUsers = $this->View_internasional_users->whereAnd($searchNIK)->row();
		$data = array(
			'tbIUsers'		=> $tbIUsers,
		);
		$this->load->view('internasional/users_detail',$data);
	}

}
