<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Kelulusan extends CI_Controller {
	
    function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != 'DEVELOPMENT') {
            $this->session->set_flashdata('message','Hak akses ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
		$this->load->model('Views/Internasional/View_internasional_kelulusan');
		$this->load->model('Views/Internasional/View_internasional_users');
	}

    function index(){
        $rules = array(
            'select'    => 'YEAR(date_create) as tahun',
            'where'     => null,
            'order'     => null,
        );
    	$data = array(
            'content'       => 'International/Export/kelulusan/content',
            'css'           => 'International/Export/kelulusan/css',
            'javascript'    => 'International/Export/kelulusan/javascript',
            'modal'         => 'International/Export/kelulusan/modal',
    		'tahun'		=> $this->View_internasional_kelulusan->distinct($rules)->result(),
		);
    	$this->load->view('index',$data);
	}
	
    function actionExport(){
        $tahun 		= $this->input->post('tahun');
        $rules = array(
            'select'    => null,
            'where'     => array(
				'YEAR(date_create)' => $tahun
			),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
		);
    	$viewIKelulusan = $this->View_internasional_kelulusan->where($rules)->result();
    	
    	$data = array(
            'content'       => 'International/Export/kelulusan_data/content',
            'css'           => 'International/Export/kelulusan_data/css',
            'javascript'    => 'International/Export/kelulusan_data/javascript',
            'modal'         => 'International/Export/kelulusan_data/modal',
    		'viewKelulusan' => $viewIKelulusan,
    	);
    	$this->load->view('internasional/export/kelulusan_data', $data);
    }

}
