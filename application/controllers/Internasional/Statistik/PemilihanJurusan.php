<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class PemilihanJurusan extends CI_Controller {
	
    function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != 'DEVELOPMENT') {
            $this->session->set_flashdata('message','Hak akses ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }

        $this->load->model('Internasional/Tbl_internasional_pilihan');
        $this->load->model('Views/Settings/View_setting_fakultas_jurusan');
    }
	
    function index(){
		$rules = array(
            'select'    => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
    	$viewSJurusan = $this->View_setting_fakultas_jurusan->read($rules)->result();
    	foreach ($viewSJurusan as $value){
			$rules2 = array(
                'select'    => null,
                'where'     => array(
                    'jurusan_1' => $value->kode_jurusan
                ),
                'or_where'  => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
    		$pilihan1[$value->kode_jurusan] = $this->Tbl_internasional_pilihan->where($rules2)->num_rows();
		}
		foreach ($viewSJurusan as $value){
			$rules2 = array(
                'select'    => null,
                'where'     => array(
                    'jurusan_2' => $value->kode_jurusan
                ),
                'or_where'  => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
			$pilihan2[$value->kode_jurusan] = $this->Tbl_internasional_pilihan->where($rules2)->num_rows();
		}
		foreach ($viewSJurusan as $value){
			$rules2 = array(
                'select'    => null,
                'where'     => array(
                    'jurusan_3' => $value->kode_jurusan
                ),
                'or_where'  => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
			$pilihan3[$value->kode_jurusan] = $this->Tbl_internasional_pilihan->where($rules2)->num_rows();
		}
		foreach ($viewSJurusan as $value){
    		$jumlah[$value->kode_jurusan] = $pilihan1[$value->kode_jurusan] + $pilihan2[$value->kode_jurusan] + $pilihan3[$value->kode_jurusan];
		}
		$data = array(
			'content'       => 'International/Statistik/pemilihan_jurusan/content',
            'css'           => 'International/Statistik/pemilihan_jurusan/css',
            'javascript'    => 'International/Statistik/pemilihan_jurusan/javascript',
            'modal'         => 'International/Statistik/pemilihan_jurusan/modal',
			'viewSJurusan'	=> $viewSJurusan,
			'pilihan1'		=> $pilihan1,
			'pilihan2'		=> $pilihan2,
			'pilihan3'		=> $pilihan3,
			'jumlah'		=> $jumlah,
		);
		$this->load->view('index',$data);
    }

}
