<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Kelulusan extends CI_Controller {
	
    function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != 'DEVELOPMENT') {
            $this->session->set_flashdata('message','Hak akses ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }

        $this->load->model('Internasional/Tbl_internasional_kelulusan');
        $this->load->model('Views/Settings/View_setting_fakultas_jurusan');
    }
	
    function index(){
        $rules = array(
            'select'    => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
    	$viewSJurusan = $this->View_setting_fakultas_jurusan->read($rules)->result();
    	foreach ($viewSJurusan as $value){
            $rules2 = array(
                'select'    => null,
                'where'     => array(
                    'kode_jurusan_kelulusan' => $value->kode_jurusan
                ),
                'or_where'  => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
    		$kelulusan[$value->kode_jurusan] = $this->Tbl_internasional_kelulusan->where($rules2)->num_rows();
		}
		$data = array(
            'content'       => 'International/Statistik/kelulusan/content',
            'css'           => 'International/Statistik/kelulusan/css',
            'javascript'    => 'International/Statistik/kelulusan/javascript',
            'modal'         => 'International/Statistik/kelulusan/modal',
			'viewSJurusan'	=> $viewSJurusan,
			'kelulusan'		=> $kelulusan,
		);
		$this->load->view('index',$data);
    }

}
