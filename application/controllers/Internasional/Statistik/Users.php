<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Users extends CI_Controller {
	
    function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != 'DEVELOPMENT') {
            $this->session->set_flashdata('message','Hak akses ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }

		$this->load->model('Internasional/Tbl_internasional_users');

    }
	
    function index(){
        $rules = array(
            'select'    => null,
            'where'     => array(
                'YEAR(date_created)' => date('Y')
            ),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $rules2 = array(
            'select'    => null,
            'where'     => array(
                'status_bayar' => 'SUDAH BAYAR',
                'YEAR(date_created)' => date('Y')
            ),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $rules3 = array(
            'select'    => null,
            'where'     => array(
                'status_bayar' => 'BELUM BAYAR',
                'YEAR(date_created)' => date('Y')
            ),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
    	$data = array(
            'content'       => 'International/Statistik/users/content',
            'css'           => 'International/Statistik/users/css',
            'javascript'    => 'International/Statistik/users/javascript',
            'modal'         => 'International/Statistik/users/modal',
    		'num_users' 		=> $this->Tbl_internasional_users->where($rules)->num_rows(),
    		'num_users_sv' 		=> $this->Tbl_internasional_users->where($rules2)->num_rows(),
    		'num_users_bv' 		=> $this->Tbl_internasional_users->where($rules3)->num_rows(),

		);
    	$this->load->view('index', $data);
    }

}
