<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class PerNegara extends CI_Controller {
	
    function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != 'DEVELOPMENT') {
            $this->session->set_flashdata('message','Hak akses ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }

        $this->load->model('Views/Internasional/View_internasional_users');
        $this->load->model('Settings/Tbl_setting_negara');
    }
	
    function index(){
        $rules = array(
            'select'    => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
    	$viewSNegara = $this->Tbl_setting_negara->read($rules)->result();
    	foreach ($viewSNegara as $value){
            $rules2 = array(
                'select'    => null,
                'where'     => array(
                    'status_bayar' => 'SUDAH BAYAR',
                    'kebangsaan' => strtoupper($value->country_name),
                    'YEAR(date_created)' => date('Y')
                ),
                'or_where'  => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
    		$jumlah[$value->id_negara] = $this->View_internasional_users->where($rules2)->num_rows();
		}
		$data = array(
            'content'       => 'International/Statistik/per_negara/content',
            'css'           => 'International/Statistik/per_negara/css',
            'javascript'    => 'International/Statistik/per_negara/javascript',
            'modal'         => 'International/Statistik/per_negara/modal',
			'viewSNegara'	=> $viewSNegara,
			'jumlah'		=> $jumlah,
		);
		$this->load->view('index',$data);
    }

}
