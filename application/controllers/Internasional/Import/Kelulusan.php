<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Kelulusan extends CI_Controller {

	function __construct(){
		parent::__construct();
		if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
		if ($this->session->userdata('level') != 'DEVELOPMENT') {
			$this->session->set_flashdata('message','Hak akses ditolak.');
			$this->session->set_flashdata('type_message','danger');
			redirect('Auth');
		}
		$this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
		$this->load->model('Internasional/Tbl_internasional_kelulusan');
		$this->load->model('Settings/Tbl_setting_jalur_masuk');
	}

	function index(){
		$data = array(
			'content'       => 'International/Import/kelulusan/content',
            'css'           => 'International/Import/kelulusan/css',
            'javascript'    => 'International/Import/kelulusan/javascript',
            'modal'         => 'International/Import/kelulusan/modal',
		);
    	$this->load->view('index', $data);
	}

	function Import(){
		$rules[] = array('field' => 'id_jlr_msk', 'label' => 'Jalur Masuk', 'rules' => 'required');
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('message',validation_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('Internasional/Import/Kelulusan/');
		}else{
			$config = array(
				'upload_path'   => './import/internasional/',
				'allowed_types' => 'xls|xlsx|csv|ods|ots',
				'max_size'      => 51200,
				'overwrite'     => TRUE,
				'file_name'     => 'Kelulusan_INTL00'.$this->input->post('id_jlr_msk').'_'.date('Y').'_'.date('H i s d m Y'),
			);
			$this->upload->initialize($config);
			if(!$this->upload->do_upload()){
				$this->session->set_flashdata('message',$this->upload->display_errors());
				$this->session->set_flashdata('type_message','danger');
				redirect('Internasional/Import/Kelulusan/');
			}else{
				$file = $this->upload->data();
				$inputFileName = 'import/internasional/'.$file['file_name'];
				try {
					$inputFileType	= IOFactory::identify($inputFileName);
					$objReader		= IOFactory::createReader($inputFileType);
					$objPHPExcel	= $objReader->load($inputFileName);
				} catch (Exception $e) {
					die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'" : '.$e->getMessage());
				}
				$sheet	= $objPHPExcel->getSheet(0);
				$highestRow	= $sheet->getHighestRow();
				for ($row = 2; $row <= $highestRow; $row++) {
					$nomor_peserta	= $sheet->getCellByColumnAndRow(0,$row)->getValue();
					$status_kelulusan			= str_replace('\'','`',strtoupper($sheet->getCellByColumnAndRow(1,$row)->getValue()));
					$kode_jurusan_kelulusan	= $sheet->getCellByColumnAndRow(2,$row)->getValue();
					$keterangan	= $sheet->getCellByColumnAndRow(3,$row)->getValue();
					$rules = array(
						'select'    => null,
						'where'     => array(
							'nomor_peserta' => $nomor_peserta
						),
						'or_where'  => null,
						'order'     => null,
						'limit'     => null,
						'pagging'   => null,
					);
					$num_rows		= $this->Tbl_internasional_kelulusan->where($rules)->num_rows();
					if ($num_rows == 0) {
						$data = array(
							'nomor_peserta' => $nomor_peserta,
							'status_kelulusan'			=> $status_kelulusan,
							'kode_jurusan_kelulusan'  => $kode_jurusan_kelulusan,
							'keterangan'	=> $keterangan,
						);
						$this->Tbl_internasional_kelulusan->create($data);
					}else{
						$rules = array(
							'where' => array(
								'nomor_peserta' => $nomor_peserta,
							),
							'data'  => array(
								'status_kelulusan'			=> $status_kelulusan,
								'kode_jurusan_kelulusan'  => $kode_jurusan_kelulusan,
								'keterangan'	=> $keterangan,
							),
						);
                        $this->Tbl_internasional_kelulusan->update($rules);
                    }
				}
				$this->session->set_flashdata('message','Import berhasil.');
				$this->session->set_flashdata('type_message','success');
				redirect('Internasional/Import/Kelulusan/');
			}
		}
	}
}