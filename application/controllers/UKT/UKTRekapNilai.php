<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class UKTRekapNilai extends CI_Controller {

	function __construct(){
        parent::__construct();
		if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
		if ($this->session->userdata('level') != "DEVELOPMENT") {
			$this->session->set_flashdata('message','Hak Akses Ditolak.');
			$this->session->set_flashdata('type_message','danger');
			redirect('Dashboard');
		}
		$this->load->model('Settings/Tbl_setting_jalur_masuk');
		$this->load->model('Views/Daftar/View_daftar_extra');
		$this->load->model('Views/Daftar/View_daftar_orangtua');
		$this->load->model('Views/Daftar/View_daftar_mahasiswa');
		$this->load->model('Views/Daftar/View_daftar_ukt');
    }

    function index(){
		$jalur_masuk = $this->input->post('jalur_masuk');
		$tahun = $this->input->post('tahun');
		$status = $this->input->post('status');
		if (empty($jalur_masuk) && empty($tahun) && empty($status)){
		    $kosong = true;
		}else{
            $kosong = false;
            $rules = array(
                'select'    => null,
                'where'     => array(
                    'status'			=> $status,
                    'jalur_masuk'		=> $jalur_masuk,
                    'year(date_created)'=> $tahun,
                ),
                'or_where'  => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
			$viewDUKT	= $this->View_daftar_ukt->where($rules)->result();
			foreach ($viewDUKT as $value) {
                $rules = array(
                    'select'    => null,
                    'where'     => array('id_daftar_users' => $value->id_daftar_users),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
				$viewDExtra = $this->View_daftar_extra->where($rules)->row();
                $viewDMahasiswa = $this->View_daftar_mahasiswa->where($rules)->row();
                $viewDOrangTua = $this->View_daftar_orangtua->where($rules)->result();
				$pekerjaan_ayah = '-';
				$penghasilan_ayah = '-';
                $pekerjaan_ibu = '-';
                $penghasilan_ibu = '-';
                $pekerjaan_wali = '-';
                $penghasilan_wali = '-';
				foreach ($viewDOrangTua as $a){
				    if ($a->orangtua == 'Ayah'){
				        $pekerjaan_ayah = $a->pekerjaan;
				        $penghasilan_ayah = $a->penghasilan;
                    }
                    if ($a->orangtua == 'Ibu'){
                        $pekerjaan_ibu = $a->pekerjaan;
                        $penghasilan_ibu = $a->penghasilan;
                    }
                    if ($a->orangtua == 'Wali'){
                        $pekerjaan_wali = $a->pekerjaan;
                        $penghasilan_wali = $a->penghasilan;
                    }
                }
				$dataDaftarUkt[] = array(
					'nomor_peserta' 		=> $value->nomor_peserta,
					'nama' 					=> $value->nama, 
					'jurusan' 				=> $value->jurusan,
					'kategori' 				=> $value->kategori,
					'score' 				=> $value->score,
					'jumlah' 				=> $value->jumlah,
					'pekerjaan_ayah' 		=> $pekerjaan_ayah,
					'penghasilan_ayah'		=> $penghasilan_ayah,
					'pekerjaan_ibu' 		=> $pekerjaan_ibu,
					'penghasilan_ibu' 		=> $penghasilan_ibu,
					'pekerjaan_wali' 		=> $pekerjaan_wali,
					'penghasilan_wali' 		=> $penghasilan_wali,
					'alat_transportasi' 	=> $viewDMahasiswa->alat_transportasi,
					'tanggungan' 			=> $viewDExtra->tanggungan,
					'rekening_listrik' 		=> $viewDExtra->rekening_listrik,
					'pembayaran_listrik' 	=> $viewDExtra->pembayaran_listrik,
					'rekening_pbb' 			=> $viewDExtra->rekening_pbb,
					'pembayaran_pbb' 		=> $viewDExtra->pembayaran_pbb,
				);
			}
		}
        $rules[0] = array(
            'select'    => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $rules[1] = array(
            'select'    => 'YEAR(date_created) as tahun',
            'where'     => null,
            'order'     => 'tahun DESC',
        );
        $data = array(
            'content'       => 'UKT/ukt_rekap_nilai/content',
            'css'           => 'UKT/ukt_rekap_nilai/css',
            'javascript'    => 'UKT/ukt_rekap_nilai/javascript',
            'modal'         => 'UKT/ukt_rekap_nilai/modal',
            'kosong'        => $kosong,
            'tblSJalurMasuk'=> $this->Tbl_setting_jalur_masuk->read($rules[0])->result(),
            'tahun'         => $this->View_daftar_ukt->distinct($rules[1])->result(),
            'jalur_masuk'	=> (!empty($jalur_masuk))? $jalur_masuk : '',
            'old_tahun'	    => (!empty($tahun))? $tahun : '',
            'status'	    => (!empty($status))? $status : '',
            'viewDaftarUKT'	=> (!empty($dataDaftarUkt))? $dataDaftarUkt : '',
        );
        $this->load->view('index', $data);
    }

}
