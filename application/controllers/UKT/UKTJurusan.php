<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class UKTJurusan extends CI_Controller {

	function __construct(){
        parent::__construct();
		if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
		if ($this->session->userdata('level') != "DEVELOPMENT") {
			$this->session->set_flashdata('message','Hak Akses Ditolak.');
			$this->session->set_flashdata('type_message','danger');
			redirect('Dashboard');
		}
		$this->load->model('Settings/Tbl_setting_jurusan');
		$this->load->model('Settings/Tbl_setting_jalur_masuk');
		$this->load->model('Views/Daftar/View_daftar_ukt');
    }

    function index(){
		$jalur_masuk = $this->input->post('jalur_masuk');
		$tahun 		= $this->input->post('tahun');
		if (empty($jalur_masuk) && empty($tahun)){
		    $kosong = true;
		}else{
            $kosong = false;
            $rules = array(
                'select'    => null,
                'where'     => array(
                    'status' => '1',
                ),
                'or_where'  => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
			$tblSJurusan = $this->Tbl_setting_jurusan->where($rules)->result();
			foreach ($tblSJurusan as $value){
                for ($i = 0; $i < 9; $i++){
                    $rules = array(
                        'select'    => null,
                        'where'     => array(
                            'kode_jurusan'		=> $value->kode_jurusan,
                            'kategori'			=> 'K'.($i+1),
                            'status'			=> 'SUDAH VERIFIKASI',
                            'jalur_masuk'		=> $jalur_masuk,
                            'year(date_created)'=> $tahun,
                        ),
                        'or_where'  => null,
                        'order'     => null,
                        'limit'     => null,
                        'pagging'   => null,
                    );
                    $count[$value->kode_jurusan][$i] = $this->View_daftar_ukt->where($rules)->num_rows();
                }
			}
		}
        $rules[0] = array(
            'select'    => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $rules[1] = array(
            'select'    => 'YEAR(date_created) as tahun',
            'where'     => null,
            'order'     => 'tahun DESC',
        );
        $data = array(
            'content'       => 'UKT/ukt_jurusan/content',
            'css'           => 'UKT/ukt_jurusan/css',
            'javascript'    => 'UKT/ukt_jurusan/javascript',
            'modal'         => 'UKT/ukt_jurusan/modal',
            'kosong'        => $kosong,
            'tblSJalurMasuk'=> $this->Tbl_setting_jalur_masuk->read($rules[0])->result(),
            'tahun'         => $this->View_daftar_ukt->distinct($rules[1])->result(),
            'count'			=> (!empty($count))? $count : '',
            'jalur_masuk'	=> (!empty($jalur_masuk))? $jalur_masuk : '',
            'old_tahun'	    => (!empty($tahun))? $tahun : '',
            'jumlah'		=> null,
            'tbSJurusan'	=> (!empty($tblSJurusan))? $tblSJurusan : '',
        );
        $this->load->view('index', $data);
    }

    function Search($tahun,$jalur,$kategori){
        $rules = array(
            'select'    => null,
            'where'     => array(
                'kategori'			=> $kategori,
                'status'			=> 'SUDAH VERIFIKASI',
                'jalur_masuk'		=> $jalur,
                'year(date_created)'=> $tahun,
            ),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
		$data = array(
            'content'       => 'UKT/ukt_users/content',
            'css'           => 'UKT/ukt_users/css',
            'javascript'    => 'UKT/ukt_users/javascript',
            'modal'         => 'UKT/ukt_users/modal',
			'viewDaftarUKT'	=> $this->View_daftar_ukt->where($rules)->result(),
			'jalur'			=> $jalur,
			'tahun'			=> $tahun,
			'kategori'		=> $kategori,
		);
		$this->load->view('index', $data);
	}

}
