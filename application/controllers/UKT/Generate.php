<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Generate extends CI_Controller {

    function __construct(){
        parent::__construct();
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != "DEVELOPMENT") {
            $this->session->set_flashdata('message','Hak Akses Ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Dashboard');
        }
		$this->load->model('Daftar/Tbl_daftar_ukt');
		$this->load->model('Settings/Tbl_setting_ukt');
        $this->load->model('Settings/Tbl_setting_bobot_range');
        $this->load->model('Settings/Tbl_setting_bobot_ukt');
        $this->load->model('Settings/Tbl_setting_jalur_masuk');
        $this->load->model('Views/Daftar/View_daftar_extra');
		$this->load->model('Views/Daftar/View_daftar_orangtua');
		$this->load->model('Views/Daftar/View_daftar_mahasiswa');
		$this->load->model('Views/Daftar/View_daftar_ukt');
        $this->load->model('Views/Daftar/View_daftar_users');
    }

    function index(){
        $rules[0] = array(
            'select'    => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $rules[1] = array(
            'select'    => 'YEAR(date_created) as tahun',
            'where'     => null,
            'order'     => 'tahun DESC',
        );
        $data = array(
            'content'       => 'UKT/generate/content',
            'css'           => 'UKT/generate/css',
            'javascript'    => 'UKT/generate/javascript',
            'modal'         => 'UKT/generate/modal',
            'tblSJalurMasuk'=> $this->Tbl_setting_jalur_masuk->read($rules[0])->result(),
            'tahun'         => $this->View_daftar_ukt->distinct($rules[1])->result(),
        );
        $this->load->view('index',$data);
    }

    function SWA(){
        $rules[] = array('field' => 'jalur_masuk', 'label' => 'Jalur Masuk', 'rules' => 'required');
        $rules[] = array('field' => 'tahun', 'label' => 'Tahun', 'rules' => 'required');
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == FALSE){
            $this->session->set_flashdata('message',validation_errors());
            $this->session->set_flashdata('type_message','danger');
            redirect('UKT/Generate/');
        }else{
            try{
                $jalur = $this->input->post('jalur_masuk');
                $tahun = $this->input->post('tahun');

                /* Bobot UKT */
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'nama_field' => 'nilai_penghasilan_ayah',
                        'jalur' => $jalur,
                        'tahun' => $tahun
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $nilai_penghasilan_ayah = $this->Tbl_setting_bobot_ukt->where($rules)->row();
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'nama_field' => 'nilai_penghasilan_ibu',
                        'jalur' => $jalur,
                        'tahun' => $tahun
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $nilai_penghasilan_ibu = $this->Tbl_setting_bobot_ukt->where($rules)->row();
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'nama_field' => 'nilai_penghasilan_wali',
                        'jalur' => $jalur,
                        'tahun' => $tahun
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $nilai_penghasilan_wali = $this->Tbl_setting_bobot_ukt->where($rules)->row();
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'nama_field' => 'nilai_pekerjaan_ayah',
                        'jalur' => $jalur,
                        'tahun' => $tahun
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $nilai_pekerjaan_ayah = $this->Tbl_setting_bobot_ukt->where($rules)->row();
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'nama_field' => 'nilai_pekerjaan_ibu',
                        'jalur' => $jalur,
                        'tahun' => $tahun
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $nilai_pekerjaan_ibu = $this->Tbl_setting_bobot_ukt->where($rules)->row();
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'nama_field' => 'nilai_penghasilan_wali',
                        'jalur' => $jalur,
                        'tahun' => $tahun
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $nilai_pekerjaan_wali = $this->Tbl_setting_bobot_ukt->where($rules)->row();
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'nama_field' => 'nilai_rekening_listrik',
                        'jalur' => $jalur,
                        'tahun' => $tahun
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $nilai_rekening_listrik = $this->Tbl_setting_bobot_ukt->where($rules)->row();
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'nama_field' => 'nilai_rekening_pbb',
                        'jalur' => $jalur,
                        'tahun' => $tahun
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $nilai_rekening_pbb = $this->Tbl_setting_bobot_ukt->where($rules)->row();
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'nama_field' => 'nilai_tanggungan',
                        'jalur' => $jalur,
                        'tahun' => $tahun
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $nilai_tanggungan = $this->Tbl_setting_bobot_ukt->where($rules)->row();
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'nama_field' => 'nilai_pembayaran_listrik',
                        'jalur' => $jalur,
                        'tahun' => $tahun
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $nilai_pembayaran_listrik = $this->Tbl_setting_bobot_ukt->where($rules)->row();
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'nama_field' => 'nilai_pembayaran_pbb',
                        'jalur' => $jalur,
                        'tahun' => $tahun
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $nilai_pembayaran_pbb = $this->Tbl_setting_bobot_ukt->where($rules)->row();
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'nama_field' => 'nilai_alat_transportasi',
                        'jalur' => $jalur,
                        'tahun' => $tahun
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $nilai_alat_transportasi = $this->Tbl_setting_bobot_ukt->where($rules)->row();

                /* Rentang UKT */
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'kategori' => 'K1',
                        'jalur' => $jalur,
                        'tahun' => $tahun
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $k1 = $this->Tbl_setting_bobot_range->where($rules)->row();
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'kategori' => 'K2',
                        'jalur' => $jalur,
                        'tahun' => $tahun
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $k2 = $this->Tbl_setting_bobot_range->where($rules)->row();
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'kategori' => 'K3',
                        'jalur' => $jalur,
                        'tahun' => $tahun
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $k3 = $this->Tbl_setting_bobot_range->where($rules)->row();
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'kategori' => 'K4',
                        'jalur' => $jalur,
                        'tahun' => $tahun
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $k4 = $this->Tbl_setting_bobot_range->where($rules)->row();
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'kategori' => 'K5',
                        'jalur' => $jalur,
                        'tahun' => $tahun
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $k5 = $this->Tbl_setting_bobot_range->where($rules)->row();
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'kategori' => 'K6',
                        'jalur' => $jalur,
                        'tahun' => $tahun
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $k6 = $this->Tbl_setting_bobot_range->where($rules)->row();
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'kategori' => 'K7',
                        'jalur' => $jalur,
                        'tahun' => $tahun
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $k7 = $this->Tbl_setting_bobot_range->where($rules)->row();

                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'year(date_created)' => $tahun,
                        'jalur_masuk' => $jalur,
                        'verifikasi' => 'SUDAH VERIFIKASI',
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $mahasiswa	= $this->View_daftar_mahasiswa->where($rules)->result();
                foreach ($mahasiswa as $value){
                    $rules = array(
                        'select'    => null,
                        'where'     => array(
                            'id_daftar_users' => $value->id_daftar_users
                        ),
                        'or_where'  => null,
                        'order'     => null,
                        'limit'     => null,
                        'pagging'   => null,
                    );
                    $extra		= $this->View_daftar_extra->where($rules)->row();
                    $orangtua	= $this->View_daftar_orangtua->where($rules)->result();
                    $viewDUsers = $this->View_daftar_users->where($rules)->row();

                    /* Pembobotan Score */
                    $score_alat_transportasi = ($value->nilai_alat_transportasi / $nilai_alat_transportasi->nilai_max) * $nilai_alat_transportasi->bobot;
                    $score_tanggungan = ($extra->nilai_rekening_listrik / $nilai_rekening_listrik->nilai_max) * $nilai_rekening_listrik->bobot;
                    $score_rekening_listrik = ($extra->nilai_rekening_listrik / $nilai_rekening_listrik->nilai_max) * $nilai_rekening_listrik->bobot;
                    $score_pembayaran_listrik = ($extra->nilai_pembayaran_listrik / $nilai_pembayaran_listrik->nilai_max) * $nilai_pembayaran_listrik->bobot;
                    $score_rekening_pbb = ($extra->nilai_rekening_pbb / $nilai_rekening_pbb->nilai_max) * $nilai_rekening_pbb->bobot;
                    $score_pembayaran_pbb = ($extra->nilai_pembayaran_pbb / $nilai_pembayaran_pbb->nilai_max) * $nilai_pembayaran_pbb->bobot;
                    $score_pekerjaan_ayah = 0 ;
                    $score_penghasilan_ayah = 0;
                    $score_pekerjaan_ibu = 0;
                    $score_penghasilan_ibu = 0;
                    $score_pekerjaan_wali = 0;
                    $score_penghasilan_wali = 0;
                    foreach ($orangtua as $a){
                        if ($a->orangtua == 'Ayah'){
                            $score_pekerjaan_ayah = ($a->nilai_pekerjaan / $nilai_pekerjaan_ayah->nilai_max) * $nilai_pekerjaan_ayah->bobot;
                            $score_penghasilan_ayah = ($a->nilai_penghasilan / $nilai_penghasilan_ayah->nilai_max) * $nilai_penghasilan_ayah->bobot;
                        }
                        if ($a->orangtua == 'Ibu'){
                            $score_pekerjaan_ibu = ($a->nilai_pekerjaan / $nilai_pekerjaan_ibu->nilai_max) * $nilai_pekerjaan_ibu->bobot;
                            $score_penghasilan_ibu = ($a->nilai_penghasilan / $nilai_penghasilan_ibu->nilai_max) * $nilai_penghasilan_ibu->bobot;
                        }
                        if ($a->orangtua == 'Wali'){
                            $score_pekerjaan_wali = ($a->nilai_pekerjaan / $nilai_pekerjaan_wali->nilai_max) * $nilai_pekerjaan_wali->bobot;
                            $score_penghasilan_wali = ($a->nilai_penghasilan / $nilai_penghasilan_wali->nilai_max) * $nilai_penghasilan_wali->bobot;
                        }
                    }

                    $score =
                        $score_alat_transportasi +
                        $score_tanggungan +
                        $score_rekening_listrik +
                        $score_pembayaran_listrik +
                        $score_rekening_pbb +
                        $score_pembayaran_pbb +
                        $score_pekerjaan_ayah +
                        $score_penghasilan_ayah +
                        $score_pekerjaan_ibu +
                        $score_penghasilan_ibu +
                        $score_pekerjaan_wali +
                        $score_penghasilan_wali
                    ;

                    /* Penentuan UKT berdasarkan nilai Score */
                    if ($score >= $k1->nilai_min && $score <= $k1->nilai_max){
                        $kategori = 'K1';
                    }else if ($score > $k2->nilai_min && $score <= $k2->nilai_max){
                        $kategori = 'K2';
                    }else if ($score > $k3->nilai_min && $score <= $k3->nilai_max){
                        $kategori = 'K3';
                    }else if ($score > $k4->nilai_min && $score <= $k4->nilai_max){
                        $kategori = 'K4';
                    }else if ($score > $k5->nilai_min && $score <= $k5->nilai_max){
                        $kategori = 'K5';
                    }else if ($score > $k6->nilai_min && $score <= $k6->nilai_max){
                        $kategori = 'K6';
                    }else if ($score > $k7->nilai_min && $score <= $k7->nilai_max){
                        $kategori = 'K7';
                    }else{
                        $kategori = 'K7';
                    }

                    /* Mencari nominal kategori jurusan */
                    $rules = array(
                        'select'    => null,
                        'where'     => array(
                            'kategori' => $kategori,
                            'kode_jurusan' => $viewDUsers->kode_jurusan,
                        ),
                        'or_where'  => null,
                        'order'     => null,
                        'limit'     => null,
                        'pagging'   => null,
                    );
                    $tblSUKT = $this->Tbl_setting_ukt->where($rules)->row();

                    /* Update kategori */
                    $rules = array(
                        'where' => array('id_daftar_users' => $value->id_daftar_users),
                        'data'  => array(
                            'score'     => $score,
                            'kategori'  => $kategori,
                            'jumlah'    => $tblSUKT->nominal,
                            'status'    => 'SUDAH VERIFIKASI',
                            'updated_by' => $this->session->userdata('id_users'),
                        ),
                    );
                    $this->Tbl_daftar_ukt->update($rules);
                }
                $this->session->set_flashdata('message','Generate data berhasil.');
                $this->session->set_flashdata('type_message','success');
                redirect('UKT/Generate/');
            }catch (Exception $e){
                $this->session->set_flashdata('message',$e->getMessage());
                $this->session->set_flashdata('type_message','success');
                redirect('UKT/Generate/');
            }
        }
    }

    function SWAPersonal(){
        $rules[] = array('field' => 'nomor_peserta', 'label' => 'Nomor Peserta', 'rules' => 'required');
        $rules[] = array('field' => 'jalur_masuk', 'label' => 'Jalur Masuk', 'rules' => 'required');
        $rules[] = array('field' => 'tahun', 'label' => 'Tahun', 'rules' => 'required');
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == FALSE){
            $this->session->set_flashdata('message',validation_errors());
            $this->session->set_flashdata('type_message','danger');
            redirect('UKT/Generate/');
        }else{
            try{
                $nomor_peserta = $this->input->post('nomor_peserta');
                $jalur = $this->input->post('jalur_masuk');
                $tahun = $this->input->post('tahun');

                /* Bobot UKT */
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'nama_field' => 'nilai_penghasilan_ayah',
                        'jalur' => $jalur,
                        'tahun' => $tahun
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $nilai_penghasilan_ayah = $this->Tbl_setting_bobot_ukt->where($rules)->row();
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'nama_field' => 'nilai_penghasilan_ibu',
                        'jalur' => $jalur,
                        'tahun' => $tahun
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $nilai_penghasilan_ibu = $this->Tbl_setting_bobot_ukt->where($rules)->row();
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'nama_field' => 'nilai_penghasilan_wali',
                        'jalur' => $jalur,
                        'tahun' => $tahun
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $nilai_penghasilan_wali = $this->Tbl_setting_bobot_ukt->where($rules)->row();
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'nama_field' => 'nilai_pekerjaan_ayah',
                        'jalur' => $jalur,
                        'tahun' => $tahun
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $nilai_pekerjaan_ayah = $this->Tbl_setting_bobot_ukt->where($rules)->row();
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'nama_field' => 'nilai_pekerjaan_ibu',
                        'jalur' => $jalur,
                        'tahun' => $tahun
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $nilai_pekerjaan_ibu = $this->Tbl_setting_bobot_ukt->where($rules)->row();
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'nama_field' => 'nilai_penghasilan_wali',
                        'jalur' => $jalur,
                        'tahun' => $tahun
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $nilai_pekerjaan_wali = $this->Tbl_setting_bobot_ukt->where($rules)->row();
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'nama_field' => 'nilai_rekening_listrik',
                        'jalur' => $jalur,
                        'tahun' => $tahun
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $nilai_rekening_listrik = $this->Tbl_setting_bobot_ukt->where($rules)->row();
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'nama_field' => 'nilai_rekening_pbb',
                        'jalur' => $jalur,
                        'tahun' => $tahun
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $nilai_rekening_pbb = $this->Tbl_setting_bobot_ukt->where($rules)->row();
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'nama_field' => 'nilai_tanggungan',
                        'jalur' => $jalur,
                        'tahun' => $tahun
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $nilai_tanggungan = $this->Tbl_setting_bobot_ukt->where($rules)->row();
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'nama_field' => 'nilai_pembayaran_listrik',
                        'jalur' => $jalur,
                        'tahun' => $tahun
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $nilai_pembayaran_listrik = $this->Tbl_setting_bobot_ukt->where($rules)->row();
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'nama_field' => 'nilai_pembayaran_pbb',
                        'jalur' => $jalur,
                        'tahun' => $tahun
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $nilai_pembayaran_pbb = $this->Tbl_setting_bobot_ukt->where($rules)->row();
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'nama_field' => 'nilai_alat_transportasi',
                        'jalur' => $jalur,
                        'tahun' => $tahun
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $nilai_alat_transportasi = $this->Tbl_setting_bobot_ukt->where($rules)->row();

                /* Rentang UKT */
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'kategori' => 'K1',
                        'jalur' => $jalur,
                        'tahun' => $tahun
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $k1 = $this->Tbl_setting_bobot_range->where($rules)->row();
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'kategori' => 'K2',
                        'jalur' => $jalur,
                        'tahun' => $tahun
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $k2 = $this->Tbl_setting_bobot_range->where($rules)->row();
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'kategori' => 'K3',
                        'jalur' => $jalur,
                        'tahun' => $tahun
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $k3 = $this->Tbl_setting_bobot_range->where($rules)->row();
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'kategori' => 'K4',
                        'jalur' => $jalur,
                        'tahun' => $tahun
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $k4 = $this->Tbl_setting_bobot_range->where($rules)->row();
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'kategori' => 'K5',
                        'jalur' => $jalur,
                        'tahun' => $tahun
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $k5 = $this->Tbl_setting_bobot_range->where($rules)->row();
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'kategori' => 'K6',
                        'jalur' => $jalur,
                        'tahun' => $tahun
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $k6 = $this->Tbl_setting_bobot_range->where($rules)->row();
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'kategori' => 'K7',
                        'jalur' => $jalur,
                        'tahun' => $tahun
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $k7 = $this->Tbl_setting_bobot_range->where($rules)->row();

                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'nomor_peserta' => $nomor_peserta,
                        'jalur_masuk' => $jalur,
                        'verifikasi' => 'SUDAH VERIFIKASI',
                        'year(date_created)' => $tahun,
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $viewDUsers = $this->View_daftar_users->where($rules)->row();
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'id_daftar_users' => $viewDUsers->id_daftar_users
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $mahasiswa	= $this->View_daftar_mahasiswa->where($rules)->row();
                $extra		= $this->View_daftar_extra->where($rules)->row();
                $orangtua	= $this->View_daftar_orangtua->where($rules)->result();

                /* Pembobotan Score */
                $score_alat_transportasi = ($mahasiswa->nilai_alat_transportasi / $nilai_alat_transportasi->nilai_max) * $nilai_alat_transportasi->bobot;
                $score_tanggungan = ($extra->nilai_rekening_listrik / $nilai_rekening_listrik->nilai_max) * $nilai_rekening_listrik->bobot;
                $score_rekening_listrik = ($extra->nilai_rekening_listrik / $nilai_rekening_listrik->nilai_max) * $nilai_rekening_listrik->bobot;
                $score_pembayaran_listrik = ($extra->nilai_pembayaran_listrik / $nilai_pembayaran_listrik->nilai_max) * $nilai_pembayaran_listrik->bobot;
                $score_rekening_pbb = ($extra->nilai_rekening_pbb / $nilai_rekening_pbb->nilai_max) * $nilai_rekening_pbb->bobot;
                $score_pembayaran_pbb = ($extra->nilai_pembayaran_pbb / $nilai_pembayaran_pbb->nilai_max) * $nilai_pembayaran_pbb->bobot;
                $score_pekerjaan_ayah = 0 ;
                $score_penghasilan_ayah = 0;
                $score_pekerjaan_ibu = 0;
                $score_penghasilan_ibu = 0;
                $score_pekerjaan_wali = 0;
                $score_penghasilan_wali = 0;
                foreach ($orangtua as $a){
                    if ($a->orangtua == 'Ayah'){
                        $score_pekerjaan_ayah = ($a->nilai_pekerjaan / $nilai_pekerjaan_ayah->nilai_max) * $nilai_pekerjaan_ayah->bobot;
                        $score_penghasilan_ayah = ($a->nilai_penghasilan / $nilai_penghasilan_ayah->nilai_max) * $nilai_penghasilan_ayah->bobot;
                    }
                    if ($a->orangtua == 'Ibu'){
                        $score_pekerjaan_ibu = ($a->nilai_pekerjaan / $nilai_pekerjaan_ibu->nilai_max) * $nilai_pekerjaan_ibu->bobot;
                        $score_penghasilan_ibu = ($a->nilai_penghasilan / $nilai_penghasilan_ibu->nilai_max) * $nilai_penghasilan_ibu->bobot;
                    }
                    if ($a->orangtua == 'Wali'){
                        $score_pekerjaan_wali = ($a->nilai_pekerjaan / $nilai_pekerjaan_wali->nilai_max) * $nilai_pekerjaan_wali->bobot;
                        $score_penghasilan_wali = ($a->nilai_penghasilan / $nilai_penghasilan_wali->nilai_max) * $nilai_penghasilan_wali->bobot;
                    }
                }

                $score =
                    $score_alat_transportasi +
                    $score_tanggungan +
                    $score_rekening_listrik +
                    $score_pembayaran_listrik +
                    $score_rekening_pbb +
                    $score_pembayaran_pbb +
                    $score_pekerjaan_ayah +
                    $score_penghasilan_ayah +
                    $score_pekerjaan_ibu +
                    $score_penghasilan_ibu +
                    $score_pekerjaan_wali +
                    $score_penghasilan_wali
                ;

                /* Penentuan UKT berdasarkan nilai Score */
                if ($score >= $k1->nilai_min && $score <= $k1->nilai_max){
                    $kategori = 'K1';
                }else if ($score > $k2->nilai_min && $score <= $k2->nilai_max){
                    $kategori = 'K2';
                }else if ($score > $k3->nilai_min && $score <= $k3->nilai_max){
                    $kategori = 'K3';
                }else if ($score > $k4->nilai_min && $score <= $k4->nilai_max){
                    $kategori = 'K4';
                }else if ($score > $k5->nilai_min && $score <= $k5->nilai_max){
                    $kategori = 'K5';
                }else if ($score > $k6->nilai_min && $score <= $k6->nilai_max){
                    $kategori = 'K6';
                }else if ($score > $k7->nilai_min && $score <= $k7->nilai_max){
                    $kategori = 'K7';
                }else{
                    $kategori = 'K7';
                }

                /* Mencari nominal kategori jurusan */
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'kategori' => $kategori,
                        'kode_jurusan' => $viewDUsers->kode_jurusan,
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $tblSUKT = $this->Tbl_setting_ukt->where($rules)->row();

                /* Update kategori */
                $rules = array(
                    'where' => array('id_daftar_users' => $mahasiswa->id_daftar_users),
                    'data'  => array(
                        'score'     => $score,
                        'kategori'  => $kategori,
                        'jumlah'    => $tblSUKT->nominal,
                        'status'    => 'SUDAH VERIFIKASI',
                        'updated_by' => $this->session->userdata('id_users'),
                    ),
                );
                $this->Tbl_daftar_ukt->update($rules);

                $this->session->set_flashdata('message','Generate data berhasil.');
                $this->session->set_flashdata('type_message','success');
                redirect('UKT/Generate/');
            }catch (Exception $e){
                $this->session->set_flashdata('message',$e->getMessage());
                $this->session->set_flashdata('type_message','success');
                redirect('UKT/Generate/');
            }
        }
    }

}

