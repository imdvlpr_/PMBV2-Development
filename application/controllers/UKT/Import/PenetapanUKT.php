<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class PenetapanUKT extends CI_Controller {

	function __construct(){
		parent::__construct();
		if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
		if ($this->session->userdata('level') != 'DEVELOPMENT') {
			$this->session->set_flashdata('message','Hak akses ditolak.');
			$this->session->set_flashdata('type_message','danger');
			redirect('Auth');
		}
		$this->load->library(array('PHPExcel','PHPExcel/IOFactory'));

        $this->load->model('Daftar/Tbl_daftar_ukt');
        $this->load->model('Settings/Tbl_setting_ukt');
        $this->load->model('Views/Daftar/View_daftar_ukt');
	}

	function index(){
        $data = array(
            'content'       => 'UKT/Import/penetapan_ukt/content',
            'css'           => 'UKT/Import/penetapan_ukt/css',
            'javascript'    => 'UKT/Import/penetapan_ukt/javascript',
            'modal'         => 'UKT/Import/penetapan_ukt/modal',
        );
    	$this->load->view('index', $data);
	}

	function Import(){
		$config = array(
			'upload_path'   => './import/ukt/',
			'allowed_types' => 'xls|xlsx|csv|ods|ots',
			'max_size'      => 51200,
			'overwrite'     => TRUE,
			'file_name'     => 'PENETAPAN_UKT_'.date('H i s d m Y'),
		);
		$this->upload->initialize($config);
		if(!$this->upload->do_upload()){
			$this->session->set_flashdata('message',$this->upload->display_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('UKT/Import/PenetapanUKT/');
		}else {
			$file = $this->upload->data();
			$inputFileName = 'import/ukt/' . $file['file_name'];
			try {
				$inputFileType = IOFactory::identify($inputFileName);
				$objReader = IOFactory::createReader($inputFileType);
				$objPHPExcel = $objReader->load($inputFileName);
			} catch (Exception $e) {
				die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '" : ' . $e->getMessage());
			}
			$sheet = $objPHPExcel->getSheet(0);
			$highestRow = $sheet->getHighestRow();
			$num_update = 0;
			$num_error = 0;
			try{
                for ($row = 2; $row <= $highestRow; $row++) {
                    $nomor_peserta  = $sheet->getCellByColumnAndRow(0, $row)->getValue();
                    $nama           = $sheet->getCellByColumnAndRow(1, $row)->getValue();
                    $kategori       = $sheet->getCellByColumnAndRow(2, $row)->getValue();
                    $rules = array(
                        'select'    => null,
                        'where'     => array(
                            'nomor_peserta' => $nomor_peserta
                        ),
                        'or_where'  => null,
                        'order'     => null,
                        'limit'     => null,
                        'pagging'   => null,
                    );
                    $viewDaftarUKT = $this->View_daftar_ukt->where($rules);
                    if ($viewDaftarUKT->num_rows() > 0){
                        $viewDaftarUKT = $viewDaftarUKT->row();
                        $rules = array(
                            'select'    => null,
                            'where'     => array(
                                'kategori' => $kategori,
                                'kode_jurusan' => $viewDaftarUKT->kode_jurusan,
                            ),
                            'or_where'  => null,
                            'order'     => null,
                            'limit'     => null,
                            'pagging'   => null,
                        );
                        $tbSUKT = $this->Tbl_setting_ukt->where($rules)->row();
                        $rules = array(
                            'where' => array(
                                'id_daftar_users' => $viewDaftarUKT->id_daftar_users
                            ),
                            'data'  => array(
                                'score' => 1,
                                'kategori' => $kategori,
                                'jumlah' => $tbSUKT->nominal,
                                'status' => 'SUDAH VERIFIKASI',
                                'updated_by' => $this->session->userdata('id_users'),
                            ),
                        );
                        if ($this->Tbl_daftar_ukt->update($rules)){
                            $num_update++;
                        }else{
                            $num_error++;
                            $error[] = "Nomor Peserta/Nama : $nomor_peserta / $nama";
                        }
                    }else{
                        $error[] = "Nomor Peserta/Nama : $nomor_peserta / $nama";
                    }
                }
                $this->session->set_flashdata('message', 'Import berhasil. Update : '.$num_update.' Error : '.$num_error);
                $this->session->set_flashdata('type_message', 'success');
                redirect('UKT/Import/PenetapanUKT/');
            }catch (Exception $e){
                $this->session->set_flashdata('message', $e->getMessage());
                $this->session->set_flashdata('type_message', 'danger');
                redirect('UKT/Import/PenetapanUKT/');
            }

		}
	}
}

