<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class BobotRangeUkt extends CI_Controller {

    function __construct(){
        parent::__construct();
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != "DEVELOPMENT") {
            $this->session->set_flashdata('message','Hak Akses Ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Dashboard');
        }
		$this->load->model('Histori/Tbl_histori_bobot_range_ukt');
    }

	function index(){
        $rules = array(
            'select'    => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $data = array(
            'content'           => 'UKT/Histori/histori_bobot_range/content',
            'css'               => 'UKT/Histori/histori_bobot_range/css',
            'javascript'        => 'UKT/Histori/histori_bobot_range/javascript',
            'modal'             => 'UKT/Histori/histori_bobot_range/modal',
            'tblHBobotRangeUkt' => $this->Tbl_histori_bobot_range_ukt->read($rules)->result(),
        );
        $this->load->view('index',$data);
	}

}

