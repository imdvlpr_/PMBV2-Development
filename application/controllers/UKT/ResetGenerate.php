<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class ResetGenerate extends CI_Controller {

	function __construct(){
        parent::__construct();
		if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
		if ($this->session->userdata('level') != "DEVELOPMENT") {
			$this->session->set_flashdata('message','Hak Akses Ditolak.');
			$this->session->set_flashdata('type_message','danger');
			redirect('Dashboard');
		}
		$this->load->model('Daftar/Tbl_daftar_ukt');
		$this->load->model('Settings/Tbl_setting_jalur_masuk');
		$this->load->model('Views/Daftar/View_daftar_ukt');
    }

    function index(){
        $rules[0] = array(
            'select'    => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $rules[1] = array(
            'select'    => 'YEAR(date_created) as tahun',
            'where'     => null,
            'order'     => 'tahun DESC',
        );
        $data = array(
            'content'       => 'UKT/reset_generate/content',
            'css'           => 'UKT/reset_generate/css',
            'javascript'    => 'UKT/reset_generate/javascript',
            'modal'         => 'UKT/reset_generate/modal',
            'tblSJalurMasuk'=> $this->Tbl_setting_jalur_masuk->read($rules[0])->result(),
            'tahun'         => $this->View_daftar_ukt->distinct($rules[1])->result(),
        );
        $this->load->view('index',$data);
    }

    function Reset(){
        $rules[] = array('field' => 'jalur_masuk', 'label' => 'Jalur Masuk', 'rules' => 'required');
        $rules[] = array('field' => 'tahun', 'label' => 'Tahun', 'rules' => 'required');
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == FALSE){
            $this->session->set_flashdata('message',validation_errors());
            $this->session->set_flashdata('type_message','danger');
            redirect('UKT/Generate/');
        }else{
            try{
                $jalur_masuk = $this->input->post('jalur_masuk');
                $tahun = $this->input->post('tahun');
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'status'    => 'SUDAH VERIFIKASI',
                        'jalur_masuk'   => $jalur_masuk,
                        'year(date_created)' => $tahun,
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $viewDaftarUKT = $this->View_daftar_ukt->where($rules)->result();
                $num_reset_update	= 0;
                $num_reset_error	= 0;
                foreach ($viewDaftarUKT as $value) {
                    $rules = array(
                        'where' => array('id_daftar_users' => $value->id_daftar_users),
                        'data'  => array(
                            'score' => 0,
                            'kategori' => 'K1',
                            'jumlah' => 0,
                            'status' => 'BELUM VERIFIKASI',
                            'updated_by' => $this->session->userdata('id_users'),
                        ),
                    );
                    if ($this->Tbl_daftar_ukt->update($rules)) {
                        $num_reset_update++;
                    }else{
                        $num_reset_error++;
                    }
                }
                $this->session->set_flashdata('message','Reset data berhasil. Update : '.$num_reset_update.' Error : '.$num_reset_error);
                $this->session->set_flashdata('type_message','success');
                redirect('UKT/ResetGenerate/');
            }catch (Exception $e){
                $this->session->set_flashdata('message', $e->getMessage());
                $this->session->set_flashdata('type_message','success');
                redirect('UKT/ResetGenerate/');
            }
        }
    }

}
