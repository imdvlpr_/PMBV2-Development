<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration2018 extends CI_Controller {

    var $token  = 'ea9f91b2cda019730f2891bd12a7a4d6';
    var $url    = 'http://php5-6.alfi-gusman.web.id/PMBV2-API/';

    function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != 'DEVELOPMENT') {
            $this->session->set_flashdata('message','Hak akses ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        $this->load->model('Daftar/Tbl_daftar_extra');
        $this->load->model('Daftar/Tbl_daftar_gambar');
        $this->load->model('Daftar/Tbl_daftar_kelulusan');
        $this->load->model('Daftar/Tbl_daftar_mahasiswa');
        $this->load->model('Daftar/Tbl_daftar_orangtua');
        $this->load->model('Daftar/Tbl_daftar_ukt');
        $this->load->model('Daftar/Tbl_daftar_users');
        $this->load->model('Histori/Tbl_histori_bobot_nilai_ukt');
        $this->load->model('Histori/Tbl_histori_bobot_range_ukt');
        $this->load->model('Internasional/Tbl_internasional_biodata');
        $this->load->model('Internasional/Tbl_internasional_kelulusan');
        $this->load->model('Internasional/Tbl_internasional_pilihan');
        $this->load->model('Internasional/Tbl_internasional_users');
        $this->load->model('Penyesuaian/Tbl_penyesuaian_extra');
        $this->load->model('Penyesuaian/Tbl_penyesuaian_gambar');
        $this->load->model('Penyesuaian/Tbl_penyesuaian_pddikti');
        $this->load->model('Penyesuaian/Tbl_penyesuaian_ukt');
        $this->load->model('Penyesuaian/Tbl_penyesuaian_users');
        $this->load->model('Pradaftar/Tbl_pradaftar_biodata');
        $this->load->model('Pradaftar/Tbl_pradaftar_jadwal');
        $this->load->model('Pradaftar/Tbl_pradaftar_kelulusan');
        $this->load->model('Pradaftar/Tbl_pradaftar_orangtua');
        $this->load->model('Pradaftar/Tbl_pradaftar_pembayaran');
        $this->load->model('Pradaftar/Tbl_pradaftar_pilihan');
        $this->load->model('Pradaftar/Tbl_pradaftar_setting');
        $this->load->model('Pradaftar/Tbl_pradaftar_users');
        $this->load->model('Pradaftar/Tbl_pradaftar_users');
        $this->load->model('Tmp/Tmp_pradaftar_users');
    }

    function index(){
        /* Migration Data PMB 2018  */

        /* Daftar */
        //$this->tbl_daftar_kelulusan($this->url);
        //$this->tbl_daftar_users($this->url);
        //$this->tbl_daftar_pddikti($this->url);
        //$this->tbl_daftar_extra($this->url);
        //$this->tbl_daftar_gambar($this->url);
        //$this->tbl_daftar_ukt($this->url);
        //$this->UpdateUsers();
        //$this->UpdateExtra();
        //$this->UpdateGambar();
        //$this->UpdateMahasiswa();
        //$this->UpdateOrangTua();
        //$this->UpdateUKT();

        /* Pradaftar */
        //$this->tbl_pradaftar_users($this->url);
        //$this->tbl_pradaftar_pembayaran($this->url);
        //$this->tbl_pradaftar_biodata($this->url);
        //$this->tbl_pradaftar_ujian($this->url);
        //$this->tbl_pradaftar_kelulusan($this->url);
        //$this->update_pembayaran($this->url);
        //$this->update_biodata();
        //$this->update_nilai($this->url);
        //$this->update_orangtua();

        /* Histori
        $this->tbl_histori_bobot_nilai_ukt($this->url);
        $this->tbl_histori_bobot_range_ukt($this->url);
        */

        /* Temporary Pradaftar Users
        $this->tmp_pradaftar_users($this->url);
        */
    }

    /* Daftar */
    function tbl_daftar_extra($url){
        $controller = 'Migration2018/tbl_daftar_extra/'.$this->token;
        $response 	= file_get_contents($url.$controller);
        $json 		= json_decode($response);
        foreach ($json->data as $a){
            try{
                $data = array(
                    'id_daftar_users' => 1,
                    'nik_passport' => $a->nik_passport,
                    'id_tanggungan' => $a->id_tanggungan,
                    'id_rekening_listrik' => $a->id_rekening_listrik,
                    'id_pembayaran_listrik' => $a->id_pembayaran_listrik,
                    'id_rekening_pbb' => $a->id_rekening_pbb,
                    'id_pembayaran_pbb' => $a->id_pembayaran_pbb,
                    'ukuran_baju' => $a->ukuran_baju,
                    'ukuran_jas' => $a->ukuran_jas,
                    'date_created' => $a->date_create,
                    'date_updated' => $a->date_update,
                );
                $this->Tbl_daftar_extra->create($data);
                $rules = array(
                    'where' => array(
                        'nik_passport' => $a->nik_passport,
                        'orangtua' => 'Ayah'
                    ), //not null
                    'data'  => array(
                        'nominal_penghasilan' => (!empty($a->nominal_penghasilan_ayah))? $a->nominal_penghasilan_ayah : '0',
                        'terbilang_penghasilan' => (!empty($a->terbilang_penghasilan_ayah))? $a->terbilang_penghasilan_ayah : '-',
                    ), //not null
                );
                $this->Tbl_daftar_orangtua->update($rules);
                $rules = array(
                    'where' => array(
                        'nik_passport' => $a->nik_passport,
                        'orangtua' => 'Ibu'
                    ), //not null
                    'data'  => array(
                        'nominal_penghasilan' => (!empty($a->nominal_penghasilan_ibu))? $a->nominal_penghasilan_ibu : '0',
                        'terbilang_penghasilan' => (!empty($a->terbilang_penghasilan_ibu))? $a->terbilang_penghasilan_ibu : '-',
                    ), //not null
                );
                $this->Tbl_daftar_orangtua->update($rules);
                $rules = array(
                    'where' => array(
                        'nik_passport' => $a->nik_passport,
                        'orangtua' => 'Wali'
                    ), //not null
                    'data'  => array(
                        'nominal_penghasilan' => (!empty($a->nominal_penghasilan_wali))? $a->nominal_penghasilan_wali : '0',
                        'terbilang_penghasilan' => (!empty($a->terbilang_penghasilan_wali))? $a->terbilang_penghasilan_wali : '-',
                    ), //not null
                );
                $this->Tbl_daftar_orangtua->update($rules);
            }catch (Exception $e){
                echo $e->getMessage();
            }
        }
    }
    function tbl_daftar_gambar($url){
        $controller = 'Migration2018/tbl_daftar_gambar/'.$this->token;
        $response 	= file_get_contents($url.$controller);
        $json 		= json_decode($response);
        foreach ($json->data as $a){
            try{
                $data = array(
                    'id_daftar_users' => 1,
                    'nik_passport' => $a->nik_passport,
                    'id_tipe_gambar' => $a->id_tipe_gambar,
                    'gambar' => $a->gambar,
                    'date_created' => $a->date_create,
                    'date_updated' => $a->date_update,
                );
                $this->Tbl_daftar_gambar->create($data);
            }catch (Exception $e){
                echo $e->getMessage();
            }
        }
    }
    function tbl_daftar_kelulusan($url){
        $controller = 'Migration2018/tbl_daftar_kelulusan/'.$this->token;
        $response 	= file_get_contents($url.$controller);
        $json 		= json_decode($response);
        foreach ($json->data as $a){
            try{
                $data = array(
                    'nomor_peserta' => $a->nomor_peserta,
                    'nama' => $a->nama,
                    'kode_jurusan' => $a->kode_jurusan,
                    'id_jlr_msk' => $a->id_jlr_msk,
                    'status' => $a->status,
                    'tahun' => $a->tahun,
                    'created_by' => $a->create_by,
                    'updated_by' => $a->update_by,
                    'date_created' => $a->date_create,
                    'date_updated' => $a->date_update,
                );
                $this->Tbl_daftar_kelulusan->create($data);
            }catch (Exception $e){
                echo $e->getMessage();
            }
        }
    }
    function tbl_daftar_pddikti($url){
        $controller = 'Migration2018/tbl_daftar_pddikti/'.$this->token;
        $response 	= file_get_contents($url.$controller);
        $json 		= json_decode($response);
        foreach ($json->data as $a){
            try{
                $data = array(
                    'id_daftar_users' => 1,
                    'nik_passport' => (!empty($a->nik_passport))? $a->nik_passport : '-',
                    'nim' => (!empty($a->nim))? $a->nim : '-',
                    'tmp_lhr' => (!empty($a->tmp_lhr))? $a->tmp_lhr : '-',
                    'jenis_kelamin' => (!empty($a->jenis_kelamin))? $a->jenis_kelamin : '-',
                    'id_agama' => (!empty($a->id_agama))? $a->id_agama : '-',
                    'nisn' => (!empty($a->nisn))? $a->nisn : '-',
                    'npwp' => (!empty($a->npwp))? $a->npwp : '-',
                    'warga_negara' => (!empty($a->warga_negara))? $a->warga_negara : '-',
                    'id_jns_pndftrn' => (!empty($a->id_jns_pndftrn))? $a->id_jns_pndftrn : '-',
                    'tgl_msk_kuliah' => (!empty($a->tgl_msk_kuliah))? $a->tgl_msk_kuliah : '-',
                    'mulai_semester ' => (!empty($a->mulai_semester ))? $a->mulai_semester  : '-',
                    'jalan' => (!empty($a->jalan))? $a->jalan : '-',
                    'rt' => (!empty($a->rt))? $a->rt : '-',
                    'rw' => (!empty($a->rw))? $a->rw : '-',
                    'nama_dusun' => (!empty($a->nama_dusun))? $a->nama_dusun : '-',
                    'id_kelurahan' => (!empty($a->id_kelurahan))? $a->id_kelurahan : '-',
                    'kode_pos' => (!empty($a->kode_pos))? $a->kode_pos : '-',
                    'id_jns_tinggal' => (!empty($a->id_jns_tinggal))? $a->id_jns_tinggal : '-',
                    'id_alat_transportasi' => (!empty($a->id_alat_transportasi))? $a->id_alat_transportasi : '-',
                    'id_rumpun' => (!empty($a->id_rumpun))? $a->id_rumpun : '-',
                    'tlp_rmh' => (!empty($a->tlp_rmh))? $a->tlp_rmh : '-',
                    'terima_kps' => (!empty($a->terima_kps))? $a->terima_kps : '-',
                    'date_created' => $a->date_create,
                    'date_updated' => $a->date_update,
                );
                $this->Tbl_daftar_mahasiswa->create($data);
                if(!empty($a->nik_ayah) || !empty($a->nama_ayah)){
                    $data = array(
                        'id_daftar_users' => 1,
                        'nik_passport' => $a->nik_passport,
                        'orangtua' => 'Ayah',
                        'nik_orangtua' => (!empty($a->nik_ayah))? $a->nik_ayah : '-',
                        'nama_orangtua' => (!empty($a->nama_ayah))? $a->nama_ayah : '-',
                        'tgl_lhr_orangtua' => (!empty($a->tgl_lhr_ayah))? $a->tgl_lhr_ayah : '-',
                        'id_pendidikan' => (!empty($a->id_pendidikan_ayah))? $a->id_pendidikan_ayah : '-',
                        'id_pekerjaan' => (!empty($a->id_pekerjaan_ayah))? $a->id_pekerjaan_ayah : '-',
                        'id_penghasilan' => (!empty($a->id_penghasilan_ayah))? $a->id_penghasilan_ayah : '-',
                        'nominal_penghasilan' => 0,
                        'terbilang_penghasilan' => '-',
                        'date_created' => $a->date_create,
                        'date_updated' => $a->date_update,
                    );
                    $this->Tbl_daftar_orangtua->create($data);
                }
                if(!empty($a->nik_ibu) || !empty($a->nama_ibu)){
                    $data = array(
                        'id_daftar_users' => 1,
                        'nik_passport' => $a->nik_passport,
                        'orangtua' => 'Ibu',
                        'nik_orangtua' => (!empty($a->nik_ibu))? $a->nik_ibu : '-',
                        'nama_orangtua' => (!empty($a->nama_ibu))? $a->nama_ibu : '-',
                        'tgl_lhr_orangtua' => (!empty($a->tgl_lhr_ibu))? $a->tgl_lhr_ibu : '-',
                        'id_pendidikan' => (!empty($a->id_pendidikan_ibu))? $a->id_pendidikan_ibu : '-',
                        'id_pekerjaan' => (!empty($a->id_pekerjaan_ibu))? $a->id_pekerjaan_ibu : '-',
                        'id_penghasilan' => (!empty($a->id_penghasilan_ibu))? $a->id_penghasilan_ibu : '-',
                        'nominal_penghasilan' => 0,
                        'terbilang_penghasilan' => '-',
                        'date_created' => $a->date_create,
                        'date_updated' => $a->date_update,
                    );
                    $this->Tbl_daftar_orangtua->create($data);
                }
                if(!empty($a->nik_wali) || !empty($a->nama_wali)){
                    $data = array(
                        'id_daftar_users' => 1,
                        'nik_passport' => $a->nik_passport,
                        'orangtua' => 'Wali',
                        'nik_orangtua' => (!empty($a->nik_wali))? $a->nik_wali : '-',
                        'nama_orangtua' => (!empty($a->nama_wali))? $a->nama_wali : '-',
                        'tgl_lhr_orangtua' => (!empty($a->tgl_lhr_wali))? $a->tgl_lhr_wali : '-',
                        'id_pendidikan' => (!empty($a->id_pendidikan_wali))? $a->id_pendidikan_wali : '-',
                        'id_pekerjaan' => (!empty($a->id_pekerjaan_wali))? $a->id_pekerjaan_wali : '-',
                        'id_penghasilan' => (!empty($a->id_penghasilan_wali))? $a->id_penghasilan_wali : '-',
                        'nominal_penghasilan' => 0,
                        'terbilang_penghasilan' => '-',
                        'date_created' => $a->date_create,
                        'date_updated' => $a->date_update,
                    );
                    $this->Tbl_daftar_orangtua->create($data);
                }
            }catch (Exception $e){
                echo $e->getMessage();
            }
        }
    }
    function tbl_daftar_ukt($url){
        $controller = 'Migration2018/tbl_daftar_ukt/'.$this->token;
        $response 	= file_get_contents($url.$controller);
        $json 		= json_decode($response);
        foreach ($json->data as $a){
            try{
                $data = array(
                    'id_daftar_users' => 1,
                    'nik_passport' => $a->nik_passport,
                    'score' => $a->score,
                    'kategori' => $a->kategori,
                    'jumlah' => $a->jumlah,
                    'status' => $a->status,
                    'status_bayar' => $a->status_bayar,
                    'created_by' => $a->create_by,
                    'updated_by' => $a->update_by,
                    'date_created' => $a->date_create,
                    'date_updated' => $a->date_update,
                );
                $this->Tbl_daftar_ukt->create($data);
            }catch (Exception $e){
                echo $e->getMessage();
            }
        }
    }
    function tbl_daftar_users($url){
        $controller = 'Migration2018/tbl_daftar_users/'.$this->token;
        $response 	= file_get_contents($url.$controller);
        $json 		= json_decode($response);
        foreach ($json->data as $a){
            try{
                $data = array(
                    'id_daftar_kelulusan' => 1,
                    'nomor_peserta' => $a->nomor_peserta,
                    'nik_passport' => $a->nik_passport,
                    'username' => $a->username,
                    'password   ' => $a->password,
                    'nmr_hp' => $a->nmr_hp,
                    'tgl_lhr' => $a->tgl_lhr,
                    'ip_login' => $a->ip_login,
                    'date_login' => ($a->date_login != '0000-00-00 00:00:00')? $a->date_login : '2018-01-01 01:00:00',
                    'ip_logout' => $a->ip_logout,
                    'date_logout' => ($a->date_logout != '0000-00-00 00:00:00')? $a->date_logout : '2018-01-01 01:00:00',
                    'foto' => $a->foto,
                    'verifikasi' => $a->verifikasi,
                    'date_created' => '2018-01-01 00:00:00',
                    'date_updated' => '2018-01-01 00:00:00',
                );
                $this->Tbl_daftar_users->create($data);
            }catch (Exception $e){
                echo $e->getMessage();
            }
        }
    }
    function UpdateUsers(){
        $rules = array(
            'select'    => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $tblDUsers = $this->Tbl_daftar_users->read($rules)->result();
        foreach ($tblDUsers as $a){
            try{
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'nomor_peserta' => $a->nomor_peserta
                    ), //not null or null
                    'or_where'  => null, //not null or null
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $tblDKelulusan = $this->Tbl_daftar_kelulusan->where($rules)->row();
                $rules = array(
                    'where' => array(
                        'nomor_peserta' => $a->nomor_peserta
                    ), //not null
                    'data'  => array(
                        'id_daftar_kelulusan' => $tblDKelulusan->id_daftar_kelulusan
                    ), //not null
                );
                $this->Tbl_daftar_users->update($rules);
            }catch (Exception $e){
                echo $e->getMessage();
            }
        }
    }
    function UpdateExtra(){
        $rules = array(
            'select'    => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $tblDExtra = $this->Tbl_daftar_extra->read($rules)->result();
        foreach ($tblDExtra as $a){
            try{
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'nik_passport' => $a->nik_passport
                    ), //not null or null
                    'or_where'  => null, //not null or null
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $tblDUsers = $this->Tbl_daftar_users->where($rules)->row();
                $rules = array(
                    'where' => array(
                        'nik_passport' => $a->nik_passport
                    ), //not null
                    'data'  => array(
                        'id_daftar_users' => $tblDUsers->id_daftar_users
                    ), //not null
                );
                $this->Tbl_daftar_extra->update($rules);
            }catch (Exception $e){
                echo $e->getMessage();
            }
        }
    }
    function UpdateGambar(){
        $rules = array(
            'select'    => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $tblDGambar = $this->Tbl_daftar_gambar->read($rules)->result();
        foreach ($tblDGambar as $a){
            try{
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'nik_passport' => $a->nik_passport
                    ), //not null or null
                    'or_where'  => null, //not null or null
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $tblDUsers = $this->Tbl_daftar_users->where($rules)->row();
                $rules = array(
                    'where' => array(
                        'nik_passport' => $a->nik_passport
                    ), //not null
                    'data'  => array(
                        'id_daftar_users' => $tblDUsers->id_daftar_users
                    ), //not null
                );
                $this->Tbl_daftar_gambar->update($rules);
            }catch (Exception $e){
                echo $e->getMessage();
            }
        }
    }
    function UpdateMahasiswa(){
        $rules = array(
            'select'    => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $tblDMahasiswa = $this->Tbl_daftar_mahasiswa->read($rules)->result();
        foreach ($tblDMahasiswa as $a){
            try{
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'nik_passport' => $a->nik_passport
                    ), //not null or null
                    'or_where'  => null, //not null or null
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $tblDUsers = $this->Tbl_daftar_users->where($rules)->row();
                $rules = array(
                    'where' => array(
                        'nik_passport' => $a->nik_passport
                    ), //not null
                    'data'  => array(
                        'id_daftar_users' => $tblDUsers->id_daftar_users
                    ), //not null
                );
                $this->Tbl_daftar_mahasiswa->update($rules);
            }catch (Exception $e){
                echo $e->getMessage();
            }
        }
    }
    function UpdateOrangTua(){
        $rules = array(
            'select'    => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $tblDOrangTua = $this->Tbl_daftar_orangtua->read($rules)->result();
        foreach ($tblDOrangTua as $a){
            try{
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'nik_passport' => $a->nik_passport
                    ), //not null or null
                    'or_where'  => null, //not null or null
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $tblDUsers = $this->Tbl_daftar_users->where($rules)->row();
                $rules = array(
                    'where' => array(
                        'nik_passport' => $a->nik_passport
                    ), //not null
                    'data'  => array(
                        'id_daftar_users' => $tblDUsers->id_daftar_users
                    ), //not null
                );
                $this->Tbl_daftar_orangtua->update($rules);
            }catch (Exception $e){
                echo $e->getMessage();
            }
        }
    }
    function UpdateUKT(){
        $rules = array(
            'select'    => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $tblDUKT = $this->Tbl_daftar_ukt->read($rules)->result();
        foreach ($tblDUKT as $a){
            try{
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'nik_passport' => $a->nik_passport
                    ), //not null or null
                    'or_where'  => null, //not null or null
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $tblDUsers = $this->Tbl_daftar_users->where($rules)->row();
                $rules = array(
                    'where' => array(
                        'nik_passport' => $a->nik_passport
                    ), //not null
                    'data'  => array(
                        'id_daftar_users' => $tblDUsers->id_daftar_users
                    ), //not null
                );
                $this->Tbl_daftar_ukt->update($rules);
            }catch (Exception $e){
                echo $e->getMessage();
            }
        }
    }

    /* Histori */
    function tbl_histori_bobot_nilai_ukt($url){
        $controller = 'Migration2018/tbl_histori_bobot_nilai_ukt/'.$this->token;
        $response 	= file_get_contents($url.$controller);
        $json 		= json_decode($response);
        foreach ($json->data as $a){
            try{
                $data = array(
                    'nama_field' => $a->nama_field,
                    'alias' => $a->alias,
                    'bobot' => $a->bobot,
                    'nilai_max' => $a->nilai_max,
                    'jalur' => $a->jalur,
                    'tahun' => $a->tahun,
                    'created_by' => $a->create_by,
                    'updated_by' => $a->update_by,
                    'date_created' => $a->date_create,
                    'date_updated' => $a->date_update,
                );
                $this->Tbl_histori_bobot_nilai_ukt->create($data);
            }catch (Exception $e){
                echo $e->getMessage();
            }
        }
    }
    function tbl_histori_bobot_range_ukt($url){
        $controller = 'Migration2018/tbl_histori_bobot_range_ukt/'.$this->token;
        $response 	= file_get_contents($url.$controller);
        $json 		= json_decode($response);
        foreach ($json->data as $a){
            try{
                $data = array(
                    'kategori' => $a->kategori,
                    'nilai_min' => $a->nilai_min,
                    'nilai_max' => $a->nilai_max,
                    'jalur' => $a->jalur,
                    'tahun' => $a->tahun,
                    'created_by' => $a->create_by,
                    'updated_by' => $a->update_by,
                    'date_created' => $a->date_create,
                    'date_updated' => $a->date_update,
                );
                $this->Tbl_histori_bobot_range_ukt->create($data);
            }catch (Exception $e){
                echo $e->getMessage();
            }
        }
    }

    /* Internasional */
    function tbl_internasional_biodata(){}
    function tbl_internasional_kelulusan(){}
    function tbl_internasional_pilihan(){}
    function tbl_internasional_users(){}

    /* Penyesuaian */
    function tbl_penyesuaian_extra(){}
    function tbl_penyesuaian_gambar(){}
    function tbl_penyesuaian_pddikti(){}
    function tbl_penyesuaian_ukt(){}
    function tbl_penyesuaian_users(){}

    /* Pradaftar */
    function tbl_pradaftar_biodata($url){
        $controller = 'Migration2018/tbl_pradaftar_biodata/'.$this->token;
        $response 	= file_get_contents($url.$controller);
        $json 		= json_decode($response);
        foreach ($json->data as $a){
            try{
                $data = array(
                    'id_pradaftar_users' => 1,
                    'nik_passport' => $a->nik_passport,
                    'tempat' => $a->tempat,
                    'jenis_kelamin' => $a->jenis_kelamin,
                    'warga_negara' => $a->warga_negara,
                    'id_agama' => $a->id_agama,
                    'id_kelurahan' => $a->id_kelurahan,
                    'kodepos' => $a->kodepos,
                    'alamat' => (empty($a->alamat))? null : $a->alamat,
                    'id_jenis_asal_sekolah' => 1,
                    'asal_sekolah' => $a->asal_sekolah,
                    'id_rumpun' => $a->id_rumpun,
                    'foto' => $a->foto,
                    'date_created' => $a->date_create,
                    'date_updated' => $a->date_update,
                );
                $this->Tbl_pradaftar_biodata->create($data);
                $data = array(
                    'id_pradaftar_users' => 1,
                    'nik_passport' => $a->nik_passport,
                    'orangtua' => 'Ayah',
                    'nik_orangtua' => 0,
                    'nama_orangtua' => $a->nama_ayah,
                    'tgl_lhr_orangtua' => '1999-01-01',
                    'id_pendidikan' => $a->id_pendidikan_ayah,
                    'id_pekerjaan' => $a->id_pekerjaan_ayah,
                    'id_penghasilan' => $a->id_penghasilan_ayah,
                    'date_created' => $a->date_create,
                    'date_updated' => $a->date_update,
                );
                $this->Tbl_pradaftar_orangtua->create($data);
                $data = array(
                    'id_pradaftar_users' => 1,
                    'nik_passport' => $a->nik_passport,
                    'orangtua' => 'Ibu',
                    'nik_orangtua' => 0,
                    'nama_orangtua' => $a->nama_ibu,
                    'tgl_lhr_orangtua' => '1999-01-01',
                    'id_pendidikan' => $a->id_pendidikan_ibu,
                    'id_pekerjaan' => $a->id_pekerjaan_ibu,
                    'id_penghasilan' => $a->id_penghasilan_ibu,
                    'date_created' => $a->date_create,
                    'date_updated' => $a->date_update,
                );
                $this->Tbl_pradaftar_orangtua->create($data);
            }catch (Exception $e){
                echo $e->getMessage();
            }
        }
    }
    function tbl_pradaftar_kelulusan($url){
        $controller = 'Migration2018/tbl_pradaftar_kelulusan/'.$this->token;
        $response 	= file_get_contents($url.$controller);
        $json 		= json_decode($response);
        foreach ($json->data as $a){
            try{
                $data = array(
                    'nomor_peserta' => $a->nomor_peserta,
                    'kode_jurusan' => $a->kode_jurusan_kelulusan,
                    'nilai' => 0,
                    'status' => ($a->status_kelulusan == 'TIDAK LULUS')? '0' : '1',
                    'keterangan' => $a->keterangan,
                    'created_by' => 1,
                    'updated_by' => 1,
                    'date_created' => $a->date_create,
                    'date_updated' => $a->date_update,
                );
                $this->Tbl_pradaftar_kelulusan->create($data);
            }catch (Exception $e){
                echo $e->getMessage();
            }
        }
    }
    function tbl_pradaftar_pembayaran($url){
        $controller = 'Migration2018/tbl_pradaftar_pembayaran/'.$this->token;
        $response 	= file_get_contents($url.$controller);
        $json 		= json_decode($response);
        foreach ($json->data as $a){
            try{
                $data = array(
                    'kd_pembayaran' => '20181'.$a->kd_pembayaran,
                    'uang' => $a->uang,
                    'id_bank' => 1,
                    'bank' => $a->bank,
                    'status' => ($a->status == "BELUM VERIFIKASI")? '0' : '1',
                    'date_created' => $a->tanggal,
                    'date_updated' => $a->tanggal,
                );
                $this->Tbl_pradaftar_pembayaran->create($data);
            }catch (Exception $e){
                echo $e->getMessage();
            }
        }
    }
    function tbl_pradaftar_setting($url){
        $controller = 'Migration2018/tbl_pradaftar_setting/'.$this->token;
        $response 	= file_get_contents($url.$controller);
        $json 		= json_decode($response);
        foreach ($json->data as $a){
            try{
                $data = array(
                    'no' => $a->no,
                    'nama_setting' => $a->nama_setting,
                    'setting' => $a->setting,
                    'created_by' => $a->create_by,
                    'updated_by' => $a->update_by,
                    'date_created' => $a->date_create,
                    'date_updated' => $a->date_update,
                );
                $this->Tbl_pradaftar_setting->create($data);
            }catch (Exception $e){
                echo $e->getMessage();
            }
        }
    }
    function tbl_pradaftar_ujian($url){
        $controller = 'Migration2018/tbl_pradaftar_ujian/'.$this->token;
        $response 	= file_get_contents($url.$controller);
        $json 		= json_decode($response);
        foreach ($json->data as $a){
            try{
                $rules = array(
                    'nomor_peserta' => $a->nomor_peserta,
                    'pilihan' => '1',
                    'kode_jurusan' => $a->kode_jurusan_1,
                    'date_created' => $a->date_create,
                    'date_updated' => $a->date_update,
                );
                $this->Tbl_pradaftar_pilihan->create($rules);
                $rules = array(
                    'nomor_peserta' => $a->nomor_peserta,
                    'pilihan' => '2',
                    'kode_jurusan' => $a->kode_jurusan_2,
                    'date_created' => $a->date_create,
                    'date_updated' => $a->date_update,
                );
                $this->Tbl_pradaftar_pilihan->create($rules);
                $rules = array(
                    'nomor_peserta' => $a->nomor_peserta,
                    'pilihan' => '3',
                    'kode_jurusan' => $a->kode_jurusan_3,
                    'date_created' => $a->date_create,
                    'date_updated' => $a->date_update,
                );
                $this->Tbl_pradaftar_pilihan->create($rules);
                $rules = array(
                    'nomor_peserta' => $a->nomor_peserta,
                    'id_jadwal' => $a->id_jadwal,
                    'date_created' => $a->date_create,
                    'date_updated' => $a->date_update,
                );
                $this->Tbl_pradaftar_jadwal->create($rules);
                $rules = array(
                    'where' => array(
                        'nomor_peserta' => $a->nomor_peserta,
                    ),
                    'data'  => array(
                        'nilai' => $a->nilai,
                    ),
                );
                $this->Tbl_pradaftar_kelulusan->update($rules);
            }catch (Exception $e){
                echo $e->getMessage();
            }
        }
    }
    function tbl_pradaftar_users($url){
        $controller = 'Migration2018/tbl_pradaftar_users/'.$this->token;
        $response 	= file_get_contents($url.$controller);
        $json 		= json_decode($response);
        foreach ($json->data as $a){
            try{
                $data = array(
                    'kd_pembayaran' => '20181'.$a->kd_pembayaran,
                    'nomor_peserta' => $a->nomor_peserta,
                    'nik_passport' => $a->nik_passport,
                    'nama' => $a->nama,
                    'tgl_lhr' => $a->tgl_lhr,
                    'kategori' => $a->kategori,
                    'id_tipe_ujian' => $a->id_tipe_ujian,
                    'nmr_tlp' => $a->nmr_tlp,
                    'email' => $a->email,
                    'password   ' => $a->password,
                    'ip_login' => $a->ip_login,
                    'login' => $a->login,
                    'ip_logout' => $a->ip_logout,
                    'logout' => $a->logout,
                    'pembayaran' => ($a->verifikasi_pembayaran == 'BELUM BAYAR')? '0' : '1',
                    'biodata' => ($a->verifikasi_biodata == 'BELUM VERIFIKASI')? '0' : '1',
                    'date_created' => $a->date_create,
                    'date_updated' => $a->date_update,
                );
                $this->Tbl_pradaftar_users->create($data);
            }catch (Exception $e){
                echo $e->getMessage();
            }
        }
    }
    function update_pembayaran($url){
        $controller = 'Migration2018/tbl_pradaftar_pembayaran/'.$this->token;
        $response 	= file_get_contents($url.$controller);
        $json 		= json_decode($response);
        foreach ($json->data as $a){
            try{
                $rules = array(
                    'where' => array(
                        'kd_pembayaran' => '20181'.$a->kd_pembayaran,
                    ),
                    'data'  => array(
                        'status' => ($a->status == "BELUM VERIFIKASI")? '0' : '1',
                    ),
                );
                $this->Tbl_pradaftar_pembayaran->update($rules);
            }catch (Exception $e){
                echo $e->getMessage();
            }
        }
    }
    function update_biodata(){
        try{
            $rules = array(
                'select'    => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $data = $this->Tbl_pradaftar_biodata->read($rules)->result();
            foreach ($data as $a){
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'nik_passport' => $a->nik_passport
                    ),
                    'or_where'  => null, //not null or null
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $tblPUsers = $this->Tbl_pradaftar_users->where($rules)->row();
                $rules = array(
                    'where' => array(
                        'nik_passport' => $a->nik_passport
                    ),
                    'data'  => array(
                        'id_pradaftar_users' => $tblPUsers->id_pradaftar_users,
                    ),
                );
                $this->Tbl_pradaftar_biodata->update($rules);
            }
        }catch (Exception $e){
            echo $e->getMessage();
        }
    }
    function update_nilai($url){
        try{
            $controller = 'Migration2018/tbl_pradaftar_ujian/'.$this->token;
            $response 	= file_get_contents($url.$controller);
            $json 		= json_decode($response);
            foreach ($json->data as $a){
                $rules = array(
                    'where' => array(
                        'nomor_peserta' => $a->nomor_peserta,
                    ),
                    'data'  => array(
                        'nilai' => $a->nilai,
                    ),
                );
                $this->Tbl_pradaftar_kelulusan->update($rules);
            }
        }catch (Exception $e){
            echo $e->getMessage();
        }
    }
    function update_orangtua(){
        try{
            $rules = array(
                'select'    => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $data = $this->Tbl_pradaftar_orangtua->read($rules)->result();
            foreach ($data as $a){
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'nik_passport' => $a->nik_passport
                    ),
                    'or_where'  => null, //not null or null
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $tblPUsers = $this->Tbl_pradaftar_users->where($rules)->row();
                $rules = array(
                    'where' => array(
                        'nik_passport' => $a->nik_passport
                    ),
                    'data'  => array(
                        'id_pradaftar_users' => $tblPUsers->id_pradaftar_users,
                    ),
                );
                $this->Tbl_pradaftar_orangtua->update($rules);
            }
        }catch (Exception $e){
            echo $e->getMessage();
        }
    }

    /* Temporary Pradaftar Users */
    function tmp_pradaftar_users($url){
        $controller = 'Migration2018/tmp_pradaftar_users/'.$this->token;
        $response 	= file_get_contents($url.$controller);
        $json 		= json_decode($response);
        foreach ($json->data as $a){
            try{
                $data = array(
                    'kd_pembayaran' => '20181'.$a->kd_pembayaran,
                    'nik_passport' => $a->nik_passport,
                    'nomor_peserta' => $a->nomor_peserta,
                    'nama' => $a->nama,
                    'tgl_lhr' => $a->tgl_lhr,
                    'kategori' => $a->kategori,
                    'id_tipe_ujian' => $a->id_tipe_ujian,
                    'nmr_tlp' => $a->nmr_tlp,
                    'email' => $a->email,
                    'password   ' => $a->password,
                    'ip_login' => $a->ip_login,
                    'login' => $a->login,
                    'ip_logout' => $a->ip_logout,
                    'logout' => $a->logout,
                    'pembayaran' => ($a->verifikasi_pembayaran == 'BELUM BAYAR')? '0' : '1',
                    'biodata' => ($a->verifikasi_biodata == 'BELUM VERIFIKASI')? '0' : '1',
                    'tgl_daftar' => $a->tgl_daftar,
                    'date_created' => $a->date_create,
                    'date_updated' => $a->date_update,
                );
                $this->Tmp_pradaftar_users->create($data);
            }catch (Exception $e){
                echo $e->getMessage();
            }
        }
    }
}