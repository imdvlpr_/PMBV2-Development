<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class TipeUjian extends CI_Controller {

	function __construct(){
        parent::__construct();
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != 'DEVELOPMENT') {
            $this->session->set_flashdata('message','Hak akses ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        $this->load->model('Settings/Tbl_setting_tipe_ujian');
    }

	function index(){
        $rules = array(
            'select'    => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
		$data = array(
            'content'       => 'Settings/tipe_ujian/content',
            'css'           => 'Settings/tipe_ujian/css',
            'javascript'    => 'Settings/tipe_ujian/javascript',
            'modal'         => 'Settings/tipe_ujian/modal',
			'tblSTipeUjian' => $this->Tbl_setting_tipe_ujian->read($rules)->result(),
		);
		$this->load->view('index',$data);
	}

	function Create(){
	    $rules[] = array('field' => 'tipe_ujian', 'label' => 'Tipe Ujian', 'rules' => 'required');
		$rules[] = array('field' => 'kode', 'label' => 'Kode', 'rules' => 'required');
	    $rules[] = array('field' => 'quota', 'label' => 'Quota', 'rules' => 'required');
	    $rules[] = array('field' => 'status', 'label' => 'Status', 'rules' => 'required');
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('message',validation_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('Settings/TipeUjian/');
		}else{
		    try{
                $data = array(
                    'tipe_ujian' 	=> strtoupper($this->input->post('tipe_ujian')),
                    'kode' 	   		=> $this->input->post('kode'),
                    'quota' 	    => $this->input->post('quota'),
                    'status' 	    => $this->input->post('status'),
                    'created_by'    => $this->session->userdata('id_users'),
                    'updated_by'    => $this->session->userdata('id_users'),
                );
                $this->Tbl_setting_tipe_ujian->create($data);
                $this->session->set_flashdata('message','Data berhasil disimpan.');
                $this->session->set_flashdata('type_message','success');
                redirect('Settings/TipeUjian/');
            }catch (Exception $e){
                $this->session->set_flashdata('message', $e->getMessage());
                $this->session->set_flashdata('type_message','danger');
                redirect('Settings/TipeUjian/');
            }
		}
	}

	function Update($id){
        $rules[] = array('field' => 'tipe_ujian', 'label' => 'Tipe Ujian', 'rules' => 'required');
		$rules[] = array('field' => 'kode', 'label' => 'Kode', 'rules' => 'required');
        $rules[] = array('field' => 'quota', 'label' => 'Quota', 'rules' => 'required');
        $rules[] = array('field' => 'status', 'label' => 'Status', 'rules' => 'required');
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('message',validation_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('Settings/TipeUjian/');
		}else{
		    try{
                $rules = array(
                    'where' => array('id_tipe_ujian' => $id),
                    'data'  => array(
                        'tipe_ujian' 	=> strtoupper($this->input->post('tipe_ujian')),
                        'kode' 	   		=> $this->input->post('kode'),
                        'quota'     	=> $this->input->post('quota'),
                        'status'     	=> $this->input->post('status'),
                        'updated_by'    => $this->session->userdata('id_users'),
                    ),
                );
                $this->Tbl_setting_tipe_ujian->update($rules);
                $this->session->set_flashdata('message','Data berhasil diubah.');
                $this->session->set_flashdata('type_message','success');
                redirect('Settings/TipeUjian/');

            }catch (Exception $e){
                $this->session->set_flashdata('message', $e->getMessage());
                $this->session->set_flashdata('type_message','danger');
                redirect('Settings/TipeUjian/');
            }
		}
	}

	function Delete($id){
	    try{
            $rules = array('id_tipe_ujian' => $id);
            $this->Tbl_setting_tipe_ujian->delete($rules);
            $this->session->set_flashdata('message','Data berhasil dihapus.');
            $this->session->set_flashdata('type_message','success');
            redirect('Settings/TipeUjian/');
        }catch (Exception $e){
            $this->session->set_flashdata('message', $e->getMessage());
            $this->session->set_flashdata('type_message','danger');
            redirect('Settings/TipeUjian/');
        }
	}

}

