<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Gedung extends CI_Controller {

    function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != "DEVELOPMENT") {
            $this->session->set_flashdata('message','Hak Akses Ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Dashboard');
        }
        $this->load->model('Settings/Tbl_setting_kampus');
        $this->load->model('Settings/Tbl_setting_gedung');
        $this->load->model('Views/Settings/View_setting_gedung');
    }

    function index(){
        $rules[0] = array(
            'select'    => null,
            'where'     => array('status_kampus' => '1'),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $rules[1] = array(
            'select'    => null,
            'where'     => array('status' => '1'),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $data = array(
            'content'       => 'Settings/gedung/content',
            'css'           => 'Settings/gedung/css',
            'javascript'    => 'Settings/gedung/javascript',
            'modal'         => 'Settings/gedung/modal',
            'tblSKampus'    => $this->Tbl_setting_kampus->where($rules[1])->result(),
            'viewSGedung'   => $this->View_setting_gedung->where($rules[0])->result(),
        );
        $this->load->view('index',$data);
    }

    function Create(){
        $rules[] = array('field' => 'id_kampus', 'label' => 'Kampus', 'rules' => 'required');
        $rules[] = array('field' => 'gedung', 'label' => 'Gedung', 'rules' => 'required');
        $rules[] = array('field' => 'status', 'label' => 'Status', 'rules' => 'required');
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == FALSE){
            $this->session->set_flashdata('message',validation_errors());
            $this->session->set_flashdata('type_message','danger');
            redirect('Settings/Gedung/');
        }else{
            try{
                $data = array(
                    'id_kampus' => $this->input->post('id_kampus'),
                    'gedung' => strtoupper($this->input->post('gedung')),
                    'status' => $this->input->post('status'),
                    'created_by' => $this->session->userdata('id_users'),
                    'updated_by' => $this->session->userdata('id_users'),
                );
                $this->Tbl_setting_gedung->create($data);
                $this->session->set_flashdata('message','Data berhasil disimpan.');
                $this->session->set_flashdata('type_message','success');
                redirect('Settings/Gedung/');
            }catch (Exception $e){
                $this->session->set_flashdata('message', $e->getMessage());
                $this->session->set_flashdata('type_message','danger');
                redirect('Settings/Gedung/');
            }
        }
    }

    function Update($id){
        $rules[] = array('field' => 'id_kampus', 'label' => 'Kampus', 'rules' => 'required');
        $rules[] = array('field' => 'gedung', 'label' => 'Gedung', 'rules' => 'required');
        $rules[] = array('field' => 'status', 'label' => 'Status', 'rules' => 'required');
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == FALSE){
            $this->session->set_flashdata('message',validation_errors());
            $this->session->set_flashdata('type_message','danger');
            redirect('Settings/Gedung/');
        }else{
            try{
                $rules = array(
                    'where' => array('id_gedung' => $id),
                    'data'  => array(
                        'id_kampus' => $this->input->post('id_kampus'),
                        'gedung' => strtoupper($this->input->post('gedung')),
                        'status' => $this->input->post('status'),
                        'updated_by' => $this->session->userdata('id_users'),
                    ),
                );
                $this->Tbl_setting_gedung->update($rules);
                $this->session->set_flashdata('message','Data berhasil diubah.');
                $this->session->set_flashdata('type_message','success');
                redirect('Settings/Gedung/');
            }catch (Exception $e){
                $this->session->set_flashdata('message', $e->getMessage());
                $this->session->set_flashdata('type_message','danger');
                redirect('Settings/Gedung/');
            }
        }
    }

    function Delete($id){
        try{
            $rules = array('id_gedung' => $id);
            $this->Tbl_setting_gedung->delete($rules);
            $this->session->set_flashdata('message','Data berhasil dihapus.');
            $this->session->set_flashdata('type_message','success');
            redirect('Settings/Gedung/');
        }catch (Exception $e){
            $this->session->set_flashdata('message', $e->getMessage());
            $this->session->set_flashdata('type_message','danger');
            redirect('Settings/Gedung/');
        }
    }

    function Ruangan($id){
        try{
            $rules = array(
                'where' => array('id_gedung' => $id),
                'data'  => array(
                    'status' => $this->input->post('status'),
                    'updated_by' => $this->session->userdata('id_users'),
                ),
            );
            $this->Tbl_setting_ruangan->update($rules);
            return true;
        }catch (Exception $e){
            return $e->getMessage();
        }
    }

}
