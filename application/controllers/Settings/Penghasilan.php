<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Penghasilan extends CI_Controller {

	function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != 'DEVELOPMENT') {
            $this->session->set_flashdata('message','Hak akses ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        $this->load->model('Settings/Tbl_setting_penghasilan');
    }

	function index(){
        $rules = array(
            'select'    => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
		$data = array(
            'content'           => 'Settings/penghasilan/content',
            'css'               => 'Settings/penghasilan/css',
            'javascript'        => 'Settings/penghasilan/javascript',
            'modal'             => 'Settings/penghasilan/modal',
			'tblSPenghasilan'   => $this->Tbl_setting_penghasilan->read($rules)->result(),
		);
		$this->load->view('index',$data);
	}

	function Create(){
	    $rules[] = array('field' => 'penghasilan', 'label' => 'Penghasilan', 'rules' => 'required');
	    $rules[] = array('field' => 'nilai', 'label' => 'Nilai', 'rules' => 'required');
	    $rules[] = array('field' => 'status', 'label' => 'Status', 'rules' => 'required');
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('message',validation_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('Settings/Penghasilan/');
		}else{
		    try{
                $data = array(
                    'penghasilan'	=> strtoupper($this->input->post('penghasilan')),
                    'nilai'         => $this->input->post('nilai'),
                    'status'        => $this->input->post('status'),
                    'created_by'    => $this->session->userdata('id_users'),
                    'updated_by'    => $this->session->userdata('id_users'),
                );
                $this->Tbl_setting_penghasilan->create($data);
                $this->session->set_flashdata('message','Data berhasil disimpan.');
                $this->session->set_flashdata('type_message','success');
                redirect('Settings/Penghasilan/');
            }catch (Exception $e){
                $this->session->set_flashdata('message', $e->getMessage());
                $this->session->set_flashdata('type_message','danger');
                redirect('Settings/Penghasilan/');
            }
		}
	}

	function Update($id){
        $rules[] = array('field' => 'penghasilan', 'label' => 'Penghasilan', 'rules' => 'required');
        $rules[] = array('field' => 'nilai', 'label' => 'Nilai', 'rules' => 'required');
        $rules[] = array('field' => 'status', 'label' => 'Status', 'rules' => 'required');
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('message',validation_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('Settings/Penghasilan/');
		}else{
		    try{
                $rules = array(
                    'where' => array('id_penghasilan' => $id),
                    'data'  => array(
                        'penghasilan'	=> strtoupper($this->input->post('penghasilan')),
                        'nilai'         => $this->input->post('nilai'),
                        'status'        => $this->input->post('status'),
                        'updated_by'    => $this->session->userdata('id_users'),
                    ),
                );
                $this->Tbl_setting_penghasilan->update($rules);
                $this->session->set_flashdata('message','Data berhasil diubah.');
                $this->session->set_flashdata('type_message','success');
                redirect('Settings/Penghasilan/');
            }catch (Exception $e){
                $this->session->set_flashdata('message', $e->getMessage());
                $this->session->set_flashdata('type_message','danger');
                redirect('Settings/Penghasilan/');
            }
		}
	}

	function Delete($id){
	    try{
            $rules = array('id_penghasilan' => $id);
            $this->Tbl_setting_penghasilan->delete($rules);
            $this->session->set_flashdata('message','Data berhasil dihapus.');
            $this->session->set_flashdata('type_message','success');
            redirect('Settings/Penghasilan/');
        }catch (Exception $e){
            $this->session->set_flashdata('message', $e->getMessage());
            $this->session->set_flashdata('type_message','danger');
            redirect('Settings/Penghasilan');
        }
	}

}

