<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Provinsi extends CI_Controller {

	function __construct(){
        parent::__construct();
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != "DEVELOPMENT") {
            $this->session->set_flashdata('message','Hak Akses Ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Dashboard');
        }
        $this->load->model('Settings/Tbl_setting_jenis_daerah');
        $this->load->model('Settings/Tbl_setting_provinsi');
        $this->load->model('Views/Settings/View_setting_provinsi');
    }

	function index(){
        $rules = array(
            'select'    => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $data = array(
            'content'       => 'Settings/provinsi/content',
            'css'           => 'Settings/provinsi/css',
            'javascript'    => 'Settings/provinsi/javascript',
            'modal'         => 'Settings/provinsi/modal',
            'viewSProvinsi' => $this->View_setting_provinsi->read($rules)->result(),
        );
        $this->load->view('index',$data);
	}

	function Create(){
        $rules[] = array('field' => 'id_provinsi',	'label' => 'ID Provinsi', 'rules' => 'required');
        $rules[] = array('field' => 'provinsi',	'label' => 'Provinsi', 'rules' => 'required');
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('message',validation_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('Settings/Provinsi');
		}else{
		    try{
                $data = array(
                    'id_provinsi' => $this->input->post('id_provinsi'),
                    'provinsi' => $this->input->post('provinsi'),
                    'id_jenis_daerah' => 1,
                    'created_by' => $this->session->userdata('id_users'),
                    'updated_by' => $this->session->userdata('id_users'),
                );
                $this->Tbl_setting_provinsi->create($data);
                $this->session->set_flashdata('message','Data berhasil disimpan.');
                $this->session->set_flashdata('type_message','success');
                redirect('Settings/Provinsi');
            }catch (Exception $e){
                $this->session->set_flashdata('message', $e->getMessage());
                $this->session->set_flashdata('type_message','danger');
                redirect('Settings/Provinsi');
            }
		}
	}

	function Update($id){
        $rules[] = array('field' => 'id_provinsi',	'label' => 'ID Provinsi', 'rules' => 'required');
        $rules[] = array('field' => 'provinsi',	'label' => 'Provinsi', 'rules' => 'required');
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('message',validation_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('Settings/Provinsi/');
		}else{
		    try{
                $rules = array(
                    'where' => array('id_provinsi' => $id),
                    'data'  => array(
                        'id_provinsi' => $this->input->post('id_provinsi'),
                        'provinsi' => $this->input->post('provinsi'),
                        'id_jenis_daerah' => 1,
                        'updated_by' => $this->session->userdata('id_users'),
                    ),
                );
                $this->Tbl_setting_provinsi->update($rules);
                $this->session->set_flashdata('message','Data berhasil diubah.');
                $this->session->set_flashdata('type_message','success');
                redirect('Settings/Provinsi');
            }catch (Exception $e){
                $this->session->set_flashdata('message', $e->getMessage());
                $this->session->set_flashdata('type_message','danger');
                redirect('Settings/Provinsi');
            }
		}
	}

	function Delete($id){
        try{
            $rules = array('id_provinsi' => $id);
            $this->Tbl_setting_provinsi->delete($rules);
            $this->session->set_flashdata('message','Data berhasil dihapus.');
            $this->session->set_flashdata('type_message','success');
            redirect('Settings/Provinsi/');
        }catch (Exception $e){
            $this->session->set_flashdata('message', $e->getMessage());
            $this->session->set_flashdata('type_message','danger');
            redirect('Settings/Provinsi/');
        }
	}

}

