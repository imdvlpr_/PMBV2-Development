<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Bank extends CI_Controller {

    function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != "DEVELOPMENT") {
            $this->session->set_flashdata('message','Hak Akses Ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Dashboard');
        }
        $this->load->model('Settings/Tbl_setting_bank');
    }

	function index(){
        $rules = array(
            'select'    => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
		$data = array(
            'content'       => 'Settings/bank/content',
            'css'           => 'Settings/bank/css',
            'javascript'    => 'Settings/bank/javascript',
            'modal'         => 'Settings/bank/modal',
		    'tblSBank'     => $this->Tbl_setting_bank->read($rules)->result(),
		);
		$this->load->view('index',$data);
	}

	function Create(){
		$rules[] = array('field' => 'bank', 'label' => 'Bank', 'rules' => 'required');
        $rules[] = array('field' => 'status', 'label' => 'Status', 'rules' => 'required');
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('message',validation_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('Settings/Bank/');
		}else{
		    try{
                $data = array(
                    'bank'     => strtoupper($this->input->post('bank')),
                    'status'    => $this->input->post('status'),
                    'created_by' => $this->session->userdata('id_users'),
                    'updated_by' => $this->session->userdata('id_users'),
                );
                $this->Tbl_setting_bank->create($data);
                $this->session->set_flashdata('message','Data berhasil disimpan.');
            	$this->session->set_flashdata('type_message','success');
            	redirect('Settings/Bank/');
            }catch (Exception $e){
                $this->session->set_flashdata('message', $e->getMessage());
                $this->session->set_flashdata('type_message','danger');
                redirect('Settings/Bank/');
            }
		}
	}

	function Update($id){
        $rules[] = array('field' => 'bank', 'label' => 'Bank', 'rules' => 'required');
        $rules[] = array('field' => 'status', 'label' => 'Status', 'rules' => 'required');
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('message',validation_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('Settings/Bank/');
		}else{
		    try{
                $rules = array(
                    'where' => array('id_bank' => $id),
                    'data'  => array(
                        'bank'     => strtoupper($this->input->post('bank')),
                        'status'    => $this->input->post('status'),
                        'updated_by' => $this->session->userdata('id_users'),
                    ),
                );
                $this->Tbl_setting_bank->update($rules);
                $this->session->set_flashdata('message','Data berhasil diubah.');
                $this->session->set_flashdata('type_message','success');
                redirect('Settings/Bank/');
            }catch (Exception $e){
                $this->session->set_flashdata('message', $e->getMessage());
                $this->session->set_flashdata('type_message','danger');
                redirect('Settings/Bank/');
            }
		}
	}

	function Delete($id){
        try{
            $rules = array('id_bank' => $id);
            $this->Tbl_setting_bank->delete($rules);
            $this->session->set_flashdata('message','Data berhasil dihapus.');
            $this->session->set_flashdata('type_message','success');
            redirect('Settings/Bank/');
        }catch (Exception $e){
            $this->session->set_flashdata('message', $e->getMessage());
            $this->session->set_flashdata('type_message','danger');
            redirect('Settings/Bank/');
        }
	}

}

