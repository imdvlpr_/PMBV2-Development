<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Kecamatan extends CI_Controller {

    function __construct(){
        parent::__construct();
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != "DEVELOPMENT") {
            $this->session->set_flashdata('message','Hak Akses Ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Dashboard');
        }
        $this->load->model('ServerSide/SS_setting_kecamatan', '', TRUE);
        $this->load->model('Settings/Tbl_setting_provinsi');
        $this->load->model('Settings/Tbl_setting_kabupaten');
        $this->load->model('Settings/Tbl_setting_kecamatan');
        $this->load->model('Views/Settings/View_setting_kecamatan');
    }

    function index(){
        $data = array(
            'content'       => 'Settings/kecamatan/read/content',
            'css'           => 'Settings/kecamatan/read/css',
            'javascript'    => 'Settings/kecamatan/read/javascript',
            'modal'         => 'Settings/kecamatan/read/modal',
        );
        $this->load->view('index', $data);
	}

	function Tambah(){
        $rules = array(
            'select'    => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $data = array(
            'content'       => 'Settings/kecamatan/tambah/content',
            'css'           => 'Settings/kecamatan/tambah/css',
            'javascript'    => 'Settings/kecamatan/tambah/javascript',
            'modal'         => 'Settings/kecamatan/tambah/modal',
            'tblSProvinsi'	=> $this->Tbl_setting_provinsi->read($rules)->result(),
        );
        $this->load->view('index', $data);
    }

	function Create(){
	    $rules[] = array('field' => 'id_kabupaten',	'label' => 'Kabupaten', 'rules' => 'required');;
	    $rules[] = array('field' => 'kecamatan', 'label' => 'Kecamatan', 'rules' => 'required');
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('message',validation_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('Settings/Kecamatan/');
		}else{
		    try{
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'id_kabupaten' => $this->input->post('id_kabupaten')
                    ),
                    'or_where'  => null,
                    'order'     => 'id_kecamatan DESC',
                    'limit'     => 1,
                    'pagging'   => null,
                );
                $lastID = $this->Tbl_setting_kecamatan->where($rules)->row();
                $lastID = $lastID->id_kecamatan + 1;
                $data = array(
                    'id_kecamatan' => $lastID,
                    'id_kabupaten' => $this->input->post('id_kabupaten'),
                    'id_jenis_daerah' => 4,
                    'kecamatan'	=> strtoupper($this->input->post('kecamatan')),
                    'created_by' => $this->session->userdata('id_users'),
                    'updated_by' => $this->session->userdata('id_users'),
                );
                $this->Tbl_setting_kecamatan->create($data);
                $this->session->set_flashdata('message','Data berhasil disimpan.');
                $this->session->set_flashdata('type_message','success');
                redirect('Settings/Kecamatan/Tambah/');
            }catch (Exception $e){
                $this->session->set_flashdata('message', $e->getMessage());
                $this->session->set_flashdata('type_message','danger');
                redirect('Settings/Kecamatan/Tambah/');
            }
		}
	}

    function Edit($id){
        $rules[0] = array(
            'select'    => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $rules[1] = array(
            'select'    => null,
            'where'     => array(
                'id_kecamatan' => $id,
            ),
            'or_where'  => null,
            'order'     => 'id_kecamatan DESC',
            'limit'     => 1,
            'pagging'   => null,
        );
        $data = array(
            'content'       => 'Settings/kecamatan/edit/content',
            'css'           => 'Settings/kecamatan/edit/css',
            'javascript'    => 'Settings/kecamatan/edit/javascript',
            'modal'         => 'Settings/kecamatan/edit/modal',
            'tblSProvinsi'	=> $this->Tbl_setting_provinsi->read($rules[0])->result(),
            'viewSKecamatan'=> $this->View_setting_kecamatan->where($rules[1])->row(),
        );
        $this->load->view('index', $data);
    }

	function Update($id){
        $id_kabupaten = $this->input->post('id_kabupaten');
        if (!empty($id_kabupaten)){
            $rules[] = array('field' => 'id_kabupaten',	'label' => 'Kabupaten', 'rules' => 'required');
        }else{
            $rules[] = array('field' => 'old_kabupaten',	'label' => 'Old Kabupaten', 'rules' => 'required');
            $id_kabupaten = $this->input->post('old_kabupaten');
        }
        $rules[] = array('field' => 'id_kecamatan',	'label' => 'ID Kecamatan', 'rules' => 'required');
        $rules[] = array('field' => 'kecamatan', 'label' => 'Kecamatan', 'rules' => 'required');
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('message',validation_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('Settings/Kecamatan/');
		}else{
		    try{
                $rules = array(
                    'where' => array('id_kecamatan' => $id),
                    'data'  => array(
                        'id_kecamatan' => $this->input->post('id_kecamatan'),
                        'id_kabupaten' => $id_kabupaten,
                        'kecamatan'	=> strtoupper($this->input->post('kecamatan')),
                        'updated_by' => $this->session->userdata('id_users'),
                    ),
                );
                $this->Tbl_setting_kecamatan->update($rules);
                $this->session->set_flashdata('message','Data berhasil diubah.');
                $this->session->set_flashdata('type_message','success');
                redirect('Settings/Kecamatan/Edit/'.$rules['data']['id_kecamatan']);
            }catch (Exception $e){
                $this->session->set_flashdata('message', $e->getMessage());
                $this->session->set_flashdata('type_message','danger');
                redirect('Settings/Kecamatan/Edit/'.$rules['data']['id_kecamatan']);
            }
		}
	}

	function Delete($id){
        try{
            $rules = array('id_kecamatan' => $id);
            $this->Tbl_setting_kecamatan->delete($rules);
            $this->session->set_flashdata('message','Data berhasil dihapus.');
            $this->session->set_flashdata('type_message','success');
            redirect('Settings/Kecamatan/');
        }catch (Exception $e){
            $this->session->set_flashdata('message', $e->getMessage());
            $this->session->set_flashdata('type_message','danger');
            redirect('Settings/Kecamatan/');
        }
	}

    function Json(){
        $fetch_data = $this->SS_setting_kecamatan->make_datatables();
        $data = array();
        $no = 1;
        foreach($fetch_data as $row) {
            $sub_array = array();
            $sub_array[] = $no++;
            $sub_array[] = "
            <div class=\"btn-group\">
                <button type=\"button\" class=\"btn btn-default dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                    <i class=\"fa fa-gear\"></i> <span class=\"caret\"></span>
                </button>
                <ul class=\"dropdown-menu\">
                    <li><a href=\"".base_url('Settings/Kecamatan/Edit/'.$row->id_kecamatan)."\" target='_blank'>Edit</a></li>
                    <li role=\"separator\" class=\"divider\"></li>
                    <li><a href=\"".base_url('Settings/Kecamatan/Delete/'.$row->id_kecamatan)."\" onclick=\"return confirm('Apakah Anda Yakin ?')\">Delete</i></a></li>
                </ul>
            </div>";
            $sub_array[] = $row->provinsi;
            $sub_array[] = $row->id_kabupaten;
            $sub_array[] = $row->kabupaten;
            $sub_array[] = $row->id_kecamatan;
            $sub_array[] = $row->kecamatan;
            $sub_array[] = $row->jenis_daerah;
            $data[] = $sub_array;
        }
        $output = array(
            "draw"				=>	intval($_POST["draw"]),
            "recordsTotal"		=>	$this->SS_setting_kecamatan->get_all_data(),
            "recordsFiltered"	=>	$this->SS_setting_kecamatan->get_filtered_data(),
            "data"				=>	$data
        );
        echo json_encode($output);
    }

    function listKabupaten(){
        $rules = array(
            'select'    => null,
            'where'     => array(
                'id_provinsi' => $this->input->post('id_provinsi')
            ),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $kabupaten = $this->Tbl_setting_kabupaten->where($rules)->result();
        $lists = null;
        foreach($kabupaten as $data){
            $lists .= "<option value='".$data->id_kabupaten."'>".$data->kabupaten."</option>";
        }
        $callback = array('list_kabupaten' => $lists);
        echo json_encode($callback);
    }

}