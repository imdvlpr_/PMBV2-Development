<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class JenisDaerah extends CI_Controller {

    function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != "DEVELOPMENT") {
            $this->session->set_flashdata('message','Hak Akses Ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Dashboard');
        }
        $this->load->model('Settings/Tbl_setting_jenis_daerah');
    }

    function index(){
        $rules = array(
            'select'    => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $data = array(
            'content'       => 'Settings/jenis_daerah/content',
            'css'           => 'Settings/jenis_daerah/css',
            'javascript'    => 'Settings/jenis_daerah/javascript',
            'modal'         => 'Settings/jenis_daerah/modal',
            'tblSJDaerah'   => $this->Tbl_setting_jenis_daerah->read($rules)->result(),
        );
        $this->load->view('index',$data);
    }

    function Create(){
        $rules[] = array('field' => 'jenis_daerah',	'label' => 'Jenis Daerah', 'rules' => 'required');
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == FALSE){
            $this->session->set_flashdata('message',validation_errors());
            $this->session->set_flashdata('type_message','danger');
            redirect('Settings/JenisDaerah/');
        }else{
            try{
                $data = array(
                    'jenis_daerah'  => strtoupper($this->input->post('jenis_daerah')),
                    'created_by'    => $this->session->userdata('id_users'),
                    'updated_by'    => $this->session->userdata('id_users'),
                );
                $this->Tbl_setting_jenis_daerah->create($data);
                $this->session->set_flashdata('message','Data berhasil disimpan.');
                $this->session->set_flashdata('type_message','success');
                redirect('Settings/JenisDaerah/');
            }catch (Exception $e){
                $this->session->set_flashdata('message', $e->getMessage());
                $this->session->set_flashdata('type_message','danger');
                redirect('Settings/JenisDaerah/');
            }
        }
    }

    function Update($id){
        $rules[] = array('field' => 'jenis_daerah',	'label' => 'Nama Daerah', 'rules' => 'required');
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == FALSE){
            $this->session->set_flashdata('message',validation_errors());
            $this->session->set_flashdata('type_message','danger');
            redirect('Settings/JenisDaerah/');
        }else{
            try{
                $rules = array(
                    'where' => array('id_jenis_daerah' => $id),
                    'data'  => array(
                        'jenis_daerah'  => strtoupper($this->input->post('jenis_daerah')),
                        'updated_by'    => $this->session->userdata('id_users'),
                    ),
                );
                $this->Tbl_setting_jenis_daerah->update($rules);
                $this->session->set_flashdata('message','Data berhasil diubah.');
                $this->session->set_flashdata('type_message','success');
                redirect('Settings/JenisDaerah/');
            }catch (Exception $e){
                $this->session->set_flashdata('message', $e->getMessage());
                $this->session->set_flashdata('type_message','danger');
                redirect('Settings/JenisDaerah/');
            }
        }
    }

    function Delete($id){
        try{
            $rules = array('id_jenis_daerah' => $id);
            $this->Tbl_setting_jenis_daerah->delete($rules);
            $this->session->set_flashdata('message','Data berhasil dihapus.');
            $this->session->set_flashdata('type_message','success');
            redirect('Settings/JenisDaerah/');
        }catch (Exception $e){
            $this->session->set_flashdata('message', $e->getMessage());
            $this->session->set_flashdata('type_message','danger');
            redirect('Settings/JenisDaerah/');
        }
    }

}
