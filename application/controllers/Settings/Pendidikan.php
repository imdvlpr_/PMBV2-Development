<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Pendidikan extends CI_Controller {

	function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != 'DEVELOPMENT') {
            $this->session->set_flashdata('message','Hak akses ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        $this->load->model('Settings/Tbl_setting_pendidikan');
    }

	function index(){
        $rules = array(
            'select'    => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $data = array(
            'content'           => 'Settings/pendidikan/content',
            'css'               => 'Settings/pendidikan/css',
            'javascript'        => 'Settings/pendidikan/javascript',
            'modal'             => 'Settings/pendidikan/modal',
            'tblSPendidikan'    => $this->Tbl_setting_pendidikan->read($rules)->result(),
        );
        $this->load->view('index',$data);
	}

	function Create(){
	    $rules[] = array('field' => 'pendidikan', 'label' => 'Pendidikan', 'rules' => 'required');
	    $rules[] = array('field' => 'status', 'label' => 'Status', 'rules' => 'required');
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('message',validation_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('Settings/Pendidikan/');
		}else{
		    try{
                $data = array(
                    'pendidikan'	=> strtoupper($this->input->post('pendidikan')),
                    'status'        => $this->input->post('status'),
                    'created_by'    => $this->session->userdata('id_users'),
                    'updated_by'    => $this->session->userdata('id_users'),
                );
                $this->Tbl_setting_pendidikan->create($data);
                $this->session->set_flashdata('message','Data berhasil disimpan.');
                $this->session->set_flashdata('type_message','success');
                redirect('Settings/Pendidikan/');
            }catch (Exception $e){
                $this->session->set_flashdata('message', $e->getMessage());
                $this->session->set_flashdata('type_message','danger');
                redirect('Settings/Pendidikan/');
            }
		}
	}

	function Update($id){
        $rules[] = array('field' => 'pendidikan', 'label' => 'Pendidikan', 'rules' => 'required');
        $rules[] = array('field' => 'status', 'label' => 'Status', 'rules' => 'required');
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('message',validation_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('Settings/Pendidikan/');
		}else{
		    try{
                $rules = array(
                    'where' => array('id_pendidikan' => $id),
                    'data'  => array(
                        'pendidikan'	=> strtoupper($this->input->post('pendidikan')),
                        'status'        => $this->input->post('status'),
                        'updated_by'    => $this->session->userdata('id_users'),
                    ),
                );
                $this->Tbl_setting_pendidikan->update($rules);
                $this->session->set_flashdata('message','Data berhasil diubah.');
                $this->session->set_flashdata('type_message','success');
                redirect('Settings/Pendidikan/');
            }catch (Exception $e){
                $this->session->set_flashdata('message', $e->getMessage());
                $this->session->set_flashdata('type_message','danger');
                redirect('Settings/Pendidikan/');
            }
		}
	}

	function Delete($id){
	    try{
	        $rules = array('id_pendidikan' => $id);
            $this->Tbl_setting_pendidikan->delete($rules);
            $this->session->set_flashdata('message','Data berhasil dihapus.');
            $this->session->set_flashdata('type_message','success');
            redirect('Settings/Pendidikan/');
        }catch (Exception $e){
            $this->session->set_flashdata('message', $e->getMessage());
            $this->session->set_flashdata('type_message','danger');
            redirect('Settings/Pendidikan/');
        }
	}

}

