<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Jadwal extends CI_Controller {

	function __construct(){
        parent::__construct();
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != 'DEVELOPMENT') {
            $this->session->set_flashdata('message','Hak akses ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        $this->load->model('ServerSide/SS_setting_jadwal', '', TRUE);
        $this->load->model('Settings/Tbl_setting_jadwal');
        $this->load->model('Settings/Tbl_setting_kampus');
        $this->load->model('Settings/Tbl_setting_gedung');
        $this->load->model('Settings/Tbl_setting_ruangan');
        $this->load->model('Settings/Tbl_setting_tipe_ujian');
        $this->load->model('Views/Pradaftar/View_pradaftar_biodata');
        $this->load->model('Views/Pradaftar/View_pradaftar_jadwal');
        $this->load->model('Views/Pradaftar/View_pradaftar_pilihan');
        $this->load->model('Views/Pradaftar/View_pradaftar_users');
        $this->load->model('Views/Settings/View_setting_jadwal');
    }

	function index(){
		$data = array(
            'content'       => 'Settings/jadwal/read/content',
            'css'           => 'Settings/jadwal/read/css',
            'javascript'    => 'Settings/jadwal/read/javascript',
            'modal'         => 'Settings/jadwal/read/modal',
		);
		$this->load->view('index',$data);
	}

	function Detail($id){
        $rules[0] = array(
            'select'    => null,
            'where'     => array('id_jadwal' => $id),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $viewSJadwal = $this->View_setting_jadwal->where($rules[0])->row();
        $viewPJadwal = $this->View_pradaftar_jadwal->where($rules[0])->result();
        $result = null;
        foreach ($viewPJadwal as $a){
            $rules = array(
                'select'    => null,
                'where'     => array('nomor_peserta' => $a->nomor_peserta),
                'or_where'  => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $users = $this->View_pradaftar_users->where($rules)->row();
            $pilihan = $this->View_pradaftar_pilihan->where($rules)->result();
            $rules = array(
                'select'    => null,
                'where'     => array('id_pradaftar_users' => $users->id_pradaftar_users),
                'or_where'  => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $biodata = $this->View_pradaftar_biodata->where($rules)->row();
            $result[] = array(
                'pilihan'   => $pilihan,
                'users'     => $users,
                'biodata'   => $biodata
            );
        }
        $data = array(
            'content'       => 'Settings/jadwal/detail/content',
            'css'           => 'Settings/jadwal/detail/css',
            'javascript'    => 'Settings/jadwal/detail/javascript',
            'modal'         => 'Settings/jadwal/detail/modal',
            'viewSJadwal'   => $viewSJadwal,
            'data'          => $result,
        );
        $this->load->view('index',$data);
    }

    function Tambah(){
        $rules = array(
            'select'    => null,
            'where'     => array('status' => '1'),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $data = array(
            'content'       => 'Settings/jadwal/tambah/content',
            'css'           => 'Settings/jadwal/tambah/css',
            'javascript'    => 'Settings/jadwal/tambah/javascript',
            'modal'         => 'Settings/jadwal/tambah/modal',
            'tblSKampus'    => $this->Tbl_setting_kampus->where($rules)->result(),
            'tblSTipeUjian' => $this->Tbl_setting_tipe_ujian->where($rules)->result(),

        );
        $this->load->view('index',$data);
    }
	
	function Create(){
        $rules[] = array('field' => 'tanggal', 'label' => 'Tanggal', 'rules' => 'required');
        $rules[] = array('field' => 'sesi', 'label' => 'Sesi', 'rules' => 'required');
        $rules[] = array('field' => 'jam_awal', 'label' => 'Jam Awal', 'rules' => 'required');
        $rules[] = array('field' => 'jam_akhir', 'label' => 'Jam Akhir', 'rules' => 'required');
        $rules[] = array('field' => 'quota', 'label' => 'Quota', 'rules' => 'required');
        $rules[] = array('field' => 'status', 'label' => 'Quota', 'Status' => 'required');
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('message',validation_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('Settings/Jadwal/Tambah/');
		}else{
		    try{
                $rules = array(
                    'id_tipe_ujian' => $this->input->post('id_tipe_ujian'),
                    'tanggal'       => $this->input->post('tanggal'),
                    'id_ruangan'    => $this->input->post('id_ruangan'),
                    'sesi'          => $this->input->post('sesi'),
                    'jam_awal'      => $this->input->post('jam_awal'),
                    'jam_akhir'     => $this->input->post('jam_akhir'),
                    'quota'         => $this->input->post('quota'),
                    'status'        => $this->input->post('status'),
                    'created_by'    => $this->session->userdata('id_users'),
                    'updated_by'    => $this->session->userdata('id_users'),
                );
                $this->Tbl_setting_jadwal->create($rules);
                $this->session->set_flashdata('message','Data berhasil disimpan.');
                $this->session->set_flashdata('type_message','success');
                redirect('Settings/Jadwal/Tambah/');
            }catch (Exception $e){
                $this->session->set_flashdata('message', $e->getMessage());
                $this->session->set_flashdata('type_message','danger');
                redirect('Settings/Jadwal/Tambah/');
            }
		}
	}

    function Edit($id){
        $rules[0] = array(
            'select'    => null,
            'where'     => array('id_jadwal' => $id),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $rules[1] = array(
            'select'    => null,
            'where'     => array('status' => '1'),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $data = array(
            'content'       => 'Settings/jadwal/edit/content',
            'css'           => 'Settings/jadwal/edit/css',
            'javascript'    => 'Settings/jadwal/edit/javascript',
            'modal'         => 'Settings/jadwal/edit/modal',
            'viewSJadwal'   => $this->View_setting_jadwal->where($rules[0])->row(),
            'tblSKampus'    => $this->Tbl_setting_kampus->where($rules[1])->result(),
            'tblSTipeUjian' => $this->Tbl_setting_tipe_ujian->where($rules[1])->result(),
        );
        $this->load->view('index',$data);
    }

    function Update($id){
        $id_ruangan = $this->input->post('id_ruangan');
        if (!empty($id_ruangan)){
            $rules[] = array('field' => 'id_ruangan',	'label' => 'Ruangan', 'rules' => 'required');
        }else{
            $rules[] = array('field' => 'old_ruangan',	'label' => 'Old Ruangan', 'rules' => 'required');
            $id_ruangan = $this->input->post('old_ruangan');
        }
        $rules[] = array('field' => 'tanggal', 'label' => 'Tanggal', 'rules' => 'required');
        $rules[] = array('field' => 'sesi', 'label' => 'Sesi', 'rules' => 'required');
        $rules[] = array('field' => 'jam_awal', 'label' => 'Jam Awal', 'rules' => 'required');
        $rules[] = array('field' => 'jam_akhir', 'label' => 'Jam Akhir', 'rules' => 'required');
        $rules[] = array('field' => 'quota', 'label' => 'Quota', 'rules' => 'required');
        $rules[] = array('field' => 'status', 'label' => 'Quota', 'Status' => 'required');
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == FALSE){
            $this->session->set_flashdata('message',validation_errors());
            $this->session->set_flashdata('type_message','danger');
            redirect('Settings/Jadwal/Edit/'.$id);
        }else{
            try{
                $rules = array(
                    'where' => array('id_jadwal' => $id),
                    'data'  => array(
                        'id_tipe_ujian' => $this->input->post('id_tipe_ujian'),
                        'tanggal' => $this->input->post('tanggal'),
                        'id_ruangan' => $id_ruangan,
                        'sesi' => $this->input->post('sesi'),
                        'jam_awal' => $this->input->post('jam_awal'),
                        'jam_akhir' => $this->input->post('jam_akhir'),
                        'quota' => $this->input->post('quota'),
                        'status' => $this->input->post('status'),
                        'updated_by' => $this->session->userdata('id_users'),
                    ),
                );
                $this->Tbl_setting_jadwal->update($rules);
                $this->session->set_flashdata('message','Data berhasil diubah.');
                $this->session->set_flashdata('type_message','success');
                redirect('Settings/Jadwal/Edit/'.$id);
            }catch (Exception $e){
                $this->session->set_flashdata('message', $e->getMessage());
                $this->session->set_flashdata('type_message','danger');
                redirect('Settings/Jadwal/Edit/'.$id);
            }
        }
    }

	function Delete($id){
	    try{
            $rules = array('id_jadwal' => $id);
            $this->Tbl_setting_jadwal->delete($rules);
            $this->session->set_flashdata('message','Data berhasil dihapus.');
            $this->session->set_flashdata('type_message','success');
            redirect('Settings/Jadwal/');
        }catch (Exception $e){
            $this->session->set_flashdata('message', $e->getMessage());
            $this->session->set_flashdata('type_message','danger');
            redirect('Settings/Jadwal/');
        }
	}

    function Json(){
        $fetch_data = $this->SS_setting_jadwal->make_datatables();
        $data = array();
        foreach($fetch_data as $row){
            $sub_array = array();
            $sub_array[] = "
                <div class=\"btn-group\">
                    <button type=\"button\" class=\"btn btn-default dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                        <i class='fa fa-gear'></i> <span class=\"caret\"></span>
                    </button>
                    <ul class=\"dropdown-menu\">
                        <li><a href=\"".base_url('Settings/Jadwal/Detail/'.$row->id_jadwal)."\" target='_blank'>Detail</a></li>
                        <li><a href=\"".base_url('Settings/Jadwal/Edit/'.$row->id_jadwal)."\" target='_blank'>Edit</a></li>
                        <li role=\"separator\" class=\"divider\"></li>
                        <li><a href=\"".base_url('Settings/Jadwal/Delete/'.$row->id_jadwal)."\" onclick=\"return confirm('Apakah Anda Yakin ?')\">Delete</i></a></li>
                    </ul>
                </div>";
            $sub_array[] = $row->tipe_ujian;
            $sub_array[] = $row->tanggal;
            $sub_array[] = $row->kampus;
            $sub_array[] = $row->gedung;
            $sub_array[] = $row->ruangan;
            $sub_array[] = $row->sesi;
            $sub_array[] = $row->jam_awal;
            $sub_array[] = $row->jam_akhir;
            $sub_array[] = $row->quota;
            $sub_array[] = ($row->status == 1) ? "<span class=\"label label-success\">True</span>" : "<span class=\"label label-danger\">False</span>";
            $data[] = $sub_array;
        }
        $output = array(
            "draw"				=>	intval($_POST["draw"]),
            "recordsTotal"		=>	$this->SS_setting_jadwal->get_all_data(),
            "recordsFiltered"	=>	$this->SS_setting_jadwal->get_filtered_data(),
            "data"				=>	$data
        );
        echo json_encode($output);
    }

    function listTipeUjian(){
        $rules = array(
            'select'    => null,
            'where'     => array(
                'program' => $this->input->post('program'),
                'status' => '1'
            ),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $tblSTipeUjian = $this->Tbl_setting_tipe_ujian->where($rules)->result();
        $lists = null;
        foreach($tblSTipeUjian as $data){
            $lists .= "<option value='".$data->id_tipe_ujian."'>".$data->tipe_ujian."</option>";
        }
        $callback = array('list_tipe_ujian' => $lists);
        echo json_encode($callback);
    }

    function listGedung(){
        $rules = array(
            'select'    => null,
            'where'     => array(
                'id_kampus' => $this->input->post('id_kampus'),
                'status' => '1'
            ),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $tblSGedung = $this->Tbl_setting_gedung->where($rules)->result();
        $lists = "<option value=\"\">Pilih</option>";
        foreach($tblSGedung as $data){
            $lists .= "<option value='".$data->id_gedung."'>".$data->gedung."</option>";
        }
        $callback = array('list_gedung' => $lists);
        echo json_encode($callback);
    }

    function listRuangan(){
        $rules = array(
            'select'    => null,
            'where'     => array(
                'id_gedung' => $this->input->post('id_gedung'),
                'status' => '1'
            ),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $tblSRuangan = $this->Tbl_setting_ruangan->where($rules)->result();
        $lists = null;
        foreach($tblSRuangan as $data){
            $lists .= "<option value='".$data->id_ruangan."'>".$data->ruangan."</option>";
        }
        $callback = array('list_ruangan' => $lists);
        echo json_encode($callback);
    }

}

