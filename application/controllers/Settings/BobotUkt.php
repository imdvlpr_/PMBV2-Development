<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class BobotUkt extends CI_Controller {

    function __construct(){
        parent::__construct();
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != "DEVELOPMENT") {
            $this->session->set_flashdata('message','Hak Akses Ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Dashboard');
        }
        $this->load->model('Histori/Tbl_histori_bobot_nilai_ukt');
        $this->load->model('Settings/Tbl_setting_bobot_ukt');
        $this->load->model('Settings/Tbl_setting_jalur_masuk');
        $this->load->model('Views/Daftar/View_daftar_extra');
        $this->load->model('Views/Daftar/View_daftar_mahasiswa');
        $this->load->model('Views/Daftar/View_daftar_orangtua');
        $this->load->model('Views/Daftar/View_daftar_users');
    }

	function index(){
        $rules[0] = array(
            'select'    => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $rules[1] = array(
            'select'    => 'YEAR(date_created) as tahun',
            'where'     => null,
            'order'     => 'tahun DESC',
        );
		$data = array(
            'content'       => 'Settings/bobot_ukt/content',
            'css'           => 'Settings/bobot_ukt/css',
            'javascript'    => 'Settings/bobot_ukt/javascript',
            'modal'         => 'Settings/bobot_ukt/modal',
		    'tblSBobotUkt'  => $this->Tbl_setting_bobot_ukt->read($rules[0])->result(),
            'tblSJalurMasuk'=> $this->Tbl_setting_jalur_masuk->read($rules[0])->result(),
            'tahun'         => $this->View_daftar_users->distinct($rules[1])->result(),
		);
		$this->load->view('index',$data);
	}

	function Create(){
        $rules[] = array('field' => 'nama_field', 'label' => 'Nama Field', 'rules' => 'required');
        $rules[] = array('field' => 'alias', 'label' => 'Alias', 'rules' => 'required');
        $rules[] = array('field' => 'bobot', 'label' => 'Bobot', 'rules' => 'required');
        $rules[] = array('field' => 'nilai_max', 'label' => 'Nilai Maksimum', 'rules' => 'required');
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('message',validation_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('Settings/BobotUkt/');
		}else{
		    try{
                $data = array(
                    'nama_field'    => $this->input->post('nama_field'),
                    'alias'         => strtoupper($this->input->post('alias')),
                    'bobot'         => $this->input->post('bobot'),
                    'jalur'			=> 'SNMPTN',
                    'tahun'			=> date('Y'),
                    'nilai_max'     => $this->input->post('nilai_max'),
                    'created_by'    => $this->session->userdata('id_users'),
                    'updated_by'    => $this->session->userdata('id_users'),
                );
                $this->Tbl_setting_bobot_ukt->create($data);
                $this->session->set_flashdata('message','Data berhasil disimpan.');
                $this->session->set_flashdata('type_message','success');
                redirect('Settings/BobotUkt/');
            }catch (Exception $e){
                $this->session->set_flashdata('message', $e->getMessage());
                $this->session->set_flashdata('type_message','danger');
                redirect('Settings/BobotUkt/');
            }
		}
	}

	function Update($id){
        $rules[] = array('field' => 'nama_field', 'label' => 'Nama Field', 'rules' => 'required');
        $rules[] = array('field' => 'alias', 'label' => 'Alias', 'rules' => 'required');
        $rules[] = array('field' => 'bobot', 'label' => 'Bobot', 'rules' => 'required');
        $rules[] = array('field' => 'nilai_max', 'label' => 'Nilai Maksimum', 'rules' => 'required');
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('message',validation_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('Settings/BobotUkt/');
		}else{
		    try{
                $rules = array(
                    'where' => array('id_bobot_ukt' => $id),
                    'data'  => array(
                        'nama_field'    => $this->input->post('nama_field'),
                        'alias'         => strtoupper($this->input->post('alias')),
                        'bobot'         => $this->input->post('bobot'),
                        'nilai_max'     => $this->input->post('nilai_max'),
                        'updated_by'    => $this->session->userdata('id_users'),
                    ),
                );
                $this->Tbl_setting_bobot_ukt->update($rules);
                $this->session->set_flashdata('message','Data berhasil diubah.');
                $this->session->set_flashdata('type_message','success');
                redirect('Settings/BobotUkt/');
            }catch (Exception $e){
                $this->session->set_flashdata('message', $e->getMessage());
                $this->session->set_flashdata('type_message','danger');
                redirect('Settings/BobotUkt/');
            }
		}
	}

	function Delete($id){
        try{
            $rules = array('id_bobot_ukt' => $id);
            $this->Tbl_setting_bobot_ukt->delete($rules);
            $this->session->set_flashdata('message','Data berhasil dihapus.');
            $this->session->set_flashdata('type_message','success');
            redirect('Settings/BobotUkt/');
        }catch (Exception $e){
            $this->session->set_flashdata('message', $e->getMessage());
            $this->session->set_flashdata('type_message','danger');
            redirect('Settings/BobotUkt/');
        }
	}

	function Generate(){
        $rules[] = array('field' => 'jalur','label' => 'Jalur','rules' => 'required');
        $rules[] = array('field' => 'tahun','label' => 'Tahun','rules' => 'required');
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == FALSE){
            $this->session->set_flashdata('message',validation_errors());
            $this->session->set_flashdata('type_message','danger');
            redirect('Settings/BobotUkt/');
        }else{
            try{
                $jalur = $this->input->post('jalur');
                $tahun = $this->input->post('tahun');
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'id_bobot_ukt >=' => 1,
                        'id_bobot_ukt <=' => 6
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $nilai = $this->Tbl_setting_bobot_ukt->where($rules)->result();
                foreach ($nilai as $value){
                    try{
                        $data = explode('_',$value->nama_field);
                        $nama_field = $data[0].'_'.$data[1];
                        $rules = array(
                            'select'    => "max($nama_field) as nilai",
                            'where'     => array(
                                'orangtua' => strtoupper($data[2]),
                                'jalur_masuk' => $jalur,
                                'verifikasi' => 'SUDAH VERIFIKASI',
                                'YEAR(date_created)' => $tahun,
                            ),
                            'or_where'  => null,
                            'order'     => null,
                            'limit'     => null,
                            'pagging'   => null,
                        );
                        $generate = $this->View_daftar_orangtua->where($rules)->row();
                        $this->UpdateNilai($value->id_bobot_ukt,$jalur,$generate->nilai,$tahun);
                    }catch (Exception $e){
                        echo $e->getMessage();
                        exit;
                    }
                }
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'id_bobot_ukt >=' => 7,
                        'id_bobot_ukt <=' => 11
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $extra = $this->Tbl_setting_bobot_ukt->where($rules)->result();
                foreach ($extra as $value){
                    try{
                        $rules = array(
                            'select'    => "max($value->nama_field) as nilai",
                            'where'     => array(
                                'jalur_masuk' => $jalur,
                                'verifikasi' => 'SUDAH VERIFIKASI',
                                'YEAR(date_created)' => $tahun,
                            ),
                            'or_where'  => null,
                            'order'     => null,
                            'limit'     => null,
                            'pagging'   => null,
                        );
                        $generate = $this->View_daftar_extra->where($rules)->row();
                        $this->UpdateNilai($value->id_bobot_ukt,$jalur,$generate->nilai,$tahun);
                    }catch (Exception $e){
                        echo $e->getMessage();
                        exit;
                    }

                }
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'id_bobot_ukt' => 12,
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $alat_transportasi = $this->Tbl_setting_bobot_ukt->where($rules)->row();
                $rules = array(
                    'select'    => "max($alat_transportasi->nama_field) as nilai",
                    'where'     => array(
                        'jalur_masuk' => $jalur,
                        'verifikasi' => 'SUDAH VERIFIKASI',
                        'YEAR(date_created)' => $tahun,
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $generate = $this->View_daftar_mahasiswa->where($rules)->row();
                $this->UpdateNilai($alat_transportasi->id_bobot_ukt,$jalur,$generate->nilai,$tahun);
                $this->session->set_flashdata('message','Data berhasil diubah.');
                $this->session->set_flashdata('type_message','success');
                redirect('Settings/BobotUkt/');
            }catch (Exception $e){
                $this->session->set_flashdata('message', $e->getMessage());
                $this->session->set_flashdata('type_message','danger');
                redirect('Settings/BobotUkt/');
            }

        }
    }

    function UpdateNilai($id_bobot_ukt,$jalur,$nilai,$tahun){
        try{
            $nilai = ($nilai == null) ? 0 : $nilai;
            $rules = array(
                'where' => array('id_bobot_ukt' => $id_bobot_ukt),
                'data'  => array(
                    'jalur'     => $jalur,
                    'nilai_max' => $nilai,
                    'tahun'     => $tahun,
                ),
            );
            $this->Tbl_setting_bobot_ukt->update($rules);
            return true;
        }catch (Exception $e){
            return $e->getMessage();
        }
	}

    function Simpan(){
        try{
            $rules = array(
                'select'    => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $tblSBobotUkt = $this->Tbl_setting_bobot_ukt->read($rules)->result();
            $create = 0;
            $update = 0;
            $error = 0;
            foreach ($tblSBobotUkt as $value){
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'nama_field'    => $value->nama_field,
                        'jalur'         => $value->jalur,
                        'tahun'         => $value->tahun,
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $tblHBobotUkt = $this->Tbl_histori_bobot_nilai_ukt->where($rules);
                if ($tblHBobotUkt->num_rows() == 0){
                    $data = array(
                        'nama_field'    => $value->nama_field,
                        'alias'         => $value->alias,
                        'bobot'         => $value->bobot,
                        'nilai_max'     => $value->nilai_max,
                        'jalur'         => $value->jalur,
                        'tahun'         => $value->tahun,
                        'created_by'    => $this->session->userdata('id_users'),
                        'updated_by'    => $this->session->userdata('id_users'),
                    );
                    if ($this->Tbl_histori_bobot_nilai_ukt->create($data)){
                        $create++;
                    }else{
                        $error++;
                    }
                }else{
                    $tblHBobotUkt = $tblHBobotUkt->row();
                    $rules = array(
                        'where' => array('id_histori_bobot_nilai_ukt' => $tblHBobotUkt->id_histori_bobot_nilai_ukt),
                        'data'  => array(
                            'nama_field'    => $value->nama_field,
                            'alias'         => $value->alias,
                            'bobot'         => $value->bobot,
                            'nilai_max'     => $value->nilai_max,
                            'jalur'         => $value->jalur,
                            'tahun'         => $value->tahun,
                            'updated_by'     => $this->session->userdata('id_users'),
                        ),
                    );
                    if ($this->Tbl_histori_bobot_nilai_ukt->update($rules)){
                        $update++;
                    }else{
                        $error++;
                    }
                }
            }
            $this->session->set_flashdata('message',"Proses penyimpanan selesai.<br >Proses Create : $create <br />Proses Update : $update <br />Proses Error : $error");
            $this->session->set_flashdata('type_message','success');
            redirect('Settings/BobotUkt/');
        }catch (Exception $e){
            $this->session->set_flashdata('message', $e->getMessage());
            $this->session->set_flashdata('type_message','danger');
            redirect('Settings/BobotUkt/');
        }
    }

}

