<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Kelurahan extends CI_Controller {

	function __construct(){
        parent::__construct();
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != "DEVELOPMENT") {
            $this->session->set_flashdata('message','Hak Akses Ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Dashboard');
        }
        $this->load->model('Settings_model');
    }

	function index(){
		$tbSJDaerah = $this->Settings_model->readSJDaerah()->result();
		$tbSKEL = $this->Settings_model->readSKEL()->result();
		$tbSKEC = $this->Settings_model->readSKEC()->result();
		$data = array(
			'tbSJDaerah' => $tbSJDaerah,
			'tbSKEL' => $tbSKEL, 
			'tbSKEC' => $tbSKEC, 
		);
		$this->load->view('settings/setting_kelurahan',$data);
	}

	function Tambah(){
		$rules = [
			['field' => 'id_kec',	'label' => 'ID Kecamatan', 'rules' => 'required'],
			['field' => 'nama',	'label' => 'Nama', 'rules' => 'required'],
			['field' => 'id_jenis',	'label' => 'Jenis', 'rules' => 'required'],
		];
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('message',validation_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('Settings/Kelurahan');
		}else{
			$lastID = $this->Settings_model->lastIDKEL()->row();
			$lastID = $lastID->id_kel+1;
			$data = array(
				'id_kel' => $lastID,
				'id_kec' => set_value('id_kec'),
				'nama'	=> set_value('nama'), 
				'id_jenis' => set_value('id_jenis'),
			);
			if ($this->Settings_model->createSKEL($data)) {
				$this->session->set_flashdata('message','Data berhasil disimpan.');
            	$this->session->set_flashdata('type_message','success');
            	redirect('Settings/Kelurahan');
			}else{
				$this->session->set_flashdata('message','Terjadi kesalahan dalam tambah data kelurahan');
            	$this->session->set_flashdata('type_message','danger');
            	redirect('Settings/Kelurahan');
			}
		}
	}

	function Update($id){
		$rules = [
			['field' => 'id_kec',	'label' => 'ID Kecamatan', 'rules' => 'required'],
			['field' => 'nama',	'label' => 'Nama', 'rules' => 'required'],
			['field' => 'id_jenis',	'label' => 'Jenis', 'rules' => 'required'],
		];
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('message',validation_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('Settings/Kelurahan/');
		}else{
			$data = array(
				'id_kec' => set_value('id_kec'),
				'nama'	=> set_value('nama'), 
				'id_jenis' => set_value('id_jenis'),
			);
			if ($this->Settings_model->updateSKEL($id,$data)) {
				$this->session->set_flashdata('message','Data berhasil diubah.');
            	$this->session->set_flashdata('type_message','success');
            	redirect('Settings/Kelurahan');
			}else{
				$this->session->set_flashdata('message','Terjadi kesalahan dalam edit data kelurahan');
            	$this->session->set_flashdata('type_message','danger');
            	redirect('Settings/Kelurahan');
			}
		}
	}

	function Delete($id){
		if ($this->Settings_model->deleteSKEL($id)) {
			$this->session->set_flashdata('message','Data berhasil dihapus.');
            $this->session->set_flashdata('type_message','success');
            redirect('Settings/Kelurahan');
		}else{
			$this->session->set_flashdata('message','Terjadi kesalahan dalam hapus data kelurahan');
            $this->session->set_flashdata('type_message','danger');
            redirect('Settings/Kelurahan');
		}
	}
}