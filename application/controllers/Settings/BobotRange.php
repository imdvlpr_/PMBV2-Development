<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class BobotRange extends CI_Controller {

    function __construct(){
        parent::__construct();
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != "DEVELOPMENT") {
            $this->session->set_flashdata('message','Hak Akses Ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Dashboard');
        }
        $this->load->model('Histori/Tbl_histori_bobot_range_ukt');
        $this->load->model('Settings/Tbl_setting_bobot_range');
        $this->load->model('Settings/Tbl_setting_jalur_masuk');
        $this->load->model('Views/Daftar/View_daftar_users');
    }

	function index(){
        $rules[0] = array(
            'select'    => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $rules[1] = array(
            'select'    => 'YEAR(date_created) as tahun',
            'where'     => null,
            'order'     => 'tahun DESC',
        );
		$data = array(
            'content'       => 'Settings/bobot_range/content',
            'css'           => 'Settings/bobot_range/css',
            'javascript'    => 'Settings/bobot_range/javascript',
            'modal'         => 'Settings/bobot_range/modal',
		    'tblSBobotRange'=> $this->Tbl_setting_bobot_range->read($rules[0])->result(),
            'tblSJalurMasuk'=> $this->Tbl_setting_jalur_masuk->read($rules[0])->result(),
            'tahun'         => $this->View_daftar_users->distinct($rules[1])->result(),
		);
		$this->load->view('index',$data);
	}

	function Create(){
        $rules[] = array('field' => 'kategori', 'label' => 'Kategori', 'rules' => 'required');
        $rules[] = array('field' => 'nilai_min', 'label' => 'Nilai Minimum', 'rules' => 'required');
        $rules[] = array('field' => 'nilai_max', 'label' => 'Nilai Maksimal', 'rules' => 'required');
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('message',validation_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('Settings/BobotRange/');
		}else{
		    try{
                $data = array(
                    'kategori'  => $this->input->post('kategori'),
                    'nilai_min' => $this->input->post('nilai_min'),
                    'nilai_max' => $this->input->post('nilai_max'),
                    'jalur'     => 'SNMPTN',
                    'tahun'     => date('Y'),
                    'created_by'=> $this->session->userdata('id_users'),
                    'updated_by'=> $this->session->userdata('id_users'),
                );
                $this->Tbl_setting_bobot_range->create($data);
                $this->session->set_flashdata('message','Data berhasil disimpan.');
                $this->session->set_flashdata('type_message','success');
                redirect('Settings/BobotRange/');
            }catch (Exception $e){
                $this->session->set_flashdata('message', $e->getMessage());
                $this->session->set_flashdata('type_message','danger');
                redirect('Settings/BobotRange/');
            }
		}
	}

	function Update($id){
        $rules[] = array('field' => 'kategori', 'label' => 'Kategori', 'rules' => 'required');
        $rules[] = array('field' => 'nilai_min', 'label' => 'Nilai Minimum', 'rules' => 'required');
        $rules[] = array('field' => 'nilai_max', 'label' => 'Nilai Maksimal', 'rules' => 'required');
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('message',validation_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('Settings/BobotRange/');
		}else{
		    try{
                $rules = array(
                    'where' => array('id_bobot_range' => $id),
                    'data'  => array(
                        'kategori'  => $this->input->post('kategori'),
                        'nilai_min' => $this->input->post('nilai_min'),
                        'nilai_max' => $this->input->post('nilai_max'),
                        'updated_by'=> $this->session->userdata('id_users'),
                    ),
                );
                $this->Tbl_setting_bobot_range->update($rules);
                $this->session->set_flashdata('message','Data berhasil diubah.');
                $this->session->set_flashdata('type_message','success');
                redirect('Settings/BobotRange/');
            }catch (Exception $e){
                $this->session->set_flashdata('message', $e->getMessage());
                $this->session->set_flashdata('type_message','danger');
                redirect('Settings/BobotRange/');
            }
		}
	}

	function Delete($id){
        try{
            $rules = array('id_bobot_range' => $id);
            $this->Tbl_setting_bobot_range->delete($rules);
            $this->session->set_flashdata('message','Data berhasil dihapus.');
            $this->session->set_flashdata('type_message','success');
            redirect('Settings/BobotRange/');
        }catch (Exception $e){
            $this->session->set_flashdata('message', $e->getMessage());
            $this->session->set_flashdata('type_message','danger');
            redirect('Settings/BobotRange/');
        }
	}

	function Generate(){
        $rules[] = array('field' => 'jalur','label' => 'Jalur','rules' => 'required');
        $rules[] = array('field' => 'tahun','label' => 'Tahun','rules' => 'required');
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == FALSE){
            $this->session->set_flashdata('message',validation_errors());
            $this->session->set_flashdata('type_message','danger');
            redirect('Settings/BobotRange/');
        }else{
            try{
                $rules = array(
                    'select'    => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $tblSBobotRange = $this->Tbl_setting_bobot_range->read($rules)->result();
                foreach ($tblSBobotRange as $value){
                    $rules = array(
                        'where' => array('id_bobot_range' => $value->id_bobot_range),
                        'data'  => array(
                            'jalur'     => $this->input->post('jalur'),
                            'tahun'     => $this->input->post('tahun'),
                            'updated_by'=> $this->session->userdata('id_users'),
                        ),
                    );
                    $this->Tbl_setting_bobot_range->update($rules);
                }
                $this->session->set_flashdata('message','Data berhasil diubah.');
                $this->session->set_flashdata('type_message','success');
                redirect('Settings/BobotRange/');
            }catch (Exception $e){
                $this->session->set_flashdata('message', $e->getMessage());
                $this->session->set_flashdata('type_message','danger');
                redirect('Settings/BobotRange/');
            }

        }
    }

    function Simpan(){
        try{
            $rules = array(
                'select'    => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $tblSBobotRange = $this->Tbl_setting_bobot_range->read($rules)->result();
            $create = 0;
            $update = 0;
            $error = 0;
            foreach ($tblSBobotRange as $value){
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'kategori'  => $value->kategori,
                        'jalur'     => $value->jalur,
                        'tahun'     => $value->tahun,
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $tblHBobotRange = $this->Tbl_histori_bobot_range_ukt->where($rules);
                if ($tblHBobotRange->num_rows() == 0){
                    $data = array(
                        'kategori'  => $value->kategori,
                        'nilai_min' => $value->nilai_min,
                        'nilai_max' => $value->nilai_max,
                        'jalur'     => $value->jalur,
                        'tahun'     => $value->tahun,
                        'created_by'=> $this->session->userdata('id_users'),
                        'updated_by'=> $this->session->userdata('id_users'),
                    );
                    if ($this->Tbl_histori_bobot_range_ukt->create($data)){
                        $create++;
                    }else{
                        $error++;
                    }
                }else{
                    $tblHBobotRange = $tblHBobotRange->row();
                    $rules = array(
                        'where' => array('id_histori_bobot_range_ukt' => $tblHBobotRange->id_histori_bobot_range_ukt),
                        'data'  => array(
                            'kategori'  => $value->kategori,
                            'nilai_min' => $value->nilai_min,
                            'nilai_max' => $value->nilai_max,
                            'jalur'     => $value->jalur,
                            'tahun'     => $value->tahun,
                            'updated_by'=> $this->session->userdata('id_users'),
                        ),
                    );
                    if ($this->Tbl_histori_bobot_range_ukt->update($rules)){
                        $update++;
                    }else{
                        $error++;
                    }
                }
            }
            $this->session->set_flashdata('message',"Proses penyimpanan selesai.<br >Proses Create : $create <br />Proses Update : $update <br />Proses Error : $error");
            $this->session->set_flashdata('type_message','success');
            redirect('Settings/BobotRange/');
        }catch (Exception $e){
            $this->session->set_flashdata('message', $e->getMessage());
            $this->session->set_flashdata('type_message','danger');
            redirect('Settings/BobotRange/');
        }
    }

}

