<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Kampus extends CI_Controller {

    function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != "DEVELOPMENT") {
            $this->session->set_flashdata('message','Hak Akses Ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Dashboard');
        }
        $this->load->model('Settings/Tbl_setting_kampus');
        $this->load->model('Settings/Tbl_setting_gedung');
        $this->load->model('Settings/Tbl_setting_ruangan');
    }

	function index(){
        $rules = array(
            'select'    => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
		$data = array(
            'content'       => 'Settings/kampus/content',
            'css'           => 'Settings/kampus/css',
            'javascript'    => 'Settings/kampus/javascript',
            'modal'         => 'Settings/kampus/modal',
		    'tblSKampus'     => $this->Tbl_setting_kampus->read($rules)->result(),
		);
		$this->load->view('index',$data);
	}

	function Create(){
		$rules[] = array('field' => 'kampus', 'label' => 'Kampus', 'rules' => 'required');
        $rules[] = array('field' => 'status', 'label' => 'Status', 'rules' => 'required');
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('message',validation_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('Settings/Kampus/');
		}else{
		    try{
                $data = array(
                    'kampus'     => strtoupper($this->input->post('kampus')),
                    'status'    => $this->input->post('status'),
                    'created_by' => $this->session->userdata('id_users'),
                    'updated_by' => $this->session->userdata('id_users'),
                );
                $this->Tbl_setting_kampus->create($data);
                $this->session->set_flashdata('message','Data berhasil disimpan.');
            	$this->session->set_flashdata('type_message','success');
            	redirect('Settings/Kampus/');
            }catch (Exception $e){
                $this->session->set_flashdata('message', $e->getMessage());
                $this->session->set_flashdata('type_message','danger');
                redirect('Settings/Kampus/');
            }
		}
	}

	function Update($id){
        $rules[] = array('field' => 'kampus', 'label' => 'Kampus', 'rules' => 'required');
        $rules[] = array('field' => 'status', 'label' => 'Status', 'rules' => 'required');
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('message',validation_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('Settings/Kampus/');
		}else{
		    try{
                $rules = array(
                    'where' => array('id_kampus' => $id),
                    'data'  => array(
                        'kampus'     => strtoupper($this->input->post('kampus')),
                        'status'    => $this->input->post('status'),
                        'updated_by' => $this->session->userdata('id_users'),
                    ),
                );
                $this->Tbl_setting_kampus->update($rules);
                $this->Gedung($id);
                $this->session->set_flashdata('message','Data berhasil diubah.');
                $this->session->set_flashdata('type_message','success');
                redirect('Settings/Kampus/');
            }catch (Exception $e){
                $this->session->set_flashdata('message', $e->getMessage());
                $this->session->set_flashdata('type_message','danger');
                redirect('Settings/Kampus/');
            }
		}
	}

	function Delete($id){
        try{
            $rules = array('id_kampus' => $id);
            $this->Tbl_setting_kampus->delete($rules);
            $this->session->set_flashdata('message','Data berhasil dihapus.');
            $this->session->set_flashdata('type_message','success');
            redirect('Settings/Kampus/');
        }catch (Exception $e){
            $this->session->set_flashdata('message', $e->getMessage());
            $this->session->set_flashdata('type_message','danger');
            redirect('Settings/Kampus/');
        }
	}

    function Gedung($id){
        try{
            $status = $this->input->post('status');
            $id_users = $this->session->userdata('id_users');
            $rules = array(
                'where' => array('id_kampus' => $id),
                'data'  => array(
                    'status' => $status,
                    'updated_by' => $id_users,
                ),
            );
            $this->Tbl_setting_gedung->update($rules);
            $rules = array(
                'select'    => 'id_gedung',
                'where'     => array('id_kampus' => $id),
                'or_where'  => null,
                'order'     => null,
                'limit'     => array(
                    'awal'  => 0,
                    'akhir' => 1,
                ),
                'pagging'   => null,
            );
            $id = $this->Tbl_setting_gedung->where($rules)->row();
            $this->Ruangan($id, $status, $id_users);
            return true;
        }catch (Exception $e){
            return $e->getMessage();
        }
    }

    function Ruangan($id, $status, $id_users){
        try{
            $rules = array(
                'where' => array('id_gedung' => $id),
                'data'  => array(
                    'status' => $status,
                    'updated_by' => $id_users,
                ),
            );
            $this->Tbl_setting_ruangan->update($rules);
            return true;
        }catch (Exception $e){
            return $e->getMessage();
        }
    }

}

