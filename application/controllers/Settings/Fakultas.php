<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Fakultas extends CI_Controller {

	function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }       
        if ($this->session->userdata('level') != "DEVELOPMENT") {
            $this->session->set_flashdata('message','Hak Akses Ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Dashboard');
        }
        $this->load->model('Settings/Tbl_setting_fakultas');
        $this->load->model('Views/Daftar/View_daftar_ukt');
        $this->load->model('Views/Daftar/View_daftar_users');
    }

	function index(){
        $rules[0] = array(
            'select'    => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $rules[1] = array(
            'select'    => "YEAR(date_updated) as tahun",
            'where'     => null,
            'order'     => "tahun DESC",
        );
		$data = array(
            'content'       => 'Settings/fakultas/content',
            'css'           => 'Settings/fakultas/css',
            'javascript'    => 'Settings/fakultas/javascript',
            'modal'         => 'Settings/fakultas/modal',
			'tblSFakultas' 	=> $this->Tbl_setting_fakultas->read($rules[0])->result(),
			'tahun'			=> $this->View_daftar_users->distinct($rules[1])->result(),
		);
		$this->load->view('index',$data);
	}

	function Create(){
        $rules[] = array('field' => 'fakultas',	'label' => 'Fakultas', 'rules' => 'required');
        $rules[] = array('field' => 'status',	'label' => 'Status', 'rules' => 'required');
        $rules[] = array('field' => 'potongan',	'label' => 'Potongan', 'rules' => 'required');
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('message',validation_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('Settings/Fakultas/');
		}else{
		    try{
                $data = array(
                    'fakultas'		=> strtoupper($this->input->post('fakultas')),
                    'pendapatan'	=> 0,
                    'potongan'		=> ($this->input->post('potongan')/100),
                    'target'		=> 0,
                    'tahun'			=> date('Y'),
                    'status'		=> $this->input->post('status'),
                    'created_by'		=> $this->session->userdata('id_users'),
                    'updated_by'		=> $this->session->userdata('id_users'),
                );
                $this->Tbl_setting_fakultas->create($data);
                $this->session->set_flashdata('message','Data berhasil disimpan.');
                $this->session->set_flashdata('type_message','success');
                redirect('Settings/Fakultas/');
            }catch (Exception $e){
                $this->session->set_flashdata('message', $e->getMessage());
                $this->session->set_flashdata('type_message','danger');
                redirect('Settings/Fakultas/');
            }
		}
	}

	function Update($id){
        $rules[] = array('field' => 'fakultas',	'label' => 'Fakultas', 'rules' => 'required');
        $rules[] = array('field' => 'status',	'label' => 'Status', 'rules' => 'required');
		$rules[] = array('field' => 'potongan',	'label' => 'Potongan', 'rules' => 'required');
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('message',validation_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('Settings/Fakultas/');
		}else{
		    try{
                $rules = array(
                    'where' => array('id_fakultas' => $id),
                    'data'  => array(
                        'fakultas'	=> strtoupper($this->input->post('fakultas')),
                        'potongan'	=> ($this->input->post('potongan')/100),
                        'status'	=> $this->input->post('status'),
                        'updated_by'	=> $this->session->userdata('id_users'),
                    ),
                );
                $this->Tbl_setting_fakultas->update($rules);
                $this->session->set_flashdata('message','Data berhasil diubah.');
                $this->session->set_flashdata('type_message','success');
                redirect('Settings/Fakultas/');
            }catch (Exception $e){
                $this->session->set_flashdata('message', $e->getMessage());
                $this->session->set_flashdata('type_message','danger');
                redirect('Settings/Fakultas/');
            }
		}
	}

	function Delete($id){
	    try{
            $rules = array('id_fakultas' => $id);
            $this->Tbl_setting_fakultas->delete($rules);
            $this->session->set_flashdata('message','Data berhasil dihapus.');
            $this->session->set_flashdata('type_message','success');
            redirect('Settings/Fakultas/');
        }catch (Exception $e){
            $this->session->set_flashdata('message', $e->getMessage());
            $this->session->set_flashdata('type_message','danger');
            redirect('Settings/Fakultas/');
        }
	}

	function Generate(){
        $rules[] = array('field' => 'tahun', 'label' => 'tahun', 'rules' => 'required');
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == FALSE){
            $this->session->set_flashdata('message',validation_errors());
            $this->session->set_flashdata('type_message','danger');
            redirect('Settings/Fakultas/');
        }else{
            try{
                $tahun = $this->input->post('tahun');
                $rules = array(
                    'select'    => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $tblSFakultas = $this->Tbl_setting_fakultas->read($rules)->result();
                $success = 0;
                $error = 0;
                foreach ($tblSFakultas as $a){
                    $pendapatan = 0;
                    $rules = array(
                        'select'    => null,
                        'where'     => array(
                            'id_fakultas'		=> $a->id_fakultas,
                            'status_bayar'		=> '1',
                            'YEAR(date_created)'=> $tahun,
                        ),
                        'or_where'  => null,
                        'order'     => null,
                        'limit'     => null,
                        'pagging'   => null,
                    );
                    $viewDUkt = $this->View_daftar_ukt->where($rules);
                    foreach ($viewDUkt->result() as $b){
                        $pendapatan += $b->jumlah;
                    }
                    $target = $pendapatan - ($pendapatan * $a->potongan);
                    $rules = array(
                        'where' => array('id_fakultas' => $a->id_fakultas),
                        'data'  => array(
                            'pendapatan'	=> $pendapatan,
                            'target'		=> $target,
                            'tahun'			=> $tahun,
                        ),
                    );
                    if ($this->Tbl_setting_fakultas->update($rules)){
                        $success++;
                    }else{
                        $error++;
                    }
                }
                $this->session->set_flashdata('message','Generate data berhasil. Success : '.$success.'. Error : '.$error);
                $this->session->set_flashdata('type_message','success');
                redirect('Settings/Fakultas/');
            }catch (Exception $e){
                $this->session->set_flashdata('message', $e->getMessage());
                $this->session->set_flashdata('type_message','danger');
                redirect('Settings/Fakultas/');
            }
        }
	}

}

