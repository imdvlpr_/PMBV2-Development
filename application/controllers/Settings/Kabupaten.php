<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Kabupaten extends CI_Controller {

    function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != "DEVELOPMENT") {
            $this->session->set_flashdata('message','Hak Akses Ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Dashboard');
        }
        $this->load->model('Settings/Tbl_setting_jenis_daerah');
        $this->load->model('Settings/Tbl_setting_provinsi');
        $this->load->model('Settings/Tbl_setting_kabupaten');
        $this->load->model('Views/Settings/View_setting_kabupaten');
    }

    function index(){
        $rules[0] = array(
            'select'    => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $rules[1] = array(
            'select'    => null,
            'where'     => array(
                'id_jenis_daerah >' => 1,
                'id_jenis_daerah <' => 4,
            ),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $data = array(
            'content'       => 'Settings/kabupaten/content',
            'css'           => 'Settings/kabupaten/css',
            'javascript'    => 'Settings/kabupaten/javascript',
            'modal'         => 'Settings/kabupaten/modal',
            'viewSKabupaten'=> $this->View_setting_kabupaten->read($rules[0])->result(),
            'tblSProvinsi'  => $this->Tbl_setting_provinsi->read($rules[0])->result(),
            'tblSJDaerah'   => $this->Tbl_setting_jenis_daerah->where($rules[1])->result(),
        );
        $this->load->view('index',$data);
    }

    function Create(){
        $rules[] = array('field' => 'id_jenis_daerah',	'label' => 'Jenis Daerah', 'rules' => 'required');
        $rules[] = array('field' => 'id_provinsi',	'label' => 'ID Provinsi', 'rules' => 'required');
        $rules[] = array('field' => 'id_kabupaten',	'label' => 'ID Kabupaten', 'rules' => 'required');
        $rules[] = array('field' => 'kabupaten',	'label' => 'Provinsi', 'rules' => 'required');
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == FALSE){
            $this->session->set_flashdata('message',validation_errors());
            $this->session->set_flashdata('type_message','danger');
            redirect('Settings/Kabupaten/');
        }else{
            try{
                $data = array(
                    'id_jenis_daerah' => $this->input->post('id_jenis_daerah'),
                    'id_provinsi' => $this->input->post('id_provinsi'),
                    'id_kabupaten' => $this->input->post('id_kabupaten'),
                    'kabupaten' => $this->input->post('kabupaten'),
                    'created_by' => $this->session->userdata('id_users'),
                    'updated_by' => $this->session->userdata('id_users'),
                );
                $this->Tbl_setting_kabupaten->create($data);
                $this->session->set_flashdata('message','Data berhasil disimpan.');
                $this->session->set_flashdata('type_message','success');
                redirect('Settings/Kabupaten/');
            }catch (Exception $e){
                $this->session->set_flashdata('message', $e->getMessage());
                $this->session->set_flashdata('type_message','danger');
                redirect('Settings/Kabupaten/');
            }
        }
    }

    function Update($id){
        $rules[] = array('field' => 'id_jenis_daerah',	'label' => 'Jenis Daerah', 'rules' => 'required');
        $rules[] = array('field' => 'id_provinsi',	'label' => 'ID Provinsi', 'rules' => 'required');
        $rules[] = array('field' => 'id_kabupaten',	'label' => 'ID Kabupaten', 'rules' => 'required');
        $rules[] = array('field' => 'kabupaten',	'label' => 'Provinsi', 'rules' => 'required');
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == FALSE){
            $this->session->set_flashdata('message',validation_errors());
            $this->session->set_flashdata('type_message','danger');
            redirect('Settings/Kabupaten/');
        }else{
            try{
                $rules = array(
                    'where' => array('id_kabupaten' => $id),
                    'data'  => array(
                        'id_jenis_daerah' => $this->input->post('id_jenis_daerah'),
                        'id_provinsi' => $this->input->post('id_provinsi'),
                        'id_kabupaten' => $this->input->post('id_kabupaten'),
                        'kabupaten' => $this->input->post('kabupaten'),
                        'updated_by' => $this->session->userdata('id_users'),
                    ),
                );
                $this->Tbl_setting_kabupaten->update($rules);
                $this->session->set_flashdata('message','Data berhasil diubah.');
                $this->session->set_flashdata('type_message','success');
                redirect('Settings/Kabupaten/');
            }catch (Exception $e){
                $this->session->set_flashdata('message', $e->getMessage());
                $this->session->set_flashdata('type_message','danger');
                redirect('Settings/Kabupaten/');
            }
        }
    }

    function Delete($id){
        try{
            $rules = array('id_kabupaten' => $id);
            $this->Tbl_setting_kabupaten->delete($rules);
            $this->session->set_flashdata('message','Data berhasil dihapus.');
            $this->session->set_flashdata('type_message','success');
            redirect('Settings/Kabupaten/');
        }catch (Exception $e){
            $this->session->set_flashdata('message', $e->getMessage());
            $this->session->set_flashdata('type_message','danger');
            redirect('Settings/Kabupaten/');
        }
    }

}

