<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Pekerjaan extends CI_Controller {

	function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != "DEVELOPMENT") {
            $this->session->set_flashdata('message','Hak Akses Ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Dashboard');
        }
        $this->load->model('Settings/Tbl_setting_pekerjaan');
    }

	function index(){
        $rules = array(
            'select'    => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
		$data = array(
            'content'       => 'Settings/pekerjaan/content',
            'css'           => 'Settings/pekerjaan/css',
            'javascript'    => 'Settings/pekerjaan/javascript',
            'modal'         => 'Settings/pekerjaan/modal',
			'tblSPekerjaan' => $this->Tbl_setting_pekerjaan->read($rules)->result(),
		);
		$this->load->view('index',$data);
	}

	function Create(){
	    $rules[] = array('field' => 'pekerjaan', 'label' => 'Pekerjaan', 'rules' => 'required');
	    $rules[] = array('field' => 'nilai', 'label' => 'Nilai', 'rules' => 'required');
	    $rules[] = array('field' => 'status', 'label' => 'Status', 'rules' => 'required');
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('message',validation_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('Settings/Pekerjaan/');
		}else{
		    try{
                $data = array(
                    'pekerjaan'     => strtoupper($this->input->post('pekerjaan')),
                    'nilai'         => $this->input->post('nilai'),
                    'status'        => $this->input->post('status'),
                    'created_by'    => $this->session->userdata('id_users'),
                    'updated_by'    => $this->session->userdata('id_users'),
                );
                $this->Tbl_setting_pekerjaan->create($data);
                $this->session->set_flashdata('message','Data berhasil disimpan.');
                $this->session->set_flashdata('type_message','success');
                redirect('Settings/Pekerjaan/');
            }catch (Exception $e){
                $this->session->set_flashdata('message', $e->getMessage());
                $this->session->set_flashdata('type_message','danger');
                redirect('Settings/Pekerjaan/');
            }
		}
	}

	function Update($id){
        $rules[] = array('field' => 'pekerjaan', 'label' => 'Pekerjaan', 'rules' => 'required');
        $rules[] = array('field' => 'nilai', 'label' => 'Nilai', 'rules' => 'required');
        $rules[] = array('field' => 'status', 'label' => 'Status', 'rules' => 'required');
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('message',validation_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('Settings/Pekerjaan/');
		}else{
		    try{
                $rules = array(
                    'where' => array('id_pekerjaan' => $id),
                    'data'  => array(
                        'pekerjaan'     => strtoupper($this->input->post('pekerjaan')),
                        'nilai'         => $this->input->post('nilai'),
                        'status'        => $this->input->post('status'),
                        'updated_by'    => $this->session->userdata('id_users'),
                    ),
                );
                $this->Tbl_setting_pekerjaan->update($rules);
                $this->session->set_flashdata('message','Data berhasil diubah.');
                $this->session->set_flashdata('type_message','success');
                redirect('Settings/Pekerjaan/');
            }catch (Exception $e){
                $this->session->set_flashdata('message', $e->getMessage());
                $this->session->set_flashdata('type_message','danger');
                redirect('Settings/Pekerjaan/');
            }
		}
	}

	function Delete($id){
	    try{
            $rules = array('id_pekerjaan' => $id);
            $this->Tbl_setting_pekerjaan->delete($rules);
            $this->session->set_flashdata('message','Data berhasil dihapus.');
            $this->session->set_flashdata('type_message','success');
            redirect('Settings/Pekerjaan');
        }catch (Exception $e){
            $this->session->set_flashdata('message', $e->getMessage());
            $this->session->set_flashdata('type_message','danger');
            redirect('Settings/Pekerjaan');
        }
	}

}
