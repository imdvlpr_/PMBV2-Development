<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class SMTP extends CI_Controller {

	function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != "DEVELOPMENT") {
            $this->session->set_flashdata('message','Hak Akses Ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Dashboard');
        }
        $this->load->model('Settings/Tbl_setting_smtp');
    }

	function index(){
        $rules = array(
            'select'    => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $data = array(
            'content'   => 'Settings/smtp/content',
            'css'       => 'Settings/smtp/css',
            'javascript'=> 'Settings/smtp/javascript',
            'modal'     => 'Settings/smtp/modal',
            'tblSSmtp'  => $this->Tbl_setting_smtp->read($rules)->result(),
        );
        $this->load->view('index',$data);
	}

	function Create(){
	    $rules[] = array('field' => 'username',	'label' => 'username', 'rules' => 'required');
	    $rules[] = array('field' => 'password',	'label' => 'Password', 'rules' => 'required');
	    $rules[] = array('field' => 'quota',	'label' => 'Quota', 'rules' => 'required');
	    $rules[] = array('field' => 'status',	'label' => 'Status', 'rules' => 'required');
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('message',validation_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('Settings/SMTP/');
		}else{
		    try{
                $data = array(
                    'username'      => $this->input->post('username'),
                    'password' 		=> $this->input->post('password'),
                    'quota' 		=> $this->input->post('quota'),
                    'status' 		=> $this->input->post('status'),
                    'created_by'    => $this->session->userdata('id_users'),
                    'updated_by'    => $this->session->userdata('id_users'),
                );
                $this->Tbl_setting_smtp->create($data);
                $this->session->set_flashdata('message','Data berhasil disimpan.');
                $this->session->set_flashdata('type_message','success');
                redirect('Settings/SMTP/');
            }catch (Exception $e){
                $this->session->set_flashdata('message', $e->getMessage());
                $this->session->set_flashdata('type_message','danger');
                redirect('Settings/SMTP/');
            }
		}
	}

	function Update($id){
        $rules[] = array('field' => 'username',	'label' => 'username', 'rules' => 'required');
        $rules[] = array('field' => 'password',	'label' => 'Password', 'rules' => 'required');
        $rules[] = array('field' => 'quota',	'label' => 'Quota', 'rules' => 'required');
        $rules[] = array('field' => 'status',	'label' => 'Status', 'rules' => 'required');
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('message',validation_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('Settings/SMTP/');
		}else{
		    try{
                $rules = array(
                    'where' => array('id_smtp' => $id),
                    'data'  => array(
                        'username' 		=> $this->input->post('username'),
                        'password' 		=> $this->input->post('password'),
                        'quota' 		=> $this->input->post('quota'),
                        'status' 		=> $this->input->post('status'),
                        'updated_by'    => $this->session->userdata('id_users'),
                    ),
                );
                $this->Tbl_setting_smtp->update($rules);
                $this->session->set_flashdata('message','Data berhasil diubah.');
                $this->session->set_flashdata('type_message','success');
                redirect('Settings/SMTP/');
            }catch (Exception $e){
                $this->session->set_flashdata('message', $e->getMessage());
                $this->session->set_flashdata('type_message','danger');
                redirect('Settings/SMTP/');
            }
		}
	}

	function Delete($id){
        try{
            $rules = array('id_smtp' => $id);
            $this->Tbl_setting_smtp->delete($rules);
            $this->session->set_flashdata('message','Data berhasil dihapus.');
            $this->session->set_flashdata('type_message','success');
            redirect('Settings/SMTP/');
        }catch (Exception $e){
            $this->session->set_flashdata('message', $e->getMessage());
            $this->session->set_flashdata('type_message','danger');
            redirect('Settings/SMTP/');
        }
	}

}
