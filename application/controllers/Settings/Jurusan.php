<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Jurusan extends CI_Controller {

	function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != "DEVELOPMENT") {       
            $this->session->set_flashdata('message','Hak Akses Ditolak.');       
            $this->session->set_flashdata('type_message','danger');     
            redirect('Dashboard');     
        }
        $this->load->model('Settings/Tbl_setting_fakultas');
        $this->load->model('Settings/Tbl_setting_jurusan');
        $this->load->model('Settings/Tbl_setting_ukt');
        $this->load->model('Views/Settings/View_setting_fakultas_jurusan');
    }
	
	function index(){
        $rules[0] = array(
            'select'    => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $tblSJurusan = $this->Tbl_setting_jurusan->read($rules[0])->result();
        foreach ($tblSJurusan as $a){
            $rules[1] = array(
                'select'    => null,
                'where'     => array('kode_jurusan' => $a->kode_jurusan), //not null or null
                'or_where'  => null, //not null or null
                'order'     => 'kode_jurusan ASC, kategori ASC',
                'limit'     => null,
                'pagging'   => null,
            );
            $tblSUKT = $this->Tbl_setting_ukt->where($rules[1])->result();
            foreach ($tblSUKT as $b){
                $UKT[$a->kode_jurusan][0][] = $b->kategori;
                $UKT[$a->kode_jurusan][1][] = $b->nominal;
                $UKT[$a->kode_jurusan][2][] = $b->status;
            }
        }
        $data = array(
            'content'               => 'Settings/jurusan/content',
            'css'                   => 'Settings/jurusan/css',
            'javascript'            => 'Settings/jurusan/javascript',
            'modal'                 => 'Settings/jurusan/modal',
            'tblSFakultas'          => $this->Tbl_setting_fakultas->read($rules[0])->result(),
            'tblSJurusan'           => $tblSJurusan,
            'viewFakultasJurusan'   => $this->View_setting_fakultas_jurusan->read($rules[0])->result(),
            'UKT'                   => $UKT,
		);
		$this->load->view('index',$data);
	}

	function Create(){
        $rules[] = array('field' => 'kode_jurusan', 'label' => 'Kode Jurusan', 'rules' => 'required');
        $rules[] = array('field' => 'id_fakultas', 'label' => 'Fakultas', 'rules' => 'required');
        $rules[] = array('field' => 'jurusan', 'label' => 'Jurusan', 'rules' => 'required');
        $rules[] = array('field' => 'akreditasi', 'label' => 'Akreditasi', 'rules' => 'required');
        $rules[] = array('field' => 'kategori', 'label' => 'Kategori', 'rules' => 'required');
        $rules[] = array('field' => 'grade_ipa', 'label' => 'Grade IPA', 'rules' => 'required');
        $rules[] = array('field' => 'grade_ipa', 'label' => 'Grade IPS', 'rules' => 'required');
        $rules[] = array('field' => 'quota', 'label' => 'Kuota', 'rules' => 'required');
        $rules[] = array('field' => 'quota_k1', 'label' => 'Kuota', 'rules' => 'required');
	    $rules[] = array('field' => 'status', 'label' => 'Status', 'rules' => 'required');
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('message',validation_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('Settings/Jurusan/');
		}else{
		    try{
                $data = array(
                    'kode_jurusan'	=> $this->input->post('kode_jurusan'),
                    'id_fakultas'	=> $this->input->post('id_fakultas'),
                    'jurusan'		=> strtoupper($this->input->post('jurusan')),
                    'akreditasi'	=> $this->input->post('akreditasi'),
                    'kategori'		=> $this->input->post('kategori'),
                    'grade_ipa'     => $this->input->post('grade_ipa'),
                    'grade_ips'     => $this->input->post('grade_ips'),
                    'quota'         => $this->input->post('quota'),
                    'quota_k1'      => $this->input->post('quota_k1'),
                    'status'        => $this->input->post('status'),
                    'created_by'    => $this->session->userdata('id_users'),
                    'updated_by'    => $this->session->userdata('id_users'),
                );
                $this->Tbl_setting_jurusan->create($data);
                $this->session->set_flashdata('message','Data berhasil disimpan.');
                $this->session->set_flashdata('type_message','success');
                redirect('Settings/Jurusan/');
            }catch (Exception $e){
                $this->session->set_flashdata('message', $e->getMessage());
                $this->session->set_flashdata('type_message','danger');
                redirect('Settings/Jurusan/');
            }
		}
	}

	function Update($id){
        $rules[] = array('field' => 'kode_jurusan', 'label' => 'Kode Jurusan', 'rules' => 'required');
        $rules[] = array('field' => 'id_fakultas', 'label' => 'Fakultas', 'rules' => 'required');
        $rules[] = array('field' => 'jurusan', 'label' => 'Jurusan', 'rules' => 'required');
        $rules[] = array('field' => 'akreditasi', 'label' => 'Akreditasi', 'rules' => 'required');
        $rules[] = array('field' => 'kategori', 'label' => 'Kategori', 'rules' => 'required');
        $rules[] = array('field' => 'grade', 'label' => 'Grade', 'rules' => 'required');
        $rules[] = array('field' => 'quota', 'label' => 'Kuota', 'rules' => 'required');
        $rules[] = array('field' => 'quota_k1', 'label' => 'Kuota', 'rules' => 'required');
        $rules[] = array('field' => 'status', 'label' => 'Status', 'rules' => 'required');
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('message',validation_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('Settings/Jurusan/');
		}else{
		    try{
                $rules = array(
                    'where' => array('kode_jurusan' => $id),
                    'data'  => array(
                        'kode_jurusan'	=> $this->input->post('kode_jurusan'),
                        'id_fakultas'	=> $this->input->post('id_fakultas'),
                        'jurusan'		=> strtoupper($this->input->post('jurusan')),
                        'akreditasi'	=> $this->input->post('akreditasi'),
                        'kategori'		=> $this->input->post('kategori'),
                        'grade_ipa'     => $this->input->post('grade_ipa'),
                        'grade_ips'     => $this->input->post('grade_ips'),
                        'quota'         => $this->input->post('quota'),
                        'quota_k1'      => $this->input->post('quota_k1'),
                        'status'        => $this->input->post('status'),
                        'updated_by'    => $this->session->userdata('id_users'),
                    ),
                );
                $this->Tbl_setting_jurusan->update($rules);
                $this->session->set_flashdata('message','Data berhasil diubah.');
                $this->session->set_flashdata('type_message','success');
                redirect('Settings/Jurusan/');
            }catch (Exception $e){
                $this->session->set_flashdata('message', $e->getMessage());
                $this->session->set_flashdata('type_message','danger');
                redirect('Settings/Jurusan/');
            }
		}
	}

	function Delete($id){
	    try{
            $rules = array('kode_jurusan' => $id);
            $this->Tbl_setting_jurusan->delete($rules);
            $this->session->set_flashdata('message','Data berhasil dihapus.');
            $this->session->set_flashdata('type_message','success');
            redirect('Settings/Jurusan/');
        }catch (Exception $e){
            $this->session->set_flashdata('message', $e->getMessage());
            $this->session->set_flashdata('type_message','danger');
            redirect('Settings/Jurusan/');
        }
	}

}
