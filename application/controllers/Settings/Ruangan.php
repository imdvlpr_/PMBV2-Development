<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Ruangan extends CI_Controller {

    function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != "DEVELOPMENT") {
            $this->session->set_flashdata('message','Hak Akses Ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Dashboard');
        }
        $this->load->model('Settings/Tbl_setting_kampus');
        $this->load->model('Settings/Tbl_setting_gedung');
        $this->load->model('Settings/Tbl_setting_ruangan');
        $this->load->model('Views/Settings/View_setting_ruangan');
    }

    function index(){
        $rules[0] = array(
            'select'    => null,
            'where'     => array('status_gedung' => '1'),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $rules[1] = array(
            'select'    => null,
            'where'     => array('status' => '1'),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $data = array(
            'content'       => 'Settings/ruangan/read/content',
            'css'           => 'Settings/ruangan/read/css',
            'javascript'    => 'Settings/ruangan/read/javascript',
            'modal'         => 'Settings/ruangan/read/modal',
            'tblSKampus'    => $this->Tbl_setting_kampus->where($rules[1])->result(),
            'viewSRuangan'  => $this->View_setting_ruangan->where($rules[0])->result(),
        );
        $this->load->view('index',$data);
    }

    function Create(){
        $rules[] = array('field' => 'id_gedung', 'label' => 'Gedung', 'rules' => 'required');
        $rules[] = array('field' => 'ruangan', 'label' => 'Ruangan', 'rules' => 'required');
        $rules[] = array('field' => 'status', 'label' => 'Status', 'rules' => 'required');
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == FALSE){
            $this->session->set_flashdata('message',validation_errors());
            $this->session->set_flashdata('type_message','danger');
            redirect('Settings/Ruangan/');
        }else{
            try{
                $data = array(
                    'id_gedung' => $this->input->post('id_gedung'),
                    'ruangan' => strtoupper($this->input->post('ruangan')),
                    'status' => $this->input->post('status'),
                    'created_by' => $this->session->userdata('id_users'),
                    'updated_by' => $this->session->userdata('id_users'),
                );
                $this->Tbl_setting_ruangan->create($data);
                $this->session->set_flashdata('message','Data berhasil disimpan.');
                $this->session->set_flashdata('type_message','success');
                redirect('Settings/Ruangan/');
            }catch (Exception $e){
                $this->session->set_flashdata('message', $e->getMessage());
                $this->session->set_flashdata('type_message','danger');
                redirect('Settings/Ruangan/');
            }
        }
    }

    function Edit($id){
        $rules[0] = array(
            'select'    => null,
            'where'     => array('id_ruangan' => $id),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $rules[1] = array(
            'select'    => null,
            'where'     => array('status' => '1'),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $data = array(
            'content'       => 'Settings/ruangan/edit/content',
            'css'           => 'Settings/ruangan/edit/css',
            'javascript'    => 'Settings/ruangan/edit/javascript',
            'modal'         => 'Settings/ruangan/edit/modal',
            'tblSKampus'    => $this->Tbl_setting_kampus->where($rules[1])->result(),
            'viewSRuangan'  => $this->View_setting_ruangan->where($rules[0])->row(),
        );
        $this->load->view('index',$data);
    }

    function Update($id){
        $id_gedung = $this->input->post('id_gedung');
        if (!empty($id_gedung)){
            $rules[] = array('field' => 'id_gedung',	'label' => 'Gedung', 'rules' => 'required');
        }else{
            $rules[] = array('field' => 'old_gedung',	'label' => 'Old Gedung', 'rules' => 'required');
            $id_gedung = $this->input->post('old_gedung');
        }
        $rules[] = array('field' => 'ruangan', 'label' => 'Ruangan', 'rules' => 'required');
        $rules[] = array('field' => 'status', 'label' => 'Status', 'rules' => 'required');
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == FALSE){
            $this->session->set_flashdata('message',validation_errors());
            $this->session->set_flashdata('type_message','danger');
            redirect('Settings/Ruangan/');
        }else{
            try{
                $rules = array(
                    'where' => array('id_ruangan' => $id),
                    'data'  => array(
                        'id_gedung' => $id_gedung,
                        'ruangan' => strtoupper($this->input->post('ruangan')),
                        'status' => $this->input->post('status'),
                        'updated_by' => $this->session->userdata('id_users'),
                    ),
                );
                $this->Tbl_setting_ruangan->update($rules);
                $this->session->set_flashdata('message','Data berhasil diubah.');
                $this->session->set_flashdata('type_message','success');
                redirect('Settings/Ruangan/Edit/'.$id);
            }catch (Exception $e){
                $this->session->set_flashdata('message', $e->getMessage());
                $this->session->set_flashdata('type_message','danger');
                redirect('Settings/Ruangan/Edit/'.$id);
            }
        }
    }

    function Delete($id){
        try{
            $rules = array('id_ruangan' => $id);
            $this->Tbl_setting_ruangan->delete($rules);
            $this->session->set_flashdata('message','Data berhasil dihapus.');
            $this->session->set_flashdata('type_message','success');
            redirect('Settings/Ruangan/');
        }catch (Exception $e){
            $this->session->set_flashdata('message', $e->getMessage());
            $this->session->set_flashdata('type_message','danger');
            redirect('Settings/Ruangan/');
        }
    }

    function listGedung(){
        $rules = array(
            'select'    => null,
            'where'     => array(
                'id_kampus' => $this->input->post("id_kampus"),
                'status' => '1'
            ),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $gedung = $this->Tbl_setting_gedung->where($rules)->result();
        $lists = null;
        foreach($gedung as $data){
            $lists .= "<option value='".$data->id_gedung."'>".$data->gedung."</option>";
        }
        $callback = array('list_gedung' => $lists);
        echo json_encode($callback);
    }

}
