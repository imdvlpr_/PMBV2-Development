<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Rumpun extends CI_Controller {
	
	function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != "DEVELOPMENT") {
            $this->session->set_flashdata('message','Hak Akses Ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Dashboard');
        }
        $this->load->model('Settings/Tbl_setting_rumpun');
    }

	function index(){
        $rules = array(
            'select'    => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $data = array(
            'content'       => 'Settings/rumpun/content',
            'css'           => 'Settings/rumpun/css',
            'javascript'    => 'Settings/rumpun/javascript',
            'modal'         => 'Settings/rumpun/modal',
            'tblSRumpun'    => $this->Tbl_setting_rumpun->read($rules)->result(),
        );
        $this->load->view('index',$data);
	}

	function Create(){
	    $rules[] = array('field' => 'rumpun',	'label' => 'Rumpun', 'rules' => 'required');
	    $rules[] = array('field' => 'status',	'label' => 'Status', 'rules' => 'required');
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('message',validation_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('Settings/Rumpun/');
		}else{
		    try{
                $data = array(
                    'rumpun'    => strtoupper($this->input->post('rumpun')),
                    'status'    => $this->input->post('status'),
                    'created_by'=> $this->session->userdata('id_users'),
                    'updated_by'=> $this->session->userdata('id_users'),
                );
                $this->Tbl_setting_rumpun->create($data);
                $this->session->set_flashdata('message','Data berhasil disimpan.');
                $this->session->set_flashdata('type_message','success');
                redirect('Settings/Rumpun/');
            }catch (Exception $e){
                $this->session->set_flashdata('message', $e->getMessage());
                $this->session->set_flashdata('type_message','danger');
                redirect('Settings/Rumpun/');
            }
		}
	}

	function Update($id){
        $rules[] = array('field' => 'rumpun',	'label' => 'Rumpun', 'rules' => 'required');
        $rules[] = array('field' => 'status',	'label' => 'Status', 'rules' => 'required');
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('message',validation_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('Settings/Rumpun/');
		}else{
		    try{
                $rules = array(
                    'where' => array('id_rumpun' => $id),
                    'data'  => array(
                        'rumpun'    => strtoupper($this->input->post('rumpun')),
                        'status'    => $this->input->post('status'),
                        'updated_by'=> $this->session->userdata('id_users'),
                    )
                );
                $this->Tbl_setting_rumpun->update($rules);
                $this->session->set_flashdata('message','Data berhasil diubah.');
                $this->session->set_flashdata('type_message','success');
                redirect('Settings/Rumpun/');
            }catch (Exception $e){
                $this->session->set_flashdata('message', $e->getMessage());
                $this->session->set_flashdata('type_message','danger');
                redirect('Settings/Rumpun/');
            }
		}
	}

	function Delete($id){
        try{
            $rules = array('id_rumpun' => $id);
            $this->Tbl_setting_rumpun->delete($rules);
            $this->session->set_flashdata('message','Data berhasil dihapus.');
            $this->session->set_flashdata('type_message','success');
            redirect('Settings/Rumpun/');
        }catch (Exception $e){
            $this->session->set_flashdata('message', $e->getMessage());
            $this->session->set_flashdata('type_message','danger');
            redirect('Settings/Rumpun/');
        }
	}

}

