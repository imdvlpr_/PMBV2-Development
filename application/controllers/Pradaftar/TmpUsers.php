<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class TmpUsers extends CI_Controller {

	function __construct(){
		parent::__construct();
		if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
		if ($this->session->userdata('level') != 'DEVELOPMENT') {
			$this->session->set_flashdata('message','Hak akses ditolak.');
			$this->session->set_flashdata('type_message','danger');
			redirect('Auth');
		}
		$this->load->model('ServerSide/SS_tmp_pradaftar_users', '', TRUE);
		$this->load->model('Pradaftar/Tbl_pradaftar_users');
		$this->load->model('Tmp/tmp_pradaftar_users');
	}

	function index(){
        $data = array(
            'content'       => 'Pradaftar/tmpusers/read/content',
            'css'           => 'Pradaftar/tmpusers/read/css',
            'javascript'    => 'Pradaftar/tmpusers/read/javascript',
            'modal'         => 'Pradaftar/tmpusers/read/modal',
        );
		$this->load->view('index', $data);
	}

	function Detail($id){
        $rules = array(
            'select'    => null,
            'where'     => array(
                'id_tmp_pradaftar_users' => $id,
            ),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
		$data = array(
            'content'       => 'Pradaftar/tmpusers/detail/content',
            'css'           => 'Pradaftar/tmpusers/detail/css',
            'javascript'    => 'Pradaftar/tmpusers/detail/javascript',
            'modal'         => 'Pradaftar/tmpusers/detail/modal',
			'tmpPUsers'		=> $this->tmp_pradaftar_users->where($rules)->row(),
		);
		$this->load->view('index',$data);
	}

	function Rollback($id){
        try{
            $rules = array(
                'select'    => null,
                'where'     => array(
                    'id_tmp_pradaftar_users' => $id,
                ),
                'or_where'  => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $tmpPUsers = $this->tmp_pradaftar_users->where($rules)->row();
            $data = array(
                'kd_pembayaran' => $tmpPUsers->kd_pembayaran,
                'nomor_peserta' => ($tmpPUsers->nomor_peserta == '-')? $tmpPUsers->nomor_peserta.$tmpPUsers->kd_pembayaran : $tmpPUsers->nomor_peserta,
                'nik_passport' => $tmpPUsers->nik_passport,
                'nama' => $tmpPUsers->nama,
                'tgl_lhr' => $tmpPUsers->tgl_lhr,
                'kategori' => $tmpPUsers->kategori,
                'id_tipe_ujian' => $tmpPUsers->id_tipe_ujian,
                'nmr_tlp' => $tmpPUsers->nmr_tlp,
                'email' => $tmpPUsers->email,
                'password' => $tmpPUsers->password,
                'ip_login' => $tmpPUsers->ip_login,
                'login' => $tmpPUsers->login,
                'ip_logout' => $tmpPUsers->ip_logout,
                'logout' => $tmpPUsers->logout,
                'pembayaran' => $tmpPUsers->pembayaran,
                'biodata' => $tmpPUsers->biodata,
            );
            $this->Tbl_pradaftar_users->create($data);
            $rules = array('id_tmp_pradaftar_users' => $id);
            $this->tmp_pradaftar_users->delete($rules);
            $this->session->set_flashdata('message','Akun berhasil digunakan kembali.');
            $this->session->set_flashdata('type_message','success');
            redirect('Pradaftar/TmpUsers/');
        }catch (Exception $e){
            $this->session->set_flashdata('message', $e->getMessage());
            $this->session->set_flashdata('type_message','danger');
            redirect('Pradaftar/TmpUsers/Detail/'.$id);
        }

    }

    function Json(){
        $fetch_data = $this->SS_tmp_pradaftar_users->make_datatables();
        $data = array();
        foreach($fetch_data as $row)
        {
            $sub_array = array();
            $sub_array[] = "
                <div class=\"btn-group\">
                    <button type=\"button\" class=\"btn btn-default dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                        <i class='fa fa-gear'></i> <span class=\"caret\"></span>
                    </button>
                    <ul class=\"dropdown-menu\">
                        <li><a href=\"".base_url('Pradaftar/TmpUsers/Detail/'.$row->id_tmp_pradaftar_users)."\" target='_blank'>Detail</a></li>
                        <li role=\"separator\" class=\"divider\"></li>
                        <li><a href=\"".base_url('Pradaftar/TmpUsers/Rollback/'.$row->id_tmp_pradaftar_users)."\" onclick=\"return confirm('Apakah Anda Yakin ?')\">Gunakan Kembali</a></li>
                    </ul>
                </div>";
            $sub_array[] = $row->kd_pembayaran;
            $sub_array[] = $row->nik_passport;
            $sub_array[] = $row->nama;
            $sub_array[] = ($row->kategori == 1) ? "IPA / SAINS" : "IPS / SOSIAL DAN HUMANIORA";
            $sub_array[] = $row->tgl_daftar;
            $sub_array[] = $row->date_created;
            $sub_array[] = $row->date_updated;
            $data[] = $sub_array;
        }
        $output = array(
            "draw"				=>	intval($_POST["draw"]),
            "recordsTotal"		=>	$this->SS_tmp_pradaftar_users->get_all_data(),
            "recordsFiltered"	=>	$this->SS_tmp_pradaftar_users->get_filtered_data(),
            "data"				=>	$data
        );
        echo json_encode($output);
    }

}
