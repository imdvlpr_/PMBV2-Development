<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Users extends CI_Controller {

	function __construct(){
		parent::__construct();
		if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
		if ($this->session->userdata('level') != 'DEVELOPMENT') {
			$this->session->set_flashdata('message','Hak akses ditolak.');
			$this->session->set_flashdata('type_message','danger');
			redirect('Auth');
		}
		$this->load->model('ServerSide/SS_pradaftar_users', '', TRUE);
		$this->load->model('Pradaftar/Tbl_pradaftar_biodata');
		$this->load->model('Pradaftar/Tbl_pradaftar_jadwal');
		$this->load->model('Pradaftar/Tbl_pradaftar_kelulusan');
		$this->load->model('Pradaftar/Tbl_pradaftar_orangtua');
		$this->load->model('Pradaftar/Tbl_pradaftar_pembayaran');
		$this->load->model('Pradaftar/Tbl_pradaftar_pilihan');
		$this->load->model('Pradaftar/Tbl_pradaftar_setting');
		$this->load->model('Pradaftar/Tbl_pradaftar_users');
		$this->load->model('Tmp/Tmp_pradaftar_users');
		$this->load->model('Settings/Tbl_setting_tipe_ujian');
		$this->load->model('Views/Pradaftar/View_pradaftar_biodata');
		$this->load->model('Views/Pradaftar/View_pradaftar_jadwal');
		$this->load->model('Views/Pradaftar/View_pradaftar_kelulusan');
		$this->load->model('Views/Pradaftar/View_pradaftar_orangtua');
		$this->load->model('Views/Pradaftar/View_pradaftar_pembayaran');
		$this->load->model('Views/Pradaftar/View_pradaftar_pilihan');
		$this->load->model('Views/Pradaftar/View_pradaftar_users');
	}

	function index(){
        $data = array(
            'content'       => 'Pradaftar/users/read/content',
            'css'           => 'Pradaftar/users/read/css',
            'javascript'    => 'Pradaftar/users/read/javascript',
            'modal'         => 'Pradaftar/users/read/modal',
        );
        $this->load->view('index', $data);
	}

	function Detail($id){
        $rules[0] = array(
            'select'    => null,
            'where'     => array(
                'id_pradaftar_users' => $id,
            ),
            'or_where'  => null,
            'order'     => 'date_created ASC',
            'limit'     => null,
            'pagging'   => null,
        );
        $tblPUsers = $this->View_pradaftar_users->where($rules[0])->row();
        $rules[1] = array(
            'select'    => null,
            'where'     => array(
                'nomor_peserta' => $tblPUsers->nomor_peserta,
            ),
            'or_where'  => null,
            'order'     => 'date_created ASC',
            'limit'     => null,
            'pagging'   => null,
        );
        $rules[2] = array(
            'select'    => null,
            'where'     => array(
                'kd_pembayaran' => $tblPUsers->kd_pembayaran,
            ),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
		$data = array(
            'content'           => 'Pradaftar/users/detail/content',
            'css'               => 'Pradaftar/users/detail/css',
            'javascript'        => 'Pradaftar/users/detail/javascript',
            'modal'             => 'Pradaftar/users/detail/modal',
            'tblPUsers'         => $tblPUsers,
            'tblPBiodata'       => $this->View_pradaftar_biodata->where($rules[0])->row(),
            'tblPJadwal'        => $this->View_pradaftar_jadwal->where($rules[1])->row(),
            'tblPKelulusan' 	=> $this->View_pradaftar_kelulusan->where($rules[1])->row(),
            'tblPOrangTua'      => $this->View_pradaftar_orangtua->where($rules[0])->result(),
            'tblPPembayaran'    => $this->View_pradaftar_pembayaran->where($rules[2])->row(),
            'tblPPilihan'       => $this->View_pradaftar_pilihan->where($rules[1])->result(),
		);
		$this->load->view('index',$data);
	}

	function Update($id){
		$rules[] = array('field' => 'nik_passport', 'label' => 'NIK (No.KTP)', 'rules' => 'required');
		$rules[] = array('field' => 'nama', 'label' => 'Nama', 'rules' => 'required');
		$rules[] = array('field' => 'tgl_lhr', 'label' => 'Tanggal Lahir', 'rules' => 'required');
		$rules[] = array('field' => 'kategori', 'label' => 'Kategori', 'rules' => 'required');
		$rules[] = array('field' => 'nmr_tlp', 'label' => 'Nomor Telepon', 'rules' => 'required');
		$rules[] = array('field' => 'email', 'label' => 'Email', 'rules' => 'required');
		$rules[] = array('field' => 'biodata', 'label' => 'Verifikasi Biodata', 'rules' => 'required');
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('message',validation_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('Pradaftar/Users/Detail/'.$id);
		}else{
			try{
                $password = $this->input->post('password');
				if (!empty($password)){
					$data = array(
						'nik_passport'  => $this->input->post('nik_passport'),
						'nama'          => strtoupper($this->input->post('nama')),
						'tgl_lhr'       => $this->input->post('tgl_lhr'),
						'kategori'      => $this->input->post('kategori'),
						'nmr_tlp'       => $this->input->post('nmr_tlp'),
						'email'         => $this->input->post('email'),
						'password'      => md5(md5($password)),
						'biodata'       => $this->input->post('biodata'),
					);
				}else{
					$data = array(
						'nik_passport'  => $this->input->post('nik_passport'),
						'nama'          => strtoupper($this->input->post('nama')),
						'tgl_lhr'       => $this->input->post('tgl_lhr'),
						'kategori'      => $this->input->post('kategori'),
						'nmr_tlp'       => $this->input->post('nmr_tlp'),
						'email'         => $this->input->post('email'),
						'biodata'       => $this->input->post('biodata'),
					);
				}
                $rules = array(
                    'where' => array(
                        'id_pradaftar_users' => $id
                    ),
                    'data'  => $data,
                );
				$this->Tbl_pradaftar_users->update($rules);
				$this->session->set_flashdata('message','Data berhasil diubah.');
				$this->session->set_flashdata('type_message','success');
				redirect('Pradaftar/Users/Detail/'.$id);
			}catch (Exception $e){
				$this->session->set_flashdata('message', $e->getMessage());
				$this->session->set_flashdata('type_message','danger');
				redirect('Pradaftar/Users/Detail/'.$id);
			}
		}
	}

	function Delete($id){
		try{
            $rules = array(
                'select'    => null,
                'where'     => array(
                    'id_pradaftar_users' => $id,
                ),
                'or_where'  => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
			$tblPUsers = $this->Tbl_pradaftar_users->where($rules)->row();
			if ($tblPUsers->pembayaran == '1'){
				$this->session->set_flashdata('message', 'Data yang sudah diverifikasi pembayaran tidak bisa dihapus.');
				$this->session->set_flashdata('type_message','warning');
				redirect('Pradaftar/Users/Detail/'.$id);
			}else{
				$data = array(
					'kd_pembayaran'	=> $tblPUsers->kd_pembayaran,
                    'nomor_peserta'	=> $tblPUsers->nomor_peserta,
                    'nik_passport'	=> $tblPUsers->nik_passport,
					'nama'	=> $tblPUsers->nama,
					'tgl_lhr'	=> $tblPUsers->tgl_lhr,
					'kategori'	=> $tblPUsers->kategori,
					'id_tipe_ujian'	=> $tblPUsers->id_tipe_ujian,
					'nmr_tlp'	=> $tblPUsers->nmr_tlp,
					'email'	=> $tblPUsers->email,
					'password'	=> $tblPUsers->password,
                    'ip_login'	=> $tblPUsers->ip_login,
                    'login'	=> $tblPUsers->login,
                    'ip_logout'	=> $tblPUsers->ip_logout,
                    'logout'	=> $tblPUsers->logout,
					'pembayaran'	=> $tblPUsers->pembayaran,
					'biodata'	=> $tblPUsers->biodata,
					'tgl_daftar'	=> $tblPUsers->date_created,
				);
				$this->Tmp_pradaftar_users->create($data);
				$rules = array('id_pradaftar_users' => $id);
				$this->Tbl_pradaftar_users->delete($rules);
				$this->session->set_flashdata('message','Data berhasil dihapus.');
				$this->session->set_flashdata('type_message','success');
				redirect('Pradaftar/Users/');
			}
		}catch (Exception $e){
			$this->session->set_flashdata('message',$e->getMessage());
			$this->session->set_flashdata('type_message','danger');
			redirect('Pradaftar/Users/Detail/'.$id);
		}
	}

	function Json(){
		$fetch_data = $this->SS_pradaftar_users->make_datatables();
		$data = array();
		foreach($fetch_data as $row)
		{
			$sub_array = array();
            $sub_array[] = "
                <div class=\"btn-group\">
                    <button type=\"button\" class=\"btn btn-default dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                        <i class='fa fa-gear'></i> <span class=\"caret\"></span>
                    </button>
                    <ul class=\"dropdown-menu\">
                        <li><a href=\"".base_url('Pradaftar/Users/Detail/'.$row->id_pradaftar_users)."\" target='_blank'>Detail</a></li>
                        <li role=\"separator\" class=\"divider\"></li>
                        <li><a href=\"".base_url('Pradaftar/Users/Delete/'.$row->id_pradaftar_users)."\" onclick=\"return confirm('Apakah Anda Yakin ?')\">Delete</i></a></li>
                    </ul>
                </div>";
            $sub_array[] = $row->kd_pembayaran;
            $sub_array[] = $row->nomor_peserta;
            $sub_array[] = $row->nik_passport;
            $sub_array[] = $row->nama;
            $sub_array[] = ($row->kategori == 1) ? "IPA / SAINS" : "IPS / SOSIAL DAN HUMANIORA";
            $sub_array[] = $row->tipe_ujian;
            $sub_array[] = ($row->pembayaran == 1) ? "<span class=\"label label-success\">Sudah Bayar</span>" : "<span class=\"label label-danger\">Belum Bayar</span>";
            $sub_array[] = ($row->biodata == 1) ? "<span class=\"label label-success\">Sudah Verifikasi</span>" : "<span class=\"label label-danger\">Belum Verifikasi</span>";
			$data[] = $sub_array;
		}
		$output = array(
			"draw"				=>	intval($_POST["draw"]),
			"recordsTotal"		=>	$this->SS_pradaftar_users->get_all_data(),
			"recordsFiltered"	=>	$this->SS_pradaftar_users->get_filtered_data(),
			"data"				=>	$data
		);
		echo json_encode($output);
	}

}
