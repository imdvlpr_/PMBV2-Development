<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Statistik extends CI_Controller {
	
    function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != 'DEVELOPMENT') {
            $this->session->set_flashdata('message','Hak akses ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }

        $this->load->model('Pradaftar/Tbl_pradaftar_biodata');
        $this->load->model('Pradaftar/Tbl_pradaftar_kelulusan');
        $this->load->model('Pradaftar/Tbl_pradaftar_pembayaran');
        $this->load->model('Pradaftar/Tbl_pradaftar_pilihan');
        $this->load->model('Pradaftar/Tbl_pradaftar_users');
        $this->load->model('Settings/Tbl_setting_provinsi');
        $this->load->model('Settings/Tbl_setting_rumpun');
        $this->load->model('Settings/Tbl_setting_asal_sekolah');
        $this->load->model('Settings/Tbl_setting_tipe_ujian');
        $this->load->model('Tmp/Tmp_pradaftar_users');
        $this->load->model('Views/Pradaftar/View_pradaftar_biodata');
        $this->load->model('Views/Settings/View_setting_fakultas_jurusan');

    }
	
    function index(){
        $tahun = $this->input->post('tahun');
        if (empty($tahun)){
            $kosong = true;
        }else{
            $kosong = false;
            $rules = array(
                'select'    => null,
                'where'     => array('YEAR(date_created)' => $tahun),
                'or_where'  => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $num_users = $this->Tbl_pradaftar_users->where($rules)->num_rows();
            $num_tmp_users = $this->Tmp_pradaftar_users->where($rules)->num_rows();
            $rules = array(
                'select'    => null,
                'where'     => array(
                    'biodata' => '1',
                    'pembayaran' => '1',
                    'YEAR(date_created)' => $tahun
                ),
                'or_where'  => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $num_users_sv = $this->Tbl_pradaftar_users->where($rules)->num_rows();
            $rules = array(
                'select'    => null,
                'where'     => array(
                    'biodata' => '0',
                    'pembayaran' => '1',
                    'YEAR(date_created)' => $tahun
                ),
                'or_where'  => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $num_users_bv = $this->Tbl_pradaftar_users->where($rules)->num_rows();
            $rules = array(
                'select'    => null,
                'where'     => array('YEAR(date_created)' => $tahun),
                'or_where'  => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $total = 0;
            $tblPPembayaran = $this->Tbl_pradaftar_pembayaran->where($rules);
            $num_pembayaran = $tblPPembayaran->num_rows();
            foreach ($tblPPembayaran->result() as $value){
                $total += $value->uang;
            }
            $rules = array(
                'select'    => null,
                'where'     => array(
                    'status' => '1',
                    'YEAR(date_created)' => $tahun
                ),
                'or_where'  => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $num_pembayaran_sv = $this->Tbl_pradaftar_pembayaran->where($rules)->num_rows();
            $rules = array(
                'select'    => null,
                'where'     => array(
                    'status' => '0',
                    'YEAR(date_created)' => $tahun
                ),
                'or_where'  => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $num_pembayaran_bv = $this->Tbl_pradaftar_pembayaran->where($rules)->num_rows();
            $rules = array(
                'select'    => null,
                'where'     => array(
                    'kategori' => 1,
                    'YEAR(date_created)' => $tahun
                ),
                'or_where'  => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $num_kategori_ipa = $this->Tbl_pradaftar_users->where($rules)->num_rows();
            $rules = array(
                'select'    => null,
                'where'     => array(
                    'kategori' => 2,
                    'YEAR(date_created)' => $tahun
                ),
                'or_where'  => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $num_kategori_ips = $this->Tbl_pradaftar_users->where($rules)->num_rows();
            $rules = array(
                'select'    => null,
                'where'     => array(
                    'jenis_kelamin' => 'L',
                    'YEAR(date_created)' => $tahun
                ),
                'or_where'  => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $num_man = $this->Tbl_pradaftar_biodata->where($rules)->num_rows();
            $rules = array(
                'select'    => null,
                'where'     => array(
                    'jenis_kelamin' => 'P',
                    'YEAR(date_created)' => $tahun
                ),
                'or_where'  => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $num_women = $this->Tbl_pradaftar_biodata->where($rules)->num_rows();
            $rules = array(
                'select'    => null,
                'where'     => array(
                    'status' => '1',
                    'YEAR(date_created)' => $tahun
                ),
                'or_where'  => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $num_lulus = $this->Tbl_pradaftar_kelulusan->where($rules)->num_rows();
            $rules = array(
                'select'    => null,
                'where'     => array(
                    'status' => '0',
                    'YEAR(date_created)' => $tahun
                ),
                'or_where'  => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $num_tidak_lulus = $this->Tbl_pradaftar_kelulusan->where($rules)->num_rows();
            $rules = array(
                'select'    => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $tblSProvinsi = $this->Tbl_setting_provinsi->read($rules)->result();
            foreach ($tblSProvinsi as $value){
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'id_provinsi' => $value->id_provinsi,
                        'YEAR(date_created)' => $tahun
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $provinsi[$value->id_provinsi] = $this->View_pradaftar_biodata->where($rules)->num_rows();
            }
            $rules = array(
                'select'    => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $tblSRumpun = $this->Tbl_setting_rumpun->read($rules)->result();
            foreach ($tblSRumpun as $value){
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'id_rumpun' => $value->id_rumpun,
                        'YEAR(date_created)' => $tahun
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $rumpun[$value->id_rumpun] = $this->Tbl_pradaftar_biodata->where($rules)->num_rows();
            }
            $rules = array(
                'select'    => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $viewSJurusan = $this->View_setting_fakultas_jurusan->read($rules)->result();
            foreach ($viewSJurusan as $value){
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'pilihan' => '1',
                        'kode_jurusan' => $value->kode_jurusan,
                        'YEAR(date_created)' => $tahun
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $pilihan1[$value->kode_jurusan] = $this->Tbl_pradaftar_pilihan->where($rules)->num_rows();
            }
            foreach ($viewSJurusan as $value){
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'pilihan' => '2',
                        'kode_jurusan' => $value->kode_jurusan,
                        'YEAR(date_created)' => $tahun
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $pilihan2[$value->kode_jurusan] = $this->Tbl_pradaftar_pilihan->where($rules)->num_rows();
            }
            foreach ($viewSJurusan as $value){
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'pilihan' => '3',
                        'kode_jurusan' => $value->kode_jurusan,
                        'YEAR(date_created)' => $tahun
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $pilihan3[$value->kode_jurusan] = $this->Tbl_pradaftar_pilihan->where($rules)->num_rows();
            }
            foreach ($viewSJurusan as $value){
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'kode_jurusan' => $value->kode_jurusan,
                        'YEAR(date_created)' => $tahun
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $kelulusan[$value->kode_jurusan] = $this->Tbl_pradaftar_kelulusan->where($rules)->num_rows();
            }
            foreach ($viewSJurusan as $value){
                $jumlah[$value->kode_jurusan] = $pilihan1[$value->kode_jurusan] + $pilihan2[$value->kode_jurusan] + $pilihan3[$value->kode_jurusan];
            }
            $rules = array(
                'select'    => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $tblSTipeUjian = $this->Tbl_setting_tipe_ujian->read($rules)->result();
            foreach ($tblSTipeUjian as $value){
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'id_tipe_ujian' => $value->id_tipe_ujian,
                        'YEAR(date_created)' => $tahun
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $tipe_ujian[$value->id_tipe_ujian] = $this->Tbl_pradaftar_users->where($rules)->num_rows();
            }
            $rules = array(
                'select'    => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $tblSAsalSekolah = $this->Tbl_setting_asal_sekolah->read($rules)->result();
            foreach ($tblSAsalSekolah as $value){
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'id_jenis_asal_sekolah' => $value->id_asal_sekolah,
                        'YEAR(date_created)' => $tahun
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $asal_sekolah[$value->id_asal_sekolah] = $this->View_pradaftar_biodata->where($rules)->num_rows();
            }
        }
        $rules = array(
            'select'    => 'YEAR(date_created) as tahun',
            'where'     => null,
            'order'     => 'tahun DESC',
        );
        $data = array(
            'content'           => 'Pradaftar/statistik/content',
            'css'               => 'Pradaftar/statistik/css',
            'javascript'        => 'Pradaftar/statistik/javascript',
            'modal'             => 'Pradaftar/statistik/modal',
            'kosong'            => $kosong,
            'tahun'             => $this->Tbl_pradaftar_users->distinct($rules)->result(),
            'num_users'         => (!empty($num_users))? $num_users : '0',
            'num_users_sv'      => (!empty($num_users_sv))? $num_users_sv : '0',
            'num_users_bv'      => (!empty($num_users_bv))? $num_users_bv : '0',
            'num_pembayaran'    => (!empty($num_pembayaran))? $num_pembayaran : '0',
            'num_pembayaran_sv' => (!empty($num_pembayaran_sv))? $num_pembayaran_sv : '0',
            'num_pembayaran_bv' => (!empty($num_pembayaran_bv))? $num_pembayaran_bv : '0',
            'num_pembayaran_ua' => (!empty($total))? $total : '0',
            'num_tmp_users'     => (!empty($num_tmp_users))? $num_tmp_users : '0',
            'num_kategori_ipa'  => (!empty($num_kategori_ipa))? $num_kategori_ipa : '0',
            'num_kategori_ips'  => (!empty($num_kategori_ips))? $num_kategori_ips : '0',
            'num_man'           => (!empty($num_man))? $num_man : '0',
            'num_women'         => (!empty($num_women))? $num_women : '0',
            'num_lulus'         => (!empty($num_lulus))? $num_lulus : '0',
            'num_tidak_lulus'   => (!empty($num_tidak_lulus))? $num_tidak_lulus : '0',
            'tblSProvinsi'      => (!empty($tblSProvinsi))? $tblSProvinsi : '',
            'provinsi'          => (!empty($provinsi))? $provinsi : '',
            'tblSRumpun'        => (!empty($tblSRumpun))? $tblSRumpun : '',
            'rumpun'            => (!empty($rumpun))? $rumpun : '',
            'viewSJurusan'	    => (!empty($viewSJurusan))? $viewSJurusan : '',
            'pilihan1'		    => (!empty($pilihan1))? $pilihan1 : '',
            'pilihan2'		    => (!empty($pilihan2))? $pilihan2 : '',
            'pilihan3'		    => (!empty($pilihan3))? $pilihan3 : '',
            'kelulusan'		    => (!empty($kelulusan))? $kelulusan : '',
            'jumlah'		    => (!empty($jumlah))? $jumlah : '',
            'tblSTipeUjian'	    => (!empty($tblSTipeUjian))? $tblSTipeUjian : '',
            'tipe_ujian'	    => (!empty($tipe_ujian))? $tipe_ujian : '',
            'tblSAsalSekolah'	=> (!empty($tblSAsalSekolah))? $tblSAsalSekolah : '',
            'asal_sekolah'	    => (!empty($asal_sekolah))? $asal_sekolah : '',
        );
        $this->load->view('index', $data);
    }

}
