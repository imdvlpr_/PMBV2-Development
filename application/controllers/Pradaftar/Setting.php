<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Setting extends CI_Controller {

    function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != 'DEVELOPMENT') {
            $this->session->set_flashdata('message','Hak akses ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        $this->load->model('Pradaftar/Tbl_pradaftar_setting');
    }

	function index(){
        $rules = array(
            'select'    => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
		$data = array(
            'content'       => 'Pradaftar/setting/content',
            'css'           => 'Pradaftar/setting/css',
            'javascript'    => 'Pradaftar/setting/javascript',
            'modal'         => 'Pradaftar/setting/modal',
		    'tblPSetting'   => $this->Tbl_pradaftar_setting->read($rules)->result(),
		);
		$this->load->view('index',$data);
	}

	function Create(){}

	function Update($id){
        $rules[] = array('field' => 'setting', 'label' => 'Setting', 'rules' => 'required');
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('message',validation_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('Pradaftar/Setting/');
		}else{
		    try{
                $rules = array(
                    'where' => array('id_pradaftar_setting' => $id),
                    'data'  => array(
                        'setting' => $this->input->post('setting'),
                        'updated_by' => $this->session->userdata('id_users'),
                    ),
                );
                $this->Tbl_pradaftar_setting->update($rules);
                $this->session->set_flashdata('message','Data berhasil diubah.');
                $this->session->set_flashdata('type_message','success');
                redirect('Pradaftar/Setting/');
            }catch (Exception $e){
                $this->session->set_flashdata('message', $e->getMessage());
                $this->session->set_flashdata('type_message','danger');
                redirect('Pradaftar/Setting/');
            }
		}
	}

	function Delete($id){}

}

