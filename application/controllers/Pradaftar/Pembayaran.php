<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Pembayaran extends CI_Controller {

	function __construct(){
		parent::__construct();
		if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
		if ($this->session->userdata('level') != 'DEVELOPMENT') {
			$this->session->set_flashdata('message','Hak akses ditolak.');
			$this->session->set_flashdata('type_message','danger');
			redirect('Auth');
		}

		$this->load->model('Pradaftar/Tbl_pradaftar_pembayaran');
        $this->load->model('Pradaftar/Tbl_pradaftar_users');
        $this->load->model('Settings/Tbl_setting_bank');
		$this->load->model('ServerSide/SS_pradaftar_pembayaran', '', TRUE);
	}

	function index(){
        $rules = array(
            'select'    => null,
            'where'     => array(
                'status' => '1',
            ), //not null or null
            'or_where'  => null, //not null or null
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $data = array(
            'content'       => 'Pradaftar/pembayaran/content',
            'css'           => 'Pradaftar/pembayaran/css',
            'javascript'    => 'Pradaftar/pembayaran/javascript',
            'modal'         => 'Pradaftar/pembayaran/modal',
            'tblSBank'      => $this->Tbl_setting_bank->where($rules)->result(),
        );
		$this->load->view('index', $data);
	}

    function Create(){
        $rules[] = array('field' => 'kd_pembayaran', 'label' => 'Kode Pembayaran', 'rules' => 'required');
        $rules[] = array('field' => 'id_bank', 'label' => 'Bank', 'rules' => 'required');
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == FALSE){
            $this->session->set_flashdata('message', validation_errors());
            $this->session->set_flashdata('type_message','danger');
            redirect('Pradaftar/Pembayaran/');
        }else{
            try{
                $kd_pembayaran = $this->input->post('kd_pembayaran');
                $id_bank = $this->input->post('id_bank');
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'kd_pembayaran' => $kd_pembayaran,
                        'id_bank' => $id_bank,
                    ), //not null or null
                    'or_where'  => null, //not null or null
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $num = $this->Tbl_pradaftar_pembayaran->where($rules)->num_rows();
                if ($num > 0){
                    $this->session->set_flashdata('message','Kode Pembayaran sudah ada.');
                    $this->session->set_flashdata('type_message','warning');
                    redirect('Pradaftar/Pembayaran/');
                }else{
                    $data = array(
                        'kd_pembayaran' => $this->input->post('kd_pembayaran'),
                        'uang'          => 250000,
                        'id_bank'       => $this->input->post('id_bank'),
                        'status'        => '0',
                    );
                    $this->Tbl_pradaftar_pembayaran->create($data);
                    $this->session->set_flashdata('message','Data berhasil disimpan.');
                    $this->session->set_flashdata('type_message','success');
                    redirect('Pradaftar/Pembayaran/');
                }
            }catch (Exception $e){
                $this->session->set_flashdata('message', $e->getMessage());
                $this->session->set_flashdata('type_message','danger');
                redirect('Pradaftar/Pembayaran/');
            }
        }
    }

	function Delete($id){
	    try{
            $rules = array(
                'select'    => null,
                'where'     => array(
                    'id_pradaftar_pembayaran' => $id,
                ), //not null or null
                'or_where'  => null, //not null or null
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $data = $this->Tbl_pradaftar_pembayaran->where($rules)->row();
            $rules = array(
                'where' => array('kd_pembayaran' => $data->kd_pembayaran),
                'data'  => array(
                    'pembayaran' => "0"
                ),
            );
            $this->Tbl_pradaftar_users->update($rules);
            $rules = array('id_pradaftar_pembayaran' => $id);
            $this->Tbl_pradaftar_pembayaran->delete($rules);
            $this->session->set_flashdata('message','Data berhasil dihapus.');
            $this->session->set_flashdata('type_message','success');
            redirect('Pradaftar/Pembayaran/');
        }catch (Exception $e){
            $this->session->set_flashdata('message', $e->getMessage());
            $this->session->set_flashdata('type_message','danger');
            redirect('Pradaftar/Pembayaran/');
        }
	}

	function Json(){
		$fetch_data = $this->SS_pradaftar_pembayaran->make_datatables();
		$data = array();
		foreach($fetch_data as $row){
			$sub_array = array();
            $sub_array[] = "
                <div class=\"btn-group\">
                    <button type=\"button\" class=\"btn btn-default dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                        <i class='fa fa-gear'></i> <span class=\"caret\"></span>
                    </button>
                    <ul class=\"dropdown-menu\">
                        <li><a href=\"".base_url('Pradaftar/Pembayaran/Delete/'.$row->id_pradaftar_pembayaran)."\" onclick=\"return confirm('Apakah Anda Yakin ?')\">Delete</i></a></li>
                        <li role=\"separator\" class=\"divider\"></li>
                    </ul>
                </div>";
			$sub_array[] = $row->kd_pembayaran;
			$sub_array[] = $row->nama;
			$sub_array[] = 'Rp '.number_format($row->uang, 2);
            $sub_array[] = $row->bank;
            $sub_array[] = ($row->status == 1) ? "<span class=\"label label-success\">Sudah Verifikasi</span>" : "<span class=\"label label-danger\">Belum Verifikasi</span>";
            $sub_array[] = $row->date_created;
            $sub_array[] = $row->date_updated;
            $data[] = $sub_array;
		}
		$output = array(
			"draw"				=>	intval($_POST["draw"]),
			"recordsTotal"		=>	$this->SS_pradaftar_pembayaran->get_all_data(),
			"recordsFiltered"	=>	$this->SS_pradaftar_pembayaran->get_filtered_data(),
			"data"				=>	$data
		);
		echo json_encode($output);
	}
}
