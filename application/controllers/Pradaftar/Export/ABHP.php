<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class ABHP extends CI_Controller {

	function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != 'DEVELOPMENT') {
            $this->session->set_flashdata('message','Hak akses ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        $this->load->model('Settings/Tbl_setting_tipe_ujian');
        $this->load->model('Views/Pradaftar/View_pradaftar_jadwal');
        $this->load->model('Views/Pradaftar/View_pradaftar_pilihan');
        $this->load->model('Views/Pradaftar/View_pradaftar_biodata');
        $this->load->model('Views/Pradaftar/View_pradaftar_users');
    }

    function index(){
        $rules = array(
            'select'    => 'YEAR(date_created) as tahun',
            'where'     => null,
            'order'     => 'tahun DESC',
        );
        $data = array(
            'content'       => 'Pradaftar/Export/abhp/content',
            'css'           => 'Pradaftar/Export/abhp/css',
            'javascript'    => 'Pradaftar/Export/abhp/javascript',
            'modal'         => 'Pradaftar/Export/abhp/modal',
            'tahun'         => $this->View_pradaftar_jadwal->distinct($rules)->result(),
        );
        $this->load->view('index', $data);
    }

    function listTipeUjian(){
        $tahun = $this->input->post('tahun');
        $rules = array(
            'select'    => 'id_tipe_ujian, tipe_ujian',
            'where'     => array(
                'YEAR(date_created)' => $tahun,
            ),
            'order'     => 'id_tipe_ujian ASC',
        );
        $viewPUjian = $this->View_pradaftar_jadwal->distinct($rules)->result();
        $lists = "<option value=''>Pilih</option>";
        foreach($viewPUjian as $data){
            $lists .= "<option value='".$data->id_tipe_ujian."'>".$data->tipe_ujian."</option>";
        }
        $callback = array('list_tipe_ujian' => $lists);
        echo json_encode($callback);
    }

    function listTanggal(){
        $tahun = $this->input->post('tahun');
        $tipe = $this->input->post('tipe');
        $rules = array(
            'select'    => 'tanggal',
            'where'     => array(
                'id_tipe_ujian' => $tipe,
                'YEAR(date_created)' => $tahun,
            ),
            'order'     => 'tanggal ASC',
        );
		$viewPUjian = $this->View_pradaftar_jadwal->distinct($rules)->result();
        $lists = "<option value=''>Pilih</option>";
        foreach($viewPUjian as $data){
        	$lists .= "<option value='".$data->tanggal."'>".$data->tanggal."</option>";
        }
        $callback = array('list_tanggal' => $lists);
        echo json_encode($callback);
    }

    function listTempat(){
		$tahun = $this->input->post('tahun');
		$tipe = $this->input->post('tipe');
		$tanggal = $this->input->post('tanggal');
        $rules = array(
            'select'    => 'kampus',
            'where'     => array(
                'date(tanggal)' => $tanggal,
                'id_tipe_ujian' => $tipe,
                'YEAR(date_created)' => $tahun,
            ),
            'order'     => 'kampus ASC',
        );
		$viewPUjian = $this->View_pradaftar_jadwal->distinct($rules)->result();
		$lists = "<option value=''>Pilih</option>";
		foreach($viewPUjian as $data){
        	$lists .= "<option value='".$data->kampus."'>".$data->kampus."</option>";
        }
        $callback = array('list_tempat' => $lists);
        echo json_encode($callback);
    }

    function listGedung(){
		$tahun = $this->input->post('tahun');
        $tipe = $this->input->post('tipe');
        $kampus = $this->input->post('kampus');
		$tanggal = $this->input->post('tanggal');
        $rules = array(
            'select'    => 'gedung',
            'where'     => array(
                'date(tanggal)' => $tanggal,
                'id_tipe_ujian' => $tipe,
                'kampus' => $kampus,
                'YEAR(date_created)' => $tahun,
            ),
            'order'     => 'gedung ASC',
        );
		$viewPUjian = $this->View_pradaftar_jadwal->distinct($rules)->result();
		$lists = "<option value=''>Pilih</option>";
		foreach($viewPUjian as $data){
        	$lists .= "<option value='".$data->gedung."'>".$data->gedung."</option>";
        }
        $callback = array('list_gedung' => $lists);
        echo json_encode($callback);
    }

    function listRuangan(){
		$tahun = $this->input->post('tahun');
        $tipe = $this->input->post('tipe');
        $kampus = $this->input->post('kampus');
        $gedung = $this->input->post('gedung');
		$tanggal = $this->input->post('tanggal');
        $rules = array(
            'select'    => 'ruangan',
            'where'     => array(
                'date(tanggal)' => $tanggal,
                'id_tipe_ujian' => $tipe,
                'kampus' => $kampus,
                'gedung' => $gedung,
                'YEAR(date_created)' => $tahun,
            ),
            'order'     => 'ruangan ASC',
        );
		$viewPUjian = $this->View_pradaftar_jadwal->distinct($rules)->result();
		$lists = "<option value=''>Pilih</option>";
		foreach($viewPUjian as $data){
        	$lists .= "<option value='".$data->ruangan."'>".$data->ruangan."</option>";
        }
        $callback = array('list_ruangan' => $lists);
        echo json_encode($callback);
    }

    function listSesi(){
        $tahun = $this->input->post('tahun');
        $tipe = $this->input->post('tipe');
		$tanggal = $this->input->post('tanggal');
        $kampus = $this->input->post('kampus');
        $gedung = $this->input->post('gedung');
        $ruangan = $this->input->post('ruangan');
        $rules = array(
            'select'    => 'sesi',
            'where'     => array(
                'date(tanggal)' => $tanggal,
                'id_tipe_ujian' => $tipe,
                'kampus' => $kampus,
                'gedung' => $gedung,
                'ruangan' => $ruangan,
                'YEAR(date_created)' => $tahun,
            ),
            'order'     => 'sesi ASC',
        );
		$viewPUjian = $this->View_pradaftar_jadwal->distinct($rules)->result();
		$lists = "<option value=''>Pilih</option>";
		foreach($viewPUjian as $data){
        	$lists .= "<option value='".$data->sesi."'>".$data->sesi."</option>";
        }
        $callback = array('list_sesi' => $lists);
        echo json_encode($callback);
    }

    function Cetak(){
		$this->load->library('m_pdf');
		$pdf = $this->m_pdf->load();
		$tahun = $this->input->post('tahun');
		$id_tipe_ujian = $this->input->post('tipe');
		$tanggal = $this->input->post('tanggal');
        $kampus = $this->input->post('kampus');
        $gedung = $this->input->post('gedung');
        $ruangan = $this->input->post('ruangan');
		$sesi = $this->input->post('sesi');

        $rules = array(
            'select'    => null,
            'where'     => array(
                'id_tipe_ujian' => $id_tipe_ujian,
            ),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
		$tblSTipeUjian = $this->Tbl_setting_tipe_ujian->where($rules)->row();

		$file['tipe_ujian'] = str_replace(array(' ','-','\'','`','.'),'_',$tblSTipeUjian->tipe_ujian);
        $file['tanggal'] = str_replace(array(' ','-','\'','`','.'),'_',$tanggal);
        $file['kampus'] = str_replace(array(' ','-','\'','`','.'),'_',$kampus);
        $file['gedung'] = str_replace(array(' ','-','\'','`','.'),'_',$gedung);
        $file['ruangan'] = str_replace(array(' ','-','\'','`','.'),'_',$ruangan);
        $file['sesi'] = str_replace(array(' ','-','\'','`','.'),'_',$sesi);

        $rules = array(
            'select'    => null,
            'where'     => array(
                'id_tipe_ujian' => $id_tipe_ujian,
                'tanggal' => $tanggal,
                'kampus' => $kampus,
                'gedung' => $gedung,
                'ruangan' => $ruangan,
                'sesi' => $sesi,
                //'verifikasi_biodata' => 'SUDAH VERIFIKASI',
                'YEAR(date_created)' => $tahun,
            ),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );

		if ($this->input->post('berkas') == 1){
			$data = array(
				'tipe_ujian' => $tblSTipeUjian->tipe_ujian,
				'tanggal' => $tanggal,
                'kampus' => $kampus,
                'gedung' => $gedung,
                'ruangan' => $ruangan,
				'sesi' => $sesi,
				'waktu' => $this->View_pradaftar_jadwal->where($rules)->row(),
				'jadwal' => $this->View_pradaftar_jadwal->where($rules)->result(),
			);
			if($id_tipe_ujian == 1){
				$pdf->AddPage('L');
				$html = $this->load->view('Pradaftar/Export/Print/print_abhp_cbt', $data, true);
			}else{
				$html = $this->load->view('Pradaftar/Export/Print/print_abhp', $data, true);
			}
			$pdfFilePath = "ABHP_".$file['tipe_ujian'].'_'.$file['tanggal'].'_'.$file['tempat'].'_'.$file['sesi'].".pdf";
		}else{
			$data = array(
				'tipe_ujian' => $tblSTipeUjian->tipe_ujian,
				'tanggal' => $tanggal,
                'kampus' => $kampus,
                'gedung' => $gedung,
                'ruangan' => $ruangan,
				'sesi' => $sesi,
				'jadwal' => $this->View_pradaftar_jadwal->where($rules)->row(),
			);
			$html = $this->load->view('Pradaftar/Export/Print/print_berita_acara', $data, true);
			$pdfFilePath = "Berita_Acara_".$file['tipe_ujian'].'_'.$file['tanggal'].'_'.$file['tempat'].'_'.$file['sesi'].".pdf";
		}

		//$pdf->debug = true;
		//$pdf->showImageErrors = true;
		$pdf->WriteHTML($html);
		$pdf->Output($pdfFilePath, "I");
    }

}

