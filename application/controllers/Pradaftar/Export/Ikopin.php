<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Ikopin extends CI_Controller {
	
    function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != 'DEVELOPMENT') {
            $this->session->set_flashdata('message','Hak akses ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
		$this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
		$this->load->model('Views/Pradaftar/View_pradaftar_biodata');
		$this->load->model('Views/Pradaftar/View_pradaftar_orangtua');
		$this->load->model('Views/Pradaftar/View_pradaftar_users');
		$this->load->model('Views/Pradaftar/View_pradaftar_jadwal');
		$this->load->model('Views/Pradaftar/View_pradaftar_kelulusan');
		$this->load->model('Views/Pradaftar/View_pradaftar_pilihan');
    }

    function index(){
        $rules = array(
            'select'    => 'YEAR(date_created) as tahun',
            'where'     => null,
            'order'     => 'tahun DESC',
		);
		$rules2 = array(
            'select'    => 'kode_jurusan, jurusan',
            'where'     => null,
            'order'     => 'jurusan ASC',
        );
        $data = array(
            'content'       => 'Pradaftar/Export/ikopin/content',
            'css'           => 'Pradaftar/Export/ikopin/css',
            'javascript'    => 'Pradaftar/Export/ikopin/javascript',
            'modal'         => 'Pradaftar/Export/ikopin/modal',
			'tahun'         => $this->View_pradaftar_kelulusan->distinct($rules)->result(),
			'jurusan'         => $this->View_pradaftar_pilihan->distinct($rules2)->result(),
        );
        $this->load->view('index', $data);
	}
	
    function Export(){
		$tahun = $this->input->post('tahun');
		$jurusan = $this->input->post('jurusan');
        $rules = array(
            'select'    => null,
            'where'     => array(
				'YEAR(date_created)' => $tahun,
				'pembayaran' => '1',
				'biodata'	=> '1'
            ),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
    	$viewPUsers = $this->View_pradaftar_users->where($rules);
    	if ($viewPUsers->num_rows() > 0){
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()
				->setCreator("Piscal Pratama Putra") //creator
				->setTitle("Export data Pilihan");  //file title
			$objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
			$objget = $objPHPExcel->getActiveSheet();  //inisiasi get object
			//$objget->setTitle('Sample Sheet'); //sheet title
			//Warna header tabel
			/*$objget->getStyle("A1:C1")->applyFromArray(
				array(
					'fill' => array(
						'type' => PHPExcel_Style_Fill::FILL_SOLID,
						'color' => array('rgb' => '92d050')
					),
					'font' => array(
						'color' => array('rgb' => '000000')
					)
				)
			);*/
			//table header
			$cols = array("A","B","C","D","E","F","G","H","I");
			$val = array(
                "No.","Nama","Jurusan 1", "Jenis Kelamin","NIK","Asal Sekolah","Alamat Rumah","Kode POS","No. HP"
			);
			for ($a = 0; $a < 9; $a++) {
				$objset->setCellValue($cols[$a].'1', $val[$a]);
				//Setting lebar cell
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
				$style = array(
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					)
				);
				$objPHPExcel->getActiveSheet()->getStyle($cols[$a].'1')->applyFromArray($style);
			}
			$baris  = 2;
			$no = 1;
    		foreach ($viewPUsers->result() as $value){
				$rules = array(
					'select'    => null,
					'where'     => array(
						'nomor_peserta' => $value->nomor_peserta
					),
					'or_where'  => null,
					'order'     => null,
					'limit'     => null,
					'pagging'   => null,
				);
				$viewPKelulusan = $this->View_pradaftar_kelulusan->where($rules)->row();


				$rules = array(
					'select'    => null,
					'where'     => array(
						'id_pradaftar_users' => $value->id_pradaftar_users
					),
					'or_where'  => null,
					'order'     => null,
					'limit'     => null,
					'pagging'   => null,
				);
				$viewPBiodata = $this->View_pradaftar_biodata->where($rules)->row();
				
				if($viewPKelulusan->status == '1'){
					continue;
				}

				if($jurusan == "semua"){
					$rules = array(
						'select'    => null,
						'where'     => array(
							'nomor_peserta' => $value->nomor_peserta,
							'pilihan'	=> '1'
						),
						'or_where'  => null,
						'order'     => null,
						'limit'     => null,
						'pagging'   => null,
					);
				}else{
					$rules = array(
						'select'    => null,
						'where'     => array(
							'nomor_peserta' => $value->nomor_peserta,
							'pilihan'	=> '1',
							'kode_jurusan' => $jurusan
						),
						'or_where'  => null,
						'order'     => null,
						'limit'     => null,
						'pagging'   => null,
					);
				}

				$viewPPilihan1 = $this->View_pradaftar_pilihan->where($rules);
				if($viewPPilihan1->num_rows() == 0){
					continue;
				}else{
					$viewPPilihan1 = $viewPPilihan1->row();
				}
				
				// if($viewPPilihan1->jurusan != "EKONOMI SYARI`AH"){
				// 	if($viewPPilihan1->jurusan != "AKUNTANSI SYARI`AH"){
				// 		if($viewPPilihan1->jurusan != "MANAJEMEN KEUANGAN SYARI`AH"){
				// 			if($viewPPilihan1->jurusan != "MANAJEMEN"){
				// 				continue;
				// 			}
				// 		}
				// 	}
				// }

				//pemanggilan sesuaikan dengan nama kolom tabel
                $objset->setCellValue("A".$baris, $no++);
                $objset->setCellValue("B".$baris, $value->nama);
                $objset->setCellValue("C".$baris, $viewPPilihan1->jurusan);
                $objset->setCellValue("D".$baris, $viewPBiodata->jenis_kelamin);
                $objset->setCellValue("E".$baris, '`'.$value->nik_passport.'`');
                $objset->setCellValue("F".$baris, $viewPBiodata->asal_sekolah);
                $objset->setCellValue("G".$baris, $viewPBiodata->alamat.", ".$viewPBiodata->kelurahan.", ".$viewPBiodata->kecamatan.", ".$viewPBiodata->kabupaten.", ".$viewPBiodata->provinsi);
				$objset->setCellValue("H".$baris, $viewPBiodata->kodepos);
				$objset->setCellValue("I".$baris, '`'.$value->nmr_tlp.'`');

                //Set number value
                //$objPHPExcel->getActiveSheet()->getStyle('C1:C'.$baris)->getNumberFormat()->setFormatCode('0');

                $baris++;
                $no++;
			}
			$objPHPExcel->getActiveSheet()->setTitle('Data Export');

			$objPHPExcel->setActiveSheetIndex(0);
			$filename = urlencode("Ikopin_".$tahun."_".date("Y_m_d_H_i_s").".xls");
			header('Content-Type: application/vnd.ms-excel'); //mime type
			header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
			header('Cache-Control: max-age=0'); //no cache
			$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save('php://output');
		}else{
			$this->session->set_flashdata('message','Data kosong.');
			$this->session->set_flashdata('type_message','danger');
			redirect('Pradaftar/Export/Ikopin/');
		}
    }

}
