<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Jadwal extends CI_Controller {
	
    function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != 'DEVELOPMENT') {
            $this->session->set_flashdata('message','Hak akses ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
		$this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
		$this->load->model('Pradaftar/Tbl_pradaftar_users');
		$this->load->model('Views/Pradaftar/View_pradaftar_jadwal');
		$this->load->model('Views/Pradaftar/View_pradaftar_pilihan');
    }

    function index(){
        $rules = array(
            'select'    => 'tanggal',
            'where'     => null,
            'order'     => 'tanggal DESC',
        );
    	$data = array(
            'content'       => 'Pradaftar/Export/jadwal/content',
            'css'           => 'Pradaftar/Export/jadwal/css',
            'javascript'    => 'Pradaftar/Export/jadwal/javascript',
            'modal'         => 'Pradaftar/Export/jadwal/modal',
    		'tanggal'       => $this->View_pradaftar_jadwal->distinct($rules)->result(),
		);
    	$this->load->view('index',$data);
	}
	
    function Export(){
    	$tanggal = $this->input->post('tanggal');
        $rules = array(
            'select'    => null,
            'where'     => array(
                'date(tanggal)' => $tanggal
            ),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
    	$viewPUjian = $this->View_pradaftar_jadwal->where($rules);
    	if ($viewPUjian->num_rows() > 0){
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()
				->setCreator("Alfi Gusman") //creator
				->setTitle("Export data Jadwal");  //file title
			$objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
			$objget = $objPHPExcel->getActiveSheet();  //inisiasi get object
			//table header
			$cols = array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V");
			$val = array("No","Nomor Peserta","NIK(No.KTP)","Nama","Tanggal Lahir","Jurusan 1","Fakultas 1","Jurusan 2","Fakultas 2","Jurusan 3","Fakultas 3","Tanggal","Kampus","Gedung","Ruangan","Sesi","Jam Awal","Jam Akhir","Tipe Ujian","Kategori","Verifikasi Biodata","Nomor Telepon");
			for ($a = 0; $a < 22; $a++) {
				$objset->setCellValue($cols[$a].'1', $val[$a]);
				//Setting lebar cell
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setAutoSize(true);
				$style = array(
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					)
				);
				$objPHPExcel->getActiveSheet()->getStyle($cols[$a].'1')->applyFromArray($style);
			}
			$baris  = 2;
			$no = 1;
    		foreach ($viewPUjian->result() as $value){
				//pemanggilan sesuaikan dengan nama kolom tabel
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'nomor_peserta' => $value->nomor_peserta
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
				$tbPUsers = $this->Tbl_pradaftar_users->where($rules)->row();
				$rules = array(
                    'select'    => null,
                    'where'     => array(
						'nomor_peserta' => $value->nomor_peserta,
						'pilihan'		=> '1'
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
				$tbPPilihan1 = $this->View_pradaftar_pilihan->where($rules)->row();
				$rules = array(
                    'select'    => null,
                    'where'     => array(
						'nomor_peserta' => $value->nomor_peserta,
						'pilihan'		=> '2'
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
				$tbPPilihan2 = $this->View_pradaftar_pilihan->where($rules)->row();
				$rules = array(
                    'select'    => null,
                    'where'     => array(
						'nomor_peserta' => $value->nomor_peserta,
						'pilihan'		=> '3'
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $tbPPilihan3 = $this->View_pradaftar_pilihan->where($rules)->row();
                
                if($tbPUsers->kategori == '1'){
                    $kategori = "IPA";
                }else{
                    $kategori = "IPS";
                }

                if($tbPUsers->biodata == "1"){
                    $biodata = "SUDAH VERIFIKASI";
                }else{
                    $biodata = "BELUM VERIFIKASI";
                }
                $objset->setCellValue("A".$baris, $no);
                $objset->setCellValue("B".$baris, $value->nomor_peserta);
                $objset->setCellValue("C".$baris, "`".$tbPUsers->nik_passport."`");
                $objset->setCellValue("D".$baris, $tbPUsers->nama);
                $objset->setCellValue("E".$baris, $tbPUsers->tgl_lhr);
                $objset->setCellValue("F".$baris, $tbPPilihan1->jurusan);
                $objset->setCellValue("G".$baris, $tbPPilihan1->fakultas);
                $objset->setCellValue("H".$baris, $tbPPilihan2->jurusan);
                $objset->setCellValue("I".$baris, $tbPPilihan2->fakultas);
                $objset->setCellValue("J".$baris, $tbPPilihan3->jurusan);
                $objset->setCellValue("K".$baris, $tbPPilihan3->fakultas);
                $objset->setCellValue("L".$baris, $value->tanggal);
				$objset->setCellValue("M".$baris, $value->kampus);
				$objset->setCellValue("N".$baris, $value->gedung);
				$objset->setCellValue("O".$baris, $value->ruangan);
                $objset->setCellValue("P".$baris, $value->sesi);
                $objset->setCellValue("Q".$baris, $value->jam_awal);
                $objset->setCellValue("R".$baris, $value->jam_akhir);
                $objset->setCellValue("S".$baris, $value->tipe_ujian);
                $objset->setCellValue("T".$baris, $kategori);
                $objset->setCellValue("U".$baris, $biodata);
				$objset->setCellValue("V".$baris, "`".$tbPUsers->nmr_tlp."`");

                //Set number value
                //$objPHPExcel->getActiveSheet()->getStyle('C1:C'.$baris)->getNumberFormat()->setFormatCode('0');

                $baris++;
                $no++;
			}
			$objPHPExcel->getActiveSheet()->setTitle('Data Export');

			$objPHPExcel->setActiveSheetIndex(0);
			$filename = urlencode("Jadwal_".$tanggal."_".date("Y_m_d_H_i_s").".xls");
			header('Content-Type: application/vnd.ms-excel'); //mime type
			header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
			header('Cache-Control: max-age=0'); //no cache
			$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save('php://output');
		}else{
			$this->session->set_flashdata('message','Data kosong.');
			$this->session->set_flashdata('type_message','danger');
			redirect('Pradaftar/Export/Jadwal/');
		}
    }

}
