<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Akademik extends CI_Controller {
	
    function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != 'DEVELOPMENT') {
            $this->session->set_flashdata('message','Hak akses ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
		$this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
		$this->load->model('Views/Pradaftar/View_pradaftar_biodata');
		$this->load->model('Views/Pradaftar/View_pradaftar_orangtua');
		$this->load->model('Views/Pradaftar/View_pradaftar_users');
        $this->load->model('Views/Pradaftar/View_pradaftar_jadwal');
    }

    function index(){
        $rules = array(
            'select'    => 'YEAR(date_created) as tahun',
            'where'     => null,
            'order'     => 'tahun DESC',
        );
        $data = array(
            'content'       => 'Pradaftar/Export/akademik/content',
            'css'           => 'Pradaftar/Export/akademik/css',
            'javascript'    => 'Pradaftar/Export/akademik/javascript',
            'modal'         => 'Pradaftar/Export/akademik/modal',
            'tahun'         => $this->View_pradaftar_biodata->distinct($rules)->result(),
        );
        $this->load->view('index', $data);
	}
	
    function Export(){
    	$tahun = $this->input->post('tahun');
        $rules = array(
            'select'    => null,
            'where'     => array(
                'YEAR(date_created)' => $tahun
            ),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
    	$viewPBiodata = $this->View_pradaftar_biodata->where($rules);
    	if ($viewPBiodata->num_rows() > 0){
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()
				->setCreator("Alfi Gusman") //creator
				->setTitle("Export data Akademik");  //file title
			$objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
			$objget = $objPHPExcel->getActiveSheet();  //inisiasi get object
			//$objget->setTitle('Sample Sheet'); //sheet title
			//Warna header tabel
			/*$objget->getStyle("A1:C1")->applyFromArray(
				array(
					'fill' => array(
						'type' => PHPExcel_Style_Fill::FILL_SOLID,
						'color' => array('rgb' => '92d050')
					),
					'font' => array(
						'color' => array('rgb' => '000000')
					)
				)
			);*/
			//table header
			$cols = array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X");
			$val = array(
                "NIK(No.KTP)","Nomor Peserta","Nama", "Tempat Lahir","Tanggal Lahir","Jenis Kelamin","Warga Negara","Agama","Provinsi",
				"Kabupaten / Kota","Kecamatan","Kelurahan","Kode Pos","Telepon","Nama Ayah","Nama Ibu","Penghasilan Ayah",
				"Penghasilan Ibu","Pekerjaan Ayah","Pekerjaan Ibu","Pendidikan Ayah","Pendidikan Ibu","Asal Sekolah","Rumpun"
			);
			for ($a = 0; $a < 24; $a++) {
				$objset->setCellValue($cols[$a].'1', $val[$a]);
				//Setting lebar cell
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('X')->setAutoSize(true);
				$style = array(
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					)
				);
				$objPHPExcel->getActiveSheet()->getStyle($cols[$a].'1')->applyFromArray($style);
			}
			$baris  = 2;
			$no = 1;
    		foreach ($viewPBiodata->result() as $value){
				$rules = array(
					'select'    => null,
					'where'     => array(
						'id_pradaftar_users' => $value->id_pradaftar_users
					),
					'or_where'  => null,
					'order'     => null,
					'limit'     => null,
					'pagging'   => null,
				);
				$viewPUSers = $this->View_pradaftar_users->where($rules)->row();

				$rules = array(
					'select'    => null,
					'where'     => array(
						'id_pradaftar_users' => $value->id_pradaftar_users,
						'orangtua'			=> 'Ayah'
					),
					'or_where'  => null,
					'order'     => null,
					'limit'     => null,
					'pagging'   => null,
				);
				$viewPOrangtuaAyah = $this->View_pradaftar_orangtua->where($rules)->row();

				$rules = array(
					'select'    => null,
					'where'     => array(
						'id_pradaftar_users' => $value->id_pradaftar_users,
						'orangtua'			=> 'Ibu'
					),
					'or_where'  => null,
					'order'     => null,
					'limit'     => null,
					'pagging'   => null,
				);
				$viewPOrangtuaIbu = $this->View_pradaftar_orangtua->where($rules)->row();
				//pemanggilan sesuaikan dengan nama kolom tabel
                $objset->setCellValue("A".$baris, $viewPUSers->nik_passport);
                $objset->setCellValue("B".$baris, $viewPUSers->nomor_peserta);
                $objset->setCellValue("C".$baris, $value->nama);
                $objset->setCellValue("D".$baris, $value->tempat);
                $objset->setCellValue("E".$baris, $viewPUSers->tgl_lhr);
                $objset->setCellValue("F".$baris, $value->jenis_kelamin);
                $objset->setCellValue("G".$baris, $value->warga_negara);
                $objset->setCellValue("H".$baris, $value->agama);
                $objset->setCellValue("I".$baris, $value->provinsi);
                $objset->setCellValue("J".$baris, $value->kabupaten);
                $objset->setCellValue("K".$baris, $value->kecamatan);
                $objset->setCellValue("L".$baris, $value->kelurahan);
                $objset->setCellValue("M".$baris, $value->kodepos);
                $objset->setCellValue("N".$baris, $viewPUSers->nmr_tlp);
                $objset->setCellValue("O".$baris, $viewPOrangtuaAyah->nama_orangtua);
                $objset->setCellValue("P".$baris, $viewPOrangtuaIbu->nama_orangtua);
                $objset->setCellValue("Q".$baris, $viewPOrangtuaAyah->penghasilan);
                $objset->setCellValue("R".$baris, $viewPOrangtuaIbu->penghasilan);
                $objset->setCellValue("S".$baris, $viewPOrangtuaAyah->pekerjaan);
                $objset->setCellValue("T".$baris, $viewPOrangtuaIbu->pekerjaan);
                $objset->setCellValue("U".$baris, $viewPOrangtuaAyah->pendidikan);
                $objset->setCellValue("V".$baris, $viewPOrangtuaIbu->pendidikan);
                $objset->setCellValue("W".$baris, $value->asal_sekolah);
                $objset->setCellValue("X".$baris, $value->rumpun);

                //Set number value
                //$objPHPExcel->getActiveSheet()->getStyle('C1:C'.$baris)->getNumberFormat()->setFormatCode('0');

                $baris++;
                $no++;
			}
			$objPHPExcel->getActiveSheet()->setTitle('Data Export');

			$objPHPExcel->setActiveSheetIndex(0);
			$filename = urlencode("Akademik_".$tahun."_".date("Y_m_d_H_i_s").".xls");
			header('Content-Type: application/vnd.ms-excel'); //mime type
			header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
			header('Cache-Control: max-age=0'); //no cache
			$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save('php://output');
		}else{
			$this->session->set_flashdata('message','Data kosong.');
			$this->session->set_flashdata('type_message','danger');
			redirect('Pradaftar/Export/Akademik/');
		}
    }

}
