<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Pembayaran extends CI_Controller {

	function __construct(){
        parent::__construct();
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != 'DEVELOPMENT') {
            $this->session->set_flashdata('message','Hak akses ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
		$this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
		$this->load->model('Pradaftar/Tbl_pradaftar_pembayaran');
		$this->load->model('Pradaftar/Tbl_pradaftar_setting');
	}

	function index(){
        $data = array(
            'content'       => 'Pradaftar/Import/pembayaran/content',
            'css'           => 'Pradaftar/Import/pembayaran/css',
            'javascript'    => 'Pradaftar/Import/pembayaran/javascript',
            'modal'         => 'Pradaftar/Import/pembayaran/modal',
        );
        $this->load->view('index', $data);
	}

	function Import(){
		$config = array(
			'upload_path'   => './import/pradaftar/',
			'allowed_types' => 'xls|xlsx|csv|ods|ots',
			'max_size'      => 51200,
			'overwrite'     => TRUE,
			'file_name'     => 'Pembayaran_'.date('H i s d m Y'),
		);
		$this->upload->initialize($config);
		if(!$this->upload->do_upload()){
			$this->session->set_flashdata('message',$this->upload->display_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('Pradaftar/Import/Pembayaran/');
		}else {
			$file = $this->upload->data();
			$inputFileName = 'import/pradaftar/'.$file['file_name'];
			try {
				$inputFileType = IOFactory::identify($inputFileName);
				$objReader = IOFactory::createReader($inputFileType);
				$objPHPExcel = $objReader->load($inputFileName);
			} catch (Exception $e) {
				die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '" : ' . $e->getMessage());
			}
			$sheet = $objPHPExcel->getSheet(0);
			$highestRow = $sheet->getHighestRow();
			try{
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'id_pradaftar_setting' => 6,
                    ), //not null or null
                    'or_where'  => null, //not null or null
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $tblPSetting = $this->Tbl_pradaftar_setting->where($rules)->row();
				for ($row = 2; $row <= $highestRow; $row++) {
                    $kd_pembayaran = $sheet->getCellByColumnAndRow(0, $row)->getValue();
				    $id_bank = $sheet->getCellByColumnAndRow(1, $row)->getValue();
                    $rules = array(
                        'select'    => null,
                        'where'     => array(
                            'kd_pembayaran' => $kd_pembayaran,
                            'id_bank' => $id_bank,
                        ), //not null or null
                        'or_where'  => null, //not null or null
                        'order'     => null,
                        'limit'     => null,
                        'pagging'   => null,
                    );
					$num = $this->Tbl_pradaftar_pembayaran->where($rules)->num_rows();
					if ($num == 0){
						$data = array(
							'kd_pembayaran' => $kd_pembayaran,
							'id_bank' => $id_bank,
							'uang' => $tblPSetting->setting,
							'status' => '0',
						);
						$this->Tbl_pradaftar_pembayaran->create($data);
					}
				}
				$this->session->set_flashdata('message', 'Import berhasil.');
				$this->session->set_flashdata('type_message', 'success');
				redirect('Pradaftar/Import/Pembayaran/');
			}catch (Exception $e){
				$this->session->set_flashdata('message', $e->getMessage());
				$this->session->set_flashdata('type_message', 'danger');
				redirect('Pradaftar/Import/Pembayaran/');
			}
		}
	}
}