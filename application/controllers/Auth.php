<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Auth extends CI_Controller {
	
	function __construct(){
        parent::__construct();
        $this->load->model('Settings/Tbl_setting_users');
    }

	function index(){
		$this->Login();
	}

	function Login(){
		$this->load->view('login');	
	}

	function actionLogin(){
	    $rules[] = array('field' => 'username',	'label' => 'Username', 'rules' => 'required');
	    $rules[] = array('field' => 'password', 'label' => 'Password', 'rules' => 'required');
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('message', validation_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('Auth');
		}else{
            $rules = array(
                'select'    => null,
                'where'     => array(
                    'username' => $this->input->post('username'),
                    'password' => md5(md5($this->input->post('password'))),
                ),
                'or_where'  => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
			$tblSUsers = $this->Tbl_setting_users->where($rules);
			if ($tblSUsers->num_rows() > 0) {
			    try{
                    $tblSUsers = $tblSUsers->row();
                    $rules = array(
                        'where' => array('id_users' => $tblSUsers->id_users),
                        'data'  => array(
                            'login' 	=> date('Y-m-d H:i:s'),
                            'ip_login' 	=> $this->getIPAddress(),
                        ),
                    );
                    $this->Tbl_setting_users->update($rules);
                    $session = array(
                        'development'	=> TRUE,
                        'id_users'		=> $tblSUsers->id_users,
                        'username'		=> $tblSUsers->username,
                        'nama'			=> $tblSUsers->nama,
                        'level'			=> $tblSUsers->level,
                        'login' 		=> $tblSUsers->login,
                        'logout' 		=> $tblSUsers->logout,
                        'ip_login' 		=> $this->getIPAddress(),
                        'ip_logout' 	=> $tblSUsers->ip_logout,
                    );
                    $this->session->set_userdata($session);
                    redirect('Dashboard');
                }catch (Exception $e){
                    $this->session->set_flashdata('message', $e->getMessage());
                    $this->session->set_flashdata('type_message','danger');
                    redirect('Auth');
                }
			}else{
				$this->session->set_flashdata('message','Username atau Password Salah');
				$this->session->set_flashdata('type_message','danger');
				redirect('Auth');
			}
		}
	}
	
	function Logout(){
	    try{
            $rules = array(
                'where' => array('id_users' => $this->session->userdata('id_users')),
                'data'  => array(
                    'logout' 	=> date('Y-m-d H:i:s'),
                    'ip_logout' => $this->getIPAddress(),
                ),
            );
            $this->Tbl_setting_users->update($rules);
            $this->session->sess_destroy();
            redirect('Auth');
        }catch (Exception $e){
            $this->session->set_flashdata('message', $e->getMessage());
            $this->session->set_flashdata('type_message','danger');
            redirect('Dashboard');
        }
	}

	function getIPAddress(){
		$ipaddress = '';
	    if (isset($_SERVER['HTTP_CLIENT_IP']))
	        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
	    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
	        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
	    else if(isset($_SERVER['HTTP_X_FORWARDED']))
	        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
	    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
	        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
	    else if(isset($_SERVER['HTTP_FORWARDED']))
	        $ipaddress = $_SERVER['HTTP_FORWARDED'];
	    else if(isset($_SERVER['REMOTE_ADDR']))
	        $ipaddress = $_SERVER['REMOTE_ADDR'];
	    else
	        $ipaddress = 'UNKNOWN';
	    return $ipaddress;
	}

}
