<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class KategoriUKT extends CI_Controller {

	function __construct(){
		parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
		if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
		if ($this->session->userdata('level') != 'DEVELOPMENT') {
			$this->session->set_flashdata('message','Hak akses ditolak.');
			$this->session->set_flashdata('type_message','danger');
			redirect('Auth');
		}
		$this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
		$this->load->model('Settings/Tbl_setting_jurusan');
	}

	function index(){
        $data = array(
            'content'       => 'Settings/Import/kategori_ukt/content',
            'css'           => 'Settings/Import/kategori_ukt/css',
            'javascript'    => 'Settings/Import/kategori_ukt/javascript',
            'modal'         => 'Settings/Import/kategori_ukt/modal',
        );
    	$this->load->view('index', $data);
	}

	function Import(){
		$config = array(
			'upload_path'   => './import/settings/',
			'allowed_types' => 'xls|xlsx|csv|ods|ots',
			'max_size'      => 51200,
			'overwrite'     => TRUE,
			'file_name'     => 'Kategori_UKT_'.date('H i s d m Y'),
		);
		$this->upload->initialize($config);
		if(!$this->upload->do_upload()){
			$this->session->set_flashdata('message',$this->upload->display_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('Settings/Import/KategoriUKT/');
		}else {
			$file = $this->upload->data();
			$inputFileName = 'import/settings/' . $file['file_name'];
			try {
				$inputFileType = IOFactory::identify($inputFileName);
				$objReader = IOFactory::createReader($inputFileType);
				$objPHPExcel = $objReader->load($inputFileName);
			} catch (Exception $e) {
				die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '" : ' . $e->getMessage());
			}
			$sheet = $objPHPExcel->getSheet(0);
			$highestRow = $sheet->getHighestRow();
			for ($row = 2; $row <= $highestRow; $row++) {
				try{
                    $rules = array(
                            'where' => array('kode_jurusan' => $sheet->getCellByColumnAndRow(0, $row)->getValue()),
                        'data'  => array(
                            'k1' => $sheet->getCellByColumnAndRow(2, $row)->getValue(),
                            'k2' => $sheet->getCellByColumnAndRow(3, $row)->getValue(),
                            'k3' => $sheet->getCellByColumnAndRow(4, $row)->getValue(),
                            'k4' => $sheet->getCellByColumnAndRow(5, $row)->getValue(),
                            'k5' => $sheet->getCellByColumnAndRow(6, $row)->getValue(),
                            'k6' => $sheet->getCellByColumnAndRow(7, $row)->getValue(),
                            'k7' => $sheet->getCellByColumnAndRow(8, $row)->getValue(),
                            'k8' => $sheet->getCellByColumnAndRow(9, $row)->getValue(),
                            'k9' => $sheet->getCellByColumnAndRow(10, $row)->getValue(),
                            'updated_by' => $this->session->userdata('id_users'),
                        ),
                    );
                    $this->Tbl_setting_jurusan->update($rules);
                }catch (Exception $e){
                    $this->session->set_flashdata('message', $e->getMessage());
                    $this->session->set_flashdata('type_message', 'danger');
                    redirect('Settings/Import/KategoriUKT/');
                }
			}
			$this->session->set_flashdata('message', 'Import berhasil.');
			$this->session->set_flashdata('type_message', 'success');
			redirect('Settings/Import/KategoriUKT/');
		}
	}
}

