<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class KuotaJurusan extends CI_Controller {

	function __construct(){
		parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != 'DEVELOPMENT') {
            $this->session->set_flashdata('message','Hak akses ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
		$this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
		$this->load->model('Settings/Tbl_setting_jurusan');
	}

	function index(){
        $data = array(
            'content'       => 'Import/Settings/kuota_jurusan/content',
            'css'           => 'Import/Settings/kuota_jurusan/css',
            'javascript'    => 'Import/Settings/kuota_jurusan/javascript',
            'modal'         => 'Import/Settings/kuota_jurusan/modal',
        );
    	$this->load->view('index', $data);
	}

	function Import(){
		$config = array(
			'upload_path'   => './import/settings/',
			'allowed_types' => 'xls|xlsx|csv|ods|ots',
			'max_size'      => 51200,
			'overwrite'     => TRUE,
			'file_name'     => 'Kuota_Jurusan_'.date('H i s d m Y'),
		);
		$this->upload->initialize($config);
		if(!$this->upload->do_upload()){
			$this->session->set_flashdata('message',$this->upload->display_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('Import/Settings/KuotaJurusan/');
		}else {
			$file = $this->upload->data();
			$inputFileName = 'import/settings/' . $file['file_name'];
			try {
				$inputFileType = IOFactory::identify($inputFileName);
				$objReader = IOFactory::createReader($inputFileType);
				$objPHPExcel = $objReader->load($inputFileName);
			} catch (Exception $e) {
				die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '" : ' . $e->getMessage());
			}
			$sheet = $objPHPExcel->getSheet(0);
			$highestRow = $sheet->getHighestRow();
            for ($row = 2; $row <= $highestRow; $row++) {
                try{
                    $rules = array(
                        'where' => array('kode_jurusan' => $sheet->getCellByColumnAndRow(0, $row)->getValue()),
                        'data'  => array(
                            'quota' => $sheet->getCellByColumnAndRow(2, $row)->getValue(),
                            'updated_by' => $this->session->userdata('id_users'),
                        ),
                    );
                    $this->Tbl_setting_jurusan->update($rules);
                }catch (Exception $e){
                    $this->session->set_flashdata('message', $e->getMessage());
                    $this->session->set_flashdata('type_message', 'danger');
                    redirect('Import/Settings/KuotaJurusan/');
                }
            }
            $this->session->set_flashdata('message', 'Import berhasil.');
            $this->session->set_flashdata('type_message', 'success');
            redirect('Import/Settings/KuotaJurusan/');
		}
	}
}

