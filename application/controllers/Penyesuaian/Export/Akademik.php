<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Akademik extends CI_Controller {
	
    function __construct(){
        parent::__construct();
        ini_set('memory_limit', '-1');
        date_default_timezone_set('Asia/Jakarta');
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != 'DEVELOPMENT') {
            $this->session->set_flashdata('message','Hak akses ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        $this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
        $this->load->model('Daftar/Tbl_daftar_pddikti');
        $this->load->model('Views/Daftar/View_daftar_users');
        $this->load->model('Views/Daftar/View_daftar_pddikti');
        $this->load->model('Views/Daftar/View_daftar_kelulusan');
        $this->load->model('Views/Daftar/View_daftar_ukt');
        $this->load->model('Views/Daftar/View_daftar_pddikti_mahasiswa');
        $this->load->model('Views/Daftar/View_daftar_pddikti_ayah');
        $this->load->model('Views/Daftar/View_daftar_pddikti_ibu');
        $this->load->model('Views/Daftar/View_daftar_pddikti_wali');
		$this->load->model('Views/Daftar/View_daftar_extra');
    }

    function index(){
    	$data = array(
    		'tahun'	=> $this->View_daftar_users->distinctAllOrder('YEAR(date_created_kelulusan) as tahun','tahun')->result(),
		);
    	$this->load->view('daftar/export/akademik',$data);
	}
	
    function actionExport(){
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', '-1');
        $tahun = $this->input->post('tahun');
        $viewUsers = $this->View_daftar_users->whereAnd(array('YEAR(date_created_kelulusan)' => $tahun, 'status_verifikasi' => "SUDAH VERIFIKASI"));
        if ($viewUsers->num_rows() > 0){
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getProperties()
                ->setCreator("Piscal Pratama Putra") //creator
                ->setTitle("Export data Akademik");  //file title
            $objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
            $objget = $objPHPExcel->getActiveSheet();  //inisiasi get object
            //$objget->setTitle('Sample Sheet'); //sheet title
            //Warna header tabel
            /*$objget->getStyle("A1:C1")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '92d050')
                    ),
                    'font' => array(
                        'color' => array('rgb' => '000000')
                    )
                )
            );*/
            //table header
            $cols = array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X", "Y");
            $val = array(
                "Nomor Peserta","NIK(No.KTP)","Nama", "Tempat Lahir","Tanggal Lahir","Jenis Kelamin","Warga Negara","Agama", "Alamat","Provinsi",
                "Kabupaten / Kota","Kecamatan","Kelurahan","Kode Pos","Telepon","Nama Ayah","Nama Ibu","Penghasilan Ayah",
                "Penghasilan Ibu","Pekerjaan Ayah","Pekerjaan Ibu","Pendidikan Ayah","Pendidikan Ibu","Asal Sekolah","Rumpun"
            );
            for ($a = 0; $a < 25; $a++) {
                $objset->setCellValue($cols[$a].'1', $val[$a]);
                //Setting lebar cell
                $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('U')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('V')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('W')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('X')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setAutoSize(true);
                $style = array(
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    )
                );
                $objPHPExcel->getActiveSheet()->getStyle($cols[$a].'1')->applyFromArray($style);
            }
            $baris  = 2;
            $no = 1;
            foreach ($viewUsers->result() as $value){
                //pemanggilan sesuaikan dengan nama kolom tabel
                $viewPMahasiswa = $this->View_daftar_pddikti_mahasiswa->whereAnd(array('nik_passport' => $value->nik_passport,'YEAR(date_created_pddikti)' => $tahun))->row();
                $viewPAyah = $this->View_daftar_pddikti_ayah->whereAnd(array('nik_passport' => $value->nik_passport,'YEAR(date_created_pddikti)' => $tahun))->row();
                $viewPIbu = $this->View_daftar_pddikti_ibu->whereAnd(array('nik_passport' => $value->nik_passport,'YEAR(date_created_pddikti)' => $tahun))->row();
                $objset->setCellValue("A".$baris, $value->nomor_peserta);
                $objset->setCellValue("B".$baris, $viewPMahasiswa->nik_passport);
                $objset->setCellValue("C".$baris, $viewPMahasiswa->nama);
                $objset->setCellValue("D".$baris, $viewPMahasiswa->tmp_lhr);
                $objset->setCellValue("E".$baris, $viewPMahasiswa->tgl_lhr);
                $objset->setCellValue("F".$baris, $viewPMahasiswa->jenis_kelamin);
                $objset->setCellValue("G".$baris, $viewPMahasiswa->warga_negara);
                $objset->setCellValue("H".$baris, $viewPMahasiswa->agama);
                $objset->setCellValue("I".$baris, $viewPMahasiswa->jalan);
                $objset->setCellValue("J".$baris, $viewPMahasiswa->provinsi);
                $objset->setCellValue("K".$baris, $viewPMahasiswa->kabupaten);
                $objset->setCellValue("L".$baris, $viewPMahasiswa->kecamatan);
                $objset->setCellValue("M".$baris, $viewPMahasiswa->kelurahan);
                $objset->setCellValue("N".$baris, $viewPMahasiswa->kode_pos);
                $objset->setCellValue("O".$baris, $viewPMahasiswa->no_hp);
                $objset->setCellValue("P".$baris, $viewPAyah->nama_ayah);
                $objset->setCellValue("Q".$baris, $viewPIbu->nama_ibu);
                $objset->setCellValue("R".$baris, $viewPAyah->penghasilan_ayah);
                $objset->setCellValue("S".$baris, $viewPIbu->penghasilan_ibu);
                $objset->setCellValue("T".$baris, $viewPAyah->pekerjaan_ayah);
                $objset->setCellValue("U".$baris, $viewPIbu->pekerjaan_ibu);
                $objset->setCellValue("V".$baris, $viewPAyah->pendidikan_ayah);
                $objset->setCellValue("W".$baris, $viewPIbu->pendidikan_ibu);
                $objset->setCellValue("X".$baris, "");
                $objset->setCellValue("Y".$baris, $viewPMahasiswa->rumpun);

                //Set number value
                //$objPHPExcel->getActiveSheet()->getStyle('C1:C'.$baris)->getNumberFormat()->setFormatCode('0');

                $baris++;
                $no++;
            }
            $objPHPExcel->getActiveSheet()->setTitle('Data Export');

            $objPHPExcel->setActiveSheetIndex(0);
            $filename = urlencode("Akademik_".$tahun."_".date("Y_m_d_H_i_s").".xls");
            header('Content-Type: application/vnd.ms-excel'); //mime type
            header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
            header('Cache-Control: max-age=0'); //no cache
            $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
        }else{
            $this->session->set_flashdata('message','Data kosong.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Daftar/Export/Akademik/');
        }
    }

}
