<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class UKT extends CI_Controller {

	function __construct(){

		parent::__construct();
		if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
		if ($this->session->userdata('level') != 'DEVELOPMENT') {
			$this->session->set_flashdata('message','Hak akses ditolak.');
			$this->session->set_flashdata('type_message','danger');
			redirect('Auth');
		}

		$this->load->model('ServerSide/SS_penyesuaian_ukt', '', TRUE);
		$this->load->model('Penyesuaian/Tbl_penyesuaian_ukt');
		$this->load->model('Settings/Tbl_setting_jurusan');
		$this->load->model('Views/Penyesuaian/View_penyesuaian_ukt');
	}

	function index(){
		$data = array(
            'content'       => 'Penyesuaian/Ukt/content',
            'css'           => 'Penyesuaian/Ukt/css',
            'javascript'    => 'Penyesuaian/Ukt/javascript',
            'modal'         => 'Penyesuaian/Ukt/modal',
        );
		$this->load->view('index', $data);
	}

	function Edit($id){
		$rules = array(
            'select'    => null,
            'where'     => array(
                'nik_passport' => $id,
            ),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $tbUkt = $this->View_penyesuaian_ukt->where($rules)->row();
        $data = array(
			'content'       => 'Penyesuaian/Ukt/ukt_edit/content',
            'css'           => 'Penyesuaian/Ukt/ukt_edit/css',
            'javascript'    => 'Penyesuaian/Ukt/ukt_edit/javascript',
            'modal'         => 'Penyesuaian/Ukt/ukt_edit/modal',
            'tbUkt' => $tbUkt,
        );
        $this->load->view('index',$data);
	}

	function Update($id){
	    $rules[] = array('field' => 'kategori', 'label' => 'Kategori', 'rules' => 'required');
	    $rules[] = array('field' => 'rekomendasi', 'label' => 'Rekomendasi', 'rules' => 'required');
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == FALSE){
            $this->session->set_flashdata('message',validation_errors());
            $this->session->set_flashdata('type_message','danger');
            redirect('Penyesuaian/UKT/');
        }else{
        	$kategori = $this->input->post('kategori');
            $kode_jurusan = $this->input->post('kode_jurusan');
            $tbSJurusan = $this->Tbl_setting_jurusan->UKT($kategori,$kode_jurusan)->row();
            $data = array(
                'kategori'          => set_value('kategori'),
                'rekomendasi'  		=> set_value('rekomendasi'),
                'jumlah'			=> $tbSJurusan->ukt,
                'updated_by'     	=> $this->session->userdata('id_users'),
                'date_updated'		=> date('Y-m-d H:i:s'),
            );

            if ($this->Tbl_penyesuaian_ukt->update($id,$data)) {
                $this->session->set_flashdata('message','Data berhasil diubah.');
                $this->session->set_flashdata('type_message','success');
                redirect('Penyesuaian/UKT/');
            }else{
                $this->session->set_flashdata('message','Terjadi kesalahan dalam edit data UKT');
                $this->session->set_flashdata('type_message','danger');
                redirect('Penyesuaian/UKT/');
            }
        }
	}

    function Delete($id){
        if ($this->Tbl_daftar_kelulusan->delete($id)) {
            $this->session->set_flashdata('message','Data berhasil dihapus.');
            $this->session->set_flashdata('type_message','success');
            redirect('Daftar/Kelulusan/');
        }else{
            $this->session->set_flashdata('message','Terjadi kesalahan dalam hapus data agama');
            $this->session->set_flashdata('type_message','danger');
            redirect('Daftar/Kelulusan/');
        }
    }

	function Json(){
		$fetch_data = $this->SS_penyesuaian_ukt->make_datatables();
		$data = array();
		foreach($fetch_data as $row)
		{
			$sub_array = array();
            $sub_array[] = "
            <div class=\"btn-group\">
                <button type=\"button\" class=\"btn btn-primary dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                    Action <span class=\"caret\"></span>
                </button>
                <ul class=\"dropdown-menu\">
                    <li><a href=\"".base_url('Penyesuaian/UKT/Edit/'.$row->nik_passport)."\" target='_blank'>Detail</a></li>
                </ul>
            </div>";
			$sub_array[] = $row->nik_passport;
			$sub_array[] = $row->nama;
			$sub_array[] = $row->score;
			$sub_array[] = $row->kategori;
			$sub_array[] = $row->jumlah;
			$sub_array[] = $row->status;
			$sub_array[] = $row->rekomendasi;
			$data[] = $sub_array;
		}
		$output = array(
			"draw"				=>	intval($_POST["draw"]),
			"recordsTotal"		=>	$this->SS_penyesuaian_ukt->get_all_data(),
			"recordsFiltered"	=>	$this->SS_penyesuaian_ukt->get_filtered_data(),
			"data"				=>	$data
		);

		echo json_encode($output);
	}
}