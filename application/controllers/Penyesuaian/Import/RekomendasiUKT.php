<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class RekomendasiUKT extends CI_Controller {

	function __construct(){
		parent::__construct();
		if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
		if ($this->session->userdata('level') != 'DEVELOPMENT') {
			$this->session->set_flashdata('message','Hak akses ditolak.');
			$this->session->set_flashdata('type_message','danger');
			redirect('Auth');
		}
		$this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
		$this->load->model('Daftar/Tbl_daftar_kelulusan');
		$this->load->model('Daftar/Tbl_daftar_users');
		$this->load->model('Daftar/Tbl_daftar_pddikti');
        $this->load->model('Daftar/Tbl_daftar_extra');
        $this->load->model('Daftar/Tbl_daftar_ukt');
        $this->load->model('Penyesuaian/Tbl_penyesuaian_users');
        $this->load->model('Penyesuaian/Tbl_penyesuaian_ukt');
		$this->load->model('Pradaftar/Tbl_pradaftar_kelulusan');
		$this->load->model('Pradaftar/Tbl_pradaftar_biodata');
		$this->load->model('Pradaftar/Tbl_pradaftar_users');
		$this->load->model('Settings/Tbl_setting_jalur_masuk');
		$this->load->model('Settings/Tbl_setting_jurusan');
	}

	function index(){
    	$data = array(
            'content'       => 'Penyesuaian/Import/rekomendasi_ukt/content',
            'css'           => 'Penyesuaian/Import/rekomendasi_ukt/css',
            'javascript'    => 'Penyesuaian/Import/rekomendasi_ukt/javascript',
            'modal'         => 'Penyesuaian/Import/rekomendasi_ukt/modal',
        );
		$this->load->view('index', $data);
	}

	function Import(){
		$rules[] = array('field' => 'rekomendasi', 'label' => 'rekomendasi', 'rules' => 'required');
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('message',validation_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('Penyesuaian/Import/RekomendasiUKT/');
		}else{
			$config = array(
				'upload_path'   => './import/penyesuaian/',
				'allowed_types' => 'xls|xlsx|csv|ods|ots',
				'max_size'      => 51200,
				'overwrite'     => TRUE,
				'file_name'     => 'RekUKT_KK00'.$this->input->post('rekomendasi').'_'.date('Y').'_'.date('H i s d m Y'),
			);
			$this->upload->initialize($config);
			if(!$this->upload->do_upload()){
				$this->session->set_flashdata('message',$this->upload->display_errors());
				$this->session->set_flashdata('type_message','danger');
				redirect('Penyesuaian/Import/RekomendasiUKT/');
			}else{
				$file = $this->upload->data();
				$inputFileName = 'import/penyesuaian/'.$file['file_name'];
				try {
					$inputFileType	= IOFactory::identify($inputFileName);
					$objReader		= IOFactory::createReader($inputFileType);
					$objPHPExcel	= $objReader->load($inputFileName);
				} catch (Exception $e) {
					die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'" : '.$e->getMessage());
				}
				$sheet	= $objPHPExcel->getSheet(0);
				$highestRow	= $sheet->getHighestRow();
				for ($row = 2; $row <= $highestRow; $row++) {
					$nomor_peserta	= $sheet->getCellByColumnAndRow(0,$row)->getValue();
					$nama			= str_replace('\'','`',strtoupper($sheet->getCellByColumnAndRow(1,$row)->getValue()));
					$jurusan	    = $sheet->getCellByColumnAndRow(2,$row)->getValue();
                    $num_rows		= $this->Tbl_penyesuaian_users->where(array('nomor_peserta' => $nomor_peserta))->num_rows();
                    $penyesuaian_users		= $this->Tbl_penyesuaian_users->where(array('nomor_peserta' => $nomor_peserta))->row();
					if ($num_rows == 0) {
						
					}else{
						$rules = array(
							'select'    => null,
							'where'     => array(
								'nik_passport' => $penyesuaian_users->nik_passport,
							),
							'or_where'  => null,
							'order'     => null,
							'limit'     => null,
							'pagging'   => null,
						);
                        $daftar_ukt = $this->Tbl_daftar_ukt->where($rules)->row();
                        
						$rules2 = array(
								'where' => array(
									'nik_passport' => $penyesuaian_users->nik_passport,
								),
								'data'  => array(
									'kategori'		=> $daftar_ukt->kategori,
									'jumlah'		=> $daftar_ukt->jumlah,
									'score' 		=> $daftar_ukt->score,
									'rekomendasi'   => $this->input->post('rekomendasi'),
									'updated_by'		=> $this->session->userdata('id_users'),
								),
						);
                        $this->Tbl_penyesuaian_ukt->update($rules2);
                    }
				}
				$this->session->set_flashdata('message','Import berhasil.');
				$this->session->set_flashdata('type_message','success');
				redirect('Penyesuaian/Import/RekomendasiUKT/');
			}
		}
	}
}