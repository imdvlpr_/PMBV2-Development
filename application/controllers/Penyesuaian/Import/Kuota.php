<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Kuota extends CI_Controller {

	function __construct(){
		parent::__construct();
		if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
		if ($this->session->userdata('level') != 'DEVELOPMENT') {
			$this->session->set_flashdata('message','Hak akses ditolak.');
			$this->session->set_flashdata('type_message','danger');
			redirect('Auth');
		}
		$this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
		$this->load->model('Daftar/Tbl_daftar_kelulusan');
		$this->load->model('Daftar/Tbl_daftar_users');
		$this->load->model('Daftar/Tbl_daftar_pddikti');
		$this->load->model('Daftar/Tbl_daftar_extra');
		$this->load->model('Pradaftar/Tbl_pradaftar_kelulusan');
		$this->load->model('Pradaftar/Tbl_pradaftar_biodata');
		$this->load->model('Pradaftar/Tbl_pradaftar_users');
		$this->load->model('Settings/Tbl_setting_jalur_masuk');
		$this->load->model('Settings/Tbl_setting_jurusan');
	}

	function index(){
    	$data = array(
            'content'       => 'Penyesuaian/Import/kuota/content',
            'css'           => 'Penyesuaian/Import/kuota/css',
            'javascript'    => 'Penyesuaian/Import/kuota/javascript',
            'modal'         => 'Penyesuaian/Import/kuota/modal',
        );
		$this->load->view('index', $data);
	}

	function Import(){
		$rules[] = array('field' => 'kategori', 'label' => 'Kategori', 'rules' => 'required');
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('message',validation_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('Penyesuaian/Import/Kuota/');
		}else{
			$config = array(
				'upload_path'   => './import/penyesuaian/',
				'allowed_types' => 'xls|xlsx|csv|ods|ots',
				'max_size'      => 51200,
				'overwrite'     => TRUE,
				'file_name'     => 'Kuota_KK00'.$this->input->post('kategori').'_'.date('Y').'_'.date('H i s d m Y'),
			);
			$this->upload->initialize($config);
			if(!$this->upload->do_upload()){
				$this->session->set_flashdata('message',$this->upload->display_errors());
				$this->session->set_flashdata('type_message','danger');
				redirect('Penyesuaian/Import/Kuota/');
			}else{
				$file = $this->upload->data();
				$inputFileName = 'import/penyesuaian/'.$file['file_name'];
				try {
					$inputFileType	= IOFactory::identify($inputFileName);
					$objReader		= IOFactory::createReader($inputFileType);
					$objPHPExcel	= $objReader->load($inputFileName);
				} catch (Exception $e) {
					die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'" : '.$e->getMessage());
				}
				$sheet	= $objPHPExcel->getSheet(0);
				$highestRow	= $sheet->getHighestRow();
				for ($row = 2; $row <= $highestRow; $row++) {
					$kode_jurusan	= $sheet->getCellByColumnAndRow(0,$row)->getValue();
					$jurusan			= str_replace('\'','`',strtoupper($sheet->getCellByColumnAndRow(1,$row)->getValue()));
					$kuota	= $sheet->getCellByColumnAndRow(2,$row)->getValue();
					$rules2 = array(
						'select'    => null,
						'where'     => array(
							'kode_jurusan' => $kode_jurusan
						),
						'or_where'  => null,
						'order'     => null,
						'limit'     => null,
						'pagging'   => null,
					);
					$num_rows		= $this->Tbl_setting_jurusan->where($rules2)->num_rows();
					if ($num_rows == 0) {
						
					}else{
                        $data = array(
                            
						);
						$rules2 = array(
							'where' => array(
								'kode_jurusan' => $kode_jurusan
							),
							'data'  => array(
								'quota_k1'			=> $kuota,
                            	'updated_by'		=> $this->session->userdata('id_users'),
						),
					);
                        $this->Tbl_setting_jurusan->update($rules2);
                    }
				}
				$this->session->set_flashdata('message','Import berhasil.');
				$this->session->set_flashdata('type_message','success');
				redirect('Penyesuaian/Import/Kuota/');
			}
		}
	}
}