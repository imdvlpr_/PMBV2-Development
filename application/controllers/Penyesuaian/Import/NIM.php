<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class NIM extends CI_Controller {

	function __construct(){
		parent::__construct();
		if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
		if ($this->session->userdata('level') != 'DEVELOPMENT') {
			$this->session->set_flashdata('message','Hak akses ditolak.');
			$this->session->set_flashdata('type_message','danger');
			redirect('Auth');
		}
		$this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
		$this->load->model('Daftar/Tbl_daftar_kelulusan');
		$this->load->model('Daftar/Tbl_daftar_users');
		$this->load->model('Daftar/Tbl_daftar_pddikti');
		$this->load->model('Daftar/Tbl_daftar_extra');
		$this->load->model('Internasional/Tbl_internasional_users');
		$this->load->model('Internasional/Tbl_internasional_biodata');
		$this->load->model('Pradaftar/Tbl_pradaftar_kelulusan');
		$this->load->model('Pradaftar/Tbl_pradaftar_biodata');
		$this->load->model('Pradaftar/Tbl_pradaftar_users');
		$this->load->model('Settings/Tbl_setting_jalur_masuk');
	}

	function index(){
		$tbSJalurMasuk = $this->Tbl_setting_jalur_masuk->read()->result();
		$data = array(
			'tbSJalurMasuk' => $tbSJalurMasuk,
		);
    	$this->load->view('daftar/import/nim', $data);
	}
	
	function Import(){
		
		$config = array(
			'upload_path'   => './import/daftar/',
			'allowed_types' => 'xls|xlsx|csv|ods|ots',
			'max_size'      => 51200,
			'overwrite'     => TRUE,
			'file_name'     => 'NIM_JM00_PDDIKTI_'.date('Y').'_'.date('H i s d m Y'),
		);
		$this->upload->initialize($config);
		if(!$this->upload->do_upload()){
			$this->session->set_flashdata('message',$this->upload->display_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('Daftar/Import/NIM/');
		}else{
			$file = $this->upload->data();
			$inputFileName = 'import/daftar/'.$file['file_name'];
			try {
				$inputFileType	= IOFactory::identify($inputFileName);
				$objReader		= IOFactory::createReader($inputFileType);
				$objPHPExcel	= $objReader->load($inputFileName);
			} catch (Exception $e) {
				die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'" : '.$e->getMessage());
			}
			$sheet	= $objPHPExcel->getSheet(0);
			$highestRow	= $sheet->getHighestRow();
			for ($row = 2; $row <= $highestRow; $row++) {
				$nomor_peserta	= $sheet->getCellByColumnAndRow(0,$row)->getValue();
				$nama			= str_replace('\'','`',strtoupper($sheet->getCellByColumnAndRow(1,$row)->getValue()));
				$nim	= $sheet->getCellByColumnAndRow(2,$row)->getValue();

				$tbUsers_row = $this->Tbl_daftar_users->likeAnd(array('nomor_peserta'=>$nomor_peserta))->num_rows();

				$tbUsers = $this->Tbl_daftar_users->likeAnd(array('nomor_peserta'=>$nomor_peserta))->row();

				if($tbUsers_row > 0){
					$data = array(
						'nim' => $nim,
						'date_updated' => date('Y-m-d H:i:s'),
					);
					$this->Tbl_daftar_pddikti->update($tbUsers->nik_passport,$data);
                }else{
                	echo "gagal";
                }
			}
			$this->session->set_flashdata('message','Import berhasil.');
			$this->session->set_flashdata('type_message','success');
			redirect('Daftar/Import/NIM/');
		}
	}
}