<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Kelulusan extends CI_Controller {
	
    function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != 'DEVELOPMENT') {
            $this->session->set_flashdata('message','Hak akses ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }

        $this->load->model('Daftar/Tbl_daftar_kelulusan');

    }
	
    function index(){
    	$data = array(
    		'num_kelulusan' 		=> $this->Tbl_daftar_kelulusan->read()->num_rows(),
    		'num_kelulusan_sv' 		=> $this->Tbl_daftar_kelulusan->whereAnd(array('status' => 'SUDAH DAFTAR'))->num_rows(),
    		'num_kelulusan_bv' 		=> $this->Tbl_daftar_kelulusan->whereAnd(array('status' => 'BELUM DAFTAR'))->num_rows(),

            'num_kelulusan_snmptn'         => $this->Tbl_daftar_kelulusan->whereAnd(array('id_jlr_msk' => 1))->num_rows(),
            'num_kelulusan_sd_snmptn'      => $this->Tbl_daftar_kelulusan->whereAnd(array('status' => 'SUDAH DAFTAR', 'id_jlr_msk' => 1))->num_rows(),
            'num_kelulusan_bd_snmptn'      => $this->Tbl_daftar_kelulusan->whereAnd(array('status' => 'BELUM DAFTAR', 'id_jlr_msk' => 1))->num_rows(),

            'num_kelulusan_span'         => $this->Tbl_daftar_kelulusan->whereAnd(array('id_jlr_msk' => 2))->num_rows(),
            'num_kelulusan_sd_span'      => $this->Tbl_daftar_kelulusan->whereAnd(array('status' => 'SUDAH DAFTAR', 'id_jlr_msk' => 2))->num_rows(),
            'num_kelulusan_bd_span'      => $this->Tbl_daftar_kelulusan->whereAnd(array('status' => 'BELUM DAFTAR', 'id_jlr_msk' => 2))->num_rows(),

            'num_kelulusan_sbmptn'         => $this->Tbl_daftar_kelulusan->whereAnd(array('id_jlr_msk' => 3))->num_rows(),
            'num_kelulusan_sd_sbmptn'      => $this->Tbl_daftar_kelulusan->whereAnd(array('status' => 'SUDAH DAFTAR', 'id_jlr_msk' => 3))->num_rows(),
            'num_kelulusan_bd_sbmptn'      => $this->Tbl_daftar_kelulusan->whereAnd(array('status' => 'BELUM DAFTAR', 'id_jlr_msk' => 3))->num_rows(),

            'num_kelulusan_umptkin'         => $this->Tbl_daftar_kelulusan->whereAnd(array('id_jlr_msk' => 4))->num_rows(),
            'num_kelulusan_sd_umptkin'      => $this->Tbl_daftar_kelulusan->whereAnd(array('status' => 'SUDAH DAFTAR', 'id_jlr_msk' => 4))->num_rows(),
            'num_kelulusan_bd_umptkin'      => $this->Tbl_daftar_kelulusan->whereAnd(array('status' => 'BELUM DAFTAR', 'id_jlr_msk' => 4))->num_rows(),

            'num_kelulusan_mandiri'         => $this->Tbl_daftar_kelulusan->whereAnd(array('id_jlr_msk' => 5))->num_rows(),
            'num_kelulusan_sd_mandiri'      => $this->Tbl_daftar_kelulusan->whereAnd(array('status' => 'SUDAH DAFTAR', 'id_jlr_msk' => 5))->num_rows(),
            'num_kelulusan_bd_mandiri'      => $this->Tbl_daftar_kelulusan->whereAnd(array('status' => 'BELUM DAFTAR', 'id_jlr_msk' => 5))->num_rows(),
		);
    	$this->load->view('daftar/statistik/kelulusan', $data);
    }

}
