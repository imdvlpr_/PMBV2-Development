<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class JurusanFakultas extends CI_Controller {
	
    function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != 'DEVELOPMENT') {
            $this->session->set_flashdata('message','Hak akses ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }

        $this->load->model('Daftar/Tbl_daftar_kelulusan');
        $this->load->model('Views/Settings/View_setting_fakultas_jurusan');
    }
	
    function index(){
    	$viewSJurusan = $this->View_setting_fakultas_jurusan->read()->result();
    	foreach ($viewSJurusan as $value){
    		$snmptn[$value->kode_jurusan] = $this->Tbl_daftar_kelulusan->whereAndUsers(array("kode_jurusan" => $value->kode_jurusan, "a.id_jlr_msk" => 1, "verifikasi" => "SUDAH VERIFIKASI"))->num_rows();
		}
		foreach ($viewSJurusan as $value){
			$span[$value->kode_jurusan] = $this->Tbl_daftar_kelulusan->whereAndUsers(array("kode_jurusan" => $value->kode_jurusan, "a.id_jlr_msk" => 2, "verifikasi" => "SUDAH VERIFIKASI"))->num_rows();
		}
		foreach ($viewSJurusan as $value){
			$sbmptn[$value->kode_jurusan] = $this->Tbl_daftar_kelulusan->whereAndUsers(array("kode_jurusan" => $value->kode_jurusan, "a.id_jlr_msk" => 3, "verifikasi" => "SUDAH VERIFIKASI"))->num_rows();
		}
        foreach ($viewSJurusan as $value){
            $umptkin[$value->kode_jurusan] = $this->Tbl_daftar_kelulusan->whereAndUsers(array("kode_jurusan" => $value->kode_jurusan, "a.id_jlr_msk" => 4, "verifikasi" => "SUDAH VERIFIKASI"))->num_rows();
        }
        foreach ($viewSJurusan as $value){
            $mandiri[$value->kode_jurusan] = $this->Tbl_daftar_kelulusan->whereAndUsers(array("kode_jurusan" => $value->kode_jurusan, "a.id_jlr_msk" => 5, "verifikasi" => "SUDAH VERIFIKASI"))->num_rows();
        }
		foreach ($viewSJurusan as $value){
    		$jumlah[$value->kode_jurusan] = $snmptn[$value->kode_jurusan] + $span[$value->kode_jurusan] + $sbmptn[$value->kode_jurusan] + $umptkin[$value->kode_jurusan] + $mandiri[$value->kode_jurusan];
		}
		$data = array(
			'viewSJurusan'	=> $viewSJurusan,
			'snmptn'		=> $snmptn,
			'span'		=> $span,
			'sbmptn'		=> $sbmptn,
            'umptkin'        => $umptkin,
            'mandiri'        => $mandiri,
			'jumlah'		=> $jumlah,
		);
		$this->load->view('daftar/statistik/jurusan_fakultas',$data);
    }

}
