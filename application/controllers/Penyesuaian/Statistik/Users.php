<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Users extends CI_Controller {
	
    function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != 'DEVELOPMENT') {
            $this->session->set_flashdata('message','Hak akses ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }

		$this->load->model('Daftar/Tbl_daftar_users');
        //$this->load->model('Daftar/Tbl_daftar_kelulusan');

    }
	
    function index(){
    	$data = array(
    		'num_users' 		=> $this->Tbl_daftar_users->read()->num_rows(),
    		'num_users_sv' 		=> $this->Tbl_daftar_users->whereAnd(array('verifikasi' => 'SUDAH VERIFIKASI'))->num_rows(),
    		'num_users_bv' 		=> $this->Tbl_daftar_users->whereAnd(array('verifikasi' => 'BELUM VERIFIKASI'))->num_rows(),

            'num_users_snmptn'         => $this->Tbl_daftar_users->whereAnd(array('id_jlr_msk' => 1))->num_rows(),
            'num_users_sv_snmptn'      => $this->Tbl_daftar_users->whereAnd(array('verifikasi' => 'SUDAH VERIFIKASI', 'id_jlr_msk' => 1))->num_rows(),
            'num_users_bv_snmptn'      => $this->Tbl_daftar_users->whereAnd(array('verifikasi' => 'BELUM VERIFIKASI', 'id_jlr_msk' => 1))->num_rows(),

            'num_users_span'         => $this->Tbl_daftar_users->whereAnd(array('id_jlr_msk' => 2))->num_rows(),
            'num_users_sv_span'      => $this->Tbl_daftar_users->whereAnd(array('verifikasi' => 'SUDAH VERIFIKASI', 'id_jlr_msk' => 2))->num_rows(),
            'num_users_bv_span'      => $this->Tbl_daftar_users->whereAnd(array('verifikasi' => 'BELUM VERIFIKASI', 'id_jlr_msk' => 2))->num_rows(),

            'num_users_sbmptn'         => $this->Tbl_daftar_users->whereAnd(array('id_jlr_msk' => 3))->num_rows(),
            'num_users_sv_sbmptn'      => $this->Tbl_daftar_users->whereAnd(array('verifikasi' => 'SUDAH VERIFIKASI', 'id_jlr_msk' => 3))->num_rows(),
            'num_users_bv_sbmptn'      => $this->Tbl_daftar_users->whereAnd(array('verifikasi' => 'BELUM VERIFIKASI', 'id_jlr_msk' => 3))->num_rows(),

            'num_users_umptkin'         => $this->Tbl_daftar_users->whereAnd(array('id_jlr_msk' => 4))->num_rows(),
            'num_users_sv_umptkin'      => $this->Tbl_daftar_users->whereAnd(array('verifikasi' => 'SUDAH VERIFIKASI', 'id_jlr_msk' => 4))->num_rows(),
            'num_users_bv_umptkin'      => $this->Tbl_daftar_users->whereAnd(array('verifikasi' => 'BELUM VERIFIKASI', 'id_jlr_msk' => 4))->num_rows(),

            'num_users_mandiri'         => $this->Tbl_daftar_users->whereAnd(array('id_jlr_msk' => 5))->num_rows(),
            'num_users_sv_mandiri'      => $this->Tbl_daftar_users->whereAnd(array('verifikasi' => 'SUDAH VERIFIKASI', 'id_jlr_msk' => 5))->num_rows(),
            'num_users_bv_mandiri'      => $this->Tbl_daftar_users->whereAnd(array('verifikasi' => 'BELUM VERIFIKASI', 'id_jlr_msk' => 5))->num_rows(),
		);
    	$this->load->view('daftar/statistik/users', $data);
    }

}
