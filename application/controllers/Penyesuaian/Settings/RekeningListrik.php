<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class RekeningListrik extends CI_Controller {

    function __construct(){
        parent::__construct();
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != "DEVELOPMENT") {
            $this->session->set_flashdata('message','Hak Akses Ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Dashboard');
        }
        $this->load->model('Settings/Tbl_setting_rekening_listrik');
    }

    function index(){
        $tbSRekeningListrik = $this->Tbl_setting_rekening_listrik->read()->result();
        $data = array(
            'tbSRekeningListrik' => $tbSRekeningListrik,
        );
        $this->load->view('daftar/settings/rekening_listrik',$data);
    }

    function Tambah(){
        $rules[] = array('field' => 'rekening_listrik', 'label' => 'Rekening Listrik', 'rules' => 'required');
        $rules[] = array('field' => 'nilai', 'label' => 'Nilai', 'rules' => 'required');
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == FALSE){
            $this->session->set_flashdata('message',validation_errors());
            $this->session->set_flashdata('type_message','danger');
            redirect('Daftar/Settings/RekeningListrik/');
        }else{
            $data = array(
                'rekening_listrik'  => strtoupper($this->input->post('rekening_listrik')),
                'nilai'             => $this->input->post('nilai'),
                'created_by'         => $this->session->userdata('id_users'),
                'updated_by'         => $this->session->userdata('id_users'),
            );
            if ($this->Tbl_setting_rekening_listrik->create($data)) {
                $this->session->set_flashdata('message','Data berhasil disimpan.');
                $this->session->set_flashdata('type_message','success');
                redirect('Daftar/Settings/RekeningListrik/');
            }else{
                $this->session->set_flashdata('message','Terjadi kesalahan dalam tambah data.');
                $this->session->set_flashdata('type_message','danger');
                redirect('Daftar/Settings/RekeningListrik/');
            }
        }
    }

    function Edit($id){
        $tbSRekeningListrik = $this->Tbl_setting_rekening_listrik->whereAnd(array('id_rekening_listrik' => $id))->row();
        $data = array(
            'tbSRekeningListrik' => $tbSRekeningListrik,
        );
        $this->load->view('daftar/settings/rekening_listrik_edit',$data);
    }

    function Update($id){
        $rules[] = array('field' => 'rekening_listrik', 'label' => 'Rekening Listrik', 'rules' => 'required');
        $rules[] = array('field' => 'nilai', 'label' => 'Nilai', 'rules' => 'required');
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == FALSE){
            $this->session->set_flashdata('message',validation_errors());
            $this->session->set_flashdata('type_message','danger');
            redirect('Daftar/Settings/RekeningListrik/');
        }else{
            $data = array(
                'rekening_listrik'  => strtoupper($this->input->post('rekening_listrik')),
                'nilai'             => $this->input->post('nilai'),
                'updated_by'         => $this->session->userdata('id_users'),
            );
            if ($this->Tbl_setting_rekening_listrik->update($id,$data)) {
                $this->session->set_flashdata('message','Data berhasil diubah.');
                $this->session->set_flashdata('type_message','success');
                redirect('Daftar/Settings/RekeningListrik/');
            }else{
                $this->session->set_flashdata('message','Terjadi kesalahan dalam edit data.');
                $this->session->set_flashdata('type_message','danger');
                redirect('Daftar/Settings/RekeningListrik/');
            }
        }
    }

    function Delete($id){
        if ($this->Tbl_setting_rekening_listrik->delete($id)) {
            $this->session->set_flashdata('message','Data berhasil dihapus.');
            $this->session->set_flashdata('type_message','success');
            redirect('Daftar/Settings/RekeningListrik/');
        }else{
            $this->session->set_flashdata('message','Terjadi kesalahan dalam hapus data.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Daftar/Settings/RekeningListrik/');
        }
    }

}
