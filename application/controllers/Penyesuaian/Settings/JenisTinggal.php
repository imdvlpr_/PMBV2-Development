<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class JenisTinggal extends CI_Controller {

	function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        } 
        if ($this->session->userdata('level') != "DEVELOPMENT") {       
            $this->session->set_flashdata('message','Hak Akses Ditolak.');      
            $this->session->set_flashdata('type_message','danger');      
            redirect('Dashboard');
        }
        $this->load->model('Settings/Tbl_setting_jenis_tinggal');
    }

	function index(){
		$tbSJenisTinggal = $this->Tbl_setting_jenis_tinggal->read()->result();
		$data = array(
			'tbSJenisTinggal' => $tbSJenisTinggal,
		);
		$this->load->view('daftar/settings/jenis_tinggal',$data);
	}

	function Tambah(){
		$rules[] = array('field' => 'jenis_tinggal',	'label' => 'Nama Daerah', 'rules' => 'required');
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('message',validation_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('Daftar/Settings/JenisTinggal/');
		}else{
			$data = array(
				'jenis_tinggal' => strtoupper($this->input->post('jenis_tinggal')),
                'created_by'     => $this->session->userdata('id_users'),
                'updated_by'     => $this->session->userdata('id_users'),
			);
			if ($this->Tbl_setting_jenis_tinggal->create($data)) {
				$this->session->set_flashdata('message','Data berhasil disimpan.');
            	$this->session->set_flashdata('type_message','success');
            	redirect('Daftar/Settings/JenisTinggal/');
			}else{
				$this->session->set_flashdata('message','Terjadi kesalahan dalam tambah data.');
            	$this->session->set_flashdata('type_message','danger');
            	redirect('Daftar/Settings/JenisTinggal/');
			}
		}
	}

	function Edit($id){
		$tbSJenisTinggal = $this->Tbl_setting_jenis_tinggal->whereAnd(array('id_jns_tinggal' => $id))->row();
		$data = array(
			'tbSJenisTinggal' => $tbSJenisTinggal,
		);
		$this->load->view('daftar/settings/jenis_tinggal_edit',$data);
	}

	function Update($id){
        $rules[] = array('field' => 'jenis_tinggal', 'label' => 'Nama Daerah', 'rules' => 'required');
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('message',validation_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('Daftar/Settings/JenisTinggal/');
		}else{
			$data = array(
				'jenis_tinggal' => strtoupper($this->input->post('jenis_tinggal')),
                'updated_by'     => $this->session->userdata('id_users'),
			);
			if ($this->Tbl_setting_jenis_tinggal->update($id,$data)) {
				$this->session->set_flashdata('message','Data berhasil diubah.');
            	$this->session->set_flashdata('type_message','success');
            	redirect('Daftar/Settings/JenisTinggal/');
			}else{
				$this->session->set_flashdata('message','Terjadi kesalahan dalam edit data.');
            	$this->session->set_flashdata('type_message','danger');
            	redirect('Daftar/Settings/JenisTinggal/');
			}
		}
	}

	function Delete($id){
		if ($this->Tbl_setting_jenis_tinggal->delete($id)) {
			$this->session->set_flashdata('message','Data berhasil dihapus.');
            $this->session->set_flashdata('type_message','success');
            redirect('Daftar/Settings/JenisTinggal/');
		}else{
			$this->session->set_flashdata('message','Terjadi kesalahan dalam hapus data.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Daftar/Settings/JenisTinggal/');
		}
	}

}
