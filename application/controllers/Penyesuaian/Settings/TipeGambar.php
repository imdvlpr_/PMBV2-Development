<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class TipeGambar extends CI_Controller {

	function __construct(){
        parent::__construct();
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != 'DEVELOPMENT') {
            $this->session->set_flashdata('message','Hak akses ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        $this->load->model('Settings/Tbl_setting_tipe_gambar');
    }

	function index(){
		$tbSTipeGambar = $this->Tbl_setting_tipe_gambar->read()->result();
		$data = array(
			'tbSTipeGambar' => $tbSTipeGambar,
		);
		$this->load->view('daftar/settings/tipe_gambar',$data);
	}

	function Tambah(){
	    $rules[] = array('field' => 'tipe_gambar', 'label' => 'Tipe Ujian', 'rules' => 'required');
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('message',validation_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('Daftar/Settings/TipeGambar/');
		}else{
			$data = array(
				'tipe_gambar' 	=> strtoupper($this->input->post('tipe_gambar')),
                'created_by'     => $this->session->userdata('id_users'),
                'updated_by'     => $this->session->userdata('id_users'),
			);
			if ($this->Tbl_setting_tipe_gambar->create($data)) {
				$this->session->set_flashdata('message','Data berhasil disimpan.');
            	$this->session->set_flashdata('type_message','success');
            	redirect('Daftar/Settings/TipeGambar/');
			}else{
				$this->session->set_flashdata('message','Terjadi kesalahan dalam tambah data.');
            	$this->session->set_flashdata('type_message','danger');
            	redirect('Daftar/Settings/TipeGambar/');
			}
		}
	}

	function Edit($id){
		$tbSTipeGambar = $this->Tbl_setting_tipe_gambar->whereAnd(array('id_tipe_gambar' => $id))->row();
		$data = array(
			'tbSTipeGambar' => $tbSTipeGambar,
		);
		$this->load->view('daftar/settings/tipe_gambar_edit',$data);
	}

	function Update($id){
        $rules[] = array('field' => 'tipe_gambar', 'label' => 'Tipe Ujian', 'rules' => 'required');
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('message',validation_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('Daftar/Settings/TipeGambar/');
		}else{
			$data = array(
				'tipe_gambar' 	=> strtoupper($this->input->post('tipe_gambar')),
                'updated_by'     => $this->session->userdata('id_users'),
			);
			if ($this->Tbl_setting_tipe_gambar->update($id,$data)) {
				$this->session->set_flashdata('message','Data berhasil diubah.');
            	$this->session->set_flashdata('type_message','success');
            	redirect('Daftar/Settings/TipeGambar/');
			}else{
				$this->session->set_flashdata('message','Terjadi kesalahan dalam edit data.');
            	$this->session->set_flashdata('type_message','danger');
            	redirect('Daftar/Settings/TipeGambar/');
			}
		}
	}

	function Delete($id){
		if ($this->Tbl_setting_tipe_gambar->delete($id)) {
			$this->session->set_flashdata('message','Data berhasil dihapus.');
            $this->session->set_flashdata('type_message','success');
            redirect('Daftar/Settings/TipeGambar/');
		}else{
			$this->session->set_flashdata('message','Terjadi kesalahan dalam hapus data.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Daftar/Settings/TipeGambar/');
		}
	}

}

