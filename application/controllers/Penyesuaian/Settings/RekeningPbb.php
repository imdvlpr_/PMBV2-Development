<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class RekeningPbb extends CI_Controller {

	function __construct(){
        parent::__construct();
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != "DEVELOPMENT") {
            $this->session->set_flashdata('message','Hak Akses Ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Dashboard');
        }
        $this->load->model('Settings/Tbl_setting_rekening_pbb');
    }

	function index(){
		$tbSRekeningPBB = $this->Tbl_setting_rekening_pbb->read()->result();
		$data = array(
			'tbSRekeningPBB' => $tbSRekeningPBB,
		);
		$this->load->view('daftar/settings/rekening_pbb',$data);
	}

	function Tambah(){
	    $rules[] = array('field' => 'rekening_pbb', 'label' => 'Rekening PBB', 'rules' => 'required');
	    $rules[] = array('field' => 'nilai', 'label' => 'Nilai', 'rules' => 'required');
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('message',validation_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('Daftar/Settings/RekeningPbb/');
		}else{
			$data = array(
				'rekening_pbb'  => strtoupper($this->input->post('rekening_pbb')),
				'nilai'         => $this->input->post('nilai'),
                'created_by'     => $this->session->userdata('id_users'),
                'updated_by'     => $this->session->userdata('id_users'),
			);
			if ($this->Tbl_setting_rekening_pbb->create($data)) {
				$this->session->set_flashdata('message','Data berhasil disimpan.');
            	$this->session->set_flashdata('type_message','success');
            	redirect('Daftar/Settings/RekeningPbb/');
			}else{
				$this->session->set_flashdata('message','Terjadi kesalahan dalam tambah data.');
            	$this->session->set_flashdata('type_message','danger');
            	redirect('Daftar/Settings/RekeningPbb/');
			}
		}
	}

	function Edit($id){
		$tbSRekeningPBB = $this->Tbl_setting_rekening_pbb->whereAnd(array('id_rekening_pbb' => $id))->row();
		$data = array(
			'tbSRekeningPBB' => $tbSRekeningPBB,
		);
		$this->load->view('daftar/settings/rekening_pbb_edit',$data);
	}

	function Update($id){
        $rules[] = array('field' => 'rekening_pbb', 'label' => 'Rekening PBB', 'rules' => 'required');
        $rules[] = array('field' => 'nilai', 'label' => 'Nilai', 'rules' => 'required');
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('message',validation_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('Daftar/Settings/RekeningPbb/');
		}else{
			$data = array(
                'rekening_pbb'  => strtoupper($this->input->post('rekening_pbb')),
                'nilai'         => $this->input->post('nilai'),
                'updated_by'     => $this->session->userdata('id_users'),
			);
			if ($this->Tbl_setting_rekening_pbb->update($id,$data)) {
				$this->session->set_flashdata('message','Data berhasil diubah.');
            	$this->session->set_flashdata('type_message','success');
            	redirect('Daftar/Settings/RekeningPbb/');
			}else{
				$this->session->set_flashdata('message','Terjadi kesalahan dalam edit data.');
            	$this->session->set_flashdata('type_message','danger');
            	redirect('Daftar/Settings/RekeningPbb/');
			}
		}
	}

	function Delete($id){
		if ($this->Tbl_setting_rekening_pbb->delete($id)) {
			$this->session->set_flashdata('message','Data berhasil dihapus.');
            $this->session->set_flashdata('type_message','success');
            redirect('Daftar/Settings/RekeningPbb/');
		}else{
			$this->session->set_flashdata('message','Terjadi kesalahan dalam hapus data.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Daftar/Settings/RekeningPbb/');
		}
	}

}
