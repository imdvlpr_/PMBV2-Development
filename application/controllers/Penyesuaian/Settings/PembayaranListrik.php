<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class PembayaranListrik extends CI_Controller {

    function __construct(){
        parent::__construct();
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != "DEVELOPMENT") {
            $this->session->set_flashdata('message','Hak Akses Ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Dashboard');
        }
        $this->load->model('Settings/Tbl_setting_pembayaran_listrik');
    }

    function index(){
        $tbSPembayaranListrik = $this->Tbl_setting_pembayaran_listrik->read()->result();
        $data = array(
            'tbSPembayaranListrik' => $tbSPembayaranListrik,
        );
        $this->load->view('daftar/settings/pembayaran_listrik',$data);
    }

    function Tambah(){
        $rules[] = array('field' => 'pembayaran_listrik', 'label' => 'Pembayaran Listrik', 'rules' => 'required');
        $rules[] = array('field' => 'nilai', 'label' => 'Nilai', 'rules' => 'required');
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == FALSE){
            $this->session->set_flashdata('message',validation_errors());
            $this->session->set_flashdata('type_message','danger');
            redirect('Daftar/Settings/PembayaranListrik/');
        }else{
            $data = array(
                'pembayaran_listrik'    => strtoupper($this->input->post('pembayaran_listrik')),
                'nilai'                 => $this->input->post('nilai'),
                'created_by'             => $this->session->userdata('id_users'),
                'updated_by'             => $this->session->userdata('id_users'),
            );
            if ($this->Tbl_setting_pembayaran_listrik->create($data)) {
                $this->session->set_flashdata('message','Data berhasil disimpan.');
                $this->session->set_flashdata('type_message','success');
                redirect('Daftar/Settings/PembayaranListrik/');
            }else{
                $this->session->set_flashdata('message','Terjadi kesalahan dalam tambah data.');
                $this->session->set_flashdata('type_message','danger');
                redirect('Daftar/Settings/PembayaranListrik/');
            }
        }
    }

    function Edit($id){
        $tbSPembayaranListrik = $this->Tbl_setting_pembayaran_listrik->whereAnd(array('id_pembayaran_listrik' => $id))->row();
        $data = array(
            'tbSPembayaranListrik' => $tbSPembayaranListrik,
        );
        $this->load->view('daftar/settings/pembayaran_listrik_edit',$data);
    }

    function Update($id){
        $rules[] = array('field' => 'pembayaran_listrik', 'label' => 'Pembayaran Listrik', 'rules' => 'required');
        $rules[] = array('field' => 'nilai', 'label' => 'Nilai', 'rules' => 'required');
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == FALSE){
            $this->session->set_flashdata('message',validation_errors());
            $this->session->set_flashdata('type_message','danger');
            redirect('Daftar/Settings/PembayaranListrik/');
        }else{
            $data = array(
                'pembayaran_listrik'    => strtoupper($this->input->post('pembayaran_listrik')),
                'nilai'                 => $this->input->post('nilai'),
                'updated_by'             => $this->session->userdata('id_users'),
            );
            if ($this->Tbl_setting_pembayaran_listrik->update($id,$data)) {
                $this->session->set_flashdata('message','Data berhasil diubah.');
                $this->session->set_flashdata('type_message','success');
                redirect('Daftar/Settings/PembayaranListrik/');
            }else{
                $this->session->set_flashdata('message','Terjadi kesalahan dalam edit data.');
                $this->session->set_flashdata('type_message','danger');
                redirect('Daftar/Settings/PembayaranListrik/');
            }
        }
    }

    function Delete($id){
        if ($this->Tbl_setting_pembayaran_listrik->delete($id)) {
            $this->session->set_flashdata('message','Data berhasil dihapus.');
            $this->session->set_flashdata('type_message','success');
            redirect('Daftar/Settings/PembayaranListrik');
        }else{
            $this->session->set_flashdata('message','Terjadi kesalahan dalam hapus data.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Daftar/Settings/PembayaranListrik');
        }
    }

}
