<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class AlatTransportasi extends CI_Controller {

    function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != "DEVELOPMENT") {
            $this->session->set_flashdata('message','Hak Akses Ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Dashboard');
        }
        $this->load->model('Settings/Tbl_setting_alat_transportasi');
    }

    function index(){
        $tbSAlatTransportasi = $this->Tbl_setting_alat_transportasi->read()->result();
        $data = array(
            'tbSAlatTransportasi' => $tbSAlatTransportasi,
        );
        $this->load->view('daftar/settings/alat_transportasi',$data);
    }

    function Tambah(){
        $rules[] = array('field' => 'alat_transportasi', 'label' => 'Alat Transportasi', 'rules' => 'required');
        $rules[] = array('field' => 'nilai', 'label' => 'Nilai', 'rules' => 'required');
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == FALSE){
            $this->session->set_flashdata('message',validation_errors());
            $this->session->set_flashdata('type_message','danger');
            redirect('Daftar/Settings/AlatTransportasi/');
        }else{
            $data = array(
                'alat_transportasi' => strtoupper($this->input->post('alat_transportasi')),
                'nilai'             => $this->input->post('nilai'),
                'created_by'         => $this->session->userdata('id_users'),
                'updated_by'         => $this->session->userdata('id_users'),
            );
            if ($this->Tbl_setting_alat_transportasi->create($data)) {
                $this->session->set_flashdata('message','Data berhasil disimpan.');
                $this->session->set_flashdata('type_message','success');
                redirect('Daftar/Settings/AlatTransportasi/');
            }else{
                $this->session->set_flashdata('message','Terjadi kesalahan dalam tambah data.');
                $this->session->set_flashdata('type_message','danger');
                redirect('Daftar/Settings/AlatTransportasi/');
            }
        }
    }

    function Edit($id){
        $tbSAlatTransportasi = $this->Tbl_setting_alat_transportasi->whereAnd(array('id_alat_transportasi' => $id))->row();
        $data = array(
            'tbSAlatTransportasi' => $tbSAlatTransportasi,
        );
        $this->load->view('daftar/settings/alat_transportasi_edit',$data);
    }

    function Update($id){
        $rules[] = array('field' => 'alat_transportasi', 'label' => 'Alat Transportasi', 'rules' => 'required');
        $rules[] = array('field' => 'nilai', 'label' => 'Nilai', 'rules' => 'required');
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == FALSE){
            $this->session->set_flashdata('message',validation_errors());
            $this->session->set_flashdata('type_message','danger');
            redirect('Daftar/Settings/AlatTransportasi/');
        }else{
            $data = array(
                'alat_transportasi' => strtoupper($this->input->post('alat_transportasi')),
                'nilai'             => $this->input->post('nilai'),
                'updated_by'         => $this->session->userdata('id_users'),
            );
            if ($this->Tbl_setting_alat_transportasi->update($id,$data)) {
                $this->session->set_flashdata('message','Data berhasil diubah.');
                $this->session->set_flashdata('type_message','success');
                redirect('Daftar/Settings/AlatTransportasi/');
            }else{
                $this->session->set_flashdata('message','Terjadi kesalahan dalam edit data.');
                $this->session->set_flashdata('type_message','danger');
                redirect('Daftar/Settings/AlatTransportasi/');
            }
        }
    }

    function Delete($id){
        if ($this->Tbl_setting_alat_transportasi->delete($id)) {
            $this->session->set_flashdata('message','Data berhasil dihapus.');
            $this->session->set_flashdata('type_message','success');
            redirect('Daftar/Settings/AlatTransportasi/');
        }else{
            $this->session->set_flashdata('message','Terjadi kesalahan dalam hapus data.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Daftar/Settings/AlatTransportasi/');
        }
    }

}

