<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Tanggungan extends CI_Controller {

	function __construct(){
        parent::__construct();
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != "DEVELOPMENT") {
            $this->session->set_flashdata('message','Hak Akses Ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Dashboard');
        }
        $this->load->model('Settings/Tbl_setting_tanggungan');
    }

	function index(){
		$tbSTanggungan = $this->Tbl_setting_tanggungan->read()->result();
		$data = array(
			'tbSTanggungan' => $tbSTanggungan,
		);
		$this->load->view('daftar/settings/tanggungan',$data);
	}

	function Tambah(){
	    $rules[] = array('field' => 'tanggungan', 'label' => 'Tanggungan', 'rules' => 'required');
	    $rules[] = array('field' => 'nilai', 'label' => 'Nilai', 'rules' => 'required');
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('message',validation_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('Daftar/Settings/Tanggungan/');
		}else{
			$data = array(
				'tanggungan'    => strtoupper($this->input->post('tanggungan')),
				'nilai'         => $this->input->post('nilai'),
                'created_by'     => $this->session->userdata('id_users'),
                'updated_by'     => $this->session->userdata('id_users'),
			);
			if ($this->Tbl_setting_tanggungan->create($data)) {
				$this->session->set_flashdata('message','Data berhasil disimpan.');
            	$this->session->set_flashdata('type_message','success');
            	redirect('Daftar/Settings/Tanggungan/');
			}else{
				$this->session->set_flashdata('message','Terjadi kesalahan dalam tambah data.');
            	$this->session->set_flashdata('type_message','danger');
            	redirect('Daftar/Settings/Tanggungan/');
			}
		}
	}

	function Edit($id){
		$tbSTanggungan = $this->Tbl_setting_tanggungan->whereAnd(array('id_tanggungan' => $id))->row();
		$data = array(
			'tbSTanggungan' => $tbSTanggungan,
		);
		$this->load->view('daftar/settings/tanggungan_edit',$data);
	}

	function Update($id){
        $rules[] = array('field' => 'tanggungan', 'label' => 'Tanggungan', 'rules' => 'required');
        $rules[] = array('field' => 'nilai', 'label' => 'Nilai', 'rules' => 'required');
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('message',validation_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('Daftar/Settings/Tanggungan/');
		}else{
			$data = array(
                'tanggungan'    => strtoupper($this->input->post('tanggungan')),
                'nilai'         => $this->input->post('nilai'),
                'updated_by'     => $this->session->userdata('id_users'),
			);
			if ($this->Tbl_setting_tanggungan->update($id,$data)) {
				$this->session->set_flashdata('message','Data berhasil diubah.');
            	$this->session->set_flashdata('type_message','success');
            	redirect('Daftar/Settings/Tanggungan/');
			}else{
				$this->session->set_flashdata('message','Terjadi kesalahan dalam edit data.');
            	$this->session->set_flashdata('type_message','danger');
            	redirect('Daftar/Settings/Tanggungan/');
			}
		}
	}

	function Delete($id){
		if ($this->Tbl_setting_tanggungan->delete($id)) {
			$this->session->set_flashdata('message','Data berhasil dihapus.');
            $this->session->set_flashdata('type_message','success');
            redirect('Daftar/Settings/Tanggungan/');
		}else{
			$this->session->set_flashdata('message','Terjadi kesalahan dalam hapus data.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Daftar/Settings/Tanggungan/');
		}
	}

}
