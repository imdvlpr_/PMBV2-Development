<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class JenisPendaftaran extends CI_Controller {

	function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != "DEVELOPMENT") {       
            $this->session->set_flashdata('message','Hak Akses Ditolak.');      
            $this->session->set_flashdata('type_message','danger');      
            redirect('Dashboard');
        }
        $this->load->model('Settings/Tbl_setting_jenis_pendaftaran');
    }

	function index(){
		$tbSJenisPendaftaran = $this->Tbl_setting_jenis_pendaftaran->read()->result();
		$data = array(
			'tbSJenisPendaftaran' => $tbSJenisPendaftaran,
		);
		$this->load->view('daftar/settings/jenis_pendaftaran',$data);
	}

	function Tambah(){
		$rules[] = array('field' => 'jenis_pendaftaran',	'label' => 'Nama Daerah', 'rules' => 'required');
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('message',validation_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('Daftar/Settings/JenisPendaftaran/');
		}else{
			$data = array(
				'jenis_pendaftaran' => strtoupper($this->input->post('jenis_pendaftaran')),
                'created_by'         => $this->session->userdata('id_users'),
                'updated_by'         => $this->session->userdata('id_users'),
			);
			if ($this->Tbl_setting_jenis_pendaftaran->create($data)) {
				$this->session->set_flashdata('message','Data berhasil disimpan.');
            	$this->session->set_flashdata('type_message','success');
            	redirect('Daftar/Settings/JenisPendaftaran/');
			}else{
				$this->session->set_flashdata('message','Terjadi kesalahan dalam tambah data.');
            	$this->session->set_flashdata('type_message','danger');
            	redirect('Daftar/Settings/JenisPendaftaran/');
			}
		}
	}

	function Edit($id){
		$tbSJenisPendaftaran = $this->Tbl_setting_jenis_pendaftaran->whereAnd(array('id_jns_pndftrn' => $id))->row();
		$data = array(
			'tbSJenisPendaftaran' => $tbSJenisPendaftaran,
		);
		$this->load->view('daftar/settings/jenis_pendaftaran_edit',$data);
	}

	function Update($id){
        $rules[] = array('field' => 'jenis_pendaftaran',	'label' => 'Nama Daerah', 'rules' => 'required');
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('message',validation_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('Daftar/Settings/JenisPendaftaran/');
		}else{
			$data = array(
				'jenis_pendaftaran' => strtoupper($this->input->post('jenis_pendaftaran')),
                'updated_by'         => $this->session->userdata('id_users'),
			);
			if ($this->Tbl_setting_jenis_pendaftaran->update($id,$data)) {
				$this->session->set_flashdata('message','Data berhasil diubah.');
            	$this->session->set_flashdata('type_message','success');
            	redirect('Daftar/Settings/JenisPendaftaran/');
			}else{
				$this->session->set_flashdata('message','Terjadi kesalahan dalam edit data.');
            	$this->session->set_flashdata('type_message','danger');
            	redirect('Daftar/Settings/JenisPendaftaran/');
			}
		}
	}

	function Delete($id){
		if ($this->Tbl_setting_jenis_pendaftaran->delete($id)) {
			$this->session->set_flashdata('message','Data berhasil dihapus.');
            $this->session->set_flashdata('type_message','success');
            redirect('Daftar/Settings/JenisPendaftaran/');
		}else{
			$this->session->set_flashdata('message','Terjadi kesalahan dalam hapus data.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Daftar/Settings/JenisPendaftaran/');
		}
	}

}
