<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Users extends CI_Controller {

	function __construct(){
		parent::__construct();
		if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
		if ($this->session->userdata('level') != 'DEVELOPMENT') {
			$this->session->set_flashdata('message','Hak akses ditolak.');
			$this->session->set_flashdata('type_message','danger');
			redirect('Auth');
		}
		$this->load->model('ServerSide/SS_daftar_users', '', TRUE);
        $this->load->model('ServerSide/SS_penyesuaian_users', '', TRUE);
		$this->load->model('Daftar/Tbl_daftar_users');
        $this->load->model('Daftar/Tbl_daftar_gambar');
		$this->load->model('Daftar/Tbl_daftar_extra');
		$this->load->model('Daftar/Tbl_daftar_pddikti');
        $this->load->model('Penyesuaian/Tbl_penyesuaian_users');
        $this->load->model('Penyesuaian/Tbl_penyesuaian_gambar');
        $this->load->model('Penyesuaian/Tbl_penyesuaian_extra');
        $this->load->model('Penyesuaian/Tbl_penyesuaian_pddikti');
        $this->load->model('Views/Daftar/View_daftar_users');
        $this->load->model('Views/Daftar/View_daftar_pddikti');
        $this->load->model('Views/Daftar/View_daftar_kelulusan');
        $this->load->model('Views/Daftar/View_daftar_ukt');
        $this->load->model('Views/Daftar/View_daftar_pddikti_mahasiswa');
        $this->load->model('Views/Daftar/View_daftar_pddikti_ayah');
        $this->load->model('Views/Daftar/View_daftar_pddikti_ibu');
        $this->load->model('Views/Daftar/View_daftar_pddikti_wali');
		$this->load->model('Views/Daftar/View_daftar_extra');
        $this->load->model('Views/Penyesuaian/View_penyesuaian_users');
        $this->load->model('Views/Penyesuaian/View_penyesuaian_pddikti');
        $this->load->model('Views/Penyesuaian/View_penyesuaian_gambar');
        $this->load->model('Views/Penyesuaian/View_penyesuaian_ukt');
        $this->load->model('Views/Penyesuaian/View_penyesuaian_pddikti_mahasiswa');
        $this->load->model('Views/Penyesuaian/View_penyesuaian_pddikti_ayah');
        $this->load->model('Views/Penyesuaian/View_penyesuaian_pddikti_ibu');
        $this->load->model('Views/Penyesuaian/View_penyesuaian_pddikti_wali');
        $this->load->model('Views/Penyesuaian/View_penyesuaian_extra');
        $this->load->model('Settings/Tbl_setting_agama');
        $this->load->model('Settings/Tbl_setting_provinsi');
        $this->load->model('Settings/Tbl_setting_kabupaten');
        $this->load->model('Settings/Tbl_setting_kecamatan');
        $this->load->model('Settings/Tbl_setting_kelurahan');
        $this->load->model('Settings/Tbl_setting_jenis_pendaftaran');
        $this->load->model('Settings/Tbl_setting_jenis_tinggal');
        $this->load->model('Settings/Tbl_setting_alat_transportasi');
        $this->load->model('Settings/Tbl_setting_pendidikan');
        $this->load->model('Settings/Tbl_setting_pekerjaan');
        $this->load->model('Settings/Tbl_setting_penghasilan');
        $this->load->model('Settings/Tbl_setting_rekening_listrik');
        $this->load->model('Settings/Tbl_setting_pembayaran_listrik');
        $this->load->model('Settings/Tbl_setting_rekening_pbb');
        $this->load->model('Settings/Tbl_setting_pembayaran_pbb');
        $this->load->model('Settings/Tbl_setting_tanggungan');
        $this->load->model('Settings/Tbl_setting_rumpun');
	}

	function index(){
        $data = array(
            'content'       => 'Penyesuaian/Users/content',
            'css'           => 'Penyesuaian/Users/css',
            'javascript'    => 'Penyesuaian/Users/javascript',
            'modal'         => 'Penyesuaian/Users/modal',
        );
		$this->load->view('index', $data);
	}

    function Detail($nik_passport,$nomor_peserta){
        $rules = array(
            'select'    => null,
            'where'     => array(
                'nik_passport' => $nik_passport,
            ),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $rules2 = array(
            'select'    => null,
            'where'     => array(
                'nomor_peserta' => $nomor_peserta,
            ),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        /*$viewDaftarUsers = $this->View_daftar_users->where($rules)->row();*/
        $tbFileUpload = $this->View_penyesuaian_gambar->where($rules)->result();
        $viewDaftarPddikti = $this->View_penyesuaian_pddikti->where($rules)->row();
        $viewDaftarUsers = $this->View_penyesuaian_users->where($rules)->row();
        $viewDaftarKelulusan = $this->View_daftar_kelulusan->where($rules2)->row();
        $viewDaftarUkt = $this->View_penyesuaian_ukt->where($rules)->row();
        $viewDaftarAyah = $this->View_penyesuaian_pddikti_ayah->where($rules)->row();
        $viewDaftarIbu = $this->View_penyesuaian_pddikti_ibu->where($rules)->row();
        $viewDaftarWali = $this->View_penyesuaian_pddikti_wali->where($rules)->row();
        $viewDaftarExtra = $this->View_penyesuaian_extra->where($rules)->row();
        $data = array(
            'content'       => 'Penyesuaian/Users/users_detail/content',
            'css'           => 'Penyesuaian/Users/users_detail/css',
            'javascript'    => 'Penyesuaian/Users/users_detail/javascript',
            'modal'         => 'Penyesuaian/Users/users_detail/modal',
            'tbFileUpload' => $tbFileUpload,
            'viewDaftarUsers'   => $viewDaftarUsers,
            'viewDaftarPddikti'   => $viewDaftarPddikti,
            'viewDaftarKelulusan'   => $viewDaftarKelulusan,
            'viewDaftarUkt'   => $viewDaftarUkt,
            'viewDaftarAyah'   => $viewDaftarAyah,
            'viewDaftarIbu'   => $viewDaftarIbu,
            'viewDaftarWali'   => $viewDaftarWali,
            'viewDaftarExtra'   => $viewDaftarExtra,
        );
        $this->load->view('index', $data);
    }

	function Edit($id){
        echo $id;
	}

	function Edit_langkah1_mahasiswa($nik){
        $rules = array(
            'select'    => null,
            'where'     => array(
                'nik_passport' => $nik,
            ),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $rules2 = array(
            'select'    => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $tbJenisTinggal         = $this->Tbl_setting_jenis_tinggal->read($rules2)->result();
        $tbAlatTransportasi     = $this->Tbl_setting_alat_transportasi->read($rules2)->result();
        $tbSAG              = $this->Tbl_setting_agama->read($rules2)->result();
        $tbRumpun              = $this->Tbl_setting_rumpun->read($rules2)->result();
        $tbBM               = $this->Tbl_penyesuaian_pddikti->where($rules)->row();
        $whereVBPDDIKTI     = $this->View_penyesuaian_pddikti->where($rules)->row();
        $data = array(
            'content'       => 'Penyesuaian/Users/Langkah1/users_langkah1_mahasiswa_edit/content',
            'css'           => 'Penyesuaian/Users/Langkah1/users_langkah1_mahasiswa_edit/css',
            'javascript'    => 'Penyesuaian/Users/Langkah1/users_langkah1_mahasiswa_edit/javascript',
            'modal'         => 'Penyesuaian/Users/Langkah1/users_langkah1_mahasiswa_edit/modal',
            'tbJenisTinggal' => $tbJenisTinggal,
            'tbAlatTransportasi' => $tbAlatTransportasi,
            'tbSAG' => $tbSAG,
            'tbBM'  => $tbBM,
            'tbRumpun'  => $tbRumpun,
            'viewBM'=> $whereVBPDDIKTI,
        );
        $this->load->view('index',$data);
    }
    
    function Edit_langkah1_alamat($nik){
        $rules = array(
            'select'    => null,
            'where'     => array(
                'nik_passport' => $nik,
            ),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $rules2 = array(
            'select'    => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $tbJenisPendaftaran     = $this->Tbl_setting_jenis_pendaftaran->read($rules2)->result();
        $tbProv                 = $this->Tbl_setting_provinsi->read($rules2)->result();
        $tbBM                   = $this->Tbl_penyesuaian_pddikti->where($rules)->row();
        $whereVBPDDIKTI         = $this->View_penyesuaian_pddikti->where($rules)->row();
        $data = array(
            'content'       => 'Penyesuaian/Users/Langkah1/users_langkah1_alamat_edit/content',
            'css'           => 'Penyesuaian/Users/Langkah1/users_langkah1_alamat_edit/css',
            'javascript'    => 'Penyesuaian/Users/Langkah1/users_langkah1_alamat_edit/javascript',
            'modal'         => 'Penyesuaian/Users/Langkah1/users_langkah1_alamat_edit/modal',
            'tbJenisPendaftaran' => $tbJenisPendaftaran,
            'tbProv'  => $tbProv,
            'tbBM'  => $tbBM,
            'viewBM'=> $whereVBPDDIKTI,
        );
        $this->load->view('index',$data);
    }

    function listKabupaten(){
        $id_provinsi = $this->input->post('id_provinsi');
        
        $kabupaten = $this->Tbl_setting_kabupaten->viewByProvinsi($id_provinsi);
        
        $lists = "<option value=''>Pilih</option>";
        
        foreach($kabupaten as $data){
          $lists .= "<option value='".$data->id_kabupaten."'>".$data->kabupaten."</option>";
        }
        
        $callback = array('list_kabupaten'=>$lists);
        echo json_encode($callback);
    }

    function listKecamatan(){
        $id_kabupaten = $this->input->post('id_kabupaten');
        
        $kecamatan = $this->Tbl_setting_kecamatan->viewByKabupaten($id_kabupaten);
        
        $lists = "<option value=''>Pilih</option>";
        
        foreach($kecamatan as $data){
          $lists .= "<option value='".$data->id_kecamatan."'>".$data->kecamatan."</option>";
        }
        
        $callback = array('list_kecamatan'=>$lists);
        echo json_encode($callback);
    }

    function listKelurahan(){
        $id_kecamatan = $this->input->post('id_kecamatan');
        
        $kelurahan = $this->Tbl_setting_kelurahan->viewByKecamatan($id_kecamatan);
        
        $lists = "<option value=''>Pilih</option>";
        
        foreach($kelurahan as $data){
          $lists .= "<option value='".$data->id_kelurahan."'>".$data->kelurahan."</option>";
        }
        
        $callback = array('list_kelurahan'=>$lists);
        echo json_encode($callback);
    }
    
    function Edit_langkah1_orangtua($nik){
        $rules = array(
            'select'    => null,
            'where'     => array(
                'nik_passport' => $nik,
            ),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $rules2 = array(
            'select'    => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $tbPendidikan           = $this->Tbl_setting_pendidikan->read($rules2)->result();
        $tbPekerjaan            = $this->Tbl_setting_pekerjaan->read($rules2)->result();
        $tbPenghasilan          = $this->Tbl_setting_penghasilan->read($rules2)->result();
        $tbExtra                = $this->Tbl_penyesuaian_extra->where($rules)->row();
        $tbBM                   = $this->Tbl_penyesuaian_pddikti->where($rules)->row();
        $whereVBPDDIKTI         = $this->View_penyesuaian_pddikti->where($rules)->row();
        $data = array(
            'content'       => 'Penyesuaian/Users/Langkah1/users_langkah1_orangtua_edit/content',
            'css'           => 'Penyesuaian/Users/Langkah1/users_langkah1_orangtua_edit/css',
            'javascript'    => 'Penyesuaian/Users/Langkah1/users_langkah1_orangtua_edit/javascript',
            'modal'         => 'Penyesuaian/Users/Langkah1/users_langkah1_orangtua_edit/modal',
            'tbPendidikan' => $tbPendidikan,
            'tbPekerjaan' => $tbPekerjaan,
            'tbPenghasilan' => $tbPenghasilan,
            'tbBM'  => $tbBM,
            'tbExtra'  => $tbExtra,
            'viewBM'=> $whereVBPDDIKTI,
        );
        $this->load->view('index',$data);
    }
    
    function Edit_langkah1_foto($nik){
        $rules = array(
            'select'    => null,
            'where'     => array(
                'nik_passport' => $nik,
            ),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $rules2 = array(
            'select'    => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $tbBM                   = $this->Tbl_penyesuaian_pddikti->where($rules)->row();
        $whereVBPDDIKTI         = $this->View_penyesuaian_pddikti->where($rules)->row();
        $data = array(
            'content'       => 'Penyesuaian/Users/Langkah1/users_langkah1_foto_edit/content',
            'css'           => 'Penyesuaian/Users/Langkah1/users_langkah1_foto_edit/css',
            'javascript'    => 'Penyesuaian/Users/Langkah1/users_langkah1_foto_edit/javascript',
            'modal'         => 'Penyesuaian/Users/Langkah1/users_langkah1_foto_edit/modal',
            'tbBM'  => $tbBM,
            'viewBM'=> $whereVBPDDIKTI,
        );
        $this->load->view('index',$data);
    }

    function Edit_langkah2_tambahan($nik){
        $rules = array(
            'select'    => null,
            'where'     => array(
                'nik_passport' => $nik,
            ),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $rules2 = array(
            'select'    => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $tbRekeningListrik      = $this->Tbl_setting_rekening_listrik->read($rules2)->result();
        $tbRekeningPBB          = $this->Tbl_setting_rekening_pbb->read($rules2)->result();
        $tbPembayaranListrik    = $this->Tbl_setting_pembayaran_listrik->read($rules2)->result();
        $tbPembayaranPBB        = $this->Tbl_setting_pembayaran_pbb->read($rules2)->result();
        $tbTanggungan           = $this->Tbl_setting_tanggungan->read($rules2)->result();
        $tbExtra                = $this->Tbl_penyesuaian_extra->where($rules)->row();
        $tbBM                   = $this->Tbl_penyesuaian_pddikti->where($rules)->row();
        $whereVBPDDIKTI         = $this->View_penyesuaian_pddikti->where($rules)->row();
        $data = array(
            'content'       => 'Penyesuaian/Users/Langkah2/users_langkah2_tambahan_edit/content',
            'css'           => 'Penyesuaian/Users/Langkah2/users_langkah2_tambahan_edit/css',
            'javascript'    => 'Penyesuaian/Users/Langkah2/users_langkah2_tambahan_edit/javascript',
            'modal'         => 'Penyesuaian/Users/Langkah2/users_langkah2_tambahan_edit/modal',
            'tbRekeningListrik' => $tbRekeningListrik,
            'tbRekeningPBB' => $tbRekeningPBB,
            'tbPembayaranListrik' => $tbPembayaranListrik,
            'tbPembayaranPBB' => $tbPembayaranPBB,
            'tbTanggungan' => $tbTanggungan,
            'tbExtra' => $tbExtra,
            'tbBM'  => $tbBM,
            'viewBM'=> $whereVBPDDIKTI,
        );
        $this->load->view('index',$data);
    }
    
    function Simpan_tahap1($nik){
        $rules  =   [
            ['field' => 'jk',           'label' => 'Jenis Kelamin',       'rules' => 'required'],
            ['field' => 'tmp_lhr',      'label' => 'Tempat lahir',        'rules' => 'required'],
            ['field' => 'id_agama',     'label' => 'Agama',               'rules' => 'required'],
            ['field' => 'nisn',         'label' => 'NISN',                'rules' => 'required'],
            ['field' => 'npwp',         'label' => 'NPWP'],
            ['field' => 'warga_negara', 'label' => 'Warga Negara',        'rules' => 'required'],
            ['field' => 'tgl_lhr',      'label' => 'Tanggal Lahir',       'rules' => 'required'],
            ['field' => 'id_rumpun',    'label' => 'Rumpun',       'rules' => 'required'],
            ['field' => 'id_jns_tinggal',       'label' => 'Jenis Tinggal',     'rules' => 'required'],
            ['field' => 'id_alat_transportasi', 'label' => 'Alat Transportasi', 'rules' => 'required'],
            ['field' => 'tlp_rmh',              'label' => 'Telpon Rumah'],
            ['field' => 'no_hp',                'label' => 'Nomor HP',          'rules' => 'required'],
            ['field' => 'email',                'label' => 'Email',             'rules' => 'required'],
            ['field' => 'terima_kps',           'label' => 'Terima KPS',        'rules' => 'required'],
            ['field' => 'no_kps',               'label' => 'Nomor KPS'],
        ];
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == FALSE){
            $this->session->set_flashdata('message',validation_errors());
            $this->session->set_flashdata('type_message','danger');
            redirect('Penyesuaian/Users/Edit_langkah1_mahasiswa/'.$nik);
        }else{
            $rules = array(
                'where' => array(
                    'nik_passport' => $nik,
                ),
                'data'  => array(
                    'jenis_kelamin'     => set_value('jk'),
                    'id_agama'          => set_value('id_agama'),
                    'tmp_lhr'           => strtoupper(set_value('tmp_lhr')),
                    'nisn'              => set_value('nisn'),
                    'npwp'              => set_value('npwp'),
                    'warga_negara'      => set_value('warga_negara'),
                    'id_rumpun'      => set_value('id_rumpun'),
                    'id_jns_tinggal'            => set_value('id_jns_tinggal'),
                    'id_alat_transportasi'      => set_value('id_alat_transportasi'),
                    'tlp_rmh'                   => strtoupper(set_value('tlp_rmh')),
                    'terima_kps'                => strtoupper(set_value('terima_kps')),
                    'no_kps'                    => strtoupper(set_value('no_kps')),
                ),
            );
            $rules2 = array(
                'where' => array(
                    'nik_passport' => $nik,
                ),
                'data'  => array(
                    'tgl_lhr'          => set_value('tgl_lhr'),
                    'nmr_hp'                     => set_value('no_hp'),
                ),
            );
            if ($this->Tbl_penyesuaian_pddikti->update($rules)) {
                if ($this->Tbl_penyesuaian_users->update($rules2)) {
                    $this->session->set_flashdata('message','Data berhasil disimpan');
                    $this->session->set_flashdata('type_message','success');
                    redirect('Penyesuaian/Users/Edit_langkah1_mahasiswa/'.$nik);   
                }else{
                    $this->session->set_flashdata('message','Terjadi kesalahan dalam proses simpan data');
                    $this->session->set_flashdata('type_message','danger');
                    redirect('Penyesuaian/Users/Edit_langkah1_mahasiswa/'.$nik);
                }
            }else{
                $this->session->set_flashdata('message','Terjadi kesalahan dalam proses simpan data');
                $this->session->set_flashdata('type_message','danger');
                redirect('Penyesuaian/Users/Edit_langkah1_mahasiswa/'.$nik);
            }
        }
    }
    
    function Simpan_tahap2($nik){
        $rules  =   [
            ['field' => 'alamat',         'label' => 'Alamat',              'rules' => 'required'],
            ['field' => 'rt',             'label' => 'RT',                  'rules' => 'required'],
            ['field' => 'rw',             'label' => 'RW',                  'rules' => 'required'],
            ['field' => 'id_kelurahan',            'label' => 'Kelurahan',           'rules' => 'required'],
            ['field' => 'nama_dusun',     'label' => 'nama_dusun'],
            ['field' => 'kode_pos',       'label' => 'Kode POS',            'rules' => 'required'],
        ];
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == FALSE){
            $this->session->set_flashdata('message',validation_errors());
            $this->session->set_flashdata('type_message','danger');
            redirect('Penyesuaian/Users/Edit_langkah1_alamat/'.$nik);
        }else{
            $rules = array(
                'where' => array(
                    'nik_passport' => $nik,
                ),
                'data'  => array(
                    'jalan'             => strtoupper(set_value('alamat')),
                    'rt'                => set_value('rt'),
                    'rw'                => set_value('rw'),
                    'id_kelurahan'      => set_value('id_kelurahan'),
                    'nama_dusun'        => strtoupper(set_value('nama_dusun')),
                    'kode_pos'          => strtoupper(set_value('kode_pos')),
                ),
            );
            if ($this->Tbl_penyesuaian_pddikti->update($rules)) {
                $this->session->set_flashdata('message','Data berhasil disimpan');
                $this->session->set_flashdata('type_message','success');
                redirect('Penyesuaian/Users/Edit_langkah1_alamat/'.$nik);  
            }else{
                $this->session->set_flashdata('message','Terjadi kesalahan dalam proses simpan data');
                $this->session->set_flashdata('type_message','danger');
                redirect('Penyesuaian/Users/Edit_langkah1_alamat/'.$nik);
            }
        }
    }
    
    function Simpan_tahap4($nik){
        $rules  =   [
            ['field' => 'nik_ayah',       'label' => 'NIK Ayah', 'rules' => 'required'],
            ['field' => 'nama_ayah', 'label' => 'Nama Ayah', 'rules' => 'required'],
            ['field' => 'tgl_lhr_ayah', 'label' => 'Tanggal Lahir Ayah', 'rules' => 'required'],
            ['field' => 'id_pendidikan_ayah',   'label' => 'Pendidikan Ayah', 'rules' => 'required'],
            ['field' => 'id_pekerjaan_ayah',   'label' => 'Pekerjaan Ayah', 'rules' => 'required'],
            ['field' => 'id_penghasilan_ayah',   'label' => 'Penghasilan Ayah', 'rules' => 'required'],
            ['field' => 'nominal_penghasilan_ayah',   'label' => 'Nominal Penghasilan Ayah', 'rules' => 'required'],
            ['field' => 'terbilang_penghasilan_ayah',   'label' => 'Terbilang Penghasilan Ayah', 'rules' => 'required'],
            ['field' => 'nik_ibu',       'label' => 'NIK Ibu', 'rules' => 'required'],
            ['field' => 'nama_ibu', 'label' => 'Nama Ibu', 'rules' => 'required'],
            ['field' => 'tgl_lhr_ibu', 'label' => 'Tanggal Lahir Ibu', 'rules' => 'required'],
            ['field' => 'id_pendidikan_ibu',   'label' => 'Pendidikan Ibu', 'rules' => 'required'],
            ['field' => 'id_pekerjaan_ibu',   'label' => 'Pekerjaan Ibu', 'rules' => 'required'],
            ['field' => 'id_penghasilan_ibu',   'label' => 'Penghasilan Ibu', 'rules' => 'required'],
            ['field' => 'nominal_penghasilan_ibu',   'label' => 'Nominal Penghasilan ibu', 'rules' => 'required'],
            ['field' => 'terbilang_penghasilan_ibu',   'label' => 'Terbilang Penghasilan ibu', 'rules' => 'required'],
            /*['field' => 'nik_wali',       'label' => 'NIK Wali'],
            ['field' => 'nama_wali', 'label' => 'Nama Wali'],
            ['field' => 'tgl_lhr_wali', 'label' => 'Tanggal Lahir Wali'],
            ['field' => 'id_pendidikan_wali',   'label' => 'Pendidikan Wali'],
            ['field' => 'id_pekerjaan_wali',   'label' => 'Pekerjaan Wali'],
            ['field' => 'id_penghasilan_wali',   'label' => 'Penghasilan Wali'],*/
        ];
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == FALSE){
            $this->session->set_flashdata('message',validation_errors());
            $this->session->set_flashdata('type_message','danger');
            redirect('Penyesuaian/Users/Edit_langkah1_orangtua/'.$nik);
        }else{
            $rules = array(
                'where' => array(
                    'nik_passport' => $nik,
                ),
                'data'  => array(
                    'nik_ayah'              => $this->input->post('nik_ayah'),
                    'nama_ayah'             => strtoupper($this->input->post('nama_ayah')),
                    'tgl_lhr_ayah'          => $this->input->post('tgl_lhr_ayah'),
                    'id_pendidikan_ayah'    => $this->input->post('id_pendidikan_ayah'),
                    'id_pekerjaan_ayah'     => $this->input->post('id_pekerjaan_ayah'),
                    'id_penghasilan_ayah'   => $this->input->post('id_penghasilan_ayah'),
                    'nik_ibu'               => $this->input->post('nik_ibu'),
                    'nama_ibu'              => strtoupper($this->input->post('nama_ibu')),
                    'tgl_lhr_ibu'           => $this->input->post('tgl_lhr_ibu'),
                    'id_pendidikan_ibu'     => $this->input->post('id_pendidikan_ibu'),
                    'id_pekerjaan_ibu'      => $this->input->post('id_pekerjaan_ibu'),
                    'id_penghasilan_ibu'    => $this->input->post('id_penghasilan_ibu'),
                    'nik_wali'              => $this->input->post('nik_wali'),
                    'nama_wali'             => strtoupper($this->input->post('nama_wali')),
                    'tgl_lhr_wali'          => $this->input->post('tgl_lhr_wali'),
                    'id_pendidikan_wali'    => $this->input->post('id_pendidikan_wali'),
                    'id_pekerjaan_wali'     => $this->input->post('id_pekerjaan_wali'),
                    'id_penghasilan_wali'   => $this->input->post('id_penghasilan_wali'),
                ),
            );
            $rules2 = array(
                'where' => array(
                    'nik_passport' => $nik,
                ),
                'data'  => array(
                    'nominal_penghasilan_ayah'      => set_value('nominal_penghasilan_ayah'),
                    'terbilang_penghasilan_ayah'    => strtoupper(set_value('terbilang_penghasilan_ayah')),
                    'nominal_penghasilan_ibu'      => set_value('nominal_penghasilan_ibu'),
                    'terbilang_penghasilan_ibu'    => strtoupper(set_value('terbilang_penghasilan_ibu')),
                    'nominal_penghasilan_wali'      => set_value('nominal_penghasilan_wali'),
                    'terbilang_penghasilan_wali'    => strtoupper(set_value('terbilang_penghasilan_wali')),
                ),
            );
            if ($this->Tbl_penyesuaian_pddikti->update($rules)) {
                if($this->Tbl_penyesuaian_extra->update($rules2)){
                    $this->session->set_flashdata('message','Data berhasil disimpan');
                    $this->session->set_flashdata('type_message','success');
                    redirect('Penyesuaian/Users/Edit_langkah1_orangtua/'.$nik);   
                }else{
                    $this->session->set_flashdata('message','Terjadi kesalahan dalam proses simpan data');
                    $this->session->set_flashdata('type_message','danger');
                    redirect('Penyesuaian/Users/Edit_langkah1_orangtua/'.$nik);
                }
            }else{
                $this->session->set_flashdata('message','Terjadi kesalahan dalam proses simpan data');
                $this->session->set_flashdata('type_message','danger');
                redirect('Penyesuaian/Users/Edit_langkah1_orangtua/'.$nik);
            }
        }
    }
    
    function Simpan_tahap5($nik){
        $rules  =   [
            ['field' => 'id_rekening_listrik',       'label' => 'Rekening Listrik',             'rules' => 'required'],
            ['field' => 'id_rekening_pbb', 'label' => 'Rekening PBB',                     'rules' => 'required'],
            ['field' => 'id_pembayaran_listrik', 'label' => 'Pembayaran Listrik',                     'rules' => 'required'],
            ['field' => 'id_pembayaran_pbb',   'label' => 'Pembayaran PBB',                    'rules' => 'required'],
            ['field' => 'id_tanggungan',   'label' => 'Tanggungan',                    'rules' => 'required'],
            ['field' => 'alasan',    'label' => 'Alasan',      'rules' => 'required'],
        ];
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == FALSE){
            $this->session->set_flashdata('message',validation_errors());
            $this->session->set_flashdata('type_message','danger');
            redirect('Penyesuaian/Users/Edit_langkah2_tambahan/'.$nik);
        }else{
            $rules = array(
                'where' => array(
                    'nik_passport' => $nik,
                ),
                'data'  => array(
                    'id_rekening_listrik'   => set_value('id_rekening_listrik'),
                    'id_rekening_pbb'           => set_value('id_rekening_pbb'),
                    'id_pembayaran_listrik'           => set_value('id_pembayaran_listrik'),
                    'id_pembayaran_pbb'           => set_value('id_pembayaran_pbb'),
                    'id_tanggungan'           => set_value('id_tanggungan'),
                    'alasan'              => set_value('alasan'),
                ),
            );
            if ($this->Tbl_penyesuaian_extra->update($rules)) {
                $this->session->set_flashdata('message','Data berhasil disimpan');
                $this->session->set_flashdata('type_message','success');
                redirect('Penyesuaian/Users/Edit_langkah2_tambahan/'.$nik);
            }else{
                $this->session->set_flashdata('message','Terjadi kesalahan dalam proses simpan data');
                $this->session->set_flashdata('type_message','danger');
                redirect('Penyesuaian/Users/Edit_langkah2_tambahan/'.$nik);
            }
        }
    }
    
    function Simpan_tahap6($nik){
        $rules  =   [
            ['field' => 'userfile',       'label' => 'Foto',             'rules' => 'required'],
        ];
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == TRUE){
            $this->session->set_flashdata('message',validation_errors());
            $this->session->set_flashdata('type_message','danger');
            redirect('Penyesuaian/Users/Edit_langkah1_foto/'.$nik);
        }else{
            if ($_FILES['userfile']['size']>0) {
                $config = array(
                    'upload_path'   => './upload/foto/',
                    'allowed_types' => 'gif|jpg|png|jpeg',
                    'max_size'      => 2048,
                    'overwrite'     => TRUE,
                    'file_name'     => $nik
                );
                $this->upload->initialize($config);
                if(!$this->upload->do_upload()){
                    $this->session->set_flashdata('message',$this->upload->display_errors());
                    $this->session->set_flashdata('type_message','danger');
                    redirect('Penyesuaian/Users/Edit_langkah1_foto/'.$nik);
                }else{
                    $file = $this->upload->data();
                    $rules = array(
                        'where' => array(
                            'nik_passport' => $nik,
                        ),
                        'data'  => array(
                            'foto'              => $file['file_name'],
                        ),
                    );
                    if ($this->Tbl_penyesuaian_users->update($rules)) {
                        $this->session->set_flashdata('message','Foto berhasil disimpan');
                        $this->session->set_flashdata('type_message','success');
                        redirect('Penyesuaian/Users/Edit_langkah1_mahasiswa/'.$nik);   
                    }else{
                        $this->session->set_flashdata('message','Terjadi kesalahan dalam proses simpan data');
                        $this->session->set_flashdata('type_message','danger');
                        redirect('Penyesuaian/Users/Edit_langkah1_foto/'.$nik);
                    }
                }  
            }else{
                $this->session->set_flashdata('message','Terjadi kesalahan dalam proses simpan data');
                $this->session->set_flashdata('type_message','danger');
                redirect('Penyesuaian/Users/Edit_langkah1_foto/'.$nik);
            }
        }
    }

    function Delete($id){
        echo $id;
    }


	function Json(){
		$fetch_data = $this->SS_penyesuaian_users->make_datatables();
		$data = array();
		foreach($fetch_data as $row)
		{
			$sub_array = array();
            $sub_array[] = "
            <div class=\"btn-group\">
                <button type=\"button\" class=\"btn btn-primary dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                    Action <span class=\"caret\"></span>
                </button>
                <ul class=\"dropdown-menu\">
                    <li><a href=\"".base_url('Penyesuaian/Users/Detail/'.$row->nik_passport.'/'.$row->nomor_peserta)."\" target='_blank'>Detail</a></li>
                </ul>
            </div>";
			$sub_array[] = $row->nik_passport;
			$sub_array[] = $row->nomor_peserta;
			$sub_array[] = $row->nama;
            $sub_array[] = $row->jalur_masuk;
            $sub_array[] = $row->username;
            $sub_array[] = $row->status_verifikasi;
			$data[] = $sub_array;
		}
		$output = array(
			"draw"				=>	intval($_POST["draw"]),
			"recordsTotal"		=>	$this->SS_penyesuaian_users->get_all_data(),
			"recordsFiltered"	=>	$this->SS_penyesuaian_users->get_filtered_data(),
			"data"				=>	$data
		);
		echo json_encode($output);
	}
}
