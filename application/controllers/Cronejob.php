<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Cronejob extends CI_Controller {

    function __construct(){
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
        $this->load->model('Pradaftar/Tbl_pradaftar_biodata');
        $this->load->model('Pradaftar/Tbl_pradaftar_jadwal');
        $this->load->model('Pradaftar/Tbl_pradaftar_kelulusan');
        $this->load->model('Pradaftar/Tbl_pradaftar_orangtua');
        $this->load->model('Pradaftar/Tbl_pradaftar_pembayaran');
        $this->load->model('Pradaftar/Tbl_pradaftar_pilihan');
        $this->load->model('Pradaftar/Tbl_pradaftar_setting');
        $this->load->model('Pradaftar/Tbl_pradaftar_users');
        $this->load->model('Views/Pradaftar/View_pradaftar_kelulusan');
        $this->load->model('Views/Pradaftar/View_pradaftar_users');
        $this->load->model('Daftar/Tbl_daftar_kelulusan');
        $this->load->model('Daftar/Tbl_daftar_users');
        $this->load->model('Settings/Tbl_setting_jadwal');
        $this->load->model('Settings/Tbl_setting_smtp');
        $this->load->model('Settings/Tbl_setting_tipe_ujian');
        $this->load->model('Tmp/Tmp_pradaftar_users');

    }

    /* Normalisasi Users */
    function CJ1(){
        try{
            $rules = array(
                'select'    => null,
                'where'     => array('pembayaran' => '0'),
                'or_where'  => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $tblPUsers = $this->Tbl_pradaftar_users->where($rules)->result();
            foreach ($tblPUsers as $value){
                $rules = array(
                    'select'    => null,
                    'where'     => array('kd_pembayaran' => $value->kd_pembayaran),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $tblPUsers = $this->Tbl_pradaftar_pembayaran->where($rules)->num_rows();
                if ($tblPUsers == 0){
                    $tgl = date('Y-m-d H:i:s');
                    $tgl_pendaftaran = date('Y-m-d H:i:s', strtotime('+1 days', strtotime($value->date_created)));
                    if($tgl > $tgl_pendaftaran){
                        try{
                            $data = array(
                                'kd_pembayaran' => $value->kd_pembayaran,
                                'nik_passport' => $value->nik_passport,
                                'nomor_peserta' => $value->nomor_peserta,
                                'nama' => $value->nama,
                                'tgl_lhr' => $value->tgl_lhr,
                                'kategori' => $value->kategori,
                                'id_tipe_ujian' => $value->id_tipe_ujian,
                                'nmr_tlp' => $value->nmr_tlp,
                                'email' => $value->email,
                                'password' => $value->password,
                                'login' => $value->login,
                                'logout' => $value->logout,
                                'ip_login' => $value->ip_login,
                                'ip_logout' => $value->ip_logout,
                                'pembayaran' => $value->pembayaran,
                                'biodata' => $value->biodata,
                                'tgl_daftar' => $value->date_created,
                            );
                            $this->Tmp_pradaftar_users->create($data);
                            $rules = array('kd_pembayaran' => $value->kd_pembayaran);
                            $this->Tbl_pradaftar_users->delete($rules);
                            echo 'Berhasil menghaspus pradaftar users.';
                        }catch (Exception $e){
                            echo $e->getMessage();
                        }
                    }
                }
                echo "<br />";
            }
            echo "<script>window.close();</script>";
        }catch (Exception $e){
            echo $e->getMessage();
        }
    }

    /* Update SMTP */
    function CJ2(){
        try{
            $rules = array(
                'select'    => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $tblSSmtp = $this->Tbl_setting_smtp->read($rules)->result();
            $tgl = date('Y-m-d');
            foreach ($tblSSmtp as $value){
                if ($value->last_updated != $tgl){
                    $rules = array(
                        'where' => array('id_smtp' => $value->id_smtp),
                        'data'  => array(
                            'last_updated' => $tgl,
                            'quota' => 1500,
                        ),
                    );
                    $this->Tbl_setting_smtp->update($rules);
                }else{
                    echo "Up to date";
                }
            }
            echo "<script>window.close();</script>";
        }catch (Exception $e){
            echo $e->getMessage();
        }
    }

    /* Update Tipe Ujian */
    function CJ3(){
        try{
            $rules = array(
                'select'    => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $tblSTipeUjian = $this->Tbl_setting_tipe_ujian->read($rules)->result();
            foreach ($tblSTipeUjian as $value){
                try{
                    $quota = 0;
                    $rules = array(
                        'select'    => null,
                        'where'     => array('id_tipe_ujian' => $value->id_tipe_ujian),
                        'or_where'  => null,
                        'order'     => null,
                        'limit'     => null,
                        'pagging'   => null,
                    );
                    $tblSJadwal = $this->Tbl_setting_jadwal->where($rules)->result();
                    foreach ($tblSJadwal as $value2){
                        $quota += $value2->quota;
                    }
                    $rules = array(
                        'where' => array('id_tipe_ujian' => $value->id_tipe_ujian),
                        'data'  => array(
                            'quota' => $quota,
                            'status' => ($quota > 0)? '1' : '0',
                        ),
                    );
                    $this->Tbl_setting_tipe_ujian->update($rules);
                }catch (Exception $e){
                    echo $e->getMessage();
                }
            }
            echo "<script>window.close();</script>";
        }catch (Exception $e){
            echo  $e->getMessage();
        }
    }

    /* Verifikasi Pembayaran */
    function CJ4(){
        try{
            $rules = array(
                'select'    => null,
                'where'     => array('status' => '0'),
                'or_where'  => null,
                'order'     => 'date_created ASC',
                'limit'     => null,
                'pagging'   => null,
            );
            $tblPPembayaran = $this->Tbl_pradaftar_pembayaran->where($rules)->result();
            foreach ($tblPPembayaran as $value){
                $update = 0;
                $rules = array(
                    'select'    => null,
                    'where'     => array('kd_pembayaran' => $value->kd_pembayaran),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $tblPUsers = $this->Tbl_pradaftar_users->where($rules)->row();
                if ($tblPUsers->nomor_peserta == '-'.$value->kd_pembayaran){
                    $nomor_peserta = $this->NomorPeserta($tblPUsers->id_tipe_ujian);
                    /* Update Nomor Peserta dan Status Verifikasi Pembayaran Users */
                    $rules = array(
                        'where' => array('id_pradaftar_users' => $tblPUsers->id_pradaftar_users),
                        'data'  => array(
                            'nomor_peserta' => $nomor_peserta,
                            'pembayaran'    => '1',
                        ),
                    );
                    $this->Tbl_pradaftar_users->update($rules);
                    $update++;
                }else{
                    $nomor_peserta = $tblPUsers->nomor_peserta;
                    $update++;
                }
                /* Create Jadwal */
                $rules = array(
                    'select'    => null,
                    'where'     => array('nomor_peserta' => $nomor_peserta),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $num = $this->Tbl_pradaftar_jadwal->where($rules)->num_rows();
                if ($num == 0){
                    $rules = array(
                        'select'    => null,
                        'where'     => array(
                            'id_tipe_ujian' => $tblPUsers->id_tipe_ujian,
                            'quota >'       => 0,
                            'status'        => '1'
                        ),
                        'or_where'  => null,
                        'order'     => 'tanggal ASC, sesi ASC, id_jadwal ASC',
                        'limit'     => 1,
                        'pagging'   => null,
                    );
                    $tblSJadwal = $this->Tbl_setting_jadwal->where($rules)->row();
                    $rules = array(
                        'nomor_peserta' => $nomor_peserta,
                        'id_jadwal' 	=> $tblSJadwal->id_jadwal,
                    );
                    $this->Tbl_pradaftar_jadwal->create($rules);
                    /* Update Quota Jadwal */
                    $rules = array(
                        'where' => array('id_jadwal' => $tblSJadwal->id_jadwal),
                        'data'  => array(
                            'quota' => ($tblSJadwal->quota - 1)
                        ),
                    );
                    $this->Tbl_setting_jadwal->update($rules);
                    /* Update Quota Tipe Ujian */
                    $rules = array(
                        'select'    => null,
                        'where'     => array('id_tipe_ujian' => $tblPUsers->id_tipe_ujian),
                        'or_where'  => null,
                        'order'     => null,
                        'limit'     => null,
                        'pagging'   => null,
                    );
                    $tblSTipeUjian = $this->Tbl_setting_tipe_ujian->where($rules)->row();
                    $rules = array(
                        'where' => array('id_tipe_ujian' => $tblSTipeUjian->id_tipe_ujian),
                        'data'  => array(
                            'quota' => ($tblSTipeUjian->quota - 1)
                        ),
                    );
                    $this->Tbl_setting_tipe_ujian->update($rules);
                    $update++;
                }else{
                    $update++;
                }
                /* Create Biodata */
                $rules = array(
                    'select'    => null,
                    'where'     => array('id_pradaftar_users' => $tblPUsers->id_pradaftar_users),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $num = $this->Tbl_pradaftar_biodata->where($rules)->num_rows();
                if ($num == 0){
                    $rules = array(
                        'id_pradaftar_users'	=> $tblPUsers->id_pradaftar_users,
                        'tempat'				=> '-',
                        'jenis_kelamin'			=> 'L',
                        'warga_negara'			=> 'WNI',
                        'id_agama'				=> 1,
                        'id_kelurahan'			=> 1201011001,
                        'kodepos'				=> 0,
                        'alamat'                => '-',
                        'id_jenis_asal_sekolah' => 1,
                        'asal_sekolah'			=> '',
                        'id_rumpun'				=> 1,
                        'foto'					=> 'users.png',
                    );
                    $this->Tbl_pradaftar_biodata->create($rules);
                    $update++;
                }else{
                    $update++;
                }
                /* Create Kelulusan */
                $rules = array(
                    'select'    => null,
                    'where'     => array('nomor_peserta' => $nomor_peserta),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $num = $this->Tbl_pradaftar_kelulusan->where($rules)->num_rows();
                if ($num == 0){
                    $rules = array(
                        'nomor_peserta' => $nomor_peserta,
                        'kode_jurusan'  => 999,
                        'nilai'         => 0,
                        'status'        => '0',
                        'keterangan'    => '-',
                        'created_by'    => 1,
                        'updated_by'    => 1,
                    );
                    $this->Tbl_pradaftar_kelulusan->create($rules);
                    $update++;
                }else{
                    $update++;
                }
                /* Create Orang Tua */
                $rules = array(
                    'select'    => null,
                    'where'     => array('id_pradaftar_users' => $tblPUsers->id_pradaftar_users),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $num = $this->Tbl_pradaftar_orangtua->where($rules)->num_rows();
                if ($num == 0){
                    $rules = array(
                        'id_pradaftar_users'    => $tblPUsers->id_pradaftar_users,
                        'orangtua'				=> 'Ayah',
                        'nik_orangtua'			=> '-',
                        'nama_orangtua'			=> '-',
                        'tgl_lhr_orangtua'      => '1990-01-01',
                        'id_pendidikan'         => 1,
                        'id_pekerjaan'          => 6,
                        'id_penghasilan'        => 7,
                    );
                    $this->Tbl_pradaftar_orangtua->create($rules);
                    $rules = array(
                        'id_pradaftar_users'    => $tblPUsers->id_pradaftar_users,
                        'orangtua'				=> 'Ibu',
                        'nik_orangtua'			=> '-',
                        'nama_orangtua'			=> '-',
                        'tgl_lhr_orangtua'      => '1990-01-01',
                        'id_pendidikan'         => 1,
                        'id_pekerjaan'          => 6,
                        'id_penghasilan'        => 7,
                    );
                    $this->Tbl_pradaftar_orangtua->create($rules);
                    $update++;
                }else{
                    $update++;
                }
                /* Create Pilihan */
                $rules = array(
                    'select'    => null,
                    'where'     => array('nomor_peserta' => $nomor_peserta),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $num = $this->Tbl_pradaftar_pilihan->where($rules)->num_rows();
                if ($num == 0){
                    $rules = array(
                        'nomor_peserta' => $nomor_peserta,
                        'pilihan'       => '1',
                        'kode_jurusan'  => 999,
                    );
                    $this->Tbl_pradaftar_pilihan->create($rules);
                    $rules = array(
                        'nomor_peserta' => $nomor_peserta,
                        'pilihan'       => '2',
                        'kode_jurusan'  => 999,
                    );
                    $this->Tbl_pradaftar_pilihan->create($rules);
                    $rules = array(
                        'nomor_peserta' => $nomor_peserta,
                        'pilihan'       => '3',
                        'kode_jurusan'  => 999,
                    );
                    $this->Tbl_pradaftar_pilihan->create($rules);
                    $update++;
                }else{
                    $update++;
                }
                /* Update Status Verifikasi Pembayaran*/
                if ($update == 6){
                    $rules = array(
                        'where' => array('kd_pembayaran' => $tblPUsers->kd_pembayaran),
                        'data'  => array(
                            'status' => '1',
                        )
                    );
                    $this->Tbl_pradaftar_pembayaran->update($rules);
                }
            }
            echo "<script>window.setTimeout(CloseMe, 1000);function CloseMe() {window.close();}</script>";
        }catch (Exception $e){
            echo $e->getMessage();
        }
    }

    function CJ5(){
        $rules = array(
            'select'    => null,
            'where'     => array('status' => '1', 'YEAR(date_created)' => '2019'),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $no=0;
        $update=0;
        $tblPKelulusan = $this->View_pradaftar_kelulusan->where($rules)->result();
        foreach($tblPKelulusan as $a){
            $rules = array(
                'select'    => null,
                'where'     => array('nomor_peserta' => $a->nomor_peserta),
                'or_where'  => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $num_tblPKelulusan = $this->Tbl_daftar_kelulusan->where($rules);
    
            $rules = array(
                'select'    => null,
                'where'     => array('nomor_peserta' => $a->nomor_peserta),
                'or_where'  => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $tblPUsers = $this->View_pradaftar_users->where($rules)->row();
            if($num_tblPKelulusan->num_rows() == 0){
                $data = array(
                    'nomor_peserta' => $a->nomor_peserta,
                    'nama'  => $a->nama,
                    'kode_jurusan' => $a->kode_jurusan,
                    'id_jlr_msk' => '5',
                    'status' => 'BELUM DAFTAR',
                    'tahun' => '2019',
                    'created_by' => '2',
                    'updated_by' => '2'
                );
                if($this->Tbl_daftar_kelulusan->create($data)){
                    $id_daftar_kelulusan = $this->db->insert_id();
                    $rules = array(
                        'select'    => null,
                        'where'     => array('id_daftar_kelulusan' => $id_daftar_kelulusan),
                        'or_where'  => null,
                        'order'     => null,
                        'limit'     => null,
                        'pagging'   => null,
                    );
                    $num_tblPUsers = $this->Tbl_daftar_users->where($rules);
                    
                    if($num_tblPUsers->num_rows() == 0){
                        $data = array(
                            'nik_passport' 	=> $tblPUsers->nik_passport,
                            'id_daftar_kelulusan' => $id_daftar_kelulusan,
                            'username' 		=> $tblPUsers->email,
                            'password' 		=> $tblPUsers->password,
                            'nmr_hp'		=> $tblPUsers->nmr_tlp,
                            'ip_login' 		=> '-',
                            'ip_logout' 	=> '-',
                            'tgl_lhr'		=> $tblPUsers->tgl_lhr,
                            'verifikasi' 	=> 'BELUM VERIFIKASI',
                            'foto'			=> '-',
                        );
                        $this->Tbl_daftar_users->create($data);
                        
                        $no++;
                    }else{
                        $num_tblPUsers = $num_tblPUsers->row();
                        $rules = array(
                            'where' => array('id_daftar_users' => $num_tblPUsers->id_daftar_users),
                            'data'  => array(
                                'nik_passport' 	=> $tblPUsers->nik_passport,
                                'username' 		=> $tblPUsers->email,
                                'password' 		=> $tblPUsers->password,
                                'nmr_hp'		=> $tblPUsers->nmr_tlp,
                                'ip_login' 		=> '-',
                                'ip_logout' 	=> '-',
                                'tgl_lhr'		=> $tblPUsers->tgl_lhr,
                                'verifikasi' 	=> 'BELUM VERIFIKASI',
                                'foto'			=> '-',
                            ),
                        );
                        $this->Tbl_daftar_users->update($rules);
                        $update++;
                    }
                }
            }else{
                $num_tblPKelulusan = $num_tblPKelulusan->row();
                $rules = array(
                    'where' => array('id_daftar_kelulusan' => $num_tblPKelulusan->id_daftar_kelulusan),
                    'data'  => array(
                        'nomor_peserta' => $a->nomor_peserta,
                        'nama'  => $a->nama,
                        'kode_jurusan' => $a->kode_jurusan,
                        'id_jlr_msk' => '5',
                        'status' => 'BELUM DAFTAR',
                        'tahun' => '2019',
                        'created_by' => '2',
                        'updated_by' => '2'
                    ),
                );
                if($this->Tbl_daftar_kelulusan->update($rules)){
                    $rules = array(
                        'select'    => null,
                        'where'     => array('id_daftar_kelulusan' => $num_tblPKelulusan->id_daftar_kelulusan),
                        'or_where'  => null,
                        'order'     => null,
                        'limit'     => null,
                        'pagging'   => null,
                    );
                    $num_tblPUsers = $this->Tbl_daftar_users->where($rules);
                    
                    if($num_tblPUsers->num_rows() == 0){
                        $data = array(
                            'nik_passport' 	=> $tblPUsers->nik_passport,
                            'id_daftar_kelulusan' => $num_tblPKelulusan->id_daftar_kelulusan,
                            'username' 		=> $tblPUsers->email,
                            'password' 		=> $tblPUsers->password,
                            'nmr_hp'		=> $tblPUsers->nmr_tlp,
                            'ip_login' 		=> '-',
                            'ip_logout' 	=> '-',
                            'tgl_lhr'		=> $tblPUsers->tgl_lhr,
                            'verifikasi' 	=> 'BELUM VERIFIKASI',
                            'foto'			=> '-',
                        );
                        $this->Tbl_daftar_users->create($data);
                        
                        $no++;
                    }else{
                        $num_tblPUsers = $num_tblPUsers->row();
                        $rules = array(
                            'where' => array('id_daftar_users' => $num_tblPUsers->id_daftar_users),
                            'data'  => array(
                                'nik_passport' 	=> $tblPUsers->nik_passport,
                                'username' 		=> $tblPUsers->email,
                                'password' 		=> $tblPUsers->password,
                                'nmr_hp'		=> $tblPUsers->nmr_tlp,
                                'ip_login' 		=> '-',
                                'ip_logout' 	=> '-',
                                'tgl_lhr'		=> $tblPUsers->tgl_lhr,
                                'verifikasi' 	=> 'BELUM VERIFIKASI',
                                'foto'			=> '-',
                            ),
                        );
                        $this->Tbl_daftar_users->update($rules);
                        $update++;
                    }
                }

            }
            
        }
        echo 'berhasil sebanyak : '.$no.'<br> Update sebanyak : '.$update;
    }

    function NomorPeserta($id_tipe_ujian){
        $rules = array(
            'select'    => null,
            'where'     => array('id_tipe_ujian' => $id_tipe_ujian),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $tblSTipeUjian = $this->Tbl_setting_tipe_ujian->where($rules)->row();
        $tahun = substr(date('Y'),2,2);
        if ($id_tipe_ujian == 1){
            $kode = "CBT-";
        }elseif ($id_tipe_ujian == 2){
            $kode = "THZ-";
        }else{
            $kode = "PRS-";
        }
        $nomor_peserta = $kode.$tahun.$tblSTipeUjian->kode."00001";
        $rules = array(
            'select'    => null,
            'where'     => array(
                'nomor_peserta' => $nomor_peserta,
                'id_tipe_ujian' => $id_tipe_ujian,
            ),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $cek_nomor_peserta = $this->Tbl_pradaftar_users->where($rules)->num_rows();
        if ($cek_nomor_peserta != 0){
            $rules = array(
                'select'    => null,
                'where'     => array('id_tipe_ujian' => $id_tipe_ujian),
                'or_where'  => null,
                'order'     => 'nomor_peserta DESC',
                'limit'     => 1,
                'pagging'   => null,
            );
            $NoPesertaLimit = $this->Tbl_pradaftar_users->where($rules)->row();
            $data = explode('-', $NoPesertaLimit->nomor_peserta);
            $nomor_peserta = $data[0].'-'.($data[1]+1);
        }
        return $nomor_peserta;
    }

}