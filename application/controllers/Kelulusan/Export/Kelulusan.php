<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Kelulusan extends CI_Controller {
	
    function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != 'DEVELOPMENT') {
            $this->session->set_flashdata('message','Hak akses ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
		$this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
		$this->load->model('Views/Pradaftar/View_pradaftar_kelulusan');
		$this->load->model('Views/Pradaftar/View_pradaftar_users');
	}

    function index(){
    	$rules = array(
            'select'    => 'YEAR(date_created) as tahun',
            'where'     => null,
            'order'     => 'tahun DESC',
        );
        $data = array(
            'content'       => 'Kelulusan/Export/kelulusan/content',
            'css'           => 'Kelulusan/Export/kelulusan/css',
            'javascript'    => 'Kelulusan/Export/kelulusan/javascript',
            'modal'         => 'Kelulusan/Export/kelulusan/modal',
            'tahun' => $this->View_pradaftar_users->distinct($rules)->result(),
        );
        $this->load->view('index',$data);
	}
	
    function actionExport(){
		$tahun 		= $this->input->post('tahun');
		$rules = array(
			'select'    => null,
			'where'     => array(
				'YEAR(date_created)' => $tahun
			),
			'or_where'  => null,
			'order'     => '',
			'limit'     => null,
			'pagging'   => null,
		);
    	$viewPKelulusan = $this->View_pradaftar_kelulusan->where($rules);
    	if ($viewPKelulusan->num_rows() > 0){
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()
				->setCreator("Alfi Gusman") //creator
				->setTitle("Export data Kelulusan");  //file title
			$objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
			$objget = $objPHPExcel->getActiveSheet();  //inisiasi get object
			//table header
			$cols = array("A","B","C","D","E","F","G","H","I","J","K");
			$val = array("No","Nomor Peserta","Nama","Tipe Ujian","Status Kelulusan","Kode Jurusan","Jurusan","Fakultas","Nilai","Bobot","Keterangan");
			for ($a = 0; $a < 11; $a++) {
				$objset->setCellValue($cols[$a].'1', $val[$a]);
				//Setting lebar cell
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
				$style = array(
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					)
				);
				$objPHPExcel->getActiveSheet()->getStyle($cols[$a].'1')->applyFromArray($style);
			}
			$baris  = 2;
			$no = 1;
    		foreach ($viewPKelulusan->result() as $value){
				//pemanggilan sesuaikan dengan nama kolom tabel
				$rules = array(
                    'select'    => null,
                    'where'     => array(
                        'nomor_peserta' => $value->nomor_peserta
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
				$tbPUsers = $this->View_pradaftar_users->where($rules)->row();
                $objset->setCellValue("A".$baris, $no);
                $objset->setCellValue("B".$baris, $value->nomor_peserta);
                $objset->setCellValue("C".$baris, $tbPUsers->nama);
                $objset->setCellValue("D".$baris, $tbPUsers->tipe_ujian);
                $objset->setCellValue("E".$baris, $value->status);
                $objset->setCellValue("F".$baris, $value->kode_jurusan);
                $objset->setCellValue("G".$baris, $value->jurusan);
                $objset->setCellValue("H".$baris, $value->fakultas);
                $objset->setCellValue("I".$baris, $value->nilai);
                $objset->setCellValue("J".$baris, $value->bobot);
                $objset->setCellValue("K".$baris, $value->keterangan);

                //Set number value
                //$objPHPExcel->getActiveSheet()->getStyle('C1:C'.$baris)->getNumberFormat()->setFormatCode('0');

                $baris++;
                $no++;
			}
			$objPHPExcel->getActiveSheet()->setTitle('Data Export');

			$objPHPExcel->setActiveSheetIndex(0);
			$filename = urlencode("Kelulusan_".$tahun."_".date("Y_m_d_H_i_s").".xls");
			header('Content-Type: application/vnd.ms-excel'); //mime type
			header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
			header('Cache-Control: max-age=0'); //no cache
			$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save('php://output');
		}else{
			$this->session->set_flashdata('message','Data kosong.');
			$this->session->set_flashdata('type_message','danger');
			redirect('Kelulusan/Export/Kelulusan/');
		}
    }

}
