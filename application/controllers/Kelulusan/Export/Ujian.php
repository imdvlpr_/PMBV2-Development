<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Ujian extends CI_Controller {
	
    function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != 'DEVELOPMENT') {
            $this->session->set_flashdata('message','Hak akses ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
		$this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
		$this->load->model('Settings/Tbl_setting_tipe_ujian');
		$this->load->model('Views/Pradaftar/View_pradaftar_ujian');
	}

    function index(){
		$rules = array(
            'select'    => 'YEAR(date_created) as tahun',
            'where'     => null,
            'order'     => null,
		);
		$rules2 = array(
            'select'    => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
    	$data = array(
			'content'       => 'Kelulusan/Export/ujian/content',
            'css'           => 'Kelulusan/Export/ujian/css',
            'javascript'    => 'Kelulusan/Export/ujian/javascript',
            'modal'         => 'Kelulusan/Export/ujian/modal',
    		'tbSTipeUjian'	=> $this->Tbl_setting_tipe_ujian->read($rules2)->result(),
    		'tahun'			=> $this->View_pradaftar_ujian->distinct($rules)->result(),
		);
    	$this->load->view('index',$data);
	}
	
    function actionExport(){
    	ini_set('memory_limit', '-1');
		$tipe_ujian = $this->input->post('tipe_ujian');
		$tahun 		= $this->input->post('tahun');
    	if ($tipe_ujian == "All"){
			$rules = array(
				'select'    => null,
				'where'     => array(
					'verifikasi_biodata' => 'SUDAH VERIFIKASI',
					'YEAR(date_created)' => $tahun
				),
				'or_where'  => null,
				'order'     => null,
				'limit'     => null,
				'pagging'   => null,
			);
			$viewPUjian = $this->View_pradaftar_ujian->where($rules);
		}else{
			$rules = array(
				'select'    => null,
				'where'     => array(
					'verifikasi_biodata' => 'SUDAH VERIFIKASI',
					'tipe_ujian' => $tipe_ujian,
					'YEAR(date_created)' => $tahun
				),
				'or_where'  => null,
				'order'     => null,
				'limit'     => null,
				'pagging'   => null,
			);
			$viewPUjian = $this->View_pradaftar_ujian->where($rules);
		}
    	if ($viewPUjian->num_rows() > 0){
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()
				->setCreator("Alfi Gusman") //creator
				->setTitle("Export data Kelulusan");  //file title
			$objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
			$objget = $objPHPExcel->getActiveSheet();  //inisiasi get object
			//table header
			$cols = array("A","B","C","D","E","F","G","H","I","J");
			$val = array(
				"Nomor Peserta",
				"Nama",
				"Kode Jurusan 1",
				"Jurusan 1",
				"Kode Jurusan 2",
				"Jurusan 2",
				"Kode Jurusan 3",
				"Jurusan 3",
				"Tipe Ujian",
				"Nilai"
			);
			for ($a = 0; $a < 10; $a++) {
				$objset->setCellValue($cols[$a].'1', $val[$a]);
				//Setting lebar cell
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
				$style = array(
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					)
				);
				$objPHPExcel->getActiveSheet()->getStyle($cols[$a].'1')->applyFromArray($style);
			}
			$baris  = 2;
    		foreach ($viewPUjian->result() as $value){
				//pemanggilan sesuaikan dengan nama kolom tabel
                $objset->setCellValue("A".$baris, $value->nomor_peserta);
                $objset->setCellValue("B".$baris, $value->nama);
                $objset->setCellValue("C".$baris, $value->kode_jurusan_1);
                $objset->setCellValue("D".$baris, $value->jurusan_1);
                $objset->setCellValue("E".$baris, $value->kode_jurusan_2);
                $objset->setCellValue("F".$baris, $value->jurusan_2);
                $objset->setCellValue("G".$baris, $value->kode_jurusan_3);
                $objset->setCellValue("H".$baris, $value->jurusan_3);
                $objset->setCellValue("I".$baris, $value->tipe_ujian);
                $objset->setCellValue("J".$baris, $value->nilai);

                //Set number value
                //$objPHPExcel->getActiveSheet()->getStyle('C1:C'.$baris)->getNumberFormat()->setFormatCode('0');

                $baris++;
			}
			$objPHPExcel->getActiveSheet()->setTitle('Data Export');

			$objPHPExcel->setActiveSheetIndex(0);
			$filename = urlencode("Ujian_".$tipe_ujian."_".$tahun."_".date("Y_m_d_H_i_s").".xls");
			header('Content-Type: application/vnd.ms-excel'); //mime type
			header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
			header('Cache-Control: max-age=0'); //no cache
			$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save('php://output');
		}else{
			$this->session->set_flashdata('message','Data kosong.');
			$this->session->set_flashdata('type_message','danger');
			redirect('Kelulusan/Export/Ujian/');
		}
    }

}
