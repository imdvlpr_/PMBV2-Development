<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Jurusan extends CI_Controller {
	
    function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != 'DEVELOPMENT') {
            $this->session->set_flashdata('message','Hak akses ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
		$this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
		$this->load->model('Pradaftar/Tbl_pradaftar_users');
		$this->load->model('Views/Settings/View_setting_fakultas_jurusan');
    }
	
    function index(){
		$rules = array(
            'select'    => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
    	$viewSFJurusan = $this->View_setting_fakultas_jurusan->read($rules);
    	if ($viewSFJurusan->num_rows() > 0){
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()
				->setCreator("Alfi Gusman") //creator
				->setTitle("Export data Jadwal");  //file title
			$objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
			$objget = $objPHPExcel->getActiveSheet();  //inisiasi get object
			//table header
			$cols = array("A","B","C","D","E","F");
			$val = array("Kode Jurusan","Jurusan","Grade IPA","Grade IPS","Quota","Fakultas");
			for ($a = 0; $a < 5; $a++) {
				$objset->setCellValue($cols[$a].'1', $val[$a]);
				//Setting lebar cell
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
				$style = array(
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					)
				);
				$objPHPExcel->getActiveSheet()->getStyle($cols[$a].'1')->applyFromArray($style);
			}
			$baris  = 2;
    		foreach ($viewSFJurusan->result() as $value){
				//pemanggilan sesuaikan dengan nama kolom tabel
                $objset->setCellValue("A".$baris, $value->kode_jurusan);
                $objset->setCellValue("B".$baris, $value->jurusan);
                $objset->setCellValue("C".$baris, $value->grade_ipa);
                $objset->setCellValue("D".$baris, $value->grade_ips);
                $objset->setCellValue("E".$baris, $value->quota);
                $objset->setCellValue("F".$baris, $value->fakultas);

                //Set number value
                //$objPHPExcel->getActiveSheet()->getStyle('C1:C'.$baris)->getNumberFormat()->setFormatCode('0');

                $baris++;
			}
			$objPHPExcel->getActiveSheet()->setTitle('Data Export');

			$objPHPExcel->setActiveSheetIndex(0);
			$filename = urlencode("Jurusan_Fakultas_".date("Y_m_d_H_i_s").".xls");
			header('Content-Type: application/vnd.ms-excel'); //mime type
			header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
			header('Cache-Control: max-age=0'); //no cache
			$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save('php://output');
		}else{
			$this->session->set_flashdata('message','Data kosong.');
			$this->session->set_flashdata('type_message','danger');
			redirect('Dashboard/');
		}
    }

}
