<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class GenerateKelulusan extends CI_Controller {

	function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != 'DEVELOPMENT') {
            $this->session->set_flashdata('message','Hak akses ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }

		$this->load->model('Pradaftar/Tbl_pradaftar_pilihan');
		$this->load->model('Pradaftar/Tbl_pradaftar_kelulusan');
		$this->load->model('Pradaftar/Tbl_pradaftar_users');
		$this->load->model('Settings/Tbl_setting_jurusan');
	}

	function index(){
		$rules = array(
            'select'    => 'YEAR(date_created) as tahun',
            'where'     => null,
            'order'     => 'tahun DESC',
        );
		$data = array(
			'content'       => 'Kelulusan/generate_kelulusan/content',
            'css'           => 'Kelulusan/generate_kelulusan/css',
            'javascript'    => 'Kelulusan/generate_kelulusan/javascript',
            'modal'         => 'Kelulusan/generate_kelulusan/modal',
			'tahun'	        => $this->Tbl_pradaftar_users->distinct($rules)->result(),
		);
		$this->load->view('index',$data);
	}

	function Generate(){
		$pilihan = $this->input->post('pilihan');
		$tahun = $this->input->post('tahun');
		$rules = array(
			'select'    => null,
			'where'     => array(
			    'status' => '0',
				'YEAR(date_created)' => $tahun
			),
			'or_where'  => null,
			'order'     => 'bobot DESC',
			'limit'     => null,
			'pagging'   => null,
		);
		$tblPKelulusan = $this->Tbl_pradaftar_kelulusan->where($rules)->result();
		$berhasil = 0; $error = 0;
		foreach ($tblPKelulusan as $value){
			$rules = array(
				'select'    => null,
				'where'     => array(
					'nomor_peserta' => $value->nomor_peserta
				),
				'or_where'  => null,
				'order'     => null,
				'limit'     => null,
				'pagging'   => null,
			);
            $tblPUsers = $this->Tbl_pradaftar_users->where($rules)->row();
            if ($tblPUsers->id_tipe_ujian == 1 && $tblPUsers->biodata == '1'){
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'pilihan' => "$pilihan",
                        'nomor_peserta' => $value->nomor_peserta
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $tblPPilihan = $this->Tbl_pradaftar_pilihan->where($rules)->row();
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'kode_jurusan' => $tblPPilihan->kode_jurusan,
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $tblSJurusan = $this->Tbl_setting_jurusan->where($rules)->row();
                if ($tblPUsers->kategori == 1){
                    if ($value->bobot >= $tblSJurusan->grade_ipa && $tblSJurusan->quota > 0){
                        $rules = array(
                            'where' => array(
                                'nomor_peserta' => $value->nomor_peserta,
                            ),
                            'data'  => array(
                                'status' => '1',
                                'kode_jurusan' => $tblSJurusan->kode_jurusan,
                            ),
                        );
                        if ($this->Tbl_pradaftar_kelulusan->update($rules)){
                            $rules = array(
                                'where' => array(
                                    'kode_jurusan' => $tblSJurusan->kode_jurusan,
                                ),
                                'data'  => array(
                                    'quota' => $tblSJurusan->quota - 1,
                                ),
                            );
                            if ($this->Tbl_setting_jurusan->update($rules)){
                                $berhasil++;
                            }else{
                                $error++;
                            }
                        }
                    }
                }else{
                    if ($value->bobot >= $tblSJurusan->grade_ips && $tblSJurusan->quota > 0){
                        $rules = array(
                            'where' => array(
                                'nomor_peserta' => $value->nomor_peserta,
                            ),
                            'data'  => array(
                                'status' => '1',
                                'kode_jurusan' => $tblSJurusan->kode_jurusan,
                            ),
                        );
                        if ($this->Tbl_pradaftar_kelulusan->update($rules)){
                            $rules = array(
                                'where' => array(
                                    'kode_jurusan' => $tblSJurusan->kode_jurusan,
                                ),
                                'data'  => array(
                                    'quota' => $tblSJurusan->quota - 1,
                                ),
                            );
                            if ($this->Tbl_setting_jurusan->update($rules)){
                                $berhasil++;
                            }else{
                                $error++;
                            }
                        }
                    }
                }

            }
		}
		$this->session->set_flashdata('message', 'Generate kelulusan berhasil. Berhasil : '.$berhasil.'. Error : '.$error.'.');
		$this->session->set_flashdata('type_message', 'success');
		redirect('Kelulusan/GenerateKelulusan/');
	}


}
