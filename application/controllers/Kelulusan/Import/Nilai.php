<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Nilai extends CI_Controller {

	function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != 'DEVELOPMENT') {
            $this->session->set_flashdata('message','Hak akses ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
		$this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
		$this->load->model('Pradaftar/Tbl_pradaftar_kelulusan');
	}

	function index(){
    	$data = array(
            'content'       => 'Kelulusan/Import/nilai/content',
            'css'           => 'Kelulusan/Import/nilai/css',
            'javascript'    => 'Kelulusan/Import/nilai/javascript',
            'modal'         => 'Kelulusan/Import/nilai/modal',
        );
		$this->load->view('index', $data);
	}

	function Import(){
		$config = array(
			'upload_path'   => './import/pradaftar/',
			'allowed_types' => 'xls|xlsx|csv|ods|ots',
			'max_size'      => 51200,
			'overwrite'     => TRUE,
			'file_name'     => 'Nilai_Ujian_'.date('H i s d m Y'),
		);
		$this->upload->initialize($config);
		if(!$this->upload->do_upload()){
			$this->session->set_flashdata('message',$this->upload->display_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('Kelulusan/Import/Nilai/');
		}else {
			$file = $this->upload->data();
			$inputFileName = 'import/pradaftar/' . $file['file_name'];
			try {
				$inputFileType = IOFactory::identify($inputFileName);
				$objReader = IOFactory::createReader($inputFileType);
				$objPHPExcel = $objReader->load($inputFileName);
			} catch (Exception $e) {
				die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '" : ' . $e->getMessage());
			}
			$sheet = $objPHPExcel->getSheet(0);
			$highestRow = $sheet->getHighestRow();
			try{
				$berhasil = 0; $error = 0;
				for ($row = 2; $row <= $highestRow; $row++) {
					$rules = array(
						'where' => array(
							'nomor_peserta' => $sheet->getCellByColumnAndRow(0, $row)->getValue(),
						),
						'data'  => array(
							'nilai' => $sheet->getCellByColumnAndRow(2, $row)->getValue(),
						),
					);
					if($this->Tbl_pradaftar_kelulusan->update($rules)){
						$berhasil++;
					}else{
						$error++;
					}
				}
				$this->session->set_flashdata('message', 'Import berhasil. Berhasil : '.$berhasil.'. Error : '.$error.'.');
				$this->session->set_flashdata('type_message', 'success');
				redirect('Kelulusan/Import/Nilai/');
			}catch (Exception $e){
				$this->session->set_flashdata('message', $e->getMessage());
				$this->session->set_flashdata('type_message', 'danger');
				redirect('Kelulusan/Import/Nilai/');
			}
		}
	}
}

