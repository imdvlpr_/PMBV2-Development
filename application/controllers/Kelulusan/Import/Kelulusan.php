<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Kelulusan extends CI_Controller {

	function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != 'DEVELOPMENT') {
            $this->session->set_flashdata('message','Hak akses ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
		$this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
		$this->load->model('Pradaftar/Tbl_pradaftar_kelulusan');
		$this->load->model('Pradaftar/Tbl_pradaftar_pilihan');
		$this->load->model('Settings/Tbl_setting_jurusan');
	}

	function index(){
		$data = array(
            'content'       => 'Kelulusan/Import/kelulusan/content',
            'css'           => 'Kelulusan/Import/kelulusan/css',
            'javascript'    => 'Kelulusan/Import/kelulusan/javascript',
            'modal'         => 'Kelulusan/Import/kelulusan/modal',
        );
		$this->load->view('index', $data);
	}

	function Import(){
		$config = array(
			'upload_path'   => './import/pradaftar/',
			'allowed_types' => 'xls|xlsx|csv|ods|ots',
			'max_size'      => 51200,
			'overwrite'     => TRUE,
			'file_name'     => 'Kelulusan_'.date('H i s d m Y'),
		);
		$this->upload->initialize($config);
		if(!$this->upload->do_upload()){
			$this->session->set_flashdata('message',$this->upload->display_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('Kelulusan/Import/Kelulusan/');
		}else {
			$file = $this->upload->data();
			$inputFileName = 'import/pradaftar/' . $file['file_name'];
			try {
				$inputFileType = IOFactory::identify($inputFileName);
				$objReader = IOFactory::createReader($inputFileType);
				$objPHPExcel = $objReader->load($inputFileName);
			} catch (Exception $e) {
				die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '" : ' . $e->getMessage());
			}
			$sheet = $objPHPExcel->getSheet(0);
			$highestRow = $sheet->getHighestRow();
			try{
				$berhasil = 0; $error = 0; $errorHtml = "<div class='table-responsive'><table class='table table-responsive'>";
				for ($row = 2; $row <= $highestRow; $row++) {
					$nomor_peserta = $sheet->getCellByColumnAndRow(0, $row)->getValue();
					$kode_jurusan = $sheet->getCellByColumnAndRow(2, $row)->getValue();
					$keterangan = $sheet->getCellByColumnAndRow(4, $row)->getValue();
					$num = 0;
					for ($i = 1; $i < 4; $i++){
                        $rules = array(
                            'select'    => null,
                            'where'     => array(
                                'nomor_peserta' => $nomor_peserta,
                                'pilihan' => "$i",
                                'kode_jurusan' => $kode_jurusan,
                            ), //not null or null
                            'or_where'  => null, //not null or null
                            'order'     => null,
                            'limit'     => null,
                            'pagging'   => null,
                        );
                        $num += $this->Tbl_pradaftar_pilihan->where($rules)->num_rows();
                    }
                    $rules = array(
                        'select'    => null,
                        'where'     => array(
                            'kode_jurusan' => $kode_jurusan,
                        ), //not null or null
                        'or_where'  => null, //not null or null
                        'order'     => null,
                        'limit'     => null,
                        'pagging'   => null,
                    );
					$tbSJurusan = $this->Tbl_setting_jurusan->where($rules)->row();
					if ($num > 0){
						if ($tbSJurusan->quota > 0){
                            $rules = array(
                                'where' => array(
                                    'nomor_peserta' => $nomor_peserta,
                                ),
                                'data'  => array(
                                    'kode_jurusan' => $kode_jurusan,
                                    'status' => '1',
                                    'keterangan' => ($keterangan != null)? $keterangan : '-',
                                ),
                            );
							if ($this->Tbl_pradaftar_kelulusan->update($rules)){
								$rules = array(
									'where' => array(
										'kode_jurusan' => $tbSJurusan->kode_jurusan,
									),
									'data'  => array(
										'quota' => $tbSJurusan->quota - 1,
									),
								);
								if ($this->Tbl_setting_jurusan->update($rules)){
                                    $berhasil++;
                                }else{
                                    $errorHtml .="<tr><td>".$nomor_peserta." #Error 0 : Error Update Quota Jurusan.</td></tr>";
								    $error++;
                                }
							}else{
								$errorHtml .="<tr><td>".$nomor_peserta." #Error 1 : Update gagal.</td></tr>";
								$error++;
							}
						}else{
							$errorHtml .="<tr><td>".$nomor_peserta." #Error 2 : Quota habis.</td></tr>";
							$error++;
						}
					}else{
						$errorHtml .="<tr><td>".$nomor_peserta." #Error 3 : Pilihan tidak sesuai.</td></tr>";
						$error++;
					}
				}
				$errorHtml .= "</table></div>";
				$this->session->set_flashdata('message', 'Import berhasil. Berhasil : '.$berhasil.'. Error : '.$error);
				$this->session->set_flashdata('error', $error);
				$this->session->set_flashdata('errorHtml', $errorHtml);
				$this->session->set_flashdata('type_message', ($error == 0)? 'success' : 'warning');
				redirect('Kelulusan/Import/Kelulusan/');
			}catch (Exception $e){
				$this->session->set_flashdata('message', $e->getMessage());
				$this->session->set_flashdata('type_message', 'danger');
				redirect('Kelulusan/Import/Kelulusan/');
			}
		}
	}
}

