<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class CekKelulusan extends CI_Controller {

	function __construct(){
		parent::__construct();
		if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
		if ($this->session->userdata('level') != 'DEVELOPMENT') {
			$this->session->set_flashdata('message','Hak akses ditolak.');
			$this->session->set_flashdata('type_message','danger');
			redirect('Auth');
		}
		$this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
		$this->load->model('Pradaftar/Tbl_pradaftar_kelulusan');
		$this->load->model('Settings/Tbl_setting_jurusan');
	}

	function index(){
		$data = array(
            'content'       => 'Kelulusan/Import/cek_kelulusan/content',
            'css'           => 'Kelulusan/Import/cek_kelulusan/css',
            'javascript'    => 'Kelulusan/Import/cek_kelulusan/javascript',
            'modal'         => 'Kelulusan/Import/cek_kelulusan/modal',
        );
		$this->load->view('index', $data);
	}

	function Import(){
		$config = array(
			'upload_path'   => './import/pradaftar/',
			'allowed_types' => 'xls|xlsx|csv|ods|ots',
			'max_size'      => 51200,
			'overwrite'     => TRUE,
			'file_name'     => 'Cek_Kelulusan_'.date('H i s d m Y'),
		);
		$this->upload->initialize($config);
		if(!$this->upload->do_upload()){
			$this->session->set_flashdata('message',$this->upload->display_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('Kelulusan/Import/CekKelulusan/');
		}else {
			$file = $this->upload->data();
			$inputFileName = 'import/pradaftar/' . $file['file_name'];
			try {
				$inputFileType = IOFactory::identify($inputFileName);
				$objReader = IOFactory::createReader($inputFileType);
				$objPHPExcel = $objReader->load($inputFileName);
			} catch (Exception $e) {
				die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '" : ' . $e->getMessage());
			}
			$sheet = $objPHPExcel->getSheet(0);
			$highestRow = $sheet->getHighestRow();
			try{
				$berhasil = 0; $error = 0; $reportHtml = null;
				for ($row = 2; $row <= $highestRow; $row++) {
					$nomor_peserta = $sheet->getCellByColumnAndRow(0, $row)->getValue();
					$kode_jurusan = $sheet->getCellByColumnAndRow(2, $row)->getValue();
					$keterangan = $sheet->getCellByColumnAndRow(3, $row)->getValue();
					$rules = array(
						'select'    => null,
						'where'     => array(
							'nomor_peserta' => $nomor_peserta
						),
						'or_where'  => null,
						'order'     => null,
						'limit'     => null,
						'pagging'   => null,
					);
					$tbPKelulusan = $this->Tbl_pradaftar_kelulusan->where($rules);
					if ($tbPKelulusan->num_rows() > 0) {
						$tbPKelulusan = $tbPKelulusan->row();
						if ($tbPKelulusan->status == "1") {
							$reportHtml .="<p>".$nomor_peserta." #Success : Sudah LULUS di kode jurusan ".$tbPKelulusan->kode_jurusan.".</p>";
                            $berhasil++;
						}else{
							$error++;
						}
					}else{
						$reportHtml .="<p>".$nomor_peserta." #Error : Nomor Peserta tidak ada.</p>";
                        $error++;
					}
				}
				$this->session->set_flashdata('message', 'Import berhasil. Berhasil : '.$berhasil.'. Error : '.$error.'.'.$reportHtml);
				$this->session->set_flashdata('type_message', 'success');
				redirect('Kelulusan/Import/CekKelulusan/');
			}catch (Exception $e){
				$this->session->set_flashdata('message', $e->getMessage());
				$this->session->set_flashdata('type_message', 'danger');
				redirect('Kelulusan/Import/CekKelulusan/');
			}
		}
	}
}

