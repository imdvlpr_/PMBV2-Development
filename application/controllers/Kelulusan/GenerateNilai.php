<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class GenerateNilai extends CI_Controller {

	function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != 'DEVELOPMENT') {
            $this->session->set_flashdata('message','Hak akses ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }

		$this->load->model('Pradaftar/Tbl_pradaftar_kelulusan');
	}

	function index(){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, 'http://192.168.130.21/catroot/index.php/ujian/tarikjawaban?ph=ujianmandiri');
        $result = curl_exec($ch);
        curl_close($ch);
        if (empty($result)){
            $status = false;
        }else{
            $status = true;
        }
		$data = array(
			'content'   => 'Kelulusan/generate_nilai/content',
            'css'       => 'Kelulusan/generate_nilai/css',
            'javascript'=> 'Kelulusan/generate_nilai/javascript',
            'modal'     => 'Kelulusan/generate_nilai/modal',
			'status'    => $status,
		);
		$this->load->view('index',$data);
	}

	function Generate(){
        $berhasil = 0;
        $error = 0;
	    $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, 'http://192.168.130.21/catroot/index.php/ujian/tarikjawaban?ph=ujianmandiri');
        $result = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($result, TRUE);
        for($i=0; $i<count($result);$i++){
            $rules = array(
                'where' => array(
                    'nomor_peserta' => $result[$i]['kodeRegistrasi']
                ),
                'data'  => array(
                    'nilai' => $result[$i]['skor'],
                    'bobot' => $result[$i]['bobotNilai'],
                ),
            );
            if ($this->Tbl_pradaftar_kelulusan->update($rules)){
                $berhasil++;
            }else{
                $error++;
            }
        }
		$this->session->set_flashdata('message', 'Generate nilai berhasil. Berhasil : '.$berhasil.'. Error : '.$error.'.');
		$this->session->set_flashdata('type_message', 'success');
		redirect('Kelulusan/GenerateNilai/');
	}


}
