<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class GenerateGrade extends CI_Controller {

	function __construct(){
		parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != 'DEVELOPMENT') {
            $this->session->set_flashdata('message','Hak akses ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }

        $this->load->model('Views/Pradaftar/View_pradaftar_kelulusan');
        $this->load->model('Views/Pradaftar/View_pradaftar_pilihan');
        $this->load->model('Views/Pradaftar/View_pradaftar_users');
		$this->load->model('Settings/Tbl_setting_jurusan');
	}

	function index(){
		$rules = array(
            'select'    => 'YEAR(date_created) as tahun',
            'where'     => null,
            'order'     => 'tahun DESC',
        );
		$data = array(
			'content'       => 'Kelulusan/generate_grade/content',
            'css'           => 'Kelulusan/generate_grade/css',
            'javascript'    => 'Kelulusan/generate_grade/javascript',
            'modal'         => 'Kelulusan/generate_grade/modal',
			'tahun'         => $this->View_pradaftar_users->distinct($rules)->result(),
		);
		$this->load->view('index',$data);
	}

	function Generate(){
		$tahun = $this->input->post('tahun');
		$rules = array(
			'select'    => null,
			'where'     => array(
				'kode_jurusan <>' => 999
			),
			'or_where'  => null,
			'order'     => null,
			'limit'     => null,
			'pagging'   => null,
		);
		$tbSJurusan = $this->Tbl_setting_jurusan->where($rules)->result();
		$berhasil = 0; $error = 0;
		foreach ($tbSJurusan as $value){
			$rules = array(
				'select'    => null,
				'where'     => array(
                    'pilihan' => '1',
                    'kode_jurusan' => $value->kode_jurusan,
					'YEAR(date_created)' => $tahun
				),
				'or_where'  => null,
				'order'     => null,
				'limit'     => null,
				'pagging'   => null,
			);
			$viewPPilihan = $this->View_pradaftar_pilihan->where($rules);
			$total_ipa = 0; $jumlah_ipa = 0; $total_ips = 0; $jumlah_ips = 0;
            foreach ($viewPPilihan->result() as $a){
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'nomor_peserta' => $a->nomor_peserta,
                        'id_tipe_ujian' => 1,
                        'biodata' => '1',
                        'YEAR(date_created)' => $tahun
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $viewPUsers = $this->View_pradaftar_users->where($rules);
                if ($viewPUsers->num_rows() > 0){
                    $viewPUsers = $viewPUsers->row();
                    $rules = array(
                        'select'    => null,
                        'where'     => array(
                            'nomor_peserta' => $a->nomor_peserta,
                        ),
                        'or_where'  => null,
                        'order'     => null,
                        'limit'     => null,
                        'pagging'   => null,
                    );
                    $viewPKelulusan = $this->View_pradaftar_kelulusan->where($rules)->row();
                    if ($viewPUsers->kategori == '1'){
                        $jumlah_ipa++;
                        $total_ipa += $viewPKelulusan->bobot;
                    }else{
                        $jumlah_ips++;
                        $total_ips += $viewPKelulusan->bobot;
                    }

                }
            }
            if ($total_ipa != 0){
                $rata_ipa = $total_ipa / $jumlah_ipa;
            }else{
                $rata_ipa = 0;
            }
            if ($total_ips != 0){
                $rata_ips = $total_ips / $jumlah_ips;
            }else{
                $rata_ips = 0;
            }
			//echo "Kode Jurusan : $a->kode_jurusan | Rata : $rata | Total : $total | Jumlah : $jumlah |<br>";
			 $rules = array(
				'where' => array(
					'kode_jurusan' => $value->kode_jurusan,
				),
				'data'  => array(
					'grade_ipa' => $rata_ipa,
					'grade_ips' => $rata_ips,
				),
			);
			if ($this->Tbl_setting_jurusan->update($rules)){
				$berhasil++;
			}else{
				$error++;
			}
		}
		$this->session->set_flashdata('message', 'Generate grade bobot berhasil. Berhasil : '.$berhasil.'. Error : '.$error.'.');
		$this->session->set_flashdata('type_message', 'success');
		redirect('Kelulusan/GenerateGrade/');
	}

}
