<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class PembayaranPbb extends CI_Controller {

    function __construct(){
        parent::__construct();
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != "DEVELOPMENT") {
            $this->session->set_flashdata('message','Hak Akses Ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Dashboard');
        }
        $this->load->model('Settings/Tbl_setting_pembayaran_pbb');
    }

    function index(){
        $rules = array(
            'select'    => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $tbSPembayaranPBB = $this->Tbl_setting_pembayaran_pbb->read($rules)->result();
        $data = array(
            'content'       => 'Daftar/Settings/pembayaran_pbb/content',
            'css'           => 'Daftar/Settings/pembayaran_pbb/css',
            'javascript'    => 'Daftar/Settings/pembayaran_pbb/javascript',
            'modal'         => 'Daftar/Settings/pembayaran_pbb/modal',
            'tbSPembayaranPBB' => $tbSPembayaranPBB,
        );
        $this->load->view('index',$data);
    }

    function Tambah(){
        $rules[] = array('field' => 'pembayaran_pbb', 'label' => 'Pembayaran PBB', 'rules' => 'required');
        $rules[] = array('field' => 'nilai', 'label' => 'Nilai', 'rules' => 'required');
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == FALSE){
            $this->session->set_flashdata('message',validation_errors());
            $this->session->set_flashdata('type_message','danger');
            redirect('Daftar/Settings/PembayaranPbb/');
        }else{
            $data = array(
                'pembayaran_pbb'    => strtoupper($this->input->post('pembayaran_pbb')),
                'nilai'             => $this->input->post('nilai'),
                'created_by'         => $this->session->userdata('id_users'),
                'updated_by'         => $this->session->userdata('id_users'),
            );
            if ($this->Tbl_setting_pembayaran_pbb->create($data)) {
                $this->session->set_flashdata('message','Data berhasil disimpan.');
                $this->session->set_flashdata('type_message','success');
                redirect('Daftar/Settings/PembayaranPbb/');
            }else{
                $this->session->set_flashdata('message','Terjadi kesalahan dalam tambah data.');
                $this->session->set_flashdata('type_message','danger');
                redirect('Daftar/Settings/PembayaranPbb/');
            }
        }
    }

    function Edit($id){
        $rules = array(
            'select'    => null,
            'where'     => array(
				'id_pembayaran_pbb' => $id
			),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
		);
        $tbSPembayaranPBB = $this->Tbl_setting_pembayaran_pbb->where($rules)->row();
        $data = array(
            'content'       => 'Daftar/Settings/pembayaran_pbb/edit/content',
            'css'           => 'Daftar/Settings/pembayaran_pbb/edit/css',
            'javascript'    => 'Daftar/Settings/pembayaran_pbb/edit/javascript',
            'modal'         => 'Daftar/Settings/pembayaran_pbb/edit/modal',
            'tbSPembayaranPBB' => $tbSPembayaranPBB,
        );
        $this->load->view('index',$data);
    }

    function Update($id){
        $rules[] = array('field' => 'pembayaran_pbb', 'label' => 'Pembayaran PBB', 'rules' => 'required');
        $rules[] = array('field' => 'nilai', 'label' => 'Nilai', 'rules' => 'required');
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == FALSE){
            $this->session->set_flashdata('message',validation_errors());
            $this->session->set_flashdata('type_message','danger');
            redirect('Daftar/Settings/PembayaranPbb/');
        }else{
            $rules = array(
                'where' => array(
                    'id_pembayaran_pbb' => $id,
                ),
                'data'  => array(
					'pembayaran_pbb'    => strtoupper($this->input->post('pembayaran_pbb')),
                    'nilai'             => $this->input->post('nilai'),
                    'updated_by'         => $this->session->userdata('id_users'),
                ),
            );
            if ($this->Tbl_setting_pembayaran_pbb->update($rules)) {
                $this->session->set_flashdata('message','Data berhasil diubah.');
                $this->session->set_flashdata('type_message','success');
                redirect('Daftar/Settings/PembayaranPbb/');
            }else{
                $this->session->set_flashdata('message','Terjadi kesalahan dalam edit data.');
                $this->session->set_flashdata('type_message','danger');
                redirect('Daftar/Settings/PembayaranPbb/');
            }
        }
    }

    function Delete($id){
        $where = array(
            'id_pembayaran_pbb' => $id
        );
        if ($this->Tbl_setting_pembayaran_pbb->delete($where)) {
            $this->session->set_flashdata('message','Data berhasil dihapus.');
            $this->session->set_flashdata('type_message','success');
            redirect('Daftar/Settings/PembayaranPbb/');
        }else{
            $this->session->set_flashdata('message','Terjadi kesalahan dalam hapus data.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Daftar/Settings/PembayaranPbb/');
        }
    }

}
