<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class AlatTransportasi extends CI_Controller {

    function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != "DEVELOPMENT") {
            $this->session->set_flashdata('message','Hak Akses Ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Dashboard');
        }
        $this->load->model('Settings/Tbl_setting_alat_transportasi');
    }

    function index(){
        $rules = array(
            'select'    => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $tbSAlatTransportasi = $this->Tbl_setting_alat_transportasi->read($rules)->result();
        $data = array(
            'content'       => 'Daftar/Settings/alat_transportasi/content',
            'css'           => 'Daftar/Settings/alat_transportasi/css',
            'javascript'    => 'Daftar/Settings/alat_transportasi/javascript',
            'modal'         => 'Daftar/Settings/alat_transportasi/modal',
            'tbSAlatTransportasi' => $tbSAlatTransportasi,
        );
        $this->load->view('index',$data);
    }

    function Tambah(){
        $rules[] = array('field' => 'alat_transportasi', 'label' => 'Alat Transportasi', 'rules' => 'required');
        $rules[] = array('field' => 'nilai', 'label' => 'Nilai', 'rules' => 'required');
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == FALSE){
            $this->session->set_flashdata('message',validation_errors());
            $this->session->set_flashdata('type_message','danger');
            redirect('Daftar/Settings/AlatTransportasi/');
        }else{
            $data = array(
                'alat_transportasi' => strtoupper($this->input->post('alat_transportasi')),
                'nilai'             => $this->input->post('nilai'),
                'created_by'         => $this->session->userdata('id_users'),
                'updated_by'         => $this->session->userdata('id_users'),
            );
            if ($this->Tbl_setting_alat_transportasi->create($data)) {
                $this->session->set_flashdata('message','Data berhasil disimpan.');
                $this->session->set_flashdata('type_message','success');
                redirect('Daftar/Settings/AlatTransportasi/');
            }else{
                $this->session->set_flashdata('message','Terjadi kesalahan dalam tambah data.');
                $this->session->set_flashdata('type_message','danger');
                redirect('Daftar/Settings/AlatTransportasi/');
            }
        }
    }

    function Edit($id){
        $rules = array(
            'select'    => null,
            'where'     => array(
				'id_alat_transportasi' => $id
			),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
		);
        $tbSAlatTransportasi = $this->Tbl_setting_alat_transportasi->where($rules)->row();
        $data = array(
            'content'       => 'Daftar/Settings/alat_transportasi/edit/content',
            'css'           => 'Daftar/Settings/alat_transportasi/edit/css',
            'javascript'    => 'Daftar/Settings/alat_transportasi/edit/javascript',
            'modal'         => 'Daftar/Settings/alat_transportasi/edit/modal',
            'tbSAlatTransportasi' => $tbSAlatTransportasi,
        );
        $this->load->view('index',$data);
    }

    function Update($id){
        $rules[] = array('field' => 'alat_transportasi', 'label' => 'Alat Transportasi', 'rules' => 'required');
        $rules[] = array('field' => 'nilai', 'label' => 'Nilai', 'rules' => 'required');
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == FALSE){
            $this->session->set_flashdata('message',validation_errors());
            $this->session->set_flashdata('type_message','danger');
            redirect('Daftar/Settings/AlatTransportasi/');
        }else{
            $rules = array(
                'where' => array(
                    'id_alat_transportasi' => $id,
                ),
                'data'  => array(
					'alat_transportasi' => strtoupper($this->input->post('alat_transportasi')),
                    'nilai'             => $this->input->post('nilai'),
                    'updated_by'         => $this->session->userdata('id_users'),
                ),
            );
            if ($this->Tbl_setting_alat_transportasi->update($rules)) {
                $this->session->set_flashdata('message','Data berhasil diubah.');
                $this->session->set_flashdata('type_message','success');
                redirect('Daftar/Settings/AlatTransportasi/');
            }else{
                $this->session->set_flashdata('message','Terjadi kesalahan dalam edit data.');
                $this->session->set_flashdata('type_message','danger');
                redirect('Daftar/Settings/AlatTransportasi/');
            }
        }
    }

    function Delete($id){
        $where = array(
            'id_alat_transportasi' => $id
        );
        if ($this->Tbl_setting_alat_transportasi->delete($where)) {
            $this->session->set_flashdata('message','Data berhasil dihapus.');
            $this->session->set_flashdata('type_message','success');
            redirect('Daftar/Settings/AlatTransportasi/');
        }else{
            $this->session->set_flashdata('message','Terjadi kesalahan dalam hapus data.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Daftar/Settings/AlatTransportasi/');
        }
    }

}

