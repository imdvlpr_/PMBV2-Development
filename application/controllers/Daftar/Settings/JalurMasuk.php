<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class JalurMasuk extends CI_Controller {

	function __construct(){
        parent::__construct();
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != 'DEVELOPMENT') {
            $this->session->set_flashdata('message','Hak akses ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        $this->load->model('Settings/Tbl_setting_jalur_masuk');
    }

	function index(){
		$rules = array(
            'select'    => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
		$tbSJalurMasuk = $this->Tbl_setting_jalur_masuk->read($rules)->result();
		$data = array(
			'content'       => 'Daftar/Settings/jalur_masuk/content',
            'css'           => 'Daftar/Settings/jalur_masuk/css',
            'javascript'    => 'Daftar/Settings/jalur_masuk/javascript',
            'modal'         => 'Daftar/Settings/jalur_masuk/modal',
			'tbSJalurMasuk' => $tbSJalurMasuk,
		);
		$this->load->view('index',$data);
	}

	function Tambah(){
        $rules[] = array('field' => 'jalur', 'label' => 'Jalur', 'rules' => 'required');
        $rules[] = array('field' => 'awal-date', 'label' => 'Awal', 'rules' => 'required');
        $rules[] = array('field' => 'awal-time', 'label' => 'Awal', 'rules' => 'required');
        $rules[] = array('field' => 'akhir-date', 'label' => 'Akhir', 'rules' => 'required');
        $rules[] = array('field' => 'akhir-time', 'label' => 'Akhir', 'rules' => 'required');
        $rules[] = array('field' => 'status', 'label' => 'Status Login', 'rules' => 'required');
        $rules[] = array('field' => 'status_ukt', 'label' => 'Status UKT', 'rules' => 'required');
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('message',validation_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('Daftar/Settings/JalurMasuk/');
		}else{
			$data = array(
				'jalur_masuk'   => strtoupper($this->input->post('jalur')),
				'awal'          => $this->input->post('awal-date').' '.$this->input->post('awal-time'),
				'akhir'         => $this->input->post('akhir-date').' '.$this->input->post('akhir-time'),
				'status'        => $this->input->post('status'),
				'status_ukt'    => $this->input->post('status_ukt'),
                'created_by'     => $this->session->userdata('id_users'),
                'updated_by'     => $this->session->userdata('id_users'),
			);
			if ($this->Tbl_setting_jalur_masuk->create($data)) {
				$this->session->set_flashdata('message','Data berhasil disimpan.');
            	$this->session->set_flashdata('type_message','success');
            	redirect('Daftar/Settings/JalurMasuk/');
			}else{
				$this->session->set_flashdata('message','Terjadi kesalahan dalam tambah data.');
            	$this->session->set_flashdata('type_message','danger');
            	redirect('Daftar/Settings/JalurMasuk/');
			}
		}
	}

	function Edit($id){
		$rules = array(
            'select'    => null,
            'where'     => array(
				'id_jlr_msk' => $id
			),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
		);
		$tbSJalurMasuk = $this->Tbl_setting_jalur_masuk->where($rules)->row();
		$data = array(
			'content'       => 'Daftar/Settings/jalur_masuk/edit/content',
            'css'           => 'Daftar/Settings/jalur_masuk/edit/css',
            'javascript'    => 'Daftar/Settings/jalur_masuk/edit/javascript',
            'modal'         => 'Daftar/Settings/jalur_masuk/edit/modal',
			'tbSJalurMasuk' => $tbSJalurMasuk,
		);
		$this->load->view('index',$data);
	}

	function Update($id){
        $rules[] = array('field' => 'jalur', 'label' => 'Jalur', 'rules' => 'required');
        $rules[] = array('field' => 'awal-date', 'label' => 'Awal', 'rules' => 'required');
        $rules[] = array('field' => 'awal-time', 'label' => 'Awal', 'rules' => 'required');
        $rules[] = array('field' => 'akhir-date', 'label' => 'Akhir', 'rules' => 'required');
        $rules[] = array('field' => 'akhir-time', 'label' => 'Akhir', 'rules' => 'required');
        $rules[] = array('field' => 'status', 'label' => 'Status Login', 'rules' => 'required');
        $rules[] = array('field' => 'status_ukt', 'label' => 'Status UKT', 'rules' => 'required');
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('message',validation_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('Daftar/Settings/JalurMasuk/');
		}else{
			$rules = array(
                'where' => array(
                    'id_jlr_msk' => $id,
                ),
                'data'  => array(
					'jalur_masuk'   => strtoupper($this->input->post('jalur')),
	                'awal'          => $this->input->post('awal-date').' '.$this->input->post('awal-time'),
					'akhir'         => $this->input->post('akhir-date').' '.$this->input->post('akhir-time'),
					'status'        => $this->input->post('status'),
					'status_ukt'    => $this->input->post('status_ukt'),
					'updated_by'     => $this->session->userdata('id_users'),
                ),
            );
			if ($this->Tbl_setting_jalur_masuk->update($rules)) {
				$this->session->set_flashdata('message','Data berhasil diubah.');
            	$this->session->set_flashdata('type_message','success');
            	redirect('Daftar/Settings/JalurMasuk/');
			}else{
				$this->session->set_flashdata('message','Terjadi kesalahan dalam edit data.');
            	$this->session->set_flashdata('type_message','danger');
            	redirect('Daftar/Settings/JalurMasuk/');
			}
		}
	}

	function Delete($id){
		$where = array(
            'id_jlr_msk' => $id
        );
		if ($this->Tbl_setting_jalur_masuk->delete($where)) {
			$this->session->set_flashdata('message','Data berhasil dihapus.');
            $this->session->set_flashdata('type_message','success');
            redirect('Daftar/Settings/JalurMasuk/');
		}else{
			$this->session->set_flashdata('message','Terjadi kesalahan dalam hapus data.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Daftar/Settings/JalurMasuk/');
		}
	}

}

