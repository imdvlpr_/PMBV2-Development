<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Tanggungan extends CI_Controller {

	function __construct(){
        parent::__construct();
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != "DEVELOPMENT") {
            $this->session->set_flashdata('message','Hak Akses Ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Dashboard');
        }
        $this->load->model('Settings/Tbl_setting_tanggungan');
    }

	function index(){
		$rules = array(
            'select'    => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
		$tbSTanggungan = $this->Tbl_setting_tanggungan->read($rules)->result();
		$data = array(
			'content'       => 'Daftar/Settings/tanggungan/content',
            'css'           => 'Daftar/Settings/tanggungan/css',
            'javascript'    => 'Daftar/Settings/tanggungan/javascript',
            'modal'         => 'Daftar/Settings/tanggungan/modal',
			'tbSTanggungan' => $tbSTanggungan,
		);
		$this->load->view('index',$data);
	}

	function Tambah(){
	    $rules[] = array('field' => 'tanggungan', 'label' => 'Tanggungan', 'rules' => 'required');
	    $rules[] = array('field' => 'nilai', 'label' => 'Nilai', 'rules' => 'required');
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('message',validation_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('Daftar/Settings/Tanggungan/');
		}else{
			$data = array(
				'tanggungan'    => strtoupper($this->input->post('tanggungan')),
				'nilai'         => $this->input->post('nilai'),
                'created_by'     => $this->session->userdata('id_users'),
                'updated_by'     => $this->session->userdata('id_users'),
			);
			if ($this->Tbl_setting_tanggungan->create($data)) {
				$this->session->set_flashdata('message','Data berhasil disimpan.');
            	$this->session->set_flashdata('type_message','success');
            	redirect('Daftar/Settings/Tanggungan/');
			}else{
				$this->session->set_flashdata('message','Terjadi kesalahan dalam tambah data.');
            	$this->session->set_flashdata('type_message','danger');
            	redirect('Daftar/Settings/Tanggungan/');
			}
		}
	}

	function Edit($id){
		$rules = array(
            'select'    => null,
            'where'     => array(
				'id_tanggungan' => $id
			),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
		);
		$tbSTanggungan = $this->Tbl_setting_tanggungan->where($rules)->row();
		$data = array(
			'content'       => 'Daftar/Settings/tanggungan/edit/content',
            'css'           => 'Daftar/Settings/tanggungan/edit/css',
            'javascript'    => 'Daftar/Settings/tanggungan/edit/javascript',
            'modal'         => 'Daftar/Settings/tanggungan/edit/modal',
			'tbSTanggungan' => $tbSTanggungan,
		);
		$this->load->view('index',$data);
	}

	function Update($id){
        $rules[] = array('field' => 'tanggungan', 'label' => 'Tanggungan', 'rules' => 'required');
        $rules[] = array('field' => 'nilai', 'label' => 'Nilai', 'rules' => 'required');
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('message',validation_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('Daftar/Settings/Tanggungan/');
		}else{
			$rules = array(
                'where' => array(
                    'id_tanggungan' => $id,
                ),
                'data'  => array(
					'tanggungan'    => strtoupper($this->input->post('tanggungan')),
					'nilai'         => $this->input->post('nilai'),
					'updated_by'     => $this->session->userdata('id_users'),
                ),
            );
			if ($this->Tbl_setting_tanggungan->update($rules)) {
				$this->session->set_flashdata('message','Data berhasil diubah.');
            	$this->session->set_flashdata('type_message','success');
            	redirect('Daftar/Settings/Tanggungan/');
			}else{
				$this->session->set_flashdata('message','Terjadi kesalahan dalam edit data.');
            	$this->session->set_flashdata('type_message','danger');
            	redirect('Daftar/Settings/Tanggungan/');
			}
		}
	}

	function Delete($id){
		$where = array(
            'id_tanggungan' => $id
        );
		if ($this->Tbl_setting_tanggungan->delete($where)) {
			$this->session->set_flashdata('message','Data berhasil dihapus.');
            $this->session->set_flashdata('type_message','success');
            redirect('Daftar/Settings/Tanggungan/');
		}else{
			$this->session->set_flashdata('message','Terjadi kesalahan dalam hapus data.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Daftar/Settings/Tanggungan/');
		}
	}

}
