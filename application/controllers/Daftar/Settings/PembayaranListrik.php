<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class PembayaranListrik extends CI_Controller {

    function __construct(){
        parent::__construct();
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != "DEVELOPMENT") {
            $this->session->set_flashdata('message','Hak Akses Ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Dashboard');
        }
        $this->load->model('Settings/Tbl_setting_pembayaran_listrik');
    }

    function index(){
        $rules = array(
            'select'    => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $tbSPembayaranListrik = $this->Tbl_setting_pembayaran_listrik->read($rules)->result();
        $data = array(
            'content'       => 'Daftar/Settings/pembayaran_listrik/content',
            'css'           => 'Daftar/Settings/pembayaran_listrik/css',
            'javascript'    => 'Daftar/Settings/pembayaran_listrik/javascript',
            'modal'         => 'Daftar/Settings/pembayaran_listrik/modal',
            'tbSPembayaranListrik' => $tbSPembayaranListrik,
        );
        $this->load->view('index',$data);
    }

    function Tambah(){
        $rules[] = array('field' => 'pembayaran_listrik', 'label' => 'Pembayaran Listrik', 'rules' => 'required');
        $rules[] = array('field' => 'nilai', 'label' => 'Nilai', 'rules' => 'required');
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == FALSE){
            $this->session->set_flashdata('message',validation_errors());
            $this->session->set_flashdata('type_message','danger');
            redirect('Daftar/Settings/PembayaranListrik/');
        }else{
            $data = array(
                'pembayaran_listrik'    => strtoupper($this->input->post('pembayaran_listrik')),
                'nilai'                 => $this->input->post('nilai'),
                'created_by'             => $this->session->userdata('id_users'),
                'updated_by'             => $this->session->userdata('id_users'),
            );
            if ($this->Tbl_setting_pembayaran_listrik->create($data)) {
                $this->session->set_flashdata('message','Data berhasil disimpan.');
                $this->session->set_flashdata('type_message','success');
                redirect('Daftar/Settings/PembayaranListrik/');
            }else{
                $this->session->set_flashdata('message','Terjadi kesalahan dalam tambah data.');
                $this->session->set_flashdata('type_message','danger');
                redirect('Daftar/Settings/PembayaranListrik/');
            }
        }
    }

    function Edit($id){
        $rules = array(
            'select'    => null,
            'where'     => array(
				'id_pembayaran_listrik' => $id
			),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
		);
        $tbSPembayaranListrik = $this->Tbl_setting_pembayaran_listrik->where($rules)->row();
        $data = array(
            'content'       => 'Daftar/Settings/pembayaran_listrik/edit/content',
            'css'           => 'Daftar/Settings/pembayaran_listrik/edit/css',
            'javascript'    => 'Daftar/Settings/pembayaran_listrik/edit/javascript',
            'modal'         => 'Daftar/Settings/pembayaran_listrik/edit/modal',
            'tbSPembayaranListrik' => $tbSPembayaranListrik,
        );
        $this->load->view('index',$data);
    }

    function Update($id){
        $rules[] = array('field' => 'pembayaran_listrik', 'label' => 'Pembayaran Listrik', 'rules' => 'required');
        $rules[] = array('field' => 'nilai', 'label' => 'Nilai', 'rules' => 'required');
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == FALSE){
            $this->session->set_flashdata('message',validation_errors());
            $this->session->set_flashdata('type_message','danger');
            redirect('Daftar/Settings/PembayaranListrik/');
        }else{
            $rules = array(
                'where' => array(
                    'id_pembayaran_listrik' => $id,
                ),
                'data'  => array(
					'pembayaran_listrik'    => strtoupper($this->input->post('pembayaran_listrik')),
                    'nilai'                 => $this->input->post('nilai'),
                    'updated_by'             => $this->session->userdata('id_users'),
                ),
            );
            if ($this->Tbl_setting_pembayaran_listrik->update($rules)) {
                $this->session->set_flashdata('message','Data berhasil diubah.');
                $this->session->set_flashdata('type_message','success');
                redirect('Daftar/Settings/PembayaranListrik/');
            }else{
                $this->session->set_flashdata('message','Terjadi kesalahan dalam edit data.');
                $this->session->set_flashdata('type_message','danger');
                redirect('Daftar/Settings/PembayaranListrik/');
            }
        }
    }

    function Delete($id){
        $where = array(
            'id_pembayaran_listrik' => $id
        );
        if ($this->Tbl_setting_pembayaran_listrik->delete($where)) {
            $this->session->set_flashdata('message','Data berhasil dihapus.');
            $this->session->set_flashdata('type_message','success');
            redirect('Daftar/Settings/PembayaranListrik');
        }else{
            $this->session->set_flashdata('message','Terjadi kesalahan dalam hapus data.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Daftar/Settings/PembayaranListrik');
        }
    }

}
