<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class RekeningPbb extends CI_Controller {

	function __construct(){
        parent::__construct();
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != "DEVELOPMENT") {
            $this->session->set_flashdata('message','Hak Akses Ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Dashboard');
        }
        $this->load->model('Settings/Tbl_setting_rekening_pbb');
    }

	function index(){
		$rules = array(
            'select'    => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
		$tbSRekeningPBB = $this->Tbl_setting_rekening_pbb->read($rules)->result();
		$data = array(
			'content'       => 'Daftar/Settings/rekening_pbb/content',
            'css'           => 'Daftar/Settings/rekening_pbb/css',
            'javascript'    => 'Daftar/Settings/rekening_pbb/javascript',
            'modal'         => 'Daftar/Settings/rekening_pbb/modal',
			'tbSRekeningPBB' => $tbSRekeningPBB,
		);
		$this->load->view('index',$data);
	}

	function Tambah(){
	    $rules[] = array('field' => 'rekening_pbb', 'label' => 'Rekening PBB', 'rules' => 'required');
	    $rules[] = array('field' => 'nilai', 'label' => 'Nilai', 'rules' => 'required');
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('message',validation_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('Daftar/Settings/RekeningPbb/');
		}else{
			$data = array(
				'rekening_pbb'  => strtoupper($this->input->post('rekening_pbb')),
				'nilai'         => $this->input->post('nilai'),
                'created_by'     => $this->session->userdata('id_users'),
                'updated_by'     => $this->session->userdata('id_users'),
			);
			if ($this->Tbl_setting_rekening_pbb->create($data)) {
				$this->session->set_flashdata('message','Data berhasil disimpan.');
            	$this->session->set_flashdata('type_message','success');
            	redirect('Daftar/Settings/RekeningPbb/');
			}else{
				$this->session->set_flashdata('message','Terjadi kesalahan dalam tambah data.');
            	$this->session->set_flashdata('type_message','danger');
            	redirect('Daftar/Settings/RekeningPbb/');
			}
		}
	}

	function Edit($id){
		$rules = array(
            'select'    => null,
            'where'     => array(
				'id_rekening_pbb' => $id
			),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
		);
		$tbSRekeningPBB = $this->Tbl_setting_rekening_pbb->where($rules)->row();
		$data = array(
			'content'       => 'Daftar/Settings/rekening_pbb/edit/content',
            'css'           => 'Daftar/Settings/rekening_pbb/edit/css',
            'javascript'    => 'Daftar/Settings/rekening_pbb/edit/javascript',
            'modal'         => 'Daftar/Settings/rekening_pbb/edit/modal',
			'tbSRekeningPBB' => $tbSRekeningPBB,
		);
		$this->load->view('index',$data);
	}

	function Update($id){
        $rules[] = array('field' => 'rekening_pbb', 'label' => 'Rekening PBB', 'rules' => 'required');
        $rules[] = array('field' => 'nilai', 'label' => 'Nilai', 'rules' => 'required');
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('message',validation_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('Daftar/Settings/RekeningPbb/');
		}else{
			$rules = array(
                'where' => array(
                    'id_rekening_pbb' => $id,
                ),
                'data'  => array(
					'rekening_pbb'  => strtoupper($this->input->post('rekening_pbb')),
					'nilai'         => $this->input->post('nilai'),
					'updated_by'     => $this->session->userdata('id_users'),
                ),
            );
			if ($this->Tbl_setting_rekening_pbb->update($rules)) {
				$this->session->set_flashdata('message','Data berhasil diubah.');
            	$this->session->set_flashdata('type_message','success');
            	redirect('Daftar/Settings/RekeningPbb/');
			}else{
				$this->session->set_flashdata('message','Terjadi kesalahan dalam edit data.');
            	$this->session->set_flashdata('type_message','danger');
            	redirect('Daftar/Settings/RekeningPbb/');
			}
		}
	}

	function Delete($id){
		$where = array(
            'id_rekening_pbb' => $id
        );
		if ($this->Tbl_setting_rekening_pbb->delete($where)) {
			$this->session->set_flashdata('message','Data berhasil dihapus.');
            $this->session->set_flashdata('type_message','success');
            redirect('Daftar/Settings/RekeningPbb/');
		}else{
			$this->session->set_flashdata('message','Terjadi kesalahan dalam hapus data.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Daftar/Settings/RekeningPbb/');
		}
	}

}
