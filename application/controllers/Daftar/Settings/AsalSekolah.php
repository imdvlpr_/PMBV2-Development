<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class AsalSekolah extends CI_Controller {

    function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != "DEVELOPMENT") {
            $this->session->set_flashdata('message','Hak Akses Ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Dashboard');
        }
        $this->load->model('Settings/Tbl_setting_asal_sekolah');
    }

	function index(){
        $rules = array(
            'select'    => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
		$data = array(
            'content'       => 'Daftar/Settings/asal_sekolah/content',
            'css'           => 'Daftar/Settings/asal_sekolah/css',
            'javascript'    => 'Daftar/Settings/asal_sekolah/javascript',
            'modal'         => 'Daftar/Settings/asal_sekolah/modal',
		    'tblSAsalSekolah'     => $this->Tbl_setting_asal_sekolah->read($rules)->result(),
		);
		$this->load->view('index',$data);
	}

	function Create(){
		$rules[] = array('field' => 'asal_sekolah', 'label' => 'Asal Sekolah', 'rules' => 'required');
        $rules[] = array('field' => 'status', 'label' => 'Status', 'rules' => 'required');
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('message',validation_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('Daftar/Settings/AsalSekolah/');
		}else{
		    try{
                $data = array(
                    'asal_sekolah'     => strtoupper($this->input->post('asal_sekolah')),
                    'status'    => $this->input->post('status'),
                    'created_by' => $this->session->userdata('id_users'),
                    'updated_by' => $this->session->userdata('id_users'),
                );
                $this->Tbl_setting_asal_sekolah->create($data);
                $this->session->set_flashdata('message','Data berhasil disimpan.');
            	$this->session->set_flashdata('type_message','success');
            	redirect('Daftar/Settings/AsalSekolah/');
            }catch (Exception $e){
                $this->session->set_flashdata('message', $e->getMessage());
                $this->session->set_flashdata('type_message','danger');
                redirect('Daftar/Settings/AsalSekolah/');
            }
		}
	}

	function Update($id){
        $rules[] = array('field' => 'asal_sekolah', 'label' => 'Asal Sekolah', 'rules' => 'required');
        $rules[] = array('field' => 'status', 'label' => 'Status', 'rules' => 'required');
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('message',validation_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('Daftar/Settings/AsalSekolah/');
		}else{
		    try{
                $rules = array(
                    'where' => array('id_asal_sekolah' => $id),
                    'data'  => array(
                        'asal_sekolah'     => strtoupper($this->input->post('asal_sekolah')),
                        'status'    => $this->input->post('status'),
                        'updated_by' => $this->session->userdata('id_users'),
                    ),
                );
                $this->Tbl_setting_asal_sekolah->update($rules);
                $this->session->set_flashdata('message','Data berhasil diubah.');
                $this->session->set_flashdata('type_message','success');
                redirect('Daftar/Settings/AsalSekolah/');
            }catch (Exception $e){
                $this->session->set_flashdata('message', $e->getMessage());
                $this->session->set_flashdata('type_message','danger');
                redirect('Daftar/Settings/AsalSekolah/');
            }
		}
	}

	function Delete($id){
        try{
            $rules = array('id_asal_sekolah' => $id);
            $this->Tbl_setting_asal_sekolah->delete($rules);
            $this->session->set_flashdata('message','Data berhasil dihapus.');
            $this->session->set_flashdata('type_message','success');
            redirect('Daftar/Settings/AsalSekolah/');
        }catch (Exception $e){
            $this->session->set_flashdata('message', $e->getMessage());
            $this->session->set_flashdata('type_message','danger');
            redirect('Daftar/Settings/AsalSekolah/');
        }
	}

}

