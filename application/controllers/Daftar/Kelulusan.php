<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Kelulusan extends CI_Controller {

	function __construct(){
		parent::__construct();
		if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
		if ($this->session->userdata('level') != 'DEVELOPMENT') {
			$this->session->set_flashdata('message','Hak akses ditolak.');
			$this->session->set_flashdata('type_message','danger');
			redirect('Auth');
		}
		$this->load->model('ServerSide/SS_daftar_kelulusan', '', TRUE);
		$this->load->model('Daftar/Tbl_daftar_kelulusan');
		$this->load->model('Settings/Tbl_setting_jurusan');
		$this->load->model('Settings/Tbl_setting_jalur_masuk');
	}

	function index(){
		$data = array(
            'content'       => 'Daftar/Kelulusan/content',
            'css'           => 'Daftar/Kelulusan/css',
            'javascript'    => 'Daftar/Kelulusan/javascript',
            'modal'         => 'Daftar/Kelulusan/modal',
        );
		$this->load->view('index', $data);
	}

	function Edit($id){
		$rules = array(
            'select'    => null,
            'where'     => array(
				'nomor_peserta' => $id
			),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
		);
		$rules2 = array(
            'select'    => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $tbDKelulusan = $this->Tbl_daftar_kelulusan->where($rules)->row();
        $tbSJurusan = $this->Tbl_setting_jurusan->read($rules2)->result();
        $tbSJalurMasuk = $this->Tbl_setting_jalur_masuk->read($rules2)->result();
        $data = array(
			'content'       => 'Daftar/Kelulusan/kelulusan_edit/content',
            'css'           => 'Daftar/Kelulusan/kelulusan_edit/css',
            'javascript'    => 'Daftar/Kelulusan/kelulusan_edit/javascript',
            'modal'         => 'Daftar/Kelulusan/kelulusan_edit/modal',
            'tbDKelulusan' => $tbDKelulusan,
            'tbSJurusan' => $tbSJurusan,
            'tbSJalurMasuk' => $tbSJalurMasuk,
        );
        $this->load->view('index',$data);
	}

	function Update($id){
	    $rules[] = array('field' => 'nama', 'label' => 'Nama', 'rules' => 'required');
	    $rules[] = array('field' => 'jurusan', 'label' => 'Jurusan', 'rules' => 'required');
	    $rules[] = array('field' => 'jalur', 'label' => 'Jalur Masuk', 'rules' => 'required');
	    $rules[] = array('field' => 'status', 'label' => 'Status', 'rules' => 'required');
	    $rules[] = array('field' => 'tahun', 'label' => 'Tahun', 'rules' => 'required');
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == FALSE){
            $this->session->set_flashdata('message',validation_errors());
            $this->session->set_flashdata('type_message','danger');
            redirect('Daftar/Kelulusan/Edit/'.$id);
        }else{
			$rules = array(
                'where' => array(
                    'nomor_peserta' => $id,
                ),
                'data'  => array(
					'nama'          => str_replace('\'','`',strtoupper(set_value('nama'))),
					'kode_jurusan'  => set_value('jurusan'),
					'id_jlr_msk'    => set_value('jalur'),
					'status' 	    => set_value('status'),
					'tahun' 	    => set_value('tahun'),
					'updated_by'     => $this->session->userdata('id_users'),
                ),
            );
            if ($this->Tbl_daftar_kelulusan->update($rules)) {
                $this->session->set_flashdata('message','Data berhasil diubah.');
                $this->session->set_flashdata('type_message','success');
                redirect('Daftar/Kelulusan/Edit/'.$id);
            }else{
                $this->session->set_flashdata('message','Terjadi kesalahan dalam edit data agama');
                $this->session->set_flashdata('type_message','danger');
                redirect('Daftar/Kelulusan/Edit/'.$id);
            }
        }
	}

    function Delete($id){
		$where = array(
			'nomor_peserta' => id,
		);
        if ($this->Tbl_daftar_kelulusan->delete($where)) {
            $this->session->set_flashdata('message','Data berhasil dihapus.');
            $this->session->set_flashdata('type_message','success');
            redirect('Daftar/Kelulusan/');
        }else{
            $this->session->set_flashdata('message','Terjadi kesalahan dalam hapus data agama');
            $this->session->set_flashdata('type_message','danger');
            redirect('Daftar/Kelulusan/');
        }
    }


	function Json(){
		$fetch_data = $this->SS_daftar_kelulusan->make_datatables();
		$data = array();
		foreach($fetch_data as $row)
		{
			$sub_array = array();
			$sub_array[] = "
            <div class=\"btn-group\">
                <button type=\"button\" class=\"btn btn-dafault dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                    <i class='fa fa-gear'></i> <span class=\"caret\"></span>
                </button>
                <ul class=\"dropdown-menu\">
					<li><a href=\"".base_url('Daftar/Kelulusan/Edit/'.$row->nomor_peserta)."\" target='_blank'>Edit</a></li>
					<li><a href=\"".base_url('Daftar/Kelulusan/Delete/'.$row->nomor_peserta)."\" target='_blank'>Hapus</a></li>
                </ul>
            </div>";
			$sub_array[] = $row->nomor_peserta;
			$sub_array[] = $row->nama;
			$sub_array[] = $row->jurusan;
			$sub_array[] = $row->fakultas;
			$sub_array[] = $row->jalur_masuk;
			$sub_array[] = $row->status_pendaftaran;
			$sub_array[] = $row->tahun_pendaftaran;
			$data[] = $sub_array;
		}
		$output = array(
			"draw"				=>	intval($_POST["draw"]),
			"recordsTotal"		=>	$this->SS_daftar_kelulusan->get_all_data(),
			"recordsFiltered"	=>	$this->SS_daftar_kelulusan->get_filtered_data(),
			"data"				=>	$data
		);
		echo json_encode($output);
	}
}
