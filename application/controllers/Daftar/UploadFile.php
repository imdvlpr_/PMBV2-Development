<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UploadFile extends CI_Controller {
    function __construct(){
   
        parent::__construct();
       
        $this->load->model('Settings/Model_tipe_gambar');
        $this->load->model('Daftar/Model_gambar');
        $this->load->model('Daftar/Model_users');
    }
    function index($nik){
        $tbTipeGambar = $this->Model_tipe_gambar->read()->result();
        $tbFileUpload = $this->Model_gambar->where($nik)->result();
        $tbUsers = $this->Model_users->where($nik)->row();
        $data = array(
            'tbFileUpload' => $tbFileUpload,
            'tbTipeGambar' => $tbTipeGambar,
            'tbUsers' => $tbUsers
        );
        $this->load->view('daftar/users_upload_file',$data);
    }
    
    function Simpan(){
        $nik = $this->input->post('nik');
        $gambar = str_replace('.', '', $nik);
        $id_tipe_gambar = set_value('tipe_gambar');
        $searchGambar = $this->Model_gambar->search($nik, $id_tipe_gambar);
        $gambar_num = $searchGambar->num_rows();
        $config = array(
            'upload_path'   => './upload/foto/'.$id_tipe_gambar.'/',
            'allowed_types' => 'gif|jpg|png|jpeg',
            'max_size'      => 2048,
            'overwrite'     => TRUE,
            'file_name'     => $id_tipe_gambar.'_'.$gambar
        );
        $this->upload->initialize($config);
        if(!$this->upload->do_upload()){
            $this->session->set_flashdata('message',$this->upload->display_errors());
            $this->session->set_flashdata('type_message','danger');
            redirect('Daftar/UploadFile/index/'.$nik);
        }else{
            if($gambar_num > 0){
                $file = $this->upload->data();
                $data = array('gambar' => $file['file_name']);
                $this->Model_gambar->update($nik, $id_tipe_gambar,$data);
                $this->session->set_flashdata('message','Foto berhasil diubah.');
                $this->session->set_flashdata('type_message','success');
                redirect('Daftar/UploadFile/index/'.$nik);   
            }else{
                $file = $this->upload->data();
                $data = array(
                        'id_tipe_gambar' => $id_tipe_gambar,
                        'gambar' => $file['file_name'],
                        'nik_passport' => $nik
                );
                $this->Model_gambar->create($data);
                $this->session->set_flashdata('message','Foto berhasil diganti.');
                $this->session->set_flashdata('type_message','success');
                redirect('Daftar/UploadFile/index/'.$nik);
            }     
        }
    }
}