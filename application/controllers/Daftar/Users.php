<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Users extends CI_Controller {

	function __construct(){
		parent::__construct();
		if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
		if ($this->session->userdata('level') != 'DEVELOPMENT') {
			$this->session->set_flashdata('message','Hak akses ditolak.');
			$this->session->set_flashdata('type_message','danger');
			redirect('Auth');
		}
		$this->load->model('ServerSide/SS_daftar_users', '', TRUE);
		$this->load->model('Daftar/Tbl_daftar_users');
        $this->load->model('Daftar/Tbl_daftar_gambar');
        $this->load->model('Daftar/Tbl_daftar_cetak');
        $this->load->model('Daftar/Tbl_daftar_extra');
        $this->load->model('Daftar/Tbl_daftar_orangtua');
        $this->load->model('Daftar/Tbl_daftar_ukt');
		$this->load->model('Daftar/Tbl_daftar_mahasiswa');
        $this->load->model('Views/Daftar/View_daftar_users');
        $this->load->model('Views/Daftar/View_daftar_mahasiswa');
        $this->load->model('Views/Daftar/View_daftar_orangtua');
        $this->load->model('Views/Daftar/View_daftar_gambar');
        $this->load->model('Views/Daftar/View_daftar_kelulusan');
        $this->load->model('Views/Daftar/View_daftar_ukt');
		$this->load->model('Views/Daftar/View_daftar_extra');
        $this->load->model('Settings/Tbl_setting_agama');
        $this->load->model('Settings/Tbl_setting_asal_sekolah');
        $this->load->model('Settings/Tbl_setting_provinsi');
        $this->load->model('Settings/Tbl_setting_kabupaten');
        $this->load->model('Settings/Tbl_setting_kecamatan');
        $this->load->model('Settings/Tbl_setting_kelurahan');
        $this->load->model('Settings/Tbl_setting_jenis_pendaftaran');
        $this->load->model('Settings/Tbl_setting_jenis_pembiayaan');
        $this->load->model('Settings/Tbl_setting_jenis_tinggal');
        $this->load->model('Settings/Tbl_setting_jalur_masuk');
        $this->load->model('Settings/Tbl_setting_alat_transportasi');
        $this->load->model('Settings/Tbl_setting_pendidikan');
        $this->load->model('Settings/Tbl_setting_pekerjaan');
        $this->load->model('Settings/Tbl_setting_penghasilan');
        $this->load->model('Settings/Tbl_setting_rekening_listrik');
        $this->load->model('Settings/Tbl_setting_pembayaran_listrik');
        $this->load->model('Settings/Tbl_setting_rekening_pbb');
        $this->load->model('Settings/Tbl_setting_rumpun');
        $this->load->model('Settings/Tbl_setting_pembayaran_pbb');
        $this->load->model('Settings/Tbl_setting_tanggungan');
    }
    
    function index(){
        $rules = array(
            'select'    => 'YEAR(date_created) as tahun',
            'where'     => null,
            'order'     => null,
        );
        $rules[0] = array(
            'select'    => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
    	$data = array(
            'content'       => 'Daftar/Users/content',
            'css'           => 'Daftar/Users/css',
            'javascript'    => 'Daftar/Users/javascript',
            'modal'         => 'Daftar/Users/modal',
            'tahun'	=> $this->View_daftar_users->distinct($rules)->result(),
            'jalur_masuk' => $this->Tbl_setting_jalur_masuk->read($rules[0])->result()
		);
    	$this->load->view('index',$data);
    }

	function DataUsers(){
        $jalur_masuk = $this->input->post('jalur_masuk');
        $tahun = $this->input->post('tahun');
        $data = array(
            'jalur_masuk'   => $jalur_masuk,
            'tahun'         => $tahun,
            'content'       => 'Daftar/Users/users_data/content',
            'css'           => 'Daftar/Users/users_data/css',
            'javascript'    => 'Daftar/Users/users_data/javascript',
            'modal'         => 'Daftar/Users/users_data/modal',
        );
		$this->load->view('index', $data);
	}

    function Detail($id_daftar_users,$nomor_peserta){
        $rules = array(
            'select'    => null,
            'where'     => array(
                'id_daftar_users' => $id_daftar_users,
            ),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $rules2 = array(
            'select'    => null,
            'where'     => array(
                'nomor_peserta' => $nomor_peserta,
            ),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        /*$viewDaftarUsers = $this->View_daftar_users->whereAnd($search)->row();*/
        $tbFileUpload = $this->View_daftar_gambar->where($rules)->result();
        $tblDaftarCetak = $this->Tbl_daftar_cetak->where($rules)->result();
        $viewDaftarMahasiswa = $this->View_daftar_mahasiswa->where($rules)->row();
        $viewDaftarUsers = $this->View_daftar_users->where($rules)->row();
        $viewDaftarKelulusan = $this->View_daftar_kelulusan->where($rules2)->row();
        $viewDaftarUkt = $this->Tbl_daftar_ukt->where($rules)->row();
        $viewDaftarOrangtua = $this->View_daftar_orangtua->where($rules)->result();
        $viewDaftarExtra = $this->View_daftar_extra->where($rules)->row();
        $data = array(
            'content'       => 'Daftar/Users/users_detail/content',
            'css'           => 'Daftar/Users/users_detail/css',
            'javascript'    => 'Daftar/Users/users_detail/javascript',
            'modal'         => 'Daftar/Users/users_detail/modal',
            'tbFileUpload' => $tbFileUpload,
            'tblDaftarCetak' => $tblDaftarCetak,
            'viewDaftarUsers'   => $viewDaftarUsers,
            'viewDaftarMahasiswa'   => $viewDaftarMahasiswa,
            'viewDaftarKelulusan'   => $viewDaftarKelulusan,
            'viewDaftarUkt'   => $viewDaftarUkt,
            'viewDaftarOrangtua'   => $viewDaftarOrangtua,
            'viewDaftarExtra'   => $viewDaftarExtra,
        );
        $this->load->view('index', $data);
    }

	function Edit($id){
        echo $id;
	}

	function Edit_langkah1($id_daftar_users){
        
        $rules = array(
            'select'    => null,
            'where'     => array(
                'id_daftar_users' => $id_daftar_users,
            ),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $rules2 = array(
            'select'    => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );

        $tbBM  	            = $this->Tbl_daftar_mahasiswa->where($rules)->row();
        $viewExtra          = $this->View_daftar_extra->where($rules)->row();
        $viewDaftarUsers          = $this->View_daftar_users->where($rules)->row();
        $whereVBPDDIKTI  	= $this->View_daftar_mahasiswa->where($rules)->row();
        $tbSAG              = $this->Tbl_setting_agama->read($rules2)->result();
        $tbRumpun           = $this->Tbl_setting_rumpun->read($rules2)->result();
        $data = array(
            'tbSAG' => $tbSAG,
            'tbRumpun'  => $tbRumpun,
            'tbBM'  => $tbBM,
            'viewDaftarUsers'  => $viewDaftarUsers,
            'viewBM'=> $whereVBPDDIKTI,
            
            'content'       => 'Daftar/Users/Users_edit/users_edit_langkah1/content',
            'css'           => 'Daftar/Users/Users_edit/users_edit_langkah1/css',
            'javascript'    => 'Daftar/Users/Users_edit/users_edit_langkah1/javascript',
            'modal'         => 'Daftar/Users/Users_edit/users_edit_langkah1/modal',
        );
        $this->load->view('index',$data);
    }
    
    function Edit_langkah2($id_daftar_users){

        $rules = array(
            'select'    => null,
            'where'     => array(
                'id_daftar_users' => $id_daftar_users,
            ),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $rules2 = array(
            'select'    => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );

        $tbBM  	            = $this->Tbl_daftar_mahasiswa->where($rules)->row();
        $whereVBPDDIKTI  	= $this->View_daftar_mahasiswa->where($rules)->row();
        $viewDaftarUsers          = $this->View_daftar_users->where($rules)->row();
        $tbJenisPendaftaran     = $this->Tbl_setting_jenis_pendaftaran->read($rules2)->result();
        $tbJenisPembiayaan     = $this->Tbl_setting_jenis_pembiayaan->read($rules2)->result();
        $tbProv                 = $this->Tbl_setting_provinsi->read($rules2)->result();
        $data = array(
            'content'       => 'Daftar/Users/Users_edit/users_edit_langkah2/content',
            'css'           => 'Daftar/Users/Users_edit/users_edit_langkah2/css',
            'javascript'    => 'Daftar/Users/Users_edit/users_edit_langkah2/javascript',
            'modal'         => 'Daftar/Users/Users_edit/users_edit_langkah2/modal',
            'tbJenisPendaftaran' => $tbJenisPendaftaran,
            'tbJenisPembiayaan' => $tbJenisPembiayaan,
            'tbProv'  => $tbProv,
            'tbBM'  => $tbBM,
            'viewDaftarUsers' => $viewDaftarUsers,
            'viewBM'=> $whereVBPDDIKTI,
        );
        $this->load->view('index',$data);
    }

    function listKabupaten(){
        $id_provinsi = $this->input->post('id_provinsi');
        
        $kabupaten = $this->Tbl_setting_kabupaten->viewByProvinsi($id_provinsi);
        
        $lists = "<option value=''>Pilih</option>";
        
        foreach($kabupaten as $data){
          $lists .= "<option value='".$data->id_kabupaten."'>".$data->kabupaten."</option>";
        }
        
        $callback = array('list_kabupaten'=>$lists);
        echo json_encode($callback);
    }

    function listKecamatan(){
        $id_kabupaten = $this->input->post('id_kabupaten');
        
        $kecamatan = $this->Tbl_setting_kecamatan->viewByKabupaten($id_kabupaten);
        
        $lists = "<option value=''>Pilih</option>";
        
        foreach($kecamatan as $data){
          $lists .= "<option value='".$data->id_kecamatan."'>".$data->kecamatan."</option>";
        }
        
        $callback = array('list_kecamatan'=>$lists);
        echo json_encode($callback);
    }

    function listKelurahan(){
        $id_kecamatan = $this->input->post('id_kecamatan');
        
        $kelurahan = $this->Tbl_setting_kelurahan->viewByKecamatan($id_kecamatan);
        
        $lists = "<option value=''>Pilih</option>";
        
        foreach($kelurahan as $data){
          $lists .= "<option value='".$data->id_kelurahan."'>".$data->kelurahan."</option>";
        }
        
        $callback = array('list_kelurahan'=>$lists);
        echo json_encode($callback);
    }
    
    function Edit_langkah3($id_daftar_users){
        
        $rules = array(
            'select'    => null,
            'where'     => array(
                'id_daftar_users' => $id_daftar_users,
            ),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $rules2 = array(
            'select'    => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );

        $tbBM  	            = $this->Tbl_daftar_mahasiswa->where($rules)->row();
        $viewDaftarUsers          = $this->View_daftar_users->where($rules)->row();
        $whereVBPDDIKTI  	= $this->View_daftar_mahasiswa->where($rules)->row();
        $tbJenisTinggal      = $this->Tbl_setting_jenis_tinggal->read($rules2)->result();
        $tbAlatTransportasi      = $this->Tbl_setting_alat_transportasi->read($rules2)->result();
        $data = array(
            'content'       => 'Daftar/Users/Users_edit/users_edit_langkah3/content',
            'css'           => 'Daftar/Users/Users_edit/users_edit_langkah3/css',
            'javascript'    => 'Daftar/Users/Users_edit/users_edit_langkah3/javascript',
            'modal'         => 'Daftar/Users/Users_edit/users_edit_langkah3/modal',
            'tbJenisTinggal' => $tbJenisTinggal,
            'tbAlatTransportasi' => $tbAlatTransportasi,
            'tbBM'  => $tbBM,
            'viewDaftarUsers'  => $viewDaftarUsers,
            'viewBM'=> $whereVBPDDIKTI,
        );
        $this->load->view('index',$data);
    }
    
    function Edit_langkah4($id_daftar_users){
        
        $rules = array(
            'select'    => null,
            'where'     => array(
                'id_daftar_users' => $id_daftar_users,
            ),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $rules['ayah'] = array(
            'select'    => null,
            'where'     => array(
                'id_daftar_users' => $id_daftar_users,
                'orangtua'  => 'Ayah'
            ),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $rules['ibu'] = array(
            'select'    => null,
            'where'     => array(
                'id_daftar_users' => $id_daftar_users,
                'orangtua'  => 'Ibu'
            ),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $rules['wali'] = array(
            'select'    => null,
            'where'     => array(
                'id_daftar_users' => $id_daftar_users,
                'orangtua'  => 'Wali'
            ),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $rules2 = array(
            'select'    => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );

        $tbBM  	            = $this->Tbl_daftar_mahasiswa->where($rules)->row();
        $whereVBPDDIKTI  	= $this->View_daftar_mahasiswa->where($rules)->row();
        $tbExtra            = $this->Tbl_daftar_extra->where($rules)->row();
        $viewDOrangtuaAyah  	            = $this->View_daftar_orangtua->where($rules['ayah'])->row();
        $viewDOrangtuaIbu  	            = $this->View_daftar_orangtua->where($rules['ibu'])->row();
        $viewDOrangtuaWali  	            = $this->View_daftar_orangtua->where($rules['wali'])->row();
        $viewDaftarUsers          = $this->View_daftar_users->where($rules)->row();
        $tbPendidikan       = $this->Tbl_setting_pendidikan->read($rules2)->result();
        $tbPekerjaan        = $this->Tbl_setting_pekerjaan->read($rules2)->result();
        $tbPenghasilan      = $this->Tbl_setting_penghasilan->read($rules2)->result();
        $data = array(
            'content'       => 'Daftar/Users/Users_edit/users_edit_langkah4/content',
            'css'           => 'Daftar/Users/Users_edit/users_edit_langkah4/css',
            'javascript'    => 'Daftar/Users/Users_edit/users_edit_langkah4/javascript',
            'modal'         => 'Daftar/Users/Users_edit/users_edit_langkah4/modal',
            'tbPendidikan' => $tbPendidikan,
            'tbPekerjaan' => $tbPekerjaan,
            'tbPenghasilan' => $tbPenghasilan,
            'tbBM'  => $tbBM,
            'viewDOrangtuaAyah'  => $viewDOrangtuaAyah,
            'viewDOrangtuaIbu'  => $viewDOrangtuaIbu,
            'viewDOrangtuaWali'  => $viewDOrangtuaWali,
            'viewDaftarUsers'  => $viewDaftarUsers,
            'tbExtra'  => $tbExtra,
            'viewBM'=> $whereVBPDDIKTI,
        );
        $this->load->view('index',$data);
    }
    
    function Edit_langkah5($id_daftar_users){

        
        $rules = array(
            'select'    => null,
            'where'     => array(
                'id_daftar_users' => $id_daftar_users,
            ),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $rules2 = array(
            'select'    => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );

        $tbBM  	            = $this->Tbl_daftar_mahasiswa->where($rules)->row();
        $whereVBPDDIKTI  	= $this->View_daftar_mahasiswa->where($rules)->row();
        $tbExtra            = $this->Tbl_daftar_extra->where($rules)->row();
        $tbRekeningListrik  = $this->Tbl_setting_rekening_listrik->read($rules2)->result();
        $tbRekeningPBB      = $this->Tbl_setting_rekening_pbb->read($rules2)->result();
        $tbPembayaranListrik  = $this->Tbl_setting_pembayaran_listrik->read($rules2)->result();
        $tbPembayaranPBB    = $this->Tbl_setting_pembayaran_pbb->read($rules2)->result();
        $tbTanggungan       = $this->Tbl_setting_tanggungan->read($rules2)->result();
        $tbAsalSekolah       = $this->Tbl_setting_asal_sekolah->read($rules2)->result();
        $viewDaftarUsers          = $this->View_daftar_users->where($rules)->row();
        $data = array(
            'content'       => 'Daftar/Users/Users_edit/users_edit_langkah5/content',
            'css'           => 'Daftar/Users/Users_edit/users_edit_langkah5/css',
            'javascript'    => 'Daftar/Users/Users_edit/users_edit_langkah5/javascript',
            'modal'         => 'Daftar/Users/Users_edit/users_edit_langkah5/modal',
            'tbRekeningListrik' => $tbRekeningListrik,
            'tbRekeningPBB' => $tbRekeningPBB,
            'tbPembayaranListrik' => $tbPembayaranListrik,
            'tbPembayaranPBB' => $tbPembayaranPBB,
            'viewDaftarUsers'  => $viewDaftarUsers,
            'tbTanggungan' => $tbTanggungan,
            'tbAsalSekolah' => $tbAsalSekolah,
            'tbExtra' => $tbExtra,
            'tbBM'  => $tbBM,
            'viewBM'=> $whereVBPDDIKTI,
        );
        $this->load->view('index',$data);
    }
    
    function Edit_langkah6($id_daftar_users){
        $this->session->set_flashdata('message','Lakukan upload foto di website daftar calon mahasiswa');
        $this->session->set_flashdata('type_message','danger');
        redirect('Daftar/Users/Edit_langkah1/'.$id_daftar_users);
        $rules = array(
            'select'    => null,
            'where'     => array(
                'id_daftar_users' => $id_daftar_users,
            ),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );

        $tbBM  	            = $this->Tbl_daftar_mahasiswa->where($rules)->row();
        $viewDaftarUsers          = $this->View_daftar_users->where($rules)->row();
        $whereVBPDDIKTI  	= $this->View_daftar_mahasiswa->where($rules)->row();
        $data = array(
            'content'       => 'Daftar/Users/Users_edit/users_edit_langkah6/content',
            'css'           => 'Daftar/Users/Users_edit/users_edit_langkah6/css',
            'javascript'    => 'Daftar/Users/Users_edit/users_edit_langkah6/javascript',
            'modal'         => 'Daftar/Users/Users_edit/users_edit_langkah6/modal',
            'tbBM'  => $tbBM,
            'viewDaftarUsers'  => $viewDaftarUsers,
            'viewBM'=> $whereVBPDDIKTI,
        );
        $this->load->view('index',$data);
    }

	function Simpan_tahap1(){
        $id_daftar_users = $this->input->post('id_daftar_users');
        $rules  =   [
            ['field' => 'jk',           'label' => 'Jenis Kelamin',       'rules' => 'required'],
            ['field' => 'tmp_lhr',      'label' => 'Tempat lahir',        'rules' => 'required'],
            ['field' => 'id_agama',     'label' => 'Agama',               'rules' => 'required'],
            ['field' => 'nisn',         'label' => 'NISN',                'rules' => 'required'],
            ['field' => 'npwp',         'label' => 'NPWP'],
            ['field' => 'warga_negara', 'label' => 'Warga Negara',        'rules' => 'required'],
            ['field' => 'tgl_lhr',      'label' => 'Tanggal Lahir',       'rules' => 'required'],
            // ['field' => 'id_rumpun',    'label' => 'Rumpun',       'rules' => 'required'],
        ];
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == FALSE){
            $this->session->set_flashdata('message',validation_errors());
            $this->session->set_flashdata('type_message','danger');
            redirect('Daftar/Users/Edit_langkah1/'.$id_daftar_users);
        }else{
            $rules = array(
                'where' => array(
                    'id_daftar_users' => $id_daftar_users,
                ),
                'data'  => array(
                    'jenis_kelamin' 	=> set_value('jk'),
                    'id_agama'			=> set_value('id_agama'),
                    'tmp_lhr'           => strtoupper(set_value('tmp_lhr')),
                    'nisn'              => set_value('nisn'),
                    'npwp'              => set_value('npwp'),
                    'warga_negara'      => set_value('warga_negara'),
                    'id_rumpun'      => set_value('id_rumpun'),
                ),
            );
            $rules2 = array(
                'where' => array(
                    'id_daftar_users' => $id_daftar_users,
                ),
                'data'  => array(
                    'tgl_lhr'          => set_value('tgl_lhr'),
                    'nik_passport'          => $this->input->post('nik_passport'),
                ),
            );
            if ($this->Tbl_daftar_mahasiswa->update($rules)) {
                if ($this->Tbl_daftar_users->update($rules2)) {
                    $this->session->set_flashdata('message','Data berhasil disimpan');
                    $this->session->set_flashdata('type_message','success');
                    redirect('Daftar/Users/Edit_langkah1/'.$id_daftar_users);	
                }else{
                    $this->session->set_flashdata('message','Terjadi kesalahan dalam proses simpan data');
                    $this->session->set_flashdata('type_message','danger');
                    redirect('Daftar/Users/Edit_langkah1/'.$id_daftar_users);
                }
            }else{
                $this->session->set_flashdata('message','Terjadi kesalahan dalam proses simpan data');
                $this->session->set_flashdata('type_message','danger');
                redirect('Daftar/Users/Edit_langkah1/'.$id_daftar_users);
            }
        }
    }
    
    function Simpan_tahap2(){
        $id_daftar_users = $this->input->post('id_daftar_users');
        $rules  =   [
            ['field' => 'id_jns_pndftrn', 'label' => 'Jenis Pendaftaran',   'rules' => 'required'],
            ['field' => 'mulai_semester', 'label' => 'Mulai Semester',      'rules' => 'required'],
            ['field' => 'alamat',         'label' => 'Alamat',              'rules' => 'required'],
            ['field' => 'rt',             'label' => 'RT',                  'rules' => 'required'],
            ['field' => 'rw',             'label' => 'RW',                  'rules' => 'required'],
            ['field' => 'id_kelurahan',            'label' => 'Kelurahan'],
            ['field' => 'nama_dusun',     'label' => 'nama_dusun'],
            ['field' => 'kode_pos',       'label' => 'Kode POS',            'rules' => 'required'],
        ];
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == FALSE){
            $this->session->set_flashdata('message',validation_errors());
            $this->session->set_flashdata('type_message','danger');
            redirect('Daftar/Users/Edit_langkah2/'.$id_daftar_users);
        }else{
            $id_kelurahan = $this->input->post('id_kelurahan');
            if (empty($id_kelurahan)){
                $id_kelurahan = $this->input->post('old_id_kelurahan');
            }
            $rules = array(
                'where' => array(
                    'id_daftar_users' => $id_daftar_users,
                ),
                'data'  => array(
                    'id_jns_pndftrn' 	=> set_value('id_jns_pndftrn'),
                    'id_jenis_pembiayaan' 	=> $this->input->post('id_jenis_pembiayaan'),
                    'mulai_semester'		=> set_value('mulai_semester'),
                    'jalan'             => strtoupper(set_value('alamat')),
                    'rt'                => set_value('rt'),
                    'rw'                => set_value('rw'),
                    'id_kelurahan'      => $id_kelurahan,
                    'nama_dusun'        => strtoupper(set_value('nama_dusun')),
                    'kode_pos'          => strtoupper(set_value('kode_pos')),
                ),
            );
            if ($this->Tbl_daftar_mahasiswa->update($rules)) {
                $this->session->set_flashdata('message','Data berhasil disimpan');
                $this->session->set_flashdata('type_message','success');
                redirect('Daftar/Users/Edit_langkah2/'.$id_daftar_users);	
            }else{
                $this->session->set_flashdata('message','Terjadi kesalahan dalam proses simpan data');
                $this->session->set_flashdata('type_message','danger');
                redirect('Daftar/Users/Edit_langkah2/'.$id_daftar_users);
            }
        }
    }
    
    function Simpan_tahap3(){
        $id_daftar_users = $this->input->post('id_daftar_users');
        $rules  =   [
            ['field' => 'id_jns_tinggal',       'label' => 'Jenis Tinggal',     'rules' => 'required'],
            ['field' => 'id_alat_transportasi', 'label' => 'Alat Transportasi', 'rules' => 'required'],
            ['field' => 'tlp_rmh',              'label' => 'Telpon Rumah'],
            ['field' => 'nmr_hp',                'label' => 'Nomor HP',          'rules' => 'required'],
            ['field' => 'email',                'label' => 'Email',             'rules' => 'required'],
            ['field' => 'terima_kps',           'label' => 'Terima KPS',        'rules' => 'required'],
            ['field' => 'no_kps',               'label' => 'Nomor KPS'],
        ];
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == FALSE){
            $this->session->set_flashdata('message',validation_errors());
            $this->session->set_flashdata('type_message','danger');
            redirect('Daftar/Users/Edit_langkah3/'.$id_daftar_users);
        }else{
            $data = array(
                
            );
            $rules = array(
                'where' => array(
                    'id_daftar_users' => $id_daftar_users,
                ),
                'data'  => array(
                    'id_jns_tinggal' 	        => set_value('id_jns_tinggal'),
                    'id_alat_transportasi'	    => set_value('id_alat_transportasi'),
                    'tlp_rmh'                   => strtoupper(set_value('tlp_rmh')),
                    'terima_kps'                => strtoupper(set_value('terima_kps')),
                    'no_kps'                    => strtoupper(set_value('no_kps')),
                ),
            );
            $rules2 = array(
                'where' => array(
                    'id_daftar_users' => $id_daftar_users,
                ),
                'data'  => array(
                    'username'                     => set_value('email'),
                    'nmr_hp'                     => set_value('nmr_hp'),
                ),
            );
            if ($this->Tbl_daftar_mahasiswa->update($rules)) {
                if ($this->Tbl_daftar_users->update($rules2)) {
                    $this->session->set_flashdata('message','Data berhasil disimpan');
                    $this->session->set_flashdata('type_message','success');
                    redirect('Daftar/Users/Edit_langkah3/'.$id_daftar_users);	
                }else{
                    $this->session->set_flashdata('message','Terjadi kesalahan dalam proses simpan data');
                    $this->session->set_flashdata('type_message','danger');
                    redirect('Daftar/Users/Edit_langkah3/'.$id_daftar_users);
                }	
            }else{
                $this->session->set_flashdata('message','Terjadi kesalahan dalam proses simpan data');
                $this->session->set_flashdata('type_message','danger');
                redirect('Daftar/Users/Edit_langkah3/'.$id_daftar_users);
            }
        }
    }
    
    function Simpan_tahap4(){
        
        $id_daftar_users = $this->input->post('id_daftar_users');
        $rules  =   [
            ['field' => 'nik_orangtua',       'label' => 'NIK Orangtua', 'rules' => 'required'],
            ['field' => 'nama_orangtua', 'label' => 'Nama Orangtua', 'rules' => 'required'],
            ['field' => 'tgl_lhr_orangtua', 'label' => 'Tanggal Lahir Orangtua', 'rules' => 'required'],
            ['field' => 'id_pendidikan',   'label' => 'Pendidikan Orangtua', 'rules' => 'required'],
            ['field' => 'id_pekerjaan',   'label' => 'Pekerjaan Orangtua', 'rules' => 'required'],
            ['field' => 'id_penghasilan',   'label' => 'Penghasilan Orangtua', 'rules' => 'required'],
            ['field' => 'nominal_penghasilan',   'label' => 'Nominal Penghasilan Orangtua', 'rules' => 'required'],
            ['field' => 'terbilang_penghasilan',   'label' => 'Terbilang Penghasilan Orangtua', 'rules' => 'required'],
        ];
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == FALSE){
            $this->session->set_flashdata('message',validation_errors());
            $this->session->set_flashdata('type_message','danger');
            redirect('Daftar/Users/Edit_langkah4/'.$id_daftar_users);
        }else{
            $rules = array(
                'where' => array(
                    'id_daftar_users' => $id_daftar_users,
                    'orangtua' => $this->input->post('orangtua')
                ),
                'data'  => array(
                    'nik_orangtua' 	            => $this->input->post('nik_orangtua'),
                    'nama_orangtua'			    => strtoupper($this->input->post('nama_orangtua')),
                    'tgl_lhr_orangtua'          => $this->input->post('tgl_lhr_orangtua'),
                    'id_pendidikan'    => $this->input->post('id_pendidikan'),
                    'id_pekerjaan'     => $this->input->post('id_pekerjaan'),
                    'id_penghasilan'   => $this->input->post('id_penghasilan'),
                    'nominal_penghasilan'      => $this->input->post('nominal_penghasilan'),
                    'terbilang_penghasilan'    => strtoupper($this->input->post('terbilang_penghasilan')),
                ),
            );
            if ($this->Tbl_daftar_orangtua->update($rules)) {
                $this->session->set_flashdata('message','Data berhasil disimpan');
                $this->session->set_flashdata('type_message','success');
                redirect('Daftar/Users/Edit_langkah4/'.$id_daftar_users);
            }else{
                $this->session->set_flashdata('message','Terjadi kesalahan dalam proses simpan data');
                $this->session->set_flashdata('type_message','danger');
                redirect('Daftar/Users/Edit_langkah4/'.$id_daftar_users);
            }
        }
    }
    
    function Simpan_tahap5(){
        $id_daftar_users = $this->input->post('id_daftar_users');
        $rules  =   [
            ['field' => 'id_rekening_listrik',       'label' => 'Rekening Listrik',             'rules' => 'required'],
            ['field' => 'id_rekening_pbb', 'label' => 'Rekening PBB',                     'rules' => 'required'],
            ['field' => 'id_pembayaran_listrik', 'label' => 'Pembayaran Listrik',                     'rules' => 'required'],
            ['field' => 'id_pembayaran_pbb',   'label' => 'Pembayaran PBB',                    'rules' => 'required'],
            ['field' => 'id_tanggungan',   'label' => 'Tanggungan',                    'rules' => 'required'],
            ['field' => 'ukuran_baju',    'label' => 'Ukuran Baju',      'rules' => 'required'],
            ['field' => 'ukuran_jas',     'label' => 'Ukuran Jas',       'rules' => 'required'],
        ];
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == FALSE){
            $this->session->set_flashdata('message',validation_errors());
            $this->session->set_flashdata('type_message','danger');
            redirect('Daftar/Users/Edit_langkah5/'.$id_daftar_users);
        }else{
            $rules = array(
                'where' => array(
                    'id_daftar_users' => $id_daftar_users,
                ),
                'data'  => array(
                    'id_asal_sekolah' 	=> $this->input->post('id_asal_sekolah'),
                    'id_rekening_listrik' 	=> set_value('id_rekening_listrik'),
                    'id_rekening_pbb'			=> set_value('id_rekening_pbb'),
                    'id_pembayaran_listrik'           => set_value('id_pembayaran_listrik'),
                    'id_pembayaran_pbb'           => set_value('id_pembayaran_pbb'),
                    'id_tanggungan'           => set_value('id_tanggungan'),
                    'ukuran_jas'              => set_value('ukuran_jas'),
                    'ukuran_baju'              => set_value('ukuran_baju'),
                ),
            );
            if ($this->Tbl_daftar_extra->update($rules)) {
                $this->session->set_flashdata('message','Data berhasil disimpan');
                $this->session->set_flashdata('type_message','success');
                redirect('Daftar/Users/Edit_langkah5/'.$id_daftar_users);
            }else{
                $this->session->set_flashdata('message','Terjadi kesalahan dalam proses simpan data');
                $this->session->set_flashdata('type_message','danger');
                redirect('Daftar/Users/Edit_langkah5/'.$id_daftar_users);
            }
        }
    }
    
    function Simpan_tahap6(){
        $id_daftar_users = $this->input->post('id_daftar_users');
        $rules  =   [
            ['field' => 'userfile',       'label' => 'Foto',             'rules' => 'required'],
        ];
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == TRUE){
            $this->session->set_flashdata('message',validation_errors());
            $this->session->set_flashdata('type_message','danger');
            redirect('Daftar/Users/Edit_langkah6/'.$id_daftar_users);
        }else{
            if ($_FILES['userfile']['size']>0) {
                $config = array(
                    'upload_path'   => './upload/foto/',
                    'allowed_types' => 'gif|jpg|png|jpeg',
                    'max_size'      => 2048,
                    'overwrite'     => TRUE,
                    'file_name'     => $id_daftar_users
                );
                $this->upload->initialize($config);
                if(!$this->upload->do_upload()){
                    $this->session->set_flashdata('message',$this->upload->display_errors());
                    $this->session->set_flashdata('type_message','danger');
                    redirect('Daftar/Users/Edit_langkah6/'.$id_daftar_users);
                }else{
                    $file = $this->upload->data();
                    $rules = array(
                        'where' => array(
                            'id_daftar_users' => $id_daftar_users,
                        ),
                        'data'  => array(
                            'foto' 				=> $file['file_name'],
                        ),
                    );
                    if ($this->Tbl_daftar_users->update($rules)) {
                    	$this->session->set_flashdata('message','Foto berhasil disimpan');
                    	$this->session->set_flashdata('type_message','success');
                    	redirect('Daftar/Users/Edit_langkah1/'.$id_daftar_users);	
                    }else{
                    	$this->session->set_flashdata('message','Terjadi kesalahan dalam proses simpan data');
                    	$this->session->set_flashdata('type_message','danger');
                    	redirect('Daftar/Users/Edit_langkah6/'.$id_daftar_users);
                    }
                }  
            }else{
                $this->session->set_flashdata('message','Terjadi kesalahan dalam proses simpan data');
                $this->session->set_flashdata('type_message','danger');
                redirect('Daftar/Users/Edit_langkah6/'.$id_daftar_users);
            }
        }
    }

    function Delete($id){
        echo $id;
    }

    function Update($id, $np){
        if(!empty($this->input->post('password'))){
            $rules = array(
                'where' => array(
                    'id_daftar_users' => $id,
                ),
                'data'  => array(
                    'nik_passport' 				=> $this->input->post('nik'),
                    'tgl_lhr' 				=> $this->input->post('tgl'),
                    'nmr_hp' 				=> $this->input->post('kontak'),
                    'username' 				=> $this->input->post('email'),
                    'verifikasi' 				=> $this->input->post('verifikasi'),
                    'password' 				=> md5(md5($this->input->post('password'))),
                ),
            );
            if ($this->Tbl_daftar_users->update($rules)) {
                $this->session->set_flashdata('message','Berhasil diubah');
                $this->session->set_flashdata('type_message','success');
                redirect('Daftar/Users/Detail/'.$id.'/'.$np);	
            }else{
                $this->session->set_flashdata('message','Terjadi kesalahan dalam proses simpan data');
                $this->session->set_flashdata('type_message','danger');
                redirect('Daftar/Users/Detail/'.$id.'/'.$np);
            }
        }else{
            $rules = array(
                'where' => array(
                    'id_daftar_users' => $id,
                ),
                'data'  => array(
                    'nik_passport' 				=> $this->input->post('nik'),
                    'tgl_lhr' 				=> $this->input->post('tgl'),
                    'nmr_hp' 				=> $this->input->post('kontak'),
                    'username' 				=> $this->input->post('email'),
                    'verifikasi' 				=> $this->input->post('verifikasi'),
                ),
            );
            if ($this->Tbl_daftar_users->update($rules)) {
                $this->session->set_flashdata('message','Berhasil diubah');
                $this->session->set_flashdata('type_message','success');
                redirect('Daftar/Users/Detail/'.$id.'/'.$np);	
            }else{
                $this->session->set_flashdata('message','Terjadi kesalahan dalam proses simpan data');
                $this->session->set_flashdata('type_message','danger');
                redirect('Daftar/Users/Detail/'.$id.'/'.$np);
            }
        }
    }


	function Json(){
		$fetch_data = $this->SS_daftar_users->make_datatables();
		$data = array();
		foreach($fetch_data as $row)
		{
            $sub_array = array();
            $sub_array[] = "
            <div class=\"btn-group\">
                <button type=\"button\" class=\"btn btn-dafault dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                    <i class='fa fa-gear'></i> <span class=\"caret\"></span>
                </button>
                <ul class=\"dropdown-menu\">
                    <li><a href=\"".base_url('Daftar/Users/Detail/'.$row->id_daftar_users.'/'.$row->nomor_peserta)."\" target='_blank'>Detail</a></li>
                </ul>
            </div>";
			$sub_array[] = $row->nik_passport;
			$sub_array[] = $row->nomor_peserta;
			$sub_array[] = $row->nama;
            $sub_array[] = $row->jalur_masuk;
            $sub_array[] = $row->username;
            $sub_array[] = $row->verifikasi;
			$data[] = $sub_array;
		}
		$output = array(
			"draw"				=>	intval($_POST["draw"]),
			"recordsTotal"		=>	$this->SS_daftar_users->get_all_data(),
			"recordsFiltered"	=>	$this->SS_daftar_users->get_filtered_data(),
			"data"				=>	$data
		);
		echo json_encode($output);
	}
}
