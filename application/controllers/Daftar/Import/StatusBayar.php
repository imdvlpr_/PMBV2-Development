<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class StatusBayar extends CI_Controller {

	function __construct(){
		parent::__construct();
		if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
		if ($this->session->userdata('level') != 'DEVELOPMENT') {
			$this->session->set_flashdata('message','Hak akses ditolak.');
			$this->session->set_flashdata('type_message','danger');
			redirect('Auth');
		}
		$this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
		$this->load->model('Daftar/Tbl_daftar_kelulusan');
        $this->load->model('Daftar/Tbl_daftar_users');
        $this->load->model('Views/Daftar/View_daftar_users');
		$this->load->model('Daftar/Tbl_daftar_mahasiswa');
        $this->load->model('Daftar/Tbl_daftar_extra');
        $this->load->model('Daftar/Tbl_daftar_ukt');
		$this->load->model('Settings/Tbl_setting_jalur_masuk');
	}

	function index(){
		$rules = array(
			'select'    => null,
			'order'     => null,
			'limit'     => null,
			'pagging'   => null,
		);
		$tbSJalurMasuk = $this->Tbl_setting_jalur_masuk->read($rules)->result();
		$data = array(
			'content'       => 'Daftar/Import/status_bayar/content',
            'css'           => 'Daftar/Import/status_bayar/css',
            'javascript'    => 'Daftar/Import/status_bayar/javascript',
            'modal'         => 'Daftar/Import/status_bayar/modal',
			'tbSJalurMasuk' => $tbSJalurMasuk,
		);
    	$this->load->view('index', $data);
	}
	
	function Import(){
		
		$config = array(
			'upload_path'   => './import/daftar/',
			'allowed_types' => 'xls|xlsx|csv|ods|ots',
			'max_size'      => 51200,
			'overwrite'     => TRUE,
			'file_name'     => 'NIM_JM00_STATUS_UKT_'.date('Y').'_'.date('H i s d m Y'),
		);
		$this->upload->initialize($config);
		if(!$this->upload->do_upload()){
			$this->session->set_flashdata('message',$this->upload->display_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('Daftar/Import/StatusBayar/');
		}else{
			$file = $this->upload->data();
			$inputFileName = 'import/daftar/'.$file['file_name'];
			try {
				$inputFileType	= IOFactory::identify($inputFileName);
				$objReader		= IOFactory::createReader($inputFileType);
				$objPHPExcel	= $objReader->load($inputFileName);
			} catch (Exception $e) {
				die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'" : '.$e->getMessage());
			}
			$sheet	= $objPHPExcel->getSheet(0);
			$highestRow	= $sheet->getHighestRow();
			for ($row = 2; $row <= $highestRow; $row++) {
				$nomor_peserta	= $sheet->getCellByColumnAndRow(0,$row)->getValue();
				$nama			= str_replace('\'','`',strtoupper($sheet->getCellByColumnAndRow(1,$row)->getValue()));
				$status	= $sheet->getCellByColumnAndRow(2,$row)->getValue();
				$rules = array(
					'select'    => null,
					'like'      => array(
                        'nomor_peserta' => $nomor_peserta,
					),
					'or_like'   => null,
					'order'     => null,
					'limit'     => null,
					'pagging'   => null,
				);
                $tbUsers_row = $this->View_daftar_users->like($rules)->num_rows();
				$tbUsers = $this->View_daftar_users->like($rules)->row();
				if($tbUsers_row > 0){
					$rules2 = array(
						'where' => array(
							'id_daftar_users' => $tbUsers->id_daftar_users
						),
						'data'  => array(
							'status_bayar' => "$status",
							'date_updated' => date('Y-m-d H:i:s'),
						),
					);
					$this->Tbl_daftar_ukt->update($rules2);
                }
			}
			$this->session->set_flashdata('message','Import berhasil.');
			$this->session->set_flashdata('type_message','success');
			redirect('Daftar/Import/StatusBayar/');
		}
	}
}