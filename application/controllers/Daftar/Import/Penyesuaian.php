<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Penyesuaian extends CI_Controller {

	function __construct(){
		parent::__construct();
		if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
		if ($this->session->userdata('level') != 'DEVELOPMENT') {
			$this->session->set_flashdata('message','Hak akses ditolak.');
			$this->session->set_flashdata('type_message','danger');
			redirect('Auth');
		}
		$this->load->model('Daftar/Tbl_daftar_kelulusan');
		$this->load->model('Daftar/Tbl_daftar_users');
		$this->load->model('Daftar/Tbl_daftar_mahasiswa');
		$this->load->model('Daftar/Tbl_daftar_extra');
		$this->load->model('Daftar/Tbl_daftar_gambar');
		$this->load->model('Daftar/Tbl_daftar_ukt');
		$this->load->model('Daftar/Tbl_daftar_ukt');
		$this->load->model('Penyesuaian/Tbl_penyesuaian_users');
		$this->load->model('Penyesuaian/Tbl_penyesuaian_pddikti');
		$this->load->model('Penyesuaian/Tbl_penyesuaian_extra');
		$this->load->model('Penyesuaian/Tbl_penyesuaian_ukt');
		$this->load->model('Penyesuaian/Tbl_penyesuaian_gambar');
		$this->load->model('Pradaftar/Tbl_pradaftar_kelulusan');
		$this->load->model('Pradaftar/Tbl_pradaftar_biodata');
		$this->load->model('Pradaftar/Tbl_pradaftar_users');
		$this->load->model('Settings/Tbl_setting_jalur_masuk');
	}

	function Import(){
		$tahun = $this->input->post('tahun');
		$search = array(
			'YEAR(date_created)' => $tahun,
		);
		$tblPddikti = $this->Tbl_daftar_pddikti->whereAnd($search)->result();

		foreach($tblPddikti as $value){
			if($value->nim == 0){
			}else if($value->nim == ""){
			}else{
				$search2 = array(
					'nik_passport' => $value->nik_passport,
				);

				$tblUsers 		= $this->Tbl_daftar_users->whereAnd($search2)->row();
				$tblExtra 		= $this->Tbl_daftar_extra->whereAnd($search2)->row();
				$tblUkt			= $this->Tbl_daftar_ukt->whereAnd($search2)->row();
				$tblGambar		= $this->Tbl_daftar_gambar->whereAnd($search2)->result();

				$data1 = array(
					'nik_passport'      => $tblUsers->nik_passport,
					'nomor_peserta' 	=> $tblUsers->nomor_peserta,
					'username'			=> $tblUsers->username,
					'password'			=> $tblUsers->password,
					'id_jlr_msk'		=> $tblUsers->id_jlr_msk,
					'nmr_hp'			=> $tblUsers->nmr_hp,
					'tgl_lhr'			=> $tblUsers->tgl_lhr,
					'verifikasi'		=> "BELUM VERIFIKASI",
					'cetak'				=> 0,
					'foto'				=> "-",
				);
				$this->Tbl_penyesuaian_users->create($data1);
			}
		}
		$this->session->set_flashdata('message','Import berhasil.');
		$this->session->set_flashdata('type_message','success');
		redirect('Daftar/Import/Penyesuaian/PenyesuaianUsers');
	}

	function PenyesuaianUKT(){
		$data = array(
			'content'       => 'Daftar/Import/penyesuaian_ukt/content',
            'css'           => 'Daftar/Import/penyesuaian_ukt/css',
            'javascript'    => 'Daftar/Import/penyesuaian_ukt/javascript',
            'modal'         => 'Daftar/Import/penyesuaian_ukt/modal',
		);
    	$this->load->view('index', $data);
	}

	function PenyesuaianUsers(){
    	$data = array(
			'content'       => 'Daftar/Import/penyesuaian_users/content',
            'css'           => 'Daftar/Import/penyesuaian_users/css',
            'javascript'    => 'Daftar/Import/penyesuaian_users/javascript',
            'modal'         => 'Daftar/Import/penyesuaian_users/modal',
		);
    	$this->load->view('index', $data);
	}

	function PenyesuaianPddikti(){
    	$data = array(
			'content'       => 'Daftar/Import/penyesuaian_pddikti/content',
            'css'           => 'Daftar/Import/penyesuaian_pddikti/css',
            'javascript'    => 'Daftar/Import/penyesuaian_pddikti/javascript',
            'modal'         => 'Daftar/Import/penyesuaian_pddikti/modal',
		);
    	$this->load->view('index', $data);
	}

	function PenyesuaianExtra(){
    	$data = array(
			'content'       => 'Daftar/Import/penyesuaian_extra/content',
            'css'           => 'Daftar/Import/penyesuaian_extra/css',
            'javascript'    => 'Daftar/Import/penyesuaian_extra/javascript',
            'modal'         => 'Daftar/Import/penyesuaian_extra/modal',
		);
    	$this->load->view('index', $data);
	}

	function PenyesuaianGambar(){
    	$data = array(
			'content'       => 'Daftar/Import/penyesuaian_gambar/content',
            'css'           => 'Daftar/Import/penyesuaian_gambar/css',
            'javascript'    => 'Daftar/Import/penyesuaian_gambar/javascript',
            'modal'         => 'Daftar/Import/penyesuaian_gambar/modal',
		);
    	$this->load->view('index', $data);
	}

	function ImportUkt(){
		$tahun = $this->input->post('tahun');
		$rules = array(
			'select'    => null,
			'where'     => array(
				'YEAR(date_created)' => $tahun,
			),
			'or_where'  => null,
			'order'     => null,
			'limit'     => null,
			'pagging'   => null,
		);
		$tblPddikti = $this->Tbl_daftar_pddikti->where($rules)->result();

		foreach($tblPddikti as $value){
			if($value->nim == 0){
			}else if($value->nim == ""){
			}else{
				$rules2 = array(
					'select'    => null,
					'where'     => array(
						'nik_passport' => $value->nik_passport,
					),
					'or_where'  => null,
					'order'     => null,
					'limit'     => null,
					'pagging'   => null,
				);
				$tblUkt			= $this->Tbl_daftar_ukt->where($rules2)->row();

				$data2 = array(
					'nik_passport'		=> $tblUkt->nik_passport,
					'score'				=> $tblUkt->score,
					'kategori'			=> $tblUkt->kategori,
					'jumlah'			=> $tblUkt->jumlah,
					'status'			=> $tblUkt->status,
					'status_bayar'			=> "1",
					'rekomendasi'		=> "0",
					'created_by'			=> $tblUkt->created_by,
					'updated_by'			=> $tblUkt->updated_by,
					'date_created'		=> $tblUkt->date_created,
					'date_updated'		=> $tblUkt->date_updated,
				);
				$this->Tbl_penyesuaian_ukt->create($data2);
			}
		}
		$this->session->set_flashdata('message','Import berhasil.');
		$this->session->set_flashdata('type_message','success');
		redirect('Daftar/Import/Penyesuaian/PenyesuaianUKT');
	}

	function ImportExtra(){
		$tahun = $this->input->post('tahun');
		$rules = array(
			'select'    => null,
			'where'     => array(
				'YEAR(date_created)' => $tahun,
			),
			'or_where'  => null,
			'order'     => null,
			'limit'     => null,
			'pagging'   => null,
		);
		$tblPddikti = $this->Tbl_daftar_pddikti->where($rules)->result();

		foreach($tblPddikti as $value){
			if($value->nim == 0){
			}else if($value->nim == ""){
			}else{
				$rules2 = array(
					'select'    => null,
					'where'     => array(
						'nik_passport' => $value->nik_passport,
					),
					'or_where'  => null,
					'order'     => null,
					'limit'     => null,
					'pagging'   => null,
				);
				$tblExtra 		= $this->Tbl_daftar_extra->where($rules2)->row();

				$data3 = array(
					'nik_passport'  				=> $tblExtra->nik_passport,
	                'id_tanggungan' 				=> $tblExtra->id_tanggungan,
	                'id_rekening_listrik' 			=> $tblExtra->id_rekening_listrik,
	                'id_pembayaran_listrik' 		=> $tblExtra->id_pembayaran_listrik,
	                'id_pembayaran_pbb' 			=> $tblExtra->id_pembayaran_pbb,
	                'id_rekening_pbb'		 		=> $tblExtra->id_rekening_pbb,
	                'nominal_penghasilan_ayah' 		=> $tblExtra->nominal_penghasilan_ayah,
	                'terbilang_penghasilan_ayah' 	=> $tblExtra->terbilang_penghasilan_ayah,
	                'nominal_penghasilan_ibu' 		=> $tblExtra->nominal_penghasilan_ibu,
	                'terbilang_penghasilan_ibu' 	=> $tblExtra->terbilang_penghasilan_ibu,
	                'nominal_penghasilan_wali' 		=> $tblExtra->nominal_penghasilan_wali,
	                'terbilang_penghasilan_wali' 	=> $tblExtra->terbilang_penghasilan_wali,
	                'date_created'					=> $tblExtra->date_created,
	                'date_updated'					=> $tblExtra->date_updated,
				);
				$this->Tbl_penyesuaian_extra->create($data3);
			}
		}
		$this->session->set_flashdata('message','Import berhasil.');
		$this->session->set_flashdata('type_message','success');
		redirect('Daftar/Import/Penyesuaian/PenyesuaianExtra');
	}

	function ImportPddikti(){
		$tahun = $this->input->post('tahun');
		$rules = array(
			'select'    => null,
			'where'     => array(
				'YEAR(date_created)' => $tahun,
			),
			'or_where'  => null,
			'order'     => null,
			'limit'     => null,
			'pagging'   => null,
		);
		$tblPddikti = $this->Tbl_daftar_pddikti->where($rules)->result();

		foreach($tblPddikti as $value){
			if($value->nim == 0){
			}else if($value->nim == ""){
			}else{
				$rules2 = array(
					'select'    => null,
					'where'     => array(
						'nik_passport' => $value->nik_passport,
					),
					'or_where'  => null,
					'order'     => null,
					'limit'     => null,
					'pagging'   => null,
				);
				$data5 = array(
					'nik_passport' 	=> $value->nik_passport,
	                'nim'			=> $value->nim,
	                'nama'			=> $value->nama,
	                'tmp_lhr'       => $value->tmp_lhr,
	                'tgl_lhr'       => $value->tgl_lhr,
	                'jenis_kelamin' => $value->jenis_kelamin,
	                'nisn'          => $value->nisn,
	                'npwp'          => $value->npwp,
	                'warga_negara'  => $value->warga_negara,
	                'id_agama'		=> $value->id_agama,
	                'id_jlr_msk'	=> $value->id_jlr_msk,
	                'id_jns_pndftrn'=> $value->id_jns_pndftrn,
	                'jalan'         => $value->jalan,
	                'rt'            => $value->rt,
	                'rw'            => $value->rw,
	                'nama_dusun'    => $value->nama_dusun,
	                'kode_pos'      => $value->kode_pos,
	                'id_jns_tinggal'=> $value->id_jns_tinggal,
	                'id_rumpun'=> $value->id_rumpun,
	                'id_alat_transportasi'=> $value->id_alat_transportasi,
	                'tlp_rmh'       => $value->tlp_rmh,
	                'no_hp'         => $value->no_hp,
	                'email'         => $value->email,
	                'terima_kps'    => $value->terima_kps,
	                'no_kps'        => $value->no_kps,
	                'nik_ayah'      => $value->nik_ayah,
	                'nama_ayah'     => $value->nama_ayah,
	                'id_pendidikan_ayah' => $value->id_pendidikan_ayah,
	                'id_pekerjaan_ayah' => $value->id_pekerjaan_ayah,
	                'id_penghasilan_ayah' => $value->id_penghasilan_ayah,
	                'nik_ibu'      => $value->nik_ibu,
	                'nama_ibu'     => $value->nama_ibu,
	                'id_pendidikan_ibu' => $value->id_pendidikan_ibu,
	                'id_pekerjaan_ibu' => $value->id_pekerjaan_ibu,
	                'id_penghasilan_ibu' => $value->id_penghasilan_ibu,
	                'nik_wali'      => $value->nik_wali,
	                'nama_wali'     => $value->nama_wali,
	                'id_pendidikan_wali' => $value->id_pendidikan_wali,
	                'id_pekerjaan_wali' => $value->id_pekerjaan_wali,
	                'id_penghasilan_wali' => $value->id_penghasilan_wali,
	                'id_kelurahan'	=> $value->id_kelurahan,
	                'tgl_lhr_ayah' => $value->tgl_lhr_ayah,
	                'tgl_lhr_ibu' => $value->tgl_lhr_ibu,
	                'tgl_lhr_wali' => $value->tgl_lhr_wali,
	                'tgl_msk_kuliah' => $value->tgl_msk_kuliah,
	                'mulai_semester' => $value->mulai_semester,
	                'date_created'	=> $value->date_created,
	                'date_updated'	=> $value->date_updated,
				);
				$this->Tbl_penyesuaian_pddikti->create($data5);
			}
		}
		$this->session->set_flashdata('message','Import berhasil.');
		$this->session->set_flashdata('type_message','success');
		redirect('Daftar/Import/Penyesuaian/PenyesuaianPddikti');
	}

	function ImportGambar(){
		$tahun = $this->input->post('tahun');
		$rules = array(
			'select'    => null,
			'where'     => array(
				'YEAR(date_created)' => $tahun,
			),
			'or_where'  => null,
			'order'     => null,
			'limit'     => null,
			'pagging'   => null,
		);
		$tblPddikti = $this->Tbl_daftar_pddikti->where($rules)->result();

		foreach($tblPddikti as $value){
			if($value->nim == 0){
			}else if($value->nim == ""){
			}else{
				$rules2 = array(
					'select'    => null,
					'where'     => array(
						'nik_passport' => $value->nik_passport,
					),
					'or_where'  => null,
					'order'     => null,
					'limit'     => null,
					'pagging'   => null,
				);

				$tblGambar		= $this->Tbl_daftar_gambar->where($rules2)->result();

				foreach ($tblGambar as $value) {
	                $data4 = array(
	                    'nik_passport'      => $value->nik_passport,
	                    'id_tipe_gambar'    => $value->id_tipe_gambar,
	                    'gambar'            => $value->gambar,
	                    'date_created'       => $value->date_created,
	                    'date_updated'       => $value->date_updated,
	                );
	                $this->Tbl_penyesuaian_gambar->create($data4);
	            }
			}
		}
		$this->session->set_flashdata('message','Import berhasil.');
		$this->session->set_flashdata('type_message','success');
		redirect('Daftar/Import/Penyesuaian/PenyesuaianGambar');
	}
}