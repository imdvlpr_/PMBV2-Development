<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Kelulusan extends CI_Controller {

	function __construct(){
		parent::__construct();
		if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
		if ($this->session->userdata('level') != 'DEVELOPMENT') {
			$this->session->set_flashdata('message','Hak akses ditolak.');
			$this->session->set_flashdata('type_message','danger');
			redirect('Auth');
		}
		$this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
		$this->load->model('Daftar/Tbl_daftar_kelulusan');
		$this->load->model('Daftar/Tbl_daftar_users');
		$this->load->model('Daftar/Tbl_daftar_mahasiswa');
		$this->load->model('Daftar/Tbl_daftar_extra');
		$this->load->model('Pradaftar/Tbl_pradaftar_kelulusan');
		$this->load->model('Pradaftar/Tbl_pradaftar_biodata');
		$this->load->model('Pradaftar/Tbl_pradaftar_users');
		$this->load->model('Settings/Tbl_setting_jalur_masuk');
	}

	function index(){
		$rules = array(
			'select'    => null,
			'order'     => null,
			'limit'     => null,
			'pagging'   => null,
		);
		$tbSJalurMasuk = $this->Tbl_setting_jalur_masuk->read($rules)->result();
		$data = array(
			'content'       => 'Daftar/Import/kelulusan/content',
            'css'           => 'Daftar/Import/kelulusan/css',
            'javascript'    => 'Daftar/Import/kelulusan/javascript',
            'modal'         => 'Daftar/Import/kelulusan/modal',
			'tbSJalurMasuk' => $tbSJalurMasuk,
		);
    	$this->load->view('index', $data);
	}

	function Import(){
		$rules[] = array('field' => 'id_jlr_msk', 'label' => 'Jalur Masuk', 'rules' => 'required');
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('message',validation_errors());
			$this->session->set_flashdata('type_message','danger');
			redirect('Daftar/Import/Kelulusan/');
		}else{
			$config = array(
				'upload_path'   => './import/daftar/',
				'allowed_types' => 'xls|xlsx|csv|ods|ots',
				'max_size'      => 51200,
				'overwrite'     => TRUE,
				'file_name'     => 'Kelulusan_JM00'.$this->input->post('id_jlr_msk').'_'.date('Y').'_'.date('H i s d m Y'),
			);
			$this->upload->initialize($config);
			if(!$this->upload->do_upload()){
				$this->session->set_flashdata('message',$this->upload->display_errors());
				$this->session->set_flashdata('type_message','danger');
				redirect('Daftar/Import/Kelulusan/');
			}else{
				$file = $this->upload->data();
				$inputFileName = 'import/daftar/'.$file['file_name'];
				try {
					$inputFileType	= IOFactory::identify($inputFileName);
					$objReader		= IOFactory::createReader($inputFileType);
					$objPHPExcel	= $objReader->load($inputFileName);
				} catch (Exception $e) {
					die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'" : '.$e->getMessage());
				}
				$sheet	= $objPHPExcel->getSheet(0);
				$highestRow	= $sheet->getHighestRow();
				for ($row = 2; $row <= $highestRow; $row++) {
					$nomor_peserta	= $sheet->getCellByColumnAndRow(0,$row)->getValue();
					$nama			= str_replace('\'','`',strtoupper($sheet->getCellByColumnAndRow(1,$row)->getValue()));
					$kode_jurusan	= $sheet->getCellByColumnAndRow(2,$row)->getValue();
					$rules = array(
						'select'    => null,
						'where'     => array(
							'nomor_peserta' => $nomor_peserta
						),
						'or_where'  => null,
						'order'     => null,
						'limit'     => null,
						'pagging'   => null,
					);
					$num_rows		= $this->Tbl_daftar_kelulusan->where($rules)->num_rows();
					if ($num_rows == 0) {
						$data = array(
							'nomor_peserta' => $nomor_peserta,
							'nama'			=> $nama,
							'kode_jurusan'  => $kode_jurusan,
							'id_jlr_msk'	=> $this->input->post('id_jlr_msk'),
							'status'		=> 'BELUM DAFTAR',
							'tahun'			=> date('Y'),
							'created_by'		=> $this->session->userdata('id_users'),
							'updated_by'		=> $this->session->userdata('id_users'),
						);
						$this->Tbl_daftar_kelulusan->create($data);
					}else{
						$rules2 = array(
							'where' => array(
								'nomor_peserta' => $nomor_peserta
							),
							'data'  => array(
								'nama'			=> $nama,
								'kode_jurusan'  => $kode_jurusan,
								'id_jlr_msk'	=> $this->input->post('id_jlr_msk'),
								'status'		=> 'BELUM DAFTAR',
								'tahun'			=> date('Y'),
								'created_by'		=> $this->session->userdata('id_users'),
								'updated_by'		=> $this->session->userdata('id_users'),
							),
						);
                        $this->Tbl_daftar_kelulusan->update($rules2);
                    }
				}
				$this->session->set_flashdata('message','Import berhasil.');
				$this->session->set_flashdata('type_message','success');
				redirect('Daftar/Import/Kelulusan/');
			}
		}
	}

	function ImportPradaftar(){
		$rules = array(
            'select'    => null,
            'where'     => array(
                'status_kelulusan' => 'LULUS',
            ),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
		$viewKelulusan = $this->Tbl_pradaftar_kelulusan->where($rules)->result();

		foreach($viewKelulusan as $value){
			$rules2 = array(
				'select'    => null,
				'where'     => array(
					'nomor_peserta' => $value->nomor_peserta,
				),
				'or_where'  => null,
				'order'     => null,
				'limit'     => null,
				'pagging'   => null,
			);
			$viewUsers = $this->Tbl_pradaftar_users->where($rules2)->row();
			$num_rows_kelulusan		= $this->Tbl_daftar_kelulusan->where($rules2)->num_rows();
			$num_rows_users		= $this->Tbl_daftar_users->where($rules2)->num_rows();
			$rules3 = array(
				'select'    => null,
				'where'     => array(
					'nik_passport'	=> $viewUsers->nik_passport,
				),
				'or_where'  => null,
				'order'     => null,
				'limit'     => null,
				'pagging'   => null,
			);
			$viewBiodata = $this->Tbl_pradaftar_biodata->where($rules3)->row();
			$num_rows_pddikti		= $this->Tbl_daftar_pddikti->where($rules3)->num_rows();

			if($num_rows_kelulusan == 0){
				$data = array(
					'nomor_peserta' => $value->nomor_peserta,
					'kode_jurusan'	=> $value->kode_jurusan_kelulusan,
					'nama'			=> $viewUsers->nama,
					'id_jlr_msk'	=> 5,
					'status'		=> 'BELUM DAFTAR',
					'tahun'			=> date('Y'),
					'created_by'		=> $this->session->userdata('id_users'),
					'updated_by'		=> $this->session->userdata('id_users'),
				);
				$this->Tbl_daftar_kelulusan->create($data);
			}

			if($num_rows_users == 0){
				$data4 = array(
					'nik_passport'      => $viewBiodata->nik_passport,
					'nomor_peserta' 	=> $value->nomor_peserta,
					'username'			=> $viewUsers->email,
					'password'			=> $viewUsers->password,
					'id_jlr_msk'		=> 5,
					'nmr_hp'			=> $viewUsers->nmr_tlp,
					'tgl_lhr'			=> $viewUsers->tgl_lhr,
					'verifikasi'		=> "BELUM VERIFIKASI",
					'cetak'				=> 0,
				);
				$this->Tbl_daftar_users->create($data4);
			}

			if($num_rows_users == 0){
				$data2 = array(
					'nik_passport'      => $viewBiodata->nik_passport,
					'nama'              => $viewBiodata->nama,
	                'jenis_kelamin' 	=> $viewBiodata->jenis_kelamin,
	                'id_jlr_msk'	=> 5,
	                'id_jns_pndftrn'=> 1,
                	'id_jns_tinggal'=> 1,
                	'id_alat_transportasi'=> 1,
	                'id_agama'			=> $viewBiodata->id_agama,
	                'tgl_lhr'           => $viewBiodata->tgl_lhr,
	                'tmp_lhr'           => $viewBiodata->tempat,
	                'warga_negara'      => $viewBiodata->warga_negara,
	                'kode_pos'          => $viewBiodata->kodepos,
	                'nama_ayah'			=> $viewBiodata->nama_ayah,
	                'nama_ibu'			=> $viewBiodata->nama_ibu,
	                'id_rumpun'          => $viewBiodata->id_rumpun,
	                'id_pendidikan_ayah'    => $viewBiodata->id_pendidikan_ayah,
	                'id_pekerjaan_ayah'     => $viewBiodata->id_pekerjaan_ayah,
	                'id_penghasilan_ayah'   => $viewBiodata->id_penghasilan_ayah,
	                'id_pendidikan_ibu'    => $viewBiodata->id_pendidikan_ibu,
	                'id_pekerjaan_ibu'     => $viewBiodata->id_pekerjaan_ibu,
	                'id_penghasilan_ibu'   => $viewBiodata->id_penghasilan_ibu,
	                'id_pendidikan_wali' => 1,
	                'id_pekerjaan_wali' => 6,
	                'id_penghasilan_wali' => 7,
	                'id_kelurahan'	=> 1101012001,
	                'tgl_lhr_ayah' => '1900-01-01',
	                'tgl_lhr_ibu' => '1900-01-01',
	                'tgl_lhr_wali' => '1900-01-01',
	                'tgl_msk_kuliah' => date('Y').'-09-01',
	                'mulai_semester' => 'SEMESTER 1',
				);
				$this->Tbl_daftar_pddikti->create($data2);
			}
		}
		$this->session->set_flashdata('message','Import berhasil.');
		$this->session->set_flashdata('type_message','success');
	}
}