<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Kelulusan extends CI_Controller {
	
    function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != 'DEVELOPMENT') {
            $this->session->set_flashdata('message','Hak akses ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }

        $this->load->model('Views/Daftar/View_daftar_kelulusan');

    }
	
    function index(){
        $tahun 		= $this->input->post('tahun');
		if (empty($tahun)){
		    $kosong = true;
		}else{
            $kosong = false;
            $rules = array(
                'select'    => null,
                'where'     => array(
                    'YEAR(date_created)' => $this->input->post('tahun'),
                ),
                'or_where'  => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $rules2 = array(
                'select'    => null,
                'where'     => array(
                    'status_pendaftaran' => 'SUDAH DAFTAR',
                    'YEAR(date_created)' => $this->input->post('tahun'),
                ),
                'or_where'  => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $rules3 = array(
                'select'    => null,
                'where'     => array(
                    'status_pendaftaran' => 'BELUM DAFTAR',
                    'YEAR(date_created)' => $this->input->post('tahun'),
                ),
                'or_where'  => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $rules4 = array(
                'select'    => null,
                'where'     => array(
                    'id_jlr_msk' => 1,
                    'YEAR(date_created)' => $this->input->post('tahun'),
                ),
                'or_where'  => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $rules5 = array(
                'select'    => null,
                'where'     => array(
                    'status_pendaftaran' => 'SUDAH DAFTAR',
                    'YEAR(date_created)' => $this->input->post('tahun'),
                    'id_jlr_msk' => 1
                ),
                'or_where'  => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $rules6 = array(
                'select'    => null,
                'where'     => array(
                    'status_pendaftaran' => 'BELUM DAFTAR',
                    'YEAR(date_created)' => $this->input->post('tahun'),
                    'id_jlr_msk' => 1
                ),
                'or_where'  => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $rules7 = array(
                'select'    => null,
                'where'     => array(
                    'id_jlr_msk' => 2,
                    'YEAR(date_created)' => $this->input->post('tahun'),
                ),
                'or_where'  => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $rules8 = array(
                'select'    => null,
                'where'     => array(
                    'status_pendaftaran' => 'SUDAH DAFTAR',
                    'YEAR(date_created)' => $this->input->post('tahun'),
                    'id_jlr_msk' => 2
                ),
                'or_where'  => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $rules9 = array(
                'select'    => null,
                'where'     => array(
                    'status_pendaftaran' => 'BELUM DAFTAR',
                    'YEAR(date_created)' => $this->input->post('tahun'),
                    'id_jlr_msk' => 2
                ),
                'or_where'  => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $rules10 = array(
                'select'    => null,
                'where'     => array(
                    'id_jlr_msk' => 3,
                    'YEAR(date_created)' => $this->input->post('tahun'),
                ),
                'or_where'  => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $rules11 = array(
                'select'    => null,
                'where'     => array(
                    'status_pendaftaran' => 'SUDAH DAFTAR',
                    'YEAR(date_created)' => $this->input->post('tahun'),
                    'id_jlr_msk' => 3
                ),
                'or_where'  => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $rules12 = array(
                'select'    => null,
                'where'     => array(
                    'status_pendaftaran' => 'BELUM DAFTAR',
                    'YEAR(date_created)' => $this->input->post('tahun'),
                    'id_jlr_msk' => 3
                ),
                'or_where'  => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $rules13 = array(
                'select'    => null,
                'where'     => array(
                    'id_jlr_msk' => 4,
                    'YEAR(date_created)' => $this->input->post('tahun'),
                ),
                'or_where'  => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $rules14 = array(
                'select'    => null,
                'where'     => array(
                    'status_pendaftaran' => 'SUDAH DAFTAR',
                    'YEAR(date_created)' => $this->input->post('tahun'),
                    'id_jlr_msk' => 4
                ),
                'or_where'  => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $rules15 = array(
                'select'    => null,
                'where'     => array(
                    'status_pendaftaran' => 'BELUM DAFTAR',
                    'YEAR(date_created)' => $this->input->post('tahun'),
                    'id_jlr_msk' => 4
                ),
                'or_where'  => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $rules16 = array(
                'select'    => null,
                'where'     => array(
                    'id_jlr_msk' => 5,
                    'YEAR(date_created)' => $this->input->post('tahun'),
                ),
                'or_where'  => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $rules17 = array(
                'select'    => null,
                'where'     => array(
                    'status_pendaftaran' => 'SUDAH DAFTAR',
                    'YEAR(date_created)' => $this->input->post('tahun'),
                    'id_jlr_msk' => 5
                ),
                'or_where'  => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $rules18 = array(
                'select'    => null,
                'where'     => array(
                    'status_pendaftaran' => 'BELUM DAFTAR',
                    'YEAR(date_created)' => $this->input->post('tahun'),
                    'id_jlr_msk' => 5
                ),
                'or_where'  => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $num_kelulusan 		= $this->View_daftar_kelulusan->where($rules)->num_rows();
            $num_kelulusan_sd 		= $this->View_daftar_kelulusan->where($rules2)->num_rows();
            $num_kelulusan_bd 		= $this->View_daftar_kelulusan->where($rules3)->num_rows();

            $num_kelulusan_snmptn         = $this->View_daftar_kelulusan->where($rules4)->num_rows();
            $num_kelulusan_sd_snmptn      = $this->View_daftar_kelulusan->where($rules5)->num_rows();
            $num_kelulusan_bd_snmptn      = $this->View_daftar_kelulusan->where($rules6)->num_rows();

            $num_kelulusan_span         = $this->View_daftar_kelulusan->where($rules7)->num_rows();
            $num_kelulusan_sd_span      = $this->View_daftar_kelulusan->where($rules8)->num_rows();
            $num_kelulusan_bd_span      = $this->View_daftar_kelulusan->where($rules9)->num_rows();

            $num_kelulusan_sbmptn         = $this->View_daftar_kelulusan->where($rules10)->num_rows();
            $num_kelulusan_sd_sbmptn      = $this->View_daftar_kelulusan->where($rules11)->num_rows();
            $num_kelulusan_bd_sbmptn      = $this->View_daftar_kelulusan->where($rules12)->num_rows();

            $num_kelulusan_umptkin         = $this->View_daftar_kelulusan->where($rules13)->num_rows();
            $num_kelulusan_sd_umptkin      = $this->View_daftar_kelulusan->where($rules14)->num_rows();
            $num_kelulusan_bd_umptkin      = $this->View_daftar_kelulusan->where($rules15)->num_rows();

            $num_kelulusan_mandiri         = $this->View_daftar_kelulusan->where($rules16)->num_rows();
            $num_kelulusan_sd_mandiri      = $this->View_daftar_kelulusan->where($rules17)->num_rows();
            $num_kelulusan_bd_mandiri      = $this->View_daftar_kelulusan->where($rules18)->num_rows();
        }
        
        $rules19 = array(
            'select'    => 'YEAR(date_created) as tahun',
            'where'     => null,
            'order'     => null,
        );

        
    	$data = array(
    		'num_kelulusan' 		=> (!empty($num_kelulusan))? $num_kelulusan : '',
    		'num_kelulusan_sd' 		=> (!empty($num_kelulusan_sd))? $num_kelulusan_sd : '',
    		'num_kelulusan_bd' 		=> (!empty($num_kelulusan_bd))? $num_kelulusan_bd : '',

            'num_kelulusan_snmptn'         => (!empty($num_kelulusan_snmptn))? $num_kelulusan_snmptn : '',
            'num_kelulusan_sd_snmptn'      => (!empty($num_kelulusan_sd_snmptn))? $num_kelulusan_sd_snmptn : '',
            'num_kelulusan_bd_snmptn'      => (!empty($num_kelulusan_bd_snmptn))? $num_kelulusan_bd_snmptn : '',

            'num_kelulusan_span'         => (!empty($num_kelulusan_span))? $num_kelulusan_span : '',
            'num_kelulusan_sd_span'      => (!empty($num_kelulusan_sd_span))? $num_kelulusan_sd_span : '',
            'num_kelulusan_bd_span'      => (!empty($num_kelulusan_bd_span))? $num_kelulusan_bd_span : '',

            'num_kelulusan_sbmptn'         => (!empty($num_kelulusan_sbmptn))? $num_kelulusan_sbmptn : '',
            'num_kelulusan_sd_sbmptn'      => (!empty($num_kelulusan_sd_sbmptn))? $num_kelulusan_sd_sbmptn : '',
            'num_kelulusan_bd_sbmptn'      => (!empty($num_kelulusan_bd_sbmptn))? $num_kelulusan_bd_sbmptn : '',

            'num_kelulusan_umptkin'         => (!empty($num_kelulusan_umptkin))? $num_kelulusan_umptkin : '',
            'num_kelulusan_sd_umptkin'      => (!empty($num_kelulusan_sd_umptkin))? $num_kelulusan_sd_umptkin : '',
            'num_kelulusan_bd_umptkin'      => (!empty($num_kelulusan_bd_umptkin))? $num_kelulusan_bd_umptkin : '',

            'num_kelulusan_mandiri'         => (!empty($num_kelulusan_mandiri))? $num_kelulusan_mandiri : '',
            'num_kelulusan_sd_mandiri'      => (!empty($num_kelulusan_sd_mandiri))? $num_kelulusan_sd_mandiri : '',
            'num_kelulusan_bd_mandiri'      => (!empty($num_kelulusan_bd_mandiri))? $num_kelulusan_bd_mandiri : '',

            'content'       => 'Daftar/Statistik/kelulusan/content',
            'css'           => 'Daftar/Statistik/kelulusan/css',
            'javascript'    => 'Daftar/Statistik/kelulusan/javascript',
            'modal'         => 'Daftar/Statistik/kelulusan/modal',
            'kosong'        => $kosong,
            'tahun_pilih'   => (!empty($this->input->post('tahun')))? $this->input->post('tahun') : '',
            'tahun'	=> $this->View_daftar_kelulusan->distinct($rules19)->result(),
		);
    	$this->load->view('index', $data);
    }

}
