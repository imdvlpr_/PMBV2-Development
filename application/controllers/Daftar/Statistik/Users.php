<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Users extends CI_Controller {
	
    function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != 'DEVELOPMENT') {
            $this->session->set_flashdata('message','Hak akses ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }

        $this->load->model('Daftar/Tbl_daftar_users');
        $this->load->model('Views/Daftar/View_daftar_users');
        //$this->load->model('Daftar/Tbl_daftar_kelulusan');

    }

    function index(){
        $tahun 		= $this->input->post('tahun');
		if (empty($tahun)){
		    $kosong = true;
		}else{
            $kosong = false;
            $rules = array(
                'select'    => null,
                'where'     => array(
                    'YEAR(date_created)' => $this->input->post('tahun'),
                ),
                'or_where'  => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $rules2 = array(
                'select'    => null,
                'where'     => array(
                    'verifikasi' => 'SUDAH VERIFIKASI',
                    'YEAR(date_created)' => $this->input->post('tahun'),
                ),
                'or_where'  => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $rules3 = array(
                'select'    => null,
                'where'     => array(
                    'verifikasi' => 'BELUM VERIFIKASI',
                    'YEAR(date_created)' => $this->input->post('tahun')
                ),
                'or_where'  => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $rules4 = array(
                'select'    => null,
                'where'     => array(
                    'id_jlr_msk' => 1,
                    'YEAR(date_created)' => $this->input->post('tahun')
                ),
                'or_where'  => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $rules5 = array(
                'select'    => null,
                'where'     => array(
                    'verifikasi' => 'SUDAH VERIFIKASI',
                    'YEAR(date_created)' => $this->input->post('tahun'),
                    'id_jlr_msk' => 1
                ),
                'or_where'  => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $rules6 = array(
                'select'    => null,
                'where'     => array(
                    'verifikasi' => 'BELUM VERIFIKASI',
                    'YEAR(date_created)' => $this->input->post('tahun'),
                    'id_jlr_msk' => 1
                ),
                'or_where'  => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $rules7 = array(
                'select'    => null,
                'where'     => array(
                    'id_jlr_msk' => 2,
                    'YEAR(date_created)' => $this->input->post('tahun'),
                ),
                'or_where'  => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $rules8 = array(
                'select'    => null,
                'where'     => array(
                    'verifikasi' => 'SUDAH VERIFIKASI',
                    'YEAR(date_created)' => $this->input->post('tahun'),
                    'id_jlr_msk' => 2
                ),
                'or_where'  => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $rules9 = array(
                'select'    => null,
                'where'     => array(
                    'verifikasi' => 'BELUM VERIFIKASI',
                    'YEAR(date_created)' => $this->input->post('tahun'),
                    'id_jlr_msk' => 2
                ),
                'or_where'  => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $rules10 = array(
                'select'    => null,
                'where'     => array(
                    'id_jlr_msk' => 3,
                    'YEAR(date_created)' => $this->input->post('tahun'),
                ),
                'or_where'  => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $rules11 = array(
                'select'    => null,
                'where'     => array(
                    'verifikasi' => 'SUDAH VERIFIKASI',
                    'YEAR(date_created)' => $this->input->post('tahun'),
                    'id_jlr_msk' => 3
                ),
                'or_where'  => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $rules12 = array(
                'select'    => null,
                'where'     => array(
                    'verifikasi' => 'BELUM VERIFIKASI',
                    'YEAR(date_created)' => $this->input->post('tahun'),
                    'id_jlr_msk' => 3
                ),
                'or_where'  => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $rules13 = array(
                'select'    => null,
                'where'     => array(
                    'id_jlr_msk' => 4,
                    'YEAR(date_created)' => $this->input->post('tahun'),
                ),
                'or_where'  => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $rules14 = array(
                'select'    => null,
                'where'     => array(
                    'verifikasi' => 'SUDAH VERIFIKASI',
                    'YEAR(date_created)' => $this->input->post('tahun'),
                    'id_jlr_msk' => 4
                ),
                'or_where'  => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $rules15 = array(
                'select'    => null,
                'where'     => array(
                    'verifikasi' => 'BELUM VERIFIKASI',
                    'YEAR(date_created)' => $this->input->post('tahun'),
                    'id_jlr_msk' => 4
                ),
                'or_where'  => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $rules16 = array(
                'select'    => null,
                'where'     => array(
                    'id_jlr_msk' => 5,
                    'YEAR(date_created)' => $this->input->post('tahun'),
                ),
                'or_where'  => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $rules17 = array(
                'select'    => null,
                'where'     => array(
                    'verifikasi' => 'SUDAH VERIFIKASI',
                    'YEAR(date_created)' => $this->input->post('tahun'),
                    'id_jlr_msk' => 5
                ),
                'or_where'  => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            $rules18 = array(
                'select'    => null,
                'where'     => array(
                    'verifikasi' => 'BELUM VERIFIKASI',
                    'YEAR(date_created)' => $this->input->post('tahun'),
                    'id_jlr_msk' => 5
                ),
                'or_where'  => null,
                'order'     => null,
                'limit'     => null,
                'pagging'   => null,
            );
            
            $num_users 		= $this->View_daftar_users->where($rules)->num_rows();
            $num_users_sv 		= $this->View_daftar_users->where($rules2)->num_rows();
            $num_users_bv 		= $this->View_daftar_users->where($rules3)->num_rows();

            $num_users_snmptn         = $this->View_daftar_users->where($rules4)->num_rows();
            $num_users_sv_snmptn      = $this->View_daftar_users->where($rules5)->num_rows();
            $num_users_bv_snmptn      = $this->View_daftar_users->where($rules6)->num_rows();

            $num_users_span         = $this->View_daftar_users->where($rules7)->num_rows();
            $num_users_sv_span      = $this->View_daftar_users->where($rules8)->num_rows();
            $num_users_bv_span      = $this->View_daftar_users->where($rules9)->num_rows();

            $num_users_sbmptn         = $this->View_daftar_users->where($rules10)->num_rows();
            $num_users_sv_sbmptn      = $this->View_daftar_users->where($rules11)->num_rows();
            $num_users_bv_sbmptn      = $this->View_daftar_users->where($rules12)->num_rows();

            $num_users_umptkin         = $this->View_daftar_users->where($rules13)->num_rows();
            $num_users_sv_umptkin      = $this->View_daftar_users->where($rules14)->num_rows();
            $num_users_bv_umptkin      = $this->View_daftar_users->where($rules15)->num_rows();

            $num_users_mandiri         = $this->View_daftar_users->where($rules16)->num_rows();
            $num_users_sv_mandiri      = $this->View_daftar_users->where($rules17)->num_rows();
            $num_users_bv_mandiri      = $this->View_daftar_users->where($rules18)->num_rows();
        }
        $rules19 = array(
            'select'    => 'YEAR(date_created) as tahun',
            'where'     => null,
            'order'     => null,
        );

        $data = array(
            'num_users' 		=> (!empty($num_users))? $num_users : '',
    		'num_users_sv' 		=> (!empty($num_users_sv))? $num_users_sv : '',
    		'num_users_bv' 		=> (!empty($num_users_bv))? $num_users_bv : '',

            'num_users_snmptn'         => (!empty($num_users_snmptn))? $num_users_snmptn : '',
            'num_users_sv_snmptn'      => (!empty($num_users_sv_snmptn))? $num_users_sv_snmptn : '',
            'num_users_bv_snmptn'      => (!empty($num_users_bv_snmptn))? $num_users_bv_snmptn : '',

            'num_users_span'         => (!empty($num_users_span))? $num_users_span : '',
            'num_users_sv_span'      => (!empty($num_users_sv_span))? $num_users_sv_span : '',
            'num_users_bv_span'      => (!empty($num_users_bv_span))? $num_users_bv_span : '',

            'num_users_sbmptn'         => (!empty($num_users_sbmptn))? $num_users_sbmptn : '',
            'num_users_sv_sbmptn'      => (!empty($num_users_sv_sbmptn))? $num_users_sv_sbmptn : '',
            'num_users_bv_sbmptn'      => (!empty($num_users_bv_sbmptn))? $num_users_bv_sbmptn : '',

            'num_users_umptkin'         => (!empty($num_users_umptkin))? $num_users_umptkin : '',
            'num_users_sv_umptkin'      => (!empty($num_users_sv_umptkin))? $num_users_sv_umptkin : '',
            'num_users_bv_umptkin'      => (!empty($num_users_bv_umptkin))? $num_users_bv_umptkin : '',

            'num_users_mandiri'         => (!empty($num_users_mandiri))? $num_users_mandiri : '',
            'num_users_sv_mandiri'      => (!empty($num_users_sv_mandiri))? $num_users_sv_mandiri : '',
            'num_users_bv_mandiri'      => (!empty($num_users_bv_mandiri))? $num_users_bv_mandiri : '',

    		'content'       => 'Daftar/Statistik/users/content',
            'css'           => 'Daftar/Statistik/users/css',
            'javascript'    => 'Daftar/Statistik/users/javascript',
            'modal'         => 'Daftar/Statistik/users/modal',
            'kosong'        => $kosong,
            'tahun_pilih'   => (!empty($this->input->post('tahun')))? $this->input->post('tahun') : '',
            'tahun'	=> $this->View_daftar_users->distinct($rules19)->result(),
		);
    	$this->load->view('index', $data);
    }

}