<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Provinsi extends CI_Controller {

	function __construct(){
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
		if ($this->session->userdata('level') != 'DEVELOPMENT') {
			$this->session->set_flashdata('message','Hak akses ditolak.');
			$this->session->set_flashdata('type_message','danger');
			redirect('Auth');
		}

		$this->load->model('Views/Daftar/View_daftar_pddikti_mahasiswa');
		$this->load->model('Settings/Tbl_setting_provinsi');
	}

	function index(){
		$year = '2018';
		$rules = array(
            'select'    => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
		$tbSProvinsi = $this->Tbl_setting_provinsi->read($rules)->result();
		foreach ($tbSProvinsi as $value){
			$rules2 = array(
				'select'    => null,
				'where'     => array(
					'provinsi' => $value->provinsi,
					'YEAR(date_created)' => $year
				),
				'or_where'  => null,
				'order'     => null,
				'limit'     => null,
				'pagging'   => null,
			);
			$provinsi[$value->id_provinsi] = $this->View_daftar_pddikti_mahasiswa->where($rules2)->num_rows();
		}
		$data = array(
			'content'       => 'Daftar/Statistik/provinsi/content',
            'css'           => 'Daftar/Statistik/provinsi/css',
            'javascript'    => 'Daftar/Statistik/provinsi/javascript',
            'modal'         => 'Daftar/Statistik/provinsi/modal',
			'tbSProvinsi'	=> $tbSProvinsi,
			'provinsi'		=> $provinsi,
		);
		$this->load->view('index',$data);
	}

}
