<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class JurusanFakultas extends CI_Controller {
    
    function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != 'DEVELOPMENT') {
            $this->session->set_flashdata('message','Hak akses ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }

        $this->load->model('Views/Daftar/View_daftar_kelulusan');
        $this->load->model('Views/Daftar/View_daftar_users');
        $this->load->model('Views/Settings/View_setting_fakultas_jurusan');
    }
    
    function index(){
        $tahun 		= $this->input->post('tahun');
        $rules = array(
            'select'    => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $viewSJurusan = $this->View_setting_fakultas_jurusan->read($rules)->result();
		if (empty($tahun)){
		    $kosong = true;
		}else{
            $kosong = false;
            foreach ($viewSJurusan as $value){
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        "kode_jurusan" => $value->kode_jurusan,
                        "id_jlr_msk" => 1,
                        "verifikasi" => "SUDAH VERIFIKASI",
                        'YEAR(date_created)' => $this->input->post('tahun'),
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $snmptn[$value->kode_jurusan] = $this->View_daftar_users->where($rules)->num_rows();
            }
            foreach ($viewSJurusan as $value){
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        "kode_jurusan" => $value->kode_jurusan,
                        "id_jlr_msk" => 2,
                        "verifikasi" => "SUDAH VERIFIKASI",
                        'YEAR(date_created)' => $this->input->post('tahun'),
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $span[$value->kode_jurusan] = $this->View_daftar_users->where($rules)->num_rows();
            }
            foreach ($viewSJurusan as $value){
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        "kode_jurusan" => $value->kode_jurusan,
                        "id_jlr_msk" => 3,
                        "verifikasi" => "SUDAH VERIFIKASI",
                        'YEAR(date_created)' => $this->input->post('tahun'),
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $sbmptn[$value->kode_jurusan] = $this->View_daftar_users->where($rules)->num_rows();
            }
            foreach ($viewSJurusan as $value){
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        "kode_jurusan" => $value->kode_jurusan,
                        "id_jlr_msk" => 4,
                        "verifikasi" => "SUDAH VERIFIKASI",
                        'YEAR(date_created)' => $this->input->post('tahun'),
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $umptkin[$value->kode_jurusan] = $this->View_daftar_users->where($rules)->num_rows();
            }
            foreach ($viewSJurusan as $value){
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        "kode_jurusan" => $value->kode_jurusan,
                        "id_jlr_msk" => 5,
                        "verifikasi" => "SUDAH VERIFIKASI",
                        'YEAR(date_created)' => $this->input->post('tahun'),
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $mandiri[$value->kode_jurusan] = $this->View_daftar_users->where($rules)->num_rows();
            }
            foreach ($viewSJurusan as $value){
                $jumlah[$value->kode_jurusan] = $snmptn[$value->kode_jurusan] + $span[$value->kode_jurusan] + $sbmptn[$value->kode_jurusan] + $umptkin[$value->kode_jurusan] + $mandiri[$value->kode_jurusan];
            }
        }
        $rules19 = array(
            'select'    => 'YEAR(date_created) as tahun',
            'where'     => null,
            'order'     => null,
        );
        
        $data = array(
            'content'       => 'Daftar/Statistik/jurusan_fakultas/content',
            'css'           => 'Daftar/Statistik/jurusan_fakultas/css',
            'javascript'    => 'Daftar/Statistik/jurusan_fakultas/javascript',
            'modal'         => 'Daftar/Statistik/jurusan_fakultas/modal',
            'viewSJurusan'  => $viewSJurusan,
            'snmptn'        => (!empty($snmptn))? $snmptn : '',
            'span'      => (!empty($span))? $span : '',
            'sbmptn'        => (!empty($sbmptn))? $sbmptn : '',
            'umptkin'        => (!empty($umptkin))? $umptkin : '',
            'mandiri'        => (!empty($mandiri))? $mandiri : '',
            'jumlah'        => (!empty($jumlah))? $jumlah : '',
            'kosong'        => $kosong,
            'tahun_pilih'   => (!empty($this->input->post('tahun')))? $this->input->post('tahun') : '',
            'tahun'	=> $this->View_daftar_kelulusan->distinct($rules19)->result(),
        );
        $this->load->view('index',$data);
    }

}
