<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Data extends CI_Controller {

	function __construct(){
		parent::__construct();
		if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
		if ($this->session->userdata('level') != 'DEVELOPMENT') {
			$this->session->set_flashdata('message','Hak akses ditolak.');
			$this->session->set_flashdata('type_message','danger');
			redirect('Auth');
		}
		$this->load->model('ServerSide/SS_daftar_users', '', TRUE);
		$this->load->model('Daftar/Tbl_daftar_users');
        $this->load->model('Daftar/Tbl_daftar_gambar');
		$this->load->model('Daftar/Tbl_daftar_extra');
		$this->load->model('Daftar/Tbl_daftar_pddikti');
        $this->load->model('Views/Daftar/View_daftar_users');
        $this->load->model('Views/Daftar/View_daftar_pddikti');
        $this->load->model('Views/Daftar/View_daftar_kelulusan');
        $this->load->model('Views/Daftar/View_daftar_ukt');
        $this->load->model('Views/Daftar/View_daftar_pddikti_mahasiswa');
        $this->load->model('Views/Daftar/View_daftar_pddikti_ayah');
        $this->load->model('Views/Daftar/View_daftar_pddikti_ibu');
        $this->load->model('Views/Daftar/View_daftar_pddikti_wali');
		$this->load->model('Views/Daftar/View_daftar_extra');
        $this->load->model('Settings/Tbl_setting_agama');
        $this->load->model('Settings/Tbl_setting_provinsi');
        $this->load->model('Settings/Tbl_setting_kabupaten');
        $this->load->model('Settings/Tbl_setting_kecamatan');
        $this->load->model('Settings/Tbl_setting_kelurahan');
        $this->load->model('Settings/Tbl_setting_jenis_pendaftaran');
        $this->load->model('Settings/Tbl_setting_jenis_tinggal');
        $this->load->model('Settings/Tbl_setting_alat_transportasi');
        $this->load->model('Settings/Tbl_setting_pendidikan');
        $this->load->model('Settings/Tbl_setting_pekerjaan');
        $this->load->model('Settings/Tbl_setting_penghasilan');
        $this->load->model('Settings/Tbl_setting_rekening_listrik');
        $this->load->model('Settings/Tbl_setting_pembayaran_listrik');
        $this->load->model('Settings/Tbl_setting_rekening_pbb');
        $this->load->model('Settings/Tbl_setting_pembayaran_pbb');
        $this->load->model('Settings/Tbl_setting_tanggungan');
	}

	function index(){
		$this->load->view('daftar/users');
	}

    function BelumDaftar(){
    	$search = array(
    		'jalur_masuk' => 'SBMPTN',
    		'status_pendaftaran' => 'BELUM DAFTAR'
    	);
        $viewDaftarKelulusan = $this->View_daftar_kelulusan->whereAnd($search)->result();
        $data = array(
            'viewDaftar'   => $viewDaftarKelulusan,
        );
        $this->load->view('Daftar/Export/data', $data);
    }

    function BelumVerifikasi(){
    	$search = array(
    		'jalur_masuk' => 'MANDIRI',
    		'status_verifikasi' => 'BELUM VERIFIKASI'
    	);
        $viewDaftarUsers = $this->View_daftar_users->whereAnd($search)->result();
        $data = array(
            'viewDaftar'   => $viewDaftarUsers,
            'jalur_masuk' => 'MANDIRI',
        );
        $this->load->view('Daftar/Export/data', $data);
    }

    function SudahVerifikasi(){
    	$search = array(
    		'jalur_masuk' => 'SBMPTN',
    		'status_verifikasi' => 'SUDAH VERIFIKASI'
    	);
        $viewDaftarUsers = $this->View_daftar_users->whereAnd($search)->result();
        $data = array(
            'viewDaftar'   => $viewDaftarUsers,
        );
        $this->load->view('Daftar/Export/data', $data);
    }
}
