<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class PDDIKTI extends CI_Controller {
    
    function __construct(){
        parent::__construct();
        ini_set('memory_limit', '-1');
        date_default_timezone_set('Asia/Jakarta');
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != 'DEVELOPMENT') {
            $this->session->set_flashdata('message','Hak akses ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        $this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
        $this->load->model('Daftar/Tbl_daftar_mahasiswa');
        $this->load->model('Settings/Tbl_setting_penghasilan');
        $this->load->model('Settings/Tbl_setting_pekerjaan');
        $this->load->model('Settings/Tbl_setting_pendidikan');
        $this->load->model('Views/Daftar/View_daftar_users');
        $this->load->model('Views/Daftar/View_daftar_mahasiswa');
        $this->load->model('Views/Daftar/View_daftar_kelulusan');
        $this->load->model('Views/Daftar/View_daftar_ukt');
        $this->load->model('Views/Daftar/View_daftar_orangtua');
        $this->load->model('Views/Daftar/View_daftar_extra');
    }

    function index(){
        $rules = array(
            'select'    => 'YEAR(date_created) as tahun',
            'where'     => null,
            'order'     => null,
        );
        $data = array(
            'content'       => 'Daftar/Export/pddikti/content',
            'css'           => 'Daftar/Export/pddikti/css',
            'javascript'    => 'Daftar/Export/pddikti/javascript',
            'modal'         => 'Daftar/Export/pddikti/modal',
            'tahun' => $this->View_daftar_users->distinct($rules)->result(),
        );
        $this->load->view('index',$data);
    }
    
    function actionExport(){
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', '-1');
        $tahun = $this->input->post('tahun');
        $rules = array(
            'select'    => null,
            'where'     => array(
                'YEAR(date_created)' => $tahun,
                'verifikasi' => "SUDAH VERIFIKASI"
            ),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $viewUsers = $this->View_daftar_users->where($rules);
        if ($viewUsers->num_rows() > 0){
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getProperties()
                ->setCreator("Piscal Pratama Putra") //creator
                ->setTitle("Export data Akademik");  //file title
            $objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
            $objget = $objPHPExcel->getActiveSheet();  //inisiasi get object
            //$objget->setTitle('Sample Sheet'); //sheet title
            //Warna header tabel
            /*$objget->getStyle("A1:C1")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '92d050')
                    ),
                    'font' => array(
                        'color' => array('rgb' => '000000')
                    )
                )
            );*/
            //table header
            $cols = array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","AA","AB","AC","AD","AE","AF","AG","AH","AI","AJ","AK","AL","AM","AN","AO","AP","AQ","AR","AS","AT","AU","AV");
            $val = array(
                "NIM", "Nama", "Tempat Lahir", "Tanggal Lahir", "Jenis Kelamin", "NIK", "Agama", "NISN", "Jalur Masuk", "NPWP", "Kewarganegaraan", "Jenis Pendaftaran", "Tanggal Masuk Kuliah", "Mulai Semester", "Jalan", "RT", "RW", "Nama Dusun", "Kelurahan", "Kecamatan", "Kode POS", "Jenis Tinggal", "Alat Transportasi", "Telp Rumah", "No HP", "Email", "Terima KPS", "No KPS", "NIK Ayah", "Nama Ayah", "Tanggal Lahir Ayah", "Pendidikan Ayah", "Pekerjaan Ayah", "Penghasilan Ayah", "NIK Ibu", "Nama Ibu", "Tanggal Lahir Ibu", "Pendidikan Ibu", "Pekerjaan Ibu", "Penghasilan Ibu", "NIK Wali", "Nama Wali", "Tanggal Lahir Wali", "Pendidikan Wali", "Pekerjaan Wali", "Penghasilan Wali", "Program Studi", "Fakultas"
            );
            for ($a = 0; $a < 48; $a++) {
                $objset->setCellValue($cols[$a].'1', $val[$a]);
                //Setting lebar cell
                $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getStyle('F')->getNumberFormat()->setFormatCode('@');
                $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('U')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('V')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('W')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getStyle('X')->getNumberFormat()
    ->setFormatCode('@');
                $objPHPExcel->getActiveSheet()->getStyle('Y')->getNumberFormat()
    ->setFormatCode('@');
                $objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('AC')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('AD')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('AE')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('AF')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('AG')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('AH')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('AI')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('AJ')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('AK')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('AL')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('AM')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('AN')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('AO')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('AP')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('AQ')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('AR')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('AS')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('AT')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('AU')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('AV')->setAutoSize(true);
                $style = array(
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    )
                );
                $objPHPExcel->getActiveSheet()->getStyle($cols[$a].'1')->applyFromArray($style);
            }
            $baris  = 2;
            $no = 1;
            foreach ($viewUsers->result() as $value){
                //pemanggilan sesuaikan dengan nama kolom tabel
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'id_daftar_users' => $value->id_daftar_users,
                        'YEAR(date_created)' => $tahun,
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $viewPMahasiswa = $this->View_daftar_mahasiswa->where($rules)->row();

                $rules[0] = array(
                    'select'    => null,
                    'where'     => array(
                        'id_daftar_users' => $viewPMahasiswa->id_daftar_users,
                        'orangtua' => 'Ayah'
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $viewOAyah = $this->View_daftar_orangtua->where($rules[0])->result();
                foreach($viewOAyah as $dataOrangtua){
                    $nik_ayah = $dataOrangtua->nik_orangtua;
                    $nama_ayah = $dataOrangtua->nama_orangtua;
                    $tgl_lhr_ayah = $dataOrangtua->tgl_lhr_orangtua;
                    $pendidikan_ayah = $dataOrangtua->pendidikan;
                    $pekerjaan_ayah = $dataOrangtua->pekerjaan;
                    $penghasilan_ayah = $dataOrangtua->penghasilan;
                }

                $rules[1] = array(
                    'select'    => null,
                    'where'     => array(
                        'id_daftar_users' => $viewPMahasiswa->id_daftar_users,
                        'orangtua' => 'Ibu'
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $viewOIbu = $this->View_daftar_orangtua->where($rules[1])->result();
                foreach($viewOIbu as $dataOrangtua){
                    $nik_ibu = $dataOrangtua->nik_orangtua;
                    $nama_ibu = $dataOrangtua->nama_orangtua;
                    $tgl_lhr_ibu = $dataOrangtua->tgl_lhr_orangtua;
                    $pendidikan_ibu = $dataOrangtua->pendidikan;
                    $pekerjaan_ibu = $dataOrangtua->pekerjaan;
                    $penghasilan_ibu = $dataOrangtua->penghasilan;
                }

                $rules[2] = array(
                    'select'    => null,
                    'where'     => array(
                        'id_daftar_users' => $viewPMahasiswa->id_daftar_users,
                        'orangtua' => 'Wali'
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $viewOWali = $this->View_daftar_orangtua->where($rules[2])->result();
                foreach($viewOWali as $dataOrangtua){
                    $nik_wali = $dataOrangtua->nik_orangtua;
                    $nama_wali = $dataOrangtua->nama_orangtua;
                    $tgl_lhr_wali = $dataOrangtua->tgl_lhr_orangtua;
                    $pendidikan_wali = $dataOrangtua->pendidikan;
                    $pekerjaan_wali = $dataOrangtua->pekerjaan;
                    $penghasilan_wali = $dataOrangtua->penghasilan;
                }

                $rules11 = array(
                    'select'    => null,
                    'where'     => array(
                        'id_daftar_users' => $value->id_daftar_users,
                        'YEAR(date_created)' => $tahun
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $viewExtra = $this->View_daftar_extra->where($rules11)->row();

                if($viewPMahasiswa->agama == "ISLAM"){
                    $id_agama = "1";
                }else if($viewPMahasiswa->agama == "KRISTEN"){
                    $id_agama = "2";
                }else if($viewPMahasiswa->agama == "HINDU"){
                    $id_agama = "4";
                }else if($viewPMahasiswa->agama == "BUDHA"){
                    $id_agama = "5";
                }else if($viewPMahasiswa->agama == "KONGHUCU"){
                    $id_agama = "6";
                }else{
                    $id_agama = "99";
                }

                if($viewPMahasiswa->jenis_kelamin == "PEREMPUAN"){
                    $jk = "P";
                }else if($viewPMahasiswa->jenis_kelamin == "LAKI-LAKI"){
                    $jk = "L";
                }

                if($viewPMahasiswa->jenis_pendaftaran == "PESERTA DIDIK BARU"){
                    $id_jenis_pendaftaran = "1";
                }else if($viewPMahasiswa->jenis_pendaftaran == "PINDAHAN"){
                    $id_jenis_pendaftaran = "2";
                }

                if($viewPMahasiswa->terima_kps == "IYA"){
                    $id_terima_kps = "1";
                }else if($viewPMahasiswa->terima_kps == "TIDAK"){
                    $id_terima_kps = "0";
                }

                if($viewPMahasiswa->jenis_tinggal == "BERSAMA ORANG TUA"){
                    $id_jenis_tinggal = "1";
                }else if($viewPMahasiswa->jenis_tinggal == "WALI"){
                    $id_jenis_tinggal = "2";
                }else if($viewPMahasiswa->jenis_tinggal == "KOST"){
                    $id_jenis_tinggal = "3";
                }else if($viewPMahasiswa->jenis_tinggal == "ASRAMA"){
                    $id_jenis_tinggal = "4";
                }else if($viewPMahasiswa->jenis_tinggal == "PANTI ASUHAN"){
                    $id_jenis_tinggal = "5";
                }else if($viewPMahasiswa->jenis_tinggal == "LAINNYA"){
                    $id_jenis_tinggal = "99";
                }

                $objset->setCellValue("A".$baris, $viewPMahasiswa->nim);
                $objset->setCellValue("B".$baris, $value->nama);
                $objset->setCellValue("C".$baris, $viewPMahasiswa->tmp_lhr);
                $objset->setCellValue("D".$baris, $value->tgl_lhr);
                $objset->setCellValue("E".$baris, $jk);
                $objset->setCellValue("F".$baris, '`'.$value->nik_passport.'`');
                $objset->setCellValue("G".$baris, $id_agama);
                $objset->setCellValue("H".$baris, $viewPMahasiswa->nisn);
                $objset->setCellValue("I".$baris, $viewPMahasiswa->jalur_masuk);
                $objset->setCellValue("J".$baris, $viewPMahasiswa->npwp);
                $objset->setCellValue("K".$baris, $viewPMahasiswa->warga_negara);
                $objset->setCellValue("L".$baris, $id_jenis_pendaftaran);
                $objset->setCellValue("M".$baris, '');
                $objset->setCellValue("N".$baris, $viewPMahasiswa->mulai_semester);
                $objset->setCellValue("O".$baris, $viewPMahasiswa->jalan);
                $objset->setCellValue("P".$baris, $viewPMahasiswa->rt);
                $objset->setCellValue("Q".$baris, $viewPMahasiswa->rw);
                $objset->setCellValue("R".$baris, $viewPMahasiswa->nama_dusun);
                $objset->setCellValue("S".$baris, $viewPMahasiswa->kelurahan);
                $objset->setCellValue("T".$baris, $viewPMahasiswa->kecamatan);
                $objset->setCellValue("U".$baris, $viewPMahasiswa->kode_pos);
                $objset->setCellValue("V".$baris, $id_jenis_tinggal);
                $objset->setCellValue("W".$baris, $viewPMahasiswa->alat_transportasi);
                $objset->setCellValue("X".$baris, '`'.$viewPMahasiswa->tlp_rmh.'`');
                $objset->setCellValue("Y".$baris, '`'.$value->nmr_hp.'`');
                $objset->setCellValue("Z".$baris, $value->username);
                $objset->setCellValue("AA".$baris, $viewPMahasiswa->terima_kps);
                $objset->setCellValue("AB".$baris, $viewPMahasiswa->no_kps);
                $objset->setCellValue("AC".$baris, '`'.$nik_ayah.'`');
                $objset->setCellValue("AD".$baris, $nama_ayah);
                $objset->setCellValue("AE".$baris, $tgl_lhr_ayah);
                $objset->setCellValue("AF".$baris, $pendidikan_ayah);
                $objset->setCellValue("AG".$baris, $pekerjaan_ayah);
                $objset->setCellValue("AH".$baris, $penghasilan_ayah);
                $objset->setCellValue("AI".$baris, '`'.$nik_ibu.'`');
                $objset->setCellValue("AJ".$baris, $nama_ibu);
                $objset->setCellValue("AK".$baris, $tgl_lhr_ibu);
                $objset->setCellValue("AL".$baris, $pendidikan_ibu);
                $objset->setCellValue("AM".$baris, $pekerjaan_ibu);
                $objset->setCellValue("AN".$baris, $penghasilan_ibu);
                if(!empty($nik_wali)){
                    $nik_wali = $nik_wali;
                }else{
                    $nik_wali = "";
                }
                if(!empty($nama_wali)){
                    $nama_wali = $nama_wali;
                }else{
                    $nama_wali = "";
                }
                if(!empty($tgl_lhr_wali)){
                    $tgl_lhr_wali = $tgl_lhr_wali;
                }else{
                    $tgl_lhr_wali = "";
                }
                if(!empty($pendidikan_wali)){
                    $pendidikan_wali = $pendidikan_wali;
                }else{
                    $pendidikan_wali = "";
                }
                if(!empty($pekerjaan_wali)){
                    $pekerjaan_wali = $pekerjaan_wali;
                }else{
                    $pekerjaan_wali = "";
                }
                if(!empty($penghasilan_wali)){
                    $penghasilan_wali = $penghasilan_wali;
                }else{
                    $penghasilan_wali = "";
                }
                $objset->setCellValue("AO".$baris, '`'.$nik_wali.'`');
                $objset->setCellValue("AP".$baris, $nama_wali);
                $objset->setCellValue("AQ".$baris, $tgl_lhr_wali);
                $objset->setCellValue("AR".$baris, $pendidikan_wali);
                $objset->setCellValue("AS".$baris, $pekerjaan_wali);
                $objset->setCellValue("AT".$baris, $penghasilan_wali);
                $objset->setCellValue("AU".$baris, $value->jurusan);
                $objset->setCellValue("AV".$baris, $value->fakultas);
                //Set number value
                //$objPHPExcel->getActiveSheet()->getStyle('C1:C'.$baris)->getNumberFormat()->setFormatCode('0');

                $baris++;
                $no++;
            }
            $objPHPExcel->getActiveSheet()->setTitle('Data Export');

            $objPHPExcel->setActiveSheetIndex(0);
            $filename = urlencode("PDDIKTI_".$tahun."_".date("Y_m_d_H_i_s").".xls");
            header('Content-Type: application/vnd.ms-excel'); //mime type
            header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
            header('Cache-Control: max-age=0'); //no cache
            $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
        }else{
            $this->session->set_flashdata('message','Data kosong.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Daftar/Export/Pddikti/');
        }
    }

}
