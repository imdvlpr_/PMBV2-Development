<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Kelulusan extends CI_Controller {
	
    function __construct(){
        parent::__construct();
        ini_set('memory_limit', '-1');
        date_default_timezone_set('Asia/Jakarta');
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != 'DEVELOPMENT') {
            $this->session->set_flashdata('message','Hak akses ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        $this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
        $this->load->model('Daftar/Tbl_daftar_mahasiswa');
        $this->load->model('Views/Daftar/View_daftar_users');
        $this->load->model('Views/Daftar/View_daftar_kelulusan');
        $this->load->model('Views/Daftar/View_daftar_mahasiswa');
        $this->load->model('Views/Daftar/View_daftar_kelulusan');
        $this->load->model('Views/Daftar/View_daftar_ukt');
        $this->load->model('Views/Daftar/View_daftar_extra');
        $this->load->model('Settings/Tbl_setting_jalur_masuk');
    }

    function index(){
        $rules = array(
            'select'    => 'YEAR(date_created) as tahun',
            'where'     => null,
            'order'     => null,
        );
        $rules[0] = array(
            'select'    => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
    	$data = array(
            'content'       => 'Daftar/Export/kelulusan/content',
            'css'           => 'Daftar/Export/kelulusan/css',
            'javascript'    => 'Daftar/Export/kelulusan/javascript',
            'modal'         => 'Daftar/Export/kelulusan/modal',
            'tahun'	=> $this->View_daftar_kelulusan->distinct($rules)->result(),
            'jalur_masuk' => $this->Tbl_setting_jalur_masuk->read($rules[0])->result()
		);
    	$this->load->view('index',$data);
	}
	
    function actionExport(){
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', '-1');
        $tahun = $this->input->post('tahun');
        $rules = array(
            'select'    => null,
            'where'     => array(
                'YEAR(date_created)' => $tahun,
                'status_pendaftaran' => $this->input->post('status_pendaftaran'),
                'id_jlr_msk' => $this->input->post('jalur_masuk')
            ),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $viewKelulusan = $this->View_daftar_kelulusan->where($rules);
        if ($viewKelulusan->num_rows() > 0){
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getProperties()
                ->setCreator("Piscal Pratama Putra") //creator
                ->setTitle("Export data Kelulusan");  //file title
            $objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
            $objget = $objPHPExcel->getActiveSheet();  //inisiasi get object
            //$objget->setTitle('Sample Sheet'); //sheet title
            //Warna header tabel
            /*$objget->getStyle("A1:C1")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '92d050')
                    ),
                    'font' => array(
                        'color' => array('rgb' => '000000')
                    )
                )
            );*/
            //table header
            $cols = array("A","B","C","D","E","F");
            $val = array(
                "Nomor Peserta","Nama","Jurusan", "Fakultas", "Jalur Masuk", "Status Pendaftaran"
            );
            for ($a = 0; $a < 6; $a++) {
                $objset->setCellValue($cols[$a].'1', $val[$a]);
                //Setting lebar cell
                $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
                
                $style = array(
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    )
                );
                $objPHPExcel->getActiveSheet()->getStyle($cols[$a].'1')->applyFromArray($style);
            }
            $baris  = 2;
            $no = 1;
            foreach ($viewKelulusan->result() as $value){
                //pemanggilan sesuaikan dengan nama kolom tabel
                $objset->setCellValue("A".$baris, $value->nomor_peserta);
                $objset->setCellValue("B".$baris, $value->nama);
                $objset->setCellValue("C".$baris, $value->jurusan);
                $objset->setCellValue("D".$baris, $value->fakultas);
                $objset->setCellValue("E".$baris, $value->jalur_masuk);
                $objset->setCellValue("F".$baris, $value->status_pendaftaran);

                //Set number value
                //$objPHPExcel->getActiveSheet()->getStyle('C1:C'.$baris)->getNumberFormat()->setFormatCode('0');

                $baris++;
                $no++;
            }
            $objPHPExcel->getActiveSheet()->setTitle('Data Export');

            $objPHPExcel->setActiveSheetIndex(0);
            $filename = urlencode("Kelulusan_".$this->input->post('jalur_masuk')."_".$tahun."_".date("Y_m_d_H_i_s").".xls");
            header('Content-Type: application/vnd.ms-excel'); //mime type
            header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
            header('Cache-Control: max-age=0'); //no cache
            $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
        }else{
            $this->session->set_flashdata('message','Data kosong.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Daftar/Export/Kelulusan/');
        }
    }

}
