<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Akademik extends CI_Controller {
	
    function __construct(){
        parent::__construct();
        ini_set('memory_limit', '-1');
        date_default_timezone_set('Asia/Jakarta');
        if ($this->session->userdata('development') == FALSE) {
            $this->session->set_flashdata('message','Session tidak tersedia.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        if ($this->session->userdata('level') != 'DEVELOPMENT') {
            $this->session->set_flashdata('message','Hak akses ditolak.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Auth');
        }
        $this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
        $this->load->model('Daftar/Tbl_daftar_mahasiswa');
        $this->load->model('Views/Daftar/View_daftar_users');
        $this->load->model('Views/Daftar/View_daftar_mahasiswa');
        $this->load->model('Views/Daftar/View_daftar_orangtua');
        $this->load->model('Views/Daftar/View_daftar_kelulusan');
        $this->load->model('Views/Daftar/View_daftar_ukt');
		$this->load->model('Views/Daftar/View_daftar_extra');
    }

    function index(){
        $rules = array(
            'select'    => 'YEAR(date_created) as tahun',
            'where'     => null,
            'order'     => null,
        );
    	$data = array(
            'content'       => 'Daftar/Export/akademik/content',
            'css'           => 'Daftar/Export/akademik/css',
            'javascript'    => 'Daftar/Export/akademik/javascript',
            'modal'         => 'Daftar/Export/akademik/modal',
    		'tahun'	=> $this->View_daftar_users->distinct($rules)->result(),
		);
    	$this->load->view('index',$data);
	}
	
    function actionExport(){
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', '-1');
        $tahun = $this->input->post('tahun');
        $rules = array(
            'select'    => null,
            'where'     => array(
                'YEAR(date_created)' => $tahun,
                'verifikasi' => "SUDAH VERIFIKASI"
            ),
            'or_where'  => null,
            'order'     => null,
            'limit'     => null,
            'pagging'   => null,
        );
        $viewUsers = $this->View_daftar_users->where($rules);
        if ($viewUsers->num_rows() > 0){
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getProperties()
                ->setCreator("Piscal Pratama Putra") //creator
                ->setTitle("Export data Akademik");  //file title
            $objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
            $objget = $objPHPExcel->getActiveSheet();  //inisiasi get object
            //$objget->setTitle('Sample Sheet'); //sheet title
            //Warna header tabel
            /*$objget->getStyle("A1:C1")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '92d050')
                    ),
                    'font' => array(
                        'color' => array('rgb' => '000000')
                    )
                )
            );*/
            //table header
            $cols = array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X");
            $val = array(
                "Nomor Peserta","NIK(No.KTP)","Nama", "Tempat Lahir","Tanggal Lahir","Jenis Kelamin","Warga Negara","Agama", "Alamat","Provinsi",
                "Kabupaten / Kota","Kecamatan","Kelurahan","Kode Pos","Telepon","Nama Ayah","Nama Ibu","Penghasilan Ayah",
                "Penghasilan Ibu","Pekerjaan Ayah","Pekerjaan Ibu","Pendidikan Ayah","Pendidikan Ibu","Rumpun"
            );
            for ($a = 0; $a < 24; $a++) {
                $objset->setCellValue($cols[$a].'1', $val[$a]);
                //Setting lebar cell
                $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('U')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('V')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('W')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('X')->setAutoSize(true);
                $style = array(
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    )
                );
                $objPHPExcel->getActiveSheet()->getStyle($cols[$a].'1')->applyFromArray($style);
            }
            $baris  = 2;
            $no = 1;
            foreach ($viewUsers->result() as $value){
                //pemanggilan sesuaikan dengan nama kolom tabel
                $rules = array(
                    'select'    => null,
                    'where'     => array(
                        'id_daftar_users' => $value->id_daftar_users,
                        'YEAR(date_created)' => $tahun,
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $viewPMahasiswa = $this->View_daftar_mahasiswa->where($rules)->row();
                $rules[0] = array(
                    'select'    => null,
                    'where'     => array(
                        'id_daftar_users' => $viewPMahasiswa->id_daftar_users,
                        'orangtua' => 'Ayah'
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $viewOAyah = $this->View_daftar_orangtua->where($rules[0])->result();
                foreach($viewOAyah as $dataOrangtua){
                    $nik_ayah = $dataOrangtua->nik_orangtua;
                    $nama_ayah = $dataOrangtua->nama_orangtua;
                    $tgl_lhr_ayah = $dataOrangtua->tgl_lhr_orangtua;
                    $pendidikan_ayah = $dataOrangtua->pendidikan;
                    $pekerjaan_ayah = $dataOrangtua->pekerjaan;
                    $penghasilan_ayah = $dataOrangtua->penghasilan;
                }

                $rules[1] = array(
                    'select'    => null,
                    'where'     => array(
                        'id_daftar_users' => $viewPMahasiswa->id_daftar_users,
                        'orangtua' => 'Ibu'
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $viewOIbu = $this->View_daftar_orangtua->where($rules[1])->result();
                foreach($viewOIbu as $dataOrangtua){
                    $nik_ibu = $dataOrangtua->nik_orangtua;
                    $nama_ibu = $dataOrangtua->nama_orangtua;
                    $tgl_lhr_ibu = $dataOrangtua->tgl_lhr_orangtua;
                    $pendidikan_ibu = $dataOrangtua->pendidikan;
                    $pekerjaan_ibu = $dataOrangtua->pekerjaan;
                    $penghasilan_ibu = $dataOrangtua->penghasilan;
                }

                $rules[2] = array(
                    'select'    => null,
                    'where'     => array(
                        'id_daftar_users' => $viewPMahasiswa->id_daftar_users,
                        'orangtua' => 'Wali'
                    ),
                    'or_where'  => null,
                    'order'     => null,
                    'limit'     => null,
                    'pagging'   => null,
                );
                $viewOWali = $this->View_daftar_orangtua->where($rules[2])->result();
                foreach($viewOWali as $dataOrangtua){
                    $nik_wali = $dataOrangtua->nik_orangtua;
                    $nama_wali = $dataOrangtua->nama_orangtua;
                    $tgl_lhr_wali = $dataOrangtua->tgl_lhr_orangtua;
                    $pendidikan_wali = $dataOrangtua->pendidikan;
                    $pekerjaan_wali = $dataOrangtua->pekerjaan;
                    $penghasilan_wali = $dataOrangtua->penghasilan;
                }
                $objset->setCellValue("A".$baris, $value->nomor_peserta);
                $objset->setCellValue("B".$baris, '`'.$value->nik_passport.'`');
                $objset->setCellValue("C".$baris, $value->nama);
                $objset->setCellValue("D".$baris, $viewPMahasiswa->tmp_lhr);
                $objset->setCellValue("E".$baris, $value->tgl_lhr);
                $objset->setCellValue("F".$baris, $viewPMahasiswa->jenis_kelamin);
                $objset->setCellValue("G".$baris, $viewPMahasiswa->warga_negara);
                $objset->setCellValue("H".$baris, $viewPMahasiswa->agama);
                $objset->setCellValue("I".$baris, $viewPMahasiswa->jalan);
                $objset->setCellValue("J".$baris, $viewPMahasiswa->provinsi);
                $objset->setCellValue("K".$baris, $viewPMahasiswa->kabupaten);
                $objset->setCellValue("L".$baris, $viewPMahasiswa->kecamatan);
                $objset->setCellValue("M".$baris, $viewPMahasiswa->kelurahan);
                $objset->setCellValue("N".$baris, $viewPMahasiswa->kode_pos);
                $objset->setCellValue("O".$baris, '`'.$value->nmr_hp.'`');
                $objset->setCellValue("P".$baris, $nama_ayah);
                $objset->setCellValue("Q".$baris, $nama_ibu);
                $objset->setCellValue("R".$baris, $penghasilan_ayah);
                $objset->setCellValue("S".$baris, $penghasilan_ibu);
                $objset->setCellValue("T".$baris, $pekerjaan_ayah);
                $objset->setCellValue("U".$baris, $pekerjaan_ibu);
                $objset->setCellValue("V".$baris, $pendidikan_ayah);
                $objset->setCellValue("W".$baris, $pendidikan_ibu);
                $objset->setCellValue("X".$baris, $viewPMahasiswa->rumpun);

                //Set number value
                //$objPHPExcel->getActiveSheet()->getStyle('C1:C'.$baris)->getNumberFormat()->setFormatCode('0');

                $baris++;
                $no++;
            }
            $objPHPExcel->getActiveSheet()->setTitle('Data Export');

            $objPHPExcel->setActiveSheetIndex(0);
            $filename = urlencode("Akademik_".$tahun."_".date("Y_m_d_H_i_s").".xls");
            header('Content-Type: application/vnd.ms-excel'); //mime type
            header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
            header('Cache-Control: max-age=0'); //no cache
            $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
        }else{
            $this->session->set_flashdata('message','Data kosong.');
            $this->session->set_flashdata('type_message','danger');
            redirect('Daftar/Export/Akademik/');
        }
    }

}
